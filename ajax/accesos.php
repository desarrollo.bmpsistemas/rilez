<?php 
require_once "../modelos/Accesos.php";

$accesos=new Accesos();

$idaccesos=isset($_REQUEST["idaccesos"])? limpiarCadena($_REQUEST["idaccesos"]):"";
$idmodulos=isset($_REQUEST["idmodulos"])? limpiarCadena($_REQUEST["idmodulos"]):"";
$idusuarios=isset($_REQUEST["idusuarios"])? limpiarCadena($_REQUEST["idusuarios"]):"";



switch ($_GET["op"]){
	case 'guardaryeditar':
		if (empty($idaccesos)){
			$rspta=$accesos->insertar($idmodulos,$idusuarios);
			echo $rspta ? "Acceso registrado " : "Acceso no se pudo registrar ";
		}
		else {
			$rspta=$accesos->editar($idaccesos,$idmodulos,$idusuarios);
			echo $rspta ? "Acceso actualizada" : "Acceso no se pudo actualizar";
		}
	break;

	case 'desactivar':
		$rspta=$accesos->desactivar($idaccesos);
 		echo $rspta ? "Acceso Desactivada" : "Acceso no se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$accesos->activar($idaccesos);
 		echo $rspta ? "Acceso activada" : "Acceso no se puede activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$accesos->mostrar($idaccesos);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$accesos->listar($idusuarios);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idaccesos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idaccesos.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idaccesos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idaccesos.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->modulo,
 				"2"=>$reg->usuario,
 				"3"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "select":
		require_once "../modelos/Accesos.php";
		$accesos = new Accesos();
		$rspta = $accesos->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idaccesos . '>' . $reg->nombre . '</option>';
				}
	break;
}
?>