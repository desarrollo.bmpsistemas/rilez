<?php 
require_once "../modelos/Accesos_puestos.php";

$accesos_puestos=new Accesos_puestos();

$idaccesos_puestos=isset($_REQUEST["idaccesos_puestos"])? limpiarCadena($_REQUEST["idaccesos_puestos"]):"";
$idpuestos=isset($_REQUEST["idpuestos"])? limpiarCadena($_REQUEST["idpuestos"]):"";
$idmodulos=isset($_REQUEST["idmodulos"])? limpiarCadena($_REQUEST["idmodulos"]):"";


switch ($_GET["op"]){
	case 'guardaryeditar':
		if (empty($idaccesos_puestos)){
			$rspta=$accesos_puestos->insertar($idpuestos,$idmodulos);
			echo $rspta ? "Accesos Puesto registgrado " : "Accesos Puestono se pudo registrar $idpuestos,$idmodulos";
		}
		else {
			$rspta=$accesos_puestos->editar($idaccesos_puestos,$idpuestos,$idmodulos);
			echo $rspta ? "Accesos Puesto actualizada" : "Accesos Puestono se pudo actualizar ";
		}
	break;

	case 'desactivar':
		$rspta=$accesos_puestos->desactivar($idaccesos_puestos);
 		echo $rspta ? "Accesos Puesto Desactivada" : "Accesos Puestono se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$accesos_puestos->activar($idaccesos_puestos);
 		echo $rspta ? "Accesos Puesto activada" : "Accesos Puestono se puede activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$accesos_puestos->mostrar($idaccesos_puestos);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$accesos_puestos->listar($idpuestos);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idaccesos_puestos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idaccesos_puestos.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idaccesos_puestos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idaccesos_puestos.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->modulo,
 				"2"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;
}
?>