<?php 
require_once "../modelos/Almacenes.php";  

$almacenes=new Almacenes(); 

$idalmacenes=isset($_REQUEST["idalmacenes"])? limpiarCadena($_REQUEST["idalmacenes"]):"";
$idmonedas=isset($_REQUEST["idmonedas"])? limpiarCadena($_REQUEST["idmonedas"]):"";
$idestatus=isset($_REQUEST["idestatus"])? limpiarCadena($_REQUEST["idestatus"]):"";
$idsucursales=isset($_REQUEST["idsucursales"])? limpiarCadena($_REQUEST["idsucursales"]):"";
$nombre=isset($_REQUEST["almacen"])? limpiarCadena($_REQUEST["almacen"]):"";
$tope=isset($_REQUEST["tope"])? limpiarCadena($_REQUEST["tope"]):"";
$idusuarios=isset($_REQUEST["idusuarios"])? limpiarCadena($_REQUEST["idusuarios"]):"";
switch ($_GET["op"]){
	case 'guardaryeditar':
		if (empty($idalmacenes)){
			$rspta=$almacenes->insertar($idsucursales,$nombre,$idmonedas,$idestatus,$tope);
			echo $rspta ? "Almacen registgrado" : "Almacen no se pudo registrar";
		}
		else {
			$rspta=$almacenes->editar($idalmacenes,$idsucursales,$nombre,$idmonedas,$idestatus,$tope);
			echo $rspta ? "Almacen actualizado" : "Almacen no se pudo actualizar $idalmacenes,$idsucursales,$nombre,$idmonedas,$tope ";
		}
	break;

	case 'desactivar':
		$rspta=$almacenes->desactivar($idalmacenes);
 		echo $rspta ? "Almacen Desactivado" : "Almacen no se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$almacenes->activar($idalmacenes);
 		echo $rspta ? "Almacen activado" : "Almacen no se puede activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$almacenes->mostrar($idalmacenes);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$almacenes->listar();
 		//Vamos a declarar un array
 		$data= Array();
 		

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idalmacenes.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idalmacenes.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idalmacenes.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idalmacenes.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->sucursal,
 				"2"=>$reg->almacen,
 				"3"=>$reg->moneda,
 				"4"=>$reg->estatus,
 				"5"=>$reg->tope,
 				"6"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;
	case "select":
	
		$rspta=$almacenes->select();
		echo '<option value="0">seleccione Almacen</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idalmacenes . '>' .$reg->nombre . '</option>';
				}
	break;
	case "selectAlmacenesPorUsuario":
		$rspta=$almacenes->selectAlmacenesPorUsuario($idusuarios);
		echo '<option value="0">seleccione Almacen</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idalmacenes . '>' .$reg->nombre . '</option>';
				}
	break;
	
	case "agregarAlmacenAlUsuario":
		$rspta=$almacenes->agregarAlmacenAlUsuario($idalmacenes,$idusuarios);
		echo $rspta ? "Almacen desactivado del usuario " : "Almacen no se puede desactivar del usuarios";
	break;

	case "quitarAlmacenAlUsuario":
		$rspta=$almacenes->quitarAlmacenAlUsuario($idusuarios,$idalmacenes);
		echo $rspta ? "Almacen desactivado del usuario " : "Almacen no se puede desactivar del usuarios";
	break;
	
	
}
?>