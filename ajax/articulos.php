<?php 
require_once "../modelos/Articulos.php";  

//ini_set ( 'max_execution_time', 3600); 
$articulos=new Articulos();


$idalmacenes=isset($_REQUEST["idalmacenes"])? limpiarCadena($_REQUEST["idalmacenes"]):"";
$referencia=isset($_REQUEST["referencia"])? limpiarCadena($_REQUEST["referencia"]):"";
$Opcion=isset($_REQUEST["Opcion"])? limpiarCadena($_REQUEST["Opcion"]):"";
$opc=isset($_REQUEST["opc"])? limpiarCadena($_REQUEST["opc"]):"";
$Limite=isset($_REQUEST["Limite"])? limpiarCadena($_REQUEST["Limite"]):"";
//if (empty($Limite)) $Limite=0;
$idarticulos=isset($_REQUEST["idarticulos"])? limpiarCadena($_REQUEST["idarticulos"]):"";
$idlineas=isset($_REQUEST["idlineas"])? limpiarCadena($_REQUEST["idlineas"]):"";
$idsublineas=isset($_REQUEST["idsublineas"])? limpiarCadena($_REQUEST["idsublineas"]):"";
$codigo=isset($_REQUEST["codigo"])? limpiarCadena($_REQUEST["codigo"]):"";
$nombre=isset($_REQUEST["nombre"])? limpiarCadena($_REQUEST["nombre"]):"";
$imagen=isset($_REQUEST["imagen"])? limpiarCadena($_REQUEST["imagen"]):"";
$vendible=isset($_REQUEST["vendible"])? limpiarCadena($_REQUEST["vendible"]):"";
$producible=isset($_REQUEST["producible"])? limpiarCadena($_REQUEST["producible"]):"";

$inventariable=isset($_REQUEST["inventariable"])? limpiarCadena($_REQUEST["inventariable"]):"";
$cambio_precio=isset($_REQUEST["cambio_precio"])? limpiarCadena($_REQUEST["cambio_precio"]):"";
$peso=isset($_REQUEST["peso"])? limpiarCadena($_REQUEST["peso"]):"";
$volumen=isset($_REQUEST["volumen"])? limpiarCadena($_REQUEST["volumen"]):"";
$condicion=isset($_REQUEST["condicion"])? limpiarCadena($_REQUEST["condicion"]):"";
$idsat_unidad_medidas=isset($_REQUEST["idsat_unidad_medidas"])? limpiarCadena($_REQUEST["idsat_unidad_medidas"]):"";
$idsat_prod_servs=isset($_REQUEST["idsat_prod_servs"])? limpiarCadena($_REQUEST["idsat_prod_servs"]):"";
$idestatus=isset($_REQUEST["idestatus"])? limpiarCadena($_REQUEST["idestatus"]):"";
//echo "op ".$_REQUEST["op"];
switch ($_REQUEST["op"]){
	case 'guardaryeditar':

		if (!file_exists($_FILES['imagen']['tmp_name']) || !is_uploaded_file($_FILES['imagen']['tmp_name']))
		{
			$imagen=$_REQUEST["imagenactual"];
		}
		else 
		{
			$ext = explode(".", $_FILES["imagen"]["name"]);
			if ($_FILES['imagen']['type'] == "image/jpg" || $_FILES['imagen']['type'] == "image/jpeg" || $_FILES['imagen']['type'] == "image/png")
			{
				
				$imagen = $codigo . '.' . end($ext);
				$imagen = strtoupper($imagen);
				
				move_uploaded_file($_FILES["imagen"]["tmp_name"], "../files/articulos/" . $imagen);
			}
		}
		if (empty($idarticulos)){
			$rspta=$articulos->insertar($idsublineas,$codigo,$nombre,$idsat_unidad_medidas,$idsat_prod_servs,$producible,$vendible,$cambio_precio,$inventariable,$peso,$volumen,$imagen,$idestatus,$condicion);
			if ($rspta>0)
				echo $rspta;
			else  
				echo "Artículo no se pudo registrar $rspta ";
		}
		else {
			$rspta=$articulos->editar($idarticulos,$idsublineas,$codigo,$nombre,$idsat_unidad_medidas,$idsat_prod_servs,$producible,$vendible,$cambio_precio,$inventariable,$peso,$volumen,$imagen,$idestatus);
			echo $rspta ? "Artículo actualizado $idarticulos,$idsublineas,$codigo,$nombre,$idsat_unidad_medidas,$idsat_prod_servs,$producible,$vendible,$cambio_precio,$inventariable,$peso,$volumen,$imagen,$idestatus" : "Artículo no se pudo actualizar ";
		}
	break;
	case 'migrarArticulos': 
		$rspta=$articulos->listarTemporal($nombre);
		//$datos=var_dump($rspta);
		//echo $rspta ? "datos extraidos $rspta" : "consulta sin datos";
		
		$registros=0;
		while ($reg=$rspta->fetch_object())
		{
 			$registros++;
 			
 			echo "$Fact_Folio,$Fact_Fecha,$NoIdentificacion,$ClaveUnidad,$ClaveProdServ,$Unidad,$Descripcion,$ValorUnitario,$nombre";
 			$rs=$articulos->insertar(1,$reg->NoIdentificacion,$reg->Descripcion,$reg->ClaveUnidad,$reg->ClaveProdServ,          "S",       "S",          "S",           "S",    0,       0,"default.jpg",1,1);		
 			                   
		}
		if ($registros>0)
			echo "Se registraron $registros productos al catalogo de articulos con el resultado $rs ";
		else
			echo "Los Artículos no se pudieron registrar con el resultado $rs  ";
		
		break;

	case 'desactivar':
		$rspta=$articulos->desactivar($idarticulos);
 		echo $rspta ? "Artículo Desactivado" : "Artículo no se puede desactivar";
	break;

	case 'activar':
		$rspta=$articulos->activar($idarticulos);
 		echo $rspta ? "Artículo activado" : "Artículo no se puede activar";
	break;

	case 'mostrar':
		$rspta=$articulos->mostrar($idarticulos);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
	break;

	case 'listar':
		$rspta=$articulos->listar($Opcion,$referencia);
 		//Vamos a declarar un array
 		//var_dump($rspta);
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idarticulos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="eliminar('.$reg->idarticulos.')"><i class="fa fa-close"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idarticulos.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idarticulos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idarticulos.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->codigo,
 				"2"=>$reg->articulo,
 				"3"=>$reg->vendible,
 				"4"=>$reg->inventariable,
 				"5"=>$reg->cambio_precio,
 				"6"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;
	case "select":
	
		$rspta=$articulos->select($Opcion,$referencia,$Limite);
		//$registros=mysql_affected_rows($rspta);
		if ($Limite>1)
		echo '<option value=' . '>' . "Selecione Producto". '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idarticulos . '>' .$reg->codigo."#".$reg->nombre . '</option>';
				}
	break;
	case "selectProductos":
	
		$rspta=$articulos->selectProductos($idalmacenes,$referencia,$opc);
		if ($Opcion!=3)
			echo '<option value=' . '>' . "Selecione Producto". '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idarticulos . '>' .$reg->codigo."#".$reg->nombre . '</option>';
				}
	break;
	

	case "selectlineas":
		require_once "../modelos/Lineas.php";
		$lineas = new Lineas();

		$rspta = $lineas->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idlineas . '>' . $reg->nombre . '</option>';
				}
	break;
	case "selectsublineas":
		require_once "../modelos/Sublineas.php";
		$sublineas = new Sublineas();

		$rspta = $sublineas->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idsublineas . '>' . $reg->nombre . '</option>';
				}
	break;
	case "listar_sat_unidad_de_medida":

		$rspta = $articulos->listar_sat_unidad_de_medida();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->clave . '>' . $reg->nombre . '</option>';
				}
	break;
	case "listar_sat_prod_y_serv":
		$rspta = $articulos->listar_sat_prod_y_serv();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->clave . '>' . $reg->nombre . '</option>';
				}
	break;

	case "selectcatalogo":
	
		$rspta=$articulos->selectcatalogo();
		echo '<option value=' . '>' . "Selecione Producto". '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idarticulos . '>' . $reg->codigo."#".$reg->nombre . '</option>';
				}
	break;

	case "selectivas":
	
		$rspta=$articulos->selectivas();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idivas . '>' .$reg->nombre . '</option>';
				}
	break;
	
	case "selectmonedas":
	
		$rspta=$articulos->selectmonedas();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idmonedas . '>' .$reg->nombre . '</option>';
				}
	break;
	case "selectarticulosporsublinea":
	
		$rspta=$articulos->selectarticulosporsublinea($idsublineas);
		echo '<option >' ."Selecione un valor" . '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idarticulos . '>' .$reg->codigo."#".$reg->nombre . '</option>';
				}
	break;
	

}
?>