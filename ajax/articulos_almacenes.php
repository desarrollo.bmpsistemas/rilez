<?php 
require_once "../modelos/Articulos_almacenes.php"; 

$articulos_almacenes=new Articulos_almacenes();

$referencia=isset($_REQUEST["referencia"])? limpiarCadena($_REQUEST["referencia"]):"";
$Opcion=isset($_REQUEST["Opcion"])? limpiarCadena($_REQUEST["Opcion"]):"";
$idarticulos_almacenes=isset($_REQUEST["idarticulos_almacenes"])? limpiarCadena($_REQUEST["idarticulos_almacenes"]):"";
$idarticulos=isset($_REQUEST["idarticulos"])? limpiarCadena($_REQUEST["idarticulos"]):"";
$codigo=isset($_REQUEST["codigo"])? limpiarCadena($_REQUEST["codigo"]):"";
$idalmacenes=isset($_REQUEST["idalmacenes"])? limpiarCadena($_REQUEST["idalmacenes"]):"";
$stock=isset($_REQUEST["stock"])? limpiarCadena($_REQUEST["stock"]):"";
$idmonedas=isset($_REQUEST["idmonedas"])? limpiarCadena($_REQUEST["idmonedas"]):"";
$costo=isset($_REQUEST["costo"])? limpiarCadena($_REQUEST["costo"]):"";
$precio=isset($_REQUEST["precio"])? limpiarCadena($_REQUEST["precio"]):"";
$iva=isset($_REQUEST["iva"])? limpiarCadena($_REQUEST["iva"]):"";
$porcentaje_utilidad=isset($_REQUEST["porcentaje_utilidad"])? limpiarCadena($_REQUEST["porcentaje_utilidad"]):"";
$ubicacion=isset($_REQUEST["ubicacion"])? limpiarCadena($_REQUEST["ubicacion"]):"";
$afectacion=isset($_REQUEST["afectacion"])? limpiarCadena($_REQUEST["afectacion"]):"";

//variables para leer archivo temporal de xml 
$archivo=isset($_REQUEST["archivo"])? limpiarCadena($_REQUEST["archivo"]):"";
$afectacion=isset($_REQUEST["afectacion"])? limpiarCadena($_REQUEST["afectacion"]):"";
$idalmacenes=isset($_REQUEST["idalmacenes"])? limpiarCadena($_REQUEST["idalmacenes"]):"";

switch ($_GET["op"]){
	case 'guardaryeditar':
		$idarticulos_almacenes=0;
		$rspta=$articulos_almacenes->buscarArticuloEnAlmacen($idalmacenes,$idarticulos);
		while ($reg=$rspta->fetch_object())
 		$idarticulos_almacenes=$reg->idarticulos_almacenes;
 				
		if (empty($idarticulos_almacenes))
		{
			//echo "insertando $idalmacenes,$idarticulos,$stock,$idmonedas,$costo,$precio,$iva,$porcentaje_utilidad,$afectacion,$ubicacion";
			$rspta=$articulos_almacenes->insertar($idalmacenes,$idarticulos,$stock,$idmonedas,$costo,$precio,$iva,$porcentaje_utilidad,$afectacion,$ubicacion);

			if ($rspta>0) 
				echo "Articulos por Almacen registgrado ";
			else 
				echo "Articulo en Almacen no se pudo registrar $rspta";
		}
		else {
			//echo "actualizando $idarticulos_almacenes,$idalmacenes,$idarticulos,$stock,$idmonedas,$costo,$precio,$iva,$porcentaje_utilidad,$afectacion,$ubicacion";
			$rspta=$articulos_almacenes->editar($idarticulos_almacenes,$idarticulos,$idalmacenes,$stock,$idmonedas,$costo,$precio,$iva,$porcentaje_utilidad,$afectacion,$ubicacion);
			if ($rspta>0) 
				echo "Articulos por Almacen actualizado ";
			else 
				echo "Articulo en Almacen no se pudo actualizar $rspta";
		}
	break;

	case 'desactivar':
		$rspta=$articulos_almacenes->desactivar($idarticulos_almacenes);
 		echo $rspta ? "Articulo en Almacen Desactivado" : "Articulo en Almacen no se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$articulos_almacenes->activar($idarticulos_almacenes);
 		echo $rspta ? "Articulo en Almacen activado" : "Articulo en Almacen no se puedo activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$articulos_almacenes->mostrar($idarticulos_almacenes);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;
	case 'mostrar2':
		$rspta=$articulos_almacenes->mostrar2($idalmacenes,$idarticulos);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;
	case 'leerIva_y_Moneda':
		$rspta=$articulos_almacenes->leerIva_y_Moneda($idalmacenes);
 		echo json_encode($rspta);
 		break;
	break;
	

	case 'listar':
		$rspta=$articulos_almacenes->listar($codigo);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idarticulos_almacenes.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Agregar" onclick="agregar('.$reg->idarticulos_almacenes.')" ><i class="fa fa-plus-circle"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idarticulos_almacenes.')"><i class="fa fa-close"></i></button>':
 					' <button class="btn btn-warning" onclick="mostrar('.$reg->idarticulos_almacenes.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Agregar" onclick="agregar('.$reg->idarticulos_almacenes.')" ><i class="fa fa-plus-circle"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idarticulos_almacenes.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->almacen,
 				"2"=>$reg->moneda,
 				"3"=>$reg->costo,
 				"4"=>$reg->porcentaje_utilidad,
 				"5"=>$reg->precio,
 				"6"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;
	
	case 'migrarPrecios':
		$procesos=0;
		$rspta=$articulos_almacenes->leerTemporalXml($archivo);
 	
 		while ($reg=$rspta->fetch_object())
 		{
 				//leer archivo temporal_xml para obtener NoIdentificacion (idarticulo) y costo
 				$idarticulos=$reg->idarticulos;
 				$costo=$reg->costo;

 				//leer porcentaje_utilidad de acuerdo al costo y generar el precio con el porcentaje de porcentaje_utilidad
 				$porciento=0;
 				$rs=$articulos_almacenes->leerporcentaje_utilidad($costo);
 				while ($rdo=$rs->fetch_object())
 				$porciento=$rdo->porciento;
 			    if (empty($porciento)) $porciento=1;

 			    //establecer en ceros estas varialbes porque se van a leer de acuerdo a la afectacion
 			    //la cuel va hacer del almacen actual por moneda dolares,pesos o todos los almacenes; 
 			    $precio=$costo*$porciento;
 			    $iva=0;
 			    $idmonedas=0;

 			    $stock=1;

 			    $idarticulos_almacenes=0;
 			    $rs=$articulos_almacenes->buscarArticuloEnAlmacen($idalmacenes,$idarticulos);
 			    while ($rdo=$rs->fetch_object())
 				$idarticulos_almacenes=$rdo->idarticulos_almacenes;
 				if (empty($idarticulos_almacenes))
					$rsi=$articulos_almacenes->insertar($idalmacenes,$idarticulos,$stock,$idmonedas,$costo,$precio,$iva,$porciento,$afectacion,$ubicacion);
 				else
 					$rse=$articulos_almacenes->editar($idarticulos_almacenes,$idarticulos,$idalmacenes,$stock,$idmonedas,$costo,$precio,$iva,$porciento,$afectacion,'MOSTRADOR');	
 			$procesos++;	
 		}
 		
 		echo "Articulos Procesados : ".$procesos;

	break;
	case "select":
	require_once "../modelos/Almacenes.php"; 
	$almacenes=new Almacenes();
	$rspta=$almacenes->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idalmacenes . '>' .$reg->nombre . '</option>';
				}
	break;
	
}
?>