<?php 
require_once "../modelos/Articulos_proveedores.php"; 

$articulos_proveedores=new Articulos_proveedores();


$idarticulos_proveedores=isset($_REQUEST["idarticulos_proveedores"])? limpiarCadena($_REQUEST["idarticulos_proveedores"]):"";
$idarticulos=isset($_REQUEST["idarticulos"])? limpiarCadena($_REQUEST["idarticulos"]):"";
$idpersonas=isset($_REQUEST["idpersonas"])? limpiarCadena($_REQUEST["idpersonas"]):"";
$costo=isset($_REQUEST["costo"])? limpiarCadena($_REQUEST["costo"]):"";
$NoIdentificacion=isset($_REQUEST["NoIdentificacion"])? limpiarCadena($_REQUEST["NoIdentificacion"]):"";


switch ($_GET["op"]){
	case 'guardaryeditar':
		$idarticulos_proveedores=0;
		$rspta=$articulos_proveedores->buscarArtEnProv($idpersonas,$idarticulos);		
		while ($reg=$rspta->fetch_object())
 		$idarticulos_proveedores=$reg->idarticulos_proveedores;	

		if (empty($idarticulos_proveedores)){

			$rspta=$articulos_proveedores->insertar($idpersonas,$idarticulos,$costo,$NoIdentificacion);
			echo $rspta ? "Articulos por Proveedor registgrado" : "Articulo por Proveedor no se pudo registrar $idpersonas,$idarticulos,$costo,$NoIdentificacion";
		}
		else {
			$rspta=$articulos_proveedores->editar($idarticulos_proveedores,$costo,$NoIdentificacion);
			echo $rspta ? "Articulos por Proveedor actualizado" : "Articulo por Proveedor no se pudo actualizar $idarticulos_proveedores,$costo,$NoIdentificacion";
		}
	break;

	case 'desactivar':
		$rspta=$articulos_proveedores->desactivar($idarticulos_proveedores);
 		echo $rspta ? "Articulo por Proveedor Desactivado" : "Articulo por Proveedor no se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$articulos_proveedores->activar($idarticulos_proveedores);
 		echo $rspta ? "Articulo por Proveedor activado" : "Articulo por Proveedor no se puedo activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$articulos_proveedores->mostrar($idpersonas,$idarticulos);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'buscarDatos':
		$rspta=$articulos_proveedores->buscarDatos($idpersonas,$idarticulos);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$articulos_proveedores->listar($idpersonas);
 		//Vamos a declarar un array idarticulos_proveedores,proveedor,articulo,costo
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idarticulos_proveedores.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idarticulos_proveedores.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idarticulos_proveedores.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idarticulos_proveedores.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->articulo,
 				"2"=>$reg->NoIdentificacion,
 				"3"=>$reg->costo,
 				"4"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;
	case 'listarProdPorProveedor':
		$rspta=$articulos_proveedores->listarProdPorProveedor($idpersonas);
 		echo '<option value=' . "0" . '>' ." Selecione un Valor ". '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idarticulos . '>' .($reg->idarticulos)."#". $reg->nombre . '</option>';
				}
	break;
	
	
	
}
?>