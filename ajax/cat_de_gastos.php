<?php 
require_once "../modelos/Cat_de_gastos.php"; 

$cat_de_gastos=new Cat_de_gastos(); 




$idcat_de_gastos=isset($_REQUEST["idcat_de_gastos"])? limpiarCadena($_REQUEST["idcat_de_gastos"]):"";
$idgrupo_de_gastos=isset($_REQUEST["idgrupo_de_gastos"])? limpiarCadena($_REQUEST["idgrupo_de_gastos"]):"";
$nombre=isset($_REQUEST["nombre"])? limpiarCadena($_REQUEST["nombre"]):"";

$Opcion=isset($_REQUEST["Opcion"])? limpiarCadena($_REQUEST["Opcion"]):"";

switch ($_GET["op"]){
	case 'guardaryeditar':
		if (empty($idcat_de_gastos)){
			$rspta=$cat_de_gastos->insertar($nombre,$idgrupo_de_gastos);
			echo $rspta ? "Gasto registrado" : "Gasto no se pudo registrar";
		}
		else {
			$rspta=$cat_de_gastos->editar($idcat_de_gastos,$nombre,$idgrupo_de_gastos);
			echo $rspta ? "Gasto actualizado" : "Gasto no se pudo actualizar";
		}
	break;

	case 'desactivar':
		$rspta=$cat_de_gastos->desactivar($idcat_de_gastos);
 		echo $rspta ? "Gasto Desactivado" : "Gasto no se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$cat_de_gastos->activar($idcat_de_gastos);
 		echo $rspta ? "Gasto activado" : "Gasto no se puede activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$cat_de_gastos->mostrar($idcat_de_gastos);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$cat_de_gastos->listar($nombre,$Opcion);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idcat_de_gastos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idcat_de_gastos.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idcat_de_gastos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idcat_de_gastos.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->idcat_de_gastos,	
 				"2"=>$reg->gasto,
 				"3"=>$reg->grupo,
 				"4"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "select":
		
		$rspta = $cat_de_gastos->select($nombre);
		echo '<option>' . "Selecione gasto". '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idcat_de_gastos . '>' . $reg->nombre . '</option>';
				}
	break;
	case "selectporid":
		
		$rspta = $cat_de_gastos->selectporid($idcat_de_gastos);

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idcat_de_gastos . '>' . $reg->nombre . '</option>';
				}
	break;
}
?>