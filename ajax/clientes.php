<?php 
require_once "../modelos/Clientes.php"; 

$clientes=new Clientes();

$idpersonas=isset($_REQUEST["idpersonas"])? limpiarCadena($_REQUEST["idpersonas"]):"";
$idclientes=isset($_REQUEST["idclientes"])? limpiarCadena($_REQUEST["idclientes"]):"";
$nombre=isset($_REQUEST["nombre"])? limpiarCadena($_REQUEST["nombre"]):"";
$nombrecomercial=isset($_REQUEST["nombrecomercial"])? limpiarCadena($_REQUEST["nombrecomercial"]):"";
$rfc=isset($_REQUEST["rfc"])? limpiarCadena($_REQUEST["rfc"]):"";
$referencia=isset($_REQUEST["referencia"])? limpiarCadena($_REQUEST["referencia"]):"";
$telefono=isset($_REQUEST["telefono"])? limpiarCadena($_REQUEST["telefono"]):"";
$correo=isset($_REQUEST["correo"])? limpiarCadena($_REQUEST["correo"]):"";
$diascredito=isset($_REQUEST["diascredito"])? limpiarCadena($_REQUEST["diascredito"]):"";
$limitecredito=isset($_REQUEST["limitecredito"])? limpiarCadena($_REQUEST["limitecredito"]):"";
$descuento=isset($_REQUEST["descuento"])? limpiarCadena($_REQUEST["descuento"]):"";
$idvendedores=isset($_REQUEST["idvendedores"])? limpiarCadena($_REQUEST["idvendedores"]):"";
$Opcion=isset($_REQUEST["Opcion"])? limpiarCadena($_REQUEST["Opcion"]):"";


switch ($_GET["op"]){
	case 'guardaryeditar':
		if (empty($idpersonas)){
			$rspta=$clientes->insertar($nombre,$nombrecomercial,$rfc,$idvendedores,$referencia,$telefono,$correo,$diascredito,$limitecredito,$descuento); 
			
			echo $rspta ? "cliente registrado $rspta " : "cliente no se pudo registrar $rspta";
		}
		else {
			$rspta=$clientes->editar($idpersonas,$nombre,$nombrecomercial,$rfc,$idvendedores,$referencia,$telefono,$correo,$diascredito,$limitecredito,$descuento);
			echo $rspta ? "cliente actualizado $rspta" : "cliente no se pudo actualizar $rspta";
		}
	break;

	/*case 'eliminar':
		$rspta=$clientes->eliminar($idpersonas);
 		echo $rspta ? "cliente Eliminada" : "cliente no se puedo eliminar";
 		break;
	break;
	*/

	case 'desactivar':
		$rspta=$clientes->desactivar($idpersonas);
 		echo $rspta ? "cliente Desactivada" : "La cliente no se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$clientes->activar($idpersonas);
 		echo $rspta ? "cliente activada" : "La cliente no se puedo activar";
 		break;
	break;

	
	
	case 'verSaldoDeLaPersona':
		$rspta=$clientes->verSaldoDeLaPersona($idpersonas);
 		echo json_encode($rspta);
 		
	break;
	case 'verHistorialDeAdeudo':
		$rspta=$clientes->verHistorialDeAdeudo($idpersonas);
 		//echo '<option value=' . "0" . '>' ." Selecione un Valor ". '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->id . '>' ."Id : ".$reg->id." Fecha : ". $reg->fecha." Saldo : ".$reg->saldo." Dias : " .$reg->dias. '</option>';
					
				}
 		
	break;
	

	case 'mostrar':
		$rspta=$clientes->mostrar($idpersonas);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	

	case 'listar':
		$rspta=$clientes->listar($Opcion,$referencia);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){ 			
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idclientes.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idclientes.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idclientes.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idclientes.')"><i class="fa fa-check"></i></button>',

 				"1"=>substr($reg->nombre,0,30),
 				"2"=>substr($reg->nombrecomercial,0,30),
 				"3"=>$reg->rfc,
 				"4"=>$reg->telefono,
 				"5"=>$reg->condicion?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;
	
	case "select":
		
		$rspta = $clientes->select($Opcion,$referencia);
		echo '<option value=' . "0" . '>' ." Selecione un Valor ". '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idclientes . '>' .($reg->idclientes)." - ". $reg->nombre. '</option>';
				}
	break;
}
?>