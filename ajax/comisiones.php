<?php 
require_once "../modelos/Comisiones.php";

$comisiones=new Comisiones();

$idcomisiones=isset($_POST["idcomisiones"])? limpiarCadena($_POST["idcomisiones"]):"";
$idvendedores=isset($_POST["idvendedores"])? limpiarCadena($_POST["idvendedores"]):"";
$lim_inf=isset($_POST["lim_inf"])? limpiarCadena($_POST["lim_inf"]):"";
$lim_sup=isset($_POST["lim_sup"])? limpiarCadena($_POST["lim_sup"]):"";
$por_vendedor=isset($_POST["por_vendedor"])? limpiarCadena($_POST["por_vendedor"]):"";
$por_gerente=isset($_POST["por_gerente"])? limpiarCadena($_POST["por_gerente"]):"";

switch ($_GET["op"]){
	case 'guardaryeditar':
		if (empty($idcomisiones)){
			$rspta=$comisiones->insertar($idvendedores,$lim_inf,$lim_sup,$por_vendedor,$por_gerente);
			echo $rspta ? "Comision registrada" : "Comision no se pudo registrar $idvendedores,$lim_inf,$lim_sup,$por_vendedor,$por_gerente";
		}
		else {
			$rspta=$comisiones->editar($idcomisiones,$idvendedores,$lim_inf,$lim_sup,$por_vendedor,$por_gerente);
			echo $rspta ? "Comision actualizada" : "Comision no se pudo actualizar $idcomisiones,$idvendedores,$lim_inf,$lim_sup,$por_vendedor,$por_gerente";
		}
	break;

	case 'desactivar':
		$rspta=$comisiones->desactivar($idcomisiones);
 		echo $rspta ? "Comision Desactivada" : "Comision no se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$comisiones->activar($idcomisiones);
 		echo $rspta ? "Comision activada" : "Comision no se puede activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$comisiones->mostrar($idcomisiones);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$comisiones->listar();
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idcomisiones.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idcomisiones.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idcomisiones.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idcomisiones.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->vendedor,
 				"2"=>$reg->lim_inf,
 				"3"=>$reg->lim_sup,
 				"4"=>$reg->por_vendedor,
 				"5"=>$reg->por_gerente,
 				"6"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "select":
		require_once "../modelos/Comisiones.php";
		$comisiones = new comisiones();
		$rspta = $comisiones->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idcomisiones . '>' . $reg->nombre . '</option>';
				}
	break;
}
?>