<?php 
require_once "../modelos/Departamentos.php";

$departamentos=new Departamentos();

$iddepartamentos=isset($_POST["iddepartamentos"])? limpiarCadena($_POST["iddepartamentos"]):"";
$nombre=isset($_POST["nombre"])? limpiarCadena($_POST["nombre"]):"";


switch ($_GET["op"]){
	case 'guardaryeditar':
		if (empty($iddepartamentos)){
			$rspta=$departamentos->insertar($nombre);
			echo $rspta ? "Departamento registrada" : "Departamento no se pudo registrar";
		}
		else {
			$rspta=$departamentos->editar($iddepartamentos,$nombre);
			echo $rspta ? "Departamento actualizada" : "Departamento no se pudo actualizar";
		}
	break;

	case 'desactivar':
		$rspta=$departamentos->desactivar($iddepartamentos);
 		echo $rspta ? "Departamento Desactivada" : "Departamento no se puede desactivar"; 
 		break;
	break;

	case 'activar':
		$rspta=$departamentos->activar($iddepartamentos);
 		echo $rspta ? "Departamento activada" : "Departamento no se puede activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$departamentos->mostrar($iddepartamentos);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$departamentos->listar();
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->iddepartamentos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->iddepartamentos.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->iddepartamentos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->iddepartamentos.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->nombre,
 				"2"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "select":
		require_once "../modelos/Departamentos.php";
		$departamentos = new Departamentos();
		$rspta = $departamentos->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->iddepartamentos . '>' . $reg->nombre . '</option>';
				}
	break;
}
?>