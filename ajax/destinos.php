<?php 
require_once "../modelos/Destinos.php";

$destinos=new Destinos();



$iddestinos=isset($_REQUEST["iddestinos"])? limpiarCadena($_REQUEST["iddestinos"]):"";
$nombre=isset($_REQUEST["nombre"])? limpiarCadena($_REQUEST["nombre"]):"";
$Opcion=isset($_REQUEST["Opcion"])? limpiarCadena($_REQUEST["Opcion"]):"";

switch ($_GET["op"]){
	case 'guardaryeditar':
		if ($Opcion=='D')
			$d="Eliminado";
		if ($Opcion=='U')
			$d="Actualizado";
		if ($Opcion=='I')
			$d="Agregado";

		$rspta=$destinos->guardar($Opcion,$iddestinos,$nombre);
		echo $rspta ? "Dato ".$d : "Dato no ".$d;
		
	break;

	case 'desactivar':
		$rspta=$destinos->desactivar($iddestinos);
 		echo $rspta ? "Destino Desactivada" : "Destino no se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$destinos->activar($iddestinos);
 		echo $rspta ? "Destino activada" : "Destino no se puede activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$destinos->mostrar($iddestinos);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$destinos->listar($nombre);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button data-toggle="tooltip" title="Selecionar"  data-placement="right" class="btn btn-warning" onclick="mostrar('.$reg->iddestinos.')"><i class="fa fa-check"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->iddestinos.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->iddestinos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->iddestinos.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->nombre,
 				"2"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "select":
		
		$rspta = $destinos->select($nombre);

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->iddestinos . '>' . $reg->nombre . '</option>';
				}
	break;
	case "selectporid":
		
		$rspta = $destinos->selectporid($iddestinos);

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->iddestinos . '>' . $reg->nombre . '</option>';
				}
	break;
}
?>