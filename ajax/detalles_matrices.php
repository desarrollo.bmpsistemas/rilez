<?php 
require_once "../modelos/Detalles_matrices.php";  

$detalles_matrices=new Detalles_matrices();

$iddetalles_matrices=isset($_REQUEST["iddetalles_matrices"])? limpiarCadena($_REQUEST["iddetalles_matrices"]):"";
$idmatrices=isset($_REQUEST["idmatrices"])? limpiarCadena($_REQUEST["idmatrices"]):"";
$idmatriz=isset($_REQUEST["idmatriz"])? limpiarCadena($_REQUEST["idmatriz"]):"";
$idarticulos=isset($_REQUEST["idarticulos"])? limpiarCadena($_REQUEST["idarticulos"]):"";
$cantidad=isset($_REQUEST["cantidad"])? limpiarCadena($_REQUEST["cantidad"]):"";
$costo=isset($_REQUEST["costo"])? limpiarCadena($_REQUEST["costo"]):"";



switch ($_GET["op"]){
	case 'guardaryeditar':
		if (empty($iddetalles_matrices)){
			$rspta=$detalles_matrices->insertar($idmatrices,$idarticulos,$cantidad,$costo);
			echo $rspta ? "Articulos en matriz registgrado $iddetalles_matrices,$idmatrices,$idarticulos,$cantidad,$costo" : "Articulo en matriz no se pudo registrar ";
		}

		else {
			$rspta=$detalles_matrices->editar($iddetalles_matrices,$idarticulos,$cantidad,$costo);
			echo $rspta ? "Articulos en matriz actualizado " : "Articulo en matriz no se pudo actualizar ";
		}
	break;

	case 'desactivar':
		$rspta=$detalles_matrices->desactivar($iddetalles_matrices);
 		echo $rspta ? "Articulo de matriz eliminado " : "Articulo en matriz no se pudo eliminar";
 		
	break;

	case 'activar':
		$rspta=$detalles_matrices->activar($iddetalles_matrices);
 		echo $rspta ? "Articulo de matriz eliminado " : "Articulo en matriz no se pudo eliminar";
 		
	break;

	case 'mostrar':
		$rspta=$detalles_matrices->mostrar($iddetalles_matrices);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$detalles_matrices->listar($idmatrices);
 		//Vamos a declarar un array idarticulos_proveedores,proveedor,articulo,costo
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->iddetalles_matrices.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->iddetalles_matrices.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->iddetalles_matrices.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->iddetalles_matrices.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->codigo,
 				"2"=>$reg->nombre,
 				"3"=>$reg->costo,
 				"4"=>$reg->cantidad,
 				"5"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;
	
	
}
?>