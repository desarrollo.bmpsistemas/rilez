<?php 
require_once "../modelos/Detalles_monedas.php";

$detalles_monedas=new Detalles_monedas();

$iddetalles_monedas=isset($_REQUEST["iddetalles_monedas"])? limpiarCadena($_REQUEST["iddetalles_monedas"]):"";
$valor=isset($_REQUEST["valor"])? limpiarCadena($_REQUEST["valor"]):"";
$fecha=isset($_REQUEST["fecha"])? limpiarCadena($_REQUEST["fecha"]):"";
$idusuarios=isset($_REQUEST["idusuarios"])? limpiarCadena($_REQUEST["idusuarios"]):"";


switch ($_GET["op"]){
	case 'guardaryeditar':
		if (empty($iddetalles_monedas)){
			$rspta=$detalles_monedas->insertar($idusuarios,$valor);
			echo $rspta ? "Detalle Moneda registrada" : "Detalle Moneda no se pudo registrar";
		}
		else {
			$rspta=$detalles_monedas->editar($iddetalles_monedas,$idusuarios,$valor);
			echo $rspta ? "Detalle Moneda actualizada" : "Detalle Moneda no se pudo actualizar";
		}
	break;

	case 'desactivar':
		//$rspta=$detalles_monedas->desactivar($iddetalles_monedas);
 		//echo $rspta ? "Detalle Moneda Desactivada" : "Detalle Moneda no se puede desactivar";
 		break;

	case 'activar':
		//$rspta=$detalles_monedas->activar($iddetalles_monedas);
 		//echo $rspta ? "Detalle Moneda activada" : "Detalle Moneda no se puede activar";
 		break;

	case 'mostrar':
		//$rspta=$detalles_monedas->mostrar($iddetalles_monedas);
 		//Codificar el resultado utilizando json
 		//echo json_encode($rspta);
 		break;


	case 'listar':
		$rspta=$detalles_monedas->listar($fecha);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->iddetalles_monedas.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->iddetalles_monedas.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->iddetalles_monedas.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->iddetalles_monedas.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->nombre,
 				"2"=>$reg->fecha,
 				"3"=>$reg->valor,
 				"4"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

}
?>