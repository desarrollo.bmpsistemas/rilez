<?php 
require_once "../modelos/Personas.php";

$personas=new Personas();

$idpersonas=isset($_POST["idpersonas"])? limpiarCadena($_POST["idpersonas"]):"";
$nombre=isset($_POST["nombre"])? limpiarCadena($_POST["nombre"]):"";
$nombrecomercial=isset($_POST["nombrecomercial"])? limpiarCadena($_POST["nombrecomercial"]):"";
$rfc=isset($_POST["rfc"])? limpiarCadena($_POST["rfc"]):"";
$idmunicipios=isset($_POST["idmunicipios"])? limpiarCadena($_POST["idmunicipios"]):"";
$idcolonias=isset($_POST["idcolonias"])? limpiarCadena($_POST["idcolonias"]):"";
$idcalles=isset($_POST["idcalles"])? limpiarCadena($_POST["idcalles"]):"";
$numero_exterior=isset($_POST["numero_exterior"])? limpiarCadena($_POST["numero_exterior"]):"";
$numero_interior=isset($_POST["numero_interior"])? limpiarCadena($_POST["numero_interior"]):"";
$referencia=isset($_POST["referencia"])? limpiarCadena($_POST["referencia"]):"";
$codigo_postal=isset($_POST["codigo_postal"])? limpiarCadena($_POST["codigo_postal"]):"";
$telefono=isset($_POST["telefono"])? limpiarCadena($_POST["telefono"]):"";
$correo=isset($_POST["correo"])? limpiarCadena($_POST["correo"]):"";
$tipodepersona=isset($_POST["tipodepersona"])? limpiarCadena($_POST["tipodepersona"]):"";
$diascredito=isset($_POST["diascredito"])? limpiarCadena($_POST["diascredito"]):"";
$limitecredito=isset($_POST["limitecredito"])? limpiarCadena($_POST["limitecredito"]):"";
$idvendedores=isset($_POST["idvendedores"])? limpiarCadena($_POST["idvendedores"]):"";

switch ($_GET["op"]){
	case 'guardaryeditar':
		if (empty($idpersonas)){
			$rspta=$personas->insertar($nombre,$nombrecomercial,$rfc,$idmunicipios,$idcolonias,$idcalles,$idvendedores,$numero_exterior,$numero_interior,$referencia,$codigo_postal,$telefono,$correo,$diascredito,$limitecredito,$tipodepersona); 
			
			echo $rspta ? "Persona registrada" : "Persona no se pudo registrar $nombre,$nombrecomercial,$rfc,$idmunicipios,$idcolonias,$idcalles,$idvendedores,$numero_exterior,$numero_interior,$referencia,$codigo_postal,$telefono,$correo,$diascredito,$limitecredito,$tipodepersona";
		}
		else {
			$rspta=$personas->editar($idpersonas,$nombre,$nombrecomercial,$rfc,$idmunicipios,$idcolonias,$idcalles,$idvendedores,$numero_exterior,$numero_interior,$referencia,$codigo_postal,$telefono,$correo,$diascredito,$limitecredito,$tipodepersona);
			echo $rspta ? "Persona actualizada" : "Persona no se pudo actualizar";
		}
	break;

	/*case 'eliminar':
		$rspta=$personas->eliminar($idpersonas);
 		echo $rspta ? "Persona Eliminada" : "Persona no se puedo eliminar";
 		break;
	break;
	*/

	case 'desactivar':
		$rspta=$personas->desactivar($idpersonas);
 		echo $rspta ? "Persona Desactivada" : "La persona no se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$personas->activar($idpersonas);
 		echo $rspta ? "Persona activada" : "La persona no se puedo activar";
 		break;
	break;

	

	case 'mostrar':
		$rspta=$personas->mostrar($idpersonas);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listarp':
		$rspta=$personas->listarp();
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idpersonas.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idpersonas.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idpersonas.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idpersonas.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->nombre,
 				"2"=>$reg->nombrecomercial,
 				"3"=>$reg->rfc,
 				"4"=>$reg->telefono,
 				"5"=>$reg->condicion?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case 'listarc':
		$rspta=$personas->listarc();
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idpersonas.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idpersonas.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idpersonas.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idpersonas.')"><i class="fa fa-check"></i></button>',

 				"1"=>$reg->nombre,
 				"2"=>$reg->nombrecomercial,
 				"3"=>$reg->rfc,
 				"4"=>$reg->telefono,
 				"5"=>$reg->condicion?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;
}
?>