<?php 
require_once "../modelos/Ent_sal.php";  

$ent_sal=new Ent_sal();

$folio=isset($_REQUEST["folio"])? limpiarCadena($_REQUEST["folio"]):"";
$tipodeprecio=isset($_REQUEST["tipodeprecio"])? limpiarCadena($_REQUEST["tipodeprecio"]):"";
$ident_sal=isset($_REQUEST["ident_sal"])? limpiarCadena($_REQUEST["ident_sal"]):"";
$idarticulos=isset($_REQUEST["idarticulos"])? limpiarCadena($_REQUEST["idarticulos"]):"";
//$idusuarios=isset($_REQUEST["idcapturistas"])? limpiarCadena($_REQUEST["idcapturistas"]):"";
$idusuarios=isset($_REQUEST["idusuarios"])? limpiarCadena($_REQUEST["idusuarios"]):"";
$idpersonas=isset($_REQUEST["idpersonas"])? limpiarCadena($_REQUEST["idpersonas"]):"";
$idpersonas2=isset($_REQUEST["idpersonas2"])? limpiarCadena($_REQUEST["idpersonas2"]):"";
$idalmacenes=isset($_REQUEST["idalmacenes"])? limpiarCadena($_REQUEST["idalmacenes"]):"";
$idalmacenes2=isset($_REQUEST["idalmacenes2"])? limpiarCadena($_REQUEST["idalmacenes2"]):"";

$valor=isset($_REQUEST["valor"])? limpiarCadena($_REQUEST["valor"]):"";
$descuento=isset($_REQUEST["descuento"])? limpiarCadena($_REQUEST["descuento"]):"";
$cantidad=isset($_REQUEST["cantidad"])? limpiarCadena($_REQUEST["cantidad"]):"";
$ivaaplicado=isset($_REQUEST["ivaaplicado"])? limpiarCadena($_REQUEST["ivaaplicado"]):"";
$iva=isset($_REQUEST["iva"])? limpiarCadena($_REQUEST["iva"]):"";

$idmonedas=isset($_REQUEST["idmonedas"])? limpiarCadena($_REQUEST["idmonedas"]):"";
$fecha=isset($_REQUEST["fecha"])? limpiarCadena($_REQUEST["fecha"]):"";
$vencimiento=isset($_REQUEST["vencimiento"])? limpiarCadena($_REQUEST["vencimiento"]):"";


$fecha_inicial=isset($_REQUEST["fecha_inicial"])? limpiarCadena($_REQUEST["fecha_inicial"]):"";
$fecha_final=isset($_REQUEST["fecha_final"])? limpiarCadena($_REQUEST["fecha_final"]):"";
$imagen=isset($_REQUEST["imagen"])? limpiarCadena($_REQUEST["imagen"]):"";
$imagenactual=isset($_REQUEST["imagenactual"])? limpiarCadena($_REQUEST["imagenactual"]):"";
$imagenmuestra=isset($_REQUEST["imagenmuestra"])? limpiarCadena($_REQUEST["imagenmuestra"]):"";

$referencia=isset($_REQUEST["referencia"])? limpiarCadena($_REQUEST["referencia"]):"";
$observaciones=isset($_REQUEST["observaciones"])? limpiarCadena($_REQUEST["observaciones"]):"";

$Subtotal=isset($_REQUEST["Subtotal"])? limpiarCadena($_REQUEST["Subtotal"]):"";
$Iva=isset($_REQUEST["Iva"])? limpiarCadena($_REQUEST["Iva"]):"";
$Total=isset($_REQUEST["Total"])? limpiarCadena($_REQUEST["Total"]):"";

$iddet_ent_sal=isset($_REQUEST["iddet_ent_sal"])? limpiarCadena($_REQUEST["iddet_ent_sal"]):"";
$idtipo_movs=isset($_REQUEST["idtipo_movs"])? limpiarCadena($_REQUEST["idtipo_movs"]):"";
$tipo_mov=isset($_REQUEST["tipo_mov"])? limpiarCadena($_REQUEST["tipo_mov"]):"";
$concepto=isset($_REQUEST["concepto"])? limpiarCadena($_REQUEST["concepto"]):"";

$cliente=isset($_REQUEST["cliente"])? limpiarCadena($_REQUEST["cliente"]):"";
$conatencion=isset($_REQUEST["conatencion"])? limpiarCadena($_REQUEST["conatencion"]):"";
$asunto=isset($_REQUEST["asunto"])? limpiarCadena($_REQUEST["asunto"]):"";

$totaldocumento=isset($_REQUEST["totaldocumento"])? limpiarCadena($_REQUEST["totaldocumento"]):"";
$nota=isset($_REQUEST["nota"])? limpiarCadena($_REQUEST["nota"]):"";
$idformadepago=isset($_REQUEST["idformadepago"])? limpiarCadena($_REQUEST["idformadepago"]):"";



switch ($_GET["op"]){
	case 'guardaryeditarAnexo':

		if (!file_exists($_FILES['imagen']['tmp_name']) || !is_uploaded_file($_FILES['imagen']['tmp_name']))
		{
			$imagen=$_REQUEST["imagenactual"];
			
		}
		else 
		{
			$ext = explode(".", $_FILES["imagen"]["name"]);
			if ($_FILES['imagen']['type'] == "image/jpg" || $_FILES['imagen']['type'] == "image/jpeg" || $_FILES['imagen']['type'] == "image/png")
			{
				$imagen = $folio . '.' . end($ext); 
				
				move_uploaded_file($_FILES["imagen"]["tmp_name"], "../files/cotizaciones/" . $imagen);
			}
		}
		$rspta=$ent_sal->Anexo($folio,$cliente,$conatencion,$asunto,$imagen);
		echo $rspta ? "Anexo Procesado " : "Anexo no se pudo Procesar $folio,$cliente,$conatencion,$asunto,$imagen";
		
	break;
	case 'buscarAnexo':
		$rspta=$ent_sal->buscarAnexo($ident_sal);
 		echo json_encode($rspta);
 		break;
	break;

	case 'guardaryeditar':
		//$iva=0;
		if (strlen($concepto)==0) $concepto=" ";
		if ($idmonedas>1)
		{
			$rspta=$ent_sal->valorMoneda();
			while ($reg=$rspta->fetch_object())
	 		$valor_moneda=$reg->valormoneda;
 		}
 		else
 			$valor_moneda=1;

		if (empty($iddet_ent_sal)){
			$rspta=$ent_sal->insertar($fecha,$idalmacenes,$idalmacenes2,$idtipo_movs,$idpersonas,$idpersonas2,$idusuarios,$idarticulos,$valor,$descuento,$idmonedas,$valor_moneda,$iva,$cantidad,$tipo_mov,$concepto);
			if ($rspta>0)
				echo "Movimiento registgrado No $rspta";
			else
				echo "Movimiento no registgrado  $rspta";

			
		}

		else 
		{	
			$rspta=$ent_sal->editar($fecha,$iddet_ent_sal,$idarticulos,$valor,$descuento,$idmonedas,$valor_moneda,$iva,$cantidad,$tipo_mov,$concepto);
			echo $rspta ? "Movimiento actualizado ($rspta) $iddet_ent_sal,$idarticulos,$valor,$iva,$cantidad,$tipo_mov,$concepto " : "Movimiento no se pudo actualizar $iddet_ent_sal,$idarticulos,$valor,$iva,$cantidad,$tipo_mov,$concepto";
		}
	break;

	case 'desactivar':
		$rspta=$ent_sal->desactivar($iddet_ent_sal,$tipo_mov);
 		echo $rspta ? "Movimiento eliminado " : "Movimento no se pudo eliminar ";
 		break;
	break;

	case 'activar':
		$rspta=$ent_sal->activar($iddet_ent_sal);
 		echo $rspta ? "Articulo por Proveedor activado" : "Articulo por Proveedor no se puedo activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$ent_sal->mostrar($iddet_ent_sal,$tipo_mov);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		
	break;
	case 'aplicarDescuento':
		$rspta=$ent_sal->aplicarDescuento($iddet_ent_sal,$descuento,$tipo_mov);
 		//Codificar el resultado utilizando json
 		echo $rspta ? "Descuento actualizado $rspta" : "Descuento  no se puedo actualizar $rspta";
 		
	break;
	
	case 'actualizarCantidad':
		$rspta=$ent_sal->actualizarCantidad($iddet_ent_sal,$cantidad,$valor,$tipo_mov);
 		echo $rspta ? "Movimiento actualizado " : "Movimiento  no se puedo actualizar ";
 		
	break;

	

	
	case 'traerIva_y_Moneda':
		$rspta=$ent_sal->traerIva_y_Moneda($idalmacenes);
 		echo json_encode($rspta);
 		break;
	break;

	case 'procesarMoneda': 
		$rspta=$ent_sal->procesarMoneda($idmonedas,$idalmacenes,$idtipo_movs,$idpersonas,$idusuarios,$tipo_mov);
 		echo $rspta ? "Moneda procesada " : "Moneda no procesada ";
 		
	break;
	
	case 'procesarIva':
	 
		$rspta=$ent_sal->procesarIva($iva,$idalmacenes,$idtipo_movs,$idpersonas,$idusuarios,$tipo_mov);
 		echo $rspta ? "Iva procesado  " : "Iva no procesado ";
 		
	break;

	case 'calcularEntSal':
		$rspta=$ent_sal->calcularEntSal($idalmacenes,$idtipo_movs,$idpersonas,$idusuarios,$tipo_mov);
 		echo json_encode($rspta);
 		
	break;
	case 'cancelarCaptura':
		$rspta=$ent_sal->cancelarCaptura($idalmacenes,$idtipo_movs,$idpersonas,$idusuarios,$tipo_mov);
 		echo $rspta ? "Movimiento eliminado " : "Movimento no se pudo eliminar ";
 		
	break;

    case 'detallesEntSal':
    	$rspta=$ent_sal->detallesEntSal($idalmacenes,$idtipo_movs,$idpersonas,$idusuarios);
		echo json_encode($rspta);
    break;

	case 'terminarEntSal':
		$valor_de_moneda=1;
		//$idmonedas=1;
		//$ivaaplicado=0.16;
		if ($idmonedas>1)
		{
			$rspta=$ent_sal->valorMoneda();

			while ($reg=$rspta->fetch_object())
	 		$valor_de_moneda=$reg->valormoneda;
		}


		$ident_sal=0;			
		//insertar entrada o salida
		$mov=0; 
		$rspta="sin folio";                            
		$ident_sal=$ent_sal->insertarEntSal($idalmacenes,$idalmacenes2,$idtipo_movs,$idpersonas,$idusuarios,$fecha,$vencimiento,$idmonedas,$valor_de_moneda,$idformadepago,$Subtotal,$Iva,$Total,$ivaaplicado,$referencia,$tipo_mov,$observaciones);
		//return "$ident_sal";
		//if ($idtipo_movs==8)
			//$ident_sal=$ent_sal->insertarEntSal($idalmacenes2,$idalmacenes,17,$idpersonas,$idusuarios,$fecha,$vencimiento,$idmonedas,$valor_de_moneda,$idformadepago,$Subtotal,$Iva,$Total,$ivaaplicado,$referencia,$tipo_mov,$observaciones);

		if ($ident_sal>0)
		{	
			//buscar detalles
			//$rspta=" con folio ";
			$rspta=$ent_sal->detallesEntSal($idalmacenes,$idtipo_movs,$idpersonas,$idusuarios,$tipo_mov);
			$contador=0;
			$mov=0;
			while ($reg=$rspta->fetch_object())
			{
				$mov++;
				$idarticulos=$reg->idarticulos;	
				$valor=$reg->valor;
				$descuento=$reg->descuento;
				$cantidad=$reg->cantidad;
				$iva=$reg->iva;
				$condicion=$reg->condicion;
				$concepto=$reg->concepto;
				$rdo=$ent_sal->insertarDetEntSal($ident_sal,$mov,$idarticulos,$valor,$descuento,$iva,$cantidad,$condicion,$concepto,$tipo_mov);
				//if ($idtipo_movs==8) // para ingresar los movimientos como entradas cuando es por traspaso
				//echo "insertando detalles ";

				
				$contador=$contador+$rdo;//.=$rsptadet;
			}	
			
			//echo "mov registrados "+$contador;
			
		}
		$rspta=$ent_sal->detallesEntSalInsertados($ident_sal,$tipo_mov);
		$registros=0;
		while ($reg=$rspta->fetch_object())
		$registros=$reg->registros;	
					
 		if  ($registros>0 && $ident_sal>0)
 		{
 			//si es un traspaso generar la entrada y luego eliminar los movimientos
 			if ($idtipo_movs==8)
 			{
 				$rspta=$ent_sal->insertaTraspaso($ident_sal,$idalmacenes2,$idalmacenes,$idpersonas2);
 			}

 			//eliminar temporal una vez terminado salida y entrada
 			$rspta=$ent_sal->eliminarTmpEntSal($idalmacenes,$idtipo_movs,$idpersonas,$idusuarios,$tipo_mov);
 			echo $ident_sal;

 		}
 		else
 		{
 			echo "Sin registro de detalles... ";	
 		}
 		
	break;
	
	

	case 'listar':
		$rspta=$ent_sal->listar($tipo_mov,$idalmacenes,$idtipo_movs,$idpersonas,$idusuarios);
 		//Vamos a declarar un array idarticulos_proveedores,proveedor,articulo,costo
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>'<button class="btn btn-warning"  aria-hidden="true" data-placement="left" title="Editar cantidad y precio" onclick="mostrar('.$reg->iddet_ent_sal.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger"  aria-hidden="true" data-placement="left" title="Eliminar" onclick="desactivar('.$reg->iddet_ent_sal.')"><i class="fa fa-close"></i></button> '.
 					'<button class="btn btn-warning" aria-hidden="true" data-placement="left" title="Aplicar descuento, al final de click a boton ver"  onclick="aplicarDescuento('.$reg->iddet_ent_sal.')"><i class="fa fa-check-circle"></i></button>',
 				"1"=>$reg->codigo,
 				"2"=>$reg->descripcion.$reg->concepto,
 				"3"=>number_format($reg->valor,2),
 				"4"=>number_format($reg->descuento,2),
 				"5"=>$reg->moneda,
 				"6"=>number_format($reg->iva,2),
 				"7"=>$reg->cantidad,
 				"8"=>number_format($reg->importe,2)
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;
	case "buscarEntSal":										
		$rspta=$ent_sal->buscarEntSal($idalmacenes,$idtipo_movs,$idpersonas,$fecha_inicial,$fecha_final,$tipo_mov);
		echo '<option value="0">Seleccione Pedido</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->ident_sal . '>' ." ( ".$reg->ident_sal." ) - ".$reg->persona ." -> " .$reg->almacen ." - ".$reg->fecha ." ( $ ".$reg->total ." ) ". '</option>';
				}
	break;

	case "tomarEntSal":
	
		$rspta=$ent_sal->tomarEntSal($ident_sal,$idalmacenes,$idtipo_movs,$idpersonas,$idusuarios,$tipo_mov,$tipodeprecio);
		//echo $ident_sal ; //$rspta;
		echo $rspta;
	break;
	
	case 'buscarProducto':
		$rspta=$ent_sal->buscarProducto($idarticulos,$idalmacenes);
 		echo json_encode($rspta);
	break;
	
	case "selecte":
		
		$rspta = $ent_sal->selecte();
		echo '<option value=' . "0" . '>' ." Selecione un Valor ". '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idtipo_movs . '>' .$reg->nombre. '</option>';
				}
	break;
	
	case "selecionarIva":
		
		$rspta = $ent_sal->selecionarIva($idalmacenes);
		//echo '<option value=' . "0" . '>' ." Selecione un Valor ". '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idivas . '>' .$reg->nombre. '</option>';
				}
	break;

	case "selects":
		
		$rspta = $ent_sal->selects();
		echo '<option value=' . "0" . '>' ." Selecione un Valor ". '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idtipo_movs . '>' .$reg->nombre. '</option>';
				}
	break;
	case 'actualizar_almacen_destino':
		$rspta=$ent_sal->actualizar_almacen_destino($folio,$idalmacenes);
 		echo $rspta ? "Almacen Destino Establecido" : "Almacen destino no se pudo establecer";
 		break;
	break;
	

}
?>