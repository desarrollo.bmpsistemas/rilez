<?php 
require_once "../modelos/Estados.php";

$estados=new Estados();

$idestados=isset($_REQUEST["idestados"])? limpiarCadena($_REQUEST["idestados"]):"";
$idpaises=isset($_REQUEST["idpaises"])? limpiarCadena($_REQUEST["idpaises"]):"";
$nombre=isset($_REQUEST["nombre"])? limpiarCadena($_REQUEST["nombre"]):"";
//echo "valores $idpaises  op trae ".$_REQUEST["op"];
switch ($_REQUEST["op"]){
	case 'guardaryeditar':
	
		if (empty($idestados)){
			$rspta=$estados->insertar($idpaises,$nombre);
			echo $rspta ? "Estado registrado" : "Estado no se pudo registrar";
		}
		else {
			$rspta=$estados->editar($idestados,$idpaises,$nombre);
			echo $rspta ? "Estado actualizado" : "Estado no se pudo actualizar";
		}
	break;

	case 'desactivar':
		$rspta=$estados->desactivar($idestados);
 		echo $rspta ? "Estado Desactivado" : "Estado no se puede desactivar";
	break;

	case 'activar':
		$rspta=$estados->activar($idestados);
 		echo $rspta ? "Estado activado" : "Estado no se puede activar";
	break;

	case 'mostrar':
		$rspta=$estados->mostrar($idestados);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
	break;

	case 'listar':
		
		$rspta=$estados->listar();
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idestados.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idestados.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idestados.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idestados.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->pais,
 				"2"=>$reg->estado,
 				"3"=>$reg->Estado,
 				"4"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case 'listarestados':
		
		$rspta=$estados->listarestados();
		
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idestados.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idestados.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idestados.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idestados.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->pais,
 				"2"=>$reg->estado,
 				"3"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	

	case "selectpaises":
		require_once "../modelos/Estados.php";
		$paises = new Estados();

		$rspta = $paises->selectpaises();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option id="idpaises" name="idpaises" value=' . $reg->idpaises . '>' . $reg->nombre . '</option>';
				}
	break;
	case "selectestados":
		

		$rspta = $estados->listar($idpaises);
		echo '<option>' . "Selecione Estado". '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idestados . '>' . $reg->estado . '</option>';
				}


	break;
	case "selectMunicipios":
		require_once "../modelos/Municipios.php";
		$municipios = new Municipios();

		$rspta = $municipios->selectEstados();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idmunicipios . '>' . $reg->nombre . '</option>';
				}
	break;
	case "select":
		

		$rspta = $estados->select();
		echo '<option>' . "Selecione Estado". '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idestados . '>' . $reg->nombre . '</option>';
				}


	break;
}
?>