<?php 
require_once "../modelos/Estatus.php";

$estatus=new Estatus();

$idestatus=isset($_POST["idestatus"])? limpiarCadena($_POST["idestatus"]):"";
$nombre=isset($_POST["nombre"])? limpiarCadena($_POST["nombre"]):"";


switch ($_GET["op"]){
	case 'guardaryeditar':
		if (empty($idestatus)){
			$rspta=$estatus->insertar($nombre);
			echo $rspta ? "Estatus registrada" : "Estatus no se pudo registrar";
		}
		else {
			$rspta=$estatus->editar($idestatus,$nombre);
			echo $rspta ? "Estatus actualizada" : "Estatus no se pudo actualizar";
		}
	break;

	case 'desactivar':
		$rspta=$estatus->desactivar($idestatus);
 		echo $rspta ? "Estatus Desactivada" : "Estatus no se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$estatus->activar($idestatus);
 		echo $rspta ? "Estatus activada" : "Estatus no se puede activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$estatus->mostrar($idestatus);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$estatus->listar();
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idestatus.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idestatus.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idestatus.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idestatus.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->nombre,
 				"2"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "select":
		
		$rspta = $estatus->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idestatus . '>' . $reg->nombre . '</option>';
				}
	break;
}
?>