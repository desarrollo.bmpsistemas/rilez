<?php 
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) 
{
  header("location: ../vistas/login.html");
}
require_once "../modelos/Gastos.php"; 

$gastos=new Gastos(); 



$idgastos=isset($_REQUEST["idgastos"])? limpiarCadena($_REQUEST["idgastos"]):"";
$idpagos=isset($_REQUEST["idpagos"])? limpiarCadena($_REQUEST["idpagos"]):"";
$referencia=isset($_REQUEST["referencia"])? limpiarCadena($_REQUEST["referencia"]):"";
$folio=isset($_REQUEST["folio"])? limpiarCadena($_REQUEST["folio"]):"";
$cat_de_gastos_id=isset($_REQUEST["idcat_de_gastos"])? limpiarCadena($_REQUEST["idcat_de_gastos"]):"";
$almacen_id=isset($_REQUEST["idalmacenes"])? limpiarCadena($_REQUEST["idalmacenes"]):"";
$concepto=isset($_REQUEST["concepto"])? limpiarCadena($_REQUEST["concepto"]):"";
$fecha=isset($_REQUEST["fecha"])? limpiarCadena($_REQUEST["fecha"]):"";
$fecha_inicial=isset($_REQUEST["fecha_inicial"])? limpiarCadena($_REQUEST["fecha_inicial"]):"";
$fecha_final=isset($_REQUEST["fecha_final"])? limpiarCadena($_REQUEST["fecha_final"]):"";
$importe=isset($_REQUEST["importe"])? limpiarCadena($_REQUEST["importe"]):"";
$iva=isset($_REQUEST["iva"])? limpiarCadena($_REQUEST["iva"]):"";
$ajuste=isset($_REQUEST["ajuste"])? limpiarCadena($_REQUEST["ajuste"]):"";
$subtotal=isset($_REQUEST["subtotal"])? limpiarCadena($_REQUEST["subtotal"]):"";
$isrretenido=isset($_REQUEST["isrretenido"])? limpiarCadena($_REQUEST["isrretenido"]):"";
$ivaretenido=isset($_REQUEST["ivaretenido"])? limpiarCadena($_REQUEST["ivaretenido"]):"";
$total=isset($_REQUEST["total"])? limpiarCadena($_REQUEST["total"]):"";
$moneda=isset($_REQUEST["moneda"])? limpiarCadena($_REQUEST["moneda"]):"";
$valormoneda=isset($_REQUEST["valormoneda"])? limpiarCadena($_REQUEST["valormoneda"]):"";
$proveedor_id=isset($_REQUEST["proveedor_id"])? limpiarCadena($_REQUEST["proveedor_id"]):"";
$tasaocuota=isset($_REQUEST["tasaocuota"])? limpiarCadena($_REQUEST["tasaocuota"]):"";
$uuid=isset($_REQUEST["uuid"])? limpiarCadena($_REQUEST["uuid"]):"";
$capturista_id=isset($_REQUEST["idusuarios"])? limpiarCadena($_REQUEST["idusuarios"]):"";
$idformasdepagos=isset($_REQUEST["idformasdepagos"])? limpiarCadena($_REQUEST["idformasdepagos"]):"";
$sat_clave_forma_de_pago=isset($_REQUEST["SatClaveFormaDePago"])? limpiarCadena($_REQUEST["SatClaveFormaDePago"]):"";
$sat_nombre_forma_de_pago=isset($_REQUEST["SatNombreFormaDePago"])? limpiarCadena($_REQUEST["SatNombreFormaDePago"]):"";

$tipodegasto=isset($_REQUEST["tipodegasto"])? limpiarCadena($_REQUEST["tipodegasto"]):"";
$rfc=isset($_REQUEST["rfc"])? limpiarCadena($_REQUEST["rfc"]):"";
$nombre=isset($_REQUEST["nombre"])? limpiarCadena($_REQUEST["nombre"]):"";

$archivo=isset($_REQUEST["archivo"])? limpiarCadena($_REQUEST["archivo"]):"";
if (strlen($fecha_inicial)<10)
{
	$fecha_inicial=$fecha;
	$fecha_final=$fecha;
}

switch ($_GET["op"]){
	case 'enviarXml':

		if (!file_exists($_FILES['imagenXml']['tmp_name']) || !is_uploaded_file($_FILES['imagenXml']['tmp_name']))
		{
			$imagen=$_REQUEST["imagenactualXml"];
		}
		else 
		{
			$ext = explode(".", $_FILES["imagenXml"]["name"]);

			//if ($ext=='xml' || $ext=="XML" )
			{
				$imagen = $_FILES['imagenXml']['name'];// end($ext); //$nombre.end($ext); //
				move_uploaded_file($_FILES["imagenXml"]["tmp_name"], "../files/xmlgastos/" . $imagen);
			}
		}
		if (!file_exists($_FILES['imagenXml']['tmp_name']) || !is_uploaded_file($_FILES['imagenXml']['tmp_name']))
		{
			echo $_FILES['imagenXml']['name'];
		}
		else
			echo "";
		

	break;
	case 'enviarImagen':

		if (!file_exists($_FILES['imagen']['tmp_name']) || !is_uploaded_file($_FILES['imagen']['tmp_name']))
		{
			$imagen=$_REQUEST["imagenactual"];
		}
		else 
		{
			$ext = explode(".", $_FILES["imagen"]["name"]);

			//if ($ext=='xml' || $ext=="XML" )
			{
				$imagen = $_FILES['imagen']['name'];// end($ext); //$nombre.end($ext); //
				move_uploaded_file($_FILES["imagen"]["tmp_name"], "../files/pdfgastos/" . $imagen);
			}
		}
		if (!file_exists($_FILES['imagen']['tmp_name']) || !is_uploaded_file($_FILES['imagen']['tmp_name']))
		{
			echo $_FILES['imagen']['name'];
		}
		else
			echo "";
		

	break;

	case 'guardaryeditar':
		if (empty($idgastos)){
			$rspta=$gastos->insertar($cat_de_gastos_id,$almacen_id,$fecha,$folio,$referencia,$concepto,$importe,$iva,$ajuste,$subtotal,$isrretenido,$ivaretenido,$total,$moneda,$valormoneda,$proveedor_id,$tasaocuota,$uuid,$capturista_id,$idformasdepagos,$sat_clave_forma_de_pago,$sat_nombre_forma_de_pago,$rfc,$nombre);
			if ($rspta>0)
			   echo "$rspta";
			else
			{
			    echo "Gasto no se pudo registrar $rspta";
			   
			}
		}
		else {
			$rspta=$gastos->editar($idgastos,$cat_de_gastos_id,$almacen_id,$fecha,$folio,$referencia,$concepto,$importe,$iva,$ajuste,$subtotal,$isrretenido,$ivaretenido,$total,$moneda,$valormoneda,$proveedor_id,$tasaocuota,$uuid,$capturista_id,$idformasdepagos,$sat_clave_forma_de_pago,$sat_nombre_forma_de_pago,$rfc,$nombre);
			echo $rspta ? "Gasto actualizado" : "Gasto no se pudo actualizar";
		}
	break;

	case 'desactivar':
		$rspta=$gastos->desactivar($idgastos,$capturista_id);
 		echo $rspta ? "Gasto no eliminado" : "Gasto eliminado ";
 		break;
	break;

	case 'activar':
		$rspta=$gastos->activar($idgastos,$capturista_id);
 		echo $rspta ? "Gasto activada" : "Gasto no se puede activar";
 		break;
	break;
	case 'eliminarPago':
		$rspta=$gastos->eliminarPago($idpagos,$capturista_id);
 		echo $rspta ? "Pago de gasto eliminado" : "Pago de gasto no eliminado";
 		
	break;
	case 'leer_xml':
		$rspta=$gastos->leer_xml($archivo);
 		echo json_encode($rspta);
 		break;
	break;
	

	case 'mostrar':
		$rspta=$gastos->mostrar($idgastos);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$gastos->listar($idgastos,$fecha_inicial,$fecha_final,$almacen_id);
 		//Vamos a declarar un array
 		$data= Array();

 		//idgastos,gasto,fecha,gasto,importe,usuario,referencia,condicion
 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="imprimir('.$reg->idgastos.')"><i class="fa fa-print"></i></button>'.
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idgastos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idgastos.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idgastos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idgastos.')"><i class="fa fa-check"></i></button>',
 						
 				"1"=>$reg->idgastos,
 				"2"=>$reg->folio,
 				"3"=>$reg->referencia,
 				"4"=>$reg->fecha,
 				"5"=>$reg->concepto,
 				"6"=>$reg->total,
 				"7"=>$reg->abonos,
 				"8"=>$reg->proveedor,
 				"9"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;
	case 'listarPagos':
		$rspta=$gastos->listarPagos($idgastos);
 		//Vamos a declarar un array
 		$data= Array();

 		//opcion,idpagos,fecha,referencia,importe,condicion
 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>'<button class="btn btn-warning" onclick="imprimir('.$reg->idpagos.')"><i class="fa fa-print"></i></button>'.
 									' <button class="btn btn-danger" onclick="eliminarPago('.$reg->idpagos.')"><i class="fa fa-close"></i></button>',
 				"1"=>$reg->idpagos,
 				"2"=>$reg->fecha,
 				"3"=>$reg->referencia,
 				"4"=>$reg->importe,
 				"5"=>$reg->forma_de_pago,
 				"6"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "select":
		
		$rspta = $gastos->select($nombre);

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idgastos . '>' . $reg->nombre . '</option>';
				}
	break;
	case "selectporid":
		
		$rspta = $gastos->selectporid($idgastos);

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idgastos . '>' . $reg->nombre . '</option>';
				}
	break;
}
?>