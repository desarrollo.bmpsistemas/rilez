<?php 
require_once "../modelos/Grupo_de_gastos.php";

$grupo_de_gastos=new Grupo_de_gastos(); 



$idgrupo_de_gastos=isset($_REQUEST["idgrupo_de_gastos"])? limpiarCadena($_REQUEST["idgrupo_de_gastos"]):"";
$nombre=isset($_REQUEST["nombre"])? limpiarCadena($_REQUEST["nombre"]):"";


switch ($_GET["op"]){
	case 'guardaryeditar':
		if (empty($idgrupo_de_gastos)){
			$rspta=$grupo_de_gastos->insertar($nombre);
			echo $rspta ? "Gasto registrado" : "Gasto no se pudo registrar";
		}
		else {
			$rspta=$grupo_de_gastos->editar($idgrupo_de_gastos,$nombre);
			echo $rspta ? "Gasto actualizado" : "Gasto no se pudo actualizar";
		}
	break;

	case 'desactivar':
		$rspta=$grupo_de_gastos->desactivar($idgrupo_de_gastos);
 		echo $rspta ? "Gasto Desactivado" : "Gasto no se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$grupo_de_gastos->activar($idgrupo_de_gastos);
 		echo $rspta ? "Gasto activado" : "Gasto no se puede activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$grupo_de_gastos->mostrar($idgrupo_de_gastos);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$grupo_de_gastos->listar($nombre);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idgrupo_de_gastos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idgrupo_de_gastos.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idgrupo_de_gastos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idgrupo_de_gastos.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->nombre,
 				"2"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "select":
		
		$rspta = $grupo_de_gastos->select($nombre);
		echo '<option>' . "Selecione gasto". '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idgrupo_de_gastos . '>' . $reg->nombre . '</option>';
				}
	break;
	case "selectporid":
		
		$rspta = $grupo_de_gastos->selectporid($idgrupo_de_gastos);

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idgrupo_de_gastos . '>' . $reg->nombre . '</option>';
				}
	break;
}
?>