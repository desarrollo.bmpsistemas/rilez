<?php 
require_once "../modelos/Ivas.php";

$ivas=new Ivas();



$idivas=isset($_REQUEST["idivas"])? limpiarCadena($_REQUEST["idivas"]):"";
$nombre=isset($_REQUEST["nombre"])? limpiarCadena($_REQUEST["nombre"]):"";
$Opcion=isset($_REQUEST["Opcion"])? limpiarCadena($_REQUEST["Opcion"]):"";

switch ($_GET["op"]){
	case 'guardaryeditar':
		if ($Opcion=='D')
			$d="Eliminado";
		if ($Opcion=='U')
			$d="Actualizado";
		if ($Opcion=='I')
			$d="Agregado";

		$rspta=$ivas->guardar($Opcion,$idivas,$nombre);
		echo $rspta ? "Dato ".$d : "Dato no ".$d;
		
	break;

	case 'desactivar':
		$rspta=$ivas->desactivar($idivas);
 		echo $rspta ? "Iva Desactivada" : "Iva no se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$ivas->activar($idivas);
 		echo $rspta ? "Iva activada" : "Iva no se puede activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$ivas->mostrar($idivas);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$ivas->listar($nombre);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button data-toggle="tooltip" title="Selecionar"  data-placement="right" class="btn btn-warning" onclick="mostrar('.$reg->idivas.')"><i class="fa fa-check"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idivas.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idivas.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idivas.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->nombre,
 				"2"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "select":
		
		$rspta = $ivas->select($nombre);

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idivas . '>' . $reg->nombre . '</option>';
				}
	break;
	case "selectporid":
		
		$rspta = $ivas->selectporid($idivas);

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idivas . '>' . $reg->nombre . '</option>';
				}
	break;
}
?>