<?php 
require_once "../modelos/Lineas.php";

$lineas=new Lineas();

$idlineas=isset($_POST["idlineas"])? limpiarCadena($_POST["idlineas"]):"";
$nombre=isset($_POST["nombre"])? limpiarCadena($_POST["nombre"]):"";


switch ($_GET["op"]){
	case 'guardaryeditar':
		if (empty($idlineas)){
			$rspta=$lineas->insertar($nombre); 
			echo $rspta ? "Linea registrada" : "Linea no se pudo registrar";
		}
		else {
			$rspta=$lineas->editar($idlineas,$nombre);
			echo $rspta ? "Linea actualizada" : "Linea no se pudo actualizar";
		}
	break;

	case 'desactivar':
		$rspta=$lineas->desactivar($idlineas);
 		echo $rspta ? "Linea Desactivada" : "Linea no se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$lineas->activar($idlineas);
 		echo $rspta ? "Linea activada" : "Linea no se puede activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$lineas->mostrar($idlineas);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$lineas->listar();
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idlineas.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idlineas.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idlineas.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idlineas.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->nombre,
 				"2"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;
	case "select":
		
		$rspta = $lineas->select();
		echo '<option>' . "Selecione un valor" . '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idlineas . '>' . $reg->nombre . '</option>';
				}
	break;
}
?>