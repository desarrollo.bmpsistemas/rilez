<?php 
require_once "../modelos/Matrices.php"; 

$matrices=new Matrices();

$idmatrices=isset($_REQUEST["idmatrices"])? limpiarCadena($_REQUEST["idmatrices"]):"";
$idarticulos=isset($_REQUEST["idarticulos"])? limpiarCadena($_REQUEST["idarticulos"]):"";
$referencia=isset($_REQUEST["referencia"])? limpiarCadena($_REQUEST["referencia"]):"";


switch ($_GET["op"]){
	case 'guardaryeditar':
		if (empty($idmatrices)){
			$rspta=$matrices->insertar($idarticulos);
			echo $rspta ? "Matriz registrada" : "Matriz no se pudo registrar";
		}
		else {
			$rspta=$matrices->editar($idmatrices);
			echo $rspta ? "Matriz actualizada" : "Matriz no se pudo actualizar";
		}
	break;

	case 'desactivar':
		$rspta=$matrices->desactivar($idmatrices);
 		echo $rspta ? "Matriz Desactivada" : "Matriz no se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$matrices->activar($idmatrices);
 		echo $rspta ? "Matriz activada" : "Matriz no se puede activar";
 		break;
	break;

	case 'mostrar':
		//$rspta=$matrices->mostrar($idarticulos);
 		//Codificar el resultado utilizando json
 		//echo json_encode($rspta);
 		//break;
	break;

	case 'listar':
		$rspta=$matrices->listar($referencia);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idmatrices.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idmatrices.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idmatrices.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idmatrices.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->codigo,
 				"2"=>$reg->nombre,
 				"3"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "select":
		
		$rspta = $matrices->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idmatrices . '>' . $reg->codigo." ". $reg->nombre . '</option>';
				}
	break;
}
?>