<?php 
require_once "../modelos/Menu_modulos.php";   

$menu_modulos=new Menu_modulos();

$idmodulos=isset($_REQUEST["idmodulos"])? limpiarCadena($_REQUEST["idmodulos"]):"";
$idmenumodulos=isset($_REQUEST["idmenumodulos"])? limpiarCadena($_REQUEST["idmenumodulos"]):"";
$idmenus=isset($_REQUEST["idmenus"])? limpiarCadena($_REQUEST["idmenus"]):"";

switch ($_GET["op"]){
	case 'guardaryeditar':
	
		if (empty($idmenumodulos)){
			$rspta=$menu_modulos->insertar($idmenus,$idmodulos);
			echo $rspta ? "Modulo registrado" : "Modulo no se pudo registrar $idmenus,$idmodulos";
		}
		else {
			$rspta=$menu_modulos->editar($idmenus,$idmodulos);
			echo $rspta ? "Modulo actualizado" : "Modulo no se pudo actualizar $idmenus,$idmodulos";
		}
	break;

	case 'desactivar':
		$rspta=$menu_modulos->desactivar($idmenumodulos);
 		echo $rspta ? "Modulo Desactivado" : "Modulo no se puede desactivar";
	break;

	case 'activar':
		$rspta=$menu_modulos->activar($idmenumodulos);
 		echo $rspta ? "Modulo Desactivado" : "Modulo no se puede desactivar";
	break;

	case 'mostrar':
		$rspta=$menu_modulos->mostrar($idmodulos); 
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
	break;

	case 'listar':
		$rspta=$menu_modulos->listar($idmenus);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>'<button class="btn btn-warning" onclick="mostrar('.$reg->idmenumodulos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idmenumodulos.')"><i class="fa fa-close"></i></button>',
 				"1"=>$reg->idmenumodulos,	
 				"2"=>$reg->descripcion,
 				"3"=>$reg->modulo
 				
 			);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	
	case "select":
		$rspta = $menu_modulos->select(); 
		echo '<option value="0">seleccione un valor</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idmodulos . '>' . $reg->nombre . '</option>';
				}
	break;
	
	case "selectpormenu":
		$rspta = $menu_modulos->selectpormenu($idmenus);

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idmodulos . '>' . $reg->nombre . '</option>';
				}
	break;

	
}
?>