<?php 
require_once "../modelos/Menus.php";

$menus=new Menus();

$idmenus=isset($_POST["idmenus"])? limpiarCadena($_POST["idmenus"]):"";
$nombre=isset($_POST["nombre"])? limpiarCadena($_POST["nombre"]):"";
$orden=isset($_POST["orden"])? limpiarCadena($_POST["orden"]):"";

switch ($_GET["op"]){
	case 'guardaryeditar':
		if (empty($idmenus)){
			$rspta=$menus->insertar($nombre,$orden);
			echo $rspta ? "Menu registrada" : "Menu no se pudo registrar $nombre,$orden";
		}
		else {
			$rspta=$menus->editar($idmenus,$nombre,$orden);
			echo $rspta ? "Menu actualizada" : "Menu no se pudo actualizar $nombre,$orden";
		}
	break;

	case 'desactivar':
		$rspta=$menus->desactivar($idmenus);
 		echo $rspta ? "Menu Desactivado" : "Menu no se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$menus->activar($idmenus);
 		echo $rspta ? "Menu activado" : "Menu no se puede activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$menus->mostrar($idmenus);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$menus->listar();
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idmenus.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idmenus.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idmenus.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idmenus.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->idmenus,	
 				"2"=>$reg->nombre,
 				"3"=>$reg->orden,
 				"4"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "select":
		require_once "../modelos/Menus.php";
		$menus = new Menus();
		$rspta = $menus->select();
		echo '<option value="0">seleccione un valor</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idmenus . '>' . $reg->nombre . '</option>';
				}
	break;
}
?>