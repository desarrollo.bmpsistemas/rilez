<?php 
require_once "../modelos/Minimos_maximos.php";

$maximos_minimos=new Minimos_maximos();



$idmaximos_minimos=isset($_REQUEST["idmaximos_minimos"])? limpiarCadena($_REQUEST["idmaximos_minimos"]):"";
$nombre=isset($_REQUEST["nombre"])? limpiarCadena($_REQUEST["nombre"]):"";


switch ($_GET["op"]){
	case 'guardaryeditar':
		if (empty($idmaximos_minimos)){
			$rspta=$maximos_minimos->insertar($nombre);
			echo $rspta ? "Linea registrada" : "Linea no se pudo registrar";
		}
		else {
			$rspta=$maximos_minimos->editar($idmaximos_minimos,$nombre);
			echo $rspta ? "Linea actualizada" : "Linea no se pudo actualizar";
		}
	break;

	case 'desactivar':
		$rspta=$maximos_minimos->desactivar($idmaximos_minimos);
 		echo $rspta ? "Linea Desactivada" : "Linea no se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$maximos_minimos->activar($idmaximos_minimos);
 		echo $rspta ? "Linea activada" : "Linea no se puede activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$maximos_minimos->mostrar($idmaximos_minimos);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$maximos_minimos->listar($nombre);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idmaximos_minimos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idmaximos_minimos.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idmaximos_minimos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idmaximos_minimos.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->nombre,
 				"2"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "select":
		
		$rspta = $maximos_minimos->select($nombre);

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idmaximos_minimos . '>' . $reg->nombre . '</option>';
				}
	break;
	case "selectporid":
		
		$rspta = $maximos_minimos->selectporid($idmaximos_minimos);

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idmaximos_minimos . '>' . $reg->nombre . '</option>';
				}
	break;
}
?>