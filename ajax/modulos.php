<?php 
require_once "../modelos/Modulos.php"; 

$modulos=new Modulos();

$idmodulos=isset($_REQUEST["idmodulos"])? limpiarCadena($_REQUEST["idmodulos"]):"";
$descripcion=isset($_REQUEST["descripcion"])? limpiarCadena($_REQUEST["descripcion"]):"";
$nombre=isset($_REQUEST["nombre"])? limpiarCadena($_REQUEST["nombre"]):"";
$orden=isset($_REQUEST["orden"])? limpiarCadena($_REQUEST["orden"]):"";

switch ($_GET["op"]){
	case 'guardaryeditar':
		if (empty($idmodulos)){
			$rspta=$modulos->insertar($descripcion,$nombre,$orden);
			echo $rspta ? "Modulo registrado" : "Modulo no se pudo registrar $descripcion,$nombre,$orden";
		}
		else {
			$rspta=$modulos->editar($idmodulos,$descripcion,$nombre,$orden);
			echo $rspta ? "Modulo actualizada" : "Modulo no se pudo actualizar $idmodulos,$descripcion,$nombre,$orden";
		}
	break;

	case 'desactivar':
		$rspta=$modulos->desactivar($idmodulos);
 		echo $rspta ? "Modulo Desactivado" : "Modulo no se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$modulos->activar($idmodulos);
 		echo $rspta ? "Modulo activado" : "Modulo no se puede activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$modulos->mostrar($idmodulos);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$modulos->listar();
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idmodulos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idmodulos.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idmodulos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idmodulos.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->idmodulos,	
 				"2"=>$reg->descripcion,
 				"3"=>$reg->nombre,
 				"4"=>$reg->orden,
 				"5"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "select":
		
		$rspta = $modulos->select();
		echo '<option value="0">seleccione un valor</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idmodulos . '>' . $reg->nombre . '</option>';
				}
	break;
}
?>