<?php 
require_once "../modelos/Monedas.php";

$monedas=new Monedas();



$idmonedas=isset($_REQUEST["idmonedas"])? limpiarCadena($_REQUEST["idmonedas"]):"";
$abreviatura=isset($_REQUEST["abreviatura"])? limpiarCadena($_REQUEST["abreviatura"]):"";
$valor=isset($_REQUEST["valor"])? limpiarCadena($_REQUEST["valor"]):"";
$nombre=isset($_REQUEST["nombre"])? limpiarCadena($_REQUEST["nombre"]):"";


switch ($_GET["op"]){
	case 'guardaryeditar':
		if (empty($idmonedas)){
			$rspta=$monedas->insertar($nombre,$abreviatura,$valor);
			echo $rspta ? "Moneda registrada" : "Moneda no se pudo registrar";
		}
		else {
			$rspta=$monedas->editar($idmonedas,$nombre,$abreviatura,$valor);
			echo $rspta ? "Moneda actualizada" : "Moneda no se pudo actualizar";
		}
	break;

	case 'desactivar':
		$rspta=$monedas->desactivar($idmonedas);
 		echo $rspta ? "Moneda Desactivada" : "Moneda no se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$monedas->activar($idmonedas);
 		echo $rspta ? "Moneda activada" : "Moneda no se puede activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$monedas->mostrar($idmonedas);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$monedas->listar();
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idmonedas.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idmonedas.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idmonedas.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idmonedas.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->nombre,
 				"2"=>$reg->abreviatura,
 				"3"=>$reg->valor,
 				"4"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "select":
		
		$rspta = $monedas->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idmonedas . '>' . $reg->nombre . '</option>';
				}
	break;
	case "formasdepagos":
		
		$rspta = $monedas->formasdepagos();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idformasdepagos . '>' . $reg->nombre . '</option>';
				}
	break;
	case "selectporid":
		
		$rspta = $monedas->selectporid($idmonedas);

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idmonedas . '>' . $reg->nombre . '</option>';
				}
	break;
}
?>