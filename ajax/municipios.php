<?php 
require_once "../modelos/Municipios.php"; 

$municipios=new Municipios();

$idmunicipios=isset($_REQUEST["idmunicipios"])? limpiarCadena($_REQUEST["idmunicipios"]):"0";
$idestados=isset($_REQUEST["idestados"])? limpiarCadena($_REQUEST["idestados"]):"0";
$idpaises=isset($_REQUEST["idpaises"])? limpiarCadena($_REQUEST["idpaises"]):"0";
$nombre=isset($_REQUEST["nombre"])? limpiarCadena($_REQUEST["nombre"]):"";

switch ($_GET["op"]){
	case 'guardaryeditar':
	
		if (empty($idmunicipios)){
			$rspta=$municipios->insertar($idestados,$nombre);
			echo $rspta ? "Municipio registrado" : "Municipio no se pudo registrar";
		}
		else {
			$rspta=$municipios->editar($idmunicipios,$idestados,$nombre);
			echo $rspta ? "Municipio actualizado" : "Municipio no se pudo actualizar";
		}
	break;

	case 'desactivar':
		$rspta=$municipios->desactivar($idmunicipios);
 		echo $rspta ? "Municipio Desactivado" : "Municipio no se puede desactivar";
	break;

	case 'activar':
		$rspta=$municipios->activar($idmunicipios);
 		echo $rspta ? "Municipio activado" : "Municipio no se puede activar";
	break;

	case 'mostrar':
		$rspta=$municipios->mostrar($idmunicipios);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
	break;

	case 'listar':
		$rspta=$municipios->listar($nombre);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idmunicipios.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idmunicipios.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idmunicipios.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idmunicipios.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->pais,
 				"2"=>$reg->estado,
 				"3"=>$reg->municipio,
 				"4"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "selectmunicipios":
		

		$rspta = $municipios->selectmunicipios($idestados);

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idmunicipios . '>' . $reg->nombre . '</option>';
				}
	break;
	case "select":
		

		$rspta = $municipios->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idmunicipios . '>' . $reg->nombre . '</option>';
				}
	break;

	case "selectpaises":
		

		$rspta = $paises->selectpaises();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option id="idpaises" name="idpaises" value=' . $reg->idpaises . '>' . $reg->nombre . '</option>';
				}
	break;
	case "selectestados":
		
		$rspta = $estados->selectestados($idpaises);

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idestados . '>' . $reg->nombre . '</option>';
				}
	break;
	case "buscarMunicipios":
		

		$rspta = $estados->buscarMunicipios($nombre);

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idestados . '>' . $reg->nombre . '</option>';
				}
	break;
	case "selectporid":
		
		$rspta = $calles->selectporid($idcalles);

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idmunicipios . '>' . $reg->nombre . '</option>';
				}
	break;
	

	
}
?>