<?php 
require_once "../modelos/Origenes.php"; 

$origenes=new Origenes();

$idorigenes=isset($_REQUEST["idorigenes"])? limpiarCadena($_REQUEST["idorigenes"]):"";
$idcat_de_origenes=isset($_REQUEST["idcatdeorigenes"])? limpiarCadena($_REQUEST["idcatdeorigenes"]):"";

$idusuarios=isset($_REQUEST["idusuarios"])? limpiarCadena($_REQUEST["idusuarios"]):"";
$idalmacenes=isset($_REQUEST["idalmacenes"])? limpiarCadena($_REQUEST["idalmacenes"]):"";

$fecha=isset($_REQUEST["fecha"])? limpiarCadena($_REQUEST["fecha"]):"";
$importe=isset($_REQUEST["importe"])? limpiarCadena($_REQUEST["importe"]):"";
$nota=isset($_REQUEST["nota"])? limpiarCadena($_REQUEST["nota"]):"";
$Opcion=isset($_REQUEST["Opcion"])? limpiarCadena($_REQUEST["Opcion"]):"";
$referencia=isset($_REQUEST["referencia"])? limpiarCadena($_REQUEST["referencia"]):"";

switch ($_GET["op"]){
	case 'guardaryeditar':
		if (empty($idorigenes)){
			$rspta=$origenes->insertar($idalmacenes,$idusuarios,$idcat_de_origenes,$fecha,$importe,$nota); 
			echo $rspta ? "Origen de saldos registrado $rspta " : "Origen de saldo no se pudo registrar $rspta";
		}
		else {
			$rspta=$origenes->editar($idorigenes,$idalmacenes,$idusuarios,$idcat_de_origenes,$fecha,$importe,$nota);
			echo $rspta ? "origen de saldo actualizado $rspta" : "Origen de saldo a favor no se pudo actualizar $rspta";
		}
	break;

	case 'desactivar':
		$rspta=$origenes->desactivar($idorigenes);
 		echo $rspta ? "Saldo eliminado" : "Saldo no se pudo eliminar";
 		
	break;

	case 'activar':
		$rspta=$origenes->activar($idorigenes);
 		echo $rspta ? "Origen activado" : "Origen no se puedo activar";
 		
	break;

	

	case 'mostrar':
		$rspta=$origenes->mostrar($idorigenes);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	

	case 'listar':
		$rspta=$origenes->listar($fecha);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){ 			
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning"  title="De click aqui para editar datos "  onclick="mostrar('.$reg->idorigenes.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger"  title="De click aqui para eliminar registro"  onclick="desactivar('.$reg->idorigenes.')"><i class="fa fa-close"></i></button>'.
 					' <button class="btn btn-warning"  title="De click aqui para listar destinos " onclick="listarDestinos('.$reg->idorigenes.')"><i class="fa fa-eye"></i></button>':
 					' <button class="btn btn-warning" onclick="mostrar('.$reg->idorigenes.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idorigenes.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->idorigenes,		
 				"2"=>substr($reg->nombre,0,30),
 				"3"=>substr($reg->almacen,0,30),
 				"4"=>$reg->fecha,
 				"5"=>$reg->importe,
 				"6"=>$reg->nota,
 				"7"=>$reg->condicion?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;
	case 'listarDestinos':
		$rspta=$origenes->listarDestinos($idorigenes);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){ 			
 			$data[]=array(
 				"0"=>'<button class="btn btn-warning" onclick="mostrarPago('.$reg->iddet_destinos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivarPago('.$reg->iddet_destinos.')"><i class="fa fa-close"></i></button>',
 				"1"=>$reg->iddet_destinos,	
 				"2"=>$reg->destino,
 				"3"=>$reg->fecha,
 				"4"=>$reg->importe,
 				"5"=>$reg->condicion?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;
	
	case "select":
		
		$rspta = $origenes->select($Opcion,$referencia);
		echo '<option value=' . "0" . '>' ." Selecione un Valor ". '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idorigenes_a_favor . '>' .($reg->idorigenes_a_favor)." - ". $reg->nombre . '</option>';
				}
	break;
	case "selectCat_de_Origenes":
		
		$rspta = $origenes->selectCat_de_Origenes();
		echo '<option value=' . "0" . '>' ." Selecione un Valor ". '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idcat_de_origenes . '>' .($reg->idcat_de_origenes)." - ". $reg->nombre . '</option>';
				}
	break;
	case "selectDestinos":
		
		$rspta = $origenes->selectDestinos();
		echo '<option value=' . "0" . '>' ." Selecione un Valor ". '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->iddestinos . '>' .($reg->iddestinos)." - ". $reg->nombre . '</option>';
				}
	break;
}
?>