<?php 
require_once "../modelos/Pagos.php";  

$pagos=new Pagos();

$referencia=isset($_REQUEST["referencia"])? limpiarCadena($_REQUEST["referencia"]):"";
$Opcion=isset($_REQUEST["Opcion"])? limpiarCadena($_REQUEST["Opcion"]):"";
$ident_sal=isset($_REQUEST["ident_sal"])? limpiarCadena($_REQUEST["ident_sal"]):"";
$fecha_inicial=isset($_REQUEST["fecha_inicial"])? limpiarCadena($_REQUEST["fecha_inicial"]):"";
$fecha_final=isset($_REQUEST["fecha_final"])? limpiarCadena($_REQUEST["fecha_final"]):"";
$saldo=isset($_REQUEST["saldo"])? limpiarCadena($_REQUEST["saldo"]):"";
$idpersonas=isset($_REQUEST["idpersonas"])? limpiarCadena($_REQUEST["idpersonas"]):"";
$tipo=isset($_REQUEST["tipo"])? limpiarCadena($_REQUEST["tipo"]):"";
$idpagos=isset($_REQUEST["idpagos"])? limpiarCadena($_REQUEST["idpagos"]):"";
$fecha=isset($_REQUEST["fecha"])? limpiarCadena($_REQUEST["fecha"]):"";
$idformadepagos=isset($_REQUEST["idformadepagos"])? limpiarCadena($_REQUEST["idformadepagos"]):"";
$importe=isset($_REQUEST["importe"])? limpiarCadena($_REQUEST["importe"]):"";
$idusuarios=isset($_REQUEST["idusuarios"])? limpiarCadena($_REQUEST["idusuarios"]):"";
$clave=isset($_REQUEST["clave"])? limpiarCadena($_REQUEST["clave"]):"";
$np=isset($_REQUEST["np"])? limpiarCadena($_REQUEST["np"]):"";

switch ($_GET["op"]){
	case 'agregarPago':
	
		if (empty($np)){
			$rspta=$pagos->insertar($ident_sal,$fecha,$referencia,$idformadepagos,$importe,$idusuarios);
			echo $rspta ? "Pago registgrado" : " Pago no registrado $ident_sal,$fecha,$referencia,$idformadepagos,$importe,$idusuarios";
		}
		else {
			$rspta=$pagos->editar($idpagos,$ident_sal,$fecha,$referencia,$idformadepagos,$importe,$idusuarios,$np);
			echo $rspta ? "Pago actualizado " : "no se pudo actualizar el pago $idpagos,$ident_sal,$fecha,$referencia,$idformadepagos,$importe,$idusuarios";
		}
	break;

	case 'desactivar':
		$rspta=$pagos->desactivar($idarticulos_almacenes);
 		echo $rspta ? "Articulo en Almacen Desactivado" : "Articulo en Almacen no se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$pagos->activar($idarticulos_almacenes);
 		echo $rspta ? "Articulo en Almacen activado" : "Articulo en Almacen no se puedo activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$pagos->mostrar($idarticulos_almacenes);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;
	
	

	case 'listar':
		$rspta=$pagos->listar($Opcion,$referencia,$fecha_inicial,$fecha_final,$ident_sal,$idpersonas,$tipo,$saldo);
 		
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>'<button class="btn btn-warning" onclick="selectFolio('.$reg->ident_sal.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->nombre,
 				"2"=>$reg->ident_sal,
 				"3"=>$reg->fecha,
 				"4"=>$reg->estatus,
 				"5"=>$reg->total,
 				"6"=>$reg->abonos,
 				"7"=>$reg->saldo
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);
 		

	break;
	case 'mostrarPagos':
		$rspta=$pagos->mostrarPagos($idpagos,$ident_sal);
 		
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>'<button class="btn btn-warning" onclick="imprimirPago('.$reg->ident_sal.','.$reg->idpagos.')"><i class="fa fa-print"></i></button>',
 				"1"=>$reg->ident_sal,
 				"2"=>$reg->idpagos,
 				"3"=>$reg->fecha,
 				"4"=>$reg->tipo_de_pago,
 				"5"=>$reg->importe,
 				"6"=>$reg->referencia,
 				"7"=>$reg->usuario
 				);
 		}

 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);
 		

	break;
	
	case "select":
	require_once "../modelos/Almacenes.php"; 
	$almacenes=new Almacenes();
	$rspta=$almacenes->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idalmacenes . '>' .$reg->nombre . '</option>';
				}
	break;
	case 'activarOpciones':
		$rspta=$pagos->activarOpciones($idusuarios,$clave);
 		//Codificar el resultado utilizando json
 		echo $rspta;
 		break;
}
?>