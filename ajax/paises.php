<?php 
require_once "../modelos/Paises.php";

$paises=new Paises();

$idpaises=isset($_POST["idpaises"])? limpiarCadena($_POST["idpaises"]):"";
$nombre=isset($_POST["nombre"])? limpiarCadena($_POST["nombre"]):"";


switch ($_GET["op"]){
	case 'guardaryeditar':
		if (empty($idpaises)){
			$rspta=$paises->insertar($nombre);
			echo $rspta ? "Pais registrada" : "Pais no se pudo registrar";
		}
		else {
			$rspta=$paises->editar($idpaises,$nombre);
			echo $rspta ? "Pais actualizada" : "Pais no se pudo actualizar";
		}
	break;

	case 'desactivar':
		$rspta=$paises->desactivar($idpaises);
 		echo $rspta ? "Pais Desactivada" : "Pais no se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$paises->activar($idpaises);
 		echo $rspta ? "Pais activada" : "Pais no se puede activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$paises->mostrar($idpaises);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$paises->listar();
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idpaises.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idpaises.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idpaises.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idpaises.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->nombre,
 				"2"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "select":
		
		$rspta = $paises->select();
	    echo '<option>' . "Selecione valor" . '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idpaises . '>' . $reg->nombre . '</option>';
				}
	break;
}
?>