<?php 
require_once "../modelos/Pendientes.php"; 

//ini_set ( 'max_execution_time', 3600);  
$pendientes=new Pendientes();




$idpendientes=isset($_REQUEST["idpendientes"])? limpiarCadena($_REQUEST["idpendientes"]):"";
$idpersonas=isset($_REQUEST["idpersonas"])? limpiarCadena($_REQUEST["idpersonas"]):"";
$idusuarios=isset($_REQUEST["idusuarios"])? limpiarCadena($_REQUEST["idusuarios"]):"";
$asunto=isset($_REQUEST["asunto"])? limpiarCadena($_REQUEST["asunto"]):"";
$contestacion=isset($_REQUEST["contestacion"])? limpiarCadena($_REQUEST["contestacion"]):"";
$imagen=isset($_REQUEST["imagen"])? limpiarCadena($_REQUEST["imagen"]):"";
$origen=isset($_REQUEST["origen"])? limpiarCadena($_REQUEST["origen"]):"";
$importe=isset($_REQUEST["importe"])? limpiarCadena($_REQUEST["importe"]):"";
$fecha_inicial=isset($_REQUEST["fecha_inicial"])? limpiarCadena($_REQUEST["fecha_inicial"]):"";
$fecha_final=isset($_REQUEST["fecha_final"])? limpiarCadena($_REQUEST["fecha_final"]):"";
$fecha=isset($_REQUEST["fecha"])? limpiarCadena($_REQUEST["fecha"]):"";
$codigo=1;

//echo "op ".$_REQUEST["op"];
switch ($_REQUEST["op"]){
	case 'guardaryeditar':

		if (!file_exists($_FILES['imagen']['tmp_name']) || !is_uploaded_file($_FILES['imagen']['tmp_name']))
		{
			$imagen=$_REQUEST["imagenactual"];
		}
		else 
		{
			$ext = explode(".", $_FILES["imagen"]["name"]);
			if ($_FILES['imagen']['type'] == "image/jpg" || $_FILES['imagen']['type'] == "image/jpeg" || $_FILES['imagen']['type'] == "image/png")
			{
				$imagen = $codigo . '.' . end($ext);
				move_uploaded_file($_FILES["imagen"]["tmp_name"], "../files/pendientes/" . $imagen);
			}
		}
		if (empty($idpendientes)){
			$fecha_inicial=date('Y-m-d');$hora_inicial=date("H:i"); 
			$rspta=$pendientes->insertar($idusuarios,$idpersonas,$fecha,$fecha_inicial,$hora_inicial,$origen,$asunto,$contestacion,$importe,$imagen);
			echo $rspta ? "Pendiente registrado " : "Pendiente no se pudo registrar $idusuarios,$idpersonas,$fecha,$fecha_inicial,$hora_inicial,$origen,$asunto,$contestacion,$importe,$imagen,$condicion";
		}
		else {
			$fecha_final=date('Y-m-d');$hora_final=date("H:i"); 
			$rspta=$pendientes->editar($idpendientes,$fecha_final,$hora_final,$contestacion,$importe,$imagen);
			echo $rspta ? "Pendiente actualizado" : "Pendiente no se pudo actualizar $idusuarios,$idpersonas,$fecha,$fecha_inicial,$hora_inicial,$origen,$asunto,$contestacion,$importe,$imagen,$condicion";
		}
	break;
	
	case 'desactivar':
		$rspta=$pendientes->desactivar($idpendientes);
 		echo $rspta ? "Artículo Desactivado" : "Artículo no se puede desactivar";
	break;

	case 'activar':
		$rspta=$pendientes->activar($idpendientes);
 		echo $rspta ? "Artículo activado" : "Artículo no se puede activar";
	break;

	case 'mostrar':
		$rspta=$pendientes->mostrar($idpendientes);
 		//Codificar el resultado utilizando json
 		//echo "id de pendientes $idpendientes";
 		echo json_encode($rspta);
	break;

	case 'listar':
		$rspta=$pendientes->listar($fecha_inicial,$fecha_final);
 		//Vamos a declarar un array
 		//var_dump($rspta);
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idpendientes.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idpendientes.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idpendientes.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idpendientes.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->cliente,
 				"2"=>$reg->fecha,
 				"3"=>$reg->hora_inicial,
 				"4"=>$reg->hora_final,
 				"5"=>$reg->asunto,
 				"6"=>$reg->contestacion,
 				"7"=>($reg->condicion)?'<span class="label bg-green">Pendiente</span>':
 				'<span class="label bg-red">Visto</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;
	case "select":
	
		$rspta=$pendientes->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idpendientes . '>' .$reg->codigo." ".$reg->nombre . '</option>';
				}
	break;

	case "selectlineas":
		require_once "../modelos/Lineas.php";
		$lineas = new Lineas();

		$rspta = $lineas->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idlineas . '>' . $reg->nombre . '</option>';
				}
	break;
	case "selectsublineas":
		require_once "../modelos/Sublineas.php";
		$sublineas = new Sublineas();

		$rspta = $sublineas->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idsublineas . '>' . $reg->nombre . '</option>';
				}
	break;
	case "listar_sat_unidad_de_medida":

		$rspta = $pendientes->listar_sat_unidad_de_medida();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->clave . '>' . $reg->nombre . '</option>';
				}
	break;
	case "listar_sat_prod_y_serv":
		$rspta = $pendientes->listar_sat_prod_y_serv();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->clave . '>' . $reg->nombre . '</option>';
				}
	break;

	case "selectcatalogo":
	
		$rspta=$pendientes->selectcatalogo();
		echo '<option value=' . '>' . "Selecione Producto". '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idpendientes . '>' . $reg->codigo." ".$reg->nombre . '</option>';
				}
	break;

	case "selectivas":
	
		$rspta=$pendientes->selectivas();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idivas . '>' .$reg->nombre . '</option>';
				}
	break;
	
	case "selectmonedas":
	
		$rspta=$pendientes->selectmonedas();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idmonedas . '>' .$reg->nombre . '</option>';
				}
	break;
	case "selectpendientesporsublinea":
	
		$rspta=$pendientes->selectpendientesporsublinea($idsublineas);

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idpendientes . '>' .$reg->codigo." ".$reg->nombre . '</option>';
				}
	break;
	

}
?>