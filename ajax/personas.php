<?php 
require_once "../modelos/Personas.php"; 

$personas=new Personas();

$idpersonas=isset($_REQUEST["idpersonas"])? limpiarCadena($_REQUEST["idpersonas"]):"";
$nombre=isset($_REQUEST["nombre"])? limpiarCadena($_REQUEST["nombre"]):"";
$nombrecomercial=isset($_REQUEST["nombrecomercial"])? limpiarCadena($_REQUEST["nombrecomercial"]):"";
$rfc=isset($_REQUEST["rfc"])? limpiarCadena($_REQUEST["rfc"]):"";
$idmunicipios=isset($_REQUEST["idmunicipios"])? limpiarCadena($_REQUEST["idmunicipios"]):"";
$idcolonias=isset($_REQUEST["idcolonias"])? limpiarCadena($_REQUEST["idcolonias"]):"";
$idcalles=isset($_REQUEST["idcalles"])? limpiarCadena($_REQUEST["idcalles"]):"";
$numero_exterior=isset($_REQUEST["numero_exterior"])? limpiarCadena($_REQUEST["numero_exterior"]):"";
$numero_interior=isset($_REQUEST["numero_interior"])? limpiarCadena($_REQUEST["numero_interior"]):"";
$referencia=isset($_REQUEST["referencia"])? limpiarCadena($_REQUEST["referencia"]):"";
$codigo_postal=isset($_REQUEST["codigo_postal"])? limpiarCadena($_REQUEST["codigo_postal"]):"";
$telefono=isset($_REQUEST["telefono"])? limpiarCadena($_REQUEST["telefono"]):"";
$correo=isset($_REQUEST["correo"])? limpiarCadena($_REQUEST["correo"]):"";
$tipodepersona=isset($_REQUEST["tipodepersona"])? limpiarCadena($_REQUEST["tipodepersona"]):"";
$diascredito=isset($_REQUEST["diascredito"])? limpiarCadena($_REQUEST["diascredito"]):"";
$limitecredito=isset($_REQUEST["limitecredito"])? limpiarCadena($_REQUEST["limitecredito"]):"";
$idvendedores=isset($_REQUEST["idvendedores"])? limpiarCadena($_REQUEST["idvendedores"]):"";
$Opcion=isset($_REQUEST["Opcion"])? limpiarCadena($_REQUEST["Opcion"]):"";
$referencia=isset($_REQUEST["referencia"])? limpiarCadena($_REQUEST["referencia"]):"";

switch ($_GET["op"]){
	case 'guardaryeditar':
		if (empty($idpersonas)){
			$rspta=$personas->insertar($nombre,$nombrecomercial,$rfc,$idmunicipios,$idcolonias,$idcalles,$idvendedores,$numero_exterior,$numero_interior,$referencia,$codigo_postal,$telefono,$correo,$diascredito,$limitecredito,$tipodepersona); 
			
			echo $rspta ? "Persona registrada $idpersonas " : "Persona no se pudo registrar $idpersonas";
		}
		else {
			$rspta=$personas->editar($idpersonas,$nombre,$nombrecomercial,$rfc,$idmunicipios,$idcolonias,$idcalles,$idvendedores,$numero_exterior,$numero_interior,$referencia,$codigo_postal,$telefono,$correo,$diascredito,$limitecredito,$tipodepersona);
			echo $rspta ? "Persona actualizada" : "Persona no se pudo actualizar";
		}
	break;

	/*case 'eliminar':
		$rspta=$personas->eliminar($idpersonas);
 		echo $rspta ? "Persona Eliminada" : "Persona no se puedo eliminar";
 		break;
	break;
	*/

	case 'desactivar':
		$rspta=$personas->desactivar($idpersonas);
 		echo $rspta ? "Persona Desactivada" : "La persona no se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$personas->activar($idpersonas);
 		echo $rspta ? "Persona activada" : "La persona no se puedo activar";
 		break;
	break;

	

	case 'mostrar':
		$rspta=$personas->mostrar($idpersonas);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listarp':
		$rspta=$personas->listarp($Opcion,$referencia);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idpersonas.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idpersonas.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idpersonas.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idpersonas.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->nombre,
 				"2"=>$reg->nombrecomercial,
 				"3"=>$reg->rfc,
 				"4"=>$reg->telefono,
 				"5"=>$reg->condicion?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case 'listarc':
		$rspta=$personas->listarc($Opcion,$referencia);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){ 			
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idpersonas.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idpersonas.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idpersonas.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idpersonas.')"><i class="fa fa-check"></i></button>',

 				"1"=>substr($reg->nombre,0,30),
 				"2"=>substr($reg->nombrecomercial,0,30),
 				"3"=>$reg->rfc,
 				"4"=>$reg->telefono,
 				"5"=>$reg->condicion?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;
	case "selectp":
		
		$rspta = $personas->selectp($Opcion,$referencia); 
		echo '<option value=' . "0" . '>' ." Selecione un Valor ". '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idpersonas . '>' .($reg->idpersonas)." - ". $reg->nombre . '</option>';
				}
	break;
	case "selectc":
		
		$rspta = $personas->selectc($Opcion,$referencia);
		echo '<option value=' . "0" . '>' ." Selecione un Valor ". '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idpersonas . '>' .($reg->idpersonas)." - ". $reg->nombre . '</option>';
				}
	break;
}
?>