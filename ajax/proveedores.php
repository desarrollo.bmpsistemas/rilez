<?php 
require_once "../modelos/Proveedores.php"; 

$proveedores=new Proveedores();

$idpersonas=isset($_REQUEST["idpersonas"])? limpiarCadena($_REQUEST["idpersonas"]):"";
$idproveedores=isset($_REQUEST["idproveedores"])? limpiarCadena($_REQUEST["idproveedores"]):"";
$nombre=isset($_REQUEST["nombre"])? limpiarCadena($_REQUEST["nombre"]):"";
$nombrecomercial=isset($_REQUEST["nombrecomercial"])? limpiarCadena($_REQUEST["nombrecomercial"]):"";
$rfc=isset($_REQUEST["rfc"])? limpiarCadena($_REQUEST["rfc"]):"";
$referencia=isset($_REQUEST["referencia"])? limpiarCadena($_REQUEST["referencia"]):"";
$telefono=isset($_REQUEST["telefono"])? limpiarCadena($_REQUEST["telefono"]):"";
$correo=isset($_REQUEST["correo"])? limpiarCadena($_REQUEST["correo"]):"";
$diascredito=isset($_REQUEST["diascredito"])? limpiarCadena($_REQUEST["diascredito"]):"";
$limitecredito=isset($_REQUEST["limitecredito"])? limpiarCadena($_REQUEST["limitecredito"]):"";
$Opcion=isset($_REQUEST["Opcion"])? limpiarCadena($_REQUEST["Opcion"]):"";


switch ($_GET["op"]){
	case 'guardaryeditar':
		if (empty($idpersonas)){
			$rspta=$proveedores->insertar($nombre,$nombrecomercial,$rfc,$referencia,$telefono,$correo,$diascredito,$limitecredito); 
			
			echo $rspta ? "proveedor registrado $rspta " : "proveedor no se pudo registrar $rspta";
		}
		else {
			$rspta=$proveedores->editar($idpersonas,$nombre,$nombrecomercial,$rfc,$referencia,$telefono,$correo,$diascredito,$limitecredito);
			echo $rspta ? "proveedor actualizado $rspta" : "proveedor no se pudo actualizar $rspta";
		}
	break;

	/*case 'eliminar':
		$rspta=$proveedores->eliminar($idpersonas);
 		echo $rspta ? "proveedor Eliminada" : "proveedor no se puedo eliminar";
 		break;
	break;
	*/

	case 'desactivar':
		$rspta=$proveedores->desactivar($idpersonas);
 		echo $rspta ? "proveedor Desactivada" : "La proveedor no se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$proveedores->activar($idpersonas);
 		echo $rspta ? "proveedor activada" : "La proveedor no se puedo activar";
 		break;
	break;

	

	case 'mostrar':
		$rspta=$proveedores->mostrar($idpersonas);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	

	case 'listar':
		$rspta=$proveedores->listar($Opcion,$referencia);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){ 			
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idproveedores.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idproveedores.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idproveedores.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idproveedores.')"><i class="fa fa-check"></i></button>',

 				"1"=>substr($reg->nombre,0,30),
 				"2"=>substr($reg->nombrecomercial,0,30),
 				"3"=>$reg->rfc,
 				"4"=>$reg->telefono,
 				"5"=>$reg->condicion?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;
	
	case "select":
		
		$rspta = $proveedores->select($Opcion,$referencia);
		echo '<option value=' . "0" . '>' ." Selecione un Valor ". '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idproveedores . '>' .($reg->idproveedores)." - ". $reg->nombre . '</option>';
				}
	break;
}
?>