<?php 
require_once "../modelos/Puestos.php";

$puestos=new Puestos();

$idpuestos=isset($_POST["idpuestos"])? limpiarCadena($_POST["idpuestos"]):"";
$nombre=isset($_POST["nombre"])? limpiarCadena($_POST["nombre"]):"";


switch ($_GET["op"]){
	case 'guardaryeditar':
		if (empty($idpuestos)){
			$rspta=$puestos->insertar($nombre);
			echo $rspta ? "Puesto registrada" : "Puestos no se pudo registrar";
		}
		else {
			$rspta=$puestos->editar($idpuestos,$nombre);
			echo $rspta ? "Puesto actualizada" : "Puestos no se pudo actualizar";
		}
	break;

	case 'desactivar':
		$rspta=$puestos->desactivar($idpuestos);
 		echo $rspta ? "Puesto Desactivada" : "Puestos no se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$puestos->activar($idpuestos);
 		echo $rspta ? "Puesto activada" : "Puestos no se puede activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$puestos->mostrar($idpuestos);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$puestos->listar();
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idpuestos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idpuestos.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idpuestos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idpuestos.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->nombre,
 				"2"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "select":
		
		$rspta = $puestos->select();
		echo '<option value=' . '>' . "Selecione Puesto". '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idpuestos . '>' . $reg->nombre . '</option>';
				}
	break;
}
?>