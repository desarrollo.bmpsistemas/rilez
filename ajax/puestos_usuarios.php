<?php 
require_once "../modelos/Puestos_usuarios.php"; 

$puestos_usuarios=new Puestos_usuarios();


$idpuestos=isset($_REQUEST["idpuestos"])? limpiarCadena($_REQUEST["idpuestos"]):"";
$idusuarios=isset($_REQUEST["idusuarios"])? limpiarCadena($_REQUEST["idusuarios"]):"";


switch ($_GET["op"]){
	
	case 'insertar':
		$rspta=$puestos_usuarios->insertar($idpuestos,$idusuarios);
 		echo $rspta ? "Modulos Agregados al usuario $rspta" : "Modulos por Puesto no  se pueden agregar al usuario ";
 		break;
	break;

	case 'eliminar':
		$rspta=$puestos_usuarios->eliminar($idusuarios);
 		echo $rspta ? "Modulos Eliminados $rspta" : "Modulos no eliminados $rspta";
 		break;
	break;

	
	case 'mostrar':
		$rspta=$puestos_usuarios->mostrar($idpuestos_usuarios);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$puestos_usuarios->listar($idpuestos);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idusuarios.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idusuarios.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idusuarios.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idusuarios.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->puesto,
 				"2"=>$reg->usuario,
 				"3"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;
}
?>