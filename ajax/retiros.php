<?php 
require_once "../modelos/Retiros.php";  

$retiros=new Retiros();

$idcat_de_origenes=isset($_REQUEST["idcat_de_origenes"])? limpiarCadena($_REQUEST["idcat_de_origenes"]):"";
$iddestinos=isset($_REQUEST["iddestinos"])? limpiarCadena($_REQUEST["iddestinos"]):"";
$iddet_destinos=isset($_REQUEST["iddet_destinos"])? limpiarCadena($_REQUEST["iddet_destinos"]):"";
$idfondos=isset($_REQUEST["idfondos"])? limpiarCadena($_REQUEST["idfondos"]):"";
$idusuarios=isset($_REQUEST["idusuarios"])? limpiarCadena($_REQUEST["idusuarios"]):"";
$capturista_id=isset($_REQUEST["capturista_id"])? limpiarCadena($_REQUEST["capturista_id"]):"";
$fecha=isset($_REQUEST["fecha"])? limpiarCadena($_REQUEST["fecha"]):"";
$fecha_inicial=isset($_REQUEST["fecha_inicial"])? limpiarCadena($_REQUEST["fecha_inicial"]):"";
$fecha_final=isset($_REQUEST["fecha_final"])? limpiarCadena($_REQUEST["fecha_final"]):"";
$importe=isset($_REQUEST["importe"])? limpiarCadena($_REQUEST["importe"]):"";
$nota=isset($_REQUEST["nota"])? limpiarCadena($_REQUEST["nota"]):"";
$referencia=isset($_REQUEST["referencia"])? limpiarCadena($_REQUEST["referencia"]):"";

switch ($_GET["op"]){
	case 'guardaryeditar':
		//if (empty($idfondos)){
			$rspta=$retiros->insertar($idusuarios,$capturista_id,$idcat_de_origenes,$iddestinos,$fecha,$importe,$nota); 
			echo $rspta ? "Destino registrado $rspta " : "Destino no se pudo registrar $rspta";
		//}
		/*else {
			$rspta=$retiros->editar($idsaldos,$proveedor_id,$fecha,$importe,$nota);
			echo $rspta ? "saldo a favor actualizado $rspta" : "Saldo a favor no se pudo actualizar $rspta";
		}*/
	break;

	/*case 'eliminar':
		$rspta=$retiros->eliminar($idpersonas);
 		echo $rspta ? "proveedor Eliminada" : "proveedor no se puedo eliminar";
 		break;
	break;
	*/

	case 'desactivar':
		$rspta=$retiros->desactivar($iddet_destinos,$capturista_id);
 		echo $rspta ? "Saldo eliminado" : "Saldo no se pudo eliminar";
 		break;
	break;

	case 'activar':
		$rspta=$retiros->activar($idpersonas);
 		echo $rspta ? "proveedor activada" : "La proveedor no se puedo activar";
 		break;
	break;

		
	case 'verSiEsCajaChica':
		$rspta=$retiros->verSiEsCajaChica($iddet_destinos);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'mostrar':
		$rspta=$retiros->mostrar($idsaldos);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;
	case 'mostrarFondos':
		$rspta=$retiros->mostrarFondos($idcat_de_origenes);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;
	

	case 'listar':
		$rspta=$retiros->listar($fecha_inicial,$fecha_final);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){ 			
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->iddet_destinos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->iddet_destinos.')"><i class="fa fa-close"></i></button>'.
 					' <button class="btn btn-warning" onclick="listarPagos('.$reg->iddet_destinos.')"><i class="fa fa-pencil"></i></button>':
 					' <button class="btn btn-warning" onclick="mostrar('.$reg->iddet_destinos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->iddet_destinos.')"><i class="fa fa-check"></i></button>',
 				"1"=>substr($reg->origen,0,30),	
 				"2"=>substr($reg->destino,0,30),
 				"3"=>substr($reg->capturista,0,30),
 				"4"=>$reg->fecha,
 				"5"=>$reg->importe,
 				"6"=>$reg->nota,
 				"7"=>$reg->condicion?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;
	case 'listarDestinos':
		$rspta=$retiros->listarDestinos($fecha_inicial,$fecha_final,$iddestinos);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){ 			
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->iddet_destinos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->iddet_destinos.')"><i class="fa fa-close"></i></button>'.
 					' <button class="btn btn-warning" onclick="listarPagos('.$reg->iddet_destinos.')"><i class="fa fa-pencil"></i></button>':
 					' <button class="btn btn-warning" onclick="mostrar('.$reg->iddet_destinos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->iddet_destinos.')"><i class="fa fa-check"></i></button>',
 				"1"=>substr($reg->origen,0,30),	
 				"2"=>substr($reg->destino,0,30),
 				"3"=>substr($reg->capturista,0,30),
 				"4"=>$reg->fecha,
 				"5"=>$reg->importe,
 				"6"=>$reg->nota,
 				"7"=>$reg->condicion?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;
	
	case 'listarDestinosPorCapturista':
		$rspta=$retiros->listarDestinosPorCapturista($fecha_inicial,$fecha_final,$iddestinos,$capturista_id);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){ 			
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->iddet_destinos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->iddet_destinos.')"><i class="fa fa-close"></i></button>'.
 					' <button class="btn btn-warning" onclick="listarPagos('.$reg->iddet_destinos.')"><i class="fa fa-pencil"></i></button>':
 					' <button class="btn btn-warning" onclick="mostrar('.$reg->iddet_destinos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->iddet_destinos.')"><i class="fa fa-check"></i></button>',
 				"1"=>substr($reg->origen,0,30),	
 				"2"=>substr($reg->destino,0,30),
 				"3"=>substr($reg->capturista,0,30),
 				"4"=>$reg->fecha,
 				"5"=>$reg->importe,
 				"6"=>$reg->nota,
 				"7"=>$reg->condicion?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case 'listarPagos':
		$rspta=$retiros->listarPagos($idsaldos);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){ 			
 			$data[]=array(
 				"0"=>'<button class="btn btn-warning" onclick="mostrarPago('.$reg->iddet_saldos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivarPago('.$reg->iddet_saldos.')"><i class="fa fa-close"></i></button>',
 				"1"=>$reg->folio,	
 				"2"=>$reg->fecha,
 				"3"=>$reg->importe,
 				"4"=>$reg->condicion?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;
	
	case "select":
		
		$rspta = $retiros->select($Opcion,$referencia);
		echo '<option value=' . "0" . '>' ."Selecione". '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->iddet_destinos_a_favor . '>' .($reg->iddet_destinos_a_favor)." - ". $reg->nombre . '</option>';
				}
	break;
	case "selectCat_de_Origenes":
		
		$rspta = $retiros->selectCat_de_Origenes();
		echo '<option value=' . "0" . '>' ."Selecione". '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idcat_de_origenes . '>' .($reg->idcat_de_origenes)." - ". $reg->nombre . '</option>';
				}
	break;
	case "selectDestinos":
		
		$rspta = $retiros->selectDestinos();
		echo '<option value=' . "0" . '>' ."Selecione". '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->iddestinos . '>' .($reg->iddestinos)." - ". $reg->nombre . '</option>';
				}
	break;
	case "selectCapturistas":
		
		$rspta = $retiros->selectCapturistas($referencia);
		echo '<option value=' . "0" . '>' ."Selecione". '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idpersonas . '>' .($reg->idpersonas)." - ". $reg->nombre . '</option>';
				}
	break;

	
}
?>