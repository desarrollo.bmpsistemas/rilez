<?php 
require_once "../modelos/Saldos_a_favor.php"; 

$saldos_a_favor=new Saldos_a_favor();

$idsaldos=isset($_REQUEST["idsaldos"])? limpiarCadena($_REQUEST["idsaldos"]):"";
$idpersonas=isset($_REQUEST["idpersonas"])? limpiarCadena($_REQUEST["idpersonas"]):"";
$proveedor_id=isset($_REQUEST["proveedor_id"])? limpiarCadena($_REQUEST["proveedor_id"]):"";
$nombre=isset($_REQUEST["nombre"])? limpiarCadena($_REQUEST["nombre"]):"";

$fecha=isset($_REQUEST["fecha"])? limpiarCadena($_REQUEST["fecha"]):"";
$importe=isset($_REQUEST["importe"])? limpiarCadena($_REQUEST["importe"]):"";
$nota=isset($_REQUEST["nota"])? limpiarCadena($_REQUEST["nota"]):"";
$Opcion=isset($_REQUEST["Opcion"])? limpiarCadena($_REQUEST["Opcion"]):"";
$referencia=isset($_REQUEST["referencia"])? limpiarCadena($_REQUEST["referencia"]):"";

switch ($_GET["op"]){
	case 'guardaryeditar':
		if (empty($idsaldos)){
			$rspta=$saldos_a_favor->insertar($proveedor_id,$fecha,$importe,$nota); 
			echo $rspta ? "Saldo registrado $rspta " : "Saldo no se pudo registrar $rspta";
		}
		else {
			$rspta=$saldos_a_favor->editar($idsaldos,$proveedor_id,$fecha,$importe,$nota);
			echo $rspta ? "saldo a favor actualizado $rspta" : "Saldo a favor no se pudo actualizar $rspta";
		}
	break;

	/*case 'eliminar':
		$rspta=$saldos_a_favor->eliminar($idpersonas);
 		echo $rspta ? "proveedor Eliminada" : "proveedor no se puedo eliminar";
 		break;
	break;
	*/

	case 'desactivar':
		$rspta=$saldos_a_favor->desactivar($idsaldos);
 		echo $rspta ? "Saldo eliminado" : "Saldo no se pudo eliminar";
 		break;
	break;

	case 'activar':
		$rspta=$saldos_a_favor->activar($idpersonas);
 		echo $rspta ? "proveedor activada" : "La proveedor no se puedo activar";
 		break;
	break;

	

	case 'mostrar':
		$rspta=$saldos_a_favor->mostrar($idsaldos);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	

	case 'listar':
		$rspta=$saldos_a_favor->listar($idpersonas);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){ 			
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idsaldos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idsaldos.')"><i class="fa fa-close"></i></button>'.
 					' <button class="btn btn-warning" onclick="listarPagos('.$reg->idsaldos.')"><i class="fa fa-pencil"></i></button>':
 					' <button class="btn btn-warning" onclick="mostrar('.$reg->idsaldos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idsaldos.')"><i class="fa fa-check"></i></button>',

 				"1"=>substr($reg->nombre,0,30),
 				"2"=>$reg->fecha,
 				"3"=>$reg->importe,
 				"4"=>$reg->abonos,
 				"5"=>$reg->nota,
 				"6"=>$reg->condicion?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;
	case 'listarPagos':
		$rspta=$saldos_a_favor->listarPagos($idsaldos);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){ 			
 			$data[]=array(
 				"0"=>'<button class="btn btn-warning" onclick="mostrarPago('.$reg->iddet_saldos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivarPago('.$reg->iddet_saldos.')"><i class="fa fa-close"></i></button>',
 				"1"=>$reg->folio,	
 				"2"=>$reg->fecha,
 				"3"=>$reg->importe,
 				"4"=>$reg->condicion?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;
	
	case "select":
		
		$rspta = $saldos_a_favor->select($Opcion,$referencia);
		echo '<option value=' . "0" . '>' ." Selecione un Valor ". '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idsaldos_a_favor . '>' .($reg->idsaldos_a_favor)." - ". $reg->nombre . '</option>';
				}
	break;
}
?>