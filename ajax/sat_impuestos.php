  <?php 
require_once "../modelos/Sat_impuestos.php";

$sat_impuestos=new Sat_impuestos();

$idsat_impuestos=isset($_POST["idsat_impuestos"])? limpiarCadena($_POST["idsat_impuestos"]):"";
$nombre=isset($_POST["nombre"])? limpiarCadena($_POST["nombre"]):"";


switch ($_GET["op"]){

	case 'guardaryeditar':
		if (empty($idsat_impuestos)){
			$rspta=$sat_impuestos->insertar($nombre);
			echo $rspta ? "Opcion Restringida " : " Opcion no Restringida";
		}
		else {
			$rspta=$sat_impuestos->editar($idsat_impuestos,$nombre);
			echo $rspta ? "Opcion Restringida " : "Opcion no Restringida ";
		}
	break;

	case 'desactivar':
		$rspta=$sat_impuestos->desactivar($idsat_impuestos);
 		echo $rspta ? "Desactivada" : "No se puede desactivar $idsat_impuestos ";
 		break;
	break;

	case 'activar':
		$rspta=$sat_impuestos->activar($idsat_impuestos);
 		echo $rspta ? "Activada" : "No se puede activar $idsat_impuestos";
 		break;
	break;

	case 'mostrar':
		$rspta=$sat_impuestos->mostrar($idsat_impuestos);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$sat_impuestos->listar();
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idsat_impuestos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idsat_impuestos.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idsat_impuestos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idsat_impuestos.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->clave,
 				"2"=>$reg->nombre,
 				"3"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "select":
		//require_once "../modelos/sat_impuestos.php";
		//$sat_impuestos = new sat_impuestos();
		$rspta = $sat_impuestos->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idsat_impuestos . '>'.$reg->clave ." ". $reg->nombre . '</option>';
				}
	break;
	
}
?>