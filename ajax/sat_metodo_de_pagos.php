 <?php 
require_once "../modelos/Sat_metodo_de_pagos.php";

$sat_metodo_de_pagos =new Sat_metodo_de_pagos();

$idsat_metodo_de_pagos=isset($_REQUEST["idsat_metodo_de_pagos"])? limpiarCadena($_REQUEST["idsat_metodo_de_pagos"]):"";
$nombre=isset($_REQUEST["nombre"])? limpiarCadena($_REQUEST["nombre"]):"";


switch ($_GET["op"]){

	case 'guardaryeditar':
		if (empty($idsat_metodo_de_pagos)){
			$rspta=$sat_metodo_de_pagos->insertar($nombre);
			echo $rspta ? "Opcion  " : " Opcion Restringida";
		}
		else {
			$rspta=$sat_metodo_de_pagos->editar($idsat_metodo_de_pagos,$nombre);
			echo $rspta ? "Opcion  " : "Opcion Restringida ";
		}
	break;

	case 'desactivar':
		$rspta=$sat_metodo_de_pagos->desactivar($idsat_metodo_de_pagos);
 		echo $rspta ? "Desactivada $idsat_metodo_de_pagos" : "No se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$sat_metodo_de_pagos->activar($idsat_metodo_de_pagos);
 		echo $rspta ? "Activada $idsat_metodo_de_pagos" : "No se puede activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$sat_metodo_de_pagos->mostrar($idsat_metodo_de_pagos);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$sat_metodo_de_pagos->listar();
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idsat_metodo_de_pagos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idsat_metodo_de_pagos.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idsat_metodo_de_pagos.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idsat_metodo_de_pagos.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->clave,
 				"2"=>$reg->nombre,
 				"3"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "select":
		
		$rspta = $sat_metodo_de_pagos->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idsat_metodo_de_pagos . '>'.$reg->clave ." ". $reg->nombre . '</option>';
				}
	break;
	
}
?>