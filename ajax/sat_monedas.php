 <?php 
require_once "../modelos/Sat_monedas.php";

$sat_monedas=new Sat_monedas();

$idsat_monedas=isset($_POST["idsat_monedas"])? limpiarCadena($_POST["idsat_monedas"]):"";
$clave=isset($_POST["clave"])? limpiarCadena($_POST["clave"]):"";
$nombre=isset($_POST["nombre"])? limpiarCadena($_POST["nombre"]):"";
$condicion=isset($_POST["condicion"])? limpiarCadena($_POST["condicion"]):"";


switch ($_GET["op"]){
	case 'guardaryeditar':
		if (empty($idsat_monedas)){
			$rspta=$sat_monedas->insertar($nombre,$clave);
			echo $rspta ? "Sat Monedas registrada" : "Sat Monedas no se pudo registrar";
		}
		else {
			$rspta=$sat_monedas->editar($idsat_monedas,$nombre,$clave);
			echo $rspta ? "Sat Monedas actualizada" : "Sat Monedas no se pudo actualizar";
		}
	break;

	case 'desactivar':
		$rspta=$sat_monedas->desactivar($idsat_monedas);
 		echo $rspta ? "Sat Monedas Desactivada" : "Sat Monedas no se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$sat_monedas->activar($idsat_monedas);
 		echo $rspta ? "Sat Monedas activada" : "Sat Monedas no se puede activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$sat_monedas->mostrar($idsat_monedas);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$sat_monedas->listar();
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idsat_monedas.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idsat_monedas.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idsat_monedas.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idsat_monedas.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->clave,
 				"2"=>$reg->nombre,
 				"3"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "select":
		//require_once "../modelos/Sat_monedas.php";
		//$sat_monedas = new Sat_monedas();
		$rspta = $sat_monedas->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idsat_monedas . '>' .$reg->idsat_monedas ." ". $reg->nombre . '</option>';
				}
	break;
	
}
?>