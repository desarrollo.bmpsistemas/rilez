 <?php 
require_once "../modelos/Sat_prod_servs.php";

$sat_prod_servs =new Sat_prod_servs(); 

$idsat_prod_servs=isset($_POST["idsat_prod_servs"])? limpiarCadena($_POST["idsat_prod_servs"]):"";
$clave=isset($_POST["clave"])? limpiarCadena($_POST["clave"]):"";
$nombre=isset($_POST["nombre"])? limpiarCadena($_POST["nombre"]):"";
$Opcion=isset($_REQUEST["Opcion"])? limpiarCadena($_REQUEST["Opcion"]):"";
$referencia=isset($_REQUEST["referencia"])? limpiarCadena($_REQUEST["referencia"]):"";

switch ($_GET["op"]){

	case 'guardaryeditar':
		if (empty($idsat_prod_servs)){
			$rspta=$sat_prod_servs->insertar($clave);
			echo $rspta ? "Opcion Restringida " : " Opcion Restringida";
		}
		else {
			$rspta=$sat_prod_servs->editar($idsat_prod_servs,$clave);
			echo $rspta ? "Opcion Restringida " : "Opcion Restringida ";
		}
	break;

	case 'desactivar':
		$rspta=$sat_prod_servs->desactivar($idsat_prod_servs);
 		echo $rspta ? "Desactivada" : "No se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$sat_prod_servs->activar($idsat_prod_servs);
 		echo $rspta ? "Activada" : "No se puede activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$sat_prod_servs->mostrar($idsat_prod_servs);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$sat_prod_servs->listar($Opcion,$referencia);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idsat_prod_servs.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idsat_prod_servs.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idsat_prod_servs.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idsat_prod_servs.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->clave,
 				"2"=>$reg->nombre,
 				"3"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "select":
		
		$rspta = $sat_prod_servs->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idsat_prod_servs . '>'. $reg->clave . " ". $reg->nombre .   '</option>';
				}
	break;
	case "selectActivos":
		
		
		$rspta = $sat_prod_servs->selectActivos();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idsat_prod_servs . '>'. $reg->clave . " ". $reg->nombre .   '</option>';
				}
	break;
	
}
?>