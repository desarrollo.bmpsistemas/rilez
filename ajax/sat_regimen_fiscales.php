  <?php 
require_once "../modelos/Sat_regimen_fiscales.php"; 

$sat_regimen_fiscales =new Sat_regimen_fiscales();

$idsat_regimen_fiscales=isset($_POST["idsat_regimen_fiscales"])? limpiarCadena($_POST["idsat_regimen_fiscales"]):"";
$clave=isset($_POST["clave"])? limpiarCadena($_POST["clave"]):"";
$nombre=isset($_POST["nombre"])? limpiarCadena($_POST["nombre"]):"";

switch ($_GET["op"]){

	case 'guardaryeditar':
		if (empty($idsat_regimen_fiscales)){
			$rspta=$sat_regimen_fiscales->insertar($clave);
			echo $rspta ? "Opcion Restringida " : " Opcion Restringida";
		}
		else {
			$rspta=$sat_regimen_fiscales->editar($idsat_regimen_fiscales,$clave);
			echo $rspta ? "Opcion Restringida " : "Opcion Restringida ";
		}
	break;

	case 'desactivar':
		$rspta=$sat_regimen_fiscales->desactivar($idsat_regimen_fiscales);
 		echo $rspta ? "Desactivada" : "No se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$sat_regimen_fiscales->activar($idsat_regimen_fiscales);
 		echo $rspta ? "Activada" : "No se puede activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$sat_regimen_fiscales->mostrar($idsat_regimen_fiscales);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$sat_regimen_fiscales->listar();
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idsat_regimen_fiscales.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idsat_regimen_fiscales.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idsat_regimen_fiscales.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idsat_regimen_fiscales.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->clave,
 				"2"=>$reg->nombre,
 				"3"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "select":
		
		$rspta = $sat_regimen_fiscales->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idsat_regimen_fiscales . '>'.  $reg->clave . " ".$reg->nombre .'</option>';
				}
	break;
	
}
?>