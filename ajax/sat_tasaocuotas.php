 <?php 
require_once "../modelos/Sat_tasaocuotas.php";

$sat_tasaocuotas =new Sat_tasaocuotas();

$idsat_tasaocuotas=isset($_REQUEST["idsat_tasaocuotas"])? limpiarCadena($_REQUEST["idsat_tasaocuotas"]):"";
$clave=isset($_REQUEST["clave"])? limpiarCadena($_REQUEST["clave"]):"";
$valor_maximo=isset($_REQUEST["valor_maximo"])? limpiarCadena($_REQUEST["valor_maximo"]):"";



switch ($_GET["op"]){

	case 'guardaryeditar':
		if (empty($idsat_tasaocuotas)){
			$rspta=$sat_tasaocuotas->insertar($clave);
			echo $rspta ? "Opcion Restringida " : " Opcion Restringida";
		}
		else {
			$rspta=$sat_tasaocuotas->editar($idsat_tasaocuotas,$clave);
			echo $rspta ? "Opcion Restringida " : "Opcion Restringida ";
		}
	break;

	case 'desactivar':
		$rspta=$sat_tasaocuotas->desactivar($idsat_tasaocuotas);
 		echo $rspta ? "Desactivada" : "No se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$sat_tasaocuotas->activar($idsat_tasaocuotas);
 		echo $rspta ? "Activada" : "No se puede activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$sat_tasaocuotas->mostrar($idsat_tasaocuotas);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$sat_tasaocuotas->listar();
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idsat_tasaocuotas.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idsat_tasaocuotas.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idsat_tasaocuotas.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idsat_tasaocuotas.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->clave,
 				"2"=>$reg->valor_maximo,
 				"3"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "select":
		
		$rspta = $sat_tasaocuotas->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idsat_tasaocuotas . '>'.  $reg->clave ." ".$reg->valor_maximo .  '</option>';
				}
	break;

}
?>