 <?php 
require_once "../modelos/Sat_tipo_de_comprobantes.php";

$sat_tipo_de_comprobantes=new Sat_tipo_de_comprobantes();

$idsat_tipo_de_comprobantes=isset($_POST["idsat_tipo_de_comprobantes"])? limpiarCadena($_POST["idsat_tipo_de_comprobantes"]):"";
$tipo=isset($_POST["tipo"])? limpiarCadena($_POST["tipo"]):"";
$nombre=isset($_POST["nombre"])? limpiarCadena($_POST["nombre"]):"";
$condicion=isset($_POST["condicion"])? limpiarCadena($_POST["condicion"]):"";


switch ($_GET["op"]){
	case 'guardaryeditar':
		if (empty($idsat_tipo_de_comprobantes)){
			$rspta=$sat_tipo_de_comprobantes->insertar($nombre,$tipo);
			echo $rspta ? "Sat Tipo de Comprobante registrada" : "Sat Tipo de Comprobante no se pudo registrar";
		}
		else {
			$rspta=$sat_tipo_de_comprobantes->editar($idsat_tipo_de_comprobantes,$nombre,$tipo);
			echo $rspta ? "Sat Tipo de Comprobante actualizada" : "Sat Tipo de Comprobante no se pudo actualizar";
		}
	break;

	case 'desactivar':
		$rspta=$sat_tipo_de_comprobantes->desactivar($idsat_tipo_de_comprobantes);
 		echo $rspta ? "Sat Tipo de Comprobante Desactivada" : "Sat Tipo de Comprobante no se puede desactivar";
 		break;
	

	case 'activar':
		$rspta=$sat_tipo_de_comprobantes->activar($idsat_tipo_de_comprobantes);
 		echo $rspta ? "Sat Tipo de Comprobante activada" : "Sat Tipo de Comprobante no se puede activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$sat_tipo_de_comprobantes->mostrar($idsat_tipo_de_comprobantes);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$sat_tipo_de_comprobantes->listar();
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idsat_tipo_de_comprobantes.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idsat_tipo_de_comprobantes.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idsat_tipo_de_comprobantes.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idsat_tipo_de_comprobantes.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->tipo,
 				"2"=>$reg->nombre,
 				"3"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "select":
		
		$rspta = $sat_tipo_de_comprobantes->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idsat_tipo_de_comprobantes. '>' . $reg->idsat_tipo_de_comprobantes." ". $reg->nombre . '</option>';
				}
	break;

}
?>