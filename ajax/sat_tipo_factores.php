<?php 
require_once "../modelos/Sat_tipo_factores.php";

$sat_tipo_factores =new Sat_tipo_factores();

$idsat_tipo_factores=isset($_REQUEST["idsat_tipo_factores"])? limpiarCadena($_REQUEST["idsat_tipo_factores"]):"";
$nombre=isset($_REQUEST["nombre"])? limpiarCadena($_REQUEST["nombre"]):"";


switch ($_GET["op"]){

	case 'guardaryeditar':
		if (empty($idsat_tipo_factores)){
			$rspta=$sat_tipo_factores->insertar($nombre);
			echo $rspta ? "Opcion Restringida " : " Opcion Restringida";
		}
		else {
			$rspta=$sat_tipo_factores->editar($idsat_tipo_factores,$nombre);
			echo $rspta ? "Opcion Restringida " : "Opcion Restringida ";
		}
	break;

	case 'desactivar':
		$rspta=$sat_tipo_factores->desactivar($idsat_tipo_factores);
 		echo $rspta ? "Desactivada" : "No se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$sat_tipo_factores->activar($idsat_tipo_factores);
 		echo $rspta ? "Activada" : "No se puede activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$sat_tipo_factores->mostrar($idsat_tipo_factores);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$sat_tipo_factores->listar();
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idsat_tipo_factores.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idsat_tipo_factores.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idsat_tipo_factores.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idsat_tipo_factores.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->idsat_tipo_factores,
 				"2"=>$reg->nombre,
 				"3"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "select":
		
		$rspta = $sat_tipo_factores->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idsat_tipo_factores . '>'.$reg->idsat_tipo_factores . " ". $reg->nombre . '</option>';
				}
	break;

}
?>