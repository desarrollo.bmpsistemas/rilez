<?php 
require_once "../modelos/Sat_tiporelaciones.php";

$sat_tiporelaciones =new Sat_tiporelaciones();

$idsat_tiporelaciones=isset($_POST["idsat_tiporelaciones"])? limpiarCadena($_POST["idsat_tiporelaciones"]):"";
$clave=isset($_POST["clave"])? limpiarCadena($_POST["clave"]):"";
$descripcion=isset($_POST["deascripcion"])? limpiarCadena($_POST["descripcion"]):"";

switch ($_GET["op"]){

	case 'guardaryeditar':
		if (empty($idsat_tiporelaciones)){
			$rspta=$sat_tiporelaciones->insertar($clave);
			echo $rspta ? "Opcion Restringida " : " Opcion Restringida";
		}
		else {
			$rspta=$sat_tiporelaciones->editar($idsat_tiporelaciones,$clave);
			echo $rspta ? "Opcion Restringida " : "Opcion Restringida ";
		}
	break;

	case 'desactivar':
		$rspta=$sat_tiporelaciones->desactivar($idsat_tiporelaciones);
 		echo $rspta ? "Desactivada" : "No se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$sat_tiporelaciones->activar($idsat_tiporelaciones);
 		echo $rspta ? "Activada" : "No se puede activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$sat_tiporelaciones->mostrar($idsat_tiporelaciones);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$sat_tiporelaciones->listar();
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idsat_tiporelaciones.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idsat_tiporelaciones.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idsat_tiporelaciones.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idsat_tiporelaciones.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->clave,
 				"2"=>$reg->nombre,
 				"3"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "select":
		require_once "../modelos/sat_tiporelaciones.php";
		$sat_tiporelaciones = new Sat_tiporelaciones();
		$rspta = $sat_tiporelaciones->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idsat_tiporelaciones . '>'. $reg->clave ." ".$reg->nombre . '</option>';
				}
	break;
	
}
?>