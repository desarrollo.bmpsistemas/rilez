 <?php 
require_once "../modelos/Sat_unidad_medidas.php"; 

$sat_unidad_medidas =new Sat_unidad_medidas();

$idsat_unidad_medidas=isset($_REQUEST["idsat_unidad_medidas"])? limpiarCadena($_REQUEST["idsat_unidad_medidas"]):"";
$clave=isset($_REQUEST["clave"])? limpiarCadena($_REQUEST["clave"]):"";
$nombre=isset($_REQUEST["nombre"])? limpiarCadena($_REQUEST["nombre"]):"";
$Opcion=isset($_REQUEST["Opcion"])? limpiarCadena($_REQUEST["Opcion"]):"";
$referencia=isset($_REQUEST["referencia"])? limpiarCadena($_REQUEST["referencia"]):"";
switch ($_GET["op"]){

	case 'guardaryeditar':
		/*if (empty($idsat_unidad_medidas)){
			$rspta=$sat_unidad_medidas->insertar($clave);
			echo $rspta ? "Opcion Restringida " : " Opcion Restringida";
		}
		else {
			$rspta=$sat_unidad_medidas->editar($idsat_unidad_medidas,$clave);
			echo $rspta ? "Opcion Restringida " : "Opcion Restringida ";
		}*/
	break;

	case 'desactivar':
		$rspta=$sat_unidad_medidas->desactivar($idsat_unidad_medidas);
 		echo $rspta ? "Desactivada" : "No se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$sat_unidad_medidas->activar($idsat_unidad_medidas);
 		echo $rspta ? "Activada" : "No se puede activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$sat_unidad_medidas->mostrar($idsat_unidad_medidas);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;
	
	case 'listar':
		$rspta=$sat_unidad_medidas->listar($Opcion,$referencia);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idsat_unidad_medidas.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idsat_unidad_medidas.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idsat_unidad_medidas.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idsat_unidad_medidas.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->clave,
 				"2"=>substr($reg->nombre,0,100),
 				"3"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "select":
		
		$rspta = $sat_unidad_medidas->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idsat_unidad_medidas . '>'.  $reg->clave ." ". $reg->nombre .  '</option>';
				}
	break;

	case "selectActivos":
		
		$rspta = $sat_unidad_medidas->selectActivos();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idsat_unidad_medidas . '>'. $reg->clave ." ". $reg->nombre . '</option>';
				}
	break;
}
?>