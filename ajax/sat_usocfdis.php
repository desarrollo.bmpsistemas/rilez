 <?php 
require_once "../modelos/Sat_usocfdis.php"; 

$sat_usocfdis =new Sat_usocfdis();

$idsat_usocfdis=isset($_REQUEST["idsat_usocfdis"])? limpiarCadena($_REQUEST["idsat_usocfdis"]):"";
$clave=isset($_REQUEST["clave"])? limpiarCadena($_REQUEST["clave"]):"";
$descripcion=isset($_REQUEST["descripcion"])? limpiarCadena($_REQUEST["descripcion"]):"";

switch ($_GET["op"]){

	case 'guardaryeditar':
		if (empty($idsat_usocfdis)){
			$rspta=$sat_usocfdis->insertar($clave);
			echo $rspta ? "Opcion Restringida " : " Opcion Restringida";
		}
		else {
			$rspta=$sat_usocfdis->editar($idsat_usocfdis,$clave);
			echo $rspta ? "Opcion Restringida " : "Opcion Restringida ";
		}
	break;

	case 'desactivar':
		$rspta=$sat_usocfdis->desactivar($idsat_usocfdis);
 		echo $rspta ? "Desactivada" : "No se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$sat_usocfdis->activar($idsat_usocfdis);
 		echo $rspta ? "Activada" : "No se puede activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$sat_usocfdis->mostrar($idsat_usocfdis);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$sat_usocfdis->listar();
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idsat_usocfdis.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idsat_usocfdis.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idsat_usocfdis.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idsat_usocfdis.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->clave,
 				"2"=>$reg->nombre,
 				"3"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "select":
		
		$rspta = $sat_usocfdis->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idsat_usocfdis . '>'. $reg->clave ." ".$reg->nombre .  '</option>';
				}
	break;
	
}
?>