<?php 
require_once "../modelos/Servicios.php"; 

//ini_set ( 'max_execution_time', 3600);  
$servicios=new Servicios();

$idservicios=isset($_REQUEST["idservicios"])? limpiarCadena($_REQUEST["idservicios"]):"";
$idalmacenes=isset($_REQUEST["idalmacenes"])? limpiarCadena($_REQUEST["idalmacenes"]):"";
$idusuarios=isset($_REQUEST["idusuarios"])? limpiarCadena($_REQUEST["idusuarios"]):"";
$cliente=isset($_REQUEST["cliente"])? limpiarCadena($_REQUEST["cliente"]):"";
$fecha_de_entrada=isset($_REQUEST["fecha_entrada"])? limpiarCadena($_REQUEST["fecha_entrada"]):"";
$fecha_prox_serv=isset($_REQUEST["fecha_prox_serv"])? limpiarCadena($_REQUEST["fecha_prox_serv"]):"";
$imagen=isset($_REQUEST["imagen"])? limpiarCadena($_REQUEST["imagen"]):"";
$servicio_a_realizar=isset($_REQUEST["servicio_a_realizar"])? limpiarCadena($_REQUEST["servicio_a_realizar"]):"";
$servicio_realizado=isset($_REQUEST["servicio_realizado"])? limpiarCadena($_REQUEST["servicio_realizado"]):"";
$importe=isset($_REQUEST["importe"])? limpiarCadena($_REQUEST["importe"]):"";
$fecha_inicial=isset($_REQUEST["fecha_inicial"])? limpiarCadena($_REQUEST["fecha_inicial"]):"";
$fecha_final=isset($_REQUEST["fecha_final"])? limpiarCadena($_REQUEST["fecha_final"]):"";

$codigo=1;

//echo "op ".$_REQUEST["op"];
switch ($_REQUEST["op"]){
	case 'guardaryeditar':

		if (!file_exists($_FILES['imagen']['tmp_name']) || !is_uploaded_file($_FILES['imagen']['tmp_name']))
		{
			$imagen=$_REQUEST["imagenactual"];
		}
		else 
		{
			$ext = explode(".", $_FILES["imagen"]["name"]);
			if ($_FILES['imagen']['type'] == "image/jpg" || $_FILES['imagen']['type'] == "image/jpeg" || $_FILES['imagen']['type'] == "image/png")
			{
				$imagen = $codigo . '.' . end($ext);
				move_uploaded_file($_FILES["imagen"]["tmp_name"], "../files/servicios/" . $imagen);
			}
		}
		if (empty($idservicios)){
			
			$rspta=$servicios->insertar($cliente,$idalmacenes,$idusuarios,$fecha_entrada,$fecha_prox_serv,$servicio_a_realizar,$servicio_realizado,$importe,$imagen);
			echo $rspta ? "Servicio registrado $rspta" : "Servicio no se pudo $rspta";
		}
		/*else {
			
			$rspta=$servicios->editar($idservicios,$servicio_realizado);
			echo $rspta ? "Servicio actualizado " : "Servicio no se pudo actualizar ";
		}*/
	break;
	
	case 'desactivar':
		$rspta=$servicios->desactivar($idservicios);
 		echo $rspta ? "Servicio Desactivado" : "Servicio no se puede desactivar";
	break;

	case 'terminarServicio':
		$rspta=$servicios->editar($idservicios,$servicio_realizado);
			echo $rspta ? "Servicio actualizado " : "Servicio no se pudo actualizar $idservicios,$servicio_realizado";
	break;

	

	case 'activar':
		$rspta=$servicios->activar($idservicios);
 		echo $rspta ? "Servicio activado" : "Servicio no se puede activar";
	break;

	case 'mostrar':
		$rspta=$servicios->mostrar($idservicios);
 		//Codificar el resultado utilizando json
 		//echo "id de servicios $idservicios";
 		echo json_encode($rspta);
	break;

	case 'listar':
		$rspta=$servicios->listar($idalmacenes,$fecha_inicial,$fecha_final);
 		//Vamos a declarar un array
 		//var_dump($rspta);
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idservicios.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idservicios.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idservicios.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idservicios.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->cliente,
 				"2"=>$reg->fecha_de_entrada,
 				"3"=>$reg->fecha_de_salida,
 				"4"=>$reg->fecha_prox_serv,
 				"5"=>$reg->servicio_a_realizar,
 				"6"=>$reg->servicio_realizado,
 				"7"=>($reg->condicion)?'<span class="label bg-green">Terminado</span>':
 				'<span class="label bg-red">Iniciado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;
	case "select":
	
		$rspta=$servicios->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idservicios . '>' .$reg->codigo." ".$reg->nombre . '</option>';
				}
	break;

	case "selectlineas":
		require_once "../modelos/Lineas.php";
		$lineas = new Lineas();

		$rspta = $lineas->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idlineas . '>' . $reg->nombre . '</option>';
				}
	break;
	case "selectsublineas":
		require_once "../modelos/Sublineas.php";
		$sublineas = new Sublineas();

		$rspta = $sublineas->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idsublineas . '>' . $reg->nombre . '</option>';
				}
	break;
	case "listar_sat_unidad_de_medida":

		$rspta = $servicios->listar_sat_unidad_de_medida();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->clave . '>' . $reg->nombre . '</option>';
				}
	break;
	case "listar_sat_prod_y_serv":
		$rspta = $servicios->listar_sat_prod_y_serv();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->clave . '>' . $reg->nombre . '</option>';
				}
	break;

	case "selectcatalogo":
	
		$rspta=$servicios->selectcatalogo();
		echo '<option value=' . '>' . "Selecione Producto". '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idservicios . '>' . $reg->codigo." ".$reg->nombre . '</option>';
				}
	break;

	case "selectivas":
	
		$rspta=$servicios->selectivas();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idivas . '>' .$reg->nombre . '</option>';
				}
	break;
	
	case "selectmonedas":
	
		$rspta=$servicios->selectmonedas();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idmonedas . '>' .$reg->nombre . '</option>';
				}
	break;
	case "selectserviciosporsublinea":
	
		$rspta=$servicios->selectserviciosporsublinea($idsublineas);

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idservicios . '>' .$reg->codigo." ".$reg->nombre . '</option>';
				}
	break;
	

}
?>