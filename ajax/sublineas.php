<?php 
require_once "../modelos/Sublineas.php"; 

$sublineas=new Sublineas();

$idsublineas=isset($_REQUEST["idsublineas"])? limpiarCadena($_REQUEST["idsublineas"]):"";
$idlineas=isset($_REQUEST["idlineas"])? limpiarCadena($_REQUEST["idlineas"]):"";
$nombre=isset($_REQUEST["nombre"])? limpiarCadena($_REQUEST["nombre"]):"";
$Opcion=isset($_REQUEST["Opcion"])? limpiarCadena($_REQUEST["Opcion"]):"";
$referencia=isset($_REQUEST["referencia"])? limpiarCadena($_REQUEST["referencia"]):"";

switch ($_GET["op"]){
	case 'guardaryeditar':
	
		if (empty($idsublineas)){
			$rspta=$sublineas->insertar($idlineas,$nombre);
			echo $rspta ? "Sublinea registrado" : "Sublinea no se pudo registrar $idlineas,$nombre";
		}
		else {
			$rspta=$sublineas->editar($idsublineas,$idlineas,$nombre);
			echo $rspta ? "Sublinea actualizado" : "Sublinea no se pudo actualizar";
		}
	break;

	case 'desactivar':
		$rspta=$sublineas->desactivar($idsublineas);
 		echo $rspta ? "Sublinea Desactivado" : "Sublinea no se puede desactivar";
	break;

	case 'activar':
		$rspta=$sublineas->activar($idsublineas);
 		echo $rspta ? "Sublinea activado" : "Sublinea no se puede activar";
	break;

	case 'mostrar':
		$rspta=$sublineas->mostrar($idsublineas);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
	break;

	case 'listar':
		$rspta=$sublineas->listar($idlineas);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idsublineas.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idsublineas.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idsublineas.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idsublineas.')"><i class="fa fa-check"></i></button>',
 				
 				
 				"1"=>$reg->sublinea,
 				"2"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	
	case "select":
		$rspta = $sublineas->selectsublineas($Opcion,$referencia);

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idsublineas . '>' . $reg->nombre . '</option>';
				}
	break;
	case "selectporlinea":
		$rspta = $sublineas->selectporlinea($idlineas);
		echo '<option>' . "Selecione un valor" . '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idsublineas . '>' . $reg->nombre . '</option>';
				}
	break;

	
}
?>