<?php 
require_once "../modelos/Sucursales.php";  

$sucursales=new Sucursales();

$idpersonas=isset($_REQUEST["idpersonas"])? limpiarCadena($_REQUEST["idpersonas"]):"";
$idsucursales=isset($_REQUEST["idsucursales"])? limpiarCadena($_REQUEST["idsucursales"]):"";
$nombre=isset($_REQUEST["nombre"])? limpiarCadena($_REQUEST["nombre"]):"";
$nombrecomercial=isset($_REQUEST["nombrecomercial"])? limpiarCadena($_REQUEST["nombrecomercial"]):"";
$rfc=isset($_REQUEST["rfc"])? limpiarCadena($_REQUEST["rfc"]):"";
$referencia=isset($_REQUEST["referencia"])? limpiarCadena($_REQUEST["referencia"]):"";
$telefono=isset($_REQUEST["telefono"])? limpiarCadena($_REQUEST["telefono"]):"";
$correo=isset($_REQUEST["correo"])? limpiarCadena($_REQUEST["correo"]):"";
$Opcion=isset($_REQUEST["Opcion"])? limpiarCadena($_REQUEST["Opcion"]):"";


switch ($_GET["op"]){
	case 'guardaryeditar':
		if (empty($idpersonas)){
			$rspta=$sucursales->insertar($nombre,$nombrecomercial,$rfc,$referencia,$telefono,$correo); 
			
			echo $rspta ? "cliente registrado $rspta " : "cliente no se pudo registrar $rspta";
		}
		else {
			$rspta=$sucursales->editar($idpersonas,$nombre,$nombrecomercial,$rfc,$referencia,$telefono,$correo);
			echo $rspta ? "cliente actualizado $rspta" : "cliente no se pudo actualizar $rspta";
		}
	break;

	/*case 'eliminar':
		$rspta=$sucursales->eliminar($idpersonas);
 		echo $rspta ? "cliente Eliminada" : "cliente no se puedo eliminar";
 		break;
	break;
	*/

	case 'desactivar':
		$rspta=$sucursales->desactivar($idpersonas);
 		echo $rspta ? "cliente Desactivada" : "La cliente no se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$sucursales->activar($idpersonas);
 		echo $rspta ? "cliente activada" : "La cliente no se puedo activar";
 		break;
	break;

	

	case 'mostrar':
		$rspta=$sucursales->mostrar($idpersonas);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	

	case 'listar':
		$rspta=$sucursales->listar($Opcion,$referencia);
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){ 			
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idsucursales.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idsucursales.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idsucursales.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idsucursales.')"><i class="fa fa-check"></i></button>',
 				"1"=>substr($reg->nombre,0,30),
 				"2"=>substr($reg->nombrecomercial,0,30),
 				"3"=>$reg->rfc,
 				"4"=>$reg->telefono,
 				"5"=>$reg->correo,
 				"6"=>$reg->condicion?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;
	
	case "select":
		$rspta = $sucursales->select();
		echo '<option value=' . "0" . '>' ." Selecione un Valor ". '</option>';
		while ($reg = $rspta->fetch_object())
		{
			echo '<option value=' . $reg->idsucursales . '>' . $reg->nombre . '</option>';
		}

		
	break;
}
?>