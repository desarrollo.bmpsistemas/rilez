<?php 
session_start();

require_once "../modelos/Usuarios.php";  

$usuarios=new Usuarios();

$idusuarios=isset($_REQUEST["idusuarios"])? limpiarCadena($_REQUEST["idusuarios"]):"";
$idsucursales=isset($_REQUEST["idsucursales"])? limpiarCadena($_REQUEST["idsucursales"]):"";
$nombre=isset($_REQUEST["nombre"])? limpiarCadena($_REQUEST["nombre"]):"";
$curp=isset($_REQUEST["curp"])? limpiarCadena($_REQUEST["curp"]):"";
$fechadeingreso=isset($_REQUEST["fechadeingreso"])? limpiarCadena($_REQUEST["fechadeingreso"]):"";
$fechadebaja=isset($_REQUEST["fechadebaja"])? limpiarCadena($_REQUEST["fechadebaja"]):"";
$fechadereingreso=isset($_REQUEST["fechadereingreso"])? limpiarCadena($_REQUEST["fechadereingreso"]):"";
$telefono=isset($_REQUEST["telefono"])? limpiarCadena($_REQUEST["telefono"]):"";
$correo=isset($_REQUEST["correo"])? limpiarCadena($_REQUEST["correo"]):"";
$sueldo=isset($_REQUEST["sueldo"])? limpiarCadena($_REQUEST["sueldo"]):"";
$sdi=isset($_REQUEST["sdi"])? limpiarCadena($_REQUEST["sdi"]):"";
$imss=isset($_REQUEST["imss"])? limpiarCadena($_REQUEST["imss"]):"";
$iddepartamentos=isset($_REQUEST["iddepartamentos"])? limpiarCadena($_REQUEST["iddepartamentos"]):"";
$idpuestos=isset($_REQUEST["idpuestos"])? limpiarCadena($_REQUEST["idpuestos"]):"";
$idestatus=isset($_REQUEST["idestatus"])? limpiarCadena($_REQUEST["idestatus"]):"";
$login=isset($_REQUEST["login"])? limpiarCadena($_REQUEST["login"]):"";
$clave=isset($_REQUEST["clave"])? limpiarCadena($_REQUEST["clave"]):"";
$imagen=isset($_REQUEST["imagen"])? limpiarCadena($_REQUEST["imagen"]):"";
$Opcion=isset($_REQUEST["Opcion"])? limpiarCadena($_REQUEST["Opcion"]):"";
$referencia=isset($_REQUEST["referencia"])? limpiarCadena($_REQUEST["referencia"]):"";
$descuento=isset($_REQUEST["descuento"])? limpiarCadena($_REQUEST["descuento"]):"";
switch ($_GET["op"]){
	case 'guardaryeditar':

		if (!file_exists($_FILES['imagen']['tmp_name']) || !is_uploaded_file($_FILES['imagen']['tmp_name']))
		{
			$imagen=$_REQUEST["imagenactual"];
		}
		else 
		{
			$ext = explode(".", $_FILES["imagen"]["name"]);
			$imagen=$_FILES['imagen']['name'];	

			if ($_FILES['imagen']['type'] == "image/jpg" || $_FILES['imagen']['type'] == "image/jpeg" || $_FILES['imagen']['type'] == "image/png")
			{
				$imagen = $_FILES['imagen']['name'];// end($ext); //$nombre.end($ext); //
				move_uploaded_file($_FILES["imagen"]["tmp_name"], "../files/usuarios/" . $imagen);
			}
			
		}
		
		// Hash SHA256 en la contraseña
		//$clavehash = password_hash($clave, PASSWORD_BCRYPT);
		$clave=md5($clave); 

		if (empty($idusuarios)){
			$rspta=$usuarios->insertar($nombre,$curp,$imss,$fechadeingreso,$idsucursales,$iddepartamentos,$idpuestos,$idestatus,$telefono,$correo,$sueldo,$sdi,$login,$clave,$imagen,$descuento);
			echo $rspta;

		}
		else {
			
			$rspta=$usuarios->editar($idusuarios,$nombre,$curp,$imss,$fechadeingreso,$idsucursales,$iddepartamentos,$idpuestos,$idestatus,$telefono,$correo,$sueldo,$sdi,$login,$clave,$imagen,$descuento);
			echo $rspta;
		}
	break;

	case 'desactivar':
		$rspta=$usuarios->desactivar($idusuarios);
 		echo $rspta ? "Usuario Desactivado" : "Usuario no se puede desactivar";
 		
	break;

	case 'activar':
		$rspta=$usuarios->activar($idusuarios);
 		echo $rspta ? "Usuario activado" : "Usuario no se puede activar";
 		
	break;

	case 'mostrar':
		$rspta=$usuarios->mostrar($idusuarios); 
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		
	break;
	case 'autorizacion':
		$clavehash=md5($clave); 
		$rspta=$usuarios->autorizacion($login,$clavehash);
 		echo json_encode($rspta);
 		
 	break;
 		
	case 'listar':
		$rspta=$usuarios->listar($Opcion,$referencia );
 		
 		$data= Array();
 		//idusuarios,sucursal,nombre,fechadeingreso,imss,estatus,login,imagen,condicion
 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idusuarios.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idusuarios.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idusuarios.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idusuarios.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->sucursal,
 				"2"=>$reg->nombre,
 				"3"=>$reg->fechadeingreso,
 				"4"=>$reg->imss,
 				"5"=>$reg->estatus,
 				"6"=>$reg->login,
 				"7"=>"<img src='../files/usuarios/".$reg->imagen."' height='50px' width='50px' >",
 				"8"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case 'permisos':
	
		

	break;	

	case 'verificar':
		$logina= $_REQUEST['logina'];  
		$clavea= $_REQUEST['clavea'];

		//hash sha256
		//$clavehash = password_hash($clavea, PASSWORD_BCRYPT);
		$clavehash=md5($clavea); 

		$rspta=$usuarios->verificar($logina,$clavehash);
 		 		
 		$fetch = $rspta->fetch_object();
 		$capturista_id=0;
 		$_SESSION['session']=0;
 		if (isset($fetch))
 		{
 			// se declaran las variables de sesion 
 			
 			$_SESSION['nombre']=$fetch->nombre;
 			$_SESSION['imagen']=$fetch->imagen;
 			$_SESSION['login']=$fetch->login;
 			$_SESSION['rol']=$fetch->rol_id;
 			$_SESSION['almacen_id']=$fetch->almacen_id;
 			$_SESSION['capturista_id']=$fetch->capturista_id;
 			$_SESSION['idusuarios']=$fetch->capturista_id;
			$_SESSION['descuento']=$fetch->descuento;

 		}
 		echo json_encode($fetch); 

	break;

	case 'salir':
	//limpiar las variables de session
	session_unset();
	//destruir la sesion
	session_destroy();
	//rederigir al login
	header("location: ../index.php"); 
	
	break;

	case "select":
		
		

		$rspta = $usuarios->select();
		echo '<option value=' . "0" . '>' ." Selecione un Valor ". '</option>';
		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idusuarios . '>' . $reg->nombre . '</option>';
				}
	break;
	
}
?>