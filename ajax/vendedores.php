<?php 
require_once "../modelos/Vendedores.php";

$vendedores=new Vendedores();

$idvendedores=isset($_POST["idvendedores"])? limpiarCadena($_POST["idvendedores"]):"";
$nombre=isset($_POST["nombre"])? limpiarCadena($_POST["nombre"]):"";


switch ($_GET["op"]){
	case 'guardaryeditar':
		if (empty($idvendedores)){
			$rspta=$vendedores->insertar($nombre);
			echo $rspta ? "Vendedor registrado" : "Vendedor no se pudo registrar";
		}
		else {
			$rspta=$vendedores->editar($idvendedores,$nombre);
			echo $rspta ? "Vendedor actualizado" : "Vendedor no se pudo actualizar";
		}
	break;

	case 'desactivar':
		$rspta=$vendedores->desactivar($idvendedores);
 		echo $rspta ? "Vendedor Desactivado" : "Vendedor no se puede desactivar";
 		break;
	break;

	case 'activar':
		$rspta=$vendedores->activar($idvendedores);
 		echo $rspta ? "Vendedor activado" : "Vendedor no se puede activar";
 		break;
	break;

	case 'mostrar':
		$rspta=$vendedores->mostrar($idvendedores);
 		//Codificar el resultado utilizando json
 		echo json_encode($rspta);
 		break;
	break;

	case 'listar':
		$rspta=$vendedores->listar();
 		//Vamos a declarar un array
 		$data= Array();

 		while ($reg=$rspta->fetch_object()){
 			$data[]=array(
 				"0"=>($reg->condicion)?'<button class="btn btn-warning" onclick="mostrar('.$reg->idvendedores.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-danger" onclick="desactivar('.$reg->idvendedores.')"><i class="fa fa-close"></i></button>':
 					'<button class="btn btn-warning" onclick="mostrar('.$reg->idvendedores.')"><i class="fa fa-pencil"></i></button>'.
 					' <button class="btn btn-primary" onclick="activar('.$reg->idvendedores.')"><i class="fa fa-check"></i></button>',
 				"1"=>$reg->nombre,
 				"2"=>$reg->fecha_de_alta,
 				"3"=>($reg->condicion)?'<span class="label bg-green">Activado</span>':
 				'<span class="label bg-red">Desactivado</span>'
 				);
 		}
 		$results = array(
 			"sEcho"=>1, //Información para el datatables
 			"iTotalRecords"=>count($data), //enviamos el total registros al datatable
 			"iTotalDisplayRecords"=>count($data), //enviamos el total registros a visualizar
 			"aaData"=>$data);
 		echo json_encode($results);

	break;

	case "select":
		require_once "../modelos/Vendedores.php";
		$vendedores = new Vendedores();
		$rspta = $vendedores->select();

		while ($reg = $rspta->fetch_object())
				{
					echo '<option value=' . $reg->idvendedores . '>' . $reg->nombre . '</option>';
				}
	break;
}
?>