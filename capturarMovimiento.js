
function capturarMovimiento(){
    var textoID = $("input#articulo_id").val();
    var textoCantidad = $("input#cantidad").val();
    var textoPrecio = $("input#precio").val();
    var textoDescuento = $("input#descuento").val();
    var textoPedido = $("input#pedido_id2").val();

    if(textoID != "" && textoCantidad>0 && textoPrecio>0 && textoDescuento>=0 && textoPedido>0){
        $.post("../app/CapturarMovimiento.php",{
            id: textoID,
            cantidad: textoCantidad,
            precio: textoPrecio,
            descuento: textoDescuento,
            pedido_id: textoPedido
        },function(mensaje){
            $("#registros").html(mensaje);
            $("input#articulo_id").val("");
            $("input#cantidad").val("");
            $("input#precio").val("");
            $("input#descuento").val("0");
            $("input#descripcion").val("");
        });
    }else{
        $('.toast-container').remove();
        $().toastmessage('showWarningToast', "Debe completar todos los campos");
    }
} 

function limpiarInputCantidad()
{
          

        
}
    
    
            
        

function eliminarRegistro(){
        $(".boton").click(function(){
        
        var id=0;
 		var i = 0;
        
            // Obtenemos todos los valores contenidos en los <td> de la fila
            // seleccionada
            $(this).parents("tr").find("td").each(function(){
            	i++;
                //alert("resultado "+ $(this).html());

            	if(i==1) {
            		id=($(this).html());
            	}
                
            });
            var id_mov = parseInt(id);
            var Cantidad = $("input#cantidad"+id_mov).val();

            Cantidad = parseInt(Cantidad);
            //alert("id "+ id+" cantidad "+Cantidad);
            
            var textoPedido = $("input#pedido_id2").val();
            //console.log(id +" "+ id_mov+" "+textoPedido+" cantidad "+Cantidad);

            if(id_mov > 0){
		        $.post("../app/eliminarMovPedido.php",{
		            id: id_mov,
		            pedido_id: textoPedido,
                    cantidad: Cantidad
		        },function(mensaje){
		            $("#registros").html(mensaje);
                    $('.toast-container').remove();
                    //$().toastmessage('showWarningToast', "El articulo ha sido procesado ... ");
		        });
		    };

        });
};


function limpiarDatos(){
    $("input#articulo_id").val("");
    $("input#cantidad").val("");
    $("input#precio").val("");
    $("input#descuento").val("0");
    $("input#descripcion").val("");
    var dataList = $("#lista_descripciones");
    dataList.empty();
}


function elegirValor(el){
    var valor = $('option:selected', el).attr('parametro');
    console.log("valor selecionado "+valor);   
    //alert("entro "+valor);
}


function ultimasDosVentas(){
    var clienteid = $("input#cliente_id").val();
    var pedidoid = $("input#pedido_id2").val();
    //alert("cliente id "+clienteid);
    if(clienteid>0)
    {
        $.post("../app/DetUltimasDosVentas.php",{
            cliente_id: clienteid,
            pedido_id: pedidoid
            
        },function(mensaje){
            $("#registros").html(mensaje);
            
        });
    }
    else
    {
        $('.toast-container').remove();
        $().toastmessage('showWarningToast', "Verifique sus movimientos ");
    }
}

