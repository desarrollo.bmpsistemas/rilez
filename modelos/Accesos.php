<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Accesos
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	//$consulta= ejecutarConsulta($sql);
	//while($rs = mysqli_fetch_array($consulta)) 
	//$rol= $rs['roles_id'];

	public function insertar($idmodulos,$idusuarios)
	{
		$sql="INSERT INTO accesos (modulo_id,usuario_id,condicion)
		VALUES ('$idmodulos','$idusuarios','1')";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idaccesos,$idmudulos,$idusuarios)
	{
		$sql="UPDATE accesos SET idmodulos='$idmodulos',idusuarios='$idusuarios' WHERE idaccesos='$idaccesos'";
		return ejecutarConsulta($sql); 
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idaccesos)
	{
		//$sql="UPDATE accesos SET condicion='0' WHERE idaccesos='$idaccesos'";
		$sql="DELETE from  accesos  WHERE idaccesos='$idaccesos'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idaccesos)
	{
		$sql="UPDATE accesos SET condicion='1' WHERE idaccesos='$idaccesos'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idaccesos)
	{
		$sql="SELECT accesos.idaccesos,modulos.idmodulos,usuarios.idusuarios, modulos.nombre AS modulo,usuarios.nombre AS usuario,accesos.condicion FROM accesos
		INNER JOIN modulos ON modulos.idmodulos = accesos.idmodulos
		INNER JOIN usuarios ON usuarios.idusuarios=accesos.idusuarios		
		where idaccesos=$idaccesos ";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar($idusuarios)
	{
		$sql="SELECT idaccesos,modulo.nombre AS modulo,capturistas.descripcion AS  usuario,accesos.condicion FROM accesos
		INNER JOIN modulo ON modulo.id = accesos.modulo_id
		INNER JOIN capturistas ON capturistas.id=accesos.usuario_id 
		WHERE accesos.usuario_id='$idusuarios' and modulo.condicion=1
		ORDER BY modulo.nombre,capturistas.descripcion ";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select()
	{
		$sql="SELECT idaccesos,modulos.nombre AS modulo,usuarios.nombre AS usuario,accesos.condicion FROM accesos
		INNER JOIN modulos ON modulos.idmodulos = accesos.idmodulos
		INNER JOIN usuarios ON usuarios.idusuarios=accesos.idusuarios where condicion=1";
		return ejecutarConsulta($sql);		
	}
}

?>