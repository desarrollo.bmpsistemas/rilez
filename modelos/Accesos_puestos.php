<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Accesos_puestos
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($idpuestos,$idmodulos)
	{
		$sql="INSERT INTO accesos_puestos (idpuestos,idmodulos,condicion)
		VALUES ('$idpuestos','$idmodulos','1')";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idaccesos_puestos,$idpuestos,$idmodulos)
	{
		$sql="UPDATE accesos_puestos SET idpuestos='$idpuestos',idmodulos='$idmodulos' WHERE idaccesos_puestos='$idaccesos_puestos' ";
		return ejecutarConsulta($sql);
		
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idaccesos_puestos)
	{
		$sql="UPDATE accesos_puestos SET condicion='0' WHERE idaccesos_puestos='$idaccesos_puestos'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idaccesos_puestos)
	{
		$sql="UPDATE accesos_puestos SET condicion='1' WHERE idaccesos_puestos='$idaccesos_puestos'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idaccesos_puestos)
	{
		$sql="SELECT idaccesos_puestos,idpuestos,idmodulos FROM accesos_puestos WHERE idaccesos_puestos='$idaccesos_puestos'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar($idpuestos)
	{
		$sql="SELECT accesos_puestos.idaccesos_puestos,modulos.nombre AS modulo,accesos_puestos.condicion FROM accesos_puestos
		INNER JOIN modulos ON modulos.idmodulos=accesos_puestos.idmodulos 
		WHERE accesos_puestos.idpuestos='$idpuestos'
		ORDER BY modulos.nombre			";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select()
	{
		$sql="SELECT idaccesos_puestos FROM accesos_puestos where condicion=1";
		return ejecutarConsulta($sql);		
	}
}

?>