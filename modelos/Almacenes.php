<?php 
//Incluímos inicialmente la conexión a la base de datos 
require "../config/Conexion.php";

Class Almacenes
{
	//Implementamos nuestro constructor 
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($idsucursales,$nombre,$idmonedas,$idestatus,$tope)
	{
		$sql="INSERT INTO almacenes (sucursal_id,nombre,moneda_id,estatus_id,tope,condicion)
		VALUES ('$idsucursales','$nombre','$idmonedas','$idestatus','$tope','1')";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idalmacenes,$idsucursales,$nombre,$idmonedas,$idestatus,$tope)
	{
		$sql="UPDATE almacenes SET nombre='$nombre',sucursal_id='$idsucursales',moneda_id='$idmonedas',tope='$tope',idestatus='$idestatus' WHERE id='$idalmacenes' ";
		return ejecutarConsulta($sql);
		
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idalmacenes)
	{
		$sql="UPDATE almacenes SET condicion='0' WHERE id='$idalmacenes'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idalmacenes)
	{
		$sql="UPDATE almacenes SET condicion='1' WHERE id='$idalmacenes'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idalmacenes)
	{
		$sql="SELECT sucursales.id AS idsucursales,sucursales.nomComercial AS sucursal,almacenes.id AS idalmacenes,
		almacenes.nombre AS almacen,monedas.id AS idmonedas,monedas.descripcion AS moneda,almacenes.condicion,
		almacenes.tope,estatus_id FROM almacenes
		INNER JOIN sucursales ON sucursales.id=almacenes.sucursal_id 
		INNER JOIN monedas ON monedas.id=almacenes.moneda_id
			 WHERE almacenes.id='$idalmacenes' ";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar()
	{
		$sql="SELECT sucursales.id AS idsucursales,sucursales.nomComercial AS  sucursal,almacenes.id AS idalmacenes,almacenes.nombre AS almacen,
			monedas.descripcion AS moneda,almacenes.condicion,almacenes.tope,estatus.descripcion as estatus FROM almacenes
			INNER JOIN sucursales ON sucursales.id=almacenes.sucursal_id 
			INNER JOIN monedas ON monedas.id=almacenes.moneda_id
			INNER JOIN estatus ON estatus.id=almacenes.estatus_id
			ORDER BY almacenes.nombre";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select()
	{
		$sql="SELECT id as idalmacenes,nombre FROM almacenes where condicion=1";
		return ejecutarConsulta($sql);		
	}
	public function selectAlmacenesPorUsuario($idusuarios)
	{
		$sql="SELECT almacenes.id as idalmacenes,almacenes.nombre FROM accesos_suc_alm 
			INNER JOIN almacenes ON almacenes.id=accesos_suc_alm.almacen_id
			WHERE usuario_id=$idusuarios and condicion=1";
			return ejecutarConsulta($sql);		
	}
	
	public function agregarAlmacenAlUsuario($idalmacenes,$idusuarios)
	{
		$sql="INSERT INTO accesos_suc_alm  (almacen_id,usuario_id) values ($idalmacenes,$idusuarios)";
		return ejecutarConsulta($sql);		
	}

	public function quitarAlmacenAlUsuario($idusuarios,$idalmacenes)
	{
		$sql="DELETE FROM accesos_suc_alm  where almacen_id=$idalmacenes and  usuario_id=$idusuarios";
		return ejecutarConsulta($sql);		
	}
	
	
}

?>