<?php 
//Incluímos inicialmente la conexión a la base de datos 
require "../config/Conexion.php";
ini_set ( 'max_execution_time', 3600);  
Class Articulos
{
	//Implementamos nuestro constructor    
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($idsublineas,$codigo,$nombre,$idsat_unidad_medidas,$idsat_prod_servs,$producible,$vendible,$cambio_precio,$inventariable,$peso,$volumen,$imagen,$idestatus,$condicion)
	{
		if ($imagen=='DEFAULT.JPG')
		{
			//buscar id de sat unidad de medida
			/*$sql="SELECT id from sat_unidad_medidas WHERE clave='$idsat_unidad_medidas'";
			$rs= ejecutarConsulta($sql);
			while($fila = mysqli_fetch_assoc( $rs ) )
	      	$sat_unidad_medida_id= $fila['id'];
	        //buscar id de sat prod y serv
	      	$sql="SELECT id from sat_prod_serv WHERE clave='$sat_prod_serv_id' ";
			$rs= ejecutarConsulta($sql);
			while($fila = mysqli_fetch_assoc( $rs ) )
	      	$sat_prod_serv_id= $fila['id'];*/
	    }
		if (empty($idestatus)) $idestatus=1;		
		$sql="INSERT INTO articulos (sublinea_id,id2,     descripcion,   sat_unidad_medida_id,   sat_prod_serv_id,producible ,  vendible,cambio_precio,inventariable,       peso,volumen,      imagen,  idestatus,condicion)
						  VALUES ('$idsublineas','$codigo','$nombre','$idsat_unidad_medidas','$idsat_prod_servs','$producible','$vendible','$cambio_precio','$inventariable','$peso','$volumen','$imagen','$idestatus',1)";
		echo "$sql";				  
		$id=ejecutarConsulta_retornarID($sql);
		if ($id>0)
			return $id;
		else
			return $sql;
	}

	//Implementamos un método para editar registros
	public function editar($idarticulos,$idsublineas,$codigo,$nombre,$idsat_unidad_medidas,$idsat_prod_servs,$producible,$vendible,$cambio_precio,$inventariable,$peso,$volumen,$imagen,$idestatus)
	{
		$sql="UPDATE articulos SET 
			 sublinea_id='$idsublineas',
             id2='$codigo',
             descripcion='$nombre',
             sat_unidad_medida_id='$idsat_unidad_medidas',
             sat_prod_serv_id='$idsat_prod_servs',
             producible='$producible',
             vendible='$vendible',
             cambio_precio='$cambio_precio',
             inventariable='$inventariable',
             peso='$peso',
             volumen='$volumen',
             imagen='$imagen',
             idestatus='$idestatus' 
             where id=$idarticulos ";
		return ejecutarConsulta($sql);
	}
	public function listarTemporal($archivo)
	{
		$sql="SELECT idtemporal_xml,NoIdentificacion,ClaveUnidad,ClaveProdServ,Unidad,Descripcion,ValorUnitario,condicion from temporal_xml where nombre='$archivo' ";
		return ejecutarConsulta($sql);
		
	}
		
		
	//Implementamos un método para desactivar registros
	public function desactivar($idarticulos)
	{
		$sql="UPDATE articulos SET condicion=0 WHERE id=$idarticulos";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar registros
	public function activar($idarticulos)
	{
		$sql="UPDATE articulos SET condicion=1 WHERE id=$idarticulos";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idarticulos)
	{
		$sql="SELECT lineas.id AS idlineas,sublineas.id AS idsublineas,articulos.id2 AS codigo,articulos.descripcion AS nombre,sat_unidad_medida_id,
		sat_prod_serv_id,articulos.producible,articulos.vendible,articulos.cambio_precio,articulos.inventariable,articulos.peso,
		articulos.volumen,articulos.imagen,articulos.condicion,articulos.idestatus FROM articulos 
		INNER JOIN sublineas ON sublineas.id=articulos.sublinea_id
		INNER JOIN lineas ON lineas.id=sublineas.linea_id WHERE articulos.id=$idarticulos";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar($Opcion,$referencia)
	{
		/*SELECT lineas.id AS idlineas,sublineas.id AS idsublineas,articulos.id AS idarticulos
		,lineas.descripcion AS linea,sublineas.descripcion AS sublinea,articulos.descripcion AS articulo
		,articulos.id2 AS codigo,articulos.vendible,articulos.condicion FROM articulos
		INNER JOIN sublineas  ON articulos.sublinea_id = sublineas.id
		INNER JOIN lineas     ON sublineas.linea_id = lineas.id*/

		if (strlen($referencia)<1)
			$referencia="BMI";
		if ($Opcion==0)
			$q=" articulos.id2 like '%$referencia%' ";
		if ($Opcion==1)
			$q=" articulos.id2 like '%$referencia%' ";
		if ($Opcion==2)
			$q=" articulos.descripcion like '%$referencia%' ";
		if ($Opcion==3)
			$q=" articulos.sublinea_id = '$referencia' ";
		$sql="SELECT articulos.id AS idarticulos,articulos.descripcion AS articulo,articulos.id2 AS codigo,articulos.producible,articulos.vendible,
		articulos.inventariable,articulos.cambio_precio,articulos.condicion FROM articulos	where $q ";
		//echo "$sql";
		return ejecutarConsulta($sql);		
	}

	//Implementar un método para listar los registros ACTIVOS
	public function listarActivos()
	{
		$sql="SELECT idarticulos,articulos.codigo,articulos.nombre AS articulo,lineas.nombre AS linea,sublineas.nombre AS sublinea FROM articulos  
		INNER JOIN sublineas ON sublineas.idsublineas =articulos.idsublineas
		INNER JOIN lineas ON lineas.idlineas=sublineas.idlineas WHERE articulos.condicion=1 ORDER BY lineas.nombre,sublineas.nombre  ";
		return ejecutarConsulta($sql);		
	}
	
	
	public function listarActivosPorOrden($orden)
	{
		if (!isset($orden) || empty($orden))
			$orden=" ORDER BY articulos.idarticulos ";
		if (isset($orden) && $orden=='nombre')
			$orden=" ORDER BY articulos.nombre ";
		if (isset($orden) && $orden=='codigo')
			$orden=" ORDER BY articulos.codigo ";
		if (isset($orden) && $orden=='linea')
			$orden=" ORDER BY lineas.nombre ";

		$sql="SELECT idarticulos,articulos.codigo,articulos.nombre AS articulo,lineas.nombre AS linea,sublineas.nombre AS sublinea FROM articulos  
		INNER JOIN sublineas ON sublineas.idsublineas =articulos.idsublineas
		INNER JOIN lineas ON lineas.idlineas=sublineas.idlineas WHERE articulos.condicion=1 $orden  ";
		
		return ejecutarConsulta($sql);		
	}
	
	public function selectarticulosporsublinea($idsublineas)
	{
		$sql="SELECT id as idarticulos,id2 as codigo,descripcion as nombre FROM articulos WHERE sublinea_id=$idsublineas AND condicion=1";
		return ejecutarConsulta($sql);		
	}
	public function selectcatalogo()
	{
		$sql="SELECT id as idarticulos,id2 as codigo,descripcion as nombre FROM articulos where condicion=1";
		return ejecutarConsulta($sql);		
	}
	public function selectivas()
	{
		$sql="SELECT id as idivas,descripcion as nombre FROM ivas where condicion=1 order by nombre";
		return ejecutarConsulta($sql);		
	}
	public function selectmonedas()
	{
		$sql="SELECT id as idmonedas,descripcion as nombre FROM monedas where condicion=1";
		return ejecutarConsulta($sql);		
	}
	public function select($Opcion,$referencia,$Limite)
	{
		//echo "ocpion ".$Opcion." ref ".$referencia;
		if ($Opcion==0 && strlen($referencia)<3)
		{
			$referencia="   ";
			$q= " and articulos.id2 = '%$referencia%' or like '%$referencia%' ";
		}
		else
		if ($Opcion==1 && strlen($referencia)>2)
		{
			$q= " and articulos.id2 like '%$referencia%' ";
		}
		else
		if ($Opcion==2 && strlen($referencia)>2)
		{
			$q= " and articulos.descripcion like '%$referencia%' ";
		}
		if ($Opcion==3 )
		{
			$q= " and articulos.id = '$referencia' ";
		}
		if ($Opcion==1 && $Limite==1)
		{
			$q= " and articulos.id2 = '$referencia' ";
		}

		$sql="SELECT id as idarticulos,id2 as codigo,descripcion as nombre FROM articulos where condicion=1 $q ";
		//echo "$sql";
		return ejecutarConsulta($sql);		
	}
	public function selectProductos($idalmacenes,$referencia,$opc)
	{	
		$q ="";
		if ($opc==0) // por sublinea
			$q =" articulos.condicion=1 order by articulos.id desc limit 1";

		if ($opc==1) // por sublinea
			$q =" articulos.sublinea_id = $referencia AND articulos.condicion=1";
		if ($opc==2) // por codigo
			$q =" articulos.id2 like '%$referencia%' AND articulos.condicion=1";
		if ($opc==3) // por codigo
			$q =" articulos.descripcion like '%$referencia%' AND articulos.condicion=1";
		
		$sql="SELECT articulos.id as idarticulos,articulos.id2 AS codigo,articulos.descripcion AS nombre FROM articulos
		WHERE   $q ";
		
		//$sql="SELECT articulos.id as idarticulos,articulos.id2 AS codigo,articulos.descripcion AS nombre FROM articulos
		//INNER JOIN articulos_almacenes ON articulos_almacenes.articulo_id=articulos.id
		//WHERE   $q ";
			
		
		//echo "$sql";
		return ejecutarConsulta($sql);		
	}
}

?>