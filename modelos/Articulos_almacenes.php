<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Articulos_almacenes
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($idalmacenes,$idarticulos,$stock,$idmonedas,$costo,$precio,$iva,$porcentaje_utilidad,$afectacion,$ubicacion)
	{
		//buscar valor del iva por su id
		//////////////////////////
		
	    //obtener el valor del dolar
	    $r=0;
	    $valor_del_dolar=1;
	    $sql="SELECT valor FROM detalles_monedas ORDER BY id desc 	LIMIT 1 ";
		$rs= ejecutarConsulta($sql);
		while($fila = mysqli_fetch_assoc( $rs ) )
	    $valor_del_dolar= $fila['valor'];
		
	    
  			
		//leer iva y moneda del almacen actual
		if ($afectacion=='A')	
		    $sqla="SELECT id AS idalmacenes,moneda_id FROM almacenes where id='$idalmacenes' ";
		if ($afectacion=='P')	
		    $sqla="SELECT id AS idalmacenes,moneda_id FROM almacenes WHERE moneda_id=1 AND estatus_id=6";
		if ($afectacion=='D')	
		    $sqla="SELECT id AS idalmacenes,moneda_id FROM almacenes WHERE moneda_id=2 AND estatus_id=6";
		if ($afectacion=='T')	
		    $sqla="SELECT id AS idalmacenes,moneda_id FROM almacenes WHERE estatus_id=6";
		$rs= ejecutarConsulta($sqla);
		while($fila = mysqli_fetch_assoc( $rs ) )
		{
			$IDmonedas= $fila['moneda_id'];	
			$entro="N";
			if ($idmonedas==1 && $IDmonedas>1 && ($costo>0 && $valor_del_dolar>0))
			{
				$Costo=round(($costo/$valor_del_dolar),2);
				$Precio=round(($precio/$valor_del_dolar),2);
				$entro="S";
			}
			if ($idmonedas==2 && $IDmonedas==1)
			{
				$Costo=round(($costo*$valor_del_dolar),2);
				$Precio=round(($precio*$valor_del_dolar),2);
				$entro="S";
			}
			if ($idmonedas == $IDmonedas || $entro=='N')
			{
				$Costo=$costo;
				$Precio=$precio;
				$entro="S";
			}
			
			$idalmacenes= $fila['idalmacenes'];	
			//$Precio=$Costo*$porcentaje_utilidad;
				$sql="INSERT INTO articulos_almacenes (almacen_id,articulo_id,stock_minimo,moneda_id,costo,precio,iva,porcentaje_utilidad,ubicacion,condicion)
									   VALUES ('$idalmacenes','$idarticulos','$stock','$IDmonedas','$Costo','$Precio','$iva','$porcentaje_utilidad','$ubicacion','1')";
			$r= ejecutarConsulta($sql);
		}

		
		return $r; //$sqla.$sql
		
		
	}

	//Implementamos un método para editar registros
	public function editar($idarticulos_almacenes,$idarticulos,$idalmacenes,$stock,$idmonedas,$costo,$precio,$iva,$porcentaje_utilidad,$afectacion,$ubicacion)
	{
		//buscar valor del iva por su id
		
		$sqla="nada";
		//$sql="";
		$r=0;
  		
			//leer iva y moneda del almacen actual
		if ($afectacion=='A')	
		    $sqla="SELECT id AS idalmacenes,moneda_id FROM almacenes where id='$idalmacenes' ";
		if ($afectacion=='P')	
		    $sqla="SELECT id AS idalmacenes,moneda_id FROM almacenes WHERE moneda_id=1 AND estatus_id=6";
		if ($afectacion=='D')	
		    $sqla="SELECT id AS idalmacenes,moneda_id FROM almacenes WHERE moneda_id=2 AND estatus_id=6";
		if ($afectacion=='T')	
		    $sqla="SELECT id AS idalmacenes,moneda_id FROM almacenes WHERE estatus_id=6";
		//echo "consulta $sqla";
		$rs= ejecutarConsulta($sqla);
		while($fila = mysqli_fetch_assoc( $rs ) )
		{
			
			$IDmonedas= $fila['moneda_id'];	
			$idalmacenes= $fila['idalmacenes'];	
			$entro="N";
			if ($idmonedas==1 && $IDmonedas>1 && ($costo>0 && $valor_del_dolar>0))
			{
				$Costo=round(($costo/$valor_del_dolar),2);
				$Precio=round(($precio/$valor_del_dolar),2);
				$entro="S";
			}
			if ($idmonedas==2 && $IDmonedas==1)
			{
				$Costo=round(($costo*$valor_del_dolar),2);
				$Precio=round(($precio*$valor_del_dolar),2);
				$entro="S";
			}
			if ($idmonedas == $IDmonedas || $entro=='N')
			{
				$Costo=$costo;
				$Precio=$precio;
				$entro="S";
			}
			

			//$precio=$costo*$porcentaje_utilidad;
			$sql="UPDATE articulos_almacenes SET 
			stock_minimo='$stock',moneda_id='$idmonedas',ubicacion='$ubicacion',
			costo='$costo',precio='$precio',iva='$iva',porcentaje_utilidad='$porcentaje_utilidad'
			WHERE almacen_id=$idalmacenes and articulo_id='$idarticulos' ";
			$r= ejecutarConsulta($sql);
		}
	  
  		
  		
  		return  $r;

		//////////////////////////
		
		//////////////////////////
			
		
		
	}
	public function buscarArticuloEnAlmacen($idalmacenes,$idarticulos)
	{
		$sql="SELECT id as idarticulos_almacenes FROM articulos_almacenes WHERE almacen_id='$idalmacenes' and  articulo_id ='$idarticulos' ";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idarticulos_almacenes)
	{
		$sql="UPDATE articulos_almacenes SET condicion='0' WHERE id='$idarticulos_almacenes' ";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idarticulos_almacenes)
	{
		$sql="UPDATE articulos_almacenes SET condicion='1' WHERE id='$idarticulos_almacenes' ";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idarticulos_almacenes)
	{
		$sql="SELECT articulos_almacenes.id AS idarticulos_almacenes,almacen_id AS idalmacenes,articulo_id AS idarticulos,
		stock_minimo AS stock,moneda_id AS idmonedas,costo,precio,iva,porcentaje_utilidad,ubicacion,articulos_almacenes.condicion FROM articulos_almacenes
		WHERE articulos_almacenes.id='$idarticulos_almacenes' ";
		return ejecutarConsultaSimpleFila($sql);
	}
	public function mostrar2($idalmacenes,$idarticulos)
	{
		$sql="SELECT articulos_almacenes.id AS idarticulos_almacenes,almacen_id AS idalmacenes,articulo_id AS idarticulos,
		stock_minimo AS stock,moneda_id AS idmonedas,costo,precio,articulos_almacenes.iva,porcentaje_utilidad,ubicacion,articulos_almacenes.condicion FROM articulos_almacenes
		WHERE almacen_id=$idalmacenes and  articulo_id='$idarticulos' ";
		//echo "$sql";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar($codigo)
	{
		if ( !isset($codigo) || empty($codigo)) $codigo="0";
		$sql="SELECT idarticulos_almacenes,almacenes.nombre AS almacen,
		monedas.nombre AS moneda,articulos_almacenes.costo,articulos_almacenes.porcentaje_utilidad AS porcentaje_utilidad,articulos_almacenes.precio,articulos_almacenes.condicion FROM articulos
		INNER JOIN articulos_almacenes ON articulos_almacenes.idarticulos=articulos.idarticulos
		INNER JOIN monedas ON monedas.idmonedas=articulos_almacenes.idmonedas
		INNER JOIN almacenes ON almacenes.idalmacenes=articulos_almacenes.idalmacenes
		WHERE articulos.condicion=1 AND articulos.idarticulos='$codigo'
 		ORDER BY articulos_almacenes.idalmacenes  ";
		return ejecutarConsulta($sql);		
	}
	public function leerTemporalXml($archivo)
	{
		$sql="SELECT articulos.idarticulos,ValorUnitario as costo FROM temporal_xml 
		INNER JOIN articulos ON articulos.codigo=temporal_xml.NoIdentificacion
		WHERE temporal_xml.nombre='$archivo' ";
		return ejecutarConsulta($sql);
	}
	public function leerporcentaje_utilidad($costo)
	{
		$sql="SELECT porciento FROM porcentaje_utilidades
		WHERE $costo>=lim_inf AND $costo<=lim_sup ";
		return ejecutarConsulta($sql);			      
	}
	public function leerIva_y_Moneda($idalmacenes)
	{
		$sql="SELECT idivas,idmonedas FROM almacenes WHERE idalmacenes=$idalmacenes ";
		return ejecutarConsultaSimpleFila($sql);			      
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select()
	{
		$sql="SELECT idalmacenes,nombre FROM almacenes where condicion=1";
		return ejecutarConsulta($sql);		
	}
	
}

?>