<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Articulos_proveedores
{
	//Implementamos nuestro constructor
	public function __construct() 
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($idpersonas,$idarticulos,$costo,$NoIdentificacion)
	{
		  	
	    $sql="INSERT INTO articulos_proveedores (proveedor_id,articulo_id,costo,articulo_prov,condicion)
										   VALUES ('$idpersonas','$idarticulos','$costo','$NoIdentificacion','1') ";
		return ejecutarConsulta($sql);
	}
	
	//Implementamos un método para editar registros $idarticulos_proveedores,$costo,$NoIdentificacion
	public function editar($idarticulos_proveedores,$costo,$NoIdentificacion)
	{
		  	
      	$sql="UPDATE articulos_proveedores SET 	costo='$costo',articulo_prov='$NoIdentificacion'
		WHERE idap='$idarticulos_proveedores' ";
		return ejecutarConsulta($sql);
	}	
		
	

	//Implementamos un método para desactivar categorías
	public function buscarArtEnProv($idpersonas,$idarticulos)
	{
		$sql="SELECT idap as idarticulos_proveedores from  articulos_proveedores  
		WHERE proveedor_id='$idpersonas' and articulo_id= '$idarticulos' ";
		return ejecutarConsulta($sql);
	}

	public function desactivar($idarticulos_proveedores)
	{
		$sql="UPDATE articulos_proveedores SET condicion='0' WHERE idarticulos_proveedores='$idarticulos_proveedores' ";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idarticulos_proveedores)
	{
		$sql="UPDATE articulos_proveedores SET condicion='1' WHERE idarticulos_proveedores='$idarticulos_proveedores' ";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idpersonas,$idarticulos)
	{
		
		$sql="SELECT costo,articulo_prov from articulos_proveedores  WHERE proveedor_id=$idpersonas and articulo_id='$idarticulos' ";
		return ejecutarConsultaSimpleFila($sql);
	}
	public function buscarDatos($idpersonas,$idarticulos)
	{
		
		$sql="SELECT idap as idarticulos_proveedores,articulos_proveedores.costo,
		articulos_proveedores.articulo_prov as NoIdentificacion FROM articulos_proveedores
		WHERE proveedor_id='$idpersonas' AND articulo_id='$idarticulos' ";	
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar($idpersonas)
	{
		
		$sql="SELECT idarticulos_proveedores,articulos.nombre AS articulo,articulos_proveedores.NoIdentificacion,
		articulos_proveedores.costo,articulos_proveedores.condicion FROM articulos_proveedores
		INNER JOIN personas ON personas.idpersonas=articulos_proveedores.idpersonas
		INNER JOIN articulos ON articulos.idarticulos=articulos_proveedores.idarticulos
		WHERE articulos_proveedores.condicion=1 AND personas.tipodepersona='Proveedor' AND articulos_proveedores.idpersonas='$idpersonas' ";
		return ejecutarConsulta($sql);		
	}
	
	public function listarProdPorProveedor($idpersonas)
	{
		
		$sql="SELECT articulos_proveedores.articulo_id AS idarticulos,articulos.descripcion AS nombre
		FROM articulos_proveedores
		INNER JOIN articulos ON articulos.id=articulos_proveedores.articulo_id
		WHERE articulos_proveedores.condicion=1 AND  articulos_proveedores.proveedor_id=$idpersonas ";
		//echo "$sql";
		return ejecutarConsulta($sql);	

	}
	
	
	//Implementar un método para listar los registros y mostrar en el select
	//public function select()
	//{
	//	$sql="SELECT idalmacenes,nombre FROM almacenes where condicion=1";
	//	return ejecutarConsulta($sql);		
	//}
	
}

?>