<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Calles
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function guardar($Opcion,$idcalles,$nombre)
	{
		if ($Opcion=="I")
			$sql="INSERT INTO calles (descripcion,condicion) VALUES ('$nombre','1')";
		if ($Opcion=='U')
			$sql="UPDATE calles SET descripcion='$nombre' WHERE id='$idcalles'";
		if ($Opcion=='D')
			$sql="DELETE FROM  calles  WHERE id='$idcalles'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idcalles,$nombre)
	{
		$sql="UPDATE calles SET descripcion='$nombre' WHERE id='$idcalles'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idcalles)
	{
		$sql="UPDATE calles SET condicion='0' WHERE id='$idcalles'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idcalles)
	{
		$sql="UPDATE calles SET condicion='1' WHERE id='$idcalles'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idcalles)
	{
		$sql="SELECT id as idcalles,descripcion as nombre FROM calles WHERE id='$idcalles'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar($nombre)
	{
		if (strlen($nombre)<3)
		{
			$nombre="XXX";
		}
		
		$q= " where descripcion like '%$nombre%' ";
		

		$sql="SELECT id as idcalles,descripcion as nombre,condicion FROM calles  $q";
		
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select($nombre)
	{
		$sql="SELECT id as idcalles,descripcion as nombre FROM calles where condicion=1 and descripcion like '%$nombre%'";
		return ejecutarConsulta($sql);		
	}
	public function selectporid($idcalles)
	{
		$sql="SELECT id as idcalles,descripcion as nombre  FROM calles where id=$idcalles ";
		
		return ejecutarConsulta($sql);		
	}
}

?>