<?php 
//Incluímos inicialmente la conexión a la base de datos 
require "../config/Conexion.php";

Class Cat_de_gastos
{
	//Implementamos nuestro constructor 
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($nombre,$idgrupo_de_gastos)
	{
		$sql="INSERT INTO cat_de_gastos (descripcion,grupo_de_gastos_id,condicion)
		VALUES ('$nombre','$idgrupo_de_gastos','1')";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idcat_de_gastos,$nombre,$idgrupo_de_gastos)
	{
		$sql="UPDATE cat_de_gastos SET descripcion='$nombre',grupo_de_gastos_id='$idgrupo_de_gastos' WHERE id='$idcat_de_gastos'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idcat_de_gastos)
	{
		$sql="UPDATE cat_de_gastos SET condicion='0' WHERE id='$idcat_de_gastos'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idcat_de_gastos)
	{
		$sql="UPDATE cat_de_gastos SET condicion='1' WHERE id='$idcat_de_gastos'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idcat_de_gastos)
	{
		$sql="SELECT id as idcat_de_gastos,descripcion as nombre  FROM cat_de_gastos WHERE id='$idcat_de_gastos'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar($nombre,$Opcion)
	{
		if ($Opcion==0)
			$sql="SELECT cat_de_gastos.id AS idcat_de_gastos,cat_de_gastos.descripcion AS gasto,grupo_de_gastos_id,grupo_de_gastos.descripcion AS grupo,cat_de_gastos.condicion FROM cat_de_gastos  
			INNER JOIN grupo_de_gastos ON grupo_de_gastos.id= cat_de_gastos.grupo_de_gastos_id where grupo_de_gastos_id = '$nombre' ";
		else	
			$sql="SELECT cat_de_gastos.id AS idcat_de_gastos,cat_de_gastos.descripcion AS gasto,grupo_de_gastos_id,grupo_de_gastos.descripcion AS grupo,cat_de_gastos.condicion FROM cat_de_gastos  
			INNER JOIN grupo_de_gastos ON grupo_de_gastos.id = cat_de_gastos.grupo_de_gastos_id  where cat_de_gastos.descripcion like '%$nombre%' ";
			
		//echo "$sql";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select()
	{
		$sql="SELECT id as idcat_de_gastos,descripcion as nombre FROM cat_de_gastos where condicion=1";
		return ejecutarConsulta($sql);		
	}
	public function selectporid($idcat_de_gastos)
	{
		$sql="SELECT id as idcat_de_gastos,descripcion as nombre,grupo_de_gastos_id  FROM cat_de_gastos where id=$idcat_de_gastos ";
		
		return ejecutarConsulta($sql);		
	}
}

?>