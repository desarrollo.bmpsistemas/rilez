 <?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class clientes
{
	//Implementamos nuestro constructor 
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
							
	public function insertar($nombre,$nombrecomercial,$rfc,$idvendedores,$referencia,$telefono,$correo,$diascredito,$limitecredito,$descuento)
	{
		$sql="INSERT INTO clientes (descripcion,nomComercial,       rfc,vendedor_id,    referencia,telefono,    correo,     dias,     limite,descuento,condicion)
		                   VALUES ('$nombre','$nombrecomercial','$rfc','$idvendedores','$referencia','$telefono','$correo','$diascredito','$limitecredito','$descuento',1)";
		$id=ejecutarConsulta_retornarID($sql);
		if (empty($id))
			return $sql;
		else
			return $id;
	}

	//Implementamos un método para editar registros
	public function editar($idclientes,$nombre,$nombrecomercial,$rfc,$idvendedores,$referencia,$telefono,$correo,$diascredito,$limitecredito,$descuento)
	{
		$sql="UPDATE clientes SET descripcion='$nombre',nomComercial='$nombrecomercial',rfc='$rfc',vendedor_id='$idvendedores',referencia='$referencia',telefono='$telefono',correo='$correo',dias='$diascredito',limite='$limitecredito',descuento='$descuento' WHERE id='$idclientes'";
		$id= ejecutarConsulta($sql);
		//if (empty($id))
		//	return $sql;
		//else
		return $id;
	}

	//Implementamos un método para desactivar categorías
	/*public function eliminar($idclientes)
	{
		$sql="DELETE from clientes WHERE idclientes='$idclientes'";
		return ejecutarConsulta($sql);
	}*/


	//Implementar un método para mostrar los datos de un registro a modificar
	public function verSaldoDeLaPersona($idpersonas) 
	{
		$hoy=date("Y-m-d");
		$sql="SELECT SUM(total-abonos) AS saldo FROM salidas WHERE total-abonos>0 AND tipo_mov_id IN (5,6) AND cliente_id=$idpersonas AND (DATEDIFF('$hoy',vencimiento))>0  AND estatus_id <>3 ";
		return ejecutarConsultaSimpleFila($sql);
	}
	public function verHistorialDeAdeudo($idpersonas) 
	{
		
		$hoy=date("Y-m-d");
		$sql="SELECT id,fecha,total-abonos AS saldo, (DATEDIFF('$hoy',vencimiento)) AS dias FROM salidas WHERE total-abonos>0 AND tipo_mov_id IN (5,6) AND cliente_id=$idpersonas and estatus_id<>3 ";
		return ejecutarConsulta($sql);
	}

	public function mostrar($idclientes) 
	{
		//SELECT INTERVAL 1 DAY + '1997-12-31' AS vencimiento
		$hoy=date("Y-m-d");
		$sql="SELECT *,INTERVAL dias DAY + '$hoy' AS vencimiento,descuento FROM clientes WHERE id='$idclientes'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros 
	public function listar($Opcion,$referencia)
	{
		if ($Opcion==0)
			$q="  id = '$referencia' ";

		if ($Opcion==1)
			$q="  rfc like '%$referencia%' ";
		if ($Opcion==2)
			$q=" descripcion like '%$referencia%' ";
		if ($Opcion==3)
			$q=" telefono like '%$referencia%' ";
		

		$sql="SELECT id AS idclientes,descripcion AS nombre,rfc,nomComercial AS nombrecomercial,telefono,condicion FROM clientes WHERE  $q ";
		//echo "$sql";
		return ejecutarConsulta($sql);	 	
	}

	//Implementar un método para listar los registros
	
	public function select($Opcion,$referencia) // seleccionar proveedor
	{
		if ($Opcion==0)
			$q="  where id = '$referencia' ";
		if ($Opcion==1)
			$q=" where rfc like '%$referencia%' ";
		if ($Opcion==2)
			$q=" where descripcion like '%$referencia%' ";
		if ($Opcion==3)
			$q=" where telefono like '%$referencia%' ";
		

		$sql="SELECT id AS idclientes,CONCAT(SUBSTRING(descripcion,1,30),' - ',nomComercial) AS nombre FROM clientes   $q "; 
		return ejecutarConsulta($sql);		
	}
	
	//Implementamos un método para desactivar categorías
	public function desactivar($idclientes)
	{
		$sql="UPDATE clientes SET condicion='0' WHERE id='$idclientes'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idclientes)
	{
		$sql="UPDATE clientes SET condicion='1' WHERE id='$idclientes'";
		return ejecutarConsulta($sql);
	}
}

?>
