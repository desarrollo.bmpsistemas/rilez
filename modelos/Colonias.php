<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Colonias
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function guardar($Opcion,$idcolonias,$nombre)
	{
		if ($Opcion=="I")
			$sql="INSERT INTO colonias (descripcion,condicion) VALUES ('$nombre','1')";
		if ($Opcion=='U')
			$sql="UPDATE colonias SET descripcion='$nombre' WHERE id='$idcolonias'";
		if ($Opcion=='D')
			$sql="DELETE FROM  colonias  WHERE id='$idcolonias'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idcolonias,$nombre)
	{
		$sql="UPDATE colonias SET descripcion='$nombre' WHERE id='$idcolonias'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idcolonias)
	{
		$sql="UPDATE colonias SET condicion='0' WHERE id='$idcolonias'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idcolonias)
	{
		$sql="UPDATE colonias SET condicion='1' WHERE id='$idcolonias'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idcolonias)
	{
		$sql="SELECT id as idcolonias,descripcion as nombre FROM colonias WHERE id='$idcolonias'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar($nombre)
	{
		if (strlen($nombre)<3)
		{
			$nombre="XXX";
		}
		
		$q= " where descripcion like '%$nombre%' ";
		

		$sql="SELECT id as idcolonias,descripcion as nombre,condicion FROM colonias  $q";
		
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select($nombre)
	{
		$sql="SELECT id as idcolonias,descripcion as nombre FROM colonias where condicion=1 and descripcion like '%$nombre%'";
		return ejecutarConsulta($sql);		
	}
	public function selectporid($idcolonias)
	{
		$sql="SELECT id as idcolonias,descripcion as nombre  FROM colonias where id=$idcolonias ";
		
		return ejecutarConsulta($sql);		
	}
}

?>