<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Comisiones
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($idvendedores,$lim_inf,$lim_sup,$por_vendedor,$por_gerente)
	{
		$sql="INSERT INTO comisiones (idvendedores,lim_inf,lim_sup,por_vendedor,por_gerente,condicion)
		                   VALUES ('$idvendedores','$lim_inf','$lim_sup','$por_vendedor','$por_gerente','1')";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idcomisiones,$idvendedores,$lim_inf,$lim_sup,$por_vendedor,$por_gerente)
	{
		$sql="UPDATE comisiones SET 
		idvendedores=$idvendedores,lim_inf=$lim_inf,lim_sup=$lim_sup,por_vendedor=$por_vendedor,por_gerente=$por_gerente where idcomisiones=$idcomisiones";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idcomisiones)
	{
		$sql="DELETE FROM comisiones  WHERE idcomisiones='$idcomisiones'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idcomisiones)
	{
		$sql="DELETE FROM comisiones  WHERE idcomisiones='$idcomisiones'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idcomisiones)
	{
		$sql="SELECT * FROM comisiones WHERE idcomisiones='$idcomisiones'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar()
	{
		$sql="SELECT idcomisiones,comisiones.idvendedores,vendedores.nombre AS vendedor,lim_inf,lim_sup,por_vendedor,por_gerente,comisiones.condicion FROM comisiones
			  INNER JOIN vendedores ON vendedores.idvendedores=comisiones.idvendedores";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select()
	{
		$sql="SELECT * FROM comisiones where condicion=1";
		return ejecutarConsulta($sql);		
	}
}

?>