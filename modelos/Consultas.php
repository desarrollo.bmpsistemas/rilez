<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Consultas
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}


	//Implementar un método para listar los registros
	public function comprasFecha($fecha_inicio,$fecha_fin)
	{
		$sql="SELECT DATE(i.fecha_hora) AS fecha,u.nombre AS usuario,p.nombre AS proveedor,i.tipo_comprobante,i.serie_comprobante,i.num_comprobante,
		i.total_compra,i.impuesto,i.estado  FROM ingreso i 
		INNER JOIN persona p ON i.idproveedor=p.idpersona 
		INNER JOIN usuario u ON i.idusuario=u.idusuario 
		WHERE date(i.fecha_hora)>='$fecha_inicio' and date(i.fecha_hora)<='$fecha_fin'";
		return ejecutarConsulta($sql);		
	}

	//Implementar un método para listar los registros
	public function ventasfechacliente($fecha_inicio,$fecha_fin,$idcliente)
	{
		$sql="SELECT DATE(v.fecha_hora) AS fecha,u.nombre AS usuario,p.nombre AS cliente,v.tipo_comprobante,v.serie_comprobante,v.num_comprobante,
		v.total_venta,v.impuesto,v.estado  FROM venta v 
		INNER JOIN persona p ON v.idcliente=p.idpersona 
		INNER JOIN usuario u ON v.idusuario=u.idusuario
		WHERE date(v.fecha_hora)>='$fecha_inicio' and date(v.fecha_hora)<='$fecha_fin' and v.idcliente='$idcliente' ";
		return ejecutarConsulta($sql);		
	}

	public function totalcomprahoy()
	{
		$sql="SELECT IFNULL(SUM(total_compra),0) AS total_compra
		FROM ingreso  WHERE date(fecha_hora)=curdate()";
		return ejecutarConsulta($sql);		
	}
	public function totalventahoy()
	{
		$sql="SELECT IFNULL(SUM(total_venta),0) AS total_venta
		FROM venta  WHERE date(fecha_hora)=curdate()";
		return ejecutarConsulta($sql);		
	}

	public function comprasultimos_10dias()
	{
		$sql="SELECT CONCAT(DAY(fecha_hora),'-',MONTH(fecha_hora)) as fecha,SUM(total_Compra) as total FROM ingreso  group by fecha_hora  order by fecha_hora DESC limit 0,10";
		return ejecutarConsulta($sql);		
	}

	public function ventasultimos_12meses()
	{
		$sql="SELECT date_format(fecha_hora,'%M') as fecha,SUM(total_venta) as total FROM venta  group by MONTH(fecha_hora)  order by fecha_hora DESC limit 0,12";
		return ejecutarConsulta($sql);		
	}
	
}

?>