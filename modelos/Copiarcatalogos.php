<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Copiarcatalogos
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementar un método para mostrar los datos de un registro a modificar
	
	//Implementar un método para listar los registros
	public function listar($idalmacenes,$idsublineas)
	{
		
		$sql="SELECT articulos_almacenes.idarticulos_almacenes,articulos.nombre,articulos_almacenes.costo,articulos_almacenes.porcentaje_utilidad,articulos_almacenes.precio,'1' as condicion FROM articulos_almacenes
		INNER JOIN articulos ON articulos.idarticulos=articulos_almacenes.idarticulos
		WHERE articulos_almacenes.idalmacenes=$idalmacenes AND articulos.idsublineas='$idsublineas'
 		ORDER BY articulos.nombre";
		return ejecutarConsulta($sql);		
	}
	public function procesar($idalmacenesf,$idlineasf,$idsublineasf,$idalmacenesd,$idlineasd,$idsublineasd,$operacion)
	{	

		if ($operacion=='almacen'  || $operacion=='linea' || $operacion=='sublinea')
		{

			//obtener el valor del dolar
			$valor_del_dolar=1;
	    	$sql="SELECT valor FROM detalles_monedas ORDER BY iddetalles_monedas desc LIMIT 1 ";
	    	//echo "$sql </br>";
			$rs= ejecutarConsulta($sql);
			while($fila = mysqli_fetch_assoc( $rs ) )
	      	$valor_del_dolar= $fila['valor'];

			//buscar idivas e idmonedas del almacen fuente
			$sql="SELECT idmonedas,idivas FROM almacenes where idalmacenes = $idalmacenesf";
  			$rs= ejecutarConsulta($sql);
  			//echo "$sql </br>";
			while($fila = mysqli_fetch_assoc( $rs ) )
			{
				$idivasf= $fila['idivas'];	
				$idmonedasf= $fila['idmonedas'];	
			}
			//buscar idivas e idmonedas del almacen destino
			$sql="SELECT idmonedas,idivas FROM almacenes where idalmacenes = $idalmacenesd";
  			$rs= ejecutarConsulta($sql);
  			//echo "$sql </br>";
			while($fila = mysqli_fetch_assoc( $rs ) )
			{
				$idivasd= $fila['idivas'];	
				$idmonedasd= $fila['idmonedas'];	
			}

			$sql="SELECT nombre as iva from ivas WHERE idivas='$idivasd' ";
			$rs= ejecutarConsulta($sql);
			//echo "$sql </br>";
			while($fila = mysqli_fetch_assoc( $rs ) )
	      	$iva= $fila['iva'];

	      	//conversion de costo y precio por efecto de lamoneda
	      	

	      	if ($operacion=='almacen')
				$sql="SELECT idalmacenes,articulos_almacenes.idarticulos,stock,idmonedas,costo,precio,iva,costo_automatico,precio_automatico,porcentaje_utilidad,entradas,salidas,posicion,transito,ubicacion,fecha_inv,existencia,articulos_almacenes.condicion
				FROM articulos_almacenes
				INNER JOIN articulos ON articulos.idarticulos=articulos_almacenes.idarticulos 
				INNER JOIN sublineas ON sublineas.idsublineas=articulos.idsublineas
				INNER JOIN lineas ON lineas.idlineas=sublineas.idlineas
				WHERE articulos_almacenes.idalmacenes=$idalmacenesf";


	      	if ($operacion=='linea')
				$sql="SELECT idalmacenes,articulos_almacenes.idarticulos,stock,idmonedas,costo,precio,iva,costo_automatico,precio_automatico,porcentaje_utilidad,entradas,salidas,posicion,transito,ubicacion,fecha_inv,existencia,articulos_almacenes.condicion
				FROM articulos_almacenes
				INNER JOIN articulos ON articulos.idarticulos=articulos_almacenes.idarticulos 
				INNER JOIN sublineas ON sublineas.idsublineas=articulos.idsublineas
				INNER JOIN lineas ON lineas.idlineas=sublineas.idlineas
				WHERE lineas.idlineas=$idlineasf";
			
			if ($operacion=='sublinea')
				$sql="SELECT idalmacenes,articulos_almacenes.idarticulos,stock,idmonedas,costo,precio,iva,costo_automatico,precio_automatico,porcentaje_utilidad,entradas,salidas,posicion,transito,ubicacion,fecha_inv,existencia,articulos_almacenes.condicion
				FROM articulos_almacenes
				INNER JOIN articulos ON articulos.idarticulos=articulos_almacenes.idarticulos 
				WHERE articulos_almacenes.idalmacenes=$idalmacenesf AND 
				articulos.idsublineas=$idsublineasf";
			$rsdatos = ejecutarConsulta($sql);		
			//echo "$sql </br>";
			//recorrer la consulta para insertar o actualizar en almaced, linea y sublinea destino
			while ($fila = mysqli_fetch_assoc($rsdatos)) 
			{
			    $idarticulos= $fila['idarticulos'];
			    $stock= $fila['stock'];
			    $idmonedas= $fila['idmonedas'];
			    $costo= $fila['costo'];
			    $precio= $fila['precio'];
			    $costo_automatico= $fila['costo_automatico'];
			    $precio_automatico= $fila['precio_automatico'];
			    $porcentaje_utilidad= $fila['porcentaje_utilidad'];
			    $entradas= $fila['entradas'];
			    $salidas= $fila['salidas'];
			    $posicion=$fila['posicion'];
			    $transito=$fila['transito'];
			    $ubicacion=$fila['ubicacion'];
			    $fecha_inv=$fila['fecha_inv'];
			    $existencia=$fila['existencia'];
			    $condicion=$fila['condicion'];
			    $entro='N';
			    //calcular costo y precio de acuerdo a la moneda destino
			    if ($idmonedasf==1 && $idmonedasd>1 && ($costo>0 && $valor_del_dolar>0))
				{
					$Costo=round(($costo/$valor_del_dolar),2);
					$Precio=round(($precio/$valor_del_dolar),2);
					$entro="S";
				}
				if ($idmonedasf==2 && $idmonedasd==1)
				{
					$Costo=round(($costo*$valor_del_dolar),2);
					$Precio=round(($precio*$valor_del_dolar),2);
					$entro="S";
				}
				if ($idmonedasf == $idmonedasd)
				{
					$Costo=$costo;
					$Precio=$precio;
					$entro="S";
				}
				if ($entro=='N')
				{
					$Costo=$costo;
					$Precio=$precio;
				}
				//ver si existe para actualizarlo o ingresarlo 
				$idarticulos_almacenes=0;
				$sql="SELECT idarticulos_almacenes from articulos_almacenes where idalmacenes=$idalmacenesd and idarticulos=$idarticulos ";
				$rs = ejecutarConsulta($sql);		
				//echo "$sql </br>";
				//recorrer la consulta para insertar o actualizar en almaced, linea y sublinea destino
				while ($fila = mysqli_fetch_assoc($rs))
				$idarticulos_almacenes= $fila['idarticulos_almacenes'];
				if (empty($idarticulos_almacenes))	
			    	$sql="INSERT INTO articulos_almacenes (idalmacenes,idarticulos,stock,idmonedas,costo,precio,iva,porcentaje_utilidad,ubicacion,condicion)
											   VALUES ('$idalmacenesd','$idarticulos','$stock','$idmonedasd','$Costo','$Precio','$iva','$porcentaje_utilidad','$ubicacion','1')";
				else
					$sql="UPDATE articulos_almacenes SET costo='$Costo',precio='$Precio',idmonedas='$idmonedasd',iva='$iva',stock='$stock',porcentaje_utilidad='$porcentaje_utilidad',ubicacion='$ubicacion' where idalmacenes=$idalmacenesd and idarticulos=$idarticulos ";
				$rs= ejecutarConsulta($sql);
				//echo "$sql </br>";
			}

		}
		return $rs;		
	}
	
	
}

?>