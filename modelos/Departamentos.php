<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Departamentos
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($nombre)
	{
		$sql="INSERT INTO departamentos (descripcion,condicion)
		VALUES ('$nombre','1')";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($iddepartamentos,$nombre)
	{
		$sql="UPDATE departamentos SET descripcion='$nombre' WHERE id='$iddepartamentos'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($iddepartamentos)
	{
		$sql="UPDATE departamentos SET condicion='0' WHERE id='$iddepartamentos'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($iddepartamentos)
	{
		$sql="UPDATE departamentos SET condicion='1' WHERE id='$iddepartamentos'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($iddepartamentos)
	{
		$sql="SELECT id as iddepartamentos,descripcion as nombre FROM departamentos WHERE id='$iddepartamentos'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar()
	{
		$sql="SELECT id as iddepartamentos,descripcion as nombre,condicion FROM departamentos";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select()
	{
		$sql="SELECT id as iddepartamentos,descripcion as nombre FROM departamentos where condicion=1";
		return ejecutarConsulta($sql);		
	}
}

?>