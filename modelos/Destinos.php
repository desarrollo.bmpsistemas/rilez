<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Destinos
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function guardar($Opcion,$iddestinos,$nombre)
	{
		if ($Opcion=="I")
			$sql="INSERT INTO destinos (descripcion,condicion) VALUES ('$nombre','1')";
		if ($Opcion=='U')
			$sql="UPDATE destinos SET descripcion='$nombre' WHERE id='$iddestinos'";
		if ($Opcion=='D')
			$sql="DELETE FROM  destinos  WHERE id='$iddestinos'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($iddestinos,$nombre)
	{
		$sql="UPDATE destinos SET descripcion='$nombre' WHERE id='$iddestinos'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($iddestinos)
	{
		$sql="UPDATE destinos SET condicion='0' WHERE id='$iddestinos'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($iddestinos)
	{
		$sql="UPDATE destinos SET condicion='1' WHERE id='$iddestinos'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($iddestinos)
	{
		$sql="SELECT id as iddestinos,descripcion as nombre FROM destinos WHERE id='$iddestinos'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar($nombre)
	{
		//if (strlen($nombre)<3)
		//{
		//	$nombre="XXX";
		//}
		
		$q= " where descripcion like '%$nombre%' ";
		

		$sql="SELECT id as iddestinos,descripcion as nombre,condicion FROM destinos  $q";
		
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select($nombre)
	{
		$sql="SELECT id as iddestinos,descripcion as nombre FROM destinos where condicion=1 and descripcion like '%$nombre%'";
		return ejecutarConsulta($sql);		
	}
	public function selectporid($iddestinos)
	{
		$sql="SELECT id as iddestinos,descripcion as nombre  FROM destinos where id=$iddestinos ";
		
		return ejecutarConsulta($sql);		
	}
}

?>