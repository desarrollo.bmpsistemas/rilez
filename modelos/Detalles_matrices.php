<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Detalles_matrices
{
	//Implementamos nuestro constructor 
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($idmatrices,$idarticulos,$cantidad,$costo)
	{
		  	
	    $sql="INSERT INTO detalles_matrices (matriz_id,articulo_id,costo,cantidad,condicion)
										   VALUES ('$idmatrices','$idarticulos','$costo','$cantidad','1')";
		return ejecutarConsulta($sql);
	}
	
	//Implementamos un método para editar registros $iddetalles_matrices,$costo,$cantidad
	public function editar($iddetalles_matrices,$idarticulos,$cantidad,$costo)
	{
		  	
      	$sql="UPDATE detalles_matrices SET 
      	articulo_id='$idarticulos',
		costo='$costo',cantidad='$cantidad'
		WHERE mov='$iddetalles_matrices' ";
		return ejecutarConsulta($sql);
	}	
		
	

	//Implementamos un método para desactivar categorías
	public function desactivar($iddetalles_matrices)
	{
		$sql="DELETE FROM detalles_matrices  WHERE id='$iddetalles_matrices' ";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($iddetalles_matrices)
	{
		$sql="DELETE FROM detalles_matrices  WHERE id='$iddetalles_matrices' ";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($iddetalles_matrices)
	{
		$sql="SELECT detalles_matrices.mov AS iddetalles_matrices,articulos.id AS idarticulos,articulos.id2 AS codigo,articulos.descripcion AS nombre,detalles_matrices.costo,detalles_matrices.cantidad,detalles_matrices.condicion FROM detalles_matrices
			INNER JOIN articulos ON articulos.id=detalles_matrices.articulo_id		
			WHERE detalles_matrices.mov=$iddetalles_matrices ";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar($idmatrices)
	{
		
	 $sql="SELECT detalles_matrices.mov as iddetalles_matrices,articulos.id2 as codigo,articulos.descripcion as nombre,detalles_matrices.costo,detalles_matrices.cantidad,detalles_matrices.condicion FROM detalles_matrices
		INNER JOIN articulos ON articulos.id=detalles_matrices.articulo_id		
		WHERE detalles_matrices.matriz_id='$idmatrices' ";
		return ejecutarConsulta($sql);		
	}
	
	
	
}

?>