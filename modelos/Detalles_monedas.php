 <?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Detalles_monedas
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($idusuarios,$valor)
	{
		$sql="INSERT INTO detalles_monedas (idusuarios,valor) VALUES('$idusuarios','$valor')";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($iddetalles_monedas,$idusuarios,$valor)
	{
		$sql="UPDATE detalles_monedas SET idusuarios=$idusuarios, valor=$valor  WHERE iddetalles_monedas=$iddetalles_monedas";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	//editar*
	public function mostrar($iddetalles_monedas)
	{
		$sql="SELECT dm.iddetalles_monedas,dm.fecha,dm.valor,u.nombre 
			FROM detalles_monedas dm
			INNER JOIN usuarios u ON dm.idusuarios = u.idusuarios WHERE 
			dm.iddetalles_monedas=$iddetalles_monedas";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar($fecha)
	{
		$sql="SELECT dm.iddetalles_monedas,dm.fecha,dm.valor,u.nombre,dm.condicion FROM detalles_monedas dm
			INNER JOIN usuarios u ON dm.idusuarios = u.idusuarios WHERE DATE (dm.fecha)='$fecha' ORDER BY dm.fecha DESC LIMIT 4";
		return ejecutarConsulta($sql);		
	}

	public function listarporfecha($fecha)
	{
		$sql="SELECT dm.iddetalles_monedas,dm.fecha,dm.valor,u.nombre FROM detalles_monedas dm
			INNER JOIN usuarios u ON dm.idusuarios = u.idusuarios WHERE DATE (dm.fecha)='$fecha' ORDER BY dm.fecha DESC LIMIT 4";
		return ejecutarConsulta($sql);		
	}
}
?>