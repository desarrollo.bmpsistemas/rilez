 <?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Personas
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($nombre,$nombrecomercial,$rfc,$idmunicipios,$idcolonias,$idcalles,$idvendedores,$numero_exterior,$numero_interior,$referencia,$codigo_postal,$telefono,$correo,$diascredito,$limitecredito,$tipodepersona)
	{
		$sql="INSERT INTO personas (nombre,nombrecomercial,rfc,idmunicipios,idcolonias,idcalles,idvendedores,numero_exterior,numero_interior,referencia,codigo_postal,telefono,correo,diascredito,limitecredito,tipodepersona)

		VALUES ('$nombre','$nombrecomercial','$rfc','$idmunicipios','$idcolonias','$idcalles','$idvendedores','$numero_exterior','$numero_interior','$referencia','$codigo_postal','$telefono','$correo','$diascredito','$limitecredito','$tipodepersona')";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idpersonas,$nombre,$nombrecomercial,$rfc,$idmunicipios,$idcolonias,$idcalles,$idvendedores,$numero_exterior,$numero_interior,$referencia,$codigo_postal,$telefono,$correo,$diascredito,$limitecredito,$tipodepersona)
	{
		$sql="UPDATE personas SET nombre='$nombre',nombrecomercial='$nombrecomercial',rfc='$rfc',idmunicipios='$idmunicipios',idcolonias='$idcolonias',idcalles='$idcalles',idvendedores='$idvendedores',numero_exterior='$numero_exterior',numero_interior='$numero_interior',referencia='$referencia',codigo_postal='$codigo_postal',telefono='$telefono',correo='$correo',tipodepersona='$tipodepersona',diascredito='$diascredito',limitecredito='$limitecredito' WHERE idpersonas='$idpersonas'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar categorías
	/*public function eliminar($idpersonas)
	{
		$sql="DELETE from personas WHERE idpersonas='$idpersonas'";
		return ejecutarConsulta($sql);
	}*/


	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idpersonas) 
	{
		$sql="SELECT * FROM personas WHERE idpersonas='$idpersonas'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listarp()
	{
		$sql="SELECT idpersonas,nombre,rfc,nombrecomercial,telefono,condicion FROM personas WHERE tipodepersona='Proveedor' ";
		return ejecutarConsulta($sql);		
	}

	//Implementar un método para listar los registros
	public function listarc()
	{
		$sql="SELECT idpersonas,nombre,rfc,nombrecomercial,telefono,condicion FROM personas WHERE tipodepersona='Cliente' ";
		return ejecutarConsulta($sql);		
	}
	
	//Implementamos un método para desactivar categorías
	public function desactivar($idpersonas)
	{
		$sql="UPDATE personas SET condicion='0' WHERE idpersonas='$idpersonas'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idpersonas)
	{
		$sql="UPDATE personas SET condicion='1' WHERE idpersonas='$idpersonas'";
		return ejecutarConsulta($sql);
	}
}

?>
