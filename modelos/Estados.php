<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Estados
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($idpaises,$nombre)
	{
		$sql="INSERT INTO estados (pais_id,descripcion,condicion)
		VALUES ('$idpaises','$nombre','1')";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idestados,$idpaises,$nombre)
	{
		$sql="UPDATE estados SET pais_id='$idpaises',descripcion='$nombre' WHERE id='$idestados'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar registros
	public function desactivar($idestados)
	{
		$sql="UPDATE estados SET condicion='0' WHERE id='$idestados'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar registros
	public function activar($idestados)
	{
		$sql="UPDATE estados SET condicion='1' WHERE id='$idestados'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idestados)
	{
		$sql="SELECT e.id as idestados,p.id as idpaises,e.descripcion as estado,p.descripcion as pais,
		e.condicion FROM estados e 
			  INNER JOIN paises p ON e.pais_id=p.id WHERE e.id=$idestados";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar($idpaises)
	{
		$sql="SELECT e.id as idestados,p.id as idpaises,e.descripcion as  estado,p.descripcion as pais,
		e.condicion FROM estados e 
			  INNER JOIN paises p ON e.pais_id=p.id WHERE e.pais_id=$idpaises";
		return ejecutarConsulta($sql);		
	}
	public function listarestados()
	{
		$sql="SELECT e.id as idestados,p.id as idpaises,e.descripcion as estado,p.descripcion AS pais,
		e.condicion FROM estados e 
			  INNER JOIN paises p ON e.pais_id=p.id WHERE e.pais_id >0 order by p.id,e.descripcion";
		return ejecutarConsulta($sql);		
	}
	public function select()
	{
		$sql="SELECT id as idestados,descripcion as nombre FROM estados WHERE condicion>0";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros ACTIVOS
	public function listarActivos($idpaises)
	{
		$sql="SELECT e.id as idestados,p.id as idpaises,e.descripcion  AS estado,p.descripcion as pais,
		e.condicion FROM estados e 
			  INNER JOIN paises p ON e.pais_id=p.id WHERE e.pais_id=$idpaises and  e.condicion='1' ";
		return ejecutarConsulta($sql);		
	}
	
	
}

?>