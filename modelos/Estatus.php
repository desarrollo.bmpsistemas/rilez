<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Estatus
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($nombre)
	{
		$sql="INSERT INTO estatus (descripcion,condicion)
		VALUES ('$nombre','1')";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idestatus,$nombre)
	{
		$sql="UPDATE estatus SET descripcion='$nombre' WHERE id='$idestatus'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idestatus)
	{
		$sql="UPDATE estatus SET condicion='0' WHERE id='$idestatus'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idestatus)
	{
		$sql="UPDATE estatus SET condicion='1' WHERE id='$idestatus'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idestatus)
	{
		$sql="SELECT id as idestatus,descripcion as nombre FROM estatus WHERE id='$idestatus'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar()
	{
		$sql="SELECT id as idestatus,descripcion as nombre,condicion  FROM estatus";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select()
	{
		$sql="SELECT id as idestatus,descripcion as nombre  FROM estatus where condicion=1";
		return ejecutarConsulta($sql);		
	}
}

?>