<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";
ini_set ( 'max_execution_time', 3600);  
Class Filexml
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}
	
	//Implementamos un método para insertar registros
	public function insertarArticulo($Fact_Folio,$Fact_Fecha,$NoIdentificacion,$ClaveUnidad,$ClaveProdServ,$Unidad,$Descripcion,$ValorUnitario,$Moneda,$TipoCambio,$nombre)
	{
		$sql="INSERT INTO temporal_xml (Folio,Fecha,NoIdentificacion,ClaveUnidad,ClaveProdServ,Unidad,Descripcion,ValorUnitario,Moneda,TipoCambio,nombre,condicion)
								VALUES ('$Fact_Folio','$Fact_Fecha','$NoIdentificacion','$ClaveUnidad','$ClaveProdServ','$Unidad','$Descripcion','$ValorUnitario','$Moneda','$TipoCambio','$nombre',1)";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editarArticulo($Fact_Folio,$Fact_Fecha,$idtemporal_xml,$NoIdentificacion,$ClaveUnidad,$ClaveProdServ,$Unidad,$Descripcion,$ValorUnitario,$nombre)
	{
		$sql="UPDATE temporal_xml SET 
			Folio='$Fact_Folio',
			Fecha='$Fact_Fecha',
			NoIdentificacion='$NoIdentificacion',
            ClaveUnidad='$ClaveUnidad',
            ClaveProdServ='$ClaveProdServ',
            Unidad='$Unidad',
            Descripcion='$Descripcion',
            ValorUnitario='$ValorUnitario',
            nombre='$nombre'
            where idtemporal_xml=$idtemporal_xml";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar registros
	public function desactivar($idtemporal_xml)
	{
		$sql="UPDATE temporal_xml SET condicion=0 WHERE idtemporal_xml=$idtemporal_xml";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar registros
	public function activar($idtemporal_xml)
	{
		$sql="UPDATE temporal_xml SET condicion=1 WHERE idtemporal_xml=$idtemporal_xml";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idtemporal_xml)
	{
		$sql="SELECT idtemporal_xml,NoIdentificacion,ClaveUnidad,ClaveProdServ,Unidad,Descripcion,ValorUnitario,condicion FROM temporal_xml WHERE idtemporal_xml='$idtemporal_xml' ";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function migrarArticulos($archivo)
	{
		$sql="SELECT idtemporal_xml,NoIdentificacion,ClaveUnidad,ClaveProdServ,Unidad,Descripcion,ValorUnitario,condicion from temporal_xml where nombre='$archivo' ";
		return ejecutarConsulta($sql);		
	}
	public function buscarArchivo($archivo)
	{
		$sql="SELECT nombre from temporal_xml where nombre='$archivo' limit 1 ";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros
	public function listar($archivo)
	{
		$sql="SELECT idtemporal_xml,NoIdentificacion,ClaveUnidad,ClaveProdServ,Unidad,Descripcion,ValorUnitario,condicion from temporal_xml where nombre='$archivo' ";
		return ejecutarConsulta($sql);		
	}
	public function eliminar($nombre)
	{
		$sql="DELETE FROM  temporal_xml  WHERE nombre='$nombre'";
		return ejecutarConsulta($sql);
	}
	public function insertarProveedor($nombre,$nombrecomercial,$rfc,$idmunicipios,$idcolonias,$idcalles,$idvendedores,$numero_exterior,$numero_interior,$referencia,$codigo_postal,$telefono,$correo,$diascredito,$limitecredito,$tipodepersona)
	{
		$id=0;
		$sql="SELECT idmunicipios FROM municipios where idmunicipios=$idmunicipios";
		$id=ejecutarConsulta_retornarID($sql);
		if (empty($id))
		{ //Verificar si existe municipio, estado y paises para darlos de alta si no existen

			$sql="SELECT id FROM estados where idestados=1";
			$id=ejecutarConsulta_retornarID($sql);
			if (empty($id))
			{
				$sql="SELECT id FROM paises where idpaises=1";
				$id=ejecutarConsulta_retornarID($sql);
				if (empty($id))
				{
					$sql="INSERT INTO paises (nombre,condicion)
					VALUES ('NUEVO','1')";
					$rs= ejecutarConsulta($sql);
				}
				$sql="INSERT INTO estados (idpaises,nombre,condicion)
					VALUES (1,'NUEVO','1')";
					$rs= ejecutarConsulta($sql);
			}
			$sql="INSERT INTO municipios (idestados,nombre,condicion)
			VALUES ('1','$nombre','1')";
			$rs= ejecutarConsulta($sql);

		}
		$sql="INSERT INTO personas (nombre,nombrecomercial,rfc,idmunicipios,idcolonias,idcalles,idvendedores,numero_exterior,numero_interior,referencia,codigo_postal,telefono,correo,diascredito,limitecredito,tipodepersona)
		VALUES ('$nombre','$nombrecomercial','$rfc','$idmunicipios','$idcolonias','$idcalles','$idvendedores','$numero_exterior','$numero_interior','$referencia','$codigo_postal','$telefono','$correo','$diascredito','$limitecredito','$tipodepersona')";
		return ejecutarConsulta($sql);
	}
	//Implementar un método para listar los registros ACTIVOS
	
		
	
}

?>