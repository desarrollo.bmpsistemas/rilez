<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Gastos
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
							 
	public function insertar($cat_de_gastos_id,$almacen_id,$fecha,$folio,$referencia,$concepto,$importe,$iva,$ajuste,$subtotal,$isrretenido,$ivaretenido,$total,$moneda,$valormoneda,$proveedor_id,$tasaocuota,$uuid,$capturista_id,$idformasdepagos,$sat_clave_forma_de_pago,$sat_nombre_forma_de_pago,$rfc,$nombre)
	{
		$proveedor_id=0;
		$valormoneda=0;
		$sql="SELECT valor as valormoneda FROM monedas  where abreviatura='$moneda' order by fecha desc limit 1 ";
		$rs= ejecutarConsulta($sql);
		while($fila = mysqli_fetch_assoc( $rs ) )
		{
			$valormoneda= $fila['valormoneda'];	
		}

		$sql="SELECT id as proveedor_id FROM proveedores  where rfc='$rfc' ";
		$rs= ejecutarConsulta($sql);
		while($fila = mysqli_fetch_assoc( $rs ) )
		{
			$proveedor_id= $fila['proveedor_id'];	
		}
		if (empty($proveedor_id)) //registrar proveedor
		{
			$sql="INSERT INTO proveedores (descripcion,nomComercial,rfc)	
						  VALUES ('$nombre','$nombre','$rfc')";
			$proveedor_id= ejecutarConsulta_retornarID($sql);
		}

		$sql="INSERT INTO gastos  (cat_de_gastos_id,   almacen_id,   fecha,   folio,referencia,   concepto,  importe,   iva,   ajuste,  subtotal,    isrretenido,   ivaretenido,    total,   moneda,    valormoneda,  proveedor_id,    tasaocuota, estatus_id,    uuid,capturista_id,    forma_de_pago_id,   sat_clave_forma_de_pago,  sat_nombre_forma_de_pago,condicion)
						 VALUES ('$cat_de_gastos_id','$almacen_id','$fecha','$folio','$referencia','$concepto','$importe','$iva','$ajuste','$subtotal','$isrretenido','$ivaretenido','$total','$moneda','$valormoneda','$proveedor_id','$tasaocuota',        '4', '$uuid','$capturista_id','$idformasdepagos','$sat_clave_forma_de_pago','$sat_nombre_forma_de_pago',1)";
		//return ejecutarConsulta($sql);
		$id= ejecutarConsulta_retornarID($sql);


		if ($id>0 ) // contado
		{
			if ($idformasdepagos==1) // contado
			{
				if ($moneda=='MXN') 
					$moneda_id=1;
				else
					$moneda_id=2;
				$sql="INSERT INTO cxpg  (gastos_id,importe,  fecha,  estatus_id,  capturista_id, forma_de_pago_id,  sat_forma_de_pago_id,referencia,    valormoneda,moneda_id,condicion)
							     VALUES ('$id',    '$importe','$fecha',2,        '$capturista_id',                1,                    1,'$referencia',$valormoneda,  $moneda_id,       1)";
				//return ejecutarConsulta($sql);
				//echo "$sql";
				$id2=ejecutarConsulta_retornarID($sql);
				//actualizar pagos a a bonos de gastos
				$sql="SELECT sum(importe) as abonos  FROM cxpg WHERE gastos_id='$id' ";
				$abonos=0;
				$rs= ejecutarConsulta($sql);
		       	while($fila = mysqli_fetch_array($rs)) 
		       	{
					$abonos= $fila['abonos'];
					$sql="UPDATE gastos set abonos=$abonos WHERE  gastos_id=$id";
					ejecutarConsulta($sql);

				}
			}
			
			return $id;
		}
		else
		    return ($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idgastos,$cat_de_gastos_id,$almacen_id,$fecha,$folio,$referencia,$concepto,$importe,$iva,$ajuste,$subtotal,$isrretenido,$ivaretenido,$total,$moneda,$valormoneda,$proveedor_id,$tasaocuota,$uuid,$capturista_id,$idformasdepagos,$sat_clave_forma_de_pago,$sat_nombre_forma_de_pago,$rfc,$nombre)
	{

		$sql="UPDATE gastos SET cat_de_gastos_id='$cat_de_gastos_id',almacen_id='$almacen_id',fecha='$fecha',folio='$folio',referencia='$referencia',concepto='$concepto',importe='$importe',iva='$iva',ajuste='$ajuste',subtotal='$subtotal',isrretenido='$isrretenido',ivaretenido='$ivaretenido',total='$total',moneda='$moneda',valormoneda='$valormoneda',proveedor_id='$proveedor_id',tasaocuota='$tasaocuota',uuid='$uuid',capturista_id='$capturista_id',forma_de_pago_id='$idformasdepagos',sat_clave_forma_de_pago='$sat_clave_forma_de_pago',sat_nombre_forma_de_pago='$sat_nombre_forma_de_pago' WHERE gastos_id='$idgastos' ";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idgastos,$capturista_id)
	{
		$sql="SELECT abonos,estatus_id  FROM gastos WHERE gastos_id='$idgastos' ";
		$abonos=0;
		$estatus_id=0;
		$rs= ejecutarConsulta($sql);
       	while($fila = mysqli_fetch_array($rs)) 
       	{
			$abonos= $fila['abonos'];
			$estatus_id= $fila['estatus_id'];
		}

		$sql="SELECT rol_id as rol FROM capturistas WHERE id='$capturista_id' and estatus_id<>3 ";
		$rol=0;
		$rs= ejecutarConsulta($sql);
       	while($fila = mysqli_fetch_array($rs)) 
       	{
			$rol= $fila['rol'];
		}
		if ($rol==7)
		{
			$sql="DELETE  FROM cxpg  WHERE  gastos_id=$idgastos";
			ejecutarConsulta($sql);

			$sql="DELETE  FROM gastos  WHERE  gastos_id=$idgastos";
			ejecutarConsulta($sql);

			
		}
		else
		{
			if ($estatus_id==4 )
			{
				$sql="DELETE  FROM cxpg  WHERE  gastos_id=$idgastos";
				ejecutarConsulta($sql);

				$sql="DELETE  FROM gastos  WHERE  gastos_id=$idgastos";
				ejecutarConsulta($sql);
			}
			
		}
		$sql="SELECT gastos_id  FROM gastos WHERE gastos_id='$idgastos' ";
		
		return ejecutarConsulta($sql);
       	

	}

	//Implementamos un método para activar categorías
	public function activar($idgastos)
	{
		$sql="UPDATE gastos SET condicion='1' WHERE idgastos='$idgastos'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idgastos)
	{
		$sql="SELECT * FROM gastos WHERE gastos_id='$idgastos' ";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar($idgastos,$fecha_inicial,$fecha_final,$almacen_id)
	{
		if ($idgastos==0)
			$q=" gastos.fecha BETWEEN '$fecha_inicial' AND '$fecha_final' AND gastos.almacen_id=$almacen_id ";
		else
			$q=" gastos.gastos_id=$idgastos ";
		$sql="SELECT gastos_id AS idgastos,gastos.fecha,gastos.referencia,gastos.folio,IF(gastos.total>0,gastos.total,gastos.subtotal) AS total,gastos.abonos,gastos.concepto,proveedores.descripcion AS proveedor,gastos.condicion FROM gastos
        INNER JOIN proveedores ON (proveedores.id=gastos.proveedor_id)
        INNER JOIN forma_de_pago ON (forma_de_pago.id=gastos.forma_de_pago_id)
        WHERE  ".$q;
        //echo  "$sql";
		return ejecutarConsulta($sql);		
	}
	public function listarPagos($idgastos)
	{
		
		$sql="SELECT cxpg.id AS idpagos,fecha,referencia,sat_forma_de_pago.descripcion AS forma_de_pago,importe,cxpg.condicion FROM cxpg  
		INNER JOIN sat_forma_de_pago ON sat_forma_de_pago.id=cxpg.sat_forma_de_pago_id  WHERE  gastos_id=$idgastos";
        //echo "$sql";
		return ejecutarConsulta($sql);		
	}
	public function eliminarPago($idpagos,$capturista_id)
	{
		
		$sql="SELECT gastos_id,importe  FROM cxpg WHERE id='$idpagos' ";
		//echo "$sql";
		$idgastos=0;
		$rs= ejecutarConsulta($sql);
       	while($fila = mysqli_fetch_array($rs)) 
       	{
			$idgastos= $fila['gastos_id'];
			$importe= $fila['importe'];
			//buscar si fue de credito 
			$sql="SELECT estatus_id,abonos  FROM gastos WHERE gastos_id='$idgastos' ";
			//echo "$sql";
			$abonos=0;
			$rs= ejecutarConsulta($sql);
	       	while($fila = mysqli_fetch_array($rs)) 
	       	{
				$estatus_id= $fila['estatus_id'];
				$abonos= $fila['abonos'];
				//buscar si fue de credito 
			}
		}
		$sql="SELECT rol_id as rol FROM capturistas WHERE id='$capturista_id' and estatus_id<>3 ";
		$rol=0;
		$rs= ejecutarConsulta($sql);
       	while($fila = mysqli_fetch_array($rs)) 
       	{
			$rol= $fila['rol'];
		}
		if ($rol==7)
		{
			$sql="DELETE  FROM cxpg  WHERE  id=$idpagos";
        	//echo "$sql";
			ejecutarConsulta($sql);	
			if ($importe==$abonos)
			{
				$sql="DELETE  FROM gastos  WHERE  gastos_id=$idgastos";
        		ejecutarConsulta($sql);	
			}
		}
		else	
		{
			if ($estatus_id==4)
			{
				$sql="DELETE  FROM cxpg  WHERE  id=$idpagos";
        		//echo "$sql";
				ejecutarConsulta($sql);	
				if ($importe==$abonos)
				{
					$sql="DELETE  FROM gastos  WHERE  gastos_id=$idgastos";
	        		ejecutarConsulta($sql);	
				}	

			}
		}
		

		$sql="SELECT sum(importe) as abonos  FROM cxpg WHERE gastos_id='$idgastos' ";
		//echo "$sql";
		$abonos=0;
		$rs= ejecutarConsulta($sql);
       	while($fila = mysqli_fetch_array($rs)) 
       	{
			$abonos= $fila['abonos'];
			
		}
		if (empty($abonos)) $abonos=0;
		$sql="UPDATE gastos set abonos=$abonos WHERE  gastos_id=$idgastos";
		//echo "$sql";
		return ejecutarConsulta($sql);	
		
	}
	
	public function leer_xml($archivo)
	{
		
		
		$resultado="";
		$fecha="0000-00-00";
		$SubTotal=0;
		$Moneda="S/M";
		$Nombre="S/N"; 
		$Rfc="S/Rfc";
		$TotalImpuestosTrasladados=0;
		$TotalImpuestosRetenidos=0;
		$TasaOCuota="0.00";
		$Folio=0;
		$SatClaveFormaDePago="S/F";
		$Descripcion="Sin Descripcion ";
		$UUID="SIN UUID";

		$file=$archivo;
		$doc = new DOMDocument();
        $fileXml = "../files/xmlgastos/".$file;
        //echo "archivo xml ".$fileXml;
        
        $fichero=$fileXml;
        $doc->load($fichero);
        $comprobante  = $doc->getElementsByTagName("TimbreFiscalDigital");
        foreach( $comprobante as $sat )
        {
            
            $Fecha      = substr($sat->getAttribute('FechaTimbrado'),0,10); 
            $UUID               = $sat->getAttribute('UUID');
            
        }  
        $comprobante  = $doc->getElementsByTagName("Comprobante");
        foreach( $comprobante as $sat )
        {
            $Total= $sat->getAttribute('Total');
           	$SubTotal= $sat->getAttribute('SubTotal');
			$Moneda = $sat->getAttribute('Moneda');
			$Folio      = $sat->getAttribute('Folio'); 
			$SatClaveFormaDePago  = $sat->getAttribute('FormaPago'); 
			
        }
        $comprobante  = $doc->getElementsByTagName("Emisor");
        foreach( $comprobante as $sat )
        {
            $Nombre= $sat->getAttribute('Nombre');
           	$Rfc= $sat->getAttribute('Rfc');
			
        }
        $comprobante  = $doc->getElementsByTagName("Impuestos");
        foreach( $comprobante as $sat )
        {
            $TotalImpuestosTrasladados= $sat->getAttribute('TotalImpuestosTrasladados');
           	$TotalImpuestosRetenidos= $sat->getAttribute('TotalImpuestosRetenidos');
			
        }  
        $comprobante  = $doc->getElementsByTagName("Traslado");
        foreach( $comprobante as $sat )
        {
            $TasaOCuota= $sat->getAttribute('TasaOCuota');
    		
        }  
        $contador=0;
        $comprobante  = $doc->getElementsByTagName("Concepto");
        foreach( $comprobante as $sat )
        {
        	$contador++;
        	if ($contador==1)
            $Descripcion= $sat->getAttribute('Descripcion');
    		
        } 
        $SatNombreFormaDePago="EFECTIVO";
        $sql="SELECT descripcion as SatNombreFormaDePago FROM sat_forma_de_pago  where clave='$SatClaveFormaDePago' ";
		$rs= ejecutarConsulta($sql);
		while($fila = mysqli_fetch_assoc( $rs ) )
		{
			$SatNombreFormaDePago= $fila['SatNombreFormaDePago'];	
		}
	    //buscar proveedores
	    /*$IdProveedor=0;
		$sql="SELECT id as IdProveedor FROM proveedores  where rfc='$Rfc' ";
		$rs= ejecutarConsulta($sql);
		while($fila = mysqli_fetch_assoc( $rs ) )
		{
			$IdProveedor= $fila['IdProveedor'];	
		}
		if (empty($proveedor)) // registro rapido de proveedor
		{
			$sql="INSERT INTO proveedores (descripcion,nomComercial,rfc)	
						  VALUES ('$Nombre','$Nombre','$Rfc')";
		
			$IdProveedor= ejecutarConsulta_retornarID($sql);
			
		}*/

        

        $sql="SELECT '$UUID' as UUID ,'$Folio' as Folio,'$Fecha' as Fecha,'$SubTotal' as SubTotal,'$Total' as Total,
        '$Moneda' as Moneda,'$Nombre' as  Nombre,'$Rfc' as  Rfc,'$TotalImpuestosTrasladados' as TotalImpuestosTrasladados,
        '$TotalImpuestosRetenidos' as TotalImpuestosRetenidos,'$Folio' as Folio,'$TasaOCuota' as TasaOCuota,
        '$Descripcion' as Descripcion,'$SatClaveFormaDePago' as SatClaveFormaDePago,'$SatNombreFormaDePago' as SatNombreFormaDePago " ;

        //$sql="select '$UUID' as UUID "; 
        //echo "$sql";
        return ejecutarConsultaSimpleFila($sql);   
        

	}
	
	
	
}

?>