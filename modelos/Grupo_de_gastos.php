<?php 
//Incluímos inicialmente la conexión a la base de datos 
require "../config/Conexion.php";

Class Grupo_de_gastos
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($nombre)
	{
		$sql="INSERT INTO grupo_de_gastos (descripcion,condicion)
		VALUES ('$nombre','1')";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idgrupo_de_gastos,$nombre)
	{
		$sql="UPDATE grupo_de_gastos SET descripcion='$nombre' WHERE id='$idgrupo_de_gastos'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idgrupo_de_gastos)
	{
		$sql="UPDATE grupo_de_gastos SET condicion='0' WHERE id='$idgrupo_de_gastos'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idgrupo_de_gastos)
	{
		$sql="UPDATE grupo_de_gastos SET condicion='1' WHERE id='$idgrupo_de_gastos'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idgrupo_de_gastos)
	{
		$sql="SELECT id as idgrupo_de_gastos,descripcion as nombre  FROM grupo_de_gastos WHERE id='$idgrupo_de_gastos'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar($nombre)
	{
		
		$sql="SELECT id as idgrupo_de_gastos,descripcion as nombre,condicion FROM grupo_de_gastos  where descripcion like '%$nombre%' ";
		//echo "$sql";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select()
	{
		$sql="SELECT id as idgrupo_de_gastos,descripcion as nombre FROM grupo_de_gastos where condicion=1";
		return ejecutarConsulta($sql);		
	}
	public function selectporid($idgrupo_de_gastos)
	{
		$sql="SELECT id as idgrupo_de_gastos,descripcion as nombre  FROM grupo_de_gastos where id=$idgrupo_de_gastos ";
		
		return ejecutarConsulta($sql);		
	}
}

?>