<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Ivas
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function guardar($Opcion,$idivas,$nombre)
	{
		if ($Opcion=="I")
			$sql="INSERT INTO ivas (descripcion,condicion) VALUES ('$nombre','1')";
		if ($Opcion=='U')
			$sql="UPDATE ivas SET descripcion='$nombre' WHERE id='$idivas'";
		if ($Opcion=='D')
			$sql="DELETE FROM  ivas  WHERE id='$idivas'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idivas,$nombre)
	{
		$sql="UPDATE ivas SET descripcion='$nombre' WHERE id='$idivas'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idivas)
	{
		$sql="UPDATE ivas SET condicion='0' WHERE id='$idivas'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idivas)
	{
		$sql="UPDATE ivas SET condicion='1' WHERE id='$idivas'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idivas)
	{
		$sql="SELECT id as idivas,descripcion as nombre FROM ivas WHERE id='$idivas'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar($nombre)
	{
		if (strlen($nombre)<3)
		{
			$nombre="XXX";
		}
		
		$q= " where descripcion like '%$nombre%' ";
		

		$sql="SELECT id as idivas,descripcion as nombre,condicion FROM ivas  $q";
		
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select($nombre)
	{
		$sql="SELECT id as idivas,descripcion as nombre FROM ivas where condicion=1 and descripcion like '%$nombre%'";
		return ejecutarConsulta($sql);		
	}
	public function selectporid($idivas)
	{
		$sql="SELECT id as idivas,descripcion as nombre  FROM ivas where id=$idivas ";
		
		return ejecutarConsulta($sql);		
	}
}

?>