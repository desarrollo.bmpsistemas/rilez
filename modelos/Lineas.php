<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Lineas
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($nombre)
	{
		$sql="INSERT INTO lineas (descripcion,condicion)
		VALUES ('$nombre','1')";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idlineas,$nombre)
	{
		$sql="UPDATE lineas SET descripcion='$nombre' WHERE id='$idlineas'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idlineas)
	{
		$sql="UPDATE lineas SET condicion='0' WHERE id='$idlineas'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idlineas)
	{
		$sql="UPDATE lineas SET condicion='1' WHERE id='$idlineas'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idlineas)
	{
		$sql="SELECT id as idlineas,descripcion as nombre FROM lineas WHERE id='$idlineas'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar()
	{
		$sql="SELECT id as idlineas, descripcion as nombre,condicion  FROM lineas";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select()
	{
		$sql="SELECT id as idlineas,descripcion as nombre FROM lineas where condicion=1";
		return ejecutarConsulta($sql);		
	}
	public function selectlineas()
	{
		$sql="SELECT * FROM sublineas where condicion=1";
		return ejecutarConsulta($sql);		
	}
	
}

?>