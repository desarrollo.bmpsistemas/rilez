<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Minimos_maximos
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($nombre)
	{
		$sql="INSERT INTO maximos_minimos (nombre,condicion)
		VALUES ('$nombre','1')";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idmaximos_minimos,$nombre)
	{
		$sql="UPDATE maximos_minimos SET nombre='$nombre' WHERE idmaximos_minimos='$idmaximos_minimos'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idmaximos_minimos)
	{
		$sql="UPDATE maximos_minimos SET condicion='0' WHERE idmaximos_minimos='$idmaximos_minimos'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idmaximos_minimos)
	{
		$sql="UPDATE maximos_minimos SET condicion='1' WHERE idmaximos_minimos='$idmaximos_minimos'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idmaximos_minimos)
	{
		$sql="SELECT * FROM maximos_minimos WHERE idmaximos_minimos='$idmaximos_minimos'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar($nombre)
	{
		if (strlen($nombre)<3)
		{
			$nombre="XXX";
		}
		
		$q= " where nombre like '%$nombre%' ";
		

		$sql="SELECT * FROM maximos_minimos  $q";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select($nombre)
	{
		$sql="SELECT * FROM maximos_minimos where condicion=1 and nombre like '%$nombre%'";
		return ejecutarConsulta($sql);		
	}
	public function selectporid($idmaximos_minimos)
	{
		$sql="SELECT idmaximos_minimos,nombre  FROM maximos_minimos where idmaximos_minimos=$idmaximos_minimos ";
		
		return ejecutarConsulta($sql);		
	}
}

?>