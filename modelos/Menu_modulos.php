<?php 
//Incluímos inicialmente la conexión a la base de datos 
require "../config/Conexion.php";

Class Menu_modulos
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($idmenus,$idmodulos)
	{
		$sql="INSERT INTO menu_modulos (menu,modulo)
		VALUES ('$idmenus','$idmodulos')";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idmenumodulos,$idmenus,$idmodulos) 
	{
		$sql="UPDATE menu_modulos SET menu='$idmenus',modulo='$idmodulos' WHERE idmenumodulos='$idmenumodulos'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar registros
	public function desactivar($idmenumodulos)
	{
		$sql="DELETE FROM  menu_modulos  WHERE id='$idmenumodulos'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar registros
	public function activar($idmenumodulos)
	{
		$sql="DELETE FROM  menu_modulos  WHERE id='$idmenumodulos'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idmenumodulos)
	{
		/*$sql="SELECT modulo.id AS idmodulos,menu.id AS idmenus,menu.descripcion AS menu,modulo.nombre,modulo.descripcion,modulo.condicion,modulo.orden FROM modulo
		INNER JOIN menu ON menu.id=modulo.menu_id WHERE modulo.id=$idmodulos";*/
		return 0; //ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar($idmenus)
	{
			
		$sql="SELECT menu_modulos.id AS idmenumodulos,modulo.descripcion,modulo.nombre AS modulo FROM menu_modulos
		INNER JOIN modulo ON modulo.id=menu_modulos.modulo 
		WHERE menu_modulos.menu=$idmenus ";
		//echo "$sql";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros ACTIVOS
	public function listarActivos() 
	{
		$sql="SELECT modulo.id as idmodulo,menu.id as idmenus,menu.descripcion AS menu,modulo.nombre AS modulo,modulo.condicion,modulo.orden FROM modulo
		INNER JOIN menu ON menu.id=modulo.menu_id where modulo.condicion='1' ";
		return ejecutarConsulta($sql);		
	}
	
	//Implementar un método para listar los registros activos, su último precio y el stock (vamos a unir con el último registro de la tabla detalle_ingreso)
	public function selectmodulo()
	{
		$sql="SELECT idmodulo,nombre FROM modulo "; //where condicion=1
		return ejecutarConsulta($sql);		
	}
	public function select()
	{
		$sql="SELECT id as idmodulos,descripcion as nombre FROM modulo "; //where condicion=1
		return ejecutarConsulta($sql);		
	}
	public function selectpormenu($idmenus)
	{
		$sql="SELECT id as idmodulos,descripcion as nombre FROM modulo where id=$idmenus";
		return ejecutarConsulta($sql);		
	}
	
}

?>