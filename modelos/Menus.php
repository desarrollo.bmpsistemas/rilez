<?php 
//Incluímos inicialmente la conexión a la base de datos 
require "../config/Conexion.php";

Class Menus
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($nombre,$orden)
	{
		$sql="INSERT INTO menu (descripcion,orden,condicion)
		VALUES ('$nombre','$orden','1')";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idmenus,$nombre,$orden)
	{
		$sql="UPDATE menu SET descripcion='$nombre',orden='$orden' WHERE id='$idmenus'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idmenus)
	{
		$sql="DELETE FROM menu WHERE id='$idmenus'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idmenus)
	{
		$sql="DELETE FROM menu WHERE id='$idmenus'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idmenus)
	{
		$sql="SELECT id as idmenus,descripcion as nombre,orden  FROM menu WHERE id='$idmenus'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar()
	{
		$sql="SELECT id as idmenus,descripcion as nombre,condicion,orden FROM menu";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select()
	{
		$sql="SELECT id as idmenus,descripcion as nombre FROM menu where condicion=1";
		return ejecutarConsulta($sql);		
	}
}

?>