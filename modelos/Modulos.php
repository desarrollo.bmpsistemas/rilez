<?php 
//Incluímos inicialmente la conexión a la base de datos 
require "../config/Conexion.php";

Class Modulos
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($descripcion,$nombre,$orden)
	{
		$sql="INSERT INTO modulo (descripcion,nombre,orden,condicion)
		VALUES ('$descripcion','$nombre','$orden','1')";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idmodulos,$descripcion,$nombre,$orden)
	{
		$sql="UPDATE modulo SET descripcion='$descripcion',nombre='$nombre',orden='$orden' WHERE id='$idmodulos'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idmodulos)
	{
		$sql="DELETE FROM modulo WHERE id='$idmodulos'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idmodulos)
	{
		$sql="DELETE FROM modulo WHERE id='$idmodulos'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idmodulos)
	{
		$sql="SELECT id as idmodulos,descripcion,nombre,orden  FROM modulo WHERE id='$idmodulos'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar()
	{
		$sql="SELECT id as idmodulos,descripcion,nombre,condicion,orden FROM modulo";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select()
	{
		$sql="SELECT id as idmodulos,nombre FROM modulo where condicion=1";
		return ejecutarConsulta($sql);		
	}
}

?>