<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Monedas
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($nombre,$abreviatura,$valor)
	{
		$sql="INSERT INTO monedas (descripcion,abreviatura,valor,condicion)
		VALUES ('$nombre','$abreviatura','$valor','1')";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idmonedas,$nombre,$abreviatura,$valor)
	{
		$sql="UPDATE monedas SET descripcion='$nombre',abreviatura='$abreviatura',valor='$valor' WHERE id='$idmonedas'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idmonedas)
	{
		$sql="UPDATE monedas SET condicion='0' WHERE id='$idmonedas'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idmonedas)
	{
		$sql="UPDATE monedas SET condicion='1' WHERE id='$idmonedas'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idmonedas)
	{
		$sql="SELECT id as idmonedas,descripcion as nombre,abreviatura,valor FROM monedas WHERE id ='$idmonedas'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar()
	{
		$sql="SELECT id as idmonedas,descripcion as nombre,abreviatura,valor,condicion FROM monedas  ";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select()
	{
		$sql="SELECT id as idmonedas,descripcion as nombre FROM monedas where condicion=1 ";
		return ejecutarConsulta($sql);		
	}
	public function formasdepagos()
	{
		$sql="SELECT id as idformasdepagos,descripcion as nombre FROM forma_de_pago where condicion=1 ";
		return ejecutarConsulta($sql);		
	}
	public function selectporid($idmonedas)
	{
		$sql="SELECT id as idmonedas,descripcion as nombre  FROM monedas where id=$idmonedas ";
		
		return ejecutarConsulta($sql);		
	}
}

?>