<?php 
//Incluímos inicialmente la conexión a la base de datos 
require "../config/Conexion.php";

Class Municipios
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($idestados,$nombre)
	{
		$sql="INSERT INTO municipios (estado_id,descripcion,condicion)
		VALUES ('$idestados','$nombre','1')";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idmunicipios,$idestados,$nombre)
	{
		$sql="UPDATE municipios SET estado_id='$idestados',descripcion='$nombre' WHERE id='$idmunicipios'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar registros
	public function desactivar($idmunicipios)
	{
		$sql="UPDATE municipios SET condicion='0' WHERE id='$idmunicipios'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar registros
	public function activar($idmunicipios)
	{
		$sql="UPDATE municipios SET condicion='1' WHERE id ='$idmunicipios'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idmunicipios)
	{
		$sql="SELECT p.id as idpaises,e.id as idestados,m.id as idmunicipios,p.descripcion AS pais,
		e.descripcion  AS estado,m.descripcion as nombre ,m.condicion FROM municipios m 
		INNER JOIN estados e ON e.id=m.estado_id
		INNER JOIN paises p ON p.id=e.pais_id WHERE m.id='$idmunicipios'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar($nombre)
	{
		if (strlen($nombre)<3)
			$nombre="xxx";

		$sql="SELECT p.id as idpaises,e.id as idestados,m.id as idmunicipios,p.descripcion AS pais,
		e.descripcion as estado,m.descripcion AS municipio,m.condicion FROM municipios m 
		INNER JOIN estados e ON e.id=m.estado_id
		INNER JOIN paises p ON p.id=e.pais_id where m.descripcion like '%$nombre%' ";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros ACTIVOS
	public function listarActivos()
	{
		$sql="SELECT p.id as idpaises,e.id as idestados,m.id as idmunicipios,p.descripcion as pais,
		e.descripcion as estado,m.descripcion as municipio,m.condicion FROM municipios m 
		INNER JOIN estados e ON e.id=m.estado_id
		INNER JOIN paises p ON p.id=e.pais_id where m.condicion='1' ";
		return ejecutarConsulta($sql);		
	}
	
	//Implementar un método para listar los registros activos, su último precio y el stock (vamos a unir con el último registro de la tabla detalle_ingreso)
	public function selectmunicipios($idestados)
	{
		if (!isset($idestados) || $idestados==0)
		$sql="SELECT id as idmunicipios,descripcion as nombre,estado_id as idestados  FROM municipios  ";
		else
		$sql="SELECT id as idmunicipios,descripcion as nombre,estado_id as idestados  FROM municipios where estado_id=$idestados ";	
		return ejecutarConsulta($sql);		
	}
	public function selectestados($idpaises)
	{
		if (!isset($idpaises) || $idpaises==0)
		$sql="SELECT id as idestados,descripcion as nombre,pais_id as idpaises  FROM estados  ";
		else
		$sql="SELECT id as idestados,descripcion as nombre,pais_id as idpaises  FROM estados  where pais_id=$idpaises";
		return ejecutarConsulta($sql);		
	}
	public function selectpaises()
	{
		$sql="SELECT id as idpaises,descripcion as nombre  FROM paises  ";
		
		return ejecutarConsulta($sql);		
	}
	public function select()
	{
		$sql="SELECT id as idmunicipios,descripcion as nombre  FROM municipios ";
		
		return ejecutarConsulta($sql);		
	}
	public function selectporid($idmunicipios)
	{
		$sql="SELECT id as idmunicipios,descripcion as nombre  FROM municipios where id=$idmunicipios ";
		
		return ejecutarConsulta($sql);		
	}
	public function buscarMunicios($nombre)
	{
		$sql="SELECT id as idmunicipios,descripcion as nombre  FROM municipios where descripcion like '%$nombre%'  ";
		
		return ejecutarConsulta($sql);		
	}
}

?>