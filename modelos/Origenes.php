 <?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Origenes
{
	//Implementamos nuestro constructor 
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
							
	public function insertar($idalmacenes,$idusuarios,$idcatdeorigenes,$fecha,$importe,$nota)
	{
		$id1=0;$id2=0;$id3=0;
		$sql="INSERT INTO origenes (almacen_id,   capturista_id,cat_de_origenes_id,      fecha, importe,   nota)
		                   VALUES ('$idalmacenes','$idusuarios','$idcatdeorigenes','$fecha','$importe','$nota')";
		$id1=ejecutarConsulta_retornarID($sql);
		if ($id1>0)
		{
			$sql="SELECT id FROM fondos where cat_de_origenes_id=$idcatdeorigenes";
			$rs= ejecutarConsulta($sql);
			while($fila = mysqli_fetch_assoc( $rs ) )
			{
				$id2= $fila['id'];
			}
			if (empty($id2) || $id2==0)
			{
				$sql="INSERT INTO fondos (cat_de_origenes_id,entradas,salidas)
		                   VALUES ('$idcatdeorigenes','$importe',0)";
				$id2=ejecutarConsulta_retornarID($sql);
			}
			else
			{
				$sql="UPDATE fondos SET entradas=entradas+'$importe' where id=$id2";
				$id2=ejecutarConsulta_retornarID($sql);	
			}
		}
		
		if (empty($id1))
			return $sql;
		else
			return $id1;
	}

	//Implementamos un método para editar registros
	public function editar($idorigenes,$idalmacenes,$idusuarios,$idcatdeorigenes,$fecha,$importe,$nota)
	{
		/*$sql="UPDATE origenes set almacen_id='$idalmacenes',capturista_id=$idusuarios,cat_de_origenes_id='$idcatdeorigenes', fecha='$fecha',importe='$importe',nota='$nota' WHERE id='$idorigenes'";
		$id= ejecutarConsulta($sql);





		if ($id)
		return $id;
		else
		return $sql;*/	
	}

	


	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idorigenes) 
	{
		$sql="SELECT * FROM origenes WHERE id='$idorigenes'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros 
	public function listar($fecha)
	{
		
		$sql="SELECT origenes.id as idorigenes,cat_de_origenes.descripcion as nombre,almacenes.nombre as almacen,fecha,importe,nota,origenes.condicion FROM origenes
		inner join cat_de_origenes on cat_de_origenes.id=origenes.cat_de_origenes_id
		inner join almacenes on almacenes.id=origenes.almacen_id
		WHERE month(origenes.fecha) = month('$fecha') and year(origenes.fecha)= year('$fecha') ";
		//echo "$sql";
		return ejecutarConsulta($sql);	 	
	}
	public function listarDestinos($idorigenes)
	{
		
		$sql="SELECT detalles_destinos.id AS iddet_destinos,fecha,importe,destinos.descripcion AS destino,nota,capturista_id,detalles_destinos.condicion FROM detalles_destinos
		INNER JOIN destinos ON destinos.id=detalles_destinos.destinos_id
		WHERE detalles_destinos.origenes_id=$idorigenes";
		//echo "$sql";
		return ejecutarConsulta($sql);	 	
	}

	//Implementar un método para listar los registros
	
	public function select($Opcion,$referencia) // seleccionar proveedor
	{
		if ($Opcion==0)
			$q=" where id = '$referencia' ";
		if ($Opcion==1)
			$q=" where rfc like '%$referencia%' ";
		if ($Opcion==2)
			$q=" where descripcion like '%$referencia%' ";
		if ($Opcion==3)
			$q=" where telefono like '%$referencia%' ";
		

		$sql="SELECT id AS idproveedores,CONCAT(SUBSTRING(descripcion,1,30),' - ',nomComercial) AS nombre FROM proveedores   $q "; 
		return ejecutarConsulta($sql);		
	}
	
	public function selectCat_de_Origenes() // seleccionar proveedor
	{
		$sql="SELECT id AS idcat_de_origenes,descripcion AS nombre FROM cat_de_origenes order by id"; 
		return ejecutarConsulta($sql);		
	}
	
	public function selectDestinos() // seleccionar proveedor
	{
		$sql="SELECT id AS iddestinos,descripcion AS nombre FROM destinos order by id"; 
		return ejecutarConsulta($sql);		
	}
	//Implementamos un método para desactivar categorías
	public function desactivar($idorigenes)
	{
		$idcat_de_origenes=0;
		$sql="SELECT cat_de_origenes_id,importe FROM origenes where id=$idorigenes";
		$rs= ejecutarConsulta($sql);
		while($fila = mysqli_fetch_assoc( $rs ) )
		{
			$idcat_de_origenes= $fila['cat_de_origenes_id'];
			$importe= $fila['importe'];
		}

		if ($idcat_de_origenes>0 && $importe>0)
		{
			$sql="UPDATE fondos SET entradas=entradas-$importe where idcat_de_origenes=$idcat_de_origenes";
			$id2=ejecutarConsulta_retornarID($sql);	
		}

		$sql="DELETE FROM origenes  WHERE id='$idorigenes'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idproveedores)
	{
		$sql="UPDATE proveedores SET condicion='1' WHERE id='$idproveedores'";
		return ejecutarConsulta($sql);
	}
}

?>
