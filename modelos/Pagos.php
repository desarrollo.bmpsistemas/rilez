<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Pagos
{
	//Implementamos nuestro constructor 
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($ident_sal,$fecha,$referencia,$idformadepagos,$importe,$idusuarios)
	{		
	
		$sql = "SELECT id  FROM cxc   WHERE salida_id=$ident_sal order by id desc limit 1"; 
        $consulta= ejecutarConsulta($sql);
       	while($rs = mysqli_fetch_array($consulta)) 
       	$id= $rs['id'];
        if (empty($id)) 
        	$id=1;
        else
        	$id++;

		$sql="INSERT INTO cxc (salida_id,       id,       fecha,referencia,tipo_de_pago_id,importe,capturista_id)
						  VALUES ('$ident_sal',$id,'$fecha','$referencia','$idformadepagos','$importe','$idusuarios')";
		$r= ejecutarConsulta($sql);

		//obtener sumatoria para gregarlos a ent_sal prepagos
		$sql = "SELECT sum(importe) as abonos   FROM cxc   WHERE salida_id=$ident_sal"; 
        $consulta= ejecutarConsulta($sql);
       	$i=0;
		if(mysqli_num_rows($consulta) > 0)
		{
			
			while($rs = mysqli_fetch_array($consulta)) 
			$abonos= round($rs['abonos'],2);
			if (empty($abonos)) $abonos=0;
			$sql = "UPDATE salidas set abonos=$abonos   WHERE id=$ident_sal"; 
        	$consulta= ejecutarConsulta($sql);
		}
		
  		return $consulta;
		
		
	}
	public function editar($idpagos,$ident_sal,$fecha,$referencia,$idformadepagos,$importe,$idusuarios,$np)
	{
		$sql="UPDATE cxc SET 
		fecha='$fecha',referencia='$referencia',tipo_de_pago_id='$idformadepagos',importe='$importe'		
		WHERE salida_id='$ident_sal' and id= $np  ";
		$r= ejecutarConsulta($sql);
		if (empty($r)) $r=0;
  		return $r;
	}
  		

		
		
	
	public function mostrarPagos($idpagos,$ident_sal)
	{
		$sql="SELECT cxc.id AS idpagos,cxc.salida_id AS ident_sal,cxc.fecha,cxc.importe,cxc.referencia,estatus.descripcion AS estatus,
		capturistas.descripcion AS usuario,tipos_de_pagos.descripcion as tipo_de_pago FROM cxc
		INNER JOIN estatus ON estatus.id=cxc.estatus_id
		INNER JOIN tipos_de_pagos ON tipos_de_pagos.id = cxc.tipo_de_pago_id
		INNER JOIN capturistas ON capturistas.id=cxc.capturista_id
		WHERE cxc.salida_id= $ident_sal";
		//echo " $sql";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idpagos)
	{
		$sql="UPDATE pagos SET condicion='0' WHERE idpagos='$idpagos' ";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idpagos)
	{
		$sql="UPDATE pagos SET condicion='1' WHERE idpagos='$idpagos' ";
		return ejecutarConsulta($sql);
	}
	public function activarOpciones($idusuarios,$clave)
	{
		
		$clave=md5($clave);
		$sql="SELECT roles_id as rol FROM capturistas WHERE id='$idusuarios' and  clave =  '$clave' and estatus_id<>3 ";
		//return ejecutarConsultaSimpleFila($sql);
		//echo $sql;
		$rol=0;
		$rs= ejecutarConsulta($sql);
       	while($fila = mysqli_fetch_array($rs)) 
       	{
			$rol= $fila['rol'];
		}
		return $rol;
		
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idpagos)
	{
		$sql="SELECT idpagos,idpagos,idarticulos,stock,idmonedas,costo,precio,ivas.idivas,porcentaje_utilidad,ubicacion,pagos.condicion FROM pagos
		INNER JOIN pagos ON ivas.nombre=pagos.iva  WHERE idpagos='$idpagos' ";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar($Opcion,$referencia,$fecha_inicial,$fecha_final,$ident_sal,$idpersonas,$tipo,$saldo)
	{
		if ( !isset($Opcion))
		 $q=" id=0";
		else
		if ($Opcion==1)
			$q=" salidas.id=$ident_sal";
		else
		if ($Opcion==2)
		{
			if ($saldo>0)
				$q=" salidas.cliente_id=$idpersonas and fecha between '$fecha_inicial' and  '$fecha_final' and total-abonos >0 ";
			else
				$q=" salidas.cliente_id=$idpersonas and fecha between '$fecha_inicial' and  '$fecha_final' and total-abonos <= 0 ";
		}
		else
		if ($Opcion==3 || $Opcion==4 )	
		{
			if ($saldo>0)
				$q=" fecha between '$fecha_inicial' and  '$fecha_final' and total-abonos >0 ";
			else
				$q=" fecha between '$fecha_inicial' and  '$fecha_final' and total-abonos <= 0 ";	
		}
		

		$sql="SELECT clientes.descripcion AS nombre ,salidas.id AS ident_sal,fecha,total,abonos,total-abonos AS saldo,estatus.descripcion as estatus FROM salidas	
		INNER JOIN clientes ON clientes.id = salidas.cliente_id
		INNER JOIN estatus ON estatus.id = salidas.estatus_id
		WHERE ".$q ."  ORDER BY fecha ";
		
		return ejecutarConsulta($sql);		
	}
	public function leerTemporalXml($archivo)
	{
		$sql="SELECT articulos.idarticulos,ValorUnitario as costo FROM temporal_xml 
		INNER JOIN articulos ON articulos.codigo=temporal_xml.NoIdentificacion
		WHERE temporal_xml.nombre='$archivo' ";
		return ejecutarConsulta($sql);
	}
	public function leerUtilidad($costo)
	{
		$sql="SELECT porciento FROM utilidades
		WHERE $costo>=lim_inf AND $costo<=lim_sup ";
		return ejecutarConsulta($sql);			      
	}
	public function leerIva_y_Moneda($idpagos)
	{
		$sql="SELECT idivas,idmonedas FROM pagos WHERE idpagos=$idpagos ";
		return ejecutarConsultaSimpleFila($sql);			      
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select()
	{
		$sql="SELECT idpagos,nombre FROM pagos where condicion=1";
		return ejecutarConsulta($sql);		
	}
	
}

?>