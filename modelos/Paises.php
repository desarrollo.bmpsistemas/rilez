<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Paises
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($nombre)
	{
		$sql="INSERT INTO paises (descripcion,condicion)
		VALUES ('$nombre','1')";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idpaises,$nombre)
	{
		$sql="UPDATE paises SET descripcion='$nombre' WHERE id='$idpaises'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idpaises)
	{
		$sql="UPDATE paises SET condicion='0' WHERE id='$idpaises'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idpaises)
	{
		$sql="UPDATE paises SET condicion='1' WHERE id='$idpaises'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idpaises)
	{
		$sql="SELECT id as idpaises,descripcion as nombre FROM paises WHERE id='$idpaises'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar()
	{
		$sql="SELECT id as idpaises,descripcion as nombre,condicion FROM paises";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select()
	{
		$sql="SELECT id as idpaises,descripcion as nombre FROM paises where condicion=1";
		return ejecutarConsulta($sql);	
			
	}
}

?>