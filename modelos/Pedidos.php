<?php 
//Incluímos inicialmente la conexión a la base de datos 
require "../config/Conexion.php";

Class Pedidos
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function Anexo($ident_sal,$cliente,$conatencion,$asunto,$imagen)
	{
		$id=0;

		$sql="SELECT salida_id FROM anexos where salida_id=$ident_sal";
		$rs= ejecutarConsulta($sql);
		while($fila = mysqli_fetch_assoc( $rs ) )
		{
			$id= $fila['salida_id'];	
		}

		//$sql="SELECT ident_sal FROM anexos where ident_sal=$ident_sal";
		//$id=ejecutarConsulta_retornarID($sql);
		if (empty($id))
	    $sql="INSERT INTO anexos (salida_id,cliente,conatencion,asunto,imagen)  VALUES ('$ident_sal','$cliente','$conatencion','$asunto','$imagen')";
	    else
	    {
		    if (strlen($imagen>4))	
		 		$sql="UPDATE anexos set cliente='$cliente',conatencion='$conatencion',asunto='$asunto',imagen='$imagen' where salida_id=$ident_sal ";   	
		 	else
		 		$sql="UPDATE anexos set cliente='$cliente',conatencion='$conatencion',asunto='$asunto' where salida_id=$ident_sal ";   	
	 	}
       	return ejecutarConsulta($sql);
	}
	public function buscarAnexo($ident_sal)
	{
		
		$sql="SELECT * FROM anexos where salida_id=$ident_sal";
       	return ejecutarConsultaSimpleFila($sql);
	}
							 //$fecha,$idalmacenes,$idtipo_movs,$idpersonas,$idusuarios,$idarticulos,$valor,$descuento,$idmonedas,$valor_moneda,$iva,$cantidad,$tipo_mov,$concepto	
	public function insertar($fecha,$idalmacenes,$idalmacenes2,$idtipo_movs,$idpersonas,$idpersonas2,$idusuarios,$idarticulos,$valor,$descuento,$idmonedas,$valor_moneda,$iva,$cantidad,$tipo_mov,$concepto)
	{
		$id=0;


		
	    	$sql="INSERT INTO detalles_tmp_entradas (fecha,    almacen_id,almacen_origen,  tipo_mov_id,   proveedor_id,capturista_id,      articulo_id,       costo,    descuento,   moneda_id,valor_moneda,   iva,   cantidad,concepto,condicion)
					                      VALUES ('$fecha','$idalmacenes','$idalmacenes2','$idtipo_movs','$idpersonas','$idusuarios',   '$idarticulos','$valor','$descuento','$idmonedas','$valor_moneda','$iva','$cantidad','$concepto','1')";
       
			$id=ejecutarConsulta_retornarID($sql);
			if ($id>0)
				return ($id);
			else
				return ($sql);
				
		
	}
	
	//Implementamos un método para editar registros $idarticulos_proveedores,$costo,$NoIdentificacion

	public function editar($fecha,$iddet_ent_sal,$idarticulos,$valor,$descuento,$idmonedas,$valor_moneda,$iva,$cantidad,$tipo_mov,$concepto)
	{
	
		if ($tipo_mov=='E')	  	
      		$sql="UPDATE det_ent_sal_tmp SET idarticulos='$idarticulos',costo='$valor',descuento=$descuento,moneda_id='$idmonedas',valor_moneda='$valor_moneda',cantidad='$cantidad',iva='$iva',concepto='$concepto' WHERE iddet_ent_sal='$iddet_ent_sal' ";
      	else
      		$sql="UPDATE det_ent_sal_tmp SET idarticulos='$idarticulos',precio='$valor',descuento=$descuento,moneda_id='$idmonedas',valor_moneda='$valor_moneda',cantidad='$cantidad',iva='$iva',concepto='$concepto' WHERE iddet_ent_sal='$iddet_ent_sal' ";
      	//echo "$sql";
		return ejecutarConsulta($sql);
	}	
		
	

	//Implementamos un método para desactivar categorías
	public function desactivar($iddet_ent_sal,$tipo_mov)
	{
		if ($tipo_mov=="E")
		$sql="DELETE FROM  detalles_tmp_entradas WHERE id='$iddet_ent_sal' ";
		else
		$sql="DELETE FROM  detalles_tmp_salidas WHERE id='$iddet_ent_sal' ";	
		return ejecutarConsulta($sql);
	}


	//Implementamos un método para activar categorías
	public function activar($iddet_ent_sal)
	{
		$sql="DELETE FROM  det_ent_sal_tmp  WHERE iddet_ent_sal='$iddet_ent_sal' ";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($iddet_ent_sal,$tipo_mov)
	{
		if ($tipo_mov=='E')
		{
			$sql="SELECT detalles_tmp_entradas.costo AS valor,detalles_tmp_entradas.cantidad FROM detalles_tmp_entradas
	  		WHERE detalles_tmp_entradas.id='$iddet_ent_sal'	 ";
  		}
  		else
  		{
  			$sql="SELECT detalles_tmp_salidas.precio AS valor,detalles_tmp_salidas.cantidad FROM detalles_tmp_salidas
	  		WHERE detalles_tmp_salidas.id='$iddet_ent_sal'	 ";
  		}	
		return ejecutarConsultaSimpleFila($sql);
	}
	
	public function cancelarCaptura($idalmacenes,$idtipo_movs,$idpersonas,$idusuarios,$tipo_mov)
	{
		if ($tipo_mov=="E")
			$sql="DELETE FROM  FROM detalles_tmp_entradas WHERE proveedor_id='$idpersonas' AND capturista_id='$idusuarios' AND almacen_id='$idalmacenes' AND tipo_mov_id='$idtipo_movs' ";
  		else
  			$sql="DELETE FROM detalles_tmp_salidas	WHERE cliente_id='$idpersonas' AND capturista_id='$idusuarios' AND almacen_id='$idalmacenes' AND tipo_mov_id='$idtipo_movs'  ";
  		//echo "$sql";	
		return ejecutarConsulta($sql);
		//return ejecutarConsulta($sql);	
	}

	public function calcularEntSal($idalmacenes,$idtipo_movs,$idpersonas,$idusuarios,$tipo_mov)
	{
		if ($tipo_mov=="E")
			$sql="SELECT SUM(IF(descuento>0,cantidad*costo*descuento,cantidad*costo)) AS subtotal, 
				SUM(IF(descuento>0,cantidad*costo*descuento*iva,cantidad*costo*iva)) AS iva FROM detalles_tmp_entradas
  			WHERE proveedor_id='$idpersonas' AND capturista_id='$idusuarios' AND almacen_id='$idalmacenes' AND tipo_mov_id='$idtipo_movs' ";
  		else
  			$sql="SELECT SUM(IF(descuento>0,cantidad*precio*descuento,cantidad*precio)) AS subtotal, 
				SUM(IF(descuento>0,cantidad*precio*descuento*iva,cantidad*precio*iva)) AS iva
				FROM detalles_tmp_salidas
  				WHERE cliente_id='$idpersonas' AND capturista_id='$idusuarios' AND almacen_id='$idalmacenes' AND tipo_mov_id='$idtipo_movs'  ";
  		//echo "$sql";	
		return ejecutarConsultaSimpleFila($sql);
		//return ejecutarConsulta($sql);	
	}
	
	public function actualizarCantidad($iddet_ent_sal,$cantidad,$valor,$tipo_mov)
	{
		if ($tipo_mov=='E')
		$sql="UPDATE detalles_tmp_entradas SET cantidad=$cantidad,costo=$valor 	WHERE  detalles_tmp_entradas.id='$iddet_ent_sal'	";
		else
		$sql="UPDATE detalles_tmp_salidas SET cantidad=$cantidad,precio=$valor 	WHERE  detalles_tmp_salidas.id='$iddet_ent_sal'	";
			
		return  ejecutarConsulta($sql);
		
	}
	public function aplicarDescuento($iddet_ent_sal,$descuento,$tipo_mov)
	{
		if ($tipo_mov=='S')
		{
			$sql="UPDATE detalles_tmp_salidas SET descuento=$descuento 	WHERE  id='$iddet_ent_sal' ";
			return ejecutarConsulta($sql);
		}
		else
			return 0;
		
	}

	public function procesarIva($iva,$idalmacenes,$idtipo_movs,$idpersonas,$idusuarios,$tipo_mov)
	{
		if ($tipo_mov=='S')
		$sql="UPDATE detalles_tmp_salidas SET iva=$iva 	WHERE cliente_id=$idpersonas and capturista_id=$idusuarios and almacen_id='$idalmacenes' and tipo_mov_id='$idtipo_movs' ";
		else
		$sql="UPDATE detalles_tmp_entradas SET iva=$iva 	WHERE proveedor_id=$idpersonas and capturista_id=$idusuarios and almacen_id='$idalmacenes' and tipo_mov_id='$idtipo_movs' ";
		//return registrosProcesados($sql);	
		return ejecutarConsulta($sql);
		
	}
	public function procesarMoneda($idmonedas,$idalmacenes,$idtipo_movs,$idpersonas,$idusuarios,$tipo_mov)
	{
		if ($idmonedas==2)
		{
			$sql="SELECT valor  FROM detalles_monedas order by fecha desc limit 1";
			$rs= ejecutarConsulta($sql);
			while($fila = mysqli_fetch_assoc( $rs ) )
			{
				$valorMoneda= $fila['valor'];	
			}
		}
		else
			$valorMoneda=1;
		//checar moneda
		if ($tipo_mov=='S')
		{
			$sql="SELECT moneda_id,valor_moneda  FROM detalles_tmp_salidas where cliente_id=$idpersonas and capturista_id=$idusuarios and almacen_id='$idalmacenes' and tipo_mov_id='$idtipo_movs' ";
			$rs= ejecutarConsulta($sql);
			while($fila = mysqli_fetch_assoc( $rs ) )
			{
				$ValorMoneda= $fila['valor_moneda'];
				$Moneda_id= $fila['moneda_id'];	
			}
		}	
		
		if ($tipo_mov=='S'  && ($idmonedas!=$Moneda_id)  )  // actualizar costo o precio por la conversion y conservar la moneda en procesos de cambios
		{
			if ($valorMoneda>0)
			{
				if ($idmonedas==1)
				$sql="UPDATE detalles_tmp_salidas SET precio=precio*valor_moneda,moneda_id=$idmonedas,valor_moneda=$valorMoneda	WHERE cliente_id=$idpersonas and capturista_id=$idusuarios and almacen_id='$idalmacenes' and tipo_mov_id='$idtipo_movs' ";	
				else
				$sql="UPDATE detalles_tmp_salidas SET precio=precio/$valorMoneda,moneda_id=$idmonedas,valor_moneda=$valorMoneda WHERE cliente_id=$idpersonas and capturista_id=$idusuarios and almacen_id='$idalmacenes' and tipo_mov_id='$idtipo_movs' ";			
			}
		}
		else
		if ($tipo_mov=='E'  && ($idmonedas!=$Moneda_id)  )
		{
			if ($valorMoneda>0)
			{
				if ($Moneda_id==1)
				$sql="UPDATE detalles_tmp_entradas SET costo=costo/$valorMoneda,moneda_id=$idmonedas,valorMoneda=$valorMoneda	WHERE cliente_id=$idpersonas and capturista_id=$idusuarios and almacen_id='$idalmacenes' and tipo_mov_id='$idtipo_movs' ";	
				else
				$sql="UPDATE detalles_tmp_entradas SET costo=costo*$valorMoneda,moneda_id=$idmonedas,valorMoneda=$valorMoneda	WHERE cliente_id=$idpersonas and capturista_id=$idusuarios and almacen_id='$idalmacenes' and tipo_mov_id='$idtipo_movs' ";			
			}
		}
		
			
		return ejecutarConsulta($sql);

	}
	public function valorMoneda()
	{
		$sql="SELECT valor as valormoneda FROM detalles_monedas order by fecha desc limit 1";
		return ejecutarConsulta($sql);	
	}


	//Implementar un método para listar los registros 
	public function listar($tipo_mov,$idalmacenes,$idtipo_movs,$idpersonas,$idusuarios)
	{	
		if ($tipo_mov=="E")	
			$sql="SELECT detalles_tmp_entradas.id AS iddet_ent_sal,articulos.id2 AS codigo,articulos.descripcion,
			detalles_tmp_entradas.costo AS valor,IF(detalles_tmp_entradas.descuento>0,detalles_tmp_entradas.costo-(detalles_tmp_entradas.costo*detalles_tmp_entradas.descuento),detalles_tmp_entradas.costo*detalles_tmp_entradas.descuento) AS descuento,detalles_tmp_entradas.cantidad,
			(detalles_tmp_entradas.costo*detalles_tmp_entradas.cantidad) AS importe,detalles_tmp_entradas.iva,detalles_tmp_entradas.condicion,
			detalles_tmp_entradas.concepto,IF(detalles_tmp_entradas.moneda_id=1,'P','D') AS moneda FROM detalles_tmp_entradas
	  		INNER JOIN articulos ON articulos.id=detalles_tmp_entradas.articulo_id
	  		WHERE detalles_tmp_entradas.proveedor_id='$idpersonas' AND detalles_tmp_entradas.capturista_id='$idusuarios' AND detalles_tmp_entradas.almacen_id='$idalmacenes' 
	  		AND detalles_tmp_entradas.tipo_mov_id='$idtipo_movs' ";
  		else
  			$sql="SELECT detalles_tmp_salidas.id AS iddet_ent_sal,articulos.id2 AS codigo,articulos.descripcion,
			detalles_tmp_salidas.precio AS valor,IF(detalles_tmp_salidas.descuento>0,detalles_tmp_salidas.precio-(detalles_tmp_salidas.precio*detalles_tmp_salidas.descuento),detalles_tmp_salidas.precio*detalles_tmp_salidas.descuento) AS descuento,detalles_tmp_salidas.cantidad,
			(detalles_tmp_salidas.precio*detalles_tmp_salidas.cantidad) AS importe,detalles_tmp_salidas.iva,detalles_tmp_salidas.condicion,
			detalles_tmp_salidas.concepto,IF(detalles_tmp_salidas.moneda_id=1,'P','D') AS moneda FROM detalles_tmp_salidas
	  		INNER JOIN articulos ON articulos.id=detalles_tmp_salidas.articulo_id
	  		WHERE detalles_tmp_salidas.cliente_id='$idpersonas' AND detalles_tmp_salidas.capturista_id='$idusuarios' AND detalles_tmp_salidas.almacen_id='$idalmacenes' 
	  		AND detalles_tmp_salidas.tipo_mov_id='$idtipo_movs'    ";
  		
		return ejecutarConsulta($sql);		
	}
	
	
	public function buscarEntSal($idalmacenes,$idtipo_movs,$idpersonas,$fecha_inicial,$fecha_final,$tipo_mov)
	{
		if ($tipo_mov=="E")
		$sql="SELECT entradas.id AS ident_sal,proveedores.descripcion AS persona, almacenes.nombre AS almacen,fecha,total,entradas.proveedor_id FROM entradas
		INNER JOIN proveedores ON proveedores.id=entradas.proveedor_id
		INNER JOIN almacenes ON almacenes.id =entradas.almacen_id
		WHERE  tipo_mov_id='$idtipo_movs' AND fecha BETWEEN '$fecha_inicial' AND '$fecha_final' ";

		else

		$sql="SELECT salidas.id AS ident_sal,clientes.descripcion AS persona, almacenes.nombre AS almacen,fecha,total,salidas.cliente_id FROM salidas
		INNER JOIN clientes ON clientes.id=salidas.cliente_id
		INNER JOIN almacenes ON almacenes.id =salidas.almacen_id
		WHERE  tipo_mov_id='$idtipo_movs' AND fecha BETWEEN '$fecha_inicial' AND '$fecha_final'  ";
		//echo $sql;	
		return ejecutarConsulta($sql);		
	}
	
	public function tomarIva($idivas)
	{
		$sql="SELECT nombre as iva from ivas where idivas=$idivas";
		return ejecutarConsulta($sql);		
	}
	public function traerIva_y_Moneda($idalmacenes)
	{
		$sql="SELECT idmonedas,idivas from almacenes where idalmacenes='$idalmacenes' ";
		return ejecutarConsultaSimpleFila($sql);
	}
	
	public function tipo_movs($idtipo_movs)
	{
		$sql="SELECT tipo from tipo_movs where idtipo_movs=$idtipo_movs";
		return ejecutarConsulta($sql);		
	}
								   
	public function insertarEntSal($idalmacenes,$idalmacenes2,$idtipo_movs,$idpersonas,$idusuarios,$fecha,$vencimiento,$idmonedas,$valor_de_moneda,$idformadepago,$Subtotal,$Iva,$Total,$ivaaplicado,$referencia,$tipo_mov,$observaciones)
	{
		$identificador=date("Y-m-d H:i:s");
		//$sql="SELECT id FROM salidas  where id ='$id' ";
		//return  ejecutarConsulta($sql);
		//while($fila = mysqli_fetch_assoc( $rs ) )
		//{
		//	$salida_id= $fila['id'];	
		//}

		if ($tipo_mov=="S")
	    $sql="INSERT INTO salidas (hora,             almacen_id,    almacen_destino,  tipo_mov_id, cliente_id,   capturista_id,    fecha,    vencimiento,     moneda_id, valor_de_moneda,   forma_de_pago,     subtotal,   iva,    total,   referencia,  ivaaplicado,condicion,observaciones)
				          VALUES ('$identificador','$idalmacenes',  '$idalmacenes2','$idtipo_movs','$idpersonas','$idusuarios','$fecha',  '$vencimiento',  '$idmonedas','$valor_de_moneda',$idformadepago,'$Subtotal','$Iva','$Total','$referencia','$ivaaplicado','1','$observaciones')";
		if ($tipo_mov=="E")
		$sql="INSERT INTO entradas (hora,             almacen_id,    almacen_origen,  tipo_mov_id, proveedor_id,  capturista_id,    fecha,    vencimiento,     moneda_id, valor_de_moneda, forma_de_pago,    subtotal,   iva,    total,   referencia,  ivaaplicado,condicion,observaciones)
				          VALUES ('$identificador','$idalmacenes','$idalmacenes2',   '$idtipo_movs','$idpersonas','$idusuarios',   '$fecha','$vencimiento','$idmonedas','$valor_de_moneda','$idformadepago','$Subtotal','$Iva','$Total','$referencia','$ivaaplicado','1','$observaciones')";
		//echo ($sql);		          
		return ejecutarConsulta_retornarID($sql);
		//$salida_id=0;

	}
	public function insertaTraspaso($salida_id,$almacen_id,$almacen_origen,$proveedor_id)
	{
		$sql="INSERT INTO entradas (almacen_id,                    almacen_origen,capturista_id,                   proveedor_id,        tipo_mov_id,fecha,vencimiento,forma_de_pago,moneda_id,valor_de_moneda,descuento,subtotal,iva,total,referencia,ivaaplicado,hora,condicion)
		   	   SELECT $almacen_id AS almacen_id,$almacen_origen AS almacen_origen,capturista_id, $proveedor_id as  proveedor_id,'17' AS tipo_mov_id,fecha,vencimiento,forma_de_pago,moneda_id,valor_de_moneda,descuento,subtotal,iva,total,referencia,ivaaplicado,hora,condicion FROM salidas WHERE id=$salida_id ";
		$entrada_id = ejecutarConsulta_retornarID($sql);
										
		$sql="INSERT INTO detalles_entradas (entrada_id,mov,articulo_id,moneda_id,costo,descuento,iva,valor_moneda,cantidad,concepto,condicion)
						SELECT $entrada_id AS entrada_id,mov,articulo_id,moneda_id,precio AS costo,descuento,iva,valor_moneda,cantidad,concepto,condicion FROM detalles_salidas WHERE salida_id=$salida_id ";
		return ejecutarConsulta($sql);
											
	}
	public function detallesEntSal($idalmacenes,$idtipo_movs,$idpersonas,$idusuarios,$tipo_mov)
	{
		if ($tipo_mov=="E")
			$sql="SELECT articulo_id AS idarticulos,costo AS valor,iva,cantidad,descuento,condicion,concepto FROM detalles_tmp_entradas WHERE almacen_id='$idalmacenes' AND tipo_mov_id='$idtipo_movs' AND proveedor_id='$idpersonas' AND capturista_id='$idusuarios'  ";	
		else
			$sql="SELECT articulo_id AS idarticulos,precio AS valor,iva,cantidad,descuento,condicion,concepto FROM detalles_tmp_salidas WHERE almacen_id='$idalmacenes' AND tipo_mov_id='$idtipo_movs' AND cliente_id='$idpersonas' AND capturista_id='$idusuarios'  ";	
		return ejecutarConsulta($sql);		
		//echo "$sql";
	}	
									  //$ident_sal,$mov,$idarticulos,$valor,$descuento,$iva,$cantidad,$condicion,$concepto,$tipo_mov
	public function insertarDetEntSal($ident_sal,$mov,$idarticulos,$valor,$descuento,$iva,$cantidad,$condicion,$concepto,$tipo_mov)
	{
		
		if (strval($ident_sal)>0)
		{
			if ($tipo_mov=='E')
			{
				$sql="INSERT INTO detalles_entradas(entrada_id,mov,   articulo_id,   costo,   descuento,  iva,    cantidad,condicion)
			                               values('$ident_sal',$mov,'$idarticulos','$valor','$descuento','$iva','$cantidad',1)";	
			}
			else
			{
				$sql="INSERT INTO detalles_salidas(salida_id,mov,    articulo_id,  precio,   descuento,  iva,   cantidad,  condicion,concepto)
			                              values('$ident_sal',$mov,'$idarticulos','$valor','$descuento','$iva','$cantidad',        1,'$concepto')";
			       
			}         
			//echo "$sql";	
			return ejecutarConsulta_retornarID($sql);
			
		}
		
	}
	
	public function detallesEntSalInsertados($ident_sal,$tipo_mov)
	{
		if ($tipo_mov=="E")
			$sql="SELECT COUNT(entrada_id) as registros from  detalles_entradas WHERE entrada_id='$ident_sal'  ";	
		if ($tipo_mov=="S")
			$sql="SELECT COUNT(salida_id) as registros from  detalles_salidas WHERE salida_id='$ident_sal'  ";	
		$rs= ejecutarConsulta($sql);
		return $rs;
	}

	public function eliminarTmpEntSal($idalmacenes,$idtipo_movs,$idpersonas,$idusuarios,$tipo_mov)
	{
		if ($tipo_mov=="E")
			$sql="DELETE FROM detalles_tmp_entradas WHERE almacen_id='$idalmacenes' and tipo_mov_id='$idtipo_movs' and proveedor_id='$idpersonas' AND capturista_id='$idusuarios'  ";	
		if ($tipo_mov=="S")
			$sql="DELETE FROM detalles_tmp_salidas WHERE almacen_id='$idalmacenes' and tipo_mov_id='$idtipo_movs' and cliente_id='$idpersonas' AND capturista_id='$idusuarios'  ";	
		$rs= ejecutarConsulta($sql);
		return $rs;
	}		
	public function tomarEntSal($ident_sal,$idalmacenes,$idtipo_movs,$idpersonas,$idusuarios,$tipo_mov,$tipodeprecio)
	{
		
		
		if ($tipo_mov=='E')
		{
			$sql="DELETE FROM detalles_tmp_entradas WHERE almacen_id='$idalmacenes' AND tipo_mov_id='$idtipo_movs' AND proveedor_id='$idpersonas' AND capturista_id='$idusuarios'";	
			$rs= ejecutarConsulta($sql);	
			$sql="INSERT INTO detalles_tmp_entradas (almacen_id,                                    tipo_mov_id,                proveedor_id,                capturista_id,articulo_id,costo,iva,cantidad,condicion)
		                                     SELECT '$idalmacenes' AS idalmacenes,'$idtipo_movs' AS idtipo_movs,'$idpersonas' AS idpersonas,'$idusuarios' AS idcapturistas,articulo_id,costo,iva,cantidad,'1' FROM detalles_entradas WHERE entrada_id ='$ident_sal' ";		         
		}
		else
		{
			$sql="DELETE FROM detalles_tmp_salidas WHERE almacen_id='$idalmacenes' AND tipo_mov_id='$idtipo_movs' AND cliente_id='$idpersonas' AND capturista_id='$idusuarios'";	
			$rs= ejecutarConsulta($sql);	
			$sql="INSERT INTO detalles_tmp_salidas (almacen_id,                            tipo_mov_id,                 cliente_id,                 capturista_id,articulo_id,precio,iva,cantidad,condicion)
		         					SELECT '$idalmacenes' as idalmacenes,'$idtipo_movs' as idtipo_movs,'$idpersonas' as idpersonas,'$idusuarios' as idcapturistas,articulo_id,precio,iva,cantidad,'1' from detalles_salidas where salida_id ='$ident_sal' ";
		}
		echo "$sql";         
		$rs= ejecutarConsulta($sql);

		return $rs;		
	}
	public function buscarProducto($idarticulos,$idalmacenes)
	{
		$sql="SELECT costo,precio,articulos_almacenes.iva ,(articulos_almacenes.entradas-articulos_almacenes.salidas) AS existencia,articulos.cambio_precio,articulos_almacenes.moneda_id FROM articulos_almacenes 
		INNER JOIN almacenes ON almacenes.id=articulos_almacenes.almacen_id
		INNER JOIN articulos ON articulos.id=articulos_almacenes.articulo_id
		WHERE articulos_almacenes.articulo_id ='$idarticulos' AND articulos_almacenes.almacen_id='$idalmacenes'";
		return ejecutarConsultaSimpleFila($sql);
		
	}
	public function selecte() // seleccionar proveedor
	{
		$sql="SELECT id as idtipo_movs,descripcion as nombre FROM tipo_mov where tipo='E' "; //where condicion=1
		return ejecutarConsulta($sql);		
	}
	public function selects() // seleccionar proveedor
	{
		$sql="SELECT id as idtipo_movs,descripcion as nombre FROM tipo_mov where tipo='S' "; //where condicion=1
		return ejecutarConsulta($sql);		
	}
	public function selecionarIva($idalmacenes) //selecionar clientes
	{
		$sql="SELECT iva  FROM almacenes where id='$idalmacenes' "; //where condicion=1
		//echo "$sql";
		$rs= ejecutarConsulta($sql);		
		while($fila = mysqli_fetch_assoc( $rs ) )
		{
			$iva= $fila['iva'];	
		}
		$sql="SELECT id as idivas,descripcion as nombre  FROM ivas where valor='$iva' "; //where condicion=1
		//echo "$sql";
		return  ejecutarConsulta($sql);		

	}
	public function establecerIva() //selecionar clientes
	{
		$sql="SELECT id as idtipo_movs,descripcion as nombre FROM tipo_mov where tipo='S' "; //where condicion=1
		return ejecutarConsulta($sql);		
	}
	public function actualizar_almacen_destino($folio,$almacen_destino)
	{
		$sql="UPDATE ent_sal  set almacen_destino='$almacen_destino' WHERE ident_sal='$folio' ";
		return ejecutarConsulta($sql);
	}
	
	

}

?>