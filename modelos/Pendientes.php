<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";
ini_set ( 'max_execution_time', 3600); 
Class Pendientes
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($idusuarios,$idpersonas,$fecha,$fecha_inicial,$hora_inicial,$origen,$asunto,$contestacion,$importe,$imagen)
	{
		$sql="INSERT INTO pendientes (idusuarios,idpersonas,fecha,fecha_inicial,hora_inicial,origen,asunto,contestacion,importe,imagen,condicion)
							VALUES ('$idusuarios','$idpersonas','$fecha','$fecha_inicial','$hora_inicial','$origen','$asunto','?','$importe','$imagen',0)";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros 
	public function editar($idpendientes,$fecha_final,$hora_final,$contestacion,$importe,$imagen)
	{
		$sql="UPDATE pendientes SET fecha_final='$fecha_final',hora_final='$hora_final',contestacion='$contestacion',importe='$importe',imagen='$imagen' where idpendientes='$idpendientes' ";
		return ejecutarConsulta($sql);
	}
	
		
		
	//Implementamos un método para desactivar registros
	public function desactivar($idpendientes)
	{
		$sql="UPDATE pendientes SET condicion=0 WHERE idpendientes=$idpendientes";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar registros
	public function activar($idpendientes)
	{
		$sql="UPDATE pendientes SET condicion=1 WHERE idpendientes=$idpendientes";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idpendientes)
	{
		$sql="SELECT idpendientes,idusuarios,personas.idpersonas,
             personas.nombre AS cliente,
             fecha,
             asunto,
             contestacion,
             imagen,
             origen,
             importe,
             pendientes.condicion FROM pendientes  
             INNER JOIN personas ON personas.idpersonas=pendientes.idpersonas
             WHERE idpendientes=$idpendientes";
        
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar($fecha_inicial,$fecha_final)
	{
		$sql="SELECT pendientes.idpendientes,personas.nombre AS cliente,fecha,hora_inicial,hora_final,asunto,contestacion,pendientes.condicion FROM pendientes
		INNER JOIN personas ON personas.idpersonas=pendientes.idpersonas  
		WHERE pendientes.fecha BETWEEN '$fecha_inicial' AND '$fecha_final'";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros ACTIVOS
	public function listarActivos()
	{
		$sql="SELECT a.idpendientes,a.idcategorias,c.nombre as categoria,a.codigo,a.nombre,a.stock,a.descripcion,a.imagen,a.condicion FROM pendientes a INNER JOIN categoria c ON a.idcategoria=c.idcategoria where a.condicion=1 ";
		return ejecutarConsulta($sql);		
	}
		
	
	public function select()
	{
		$sql="SELECT idpendientes,codigo,nombre FROM pendientes where condicion=1";
		return ejecutarConsulta($sql);		
	}
}

?>