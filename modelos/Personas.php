 <?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Personas
{
	//Implementamos nuestro constructor 
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($nombre,$nombrecomercial,$rfc,$idmunicipios,$idcolonias,$idcalles,$idvendedores,$numero_exterior,$numero_interior,$referencia,$codigo_postal,$telefono,$correo,$diascredito,$limitecredito,$tipodepersona)
	{
		$sql="INSERT INTO personas (nombre,nombrecomercial,rfc,idmunicipios,idcolonias,idcalles,idvendedores,numero_exterior,numero_interior,referencia,codigo_postal,telefono,correo,diascredito,limitecredito,tipodepersona)

		VALUES ('$nombre','$nombrecomercial','$rfc','$idmunicipios','$idcolonias','$idcalles','$idvendedores','$numero_exterior','$numero_interior','$referencia','$codigo_postal','$telefono','$correo','$diascredito','$limitecredito','$tipodepersona')";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idpersonas,$nombre,$nombrecomercial,$rfc,$idmunicipios,$idcolonias,$idcalles,$idvendedores,$numero_exterior,$numero_interior,$referencia,$codigo_postal,$telefono,$correo,$diascredito,$limitecredito,$tipodepersona)
	{
		$sql="UPDATE personas SET nombre='$nombre',nombrecomercial='$nombrecomercial',rfc='$rfc',idmunicipios='$idmunicipios',idcolonias='$idcolonias',idcalles='$idcalles',idvendedores='$idvendedores',numero_exterior='$numero_exterior',numero_interior='$numero_interior',referencia='$referencia',codigo_postal='$codigo_postal',telefono='$telefono',correo='$correo',tipodepersona='$tipodepersona',diascredito='$diascredito',limitecredito='$limitecredito' WHERE idpersonas='$idpersonas'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar categorías
	/*public function eliminar($idpersonas)
	{
		$sql="DELETE from personas WHERE idpersonas='$idpersonas'";
		return ejecutarConsulta($sql);
	}*/


	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idpersonas) 
	{
		$sql="SELECT * FROM personas WHERE idpersonas='$idpersonas'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros 
	public function listarp($Opcion,$referencia)
	{
		if ($Opcion==0 || strlen($referencia)<3)
			$q=" and rfc = '$$$$$$$$$$$$' ";
		if ($Opcion==1)
			$q=" and rfc like '%$referencia%' ";
		if ($Opcion==2)
			$q=" and nombre like '%$referencia%' ";
		if ($Opcion==3)
			$q=" and telefono like '%$referencia%' ";
		if ($Opcion==0 || strlen($referencia)<3)
			$q=" and rfc = '$$$$$$$$$$$$' ";

		$sql="SELECT idpersonas,nombre,rfc,nombrecomercial,telefono,condicion FROM personas WHERE tipodepersona='Proveedor' $q ";
		
		return ejecutarConsulta($sql);	 	
	}

	//Implementar un método para listar los registros
	public function listarc($Opcion,$referencia)
	{
		if ($Opcion==0 || strlen($referencia)<3)
			$q=" and rfc = '$$$$$$$$$$$$' ";
		if ($Opcion==1)
			$q=" and rfc like '%$referencia%' ";
		if ($Opcion==2)
			$q=" and nombre like '%$referencia%' ";
		if ($Opcion==3)
			$q=" and telefono like '%$referencia%' ";
		if ($Opcion==0 || strlen($referencia)<3)
			$q=" and rfc = '$$$$$$$$$$$$' ";
		
		{
			$sql="SELECT idpersonas,nombre,rfc,nombrecomercial,telefono,condicion FROM personas WHERE tipodepersona='Cliente' $q ";
			return ejecutarConsulta($sql);		
			
		}
	}
	public function selectp($Opcion,$referencia) // seleccionar proveedor
	{
		if ($Opcion==0 || strlen($referencia)<3) 
			$q=" ";
		if ($Opcion==1)
			$q=" where rfc like '%$referencia%' ";
		if ($Opcion==2)
			$q=" where descripcion like '%$referencia%' ";
		if ($Opcion==3)
			$q=" where telefono like '%$referencia%' ";
		

		$sql="SELECT id as idpersonas,descripcion as nombre FROM proveedores   $q "; 
		return ejecutarConsulta($sql);		
	}
	public function selectc($Opcion,$referencia) //selecionar clientes
	{		
		if ($Opcion==0 || strlen($referencia)<3)
			$q=" where and rfc = '$$$$$$$$$$$$' ";
		if ($Opcion==1)
			$q=" where  rfc like '%$referencia%' ";
		if ($Opcion==2)
			$q=" where  descripcion like '%$referencia%' ";
		if ($Opcion==3)
			$q=" where  telefono like '%$referencia%' ";
		$sql="SELECT id as idpersonas,descripcion as nombre FROM clientes   $q"; //where condicion=1
		return ejecutarConsulta($sql);		
	}
	//Implementamos un método para desactivar categorías
	public function desactivar($idpersonas)
	{
		$sql="UPDATE personas SET condicion='0' WHERE idpersonas='$idpersonas'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idpersonas)
	{
		$sql="UPDATE personas SET condicion='1' WHERE idpersonas='$idpersonas'";
		return ejecutarConsulta($sql);
	}
}

?>
