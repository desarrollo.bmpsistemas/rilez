<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Precios_por_sublineas
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	

	

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idalmacenes,$idsublineas)
	{
		$sql="SELECT idarticulos_almacenes,idalmacenes,idarticulos,stock,idmonedas,costo,precio,ivas.idivas,porcentaje_utilidad,ubicacion,articulos_almacenes.condicion FROM articulos_almacenes
		INNER JOIN ivas ON ivas.nombre=articulos_almacenes.iva  WHERE idarticulos_almacenes='$idarticulos_almacenes' ";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar($idalmacenes,$idsublineas)
	{
		
		$sql="SELECT articulos_almacenes.idarticulos_almacenes,articulos.nombre,articulos_almacenes.costo,articulos_almacenes.porcentaje_utilidad,articulos_almacenes.precio,'1' as condicion FROM articulos_almacenes
		INNER JOIN articulos ON articulos.idarticulos=articulos_almacenes.idarticulos
		WHERE articulos_almacenes.idalmacenes=$idalmacenes AND articulos.idsublineas='$idsublineas'
 		ORDER BY articulos.nombre";
		return ejecutarConsulta($sql);		
	}
	public function actualizar_precios($idalmacenes,$idsublineas,$porcentaje_utilidad)
	{	
		$sql="UPDATE articulos_almacenes
		INNER JOIN articulos ON articulos.idarticulos=articulos_almacenes.idarticulos
		SET porcentaje_utilidad=$porcentaje_utilidad
		WHERE  articulos.idsublineas=$idsublineas AND articulos_almacenes.idalmacenes=$idalmacenes ";
 		$rs = ejecutarConsulta($sql);		

		$sql="UPDATE articulos_almacenes
		INNER JOIN articulos ON articulos.idarticulos=articulos_almacenes.idarticulos
		SET precio=costo*porcentaje_utilidad
		WHERE  articulos.idsublineas=$idsublineas AND articulos_almacenes.idalmacenes=$idalmacenes ";
 		return ejecutarConsulta($sql);		
	}
	
	
}

?>