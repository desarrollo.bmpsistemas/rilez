 <?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Proveedores
{
	//Implementamos nuestro constructor 
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
							
	public function insertar($nombre,$nombrecomercial,$rfc,$referencia,$telefono,$correo,$diascredito,$limitecredito)
	{
		$sql="INSERT INTO proveedores (descripcion,nomComercial,       rfc,    referencia,telefono,    correo,     dias,     limite,condicion)
		                   VALUES ('$nombre','$nombrecomercial','$rfc','$referencia','$telefono','$correo','$diascredito','$limitecredito',1)";
		$id=ejecutarConsulta_retornarID($sql);
		if (empty($id))
			return $sql;
		else
			return $id;
	}

	//Implementamos un método para editar registros
	public function editar($idproveedores,$nombre,$nombrecomercial,$rfc,$referencia,$telefono,$correo,$diascredito,$limitecredito)
	{
		$sql="UPDATE proveedores SET descripcion='$nombre',nomComercial='$nombrecomercial',rfc='$rfc',referencia='$referencia',telefono='$telefono',correo='$correo',dias='$diascredito',limite='$limitecredito' WHERE id='$idproveedores'";
		$id= ejecutarConsulta($sql);
		//if (empty($id))
		//	return $sql;
		//else
		return $id;
	}

	


	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idproveedores) 
	{
		$sql="SELECT * FROM proveedores WHERE id='$idproveedores'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros 
	public function listar($Opcion,$referencia)
	{
		if ($Opcion==0)
			$q="  id = '$referencia' ";
		if ($Opcion==1)
			$q="  rfc like '%$referencia%' ";
		if ($Opcion==2)
			$q=" descripcion like '%$referencia%' ";
		if ($Opcion==3)
			$q=" telefono like '%$referencia%' ";
		

		$sql="SELECT id AS idproveedores,descripcion AS nombre,rfc,nomComercial AS nombrecomercial,telefono,condicion FROM proveedores WHERE  $q ";
		//echo "$sql";
		return ejecutarConsulta($sql);	 	
	}

	//Implementar un método para listar los registros
	
	public function select($Opcion,$referencia) // seleccionar proveedor
	{
		if ($Opcion==0)
			$q=" where id = '$referencia' ";
		if ($Opcion==1)
			$q=" where rfc like '%$referencia%' ";
		if ($Opcion==2)
			$q=" where descripcion like '%$referencia%' ";
		if ($Opcion==3)
			$q=" where telefono like '%$referencia%' ";
		

		$sql="SELECT id AS idproveedores,CONCAT(SUBSTRING(descripcion,1,30),' - ',nomComercial) AS nombre FROM proveedores   $q "; 
		return ejecutarConsulta($sql);		
	}
	
	//Implementamos un método para desactivar categorías
	public function desactivar($idproveedores)
	{
		$sql="UPDATE proveedores SET condicion='0' WHERE id='$idproveedores'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idproveedores)
	{
		$sql="UPDATE proveedores SET condicion='1' WHERE id='$idproveedores'";
		return ejecutarConsulta($sql);
	}
}

?>
