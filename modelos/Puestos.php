<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Puestos
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($nombre)
	{
		$sql="INSERT INTO puestos (descripcion,condicion)
		VALUES ('$nombre','1')";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idpuestos,$nombre)
	{
		$sql="UPDATE puestos SET descripcion='$nombre' WHERE id='$idpuestos'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idpuestos)
	{
		$sql="UPDATE puestos SET condicion='0' WHERE id='$idpuestos'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idpuestos)
	{
		$sql="UPDATE puestos SET condicion='1' WHERE id='$idpuestos'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idpuestos)
	{
		$sql="SELECT id as idpuestos,descripcion as nombre FROM puestos WHERE id ='$idpuestos'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar()
	{
		$sql="SELECT id as idpuestos,descripcion as nombre,condicion FROM puestos";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select()
	{
		$sql="SELECT id as idpuestos,descripcion as nombre FROM puestos where condicion=1";
		return ejecutarConsulta($sql);		
	}
}

?>