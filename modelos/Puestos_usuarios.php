<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Puestos_usuarios
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($idpuestos,$idusuarios)
	{
		
		$sql="INSERT INTO puestos_usuarios (idpuestos,idusuarios,condicion)
		VALUES ('$idpuestos','$idusuarios','1')";
		$rs= ejecutarConsulta($sql);
		//echo "$sql";
		$sql="DELETE FROM accesos WHERE idusuarios='$idusuarios'";
		$rs=ejecutarConsulta($sql);
		//echo "$sql";
		$sql="SELECT idmodulos FROM accesos_puestos WHERE idpuestos='$idpuestos' ";
		$resultado=ejecutarConsulta($sql);		
		//echo "$sql";
	    /* obtener un array asociativo */
	    while ($fila = $resultado->fetch_assoc()) 
	    {
	        $idmodulos= $fila["idmodulos"];
	        $sql="INSERT INTO accesos (idusuarios,idmodulos,condicion)
							VALUES ('$idusuarios','$idmodulos','1')";
			$rs+= ejecutarConsulta($sql);
			//echo "$sql";
	    }
	    return $rs;


	}

	//Implementamos un método para editar registros
	public function eliminar($idusuarios)
	{
		$sql1="DELETE FROM  puestos_usuarios WHERE idusuarios='$idusuarios'";
		$rs= ejecutarConsulta($sql1);

		$sql2="DELETE FROM accesos WHERE idusuarios='$idusuarios'";
		$rs=ejecutarConsulta($sql2);
		return $rs;
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idpuestos_usuarios)
	{
		$sql="UPDATE puestos_usuarios SET condicion='0' WHERE idpuestos_usuarios='$idpuestos_usuarios'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías 
	public function activar($idpuestos,$idusuarios)
	{
		$sql="DELETE FROM accesos WHERE idusuarios='$idusuarios'";
		$rs=ejecutarConsulta($sql);
		//echo "$sql";
		$sql="SELECT idmodulos FROM accesos_puestos WHERE idpuestos='$idpuestos' ";
		$resultado=ejecutarConsulta($sql);		
		//echo "$sql";
	    /* obtener un array asociativo */
	    while ($fila = $resultado->fetch_assoc()) 
	    {
	        $idmodulos= $fila["idmodulos"];
	        $sql="INSERT INTO accesos (idusuarios,idmodulos,condicion)
							VALUES ('$idusuarios','$idmodulos','1')";
			$rs+= ejecutarConsulta($sql);
			//echo "$sql";
	    }
	    return $rs;

	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idpuestos_usuarios)
	{
		$sql="SELECT idpuestos_usuarios,idpuestos,idmodulos FROM puestos_usuarios WHERE idpuestos_usuarios='$idpuestos_usuarios'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar($idpuestos)
	{
		if ($idpuestos==0)
			$q=" puestos_usuarios.idpuestos>0 ";
		else
			$q=" puestos_usuarios.idpuestos='$idpuestos' ";
		$sql="SELECT puestos_usuarios.idusuarios,puestos.nombre AS puesto,usuarios.nombre AS usuario,puestos_usuarios.condicion FROM puestos_usuarios
		INNER JOIN puestos ON puestos.idpuestos=puestos_usuarios.idpuestos 
		INNER JOIN usuarios ON usuarios.idusuarios=puestos_usuarios.idusuarios 
		WHERE $q ORDER BY puestos.nombre,usuarios.nombre";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select()
	{
		$sql="SELECT idpuestos_usuarios FROM puestos_usuarios where condicion=1";
		return ejecutarConsulta($sql);		
	}
}

?>