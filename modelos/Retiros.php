 <?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php"; 

Class Retiros
{
	//Implementamos nuestro constructor 
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
							
	public function insertar($idusuarios,$capturista_id,$idcat_de_origenes,$iddestinos,$fecha,$importe,$nota)
	{
		
		$sql="SELECT id as fondos_id FROM fondos where cat_de_origenes_id=$idcat_de_origenes";
		$rs= ejecutarConsulta($sql);
		while($fila = mysqli_fetch_assoc( $rs ) )
		{
			$fondos_id= $fila['fondos_id'];	
		}

		$sql="INSERT INTO detalles_destinos (cat_de_origenes_id,  destinos_id,capturista_id,   fecha,   importe,   nota)
		                          VALUES ('$idcat_de_origenes','$iddestinos','$idusuarios','$fecha','$importe','$nota')";
		$id=ejecutarConsulta_retornarID($sql);

		if ($id>0)
		{
			
			$sql="UPDATE fondos SET salidas=salidas+'$importe' where cat_de_origenes_id=$idcat_de_origenes";
			$id2=ejecutarConsulta_retornarID($sql);	
			$sql="UPDATE caja_chica SET salidas=salidas+'$importe' where capturista_id=$capturista_id";
			$id2=ejecutarConsulta_retornarID($sql);	
			
		}

		if (empty($id))
			return $sql;
		else
			return $id;
	}

	//Implementamos un método para editar registros
	public function editar($idsaldos,$idpersonas,$fecha,$importe,$nota)
	{
		$sql="UPDATE origenes set proveedor_id='$idpersonas',fecha='$fecha',importe='$importe',nota='$nota' WHERE id='$idsaldos'";
		$id= ejecutarConsulta($sql);
		if ($id)
		return $id;
		else
		return $sql;	
	}

	


	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idcat_de_origenes) 
	{
		$sql="SELECT * FROM origenes WHERE id='$idsaldos'";
		return ejecutarConsultaSimpleFila($sql);
	}
	public function verSiEsCajaChica($iddet_destinos) 
	{
		$sql="SELECT * FROM detalles_destinos WHERE id='$iddet_destinos'";
		return ejecutarConsultaSimpleFila($sql);
	}
	
	public function mostrarFondos($idcat_de_origenes) 
	{
		$sql="SELECT entradas,salidas,entradas-salidas as saldo FROM fondos WHERE cat_de_origenes_id='$idcat_de_origenes'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros 
	public function listar($fecha_inicial,$fecha_final)
	{
		
		$sql="SELECT detalles_destinos.id AS iddet_destinos,cat_de_origenes.descripcion AS origen, destinos.descripcion AS destino,capturistas.descripcion AS capturista,fecha,importe,nota,detalles_destinos.condicion FROM detalles_destinos
		INNER JOIN capturistas ON capturistas.id=detalles_destinos.capturista_id
		INNER JOIN destinos ON destinos.id=detalles_destinos.destinos_id
		INNER JOIN cat_de_origenes ON cat_de_origenes.id=detalles_destinos.cat_de_origenes_id
		WHERE detalles_destinos.fecha between '$fecha_inicial' and '$fecha_final' ";
		//echo "$sql";
		return ejecutarConsulta($sql);	 	
	}
	public function listarDestinos($fecha_inicial,$fecha_final,$iddestinos)
	{
		
		$sql="SELECT detalles_destinos.id AS iddet_destinos,cat_de_origenes.descripcion AS origen, destinos.descripcion AS destino,capturistas.descripcion AS capturista,fecha,importe,nota,detalles_destinos.condicion FROM detalles_destinos
		INNER JOIN capturistas ON capturistas.id=detalles_destinos.capturista_id
		INNER JOIN destinos ON destinos.id=detalles_destinos.destinos_id
		INNER JOIN cat_de_origenes ON cat_de_origenes.id=detalles_destinos.cat_de_origenes_id
		WHERE detalles_destinos.fecha between '$fecha_inicial' and '$fecha_final' and destinos_id=$iddestinos ";
		//echo "$sql";
		return ejecutarConsulta($sql);	 	
	}
	
	public function listarDestinosPorCapturista($fecha_inicial,$fecha_final,$iddestinos,$capturista_id)
	{
		
		$sql="SELECT detalles_destinos.id AS iddet_destinos,cat_de_origenes.descripcion AS origen, destinos.descripcion AS destino,capturistas.descripcion AS capturista,fecha,importe,nota,detalles_destinos.condicion FROM detalles_destinos
		INNER JOIN capturistas ON capturistas.id=detalles_destinos.capturista_id
		INNER JOIN destinos ON destinos.id=detalles_destinos.destinos_id
		INNER JOIN cat_de_origenes ON cat_de_origenes.id=detalles_destinos.cat_de_origenes_id
		WHERE detalles_destinos.fecha between '$fecha_inicial' and '$fecha_final' and destinos_id=$iddestinos and detalles_destinos.capturista_id=$capturista_id ";
		//echo "$sql";
		return ejecutarConsulta($sql);	 	
	}
	public function listarPagos($idsaldos)
	{
		
		$sql="SELECT detalles_saldos.id AS iddet_saldos,folio,fecha,importe,condicion FROM detalles_saldos
		WHERE detalles_saldos.saldos_id=$idsaldos";
		//echo "$sql";
		return ejecutarConsulta($sql);	 	
	}

	//Implementar un método para listar los registros
	
	public function select($Opcion,$referencia) // seleccionar proveedor
	{
		if ($Opcion==0)
			$q=" where id = '$referencia' ";
		if ($Opcion==1)
			$q=" where rfc like '%$referencia%' ";
		if ($Opcion==2)
			$q=" where descripcion like '%$referencia%' ";
		if ($Opcion==3)
			$q=" where telefono like '%$referencia%' ";
		

		$sql="SELECT id AS idproveedores,CONCAT(SUBSTRING(descripcion,1,30),' - ',nomComercial) AS nombre FROM proveedores   $q "; 
		return ejecutarConsulta($sql);		
	}
	
	public function selectCat_de_Origenes() // seleccionar proveedor
	{
		$sql="SELECT id AS idcat_de_origenes,descripcion AS nombre FROM cat_de_origenes order by id"; 
		return ejecutarConsulta($sql);		
	}
	
	public function selectDestinos() // seleccionar proveedor
	{
		$sql="SELECT id AS iddestinos,descripcion AS nombre FROM destinos order by id"; 
		return ejecutarConsulta($sql);		
	}
	//Implementamos un método para desactivar categorías
	public function desactivar($iddet_destinos,$capturista_id)
	{
		$sql="SELECT cat_de_origenes_id,importe FROM detalles_destinos where id=$iddet_destinos";
		$rs= ejecutarConsulta($sql);
		while($fila = mysqli_fetch_assoc( $rs ) )
		{
			$idcat_de_origenes= $fila['cat_de_origenes_id'];	
			$importe= $fila['importe'];	
		}


		$sql="DELETE FROM detalles_destinos  WHERE id='$iddet_destinos'";
		$id= ejecutarConsulta($sql);
		if ($id>0)
		{
			$sql="UPDATE fondos SET salidas=salidas-'$importe' where cat_de_origenes_id=$idcat_de_origenes";
			$id2=ejecutarConsulta_retornarID($sql);	

			$sql="UPDATE caja_chica SET salidas=salidas-'$importe' where capturista_id=$capturista_id";
			$id2=ejecutarConsulta_retornarID($sql);	
		}
		return $id;

	}

	//Implementamos un método para activar categorías
	public function activar($idproveedores)
	{
		$sql="UPDATE proveedores SET condicion='1' WHERE id='$idproveedores'";
		return ejecutarConsulta($sql);
	}
	public function selectCapturistas($referencia)
	{
		$sql="SELECT id as idpersonas,descripcion as nombre FROM capturistas where descripcion like '%$referencia%' ";
		//echo "$sql";
		return ejecutarConsulta($sql);		
	}
}

?>
