 <?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Saldos_a_favor
{
	//Implementamos nuestro constructor 
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
							
	public function insertar($idpersonas,$fecha,$importe,$nota)
	{
		$sql="INSERT INTO saldos_a_favor (proveedor_id,fecha,importe,nota)
		                   VALUES ('$idpersonas','$fecha','$importe','$nota')";
		$id=ejecutarConsulta_retornarID($sql);
		if (empty($id))
			return $sql;
		else
			return $id;
	}

	//Implementamos un método para editar registros
	public function editar($idsaldos,$idpersonas,$fecha,$importe,$nota)
	{
		$sql="UPDATE saldos_a_favor set proveedor_id='$idpersonas',fecha='$fecha',importe='$importe',nota='$nota' WHERE id='$idsaldos'";
		$id= ejecutarConsulta($sql);
		if ($id)
		return $id;
		else
		return $sql;	
	}

	


	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idsaldos) 
	{
		$sql="SELECT * FROM saldos_a_favor WHERE id='$idsaldos'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros 
	public function listar($idpersonas)
	{
		

		$sql="SELECT saldos_a_favor.id as idsaldos,proveedores.`descripcion` AS nombre,fecha,importe,abonos,nota,saldos_a_favor.condicion FROM saldos_a_favor
		INNER JOIN proveedores ON proveedores.id=saldos_a_favor.proveedor_id
		WHERE proveedor_id=$idpersonas ";
		//echo "$sql";
		return ejecutarConsulta($sql);	 	
	}
	public function listarPagos($idsaldos)
	{
		
		$sql="SELECT detalles_saldos.id AS iddet_saldos,folio,fecha,importe,condicion FROM detalles_saldos
		WHERE detalles_saldos.saldos_id=$idsaldos";
		//echo "$sql";
		return ejecutarConsulta($sql);	 	
	}

	//Implementar un método para listar los registros
	
	public function select($Opcion,$referencia) // seleccionar proveedor
	{
		if ($Opcion==0)
			$q=" where id = '$referencia' ";
		if ($Opcion==1)
			$q=" where rfc like '%$referencia%' ";
		if ($Opcion==2)
			$q=" where descripcion like '%$referencia%' ";
		if ($Opcion==3)
			$q=" where telefono like '%$referencia%' ";
		

		$sql="SELECT id AS idproveedores,CONCAT(SUBSTRING(descripcion,1,30),' - ',nomComercial) AS nombre FROM proveedores   $q "; 
		return ejecutarConsulta($sql);		
	}
	
	//Implementamos un método para desactivar categorías
	public function desactivar($idsaldos)
	{
		$sql="DELETE FROM saldos_a_favor  WHERE id='$idsaldos'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idproveedores)
	{
		$sql="UPDATE proveedores SET condicion='1' WHERE id='$idproveedores'";
		return ejecutarConsulta($sql);
	}
}

?>
