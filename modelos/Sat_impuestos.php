  <?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Sat_impuestos
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($nombre)
	{
		/*$sql="INSERT INTO sat_impuestos (nombre,condicion)
		VALUES ('$nombre','1')";
		return ejecutarConsulta($sql);*/
	}

	//Implementamos un método para editar registros
	public function editar($idsat_impuestos,$nombre)
	{
		/*$sql="UPDATE sat_forma_de_pago SET nombre='$nombre' WHERE idsat_forma_de_pago='$idsat_forma_de_pago'";
		return ejecutarConsulta($sql);*/
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idsat_impuestos)
	{
		$sql="UPDATE sat_impuestos SET condicion='0' WHERE id='$idsat_impuestos'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idsat_impuestos)
	{
		$sql="UPDATE sat_impuestos SET condicion='1' WHERE id='$idsat_impuestos'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idsat_impuestos)
	{
		$sql="SELECT id as idsat_impuestos,clave,descripcion as nombre,condicion FROM sat_impuestos WHERE id='$idsat_impuestos'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar()
	{
		$sql="SELECT id as  idsat_impuestos,clave,descripcion as nombre,condicion FROM sat_impuestos";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select()
	{
		$sql="SELECT id as idsat_impuestos,clave,descripcion as nombre FROM sat_impuestos where condicion=1";
		return ejecutarConsulta($sql);		
	}
}

?>