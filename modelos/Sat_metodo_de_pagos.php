<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Sat_metodo_de_pagos
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($nombre)
	{
		/*$sql="INSERT INTO sat_impuestos (nombre,condicion)
		VALUES ('$nombre','1')";
		return ejecutarConsulta($sql);*/
	}

	//Implementamos un método para editar registros
	public function editar($idsat_metodo_de_pagos,$nombre)
	{
		/*$sql="UPDATE sat_forma_de_pagos SET nombre='$nombre' WHERE idsat_forma_de_pagos='$idsat_forma_de_pagos'";
		return ejecutarConsulta($sql);*/
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idsat_metodo_de_pagos)
	{
		$sql="UPDATE sat_metodo_de_pago SET condicion='0' WHERE id='$idsat_metodo_de_pagos'";
		//echo "$sql";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idsat_metodo_de_pagos)
	{
		$sql="UPDATE sat_metodo_de_pago SET condicion='1' WHERE id='$idsat_metodo_de_pagos'";
		//echo "$sql";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idsat_metodo_de_pagos)
	{
		$sql="SELECT id as idsat_metodo_de_pagos,clave,descripcion as nombre,condicion FROM sat_metodo_de_pago WHERE id='$idsat_metodo_de_pagos'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar()
	{
		$sql="SELECT id as  idsat_metodo_de_pagos,clave,descripcion as nombre,condicion FROM sat_metodo_de_pago";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select()
	{
		$sql="SELECT id as idsat_metodo_de_pagos,clave,descripcion as nombre FROM sat_metodo_de_pago where condicion=1";
		return ejecutarConsulta($sql);		
	}
}

?>