 <?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Sat_prod_servs
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($nombre)
	{
		//$sql="INSERT INTO sat_monedas (nombre,condicion)
		//VALUES ('$nombre','1')";
		//return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idsat_prod_servs,$clave,$nombre)
	{
		//$sql="UPDATE sat_monedas SET nombre='$nombre' WHERE idsat_monedas='$idsat_monedas'";
		//return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idsat_prod_servs)
	{
		$sql="UPDATE sat_prod_serv SET condicion='0' WHERE id='$idsat_prod_servs'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idsat_prod_servs)
	{
		$sql="UPDATE sat_prod_serv SET condicion='1' WHERE id='$idsat_prod_servs'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idsat_prod_servs)
	{
		$sql="SELECT id as idsat_prod_servs,clave,nombre,condicion FROM sat_prod_serv WHERE id='$idsat_prod_servs'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar($Opcion,$referencia)
	{
		$q=" where clave like '%010101%' ";
		if ($Opcion==1)
			$q=" where clave like '%$referencia%' ";
		if ($Opcion==2)
			$q=" where descripcion like '%$referencia%' ";

		$sql="SELECT id as idsat_prod_servs,clave,descripcion as nombre,condicion FROM sat_prod_serv ".$q;
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select()
	{
		$sql="SELECT id as idsat_prod_servs,clave,nombre,condicion FROM sat_prod_serv";
		return ejecutarConsulta($sql);		
	}
	public function selectActivos()
	{
		$sql="SELECT id as idsat_prod_servs,clave,descripcion as nombre,condicion FROM sat_prod_serv where condicion=1";
		return ejecutarConsulta($sql);		
	}
}

?>