  <?php 
//Incluímos inicialmente la conexión a la base de datos 
require "../config/Conexion.php";

Class Sat_regimen_fiscales
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($nombre)
	{
		//$sql="INSERT INTO sat_monedas (nombre,condicion)
		//VALUES ('$nombre','1')";
		//return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idsat_regimen_fiscales,$clave,$nombre)
	{
		//$sql="UPDATE sat_monedas SET nombre='$nombre' WHERE idsat_monedas='$idsat_monedas'";
		//return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idsat_regimen_fiscales)
	{
		$sql="UPDATE sat_regimen_fiscal SET condicion='0' WHERE id='$idsat_regimen_fiscales'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idsat_regimen_fiscales)
	{
		$sql="UPDATE sat_regimen_fiscal SET condicion='1' WHERE id='$idsat_regimen_fiscales'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idsat_regimen_fiscales)
	{
		$sql="SELECT idsat_regimen_fiscales,clave,nombre,condicion FROM sat_regimen_fiscal WHERE id='$idsat_regimen_fiscales'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar()
	{
		$sql="SELECT id as idsat_regimen_fiscales,clave,descripcion as nombre,condicion FROM sat_regimen_fiscal";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select()
	{
		$sql="SELECT id as idsat_regimen_fiscales,clave,descripcion as nombre,condicion FROM sat_regimen_fiscal where condicion=1";
		return ejecutarConsulta($sql);		
	}
}

?>