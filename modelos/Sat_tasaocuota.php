 <?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Sat_tasaocuota
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($nombre)
	{
		//$sql="INSERT INTO sat_monedas (nombre,condicion)
		//VALUES ('$nombre','1')";
		//return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idsat_tasaocuota,$nombre)
	{
		//$sql="UPDATE sat_monedas SET nombre='$nombre' WHERE idsat_monedas='$idsat_monedas'";
		//return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idsat_tasaocuota)
	{
		$sql="UPDATE sat_tasaocuota SET condicion='0' WHERE id='$idsat_tasaocuota'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idsat_tasaocuota)
	{
		$sql="UPDATE sat_tasaocuota SET condicion='1' WHERE id='$idsat_tasaocuota'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idsat_tasaocuota)
	{
		$sql="SELECT id as idsat_tasaocuota,clave,valor_maximo FROM sat_tasaocuota WHERE id='$idsat_tasaocuota'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar()
	{
		$sql="SELECT id as idsat_tasaocuota,clave,valor_maximo,condicion FROM sat_tasaocuota";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select()
	{
		$sql="SELECT id as  idsat_tasaocuota,clave,valor_maximo,condicion FROM sat_tasaocuota where condicion=1";
		return ejecutarConsulta($sql);		
	}
}

?>