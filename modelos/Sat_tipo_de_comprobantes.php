 <?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Sat_tipo_de_comprobantes
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($nombre)
	{
		//$sql="INSERT INTO sat_monedas (nombre,condicion)
		//VALUES ('$nombre','1')";
		//return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idsat_tipo_de_comprobantes,$nombre)
	{
		//$sql="UPDATE sat_monedas SET nombre='$nombre' WHERE idsat_monedas='$idsat_monedas'";
		//return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idsat_tipo_de_comprobantes)
	{
		$sql="UPDATE sat_tipo_de_comprobante SET condicion='0' WHERE id ='$idsat_tipo_de_comprobantes'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idsat_tipo_de_comprobantes)
	{
		$sql="UPDATE sat_tipo_de_comprobante SET condicion='1' WHERE id='$idsat_tipo_de_comprobantes'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idsat_tipo_de_comprobantes)
	{
		$sql="SELECT id as idsat_tipo_de_comprobantes,tipo,descripcion as nombre FROM sat_tipo_de_comprobante WHERE id='$idsat_tipo_de_comprobantes'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar()
	{
		$sql="SELECT id as  idsat_tipo_de_comprobantes ,tipo,descripcion as nombre,condicion FROM sat_tipo_de_comprobante";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select()
	{
		$sql="SELECT id as  idsat_tipo_de_comprobantes ,tipo,descripcion as nombre FROM sat_tipo_de_comprobante where condicion=1";
		return ejecutarConsulta($sql);		
	}
}

?>