<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Sat_tipo_factores
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($nombre)
	{
		//$sql="INSERT INTO sat_monedas (nombre,condicion)
		//VALUES ('$nombre','1')";
		//return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idsat_tipofactores,$nombre)
	{
		//$sql="UPDATE sat_monedas SET nombre='$nombre' WHERE idsat_monedas='$idsat_monedas'";
		//return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idsat_tipofactores)
	{
		$sql="UPDATE sat_tipofactor SET condicion='0' WHERE id ='$idsat_tipofactores'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idsat_tipofactores)
	{
		$sql="UPDATE sat_tipofactor SET condicion='1' WHERE id ='$idsat_tipofactores'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idsat_tipofactores)
	{
		$sql="SELECT id as idsat_tipofactores , descripcion as nombre FROM sat_tipofactor WHERE id='$idsat_tipofactores'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar()
	{
		$sql="SELECT id AS idsat_tipo_factores,descripcion AS nombre,condicion FROM sat_tipofactor ";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select()
	{
		$sql="SELECT id as idsat_tipofactores,descripcion as nombre,condicion FROM sat_tipofactor where condicion=1";
		return ejecutarConsulta($sql);		
	}
}

?>