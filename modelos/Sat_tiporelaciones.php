<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Sat_tiporelaciones
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($nombre)
	{
		//$sql="INSERT INTO sat_monedas (nombre,condicion)
		//VALUES ('$nombre','1')";
		//return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idsat_tiporelaciones,$clave,$descripcion)
	{
		//$sql="UPDATE sat_monedas SET nombre='$nombre' WHERE idsat_monedas='$idsat_monedas'";
		//return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idsat_tiporelaciones)
	{
		$sql="UPDATE Sat_tiporelaciones SET condicion='0' WHERE idsat_tiporelaciones='$idsat_tiporelaciones'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idsat_tiporelaciones)
	{
		$sql="UPDATE sat_tiporelaciones SET condicion='1' WHERE idsat_tiporelaciones='$idsat_tiporelaciones'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idsat_tiporelaciones)
	{
		$sql="SELECT idsat_tiporelaciones,clave,descripcion,condicion FROM sat_tiporelaciones WHERE idsat_tiporelaciones='$idsat_tiporelaciones'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar()
	{
		$sql="SELECT idsat_tiporelaciones,clave,descripcion,condicion FROM sat_tiporelaciones";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select()
	{
		$sql="SELECT id AS idsat_tiporelaciones,clave,descripcion AS nombre,condicion FROM sat_tiporelacion where condicion=1";
		return ejecutarConsulta($sql);		
	}
}

?>