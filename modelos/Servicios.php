<?php 
//Incluímos inicialmente la conexión a la base de datos 
require "../config/Conexion.php";
//ini_set ( 'max_execution_time', 3600); 
Class Servicios
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($cliente,$idalmacenes,$idusuarios,$fecha_de_entrada,$fecha_prox_serv,$servicio_a_realizar,$servicio_realizado,$importe,$imagen)
	{
		$fecha=date("Y-m-d");
		$hora= date("H:i:s"); 
		$sql="INSERT INTO servicios (cliente,   fecha, horae,  almacen_id,capturista_id,       fecha_de_entrada, fecha_prox_serv,servicio_a_realizar, importe, imagen,condicion) 
							VALUES ('$cliente','$fecha','$hora','$idalmacenes','$idusuarios','$fecha_de_entrada',$fecha_prox_serv','$servicio_a_realizar','$importe','$imagen',0)";
		$id=ejecutarConsulta_retornarID($sql);
		if ($id>0)
			return $id;
		else
			return $sql;
	}

	//Implementamos un método para editar registros
	public function editar($idservicios,$servicio_realizado)
	{
		$hora= date("H:i:s"); 
		$fecha=date("Y-m-d");
		$sql="UPDATE servicios SET fecha_de_salida='$fecha',horas='$hora',servicio_realizado='$servicio_realizado',condicion='1' where id='$idservicios' ";
		return ejecutarConsulta($sql);
	}
	
		
		
	//Implementamos un método para desactivar registros
	public function desactivar($idservicios)
	{
		$sql="UPDATE servicios SET condicion=0 WHERE id=$idservicios";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar registros
	public function activar($idservicios)
	{
		$sql="UPDATE servicios SET condicion=1 WHERE id=$idservicios";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idservicios)
	{
		$sql="SELECT *	FROM servicios	WHERE id=$idservicios";
        
		return ejecutarConsultaSimpleFila($sql);
	}

	//listar por fecha de entrada
	public function listar($idalmacenes,$fecha_inicial,$fecha_final)
	{
		
		$sql="SELECT id AS idservicios,cliente,fecha_de_entrada,fecha_de_salida,fecha_prox_serv,servicio_a_realizar,servicio_realizado,condicion FROM servicios
		WHERE servicios.fecha BETWEEN '$fecha_inicial' AND '$fecha_final' AND almacen_id=$idalmacenes";
		return ejecutarConsulta($sql);		
	}
	//listar por fecha de proximo servicio
	public function listar2($fecha_inicial,$fecha_final)
	{
		$sql="SELECT id AS idservicios,cliente,fecha_de_entrada,fecha_de_salida,fecha_prox_serv,servicio_a_realizar,servicio_realizado,condicion FROM servicios
		WHERE servicios.fecha BETWEEN '$fecha_inicial' AND '$fecha_final' ";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros ACTIVOS
	public function listarActivos()
	{
		$sql="SELECT a.idservicios,a.idcategorias,c.nombre as categoria,a.codigo,a.nombre,a.stock,a.descripcion,a.imagen,a.condicion FROM servicios a INNER JOIN categoria c ON a.idcategoria=c.idcategoria where a.condicion=1 ";
		return ejecutarConsulta($sql);		
	}
		
	
	public function select()
	{
		$sql="SELECT idservicios,codigo,nombre FROM servicios where condicion=1";
		return ejecutarConsulta($sql);		
	}
}

?>