<?php 
//Incluímos inicialmente la conexión a la base de datos 
require "../config/Conexion.php";

Class Sublineas
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($idlineas,$nombre)
	{
		$sql="INSERT INTO sublineas (linea_id,descripcion,condicion)
		VALUES ('$idlineas','$nombre','1')";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idsublineas,$idlineas,$nombre)
	{
		$sql="UPDATE sublineas SET linea_id='$idlineas',descripcion='$nombre' WHERE id='$idsublineas'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar registros
	public function desactivar($idsublineas)
	{
		$sql="UPDATE sublineas SET condicion='0' WHERE id='$idsublineas'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar registros
	public function activar($idsublineas)
	{
		$sql="UPDATE sublineas SET condicion='1' WHERE id='$idsublineas'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idsublineas)
	{
		$sql="SELECT sublineas.id AS idsublineas,lineas.id AS idlineas,sublineas.descripcion AS sublinea,lineas.descripcion AS linea,
sublineas.condicion FROM sublineas  
INNER JOIN lineas  ON lineas.id=sublineas.linea_id WHERE sublineas.id='$idsublineas'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar($idlineas)
	{
		$sql="SELECT sublineas.id as idsublineas,sublineas.descripcion AS sublinea,sublineas.condicion FROM sublineas where linea_id=$idlineas ";
		//echo "$sql";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros ACTIVOS
	public function listarActivos()
	{
		$sql="SELECT sublineas.id as idsublineas,lineas.id as idlineas,sublineas.descripcion as sublinea,
		lineas.descripcion as linea,sublineas.condicion FROM sublineas  
		INNER JOIN lineas  ON sublineas.linea_id=lineas.id where sublineas.condicion='1' ";
		return ejecutarConsulta($sql);		
	}
	
	//Implementar un método para listar los registros activos, su último precio y el stock (vamos a unir con el último registro de la tabla detalle_ingreso)
	public function selectsublineas($Opcion,$referencia)
	{
		if (!isset($Opcion) ||  empty($Opcion))
			$q=""; // buscar todoas las sublineas
		if ($Opcion==1)
			$q=" where sublineas.id='$referencia' ";
		if ($Opcion==2)
			$q=" where sublineas.descripcion ='$referencia' ";
		if ($Opcion==3)
			$q=" where sublineas.descripcion like '%$referencia'% ";

		$sql="SELECT id as idsublineas,descripcion as nombre  FROM sublineas where condicion=1 $q";
		return ejecutarConsulta($sql);		
	}
	public function selectporlinea($idlineas)
	{
		$sql="SELECT id as idsublineas,descripcion as nombre  FROM sublineas where linea_id=$idlineas";
		return ejecutarConsulta($sql);		
	}
	
}

?>