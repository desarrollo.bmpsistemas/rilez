 <?php 
//Incluímos inicialmente la conexión a la base de datos 
require "../config/Conexion.php";

Class Sucursales
{
	//Implementamos nuestro constructor 
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
							
	public function insertar($nombre,$nombrecomercial,$rfc,$referencia,$telefono,$correo)
	{
		$sql="INSERT INTO sucursales (descripcion,nomComercial,       rfc, referencia,telefono,    correo,     condicion)
		                       VALUES ('$nombre','$nombrecomercial','$rfc','$referencia','$telefono','$correo',1)";
		$id=ejecutarConsulta_retornarID($sql);
		if (empty($id))
			return $sql;
		else
			return $id;
	}

	//Implementamos un método para editar registros
	public function editar($idsucursales,$nombre,$nombrecomercial,$rfc,$referencia,$telefono,$correo)
	{
		$sql="UPDATE sucursales SET descripcion='$nombre',nomComercial='$nombrecomercial',rfc='$rfc',referencia='$referencia',telefono='$telefono',correo='$correo' WHERE id='$idsucursales'";
		$id= ejecutarConsulta($sql);
		//if (empty($id))
		//	return $sql;
		//else
		return $id;
	}

	//Implementamos un método para desactivar categorías
	/*public function eliminar($idsucursales)
	{
		$sql="DELETE from sucursales WHERE idsucursales='$idsucursales'";
		return ejecutarConsulta($sql);
	}*/


	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idsucursales) 
	{
		$sql="SELECT * FROM sucursales WHERE id='$idsucursales'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros 
	public function listar($Opcion,$referencia)
	{
		$q=" opcion $Opcion referencia $referencia";
		if ($Opcion==1)
			$q="  rfc like '%$referencia%' ";
		if ($Opcion==2)
			$q=" descripcion like '%$referencia%' ";
		if ($Opcion==3)
			$q=" telefono like '%$referencia%' ";
		

		$sql="SELECT id AS idsucursales,descripcion AS nombre,rfc,nomComercial AS nombrecomercial,telefono,correo,condicion FROM sucursales WHERE  $q ";
		//echo "$sql";
		return ejecutarConsulta($sql);	 	
	}

	//Implementar un método para listar los registros
	
	public function select() // seleccionar proveedor
	{
		/*$q="";
		if ($Opcion==1)
			$q=" where rfc like '%$referencia%' ";
		if ($Opcion==2)
			$q=" where descripcion like '%$referencia%' ";
		if ($Opcion==3)
			$q=" where telefono like '%$referencia%' ";*/
		

		$sql="SELECT id as idsucursales,descripcion as nombre FROM sucursales "; //  $q "; 
		//echo "$sql";
		return ejecutarConsulta($sql);	

	}
	
	//Implementamos un método para desactivar categorías
	public function desactivar($idsucursales)
	{
		$sql="UPDATE sucursales SET condicion='0' WHERE id='$idsucursales'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idsucursales)
	{
		$sql="UPDATE sucursales SET condicion='1' WHERE id='$idsucursales'";
		return ejecutarConsulta($sql);
	}
}

?>
