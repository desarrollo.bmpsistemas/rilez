
<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Usuarios
{
	//Implementamos nuestro constructor 
	public function __construct() 
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($nombre,$curp,$imss,$fechadeingreso,$idsucursales,$iddepartamentos,$idpuestos,$idestatus,$telefono,$correo,$sueldo,$sdi,$login,$clave,$imagen,$descuento)
	{
		if (empty($imagen) || strlen($imagen)<1)
			$imagen="default.jpg";
		$sql="INSERT INTO capturistas (descripcion,     curp,num_imss,fecha_de_ingreso,sucursal_id,    departamento_id,    puesto_id,  estatus_id,  telefono,  correo,   sueldo_diario,sueldo_integrado,login,clave,imagen,condicion,descuento)
				      		      VALUES ('$nombre','$curp','$imss','$fechadeingreso','$idsucursales','$iddepartamentos','$idpuestos','$idestatus','$telefono','$correo','$sueldo',               '$sdi','$login','$clave','$imagen','1','$descuento')";
		$id=ejecutarConsulta_retornarID($sql);		      		      
		$sql1="SELECT id,imagen from capturistas  WHERE id='$id'";
		$imagen="0";
		$id=0;
		$rs= ejecutarConsulta($sql1);
		while($fila = mysqli_fetch_assoc( $rs ) )
		{
			$id= $fila['id'];
	    	$imagen= $fila['imagen'];
		}
		return $imagen."&".$id;
	}

	//Implementamos un método para editar registros
	public function editar($idusuarios,$nombre,$curp,$imss,$fechadeingreso,$idsucursales,$iddepartamentos,$idpuestos,$idestatus,$telefono,$correo,$sueldo,$sdi,$login,$clave,$imagen,$descuento)
	{
		$sql0="UPDATE capturistas SET 
		descripcion='$nombre',
		curp='$curp',
		num_imss='$imss',
		fecha_de_ingreso='$fechadeingreso',
		sucursal_id='$idsucursales',
		departamento_id='$iddepartamentos',
		puesto_id='$idpuestos',
		estatus_id='$idestatus',
		telefono='$telefono',
		correo='$correo',
		sueldo_diario='$sueldo',
		sueldo_integrado='$sdi',
		login='$login',
		clave='$clave',
		imagen='$imagen',
		descuento='$descuento'
		WHERE id='$idusuarios'";
		ejecutarConsulta($sql0);
		$sql1="SELECT id,imagen from capturistas  WHERE id='$idusuarios'";
		$imagen="0";
		$id=0;
		$rs= ejecutarConsulta($sql1);
		while($fila = mysqli_fetch_assoc( $rs ) )
		{
			$id= $fila['id'];
	    	$imagen= $fila['imagen'];
		}
		return $imagen."&".$id;

	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idusuarios)
	{
		$sql="UPDATE capturistas SET condicion='0' WHERE id='$idusuarios'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idusuarios)
	{
		$sql="UPDATE capturistas SET condicion='1' WHERE id='$idusuario'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idusuarios)
	{
		$sql="SELECT capturistas.id AS idusuarios,capturistas.descripcion AS nombre,curp,
		num_imss AS imss,fecha_de_ingreso AS fechadeingreso,fecha_de_reingreso AS fechadereingreso,
		fecha_de_baja AS fechadebaja,telefono,correo,sueldo_diario AS sueldo,sueldo_integrado AS sdi,
		login,clave,imagen,departamento_id AS iddepartamentos,puesto_id AS idpuestos,
		estatus_id AS idestatus,sucursal_id as idsucursales,descuento FROM capturistas
		INNER JOIN puestos ON puestos.id=capturistas.puesto_id
		INNER JOIN departamentos ON departamentos.id=capturistas.departamento_id
		INNER JOIN estatus ON estatus.id=capturistas.estatus_id
		WHERE capturistas.id='$idusuarios'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar($Opcion,$referencia)
	{
		$q=" ";
		if ($Opcion==1)
		{
			$q="where sucursal_id='$referencia' ";
		}
		if ($Opcion==2)
		{
			$q="where capturistas.descripcion like '%$referencia%' ";
		}
		if ($Opcion==3)
		{
			$q="where capturistas.id>0 ";
		}
		
		if ($Opcion>0)
		{	
		$sql="SELECT capturistas.id AS idusuarios,sucursales.noMcomercial AS sucursal,capturistas.descripcion AS nombre,
			capturistas.fecha_de_ingreso AS fechadeingreso,capturistas.num_imss as imss,estatus.descripcion AS estatus,capturistas.login,
			capturistas.imagen,capturistas.condicion FROM capturistas
			INNER JOIN sucursales ON sucursales.id=capturistas.sucursal_id 
			INNER JOIN estatus ON estatus.id=capturistas.estatus_id ".$q;
			return ejecutarConsulta($sql);		
		}
	}
	//funcion para regresar los permisos de los usuarios
	public function listarmarcados($idusuarios)
	{
		//$sql="SELECT * FROM usuario_permiso where idusuario='$idusuario' ";
		//return ejecutarConsulta($sql);		
	}
	public function verificar($login,$clave)
	{
		$sql="SELECT id as capturista_id,descripcion as nombre,telefono,correo,imagen,login,clave,rol_id,almacen_id,autorizador,descuento  FROM capturistas WHERE login ='$login'  and condicion='1' and clave='$clave'";
		return ejecutarConsulta($sql);		
	}
	public function autorizacion($login,$clave)
	{
		$sql="SELECT autorizador  FROM capturistas WHERE login ='$login'  and condicion='1' and clave='$clave'";
		//echo "$sql";
		//return ejecutarConsulta($sql);
		return ejecutarConsultaSimpleFila($sql);		
	}
	public function select()
	{
		$sql="SELECT id as idusuarios,descripcion as nombre FROM capturistas where condicion=1";
		return ejecutarConsulta($sql);		
	}
}

?>