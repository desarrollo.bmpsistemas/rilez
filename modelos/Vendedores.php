<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Vendedores
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($nombre)
	{
		$sql="INSERT INTO vendedores (descripcion,condicion)
		VALUES ('$nombre','1')";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para editar registros
	public function editar($idvendedores,$nombre)
	{
		$sql="UPDATE vendedores SET descripcion='$nombre' WHERE id='$idvendedores'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idvendedores)
	{
		$sql="UPDATE vendedores SET condicion='0' WHERE id='$idvendedores'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idvendedores)
	{
		$sql="UPDATE vendedores SET condicion='1' WHERE id='$idvendedores'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idvendedores)
	{
		$sql="SELECT id,descripcion as nombre,fecha_de_alta,condicion FROM vendedores WHERE id='$idvendedores'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar()
	{
		$sql="SELECT id as idvendedores, descripcion as nombre,fecha_de_alta,condicion FROM vendedores";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los registros y mostrar en el select
	public function select()
	{
		$sql="SELECT id as idvendedores, descripcion as nombre FROM vendedores where condicion=1";
		return ejecutarConsulta($sql);		
	}
}

?>