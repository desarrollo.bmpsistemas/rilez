<?php

require_once 'fpdf.php';
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PDF_rilez
 *
 * @author Joel
 */
if (function_exists('mcrypt_encrypt')) {

    function RC4($key, $data) {
        return mcrypt_encrypt(MCRYPT_ARCFOUR, $key, $data, MCRYPT_MODE_STREAM, '');
    }

} else {

    function RC4($key, $data) {
        static $last_key, $last_state;

        if ($key != $last_key) {
            $k = str_repeat($key, 256 / strlen($key) + 1);
            $state = range(0, 255);
            $j = 0;
            for ($i = 0; $i < 256; $i++) {
                $t = $state[$i];
                $j = ($j + $t + ord($k[$i])) % 256;
                $state[$i] = $state[$j];
                $state[$j] = $t;
            }
            $last_key = $key;
            $last_state = $state;
        }
        else
            $state = $last_state;

        $len = strlen($data);
        $a = 0;
        $b = 0;
        $out = '';
        for ($i = 0; $i < $len; $i++) {
            $a = ($a + 1) % 256;
            $t = $state[$a];
            $b = ($b + $t) % 256;
            $state[$a] = $state[$b];
            $state[$b] = $t;
            $k = $state[($state[$a] + $state[$b]) % 256];
            $out .= chr(ord($data[$i]) ^ $k);
        }
        return $out;
    }

}

class PDF_Rilez extends FPDF {

    var $encrypted = false;  //whether document is protected
    var $Uvalue;             //U entry in pdf document
    var $Ovalue;             //O entry in pdf document
    var $Pvalue;             //P entry in pdf document
    var $enc_obj_id;         //encryption object id

    /**
     * Function to set permissions as well as user and owner passwords
     *
     * - permissions is an array with values taken from the following list:
     *   copy, print, modify, annot-forms
     *   If a value is present it means that the permission is granted
     * - If a user password is set, user will be prompted before document is opened
     * - If an owner password is set, document can be opened in privilege mode with no
     *   restriction if that password is entered
     */

    function SetProtection($permissions=array(), $user_pass='', $owner_pass=null) {
        $options = array('print' => 4, 'modify' => 8, 'copy' => 16, 'annot-forms' => 32);
        $protection = 192;
        foreach ($permissions as $permission) {
            if (!isset($options[$permission]))
                $this->Error('Incorrect permission: ' . $permission);
            $protection += $options[$permission];
        }
        if ($owner_pass === null)
            $owner_pass = uniqid(rand());
        $this->encrypted = true;
        $this->padding = "\x28\xBF\x4E\x5E\x4E\x75\x8A\x41\x64\x00\x4E\x56\xFF\xFA\x01\x08" .
                "\x2E\x2E\x00\xB6\xD0\x68\x3E\x80\x2F\x0C\xA9\xFE\x64\x53\x69\x7A";
        $this->_generateencryptionkey($user_pass, $owner_pass, $protection);
    }

    /*     * **************************************************************************
     *                                                                           *
     *                              Private methods                              *
     *                                                                           *
     * ************************************************************************** */

    function _putstream($s) {
        if ($this->encrypted) {
            $s = RC4($this->_objectkey($this->n), $s);
        }
        parent::_putstream($s);
    }

    function _textstring($s) {
        if ($this->encrypted) {
            $s = RC4($this->_objectkey($this->n), $s);
        }
        return parent::_textstring($s);
    }

    /**
     * Compute key depending on object number where the encrypted data is stored
     */
    function _objectkey($n) {
        return substr($this->_md5_16($this->encryption_key . pack('VXxx', $n)), 0, 10);
    }

    function _putresources() {
        parent::_putresources();
        if ($this->encrypted) {
            $this->_newobj();
            $this->enc_obj_id = $this->n;
            $this->_out('<<');
            $this->_putencryption();
            $this->_out('>>');
            $this->_out('endobj');
        }
    }

    function _putencryption() {
        $this->_out('/Filter /Standard');
        $this->_out('/V 1');
        $this->_out('/R 2');
        $this->_out('/O (' . $this->_escape($this->Ovalue) . ')');
        $this->_out('/U (' . $this->_escape($this->Uvalue) . ')');
        $this->_out('/P ' . $this->Pvalue);
    }

    function _puttrailer() {
        parent::_puttrailer();
        if ($this->encrypted) {
            $this->_out('/Encrypt ' . $this->enc_obj_id . ' 0 R');
            $this->_out('/ID [()()]');
        }
    }

    /**
     * Get MD5 as binary string
     */
    function _md5_16($string) {
        return pack('H*', md5($string));
    }

    /**
     * Compute O value
     */
    function _Ovalue($user_pass, $owner_pass) {
        $tmp = $this->_md5_16($owner_pass);
        $owner_RC4_key = substr($tmp, 0, 5);
        return RC4($owner_RC4_key, $user_pass);
    }

    /**
     * Compute U value
     */
    function _Uvalue() {
        return RC4($this->encryption_key, $this->padding);
    }

    /**
     * Compute encryption key
     */
    function _generateencryptionkey($user_pass, $owner_pass, $protection) {
        // Pad passwords
        $user_pass = substr($user_pass . $this->padding, 0, 32);
        $owner_pass = substr($owner_pass . $this->padding, 0, 32);
        // Compute O value
        $this->Ovalue = $this->_Ovalue($user_pass, $owner_pass);
        // Compute encyption key
        $tmp = $this->_md5_16($user_pass . $this->Ovalue . chr($protection) . "\xFF\xFF\xFF");
        $this->encryption_key = substr($tmp, 0, 5);
        // Compute U value
        $this->Uvalue = $this->_Uvalue();
        // Compute P value
        $this->Pvalue = -(($protection ^ 255) + 1);
    }

    function VCell($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false) {
        //Output a cell
        $k = $this->k;
        if ($this->y + $h > $this->PageBreakTrigger && !$this->InHeader && !$this->InFooter && $this->AcceptPageBreak()) {
            //Automatic page break
            $x = $this->x;
            $ws = $this->ws;
            if ($ws > 0) {
                $this->ws = 0;
                $this->_out('0 Tw');
            }
            $this->AddPage($this->CurOrientation, $this->CurPageFormat);
            $this->x = $x;
            if ($ws > 0) {
                $this->ws = $ws;
                $this->_out(sprintf('%.3F Tw', $ws * $k));
            }
        }
        if ($w == 0)
            $w = $this->w - $this->rMargin - $this->x;
        $s = '';
// begin change Cell function 
        if ($fill || $border > 0) {
            if ($fill)
                $op = ($border > 0) ? 'B' : 'f';
            else
                $op='S';
            if ($border > 1) {
                $s = sprintf('q %.2F w %.2F %.2F %.2F %.2F re %s Q ', $border, $this->x * $k, ($this->h - $this->y) * $k, $w * $k, -$h * $k, $op);
            }
            else
                $s=sprintf('%.2F %.2F %.2F %.2F re %s ', $this->x * $k, ($this->h - $this->y) * $k, $w * $k, -$h * $k, $op);
        }
        if (is_string($border)) {
            $x = $this->x;
            $y = $this->y;
            if (is_int(strpos($border, 'L')))
                $s.=sprintf('%.2F %.2F m %.2F %.2F l S ', $x * $k, ($this->h - $y) * $k, $x * $k, ($this->h - ($y + $h)) * $k);
            else if (is_int(strpos($border, 'l')))
                $s.=sprintf('q 2 w %.2F %.2F m %.2F %.2F l S Q ', $x * $k, ($this->h - $y) * $k, $x * $k, ($this->h - ($y + $h)) * $k);

            if (is_int(strpos($border, 'T')))
                $s.=sprintf('%.2F %.2F m %.2F %.2F l S ', $x * $k, ($this->h - $y) * $k, ($x + $w) * $k, ($this->h - $y) * $k);
            else if (is_int(strpos($border, 't')))
                $s.=sprintf('q 2 w %.2F %.2F m %.2F %.2F l S Q ', $x * $k, ($this->h - $y) * $k, ($x + $w) * $k, ($this->h - $y) * $k);

            if (is_int(strpos($border, 'R')))
                $s.=sprintf('%.2F %.2F m %.2F %.2F l S ', ($x + $w) * $k, ($this->h - $y) * $k, ($x + $w) * $k, ($this->h - ($y + $h)) * $k);
            else if (is_int(strpos($border, 'r')))
                $s.=sprintf('q 2 w %.2F %.2F m %.2F %.2F l S Q ', ($x + $w) * $k, ($this->h - $y) * $k, ($x + $w) * $k, ($this->h - ($y + $h)) * $k);

            if (is_int(strpos($border, 'B')))
                $s.=sprintf('%.2F %.2F m %.2F %.2F l S ', $x * $k, ($this->h - ($y + $h)) * $k, ($x + $w) * $k, ($this->h - ($y + $h)) * $k);
            else if (is_int(strpos($border, 'b')))
                $s.=sprintf('q 2 w %.2F %.2F m %.2F %.2F l S Q ', $x * $k, ($this->h - ($y + $h)) * $k, ($x + $w) * $k, ($this->h - ($y + $h)) * $k);
        }
        if (trim($txt) != '') {
            $cr = substr_count($txt, "\n");
            if ($cr > 0) { // Multi line
                $txts = explode("\n", $txt);
                $lines = count($txts);
                for ($l = 0; $l < $lines; $l++) {
                    $txt = $txts[$l];
                    $w_txt = $this->GetStringWidth($txt);
                    if ($align == 'U')
                        $dy = $this->cMargin + $w_txt;
                    elseif ($align == 'D')
                        $dy = $h - $this->cMargin;
                    else
                        $dy= ( $h + $w_txt) / 2;
                    $txt = str_replace(')', '\\)', str_replace('(', '\\(', str_replace('\\', '\\\\', $txt)));
                    if ($this->ColorFlag)
                        $s.='q ' . $this->TextColor . ' ';
                    $s.=sprintf('BT 0 1 -1 0 %.2F %.2F Tm (%s) Tj ET ', ($this->x + .5 * $w + (.7 + $l - $lines / 2) * $this->FontSize) * $k, ($this->h - ($this->y + $dy)) * $k, $txt);
                    if ($this->ColorFlag)
                        $s.=' Q ';
                }
            }
            else { // Single line
                $w_txt = $this->GetStringWidth($txt);
                $Tz = 100;
                if ($w_txt > $h - 2 * $this->cMargin) {
                    $Tz = ($h - 2 * $this->cMargin) / $w_txt * 100;
                    $w_txt = $h - 2 * $this->cMargin;
                }
                if ($align == 'U')
                    $dy = $this->cMargin + $w_txt;
                elseif ($align == 'D')
                    $dy = $h - $this->cMargin;
                else
                    $dy= ( $h + $w_txt) / 2;
                $txt = str_replace(')', '\\)', str_replace('(', '\\(', str_replace('\\', '\\\\', $txt)));
                if ($this->ColorFlag)
                    $s.='q ' . $this->TextColor . ' ';
                $s.=sprintf('q BT 0 1 -1 0 %.2F %.2F Tm %.2F Tz (%s) Tj ET Q ', ($this->x + .5 * $w + .3 * $this->FontSize) * $k, ($this->h - ($this->y + $dy)) * $k, $Tz, $txt);
                if ($this->ColorFlag)
                    $s.=' Q ';
            }
        }
// end change Cell function 
        if ($s)
            $this->_out($s);
        $this->lasth = $h;
        if ($ln > 0) {
            //Go to next line
            $this->y+=$h;
            if ($ln == 1)
                $this->x = $this->lMargin;
        }
        else
            $this->x+=$w;
    }

    function Cell($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='') {
        //Output a cell
        $k = $this->k;
        if ($this->y + $h > $this->PageBreakTrigger && !$this->InHeader && !$this->InFooter && $this->AcceptPageBreak()) {
            //Automatic page break
            $x = $this->x;
            $ws = $this->ws;
            if ($ws > 0) {
                $this->ws = 0;
                $this->_out('0 Tw');
            }
            $this->AddPage($this->CurOrientation, $this->CurPageFormat);
            $this->x = $x;
            if ($ws > 0) {
                $this->ws = $ws;
                $this->_out(sprintf('%.3F Tw', $ws * $k));
            }
        }
        if ($w == 0)
            $w = $this->w - $this->rMargin - $this->x;
        $s = '';
// begin change Cell function
        if ($fill || $border > 0) {
            if ($fill)
                $op = ($border > 0) ? 'B' : 'f';
            else
                $op='S';
            if ($border > 1) {
                $s = sprintf('q %.2F w %.2F %.2F %.2F %.2F re %s Q ', $border, $this->x * $k, ($this->h - $this->y) * $k, $w * $k, -$h * $k, $op);
            }
            else
                $s=sprintf('%.2F %.2F %.2F %.2F re %s ', $this->x * $k, ($this->h - $this->y) * $k, $w * $k, -$h * $k, $op);
        }
        if (is_string($border)) {
            $x = $this->x;
            $y = $this->y;
            if (is_int(strpos($border, 'L')))
                $s.=sprintf('%.2F %.2F m %.2F %.2F l S ', $x * $k, ($this->h - $y) * $k, $x * $k, ($this->h - ($y + $h)) * $k);
            else if (is_int(strpos($border, 'l')))
                $s.=sprintf('q 2 w %.2F %.2F m %.2F %.2F l S Q ', $x * $k, ($this->h - $y) * $k, $x * $k, ($this->h - ($y + $h)) * $k);

            if (is_int(strpos($border, 'T')))
                $s.=sprintf('%.2F %.2F m %.2F %.2F l S ', $x * $k, ($this->h - $y) * $k, ($x + $w) * $k, ($this->h - $y) * $k);
            else if (is_int(strpos($border, 't')))
                $s.=sprintf('q 2 w %.2F %.2F m %.2F %.2F l S Q ', $x * $k, ($this->h - $y) * $k, ($x + $w) * $k, ($this->h - $y) * $k);

            if (is_int(strpos($border, 'R')))
                $s.=sprintf('%.2F %.2F m %.2F %.2F l S ', ($x + $w) * $k, ($this->h - $y) * $k, ($x + $w) * $k, ($this->h - ($y + $h)) * $k);
            else if (is_int(strpos($border, 'r')))
                $s.=sprintf('q 2 w %.2F %.2F m %.2F %.2F l S Q ', ($x + $w) * $k, ($this->h - $y) * $k, ($x + $w) * $k, ($this->h - ($y + $h)) * $k);

            if (is_int(strpos($border, 'B')))
                $s.=sprintf('%.2F %.2F m %.2F %.2F l S ', $x * $k, ($this->h - ($y + $h)) * $k, ($x + $w) * $k, ($this->h - ($y + $h)) * $k);
            else if (is_int(strpos($border, 'b')))
                $s.=sprintf('q 2 w %.2F %.2F m %.2F %.2F l S Q ', $x * $k, ($this->h - ($y + $h)) * $k, ($x + $w) * $k, ($this->h - ($y + $h)) * $k);
        }
        if (trim($txt) != '') {
            $cr = substr_count($txt, "\n");
            if ($cr > 0) { // Multi line
                $txts = explode("\n", $txt);
                $lines = count($txts);
                for ($l = 0; $l < $lines; $l++) {
                    $txt = $txts[$l];
                    $w_txt = $this->GetStringWidth($txt);
                    if ($align == 'R')
                        $dx = $w - $w_txt - $this->cMargin;
                    elseif ($align == 'C')
                        $dx = ($w - $w_txt) / 2;
                    else
                        $dx=$this->cMargin;

                    $txt = str_replace(')', '\\)', str_replace('(', '\\(', str_replace('\\', '\\\\', $txt)));
                    if ($this->ColorFlag)
                        $s.='q ' . $this->TextColor . ' ';
                    $s.=sprintf('BT %.2F %.2F Td (%s) Tj ET ', ($this->x + $dx) * $k, ($this->h - ($this->y + .5 * $h + (.7 + $l - $lines / 2) * $this->FontSize)) * $k, $txt);
                    if ($this->underline)
                        $s.=' ' . $this->_dounderline($this->x + $dx, $this->y + .5 * $h + .3 * $this->FontSize, $txt);
                    if ($this->ColorFlag)
                        $s.=' Q ';
                    if ($link)
                        $this->Link($this->x + $dx, $this->y + .5 * $h - .5 * $this->FontSize, $w_txt, $this->FontSize, $link);
                }
            }
            else { // Single line
                $w_txt = $this->GetStringWidth($txt);
                $Tz = 100;
                if ($w_txt > $w - 2 * $this->cMargin) { // Need compression
                    $Tz = ($w - 2 * $this->cMargin) / $w_txt * 100;
                    $w_txt = $w - 2 * $this->cMargin;
                }
                if ($align == 'R')
                    $dx = $w - $w_txt - $this->cMargin;
                elseif ($align == 'C')
                    $dx = ($w - $w_txt) / 2;
                else
                    $dx=$this->cMargin;
                $txt = str_replace(')', '\\)', str_replace('(', '\\(', str_replace('\\', '\\\\', $txt)));
                if ($this->ColorFlag)
                    $s.='q ' . $this->TextColor . ' ';
                $s.=sprintf('q BT %.2F %.2F Td %.2F Tz (%s) Tj ET Q ', ($this->x + $dx) * $k, ($this->h - ($this->y + .5 * $h + .3 * $this->FontSize)) * $k, $Tz, $txt);
                if ($this->underline)
                    $s.=' ' . $this->_dounderline($this->x + $dx, $this->y + .5 * $h + .3 * $this->FontSize, $txt);
                if ($this->ColorFlag)
                    $s.=' Q ';
                if ($link)
                    $this->Link($this->x + $dx, $this->y + .5 * $h - .5 * $this->FontSize, $w_txt, $this->FontSize, $link);
            }
        }
// end change Cell function
        if ($s)
            $this->_out($s);
        $this->lasth = $h;
        if ($ln > 0) {
            //Go to next line
            $this->y+=$h;
            if ($ln == 1)
                $this->x = $this->lMargin;
        }
        else
            $this->x+=$w;
    }

    function RoundedRect($x, $y, $w, $h, $r, $corners = '1234', $style = '') {
        $k = $this->k;
        $hp = $this->h;
        if ($style == 'F')
            $op = 'f';
        elseif ($style == 'FD' || $style == 'DF')
            $op = 'B';
        else
            $op='S';
        $MyArc = 4 / 3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m', ($x + $r) * $k, ($hp - $y) * $k));

        $xc = $x + $w - $r;
        $yc = $y + $r;
        $this->_out(sprintf('%.2F %.2F l', $xc * $k, ($hp - $y) * $k));
        if (strpos($corners, '2') === false)
            $this->_out(sprintf('%.2F %.2F l', ($x + $w) * $k, ($hp - $y) * $k));
        else
            $this->_Arc($xc + $r * $MyArc, $yc - $r, $xc + $r, $yc - $r * $MyArc, $xc + $r, $yc);

        $xc = $x + $w - $r;
        $yc = $y + $h - $r;
        $this->_out(sprintf('%.2F %.2F l', ($x + $w) * $k, ($hp - $yc) * $k));
        if (strpos($corners, '3') === false)
            $this->_out(sprintf('%.2F %.2F l', ($x + $w) * $k, ($hp - ($y + $h)) * $k));
        else
            $this->_Arc($xc + $r, $yc + $r * $MyArc, $xc + $r * $MyArc, $yc + $r, $xc, $yc + $r);

        $xc = $x + $r;
        $yc = $y + $h - $r;
        $this->_out(sprintf('%.2F %.2F l', $xc * $k, ($hp - ($y + $h)) * $k));
        if (strpos($corners, '4') === false)
            $this->_out(sprintf('%.2F %.2F l', ($x) * $k, ($hp - ($y + $h)) * $k));
        else
            $this->_Arc($xc - $r * $MyArc, $yc + $r, $xc - $r, $yc + $r * $MyArc, $xc - $r, $yc);

        $xc = $x + $r;
        $yc = $y + $r;
        $this->_out(sprintf('%.2F %.2F l', ($x) * $k, ($hp - $yc) * $k));
        if (strpos($corners, '1') === false) {
            $this->_out(sprintf('%.2F %.2F l', ($x) * $k, ($hp - $y) * $k));
            $this->_out(sprintf('%.2F %.2F l', ($x + $r) * $k, ($hp - $y) * $k));
        }
        else
            $this->_Arc($xc - $r, $yc - $r * $MyArc, $xc - $r * $MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3) {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1 * $this->k, ($h - $y1) * $this->k, $x2 * $this->k, ($h - $y2) * $this->k, $x3 * $this->k, ($h - $y3) * $this->k));
    }

    /**
     *
     * @param type $rows 
     */
    function configureRow($rows, $dump = FALSE) {
        if (empty($rows)) {
            return;
        }
        $this->numFields = count($rows[0]);
        // starting col width
        $this->sColWidth = (($this->w - $this->lMargin - $this->rMargin)) / $this->numFields;
//        foreach ($rows[0] as $f => $value) {
//            $$this->colWidth[$f] = $this->GetStringWidth($f);
////            $fullw += $widths[$f];
//            if(is_numeric($value)){
//                $this->aligns[$f] = 'R';
//            }else{
//                $this->aligns[$f] = 'L';
//            }
//        }
        // loop through results header and set initial col widths/ titles/ alignment
        // if a col title is less than the starting col width / reduce that column size
        //for ($i = 0; $i < $this->numFields; $i++) {
        foreach ($rows[0] as $i => $value) {
            $stringWidth = $this->getstringwidth($i) + 6;
            if (($stringWidth) < $this->sColWidth) {
                $colFits[$i] = $stringWidth;
                // set any column titles less than the start width to the column title width
            }
            $this->colTitle[$i] = str_replace('_', ' ', strtoupper($i));
            if (is_numeric($value))
                $this->colAlign[$i] = 'R';
            else
                $this->colAlign[$i] = 'L';
//            switch (mysql_field_type($this->results, $i)) {
//                case 'int':
//                    $this->colAlign[$i] = 'R';
//                    break;
//                case 'real'://JPG
//                    $this->colAlign[$i] = 'R';
//                    break;
//                default:
//                    $this->colAlign[$i] = 'L';
//            }
        }
//        pre($this->colAlign);
//        echo "Fits";
//        pre($colFits);
//        echo "Width";
//        pre($this->colWidth);
        // loop through the data, any column whose contents is bigger that the col size is
        // resized
//        while ($row = mysql_fetch_row($this->results)) {
        foreach ($rows as $row) {
            foreach ($colFits as $key => $val) {
                $stringWidth = $this->Getstringwidth($row[$key]) + 6;
                if (($stringWidth) > $this->sColWidth) {
                    // any col where row is bigger than the start width is now discarded
                    unset($colFits[$key]);
                } else {
                    // if text is not bigger than the current column width setting enlarge the column
                    if (($stringWidth) > $val) {
                        $colFits[$key] = ($stringWidth);
                    }
                }
            }
        }
//        echo "Fits";
//        pre($colFits);
//        echo "Width";
//        pre($this->colWidth);
        foreach ($colFits as $key => $val) {
            // set fitted columns to smallest size
            $this->colWidth[$key] = $val;
            // to work out how much (if any) space has been freed up
            $totAlreadyFitted += $val;
        }

        $surplus = (sizeof($colFits) * $this->sColWidth) - ($totAlreadyFitted);
        //for ($i = 0; $i < $this->numFields; $i++) {
        foreach ($rows[0] as $i => $value) {
            if (!in_array($i, array_keys($colFits))) {
//                echo "$i<br/>";
                $this->colWidth[$i] = $this->sColWidth + ($surplus / (($this->numFields) - sizeof($colFits)));
            }
        }
//        echo "Title";
//        pre($this->colTitle);
//        echo "Fits";
//        pre($colFits);
//        echo "Width";
//        pre($this->colWidth);
//
//        ksort($this->tablewidths);

        if ($dump) {
            Header('Content-type: text/plain');
//                for ($i = 0; $i < $this->numFields; $i++) {
            foreach ($this->colTitle as $field => $value) {
                if (strlen($value) > $flength) {
                    $flength = strlen($value);
                }
            }
            switch ($this->k) {
                case 72 / 25.4:
                    $unit = 'millimeters';
                    break;
                case 72 / 2.54:
                    $unit = 'centimeters';
                    break;
                case 72:
                    $unit = 'inches';
                    break;
                default:
                    $unit = 'points';
            }
            print "All measurements in $unit\n\n";
//                for ($i = 0; $i < $this->numFields; $i++) {
//                    printf("%-{$flength}s : %-10s : %10f\n",
//                            mysql_field_name($this->results, $i),
//                            mysql_field_type($this->results, $i),
//                            $this->tablewidths[$i]);
//                }
            $txt_Titles = "\n";
            $txt_Titles .= "\$pdf->colTitle=\n\tarray(\n\t\t";
            $txt_widths = "\n";
            $txt_widths .= "\$pdf->colWidth=\n\tarray(\n\t\t";
            $txt_Align = "\n";
            $txt_Align .= "\$pdf->colAlign=\n\tarray(\n\t\t";
            $txt_Border = "\n";
            $txt_Border .= "\$pdf->colBorder=\n\tarray(\n\t\t";
            $i = 0;
//                for ($i = 0; $i < $this->numFields; $i++) {
            foreach ($this->colTitle as $field => $title) {
                printf("%-{$flength}s : %10f\n", $field, $this->colWidth[$field]);
                $txt_Titles .= ( $i < ($this->numFields - 1)) ?
                        "'$field' => '" . $title . "', /* " . $field . " */\n\t\t" :
                        "'$field' => '" . $title . "' /* " . $field . " */";
                $txt_widths .= ( $i < ($this->numFields - 1)) ?
                        "'$field' => " . sprintf("%0.2f",$this->colWidth[$field]) . ", /* " . $field . " */\n\t\t" :
                        "'$field' => " . sprintf("%0.2f",$this->colWidth[$field]) . " /* " . $field . " */";
                $txt_Align .= ( $i < ($this->numFields - 1)) ?
                        "'$field' => '" . $this->colAlign[$field] . "', /* " . $field . " */\n\t\t" :
                        "'$field' => '" . $this->colAlign[$field] . "' /* " . $field . " */";
                $txt_Border .= ( $i < ($this->numFields - 1)) ?
                        "'$field' => '" . "LB" . "', /* " . $field . " */\n\t\t" :
                        "'$field' => '" . "LBR" . "' /* " . $field . " */";
                $i++;
            }
            printf("%'-+{$flength}s : %10f\n", "", array_sum($this->colWidth));
            $txt_Titles .= "\n\t);\n";
            $txt_widths .= "\n\t);\n";
            $txt_Align .= "\n\t);\n";
            $txt_Border .= "\n\t);\n";
            echo $txt_Titles, $txt_widths, $txt_Align, $txt_Border;
            exit;
        }
    }

    function SetCompensation(&$width, $compensation = array()) {
//        if (empty($compensation))
//            return $width;
        $x = 0;
//        $maxw = 0;
        foreach ($width as $field => $w) {
//            if (empty($w))
//                $w = ( $this->w - $this->rMargin - $x);
//            $maxw = $maxw + $w;
            $x+=$w;
        }
        $compensationw = ( $this->w - $this->rMargin - $this->lMargin - $x);
        ;
        foreach ($width as $field => $w) {
            if (isset($compensation[$field])){
                $width[$field] += ( $compensationw * $compensation[$field]);
            }
        }
        return $compensationw;
    }

    public function __construct($orientation = 'P', $uni = 'mm', $format = 'Letter') {
        parent::__construct($orientation, $uni, $format);
//        $this->SetFont('Arial', '', 8);
    }

    /**
     *
     * @param type $format array(w,h)
     */
    function SetFormat($format) {
        //New size
        if ($this->orientation == 'P') {
            $this->w = $format[0];
            $this->h = $format[1];
        } else {
            $this->w = $format[1];
            $this->h = $format[0];
        }
        $this->wPt = $this->w * $this->k;
        $this->hPt = $this->h * $this->k;
    }

    function SetH($format) {
        if (is_null($format))
            return;
        //New size
//        if ($this->orientation == 'P') {
        $this->h = $format;
//        } else {
//            $this->w = $format;
//        }
        $this->wPt = $this->w * $this->k;
        $this->hPt = $this->h * $this->k;
    }

    function GetH() {
        return $this->h;
    }

    function Header() {
//        $this->RowH();
        if (is_array($this->headerMethod)) {
            foreach ($this->headerMethod as $header)
                foreach ($header as $method => $parameter) {
                    if (method_exists($this, $method)) {
                        call_user_func_array(array($this, $method), $parameter);
                    }
                }
        }
    }

    function Page($w, $h = 0, $format = '%d/{nb}', $border = 0, $ln = 0, $align = 'C', $fill = FAlSE, $link = '') {
        $this->Cell($w, $h, sprintf($format, $this->page), $border, $ln, $align, $fill, $link);
    }

    function Row($row, $cellh = 4, $border=0, $ln = 1, $width = array(), $align = array()) {
        if (empty($width))
            $width = $this->colWidth;
        if (empty($align))
            $align = $this->colAlign;
        $nb = 0;
//        for ($i = 0; $i < count($row); $i++)
        foreach ($width as $field => $value)
            $nb = max($nb, $this->NbLines($value, empty($this->colFormat[$field]) ? $row[$field] : sprintf($this->colFormat[$field], $row[$field])));
        $h = $cellh * $nb;
        $this->CheckPageBreak($h);
        $xi = $this->GetX();
        $yi = $this->GetY();
//        for ($i = 0; $i < count($row); $i++) {
        foreach ($width as $field => $w) {
//            $w = $value;
            $y = $this->GetY();
            $x = $this->GetX();
            if (empty($w))
                $w = ( $this->w - $this->rMargin - $x);
            $a = isset($align[$field]) ? $align[$field] : 'L';
            if (isset($row[$field])){
                $this->MultiCell($w, $cellh, empty($this->colFormat[$field]) ? $row[$field] : sprintf($this->colFormat[$field], $row[$field]), 0, $a);
                if(is_array($border)){
                    if(isset($border[$field])){
                        $_border = strtoupper($border[$field]);
                        if (strstr($_border, '1') !== false) {
                            $this->Rect($x, $y, $w, $h);
                            $_border = "";
                        }
                        if (strstr($_border, 'L') !== false) {
                            $this->Line($x, $y, $x, $y + $h);
                        }
                        if (strstr($_border, 'R') !== false) {
                            $this->Line($x + $w, $y, $x + $w, $y + $h);
                        }
                        if (strstr($_border, 'T') !== false) {
                            $this->Line($x, $y, $x + $w, $y);
                        }
                        if (strstr($_border, 'B') !== false) {
                            $this->Line($x, $y + $h, $x + $w, $y + $h);
                        }
                    }
                }
            }
            $this->SetXY($x + $w, $y);
        }
        /**
         * Imprimir bordes
         */
        if(is_string($border)){
        $border.='';
        for ($i = 0, $iMAX = strlen($border); $i < $iMAX; $i++)
            switch (strtoupper($border[$i])) {
                case '1':
//                    //T
//                    $this->Line($xi, $yi, $this->GetX(), $yi);
//                    //R
//                    $this->Line($this->GetX(), $yi, $this->GetX(), $yi + $h);
//                    //B
//                    $this->Line($xi, $yi + $h, $this->GetX(), $yi + $h);
//                    //L
//                    $this->Line($xi, $yi, $xi, $yi + $h);
                    $this->Rect($xi, $yi, $this->GetX() - $xi, $h);
                    break;
                case 'T':
                    //T
                    $this->Line($xi, $yi, $this->GetX(), $yi);
                    break;
                case 'R':
                    //R
                    $this->Line($this->GetX(), $yi, $this->GetX(), $yi + $h);
                    break;
                case 'B':
                    //B
                    $this->Line($xi, $yi + $h, $this->GetX(), $yi + $h);
                    break;
                case 'L':
                    //L
                    $this->Line($xi, $yi, $xi, $yi + $h);
                    break;
                default:
//                    $b=0;
//                    $this->Rect($x, $y, $w, $h);
                    break;
            }
        }
        if ($ln)
            $this->SetXY($xi, $yi + $h);
        return $h;
    }

    function FancyRow($data, $width=array(), $cellh=4, $border=array(), $align=array(), $style=array(), $format=array(), $maxline=array()) {
        //Calculate the height of the row
        $nb = 0;
//        for ($i = 0; $i < count($data); $i++){
        if (is_array($data))
            foreach ($data as $i => $txt) {
                $nb = max($nb, $this->NbLines($width[$i], empty($format[$i]) ? $data[$i] : sprintf($format[$i], $data[$i])));
            }
        if (count($maxline)) {
            $_maxline = max($maxline);
            if ($nb > $_maxline) {
                $nb = $_maxline;
            }
        }
        $h = $cellh * $nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
//        for ($i = 0; $i < count($data); $i++) {
        if (is_array($data))
            foreach ($data as $i => $txt) {
                //Save the current position
                $x = $this->GetX();
                $y = $this->GetY();
                $w = $width[$i];
                if (empty($w))
                    $w = ( $this->w - $this->rMargin - $x);
                // alignment
                $a = isset($align[$i]) ? $align[$i] : 'L';
                // maxline
                $m = isset($maxline[$i]) ? $maxline[$i] : false;
                //Draw the border
                if ($border[$i] == 1) {
                    $this->Rect($x, $y, $w, $h);
                } else {
                    $_border = strtoupper($border[$i]);
                    if (strstr($_border, 'L') !== false) {
                        $this->Line($x, $y, $x, $y + $h);
                    }
                    if (strstr($_border, 'R') !== false) {
                        $this->Line($x + $w, $y, $x + $w, $y + $h);
                    }
                    if (strstr($_border, 'T') !== false) {
                        $this->Line($x, $y, $x + $w, $y);
                    }
                    if (strstr($_border, 'B') !== false) {
                        $this->Line($x, $y + $h, $x + $w, $y + $h);
                    }
                }
                // Setting Style
                if (isset($style[$i])) {
                    $this->SetFont('', $style[$i]);
                }
                $this->MultiCell($w, $cellh, empty($format[$i]) ? $data[$i] : sprintf($format[$i], $data[$i]), 0, $a, 0, $m);
                if (isset($style[$i]))
                    $this->SetFont('', '');//reset
                //Put the position to the right of the cell
                $this->SetXY($x + $w, $y);
            }
        //Go to the next line
        $this->Ln($h);
    }

    function ImageRow($row, $cellh = 4, $image_array = array(), $imageh = 0, $border=0, $ln = 1, $width = array(), $align = array()) {
        if (empty($width))
            $width = $this->colWidth;
        if (empty($align))
            $align = $this->colAlign;
        $nb = 0;
//        for ($i = 0; $i < count($row); $i++)
        foreach ($width as $field => $w) {
            $nb = max($nb, $this->NbLines($w, empty($this->colFormat[$field]) ? $row[$field] : sprintf($this->colFormat[$field], $row[$field])));
        }
        $h = $cellh * $nb;
//        $h = max($cellh * $nb,$imageh);
//        $ih = 0;
        $x = $this->GetX();
        foreach ($width as $field => $w) {
            if (empty($w))
                $w = ( $this->w - $this->rMargin - $x);
            if (isset($image_array[$field])) {
                $image = sprintf($image_array[$field]['sprintf'], $row[$field]);
                if (!file_exists($image))
                    $image = $image_array[$field]['default'];
                if (file_exists($image)) {
                    $image_array[$field]['src'] = $image;
                    $imagesize = getimagesize($image);
                    /*
                     * 0 = width
                     * 1 = height
                     */
                    $scale = $imagesize[1] / (max(($cellh * $nb), $imageh) - 2);
                    if (($imagesize[0] / $scale) > $w)
                        $scale = $imagesize[0] / ($w - 2);
                    $image_array[$field]['width'] = $imagesize[0] / $scale;
                    $image_array[$field]['height'] = $imagesize[1] / $scale;
                    $h = max($h, ($image_array[$field]['height'] + 2));
                }
            }
            $x = $x + $w;
        }
        $this->CheckPageBreak($h);
        $xi = $this->GetX();
        $yi = $this->GetY();
//        for ($i = 0; $i < count($row); $i++) {
        foreach ($width as $field => $w) {
//            $w = $value;
            $y = $this->GetY();
            $x = $this->GetX();
            if (empty($w))
                $w = ( $this->w - $this->rMargin - $x);
            $a = isset($align[$field]) ? $align[$field] : 'L';
            if (isset($image_array[$field])) {
                if (isset($image_array[$field]['src'])) {
                    $this->Image($image_array[$field]['src'], $x + 1, $y + 1, $image_array[$field]['width'], $image_array[$field]['height']);
                }
            }else
                $this->MultiCell($w, $cellh, empty($this->colFormat[$field]) ? $row[$field] : sprintf($this->colFormat[$field], $row[$field]), 0, $a);
            $this->SetXY($x + $w, $y);
        }
        /**
         * Imprimir bordes
         */
        for ($i = 0, $iMAX = strlen($border); $i < $iMAX; $i++)
            switch (strtoupper($border[$i])) {
                case '1':
//                    //T
//                    $this->Line($xi, $yi, $this->GetX(), $yi);
//                    //R
//                    $this->Line($this->GetX(), $yi, $this->GetX(), $yi + $h);
//                    //B
//                    $this->Line($xi, $yi + $h, $this->GetX(), $yi + $h);
//                    //L
//                    $this->Line($xi, $yi, $xi, $yi + $h);
                    $this->Rect($xi, $yi, $this->GetX() - $xi, $h);
                    break;
                case 'T':
                    //T
                    $this->Line($xi, $yi, $this->GetX(), $yi);
                    break;
                case 'R':
                    //R
                    $this->Line($this->GetX(), $yi, $this->GetX(), $yi + $h);
                    break;
                case 'B':
                    //B
                    $this->Line($xi, $yi + $h, $this->GetX(), $yi + $h);
                    break;
                case 'L':
                    //L
                    $this->Line($xi, $yi, $xi, $yi + $h);
                    break;
                default:
//                    $b=0;
//                    $this->Rect($x, $y, $w, $h);
                    break;
            }
        if ($ln)
            $this->Ln($h);
        return $h;
    }

    function Footer() {
        parent::Footer();
        if (is_array($this->footerMethod)) {
            foreach ($this->footerMethod as $footer)
                foreach ($footer as $method => $parameter) {
                    if (method_exists($this, $method)) {
                        call_user_func_array(array($this, $method), $parameter);
                    }
                }
        }
    }

    function FooterTotales($h=4, $ln=0, $totalesWidth = array(), $totales = array(), $totalesBorder = array(), $totalesAlign = array()){
//        $this->SetY( $this->h - $this->bMargin);
        if(empty ($totalesWidth)) $totalesWidth = $this->totalesWidth;
        if(empty ($totales)) $totales = $this->totales;
        if(empty ($totalesBorder)) $totalesBorder = $this->totalesBorder;
        if(empty ($totalesAlign)) $totalesAlign = $this->totalesAlign;
        $y = $this->GetY();
        foreach ($totalesWidth as $field => $w) {
            $x = $this->GetX();
            if (empty($w))
                $w = ( $this->w - $this->rMargin - $x);
            $this->Cell($w, $h, empty($this->colFormat[$field]) ? $totales[$field] : sprintf($this->colFormat[$field], $totales[$field]), $totalesBorder[$field], 0, $totalesAlign[$field]);
//            $nb = max($nb, $this->NbLines($w, empty($this->colFormat[$field]) ? $row[$field] : sprintf($this->colFormat[$field], $row[$field])));
            $this->SetXY($x + $w, $y);
        }
        if ($ln) $this->Ln($h);
//        $h = $this->Row($this->totales, 4, $this->totalesBorder, '1', $this->totalesWidth, $this->totalesAlign);
    }
    /**
     *
     * @param type $h 
     */
    function CheckPageBreak($h) {
        if ($this->GetY() + $h > $this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w, $txt) {
        $cw = &$this->CurrentFont['cw'];
        if ($w == 0)
            $w = $this->w - $this->rMargin - $this->x;
        $wmax = ($w - 2 * $this->cMargin) * 1000 / $this->FontSize;
        $s = str_replace("\r", '', $txt);
        $nb = strlen($s);
        if ($nb > 0 and $s[$nb - 1] == "\n")
            $nb--;
        $sep = -1;
        $i = 0;
        $j = 0;
        $l = 0;
        $nl = 1;
        while ($i < $nb) {
            $c = $s[$i];
            if ($c == "\n") {
                $i++;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
                continue;
            }
            if ($c == ' ')
                $sep = $i;
            $l+=$cw[$c];
            if ($l > $wmax) {
                if ($sep == -1) {
                    if ($i == $j)
                        $i++;
                }
                else
                    $i=$sep + 1;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }

}
?>