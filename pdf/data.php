<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of data
 *
 * @author Joel
 */
include_once '../cfd/config.php';
class data {
    function __construct($server, $username, $password, $database_name) {
        $this->conn = @mysql_connect($server, $username, $password);
        if (!$this->conn) {
            die('no se pudo conectar al servidor de BD');
        }
        if (!@mysql_select_db($database_name, $this->conn)) {
            die('no se pudo seleccionar la BD');
        }
//        SET CHARACTER SET utf8;
        $this->query("SET NAMES 'utf8';", FALSE);
    }
    public function query($query = "", $in_array = TRUE) {
        $result = @mysql_query($query, $this->conn);
        if ($result) {
            if (is_resource($result)) {
                if ($in_array) {
                    $rows = array();
                    $i = 0;
                    while ($row = mysql_fetch_assoc($result)) {
                        $rows[$i] = $row;
                        $i++;
                    }
                    return $rows;
                }else
                    return $result;
            }else
                return FALSE;
        }
    }
    function __destruct() {
        if (is_resource($this->conn))
            mysql_close($this->conn);
    }
}
?>
