<?php

include_once "data.php";

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$action = trim ( $_REQUEST ['action'] );
$subaction = $_REQUEST ['subaction'];

$sucursal_id = intval ( $_REQUEST ['sucursal_id'] );
$almacen_id = intval ( $_REQUEST ['almacen_id'] );

$linea_id = intval ( $_REQUEST ['linea_id'] );
$sublinea_id = intval ( $_REQUEST ['sublinea_id'] );

$tipo_mov_id = intval ( $_REQUEST ['tipo_mov_id'] );
$inventariar = $_REQUEST['inventariar'];
if (! empty ( $action )) {
	$data = new data ( $server, $username, $password, $database_name );
	$xml = '';
	$dom = new DOMDocument ( '1.0', 'UTF-8' );
	$root = $dom->createElement ( 'root' );
	$dom->appendChild ( $root );
	switch ($action) {
		case 'sucursal' :
			$where = '';
			if (! empty ( $sucursal_id )) {
				$where = (empty ( $where ) ? " WHERE" : " AND") . " id = $sucursal_id";
			}
			$query = "SELECT id AS data, nomcomercial AS label FROM sucursales$where";
			$resultado = $data->query ( $query );
			if ($resultado) {
				//                $dom = new DOMDocument('1.0', 'UTF-8');
				//                $root = $dom->createElement('root');
				//                $dom->appendChild($root);
				foreach ( $resultado as $fields ) {
					$r = $dom->createElement ( 'resultado' );
					$root->appendChild ( $r );
					foreach ( $fields as $field => $value ) {
						$a = $dom->createAttribute ( $field );
						//                        $a = $dom->createElement($field);
						$t = $dom->createTextNode ( $value );
						$a->appendChild ( $t );
						$r->appendChild ( $a );
					
	//                        $r->setAttribute($field, $value);
					}
				}
			
	//                $xml = $dom->saveXML();
			}
			break;
		case 'almacen' :
			$where = '';
			if (! empty ( $almacen_id )) {
				$where .= (empty ( $where ) ? " WHERE" : " AND") . " id = $almacen_id";
			} else if (! empty ( $sucursal_id ))
				$where .= (empty ( $where ) ? " WHERE" : " AND") . " sucursal_id = $sucursal_id";
			$query = "SELECT id AS data, nombre AS label, sucursal_id FROM almacenes$where ORDER BY sucursal_id";
			$resultado = $data->query ( $query );
			if ($resultado) {
				//                $dom = new DOMDocument('1.0', 'UTF-8');
				//                $root = $dom->createElement('root');
				//                $dom->appendChild($root);
				foreach ( $resultado as $fields ) {
					$r = $dom->createElement ( 'resultado' );
					$root->appendChild ( $r );
					foreach ( $fields as $field => $value ) {
						$a = $dom->createAttribute ( $field );
						//                        $a = $dom->createElement($field);
						$t = $dom->createTextNode ( $value );
						$a->appendChild ( $t );
						$r->appendChild ( $a );
					
	//                        $r->setAttribute($field, $value);
					}
				}
			
	//                $xml = $dom->saveXML();
			}
			break;
		case 'linea' :
			$where = '';
			if (! empty ( $linea_id )) {
				$where .= (empty ( $where ) ? " WHERE" : " AND") . " id = $linea_id";
			}
			$query = "SELECT id AS data, descripcion AS label FROM lineas$where";
			$resultado = $data->query ( $query );
			if ($resultado) {
				//                $dom = new DOMDocument('1.0', 'UTF-8');
				//                $root = $dom->createElement('root');
				//                $dom->appendChild($root);
				foreach ( $resultado as $fields ) {
					$r = $dom->createElement ( 'resultado' );
					$root->appendChild ( $r );
					foreach ( $fields as $field => $value ) {
						$a = $dom->createAttribute ( $field );
						//                        $a = $dom->createElement($field);
						$t = $dom->createTextNode ( $value );
						$a->appendChild ( $t );
						$r->appendChild ( $a );
					
	//                        $r->setAttribute($field, $value);
					}
				}
			
	//                $xml = $dom->saveXML();
			}
			break;
		case 'sublinea' :
			$where = '';
			if (! empty ( $sublinea_id )) {
				$where .= (empty ( $where ) ? " WHERE " : " AND") . " id = $sublinea_id";
			} else if (! empty ( $linea_id )) {
				$where .= (empty ( $where ) ? " WHERE" : " AND") . " linea_id = $linea_id";
			}
			$query = "SELECT id AS data, descripcion AS label, linea_id FROM sublineas$where ORDER BY linea_id";
			$resultado = $data->query ( $query );
			if ($resultado) {
				//                $dom = new DOMDocument('1.0', 'UTF-8');
				//                $root = $dom->createElement('root');
				//                $dom->appendChild($root);
				foreach ( $resultado as $fields ) {
					$r = $dom->createElement ( 'resultado' );
					$root->appendChild ( $r );
					foreach ( $fields as $field => $value ) {
						$a = $dom->createAttribute ( $field );
						//                        $a = $dom->createElement($field);
						$t = $dom->createTextNode ( $value );
						$a->appendChild ( $t );
						$r->appendChild ( $a );
					
	//                        $r->setAttribute($field, $value);
					}
				}
			
	//                $xml = $dom->saveXML();
			}
			break;
		
		case 'entradas' :
			$where = '';
			if (! empty ( $almacen_id )) {
				$where .= (empty ( $where ) ? " WHERE" : " AND") . " almacen_id = $almacen_id";
			}
			if (! empty ( $tipo_mov_id )) {
				$where .= (empty ( $where ) ? " WHERE" : " AND") . " tipo_mov_id = $tipo_mov_id";
			}
			if (! empty ( $inventariar )) {
				$where .= (empty ( $where ) ? " WHERE" : " AND") . " inventariar = '$inventariar'";
			}
			if (!empty ( $_REQUEST ['fecha'] )) {
				//SQLINJECTION
				$where .= (empty ( $where ) ? ' WHERE ' : ' AND ');
				if (isset ( $_REQUEST ['fecha_final'] ) && ! empty ( $_REQUEST ['fecha_final'] )) {
					$where .= "entradas.fecha BETWEEN '$_REQUEST[fecha]' AND '$_REQUEST[fecha_final]'";
				} else
					$where .= "entradas.fecha = '$_REQUEST[fecha]'";
			}//
			$query = "SELECT if(proveedores.id, proveedores.nomcomercial, almacenes.nombre) AS proveedor,entradas.*, estatus.descripcion as estatus
			FROM entradas INNER JOIN tipo_mov ON entradas.tipo_mov_id = tipo_mov.id INNER JOIN estatus ON entradas.estatus_id = estatus.id LEFT OUTER JOIN proveedores ON entradas.proveedor_id = proveedores.id LEFT OUTER JOIN almacenes ON entradas.almacen_origen = almacenes.id$where";
			$resultado = $data->query ( $query );
			if ($resultado) {
				//                $dom = new DOMDocument('1.0', 'UTF-8');
				//                $root = $dom->createElement('root');
				//                $dom->appendChild($root);
				foreach ( $resultado as $fields ) {
					$r = $dom->createElement ( 'entrada' );
					$root->appendChild ( $r );
					foreach ( $fields as $field => $value ) {
						$a = $dom->createAttribute ( $field );
						//                        $a = $dom->createElement($field);
						$t = $dom->createTextNode ( $value );
						$a->appendChild ( $t );
						$r->appendChild ( $a );
					
	//                        $r->setAttribute($field, $value);
					}
				}
			
	//                $xml = $dom->saveXML();
			}
			break;
		
		case 'salidas' :
			$where = '';
			if (! empty ( $almacen_id )) {
				$where .= (empty ( $where ) ? " WHERE" : " AND") . " almacen_id = $almacen_id";
			}
			if (! empty ( $tipo_mov_id )) {
				$where .= (empty ( $where ) ? " WHERE" : " AND") . " tipo_mov_id = $tipo_mov_id";
			}
			if (! empty ( $inventariar )) {
				$where .= (empty ( $where ) ? " WHERE" : " AND") . " inventariar = '$inventariar'";
			}
	
			if (!empty ( $_REQUEST ['fecha'] )) {
				//SQLINJECTION
				$where .= (empty ( $where ) ? ' WHERE ' : ' AND ');
				if (isset ( $_REQUEST ['fecha_final'] ) && ! empty ( $_REQUEST ['fecha_final'] )) {
					$where .= "salidas.fecha BETWEEN '$_REQUEST[fecha]' AND '$_REQUEST[fecha_final]'";
				} else
					$where .= "salidas.fecha = '$_REQUEST[fecha]'";
			}
			$query = "SELECT IF(clientes.id, clientes.nomcomercial, almacenes.nombre) AS cliente,salidas.*, estatus.descripcion AS estatus FROM salidas INNER JOIN tipo_mov ON salidas.tipo_mov_id = tipo_mov.id INNER JOIN estatus ON salidas.estatus_id = estatus.id LEFT OUTER JOIN clientes ON salidas.cliente_id = clientes.id LEFT OUTER JOIN almacenes ON salidas.almacen_destino = almacenes.id$where";
			$resultado = $data->query ( $query );
			if ($resultado) {
				//                $dom = new DOMDocument('1.0', 'UTF-8');
				//                $root = $dom->createElement('root');
				//                $dom->appendChild($root);
				foreach ( $resultado as $fields ) {
					$r = $dom->createElement ( 'salida' );
					$root->appendChild ( $r );
					foreach ( $fields as $field => $value ) {
						$a = $dom->createAttribute ( $field );
						//                        $a = $dom->createElement($field);
						$t = $dom->createTextNode ( $value );
						$a->appendChild ( $t );
						$r->appendChild ( $a );
					
	//                        $r->setAttribute($field, $value);
					}
				}
			
	//                $xml = $dom->saveXML();
			}
			break;
			case 'agentes' :
			$where = '';
			if (! empty ( $_REQUEST['agente_id'] )) {
				$agente_id = intval( $_REQUEST['agente_id'] ); 
				$where .= (empty ( $where ) ? " WHERE" : " AND") . " id = $agente_id";
			}
			$query = "SELECT id, nombre, apellidoPaterno, apellidoMaterno FROM capturistas$where";
			$resultado = $data->query ( $query );
			if ($resultado) {
				//                $dom = new DOMDocument('1.0', 'UTF-8');
				//                $root = $dom->createElement('root');
				//                $dom->appendChild($root);
				foreach ( $resultado as $fields ) {
					$r = $dom->createElement ( 'agente' );
					$root->appendChild ( $r );
					foreach ( $fields as $field => $value ) {
						$a = $dom->createAttribute ( $field );
						//                        $a = $dom->createElement($field);
						$t = $dom->createTextNode ( $value );
						$a->appendChild ( $t );
						$r->appendChild ( $a );
					
	//                        $r->setAttribute($field, $value);
					}
				}
			
	//                $xml = $dom->saveXML();
			}
			break;
		case 'comision_porcentajes' :
			$where = '';
			if (! empty ( $_REQUEST['agente_id'] )) {
				$agente_id = intval( $_REQUEST['agente_id'] ); 
				$where .= (empty ( $where ) ? " WHERE" : " AND") . " agente_id = '$agente_id'";
			}
			if ($subaction){
				$subwhere = $where;
				if (! empty ( $_REQUEST['dias'] )) {
					$dias = intval($_REQUEST['dias']);
					$subwhere .= (empty ( $subwhere ) ? " WHERE" : " AND") . " dias = '$dias'";
				}
				$field_value = array();
				$sql_update = "";
				$sql_insert = "";
				foreach (array('agente_id', 'dias', 'porcent', 'porcent_parent') as $field) {
					if(isset($_REQUEST[$field])){
						$field_value[$field] = $_REQUEST[$field];
						$sql_update.=(empty ( $sql_update ) ?" ":", ") . "$field = '" . $field_value[$field] . "'";
					}
				}
				switch ($subaction) {
					case 'save':
					$num = $data->query("SELECT " . implode(", ", array_keys($field_value)) . " FROM comision_porcentajes$subwhere;");
					if(count($num)){
						$sql_update = "UPDATE comision_porcentajes SET$sql_update$subwhere;";
//						exit($sql_update);
						$data->query($sql_update);
					}else {
						$sql_insert = "INSERT INTO comision_porcentajes (" . implode(", ", array_keys($field_value)) . ") VALUES(" . implode(", ", array_values($field_value)) . ");";
//						echo $sql_insert;
						$data->query($sql_insert);
					}
					break;
					case 'delete':
						$data->query("DELETE FROM comision_porcentajes$subwhere;");						
						break;
				}
			}
			$query = "SELECT agente_id, dias, porcent, porcent_parent FROM comision_porcentajes$where ORDER BY agente_id, dias";
			$resultado = $data->query ( $query );
			if ($resultado) {
				//                $dom = new DOMDocument('1.0', 'UTF-8');
				//                $root = $dom->createElement('root');
				//                $dom->appendChild($root);
				foreach ( $resultado as $fields ) {
					$r = $dom->createElement ( 'comision_porcentaje' );
					$root->appendChild ( $r );
					foreach ( $fields as $field => $value ) {
						$a = $dom->createAttribute ( $field );
						//                        $a = $dom->createElement($field);
						$t = $dom->createTextNode ( $value );
						$a->appendChild ( $t );
						$r->appendChild ( $a );
					
	//                        $r->setAttribute($field, $value);
					}
				}
			
	//                $xml = $dom->saveXML();
			}
			break;
		case 'comision_salidas' :
			$where = '';
			if (! empty ( $almacen_id )) {
				$where .= (empty ( $where ) ? " WHERE" : " AND") . " almacen_id = $almacen_id";
			}
			if (! empty ( $tipo_mov_id )) {
				$where .= (empty ( $where ) ? " WHERE" : " AND") . " tipo_mov_id = $tipo_mov_id";
			}
			if (! empty ( $inventariar )) {
				$where .= (empty ( $where ) ? " WHERE" : " AND") . " inventariar = '$inventariar'";
			}
			// WHERE c.id is not null AND agente_id=1
			if ( isset( $_REQUEST['comision_is_null'] ) ) {
				$where .= (empty ( $where ) ? " WHERE" : " AND") . " c.id is" . ($_REQUEST['comision_is_null']? '': ' not') . " null";
			}
			if ( isset( $_REQUEST['agente_id'] ) ) {
				$agente_id = intval( $_REQUEST['agente_id'] );
				$where .= (empty ( $where ) ? " WHERE" : " AND") . " c.agente_id = $agente_id";
			}
			if (!empty ( $_REQUEST ['fecha'] )) {
				//SQLINJECTION
				$where .= (empty ( $where ) ? ' WHERE ' : ' AND ');
				if (isset ( $_REQUEST ['fecha_final'] ) && ! empty ( $_REQUEST ['fecha_final'] )) {
					$where .= "s.fecha BETWEEN '$_REQUEST[fecha]' AND '$_REQUEST[fecha_final]'";
				} else
					$where .= "s.fecha = '$_REQUEST[fecha]'";
			}
//			$query = "SELECT if(clientes.id, clientes.nomcomercial, almacenes.nombre) AS cliente,salidas.* FROM salidas INNER JOIN tipo_mov ON salidas.tipo_mov_id = tipo_mov.id LEFT OUTER JOIN clientes ON salidas.cliente_id = clientes.id LEFT OUTER JOIN almacenes ON salidas.almacen_destino = almacenes.id$where";
			$query = "SELECT s.`id`, s.`subtotal`, s.`fecha`, if(c.agente_id is null, 0, c.agente_id) as agente_id, if(c.agente_parent_id is null, 0, c.agente_parent_id) as agente_parent_id FROM salidas s INNER JOIN tipo_mov tm ON s.tipo_mov_id = tm.id LEFT OUTER JOIN comision c ON s.id = c.id$where";
			$resultado = $data->query ( $query );
			if ($resultado) {
				//                $dom = new DOMDocument('1.0', 'UTF-8');
				//                $root = $dom->createElement('root');
				//                $dom->appendChild($root);
				foreach ( $resultado as $fields ) {
					$r = $dom->createElement ( 'comision_salida' );
					$root->appendChild ( $r );
					foreach ( $fields as $field => $value ) {
						$a = $dom->createAttribute ( $field );
						//                        $a = $dom->createElement($field);
						$t = $dom->createTextNode ( $value );
						$a->appendChild ( $t );
						$r->appendChild ( $a );
					
	//                        $r->setAttribute($field, $value);
					}
				}
			
	//                $xml = $dom->saveXML();
			}
			break;
		case 'calcular_comision' :
			$where = '';
			if ( isset( $_REQUEST['agente_id'] ) ) {
				$agente_id = intval( $_REQUEST['agente_id'] );
				$where .= (empty ( $where ) ? " WHERE" : " AND") . " comision.agente_id = $agente_id";
			}
			if ( isset( $_REQUEST['agente_parent_id'] ) ) {
				$agente_id = intval( $_REQUEST['agente_parent_id'] );
				$where .= (empty ( $where ) ? " WHERE" : " AND") . " comision.agente_parent_id = $agente_parent_id";
			}
			if (!empty ( $_REQUEST ['fecha'] )) {
				//SQLINJECTION
				$where .= (empty ( $where ) ? ' WHERE ' : ' AND ');
				if (isset ( $_REQUEST ['fecha_final'] ) && ! empty ( $_REQUEST ['fecha_final'] )) {
					$where .= "comision.fecha BETWEEN '$_REQUEST[fecha]' AND '$_REQUEST[fecha_final]'";
				} else
					$where .= "comision.fecha = '$_REQUEST[fecha]'";
			}
			//	SELECT id,comision.agente_id,agente_parent_id,total,DATEDIFF(comision.fecha_final,comision.fecha) AS dia, dias,porcent * total AS comision,porcent * porcent_parent*total AS comision_parent,comision.fecha_final,comision.fecha FROM comision LEFT JOIN comision_porcentajes ON comision.agente_id = comision_porcentajes.agente_id AND DATEDIFF(comision.fecha_final,comision.fecha) <= dias GROUP BY id;
			//			$query = "SELECT if(clientes.id, clientes.nomcomercial, almacenes.nombre) AS cliente,salidas.* FROM salidas INNER JOIN tipo_mov ON salidas.tipo_mov_id = tipo_mov.id LEFT OUTER JOIN clientes ON salidas.cliente_id = clientes.id LEFT OUTER JOIN almacenes ON salidas.almacen_destino = almacenes.id$where";
			$query = "SELECT id,comision.agente_id,agente_parent_id,total,comision.fecha,comision.fecha_final,DATEDIFF(comision.fecha_final,comision.fecha) AS dia, dias,porcent * total AS comision,porcent * porcent_parent*total AS comision_parent FROM comision LEFT JOIN comision_porcentajes ON comision.agente_id = comision_porcentajes.agente_id AND DATEDIFF(comision.fecha_final,comision.fecha) <= dias$where GROUP BY id";
			$resultado = $data->query ( $query );
			if ($resultado) {
				//                $dom = new DOMDocument('1.0', 'UTF-8');
				//                $root = $dom->createElement('root');
				//                $dom->appendChild($root);
				foreach ( $resultado as $fields ) {
					$r = $dom->createElement ( 'comision' );
					$root->appendChild ( $r );
					foreach ( $fields as $field => $value ) {
						$a = $dom->createAttribute ( $field );
						//                        $a = $dom->createElement($field);
						$t = $dom->createTextNode ( $value );
						$a->appendChild ( $t );
						$r->appendChild ( $a );
					
	//                        $r->setAttribute($field, $value);
					}
				}
			
	//                $xml = $dom->saveXML();
			}
			break;
		case 'comision' :
			$where = '';
			if ( isset( $_REQUEST['agente_id'] ) ) {
				$agente_id = intval( $_REQUEST['agente_id'] );
				$where .= (empty ( $where ) ? " WHERE" : " AND") . " comision.agente_id = $agente_id";
			}
			if ( isset( $_REQUEST['agente_parent_id'] ) ) {
				$agente_id = intval( $_REQUEST['agente_parent_id'] );
				$where .= (empty ( $where ) ? " WHERE" : " AND") . " comision.agente_parent_id = $agente_parent_id";
			}
			if (!empty ( $_REQUEST ['fecha'] )) {
				//SQLINJECTION
				$where .= (empty ( $where ) ? ' WHERE ' : ' AND ');
				if (isset ( $_REQUEST ['fecha_final'] ) && ! empty ( $_REQUEST ['fecha_final'] )) {
					$where .= "comision.fecha BETWEEN '$_REQUEST[fecha]' AND '$_REQUEST[fecha_final]'";
				} else
					$where .= "comision.fecha = '$_REQUEST[fecha]'";
			}
			//	SELECT id,comision.agente_id,agente_parent_id,total,DATEDIFF(comision.fecha_final,comision.fecha) AS dia, dias,porcent * total AS comision,porcent * porcent_parent*total AS comision_parent,comision.fecha_final,comision.fecha FROM comision LEFT JOIN comision_porcentajes ON comision.agente_id = comision_porcentajes.agente_id AND DATEDIFF(comision.fecha_final,comision.fecha) <= dias GROUP BY id;
			//			$query = "SELECT if(clientes.id, clientes.nomcomercial, almacenes.nombre) AS cliente,salidas.* FROM salidas INNER JOIN tipo_mov ON salidas.tipo_mov_id = tipo_mov.id LEFT OUTER JOIN clientes ON salidas.cliente_id = clientes.id LEFT OUTER JOIN almacenes ON salidas.almacen_destino = almacenes.id$where";
			$query = "SELECT id,comision.agente_id,agente_parent_id,total,comision.fecha,comision.fecha_final,DATEDIFF(comision.fecha_final,comision.fecha) AS dia, if(dias is null,-1,dias) AS dias,if(porcent is null,0.0,porcent) AS porcent,if(porcent_parent is null,0.0,porcent_parent) AS porcent_parent,comision,comision_parent FROM comision LEFT JOIN comision_porcentajes ON comision.agente_id = comision_porcentajes.agente_id AND DATEDIFF(comision.fecha_final,comision.fecha) <= dias$where GROUP BY id";
			$resultado = $data->query ( $query );
			if ($resultado) {
				//                $dom = new DOMDocument('1.0', 'UTF-8');
				//                $root = $dom->createElement('root');
				//                $dom->appendChild($root);
				foreach ( $resultado as $fields ) {
					$r = $dom->createElement ( 'comision' );
					$root->appendChild ( $r );
					foreach ( $fields as $field => $value ) {
						$a = $dom->createAttribute ( $field );
						//                        $a = $dom->createElement($field);
						$t = $dom->createTextNode ( $value );
						$a->appendChild ( $t );
						$r->appendChild ( $a );
					
	//                        $r->setAttribute($field, $value);
					}
				}
			
	//                $xml = $dom->saveXML();
			}
			break;		
		default :
			break;
	}
	$xml = $dom->saveXML ();
	if ($xml) {
		echo ($xml);
	}
} else
	die ( "action $action no seleccionada" );
?>
