<?php

include_once "num2letras.php";

include_once "PDF_Rilez.php";
if(!is_file('../cfd/config.php')) die("Falta archivo de configuración");
include_once '../cfd/config.php';

class reporte {

//    public var conn;
    function __construct($server, $username, $password, $database_name) {
        $this->conn = @mysql_connect($server, $username, $password);
        if (!$this->conn) {
            die('no se pudo conectar al servidor de BD');
        }
        if (!@mysql_select_db($database_name, $this->conn)) {
            die('no se pudo seleccionar la BD');
        }
//        SET CHARACTER SET utf8;
//        $this->query("SET CHARACTER SET utf8;", FALSE);
//        $this->query("SET NAMES 'utf8';", FALSE);
        $this->decimals = 2;
    }

    /**
     *
     * @param type $sql
     * $sql->action = 'select'
     * $sql->fields = array('*');
     * $sql->table = 'table';
     * @return string 
     */
    public function sql($sql) {
        $query = "";
        switch (strtolower($sql->action)) {
            case 'select':
                if (isset($sql->fields)) {
                    $query.= "SELECT " . implode(', ', $sql->fields);
                }
                if (isset($sql->table)) {
                    $query.= "FROM " . $sql->table;
                }
                if (isset($sql->where)) {
                    $query.= "WHERE " . implode(" AND ", $sql->where);
                }
                if (isset($sql->group_by)) {
                    $query.= "GROUP BY " . implode(', ', $sql->group_by);
                }
                if (isset($sql->order_by)) {
                    $query.= "ORDER BY " . implode(', ', $sql->order_by);
                }
                break;
        }
        return $query;
    }

    /**
     *
     * @param type $sql
     * @param type $in_array
     * @return type 
     */
    public function query($query = "", $in_array = TRUE) {
        $result = @mysql_query($query, $this->conn);
        if ($result) {
            if (is_resource($result)) {
                if ($in_array) {
                    $rows = array();
                    $i = 0;
                    while ($row = mysql_fetch_assoc($result)) {
                        $rows[$i] = $row;
                        $i++;
                    }
                    @mysql_free_result($result);
                    return $rows;
                }else
                    return $result;
            }else
                return FALSE;
        }
    }

    /**
     *
     * @param string $dir path of directory
     * @param string $prefix prefix of file
     * @param string $ext xtencion of file
     * @param int $sec time in seconds of old file 
     */
    function CleanFiles($dir, $prefix='tmp', $ext = '.pdf', $sec = 3600) {
        //Borrar los ficheros temporales
        $t = time();
        $h = opendir($dir);
        while ($file = readdir($h)) {
            if (substr($file, 0, strlen($prefix)) == $prefix && substr($file, -1 * strlen($ext)) == $ext) {
                $path = $dir . '/' . $file;
                if ($t - filemtime($path) > $sec) {
                    @chmod($path, 0777);
                    @unlink($path);
                }
            }
        }
        closedir($h);
    }

    function Error($msg) {
        //Fatal error
        die('<b>Reporte error:</b> ' . $msg);
    }

    /**
     *
     * @param resource $pdf  FPDF
     * @param string $prefix
     * @param string $dest I|D|F|S
     * I: Internet
     * D: Download
     * F: File
     * S: String
     * @return string Nombre del archivo | PDF en string 
     */
    function Output($pdf, $prefix = '', $dest='') {
        //Determinar un nombre temporal de fichero en el directorio actual
        $name = $prefix . time() . '.pdf';
        $dest = strtoupper($dest);
        if ($dest == '') {
            if ($name == '') {
                $name = 'doc.pdf';
                $dest = 'I';
            }
            else
                $dest = 'F';
        }
        $buffer = $pdf->Output("", "S");
        header_remove('Content-type');
        switch ($dest) {
            case 'I':
                //Send to standard output
                if (ob_get_length())
                    $this->Error('Some data has already been output, can\'t send PDF file');
                if (php_sapi_name() != 'cli') {
                    //We send to a browser
                    header('Content-Type: application/pdf');
                    if (headers_sent())
                        $this->Error('Some data has already been output, can\'t send PDF file');
                    header('Content-Disposition: inline; filename="' . $name . '"');
                    header('Content-Length: ' . strlen($buffer));
                    header('Cache-Control: private, max-age=0, must-revalidate');
                    header('Pragma: public');
                    ini_set('zlib.output_compression', '0');
                }
                echo $buffer;
                break;
            case 'D':
                //Download file
                if (ob_get_length())
                    $this->Error('Some data has already been output, can\'t send PDF file');
                header('Content-Type: application/x-download');
                if (headers_sent())
                    $this->Error('Some data has already been output, can\'t send PDF file');
                header('Content-Disposition: attachment; filename="' . $name . '"');
                header('Content-Length: ' . strlen($buffer));
                header('Cache-Control: private, max-age=0, must-revalidate');
                header('Pragma: public');
                ini_set('zlib.output_compression', '0');
                echo $buffer;
                break;
            case 'F':
                $this->CleanFiles('.', $prefix, '.pdf');
                //Save to local file
                $f = fopen($name, 'wb');
                if (!$f)
                    $this->Error('Unable to create output file: ' . $name);
                fwrite($f, $buffer, strlen($buffer));
                fclose($f);
                chmod($name, 0777);
                break;
            case 'S':
                //Return as a string
                return $buffer;
            default:
                die('Incorrect output destination: ' . $dest);
        }
        return $name;
    }

    /**
     *
     * @param type $sucursal_id
     * @param type $almacen_id
     * @param type $with_matriz_id
     * @return type 
     */
    function Sucursal($sucursal_id=0, $almacen_id=0, $with_matriz_id=FALSE) {
        $where = "";
//        $sucursal_id = intval($_REQUEST['sucursal_id']);
//        $almacen_id = intval($_REQUEST['almacen_id']);
        if (!empty($almacen_id)) {
            $where = (empty($where) ? " WHERE" : " AND") . " al.id = $almacen_id";
        } else if (!empty($sucursal_id)) {
            $where = (empty($where) ? " WHERE" : " AND") . " s.id = $sucursal_id";
        } else {
            $where = " WHERE s.matriz_id = 0";
        }
        $query = "SELECT 
       s.id,
       s.nomcomercial AS sucursal, " .
                (empty($with_matriz_id) ? "" : "s.matriz_id,") .
                (empty($almacen_id) ? "" : "al.nombre AS almacen,") . "
       s.rfc,
       s.descripcion AS nombre,
       s.nomcomercial,
       calles.descripcion AS calle,
       s.numero_interior AS noInterior,
       s.numero_exterior AS noExterior,
       colonias.descripcion AS colonia,
       IF(s.poblacion_id = 1 OR poblaciones.descripcion is null,'',poblaciones.descripcion) AS poblacion,
       s.referencia,
       s.codigo_postal AS codigoPostal,
       municipios.descripcion AS municipio,
       estados.descripcion AS estado,
       paises.descripcion AS pais,
       s.telefono
  FROM    (   (   (   (   (   " . (empty($almacen_id) ? "" : "(   almacenes al
                                  INNER JOIN ") . "
                                      sucursales s
                                  " . (empty($almacen_id) ? "" : "ON ( al.sucursal_id = s.id))") . "
                           LEFT JOIN
                              poblaciones poblaciones
                           ON (s.poblacion_id = poblaciones.id))
                       INNER JOIN
                          municipios municipios
                       ON (s.municipio_id = municipios.id))
                   INNER JOIN
                      estados estados
                   ON (municipios.estado_id = estados.id)
                      AND (estados.id = municipios.estado_id))
               INNER JOIN
                  paises paises
               ON (estados.pais_id = paises.id)
                  AND (paises.id = estados.pais_id))
           INNER JOIN
              calles calles
           ON (s.calle_id = calles.id))
       INNER JOIN
          colonias colonias
       ON (s.colonia_id = colonias.id)$where";
//pre($query);exit;
        return $this->query($query);
    }

    function Proveedor($proveedor_id) {
        $where = '';
        if ($proveedor_id) {
            $where = (empty($where) ? " WHERE" : " AND") . " proveedores.id = $proveedor_id";
        }
        $query =
                "SELECT proveedores.id
     , proveedores.nomcomercial
     , proveedores.descripcion AS nombre
     , proveedores.rfc
     , calles.descripcion AS calle
     , proveedores.numero_interior AS noInterior
     , proveedores.numero_exterior AS noExterior
     , colonias.descripcion AS colonia
     , if(proveedores.poblacion_id = 1 OR poblaciones.descripcion IS NULL, '', poblaciones.descripcion) AS poblacion
     , proveedores.referencia
     , proveedores.codigo_postal AS codigoPostal
     , municipios.descripcion AS municipio
     , estados.descripcion AS estado
     , paises.descripcion AS pais
     , proveedores.telefono
FROM
  proveedores
LEFT OUTER JOIN poblaciones poblaciones
ON proveedores.poblacion_id = poblaciones.id
INNER JOIN municipios municipios
ON proveedores.municipio_id = municipios.id
INNER JOIN estados estados
ON municipios.estado_id = estados.id AND estados.id = municipios.estado_id
INNER JOIN paises paises
ON estados.pais_id = paises.id AND paises.id = estados.pais_id
LEFT OUTER JOIN calles calles
ON proveedores.calle_id = calles.id
LEFT OUTER JOIN colonias colonias
ON proveedores.colonia_id = colonias.id$where";

        return $this->query($query);
    }

    function Cliente($cliente_id) {
        $where = '';
        if ($cliente_id) {
            $where = (empty($where) ? " WHERE" : " AND") . " clientes.id = $cliente_id";
        }
        $query =
                "SELECT clientes.id
     ,  clientes.nomcomercial
     , clientes.descripcion AS nombre
     , clientes.rfc
     , calles.descripcion AS calle
     , clientes.numero_interior AS noInterior
     , clientes.numero_exterior AS noExterior
     , colonias.descripcion AS colonia
     , if(clientes.poblacion_id = 1 OR poblaciones.descripcion IS NULL, '', poblaciones.descripcion) AS poblacion
     , clientes.referencia
     , clientes.codigo_postal AS codigoPostal
     , municipios.descripcion AS municipio
     , estados.descripcion AS estado
     , paises.descripcion AS pais
     , clientes.telefono
FROM
  clientes
LEFT OUTER JOIN poblaciones poblaciones
ON clientes.poblacion_id = poblaciones.id
INNER JOIN municipios municipios
ON clientes.municipio_id = municipios.id
INNER JOIN estados estados
ON municipios.estado_id = estados.id AND estados.id = municipios.estado_id
INNER JOIN paises paises
ON estados.pais_id = paises.id AND paises.id = estados.pais_id
INNER JOIN calles calles
ON clientes.calle_id = calles.id
INNER JOIN colonias colonias
ON clientes.colonia_id = colonias.id$where";
//pre($query);exit;
        return $this->query($query);
    }

    /**
     *
     * @param type $cta_a array(cta[,scta[..,ncta]])
     * @param int $i 0 a n-1
     * @return type mixed arreglo con las cuentas precedentes
     */
    function cuentasPrecedentes($cta_a, $i = 0) {
        $a_cta = array();
//$i = 1;
        if ($i < 0)
            $i = 0;
        for ($iM = count($cta_a) - 1; $i <= $iM; $iM--) {
            for ($x = 0, $xl = strlen($cta_a[$iM]) - 1; $cta_a[$iM]; $x++) {//$x <= $xl
                $cta = $cta_a[$iM] . '';
                $d = $cta[$xl - $x];
                if ($d != '0') {
                    $s = $d * pow(10, $x);
//            echo '$s = ', $s, '$cta_a = ', $cta_a[$iM], ($s > 0 ? '+' . $cantidad : ''), "<br/>";
                    //     echo substr_replace($cta_a, '0', $xl-$x), "<br/>";
                    $a_cta[] = $cta_a;
                    $cta_a[$iM]-=$s;
                }
            }
        }
        if (0 <= $iM)
            $a_cta[] = $cta_a; //
        return $a_cta;
    }

    function sqlSelectCuentasCatalogo($a_cta, $fields = "descripcion") {
        $sql_a = array();
        foreach ($a_cta as $cta) {
            $sql = "SELECT $fields FROM catalogo WHERE ";
            for ($i = 0, $iM = count($cta); $i < $iM; $i++) {
                if ($i > 0)
                    $sql.= " AND ";
                $sql.= str_repeat('S', $i) . 'CTA' . " = '" . $cta[$i] . "'";
            }
            $sql_a[] = $sql;
        }
        return $sql_a;
    }

    function descripcionCuentas($sql_a, $pre = '(', $pos = ')') {
        $descripcion = "";
        foreach ($sql_a as $sql) {
//        echo $sql, "<br/>";
            $cuentas = $this->query($sql);
            foreach ($cuentas as $cuenta) {
                $descripcion = "$pre $cuenta[descripcion] $pos " . $descripcion;
            }
        }
        return $descripcion;
    }

    /**
     *
     * @param type $almacen_id
     * @param type $linea_id
     * @param type $sublinea_id
     * @param type $with_image
     * @param type $dest
     * @return type 
     */
    function pdfCatalogoAlmacen($almacen_id = 0, $linea_id = 0, $sublinea_id = 0, $vendible = 'S', $with_image = FALSE, $dest = '') {
        if (empty($almacen_id)) {
            echo "No seleccionó almacén";
            return;
        }
        $sucursal = $this->Sucursal(0, $almacen_id);
        if (empty($sucursal)) {
            echo "No existe almacén";
            return;
        }
        $sucursal = $sucursal[0];
//        pre($sucursal);exit;
        $title = "CAT�?LOGO";
        $subject = "ARTICULOS";
        $title .= " ALMACÉN: $almacen_id";

        $where_articulos = "";
        if (!empty($almacen_id)) {
            $where_articulos .= ( empty($where_articulos) ? " WHERE" : " AND") . " al.id = $almacen_id";
        } else if (!empty($sucursal_id))
            $where_articulos .= ( empty($where_articulos) ? " WHERE" : " AND") . " s.id = $sucursal_id";
//        $linea_id = intval($_REQUEST['linea_id']);
        //        $sublinea_id = intval($_REQUEST['sublinea_id']);
        if (!empty($sublinea_id)) {
            $subject .= "\nSUBLINEA: $sublinea_id";
            $where_articulos .= ( empty($where_articulos) ? " WHERE " : " AND") . " sl.id = $sublinea_id";
        } else if (!empty($linea_id)) {
            $subject .= "\nLINEA: $linea_id";
            $where_articulos .= ( empty($where_articulos) ? " WHERE" : " AND") . " l.id = $linea_id";
        }

        if (isset($vendible))
            $where_articulos .= ( empty($where_articulos) ? " WHERE" : " AND") . " ar.vendible = '$vendible'";
//        CONCAT('LINEA ', l.id, ': ', l.descripcion, '\nSUBLINEA ', sl.id , ': ',
//       sl.descripcion, '\nDESCRIPCION: ',

        $sql_articulos_almacen_img = "SELECT
        ar.id2 AS imagen,
       ar.id2 AS codigo,
       l.descripcion AS linea,sl.descripcion AS sublinea,
       (ar.descripcion) AS descripcion,
       ar_al.posicion,
       ar_al.precio,
       (ar_al.iva * 100 ) AS iva,
       m.abreviatura AS Moneda
  FROM    (   (   (   (   (   (   sublineas sl
                               INNER JOIN
                                  lineas l
                               ON (sl.linea_id = l.id))
                           INNER JOIN
                              articulos ar
                           ON (ar.sublinea_id = sl.id))
                       INNER JOIN
                          articulos_almacenes ar_al
                       ON (ar_al.articulo_id = ar.id))
                   INNER JOIN
                      almacenes al
                   ON (ar_al.almacen_id = al.id))
               INNER JOIN
                  sucursales s
               ON (al.sucursal_id = s.id))
           INNER JOIN
              monedas m
           ON (ar_al.moneda_id = m.id))
       INNER JOIN
          unidades u
       ON (ar.unidad_id = u.id)$where_articulos
       ORDER BY s.id,al.id,l.id,sl.id,ar.orden";
//        pre($sql_articulos_almacen_img);exit;
        $pdf = new PDF_Rilez();
        $pdf->SetProtection(array('print'));
        $rows = $this->query($sql_articulos_almacen_img);
//        $pdf->configureRow($rows);
//        $pdf->configureRow($rows,TRUE);
        if (!empty($rows)) {
            $pdf->colTitle =
                    array(
                        'codigo' => utf8_decode('CODIGO'), /* codigo */
                        'descripcion' => 'DESCRIPCION', /* descripcion */
                        'posicion' => 'POSICION', /* posicion */
                        'precio' => 'PRECIO', /* precio */
                        'iva' => 'IVA', /* iva */
                        'Moneda' => 'MONEDA', /* Moneda */
                        'imagen' => 'IMAGEN' /* imagen */
            );


            $pdf->colWidth =
                    array(
                        'imagen' => 35, /* imagen */
                        'codigo' => 22, /* codigo */
                        'descripcion' => 80, /* descripcion */
//                        'posicion' => 20, /* posicion */
                        'precio' => 16, /* precio */
//                        'iva' => 15, /* iva */
                        'Moneda' => 18 /* Moneda */
            );
            if (!$with_image) {
                unset($pdf->colWidth['imagen']);
            }
            $compensation['descripcion'] = 1;
//            $compensation['imagen'] = 1;
            $pdf->colAlign =
                    array(
                        'codigo' => 'L', /* codigo */
                        'descripcion' => 'L', /* descripcion */
                        'posicion' => 'R', /* posicion */
                        'precio' => 'R', /* precio */
                        'iva' => 'R', /* iva */
                        'Moneda' => 'L' /* Moneda */
            );
            $pdf->SetFont('Arial', '', 8);
            $pdf->AliasNbPages();
            $fullw = ($pdf->w - $pdf->rMargin - $pdf->lMargin);
            $w5 = $fullw / 5;
            /*
             * 
             */
//            if (!empty($compensation)) {
//                $maxw = 0;
//                foreach ($pdf->colWidth as $field => $w) {
//                    $maxw = $maxw + $w;
//                }
//                $compensationw = $fullw - $maxw;
//                foreach ($compensation as $field => $part) {
//                    if (isset($pdf->colWidth[$field]))
//                        $pdf->colWidth[$field] += ( $compensationw * $part);
//                }
//            }
            $pdf->SetCompensation($pdf->colWidth, $compensation);
            $pdf->headerMethod[]['SetFont'] = array('Arial', 'B');
            $pdf->headerMethod[]['Image'] = array('logo_rilez.gif', $pdf->lMargin + $w5, 1, $w5 * 3);
            $pdf->headerMethod[]['SetY'] = array(20);
            $pdf->headerMethod[]['Cell'] = array(0, 5, utf8_decode($sucursal['almacen'] . ' CAT�?LOGO ' . date("Y")), '0', 1, 'C');
            $pdf->headerMethod[]['SetFont'] = array('Arial', 'B', 10);
            $pdf->headerMethod[]['Row'] = array($pdf->colTitle, 5, '1', 1);
//            $pdf->headerMethod[]['Cell'] = array(0, 5, 'IMAGEN', 'TRB', 1, 'C');
            $pdf->headerMethod[]['SetFont'] = array('Arial', 'I', 8);

            $pdf->footerMethod[]['SetY'] = array(-10);
            $pdf->footerMethod[]['Cell'] = array($fullw, 5, utf8_decode('*Precios más IVA **Precios sujetos a cambio sin previo aviso'), '', 1, 'C');
//            $pdf->footerMethod[]['Cell'] = array($w5, 5, '', '', 0, 'C');
            $pdf->footerMethod[]['SetY'] = array(-5);
            $pdf->footerMethod[]['Page'] = array($fullw, 5);
//            $pdf->footerMethod[]['Cell'] = array($w5 * 2, 5, 'Nombre y Firma Supervisor', 'T', 0, 'C');
//            $pdf->footerMethod[]['Ln'] = array(0);
//            $pdf->footerMethod[]['Page'] = array(0, 10);
            $linea = '';
            $sublinea = '';
            $pdf->AddPage();
            foreach ($rows as $i => $row) {
//                $pdf->SetFont('Arial', 'I');
                if ($linea != $row['linea']) {
                    $linea = $row['linea'];
//                    $pdf->AddPage();
                    //imprimir
//                    $h = $pdf->ImageRow($row, 8, array(), 35, 'LBR',1,array('linea'=>0),array('linea'=>'C'));
                }
                if ($sublinea != $row['sublinea']) {
                    $sublinea = $row['sublinea'];
//                    $pdf->AddPage();
                    //imprimir
//                    $h = $pdf->ImageRow($row, 6, array(), 35, 'LBR',1,array('sublinea'=>0),array('sublinea'=>'C'));
                    $pdf->CheckPageBreak(($with_image ? 40 : 10));
                    $pdf->FancyRow(array('linea' => $linea, 'sublinea' => $sublinea), array('linea' => $fullw / 2, 'sublinea' => 0), 4, array('linea' => 'BL', 'sublinea' => 'RB'), array('linea' => 'R', 'sublinea' => 'L'), array('linea' => 'B', 'sublinea' => 'B'), array('linea' => '%s :', 'sublinea' => ' %s')); //
                }
                $row['precio'] = numberFormat($row['precio']);
//                if($row['Moneda'] == 'MXN') $row['Moneda'] = 'M.N.';
                $img = array('imagen' => array('sprintf' => "../archivos/jpg/%s.JPG", 'default' => ''));
                $h = $pdf->ImageRow($row, 4, $img, 35, 'LBR');
            }
        }
//        $pdf->Output('Almacen.pdf','D');
        return $this->Output($pdf, 'Catalogo_Almacen', $dest);
    }

    /**
     *
     * @param type $sucursal_id
     * @param type $almacen_id
     * @param type $linea_id
     * @param type $sublinea_id
     * @param type $stock_minimo
     * @param type $stock_maximo
     * @param type $with_linea
     * @param type $with_sublinea
     * @param type $with_existencia
     * @param type $with_image
     * @param type $dest
     * @return type 
     */
    function pdfInventario($sucursal_id=0, $almacen_id = 0, $linea_id = 0, $sublinea_id = 0, $stock_minimo = 0, $stock_maximo = 0, $with_linea = 0, $with_sublinea = 0, $with_existencia = FALSE, $vendible = 'S', $with_image = FALSE, $dest = '') {
//        if(empty ($almacen_id)){
//            echo "No seleccionó almacén";
//            return;
//        }
        $sucursal = $this->Sucursal($sucursal_id, $almacen_id);
        if (empty($sucursal)) {
            echo "No existe";
            return;
        }
        $sucursal = $sucursal[0];
//        pre($sucursal);exit;
        $title = "CAT�?LOGO";
        $subject = "ARTICULOS";
        $title .= " ALMACÉN: $almacen_id";

        $where_articulos = "";
        if (!empty($almacen_id)) {
            $where_articulos .= ( empty($where_articulos) ? " WHERE" : " AND") . " al.id = $almacen_id";
        } else if (!empty($sucursal_id))
            $where_articulos .= ( empty($where_articulos) ? " WHERE" : " AND") . " s.id = $sucursal_id";
//        $linea_id = intval($_REQUEST['linea_id']);
        //        $sublinea_id = intval($_REQUEST['sublinea_id']);
        if (!empty($sublinea_id)) {
            $subject .= "\nSUBLINEA: $sublinea_id";
            $where_articulos .= ( empty($where_articulos) ? " WHERE " : " AND") . " sl.id = $sublinea_id";
        } else if (!empty($linea_id)) {
            $subject .= "\nLINEA: $linea_id";
            $where_articulos .= ( empty($where_articulos) ? " WHERE" : " AND") . " l.id = $linea_id";
        }

        if (($stock_minimo)) {
            $subject .= "\nLINEA: $linea_id";
            $where_articulos .= ( empty($where_articulos) ? " WHERE" : " AND") . " (ar_al.entradas - ar_al.salidas) < ar_al.stock_minimo";
        } else if (($stock_maximo)) {
            $subject .= "\nLINEA: $linea_id";
            $where_articulos .= ( empty($where_articulos) ? " WHERE" : " AND") . " ar_al.stock_maximo < (ar_al.entradas - ar_al.salidas)";
        }

        if (isset($vendible))
            $where_articulos .= ( empty($where_articulos) ? " WHERE" : " AND") . " ar.vendible = '$vendible'";

        $sql_articulos_almacen_img = "SELECT " .
                ($with_image ? "ar.id2 AS imagen," : "") . "
       ar.id2 AS codigo,
       CONCAT(" .
                ($almacen_id ? "" : "'ALMACEN ', al.id, ': ', al.nombre, '\n',") .
                ($with_linea ? "'LINEA ', l.id, ': ', l.descripcion, '\n'," : "") .
                ($with_sublinea ? "'SUBLINEA ', sl.id, ': ', sl.descripcion, '\n'," : "") .
                (($with_linea || $with_sublinea) ? "'DESCRIPCION: '," : "") . " ar.descripcion
       ) AS descripcion,
       ar_al.posicion" .
                ($with_existencia ? ",
       (ar_al.entradas - ar_al.salidas) AS existencia" : "") . "
--       ar_al.precio,
--       (ar_al.iva * 100 ) AS iva,
--       m.descripcion AS Moneda
  FROM    (   (   (   (   (   (   sublineas sl
                               INNER JOIN
                                  lineas l
                               ON (sl.linea_id = l.id))
                           INNER JOIN
                              articulos ar
                           ON (ar.sublinea_id = sl.id))
                       INNER JOIN
                          articulos_almacenes ar_al
                       ON (ar_al.articulo_id = ar.id))
                   INNER JOIN
                      almacenes al
                   ON (ar_al.almacen_id = al.id))
               INNER JOIN
                  sucursales s
               ON (al.sucursal_id = s.id))
           INNER JOIN
              monedas m
           ON (ar_al.moneda_id = m.id))
       INNER JOIN
          unidades u
       ON (ar.unidad_id = u.id)$where_articulos
       ORDER BY s.id,al.id,l.id,sl.id,ar.orden";
//        pre($sql_articulos_almacen_img);exit;
        $pdf = new PDF_Rilez();
        $pdf->SetProtection(array('print'));
        $rows = $this->query($sql_articulos_almacen_img);
//        $pdf->configureRow($rows);
//        $pdf->configureRow($rows,TRUE);
        if (!empty($rows)) {
            $pdf->colTitle =
                    array(
                        'imagen' => 'IMAGEN', /* imagen */
                        'codigo' => 'CODIGO', /* codigo */
                        'descripcion' => 'DESCRIPCION', /* descripcion */
                        'posicion' => 'POSICION', /* posicion */
                        'existencia' => 'EXISTENCIA', /* existencia */
                        'precio' => 'PRECIO', /* precio */
                        'iva' => 'IVA', /* iva */
                        'Moneda' => 'MONEDA' /* Moneda */
            );


            $pdf->colWidth =
                    array(
                        'imagen' => 35, /* imagen */
                        'codigo' => 22, /* codigo */
                        'descripcion' => 80, /* descripcion */
                        'posicion' => 20, /* posicion */
                        'existencia' => 24 /* existencia */
//                        'precio' => 16, /* precio */
//                        'iva' => 15, /* iva */
//                        'Moneda' => 17 /* Moneda */
            );
            if (!$with_image) {
                unset($pdf->colWidth['imagen']);
            }
            $compensation['descripcion'] = 1;
//            $compensation['imagen'] = 1;
            $pdf->colAlign =
                    array(
                        'codigo' => 'L', /* codigo */
                        'descripcion' => 'L', /* descripcion */
                        'posicion' => 'R', /* posicion */
                        'existencia' => 'R', /* posicion */
                        'precio' => 'R', /* precio */
                        'iva' => 'R', /* iva */
                        'Moneda' => 'L' /* Moneda */
            );
            $pdf->SetFont('Arial', '', 8);
            $pdf->AliasNbPages();
            $fullw = ($pdf->w - $pdf->rMargin - $pdf->lMargin);
            $w5 = $fullw / 5;
            /*
             * 
             */
            if (!empty($compensation)) {
                $maxw = 0;
                foreach ($pdf->colWidth as $field => $w) {
                    $maxw = $maxw + $w;
                }
                $compensationw = $fullw - $maxw;
                foreach ($compensation as $field => $part) {
                    if (isset($pdf->colWidth[$field]))
                        $pdf->colWidth[$field] += ( $compensationw * $part);
                }
            }
            $pdf->headerMethod[]['SetFont'] = array('Arial', 'B');
            $pdf->headerMethod[]['Image'] = array('logo_rilez.gif', $pdf->lMargin + $w5, 1, $w5 * 3);
            $pdf->headerMethod[]['SetY'] = array(20);
            $pdf->headerMethod[]['Cell'] = array(0, 5, utf8_decode($sucursal['sucursal'] . " " . $sucursal['almacen'] . ' INVENTARIO ' . date("d/m/Y")), '0', 1, 'C');
            $pdf->headerMethod[]['SetFont'] = array('Arial', 'B', 10);
            $pdf->headerMethod[]['Row'] = array($pdf->colTitle, 5, '1', 1);
//            $pdf->headerMethod[]['Cell'] = array(0, 5, 'IMAGEN', 'TRB', 1, 'C');
            $pdf->headerMethod[]['SetFont'] = array('Arial', 'I', 8);

            $pdf->footerMethod[]['SetY'] = array(-10);
            $pdf->footerMethod[]['Cell'] = array($w5 * 2, 5, 'Nombre y Firma Encargado', 'T', 0, 'C');
//            $pdf->footerMethod[]['Cell'] = array($w5, 5, '', '', 0, 'C');
            $pdf->footerMethod[]['Page'] = array($w5, 10);
            $pdf->footerMethod[]['Cell'] = array($w5 * 2, 5, 'Nombre y Firma Supervisor', 'T', 0, 'C');
//            $pdf->footerMethod[]['Ln'] = array(0);
//            $pdf->footerMethod[]['Page'] = array(0, 10);
            $pdf->AddPage();
            foreach ($rows as $i => $row) {
//                if($sucursal_id)
//                    $row['descripcion'] = "ALMACEN: " . $sucursal['almacen'] . "\n" .  $row['descripcion'];
                $img = array('imagen' => array('sprintf' => "../archivos/jpg/%s.JPG", 'default' => ''));
                $h = $pdf->ImageRow($row, 4, $img, 35, 'LBR');
            }
        }
//        $pdf->Output('Almacen.pdf','D');
        return $this->Output($pdf, 'Inventario_Almacen', $dest);
    }

    function pdfSalida($salida_id =0, $with_posicion = FALSE, $with_tipo = FALSE, $with_almacen = FALSE, $detailed_totals = TRUE, $discount_included = FALSE, $dest = '') {
        if (empty($salida_id)) {
//            if(!headers_sent())
//            header_remove ('Content-type');    
            echo "No seleccionó salida";
            return;
        }
        $title = "";
        $subject = "";

        $where_salidas = "";
        $where_detalles_salidas = "";
        if (!empty($salida_id)) {
            $where_salidas .= ( empty($where_salidas) ? " WHERE" : " AND") . " salidas.id = $salida_id";
            $where_detalles_salidas .= ( empty($where_detalles_salidas) ? " WHERE" : " AND") . " ds.salida_id = $salida_id";
        }
//        if (!empty($almacen_id)) {
//            $where_salidas .= ( empty($where_salidas) ? " WHERE" : " AND") . " al.id = $almacen_id";
//        } else if (!empty($sucursal_id))
//            $where_salidas .= ( empty($where_salidas) ? " WHERE" : " AND") . " s.id = $sucursal_id";

        $sql_salidas =
                "SELECT salidas.id
     , salidas.almacen_id
     , almacenes.nombre AS almacen
     , salidas.tipo_mov_id
     , tipo_mov.descripcion AS tipo
     , tipo_mov.tipoDeComprobante
     , tipo_mov.formaDePago
     , salidas.forma_de_pago AS metodoDePago
     , salidas.moneda_id
     , monedas.descripcion AS Moneda
     , monedas.abreviatura AS Moneda_Simbolo
     , salidas.valor_de_moneda AS TipoCambio
     , salidas.almacen_destino AS almacen_id_destino
     , almacenes_destino.nombre AS almacen_destino
     , salidas.cliente_id
     , clientes.rfc
     , clientes.descripcion AS nombre
     , clientes.nomcomercial
     , salidas.fecha
     , salidas.referencia1
     , salidas.referencia2
     , salidas.vencimiento
     , salidas.suma
     , salidas.descuento
     , salidas.subtotal
     , salidas.iva
     , salidas.total
     , salidas.abonos
     , salidas.devolucion
FROM
  salidas
INNER JOIN tipo_mov
ON salidas.tipo_mov_id = tipo_mov.id
INNER JOIN monedas
ON salidas.moneda_id = monedas.id
LEFT OUTER JOIN clientes
ON salidas.cliente_id = clientes.id
INNER JOIN almacenes
ON salidas.almacen_id = almacenes.id
LEFT OUTER JOIN almacenes almacenes_destino
ON salidas.almacen_destino = almacenes_destino.id$where_salidas";
        $sql_detalles_salidas = "SELECT" .
                (empty($salida_id) ?
                        ($with_almacen ? "
       al.nombre AS almacen," : "") .
                        ($with_tipo ? "
       tm.descripcion AS tipo," : "") . "
       s.id,
       " :
                        ' ds.mov,
       ds.articulo_id,' .
                        ($with_posicion ? "
       ar_al.posicion," : "") . "
       ar.id2,
       ds.cantidad,
       unidades.descripcion AS unidad,
       ar.descripcion,
--       ds.costo,
       " . ($discount_included ? "(ds.precio * (1 - ds.descuento))" : "ds.precio") . " AS precio,
--       (ds.descuento * 100) AS descto100,
       (ds.iva * 100) AS iva100," . "") .
                (empty($salida_id) ? "SUM" : "") . "(ds.cantidad * " . ($discount_included ? "(ds.precio * (1 - ds.descuento))" : "ds.precio") . ") AS importe," .
                (empty($salida_id) ? "SUM" : "") . "(ds.cantidad * " . ($discount_included ? "0" : "(ds.precio * (ds.descuento))") . ") AS descto," .
                (empty($salida_id) ? "SUM" : "") . "(ds.cantidad * (ds.precio * (1 - ds.descuento))) AS subtotal," .
                (empty($salida_id) ? "SUM" : "") . "(ds.cantidad * (ds.precio * (1 - ds.descuento) * ds.iva)) AS iva," .
                (empty($salida_id) ? "SUM" : "") . "(ds.cantidad * (ds.precio * (1 - ds.descuento) * (1 + ds.iva))) AS total,
       ds.moneda_id AS moneda_id,
       ds.valor_moneda AS TipoCambio,
       monedas.descripcion AS Moneda
  FROM    (   (   (   (   (   (   articulos_almacenes ar_al
                               INNER JOIN
                                  articulos ar
                               ON (ar_al.articulo_id = ar.id))
                           INNER JOIN
                              salidas s
                           ON (ar_al.almacen_id = s.almacen_id))
                       INNER JOIN
                          almacenes al
                       ON (ar_al.almacen_id = al.id))
                   INNER JOIN
                      detalles_salidas ds
                   ON (ar_al.articulo_id = ds.articulo_id)
                      AND (s.id = ds.salida_id))
               INNER JOIN
                  monedas monedas
               ON (ar_al.moneda_id = monedas.id))
           INNER JOIN
              tipo_mov tm
           ON (s.tipo_mov_id = tm.id))
       INNER JOIN
          unidades unidades
       ON (ar.unidad_id = unidades.id)$where_detalles_salidas
GROUP BY s.id," .
                (empty($salida_id) ? "" : "
    ds.mov,") . "
        s.almacen_id," .
                (($with_tipo) ? "
    tm.id," : "") . "
         ar_al.moneda_id,
         ds.iva";
//        pre($sql_salidas);
        $salida = $this->query($sql_salidas);
        $salida = $salida[0];
        if (empty($salida)) {
            exit("Salida $salida_id No disponible");
        }
        $title = $salida['almacen'] . ' Salida ' . $salida['id'] . ' ' . $salida['tipo'] . ' ' . date_format(date_create($salida['fecha']), 'd/m/Y');
//        pre($salida);
        $sucursal_matriz = $this->Sucursal($sucursal_id, $salida['almacen_id'], TRUE);
        $sucursal_matriz = $sucursal_matriz[0];
        if (($sucursal_matriz['matriz_id'])) {
            $sucursal = $sucursal_matriz;
            $sucursal_matriz = $this->Sucursal($sucursal['matriz_id']);
            $sucursal_matriz = $sucursal_matriz[0];
        }
//        pre($sucursal_matriz);
//        pre($sucursal);
        if ($salida['cliente_id']) {
            $receptor = $this->Cliente($salida['cliente_id']);
            $receptor = $receptor[0];
        }
        if ($salida['almacen_id_destino']) {
            $receptor = $this->Sucursal(0, $salida['almacen_id_destino']);
            $receptor = $receptor[0];
            $subject .= "Origen: " . (isset($sucursal) ? $sucursal['almacen'] : $sucursal_matriz['almacen']) . " Destino: " . $receptor['almacen'];
        }
//        pre($receptor);
//        if (empty($sucursal)) {
//            echo "No existe";
//            return;
//        }
//        $sucursal = $sucursal[0];
//        pre($sql_detalles_salidas);
        $rows = $this->query($sql_detalles_salidas);
//        pre($rows);
//        exit;
        $pdf = new PDF_Rilez();
        $pdf->SetProtection(array('print'));
        $pdf->SetFont('Arial', '', 6);
        $pdf->AliasNbPages();
//        $pdf->configureRow($rows);
//        $pdf->configureRow($rows, TRUE);
        $fullw = ($pdf->w - $pdf->rMargin - $pdf->lMargin);
        $w5 = $fullw / 5;
        $w10 = $fullw / 10;
        $w20 = $fullw / 20;
        $pdf->headerMethod[]['SetFont'] = array('Arial', 'B');
        $pdf->headerMethod[]['Image'] = array('logo_rilez.gif', $pdf->lMargin + ($fullw * 0.3), 1, ($fullw * 0.4));
        $pdf->headerMethod[]['SetY'] = array(($fullw * 0.07));
        $pdf->headerMethod[]['Cell'] = array(0, 3, utf8_decode($title), '0', 1, 'C');
        $pdf->headerMethod[]['Cell'] = array(0, 3, utf8_decode($subject), '0', 1, 'C');

        $pdf->headerMethod[]['SetXY'] = array($pdf->lMargin + (0), ($fullw * 0.10));
        if ($sucursal_matriz) {
            $pdf->headerMethod[]['MultiCell'] = array(($fullw * .33), 4, "Emisor:\n" .
                (isset($sucursal_matriz['nomcomercial']) ? utf8_decode($sucursal_matriz['nomcomercial']) . "\n" : "") .
                (isset($sucursal_matriz['nombre']) ? utf8_decode($sucursal_matriz['nombre']) . "\n" : "") .
                (isset($sucursal_matriz['rfc']) ? 'RFC: ' . $sucursal_matriz['rfc'] . "\n" : "") .
                (isset($sucursal_matriz['calle']) ? utf8_decode($sucursal_matriz['calle']) : "") .
                (isset($sucursal_matriz['noExterior']) ? " No. " . $sucursal_matriz['noExterior'] .
                        (!empty($sucursal_matriz['noInterior']) ?
                                " - " . $sucursal_matriz['noInterior'] : '') . "\n" : "") .
                (isset($sucursal_matriz['colonia']) ? utf8_decode($sucursal_matriz['colonia']) . "\n" : "") .
                (isset($sucursal_matriz['municipio']) ? utf8_decode($sucursal_matriz['municipio']) . "\n" : "") .
                (isset($sucursal_matriz['codigoPostal']) ? "C.P. " . $sucursal_matriz['codigoPostal'] . "\n" : "") .
                (isset($sucursal_matriz['estado']) ? utf8_decode($sucursal_matriz['estado'] . "\n") : "") .
                (isset($sucursal_matriz['pais']) ? utf8_decode($sucursal_matriz['pais']) : ""), 0, "L");
        }
        $pdf->headerMethod[]['SetXY'] = array($pdf->lMargin + ($fullw * .33), ($fullw * 0.10));
        if ($sucursal) {
            $pdf->headerMethod[]['MultiCell'] = array(($fullw * .33), 4, "Expedida en:\n" .
                (isset($sucursal['nomcomercial']) ? utf8_decode($sucursal['nomcomercial']) . "\n" : "") .
                (isset($sucursal['nombre']) ? utf8_decode($sucursal['nombre']) . "\n" : "") .
                (isset($sucursal['rfc']) ? 'RFC: ' . $sucursal['rfc'] . "\n" : "") .
                (isset($sucursal['calle']) ? utf8_decode($sucursal['calle']) : "") .
                (isset($sucursal['noExterior']) ? " No. " . $sucursal['noExterior'] .
                        (!empty($sucursal['noInterior']) ?
                                " - " . $sucursal['noInterior'] : '') . "\n" : "") .
                (isset($sucursal['colonia']) ? utf8_decode($sucursal['colonia']) . "\n" : "") .
                (isset($sucursal['municipio']) ? utf8_decode($sucursal['municipio']) . "\n" : "") .
                (isset($sucursal['codigoPostal']) ? "C.P. " . $sucursal['codigoPostal'] . "\n" : "") .
                (isset($sucursal['estado']) ? utf8_decode($sucursal['estado'] . "\n") : "") .
                (isset($sucursal['pais']) ? utf8_decode($sucursal['pais']) : ""), 0, "C");
        }
        $pdf->headerMethod[]['SetXY'] = array($pdf->lMargin + ($fullw * .66), ($fullw * 0.10));
        if ($receptor) {
            $pdf->headerMethod[]['MultiCell'] = array(($fullw * .33), 4, "Receptor:\n" .
                (isset($receptor['nomcomercial']) ? utf8_decode($receptor['nomcomercial']) . "\n" : "") .
                (isset($receptor['nombre']) ? utf8_decode($receptor['nombre']) . "\n" : "") .
                (isset($receptor['rfc']) ? 'RFC: ' . $receptor['rfc'] . "\n" : "") .
                (isset($receptor['calle']) ? utf8_decode($receptor['calle']) : "") .
                (isset($receptor['noExterior']) ? " No. " . $receptor['noExterior'] .
                        (!empty($receptor['noInterior']) ?
                                " - " . $receptor['noInterior'] : '') . "\n" : "") .
                (isset($receptor['colonia']) ? utf8_decode($receptor['colonia']) . "\n" : "") .
                (isset($receptor['municipio']) ? utf8_decode($receptor['municipio']) . "\n" : "") .
                (isset($receptor['codigoPostal']) ? "C.P. " . $receptor['codigoPostal'] . "\n" : "") .
                (isset($receptor['estado']) ? utf8_decode($receptor['estado'] . "\n") : "") .
                (isset($receptor['pais']) ? utf8_decode($receptor['pais']) : ""), 0, "R");
        }

        $pdf->footerMethod[]['SetY'] = array(-15);
        $pdf->footerMethod[]['Cell'] = array(($fullw * .4), 5, 'Nombre y Firma Encargado', 'T', 0, 'C');
//            $pdf->footerMethod[]['Cell'] = array($w5, 5, '', '', 0, 'C');
        $pdf->footerMethod[]['Page'] = array(($fullw * .2), 10);
        $pdf->footerMethod[]['Cell'] = array(($fullw * .4), 5, 'Nombre y Firma Supervisor', 'T', 0, 'C');
//            $pdf->footerMethod[]['Ln'] = array(0);
//            $pdf->footerMethod[]['Page'] = array(0, 10);
        if (!empty($rows)) {
            $pdf->colTitle =
                    array(
                        'almacen' => 'ALMACEN', /* almacen */
                        'tipo' => 'TIPO', /* tipo */
                        'id' => 'ID', /* id */
                        'posicion' => 'POSICION', /* posicion */
                        'id2' => 'ID2', /* id2 */
                        'cantidad' => 'CANTIDAD', /* cantidad */
                        'unidad' => 'UNIDAD', /* unidad */
                        'descripcion' => 'DESCRIPCION', /* descripcion */
                        'costo' => 'COSTO', /* costo */
                        'precio' => 'PRECIO', /* precio */
                        'iva100' => 'IVA100', /* iva */
                        'importe' => 'IMPORTE', /* importe */
                        'descto' => 'DESCTO', /* descto */
                        'subtotal' => 'SUBTOTAL', /* subtotal */
                        'iva' => 'IVA', /* iva */
                        'total' => 'TOTAL', /* total */
                        'Moneda' => 'MONEDA' /* Moneda */
            );


            $pdf->colWidth =
                    array(
                        'almacen' => 14, /* 14.001 almacen */
                        'tipo' => 14, /* 14.703733333333 tipo */
                        'id' => 14, /* 14.703733333333 id */
                        'posicion' => 13, /* 13.763933333333 posicion */
                        'id2' => 16, /* 14.703733333333 id2 */
                        'cantidad' => 14, /* 14.001 cantidad */
                        'unidad' => 11, /* 13.528983333333 unidad */
                        'descripcion' => 37, /* 37.179758333333 descripcion */
//                        'costo' => 11, /*11.2959 costo */
                        'precio' => 13, /* 11.763683333333 precio */
                        'iva100' => 11,
                        'importe' => 16, /* 13.649633333333 importe */
                        'descto' => 15, /* 13.179758333333 descto */
                        'subtotal' => 16, /* 13.649633333333 subtotal */
                        'iva' => 15, /* 13.649633333333 iva */
                        'total' => 16, /* 13.649633333333 total */
                        'Moneda' => 14/* 11.882216666667 Moneda */
            );
            $pdf->colAlign =
                    array(
                        'almacen' => 'L', /* almacen */
                        'tipo' => 'L', /* tipo */
                        'id' => 'R', /* id */
                        'posicion' => 'R', /* posicion */
                        'id2' => 'L', /* id2 */
                        'cantidad' => 'R', /* cantidad */
                        'unidad' => 'L', /* unidad */
                        'descripcion' => 'L', /* descripcion */
                        'costo' => 'R', /* costo */
                        'precio' => 'R', /* precio */
                        'iva100' => 'R', /* iva100 */
                        'importe' => 'R', /* importe */
                        'descto' => 'R', /* descto */
                        'subtotal' => 'R', /* subtotal */
                        'iva' => 'R', /* iva */
                        'total' => 'R', /* total */
                        'Moneda' => 'L' /* Moneda */
            );

            if ($salida_id) {
                unset($pdf->colWidth['almacen']);
                unset($pdf->colWidth['tipo']);
                if (!$with_posicion)
                    unset($pdf->colWidth['posicion']);
                unset($pdf->colWidth['id']);
                $compensation['descripcion'] = 1;
            }else {
                if (!$with_almacen)
                    unset($pdf->colWidth['almacen']);
                if (!$with_tipo)
                    unset($pdf->colWidth['tipo']);
                unset($pdf->colWidth['posicion']);
                unset($pdf->colWidth['id']);
                unset($pdf->colWidth['id2']);
                unset($pdf->colWidth['cantidad']);
                unset($pdf->colWidth['unidad']);
                unset($pdf->colWidth['descripcion']);
                unset($pdf->colWidth['precio']);
                $compensation['iva100'] = 1;
            }
            if (!$detailed_totals) {
                unset($pdf->colWidth['iva100']);
                unset($pdf->colWidth['descto']);
                unset($pdf->colWidth['subtotal']);
                unset($pdf->colWidth['iva']);
                unset($pdf->colWidth['total']);
                unset($pdf->colWidth['Moneda']);
            }
//            if (!$with_image) {
//                unset($pdf->colWidth['imagen']);
//            }
//            $compensation['posicion'] = 0.10;
//            $compensation['id2'] = 0.02;
//            $compensation['cantidad'] = 0.10;
//            $compensation['unidad'] = 0.05;
//            $compensation['descripcion'] = 1;
//            $compensation['costo'] = 0.05;
//            $compensation['precio'] = 0.05;
//            $compensation['importe'] = 0.08;
//            $compensation['descto'] = 0.05;
//            $compensation['subtotal'] = 0.12;
//            $compensation['total'] = 0.04;
//            $compensation['Moneda'] = 0.04;
            $pdf->SetCompensation($pdf->colWidth, $compensation);

            $pdf->headerMethod[]['SetXY'] = array($pdf->lMargin + (0), ($fullw * 0.33));
            $pdf->headerMethod[]['SetFont'] = array('Arial', 'B', 6);
            $pdf->headerMethod[]['Row'] = array($pdf->colTitle, 5, '1', 1);
//            $pdf->headerMethod[]['Cell'] = array(0, 5, 'IMAGEN', 'TRB', 1, 'C');
            $pdf->headerMethod[]['SetFont'] = array('Arial', 'I', 6);

            $pdf->AddPage();
            $totales = array();
//            $pdf->colFormat['precio'] = "%0.{$this->decimals}f";
            $pdf->colFormat['iva100'] = "%0.{$this->decimals}f";
            $pdf->colFormat['letra'] = "%s";
//            $pdf->colFormat['importe'] = "%0.{$this->decimals}f";
//            $pdf->colFormat['descto'] = "%0.{$this->decimals}f";
//            $pdf->colFormat['subtotal'] = "%0.{$this->decimals}f";
//            $pdf->colFormat['iva'] = "%0.{$this->decimals}f";
//            $pdf->colFormat['total'] = "%0.{$this->decimals}f";
            $pdf->colFormat['Moneda'] = "%s";

            foreach ($rows as $i => $row) {
//                if($sucursal_id)
//                    $row['descripcion'] = "ALMACEN: " . $sucursal['almacen'] . "\n" .  $row['descripcion'];
//                $row['moneda_id'] = $salida['moneda_id'];
                //Conversion monedas por detalles
                foreach (array('costo', 'precio', 'importe', 'descto', 'subtotal', 'iva', 'total') as $field) {
                    if (isset($row[$field]))
                        $row[$field] = $row[$field] * ($row['TipoCambio'] / $salida['TipoCambio']);
                }
                $moneda_id = $salida['moneda_id'];
                $Moneda = strtolower(plural($salida['Moneda']));
                $row['Moneda'] = $Moneda;
                $TipoCambio = $salida['TipoCambio'];

                $monedas[$moneda_id]['moneda_id'] = $moneda_id;
                $monedas[$moneda_id]['Moneda'] = $Moneda;
                $monedas[$moneda_id]['TipoCambio'] = $TipoCambio;

                if ($detailed_totals)
                    $totales[$moneda_id]['letra'] = '';
                else
                    $totales[$moneda_id]['Moneda'] = $Moneda;
                //Incrementar totales
                foreach (array('importe', 'descto', 'subtotal', 'iva', 'total') as $field) {
                    if (isset($row['moneda_id']) && isset($row[$field]))
                        $totales[$moneda_id][$field] +=round($row[$field], $this->decimals);
                }

                //Formateo a detalles
                foreach (array('precio', 'importe', 'descto', 'subtotal', 'iva', 'total') as $field) {
                    if (isset($row[$field]))
                        $row[$field] = numberFormat(round($row[$field], $this->decimals), $this->decimals, '.', ',', 3);
                }
                $img = array('imagen' => array('sprintf' => "../archivos/jpg/%s.JPG", 'default' => ''));
                $h = $pdf->ImageRow($row, 4, $img, 35, 'LBR');
            }
            foreach ($monedas as $moneda_id => $moneda) {
//                //Redondeo de totales
//                foreach (array('importe', 'descto', 'subtotal', 'iva', 'total') as $field) {
//                    if(isset ($totales[$moneda_id][$field]))
//                        $totales[$moneda_id][$field] = round($totales[$moneda_id][$field], $this->decimals);
//                }
                $totales[$moneda_id]['Moneda'] = ucfirst($moneda['Moneda']) . " ({$moneda['TipoCambio']})";
                //Total a letra
                $totales[$moneda_id]['letra'] = 'Letra: ' . utf8_decode(num2letras(round2($totales[$moneda_id]['total'], 2), false, true, 'un', ' ' . strtolower($moneda['Moneda']) . ' con '));
                //Formateo de totales
                foreach (array('importe', 'descto', 'subtotal', 'iva', 'total') as $field) {
                    if (isset($totales[$moneda_id][$field]))
                        $totales[$moneda_id][$field] = numberFormat(round($totales[$moneda_id][$field], $this->decimals), $this->decimals, '.', ',', 3);
                }
            }
//            $h = $pdf->Row($totales,4,'LBR',1,$width);
//            array(0,$pdf->colWidth['importe'],$pdf->colWidth['descto'],$pdf->colWidth['subtotal'], $pdf->colWidth['iva'], $pdf->colWidth['total'],$pdf->colWidth['Moneda']);
//            $twidth['letra'] = array_sum($pdf->colWidth) - array_sum($width);
//            $pdf->FancyRow($totales[$moneda_id], $width, 4, array('LB','LB','LB','LB','LB','LB','LBR'), array('L','R','R','R','R','R','L'), array(), array('',"%0.{$this->decimals}f","%0.{$this->decimals}f","%0.{$this->decimals}f","%0.{$this->decimals}f","%0.{$this->decimals}f"));
            if ($detailed_totals) {
                $twidth['letra'] = 0;
                $twidth['importe'] = 0;
                $twidth['descto'] = 0;
                $twidth['subtotal'] = 0;
                $twidth['iva'] = 0;
                $twidth['total'] = 0;
                $twidth['Moneda'] = 0;
                foreach ($twidth as $field => $value) {
                    $twidth[$field] = $pdf->colWidth[$field];
                    $twidth['letra']+= $twidth[$field];
                    $tborder[$field] = 'LB';
                    $talign[$field] = $pdf->colAlign[$field];
                    $tstyle[$field] = '';
                    $tformat[$field] = $pdf->colFormat[$field];
                }
                $twidth['letra'] = array_sum($pdf->colWidth) - $twidth['letra'];
                $tborder['Moneda'] .= 'R';
//                $align['Moneda'] = 
                $talign['letra'] = 'L';
                $tstyle['letra'] = 'B';
//                $tformat['letra'] = $tformat['Moneda'] = "%s";
                foreach ($monedas as $moneda_id => $moneda) {
//                    //Redondeos
//                    foreach (array('importe', 'descto', 'subtotal', 'iva', 'total') as $field) {
//                        if(isset ($totales[$moneda_id][$field]))
//                            $totales[$moneda_id][$field] = round($totales[$moneda_id][$field], $this->decimals);
//                    }
//                    $totales[$moneda_id]['letra'] = 'Letra: ' . utf8_decode(num2letras(round2($totales[$moneda_id]['total'], 2), false, true, 'un', ' ' . strtolower($moneda['Moneda']) . ' con '));
//                    $totales[$moneda_id]['Moneda'] = $moneda['Moneda'];
//                    ksort($totales[$moneda_id]);
                    $pdf->FancyRow($totales[$moneda_id], $twidth, 4, $tborder, $talign, $tstyle, $tformat);
                }
            } else {
                $m = count($totales) + 1;
                $wf = 14;
                $hf = 4;
                $x = $pdf->w - $pdf->rMargin - ($m * $wf);
                foreach (array('Moneda', 'importe', 'descto', 'subtotal', 'iva', 'total') as $field) {
                    $pdf->SetX($x);
                    $pdf->Cell($wf, $hf, $pdf->colTitle[$field] . ': ', 'LBR', 0, 'R');
                    foreach ($monedas as $moneda_id => $moneda) {
                        $pdf->Cell($wf, $hf, isset($pdf->colFormat[$field]) ? sprintf($pdf->colFormat[$field], $totales[$moneda_id][$field]) : $totales[$moneda_id][$field], 'BR', 0, $pdf->colAlign[$field]);
                    }
                    $pdf->Ln($hf);
                }
                foreach ($monedas as $moneda_id => $moneda) {
//                    $pdf->MultiCell(0, $hf, 'Letra: ' . utf8_decode(num2letras(round2($totales[$moneda_id]['total'], 2), false, true, 'un', ' ' . strtolower($moneda['Moneda']) . '', ' M.N.')), 0, 'B');
                    $pdf->MultiCell(0, $hf, $totales[$moneda_id]['letra'], 0, 'B');
                }
            }
        }
//        $pdf->Output('Almacen.pdf','D');
        return $this->Output($pdf, 'Salida', $dest);
    }

    function pdfSalidas($sucursal_id=0, $almacen_id = 0, $cliente_id = 0, $agente_id = 0, $dest = '') {
//        if(empty ($almacen_id)){
//            echo "No seleccionó almacén";
//            return;
//        }
        $sucursal = $this->Sucursal($sucursal_id, $almacen_id);
        if (empty($sucursal)) {
            echo "No existe";
            return;
        }
        $sucursal = $sucursal[0];
//        pre($sucursal);exit;
        $title = "CAT�?LOGO";
        $subject = "ARTICULOS";
        $title .= " ALMACÉN: $almacen_id";

        $where_salidas = "";
        if (!empty($almacen_id)) {
            $where_salidas .= ( empty($where_salidas) ? " WHERE" : " AND") . " al.id = $almacen_id";
        } else if (!empty($sucursal_id))
            $where_salidas .= ( empty($where_salidas) ? " WHERE" : " AND") . " s.id = $sucursal_id";

        $sql_detalles_salidas = "SELECT al.nombre AS almacen,
       tm.descripcion AS tipo,
       ar_al.posicion,
       ds.mov,
       ds.salida_id,
       ds.articulo_id,
       ds.cantidad,
       ds.costo,
       ds.precio,
       (ds.descuento * 100) AS descuento100,
       (ds.iva * 100) AS iva100,
       (ds.cantidad * ds.precio) AS importe,
       (ds.cantidad * (ds.precio * ds.descuento)) AS descto,
       (ds.cantidad * (ds.precio * (1 - ds.descuento))) AS subtotal,
       (ds.cantidad * (ds.precio * (1 - ds.descuento) * ds.iva)) AS iva,
       (ds.cantidad * (ds.precio * (1 - ds.descuento) * (1 + ds.iva))) AS total
  FROM    (   (   (   articulos_almacenes ar_al
                   INNER JOIN
                      almacenes al
                   ON (ar_al.almacen_id = al.id))
               INNER JOIN
                  detalles_salidas ds
               ON (ar_al.articulo_id = ds.articulo_id))
           INNER JOIN
              salidas s
           ON (ar_al.almacen_id = s.almacen_id) AND (s.id = ds.salida_id))
       INNER JOIN
          tipo_mov tm
       ON (s.tipo_mov_id = tm.id)$where_salidas
  GROUP BY ds.salida_id,ds.mov,tm.id,al.id";
        pre($sql_salidas);
        exit;
        $pdf = new PDF_Rilez();
        $pdf->SetProtection(array('print'));
        $rows = $this->query($sql_detalles_salidas);
//        $pdf->configureRow($rows);
        $pdf->configureRow($rows, TRUE);
        if (!empty($rows)) {
            $pdf->colTitle =
                    array(
                        'imagen' => 'IMAGEN', /* imagen */
                        'codigo' => 'CODIGO', /* codigo */
                        'descripcion' => 'DESCRIPCION', /* descripcion */
                        'posicion' => 'POSICION', /* posicion */
                        'existencia' => 'EXISTENCIA', /* existencia */
                        'precio' => 'PRECIO', /* precio */
                        'iva' => 'IVA', /* iva */
                        'Moneda' => 'MONEDA' /* Moneda */
            );


            $pdf->colWidth =
                    array(
                        'imagen' => 35, /* imagen */
                        'codigo' => 22, /* codigo */
                        'descripcion' => 80, /* descripcion */
                        'posicion' => 20, /* posicion */
                        'existencia' => 24 /* existencia */
//                        'precio' => 16, /* precio */
//                        'iva' => 15, /* iva */
//                        'Moneda' => 17 /* Moneda */
            );
            if (!$with_image) {
                unset($pdf->colWidth['imagen']);
            }
            $compensation['descripcion'] = 1;
//            $compensation['imagen'] = 1;
            $pdf->colAlign =
                    array(
                        'codigo' => 'L', /* codigo */
                        'descripcion' => 'L', /* descripcion */
                        'posicion' => 'R', /* posicion */
                        'existencia' => 'R', /* posicion */
                        'precio' => 'R', /* precio */
                        'iva' => 'R', /* iva */
                        'Moneda' => 'L' /* Moneda */
            );
            $pdf->SetFont('Arial', '', 8);
            $pdf->AliasNbPages();
            $fullw = ($pdf->w - $pdf->rMargin - $pdf->lMargin);
            $w5 = $fullw / 5;
            /*
             * 
             */
            if (!empty($compensation)) {
                $maxw = 0;
                foreach ($pdf->colWidth as $field => $w) {
                    $maxw = $maxw + $w;
                }
                $compensationw = $fullw - $maxw;
                foreach ($compensation as $field => $part) {
                    if (isset($pdf->colWidth[$field]))
                        $pdf->colWidth[$field] += ( $compensationw * $part);
                }
            }
            $pdf->headerMethod[]['SetFont'] = array('Arial', 'B');
            $pdf->headerMethod[]['Image'] = array('logo_rilez.gif', $pdf->lMargin + $w5, 1, $w5 * 3);
            $pdf->headerMethod[]['SetY'] = array(20);
            $pdf->headerMethod[]['Cell'] = array(0, 5, utf8_decode($sucursal['sucursal'] . " " . $sucursal['almacen'] . ' INVENTARIO ' . date("Y m d")), '0', 1, 'C');
            $pdf->headerMethod[]['SetFont'] = array('Arial', 'B', 10);
            $pdf->headerMethod[]['Row'] = array($pdf->colTitle, 5, '1', 1);
//            $pdf->headerMethod[]['Cell'] = array(0, 5, 'IMAGEN', 'TRB', 1, 'C');
            $pdf->headerMethod[]['SetFont'] = array('Arial', 'I', 8);

            $pdf->footerMethod[]['SetY'] = array(-10);
            $pdf->footerMethod[]['Cell'] = array($w5 * 2, 5, 'Nombre y Firma Encargado', 'T', 0, 'C');
//            $pdf->footerMethod[]['Cell'] = array($w5, 5, '', '', 0, 'C');
            $pdf->footerMethod[]['Page'] = array($w5, 10);
            $pdf->footerMethod[]['Cell'] = array($w5 * 2, 5, 'Nombre y Firma Supervisor', 'T', 0, 'C');
//            $pdf->footerMethod[]['Ln'] = array(0);
//            $pdf->footerMethod[]['Page'] = array(0, 10);
            $pdf->AddPage();
            foreach ($rows as $i => $row) {
//                if($sucursal_id)
//                    $row['descripcion'] = "ALMACEN: " . $sucursal['almacen'] . "\n" .  $row['descripcion'];
                $img = array('imagen' => array('sprintf' => "../archivos/jpg/%s.JPG", 'default' => ''));
                $h = $pdf->ImageRow($row, 4, $img, 35, 'LBR');
            }
        }
//        $pdf->Output('Almacen.pdf','D');
        return $this->Output($pdf, 'Salidas', $dest);
    }

    function pdfTicketSalida($salida_id =0, $with_posicion = FALSE, $with_tipo = FALSE, $with_almacen = FALSE, $detailed_totals = TRUE, $discount_included = FALSE, $dest = '') {
        if (empty($salida_id)) {
//            if(!headers_sent())
//            header_remove ('Content-type');    
            echo "No seleccionó salida";
            return;
        }
        $title = "";
        $subject = "";

        $where_salidas = "";
        $where_detalles_salidas = "";
        if (!empty($salida_id)) {
            $where_salidas .= ( empty($where_salidas) ? " WHERE" : " AND") . " salidas.id = $salida_id";
            $where_detalles_salidas .= ( empty($where_detalles_salidas) ? " WHERE" : " AND") . " ds.salida_id = $salida_id";
        }
//        if (!empty($almacen_id)) {
//            $where_salidas .= ( empty($where_salidas) ? " WHERE" : " AND") . " al.id = $almacen_id";
//        } else if (!empty($sucursal_id))
//            $where_salidas .= ( empty($where_salidas) ? " WHERE" : " AND") . " s.id = $sucursal_id";

        $sql_salidas =
                "SELECT salidas.id
     , salidas.almacen_id
     , almacenes.nombre AS almacen
     , salidas.tipo_mov_id
     , tipo_mov.descripcion AS tipo
     , tipo_mov.tipoDeComprobante
     , tipo_mov.formaDePago
     , salidas.forma_de_pago AS metodoDePago
     , salidas.moneda_id
     , monedas.descripcion AS Moneda
     , monedas.abreviatura AS Moneda_Simbolo
     , salidas.valor_de_moneda AS TipoCambio
     , salidas.almacen_destino AS almacen_id_destino
     , almacenes_destino.nombre AS almacen_destino
     , salidas.cliente_id
     , clientes.rfc
     , clientes.descripcion AS nombre
     , clientes.nomcomercial
     , salidas.fecha
     , salidas.referencia1
     , salidas.referencia2
     , salidas.vencimiento
     , salidas.suma
     , salidas.descuento
     , salidas.subtotal
     , salidas.iva
     , salidas.total
     , salidas.abonos
     , salidas.devolucion
FROM
  salidas
INNER JOIN tipo_mov
ON salidas.tipo_mov_id = tipo_mov.id
INNER JOIN monedas
ON salidas.moneda_id = monedas.id
LEFT OUTER JOIN clientes
ON salidas.cliente_id = clientes.id
INNER JOIN almacenes
ON salidas.almacen_id = almacenes.id
LEFT OUTER JOIN almacenes almacenes_destino
ON salidas.almacen_destino = almacenes_destino.id$where_salidas";
        $sql_detalles_salidas = "SELECT" .
                (empty($salida_id) ?
                        ($with_almacen ? "
       al.nombre AS almacen," : "") .
                        ($with_tipo ? "
       tm.descripcion AS tipo," : "") . "
       s.id,
       " :
                        ' ds.mov,
       ds.articulo_id,' .
                        ($with_posicion ? "
       ar_al.posicion," : "") . "
       ar.id2,
       ds.cantidad,
       unidades.descripcion AS unidad,
       ar.descripcion,
--       ds.costo,
       " . ($discount_included ? "(ds.precio * (1 - ds.descuento))" : "ds.precio") . " AS precio,
--       (ds.descuento * 100) AS descto100,
       (ds.iva * 100) AS iva100," . "") .
                (empty($salida_id) ? "SUM" : "") . "(ds.cantidad * " . ($discount_included ? "(ds.precio * (1 - ds.descuento))" : "ds.precio") . ") AS importe," .
                (empty($salida_id) ? "SUM" : "") . "(ds.cantidad * " . ($discount_included ? "0" : "(ds.precio * (ds.descuento))") . ") AS descto," .
                (empty($salida_id) ? "SUM" : "") . "(ds.cantidad * (ds.precio * (1 - ds.descuento))) AS subtotal," .
                (empty($salida_id) ? "SUM" : "") . "(ds.cantidad * (ds.precio * (1 - ds.descuento) * ds.iva)) AS iva," .
                (empty($salida_id) ? "SUM" : "") . "(ds.cantidad * (ds.precio * (1 - ds.descuento) * (1 + ds.iva))) AS total,
       s.moneda_id AS moneda_id,
       s.valor_de_moneda AS TipoCambio,
       monedas.descripcion AS Moneda
  FROM    (   (   (   (   (   (   articulos_almacenes ar_al
                               INNER JOIN
                                  articulos ar
                               ON (ar_al.articulo_id = ar.id))
                           INNER JOIN
                              salidas s
                           ON (ar_al.almacen_id = s.almacen_id))
                       INNER JOIN
                          almacenes al
                       ON (ar_al.almacen_id = al.id))
                   INNER JOIN
                      detalles_salidas ds
                   ON (ar_al.articulo_id = ds.articulo_id)
                      AND (s.id = ds.salida_id))
               INNER JOIN
                  monedas monedas
               ON (ar_al.moneda_id = monedas.id))
           INNER JOIN
              tipo_mov tm
           ON (s.tipo_mov_id = tm.id))
       INNER JOIN
          unidades unidades
       ON (ar.unidad_id = unidades.id)$where_detalles_salidas
GROUP BY s.id," .
                (empty($salida_id) ? "" : "
    ds.mov,") . "
        s.almacen_id," .
                (($with_tipo) ? "
    tm.id," : "") . "
         ar_al.moneda_id,
         ds.iva";
//        pre($sql_salidas);
        $salida = $this->query($sql_salidas);
        $salida = $salida[0];
        if (empty($salida)) {
            exit("Salida $salida_id No disponible");
        }
        $title = $salida['almacen'] . ' Salida ' . $salida['id'] . ' ' . $salida['tipo'] . ' ' . date_format(date_create($salida['fecha']), 'd/m/Y');
//        pre($salida);
        $sucursal_matriz = $this->Sucursal($sucursal_id, $salida['almacen_id'], TRUE);
        $sucursal_matriz = $sucursal_matriz[0];
        if (($sucursal_matriz['matriz_id'])) {
            $sucursal = $sucursal_matriz;
            $sucursal_matriz = $this->Sucursal($sucursal['matriz_id']);
            $sucursal_matriz = $sucursal_matriz[0];
        }
        if ($salida['cliente_id']) {
            $receptor = $this->Cliente($salida['cliente_id']);
        }
        if ($salida['almacen_id_destino']) {
            $receptor = $this->Sucursal(0, $salida['almacen_id_destino']);
            $subject .= "Origen: " . $salida['almacen'] . " Destino: " . $salida['almacen_destino'];
        }
        $receptor = $receptor[0];
//        pre($receptor);
//        if (empty($sucursal)) {
//            echo "No existe";
//            return;
//        }
//        $sucursal = $sucursal[0];
//        pre($sucursal_matriz);
//        pre($sucursal);
//        pre($sql_detalles_salidas);
        $rows = $this->query($sql_detalles_salidas);
//        pre($rows);
//        exit;
        /**
         * array(612,792) Letter
         * array(80,~) Ticket
         */
        $pdf = new PDF_Rilez('P', 'mm', array(80, 130));
        $pdf->SetProtection(array('print'));
        $pdf->SetFont('Arial', '', 6);
        $pdf->AliasNbPages();
        $pdf->SetMargins(5, 5);
        $auto = TRUE;
        $margin = 5;
        $pdf->SetTopMargin($margin);
        $pdf->SetAutoPageBreak($auto, $margin);
//        $pdf->configureRow($rows);
//        $pdf->configureRow($rows, TRUE);
        $fullw = ($pdf->w - $pdf->rMargin - $pdf->lMargin);
        $w5 = $fullw / 5;
        $w10 = $fullw / 10;
        $w20 = $fullw / 20;
        $c = $fullw / 3;
        $pdf->headerMethod[]['SetFont'] = array('Arial', 'B');
        $pdf->headerMethod[]['Image'] = array('logo_rilez.gif', $pdf->lMargin + ($fullw * 0.3), 1, ($fullw * 0.4));
        $pdf->headerMethod[]['SetY'] = array(($fullw * 0.07));
        $pdf->headerMethod[]['Cell'] = array(0, 3, utf8_decode($title), '0', 1, 'C');
        $pdf->headerMethod[]['Cell'] = array(0, 3, utf8_decode($subject), '0', 1, 'C');

//        $pdf->headerMethod[]['SetXY'] = array($pdf->lMargin + (0), ($fullw * 0.10));
        if ($sucursal_matriz) {// * .33
            $pdf->headerMethod[]['MultiCell'] = array(($fullw), 3, "Emisor:\n" .
                (isset($sucursal_matriz['nomcomercial']) ? utf8_decode($sucursal_matriz['nomcomercial']) . "\n" : "") .
                (isset($sucursal_matriz['nombre']) ? utf8_decode($sucursal_matriz['nombre']) . "\n" : "") .
                (isset($sucursal_matriz['rfc']) ? 'RFC: ' . $sucursal_matriz['rfc'] . "\n" : "") .
                (isset($sucursal_matriz['calle']) ? utf8_decode($sucursal_matriz['calle']) : "") .
                (isset($sucursal_matriz['noExterior']) ? " No. " . $sucursal_matriz['noExterior'] .
                        (!empty($sucursal_matriz['noInterior']) ?
                                " - " . $sucursal_matriz['noInterior'] : '') . "\n" : "") .
                (isset($sucursal_matriz['colonia']) ? utf8_decode($sucursal_matriz['colonia']) . "\n" : "") .
                (isset($sucursal_matriz['municipio']) ? utf8_decode($sucursal_matriz['municipio']) . "\n" : "") .
                (isset($sucursal_matriz['codigoPostal']) ? "C.P. " . $sucursal_matriz['codigoPostal'] . "\n" : "") .
                (isset($sucursal_matriz['estado']) ? utf8_decode($sucursal_matriz['estado'] . "\n") : "") .
                (isset($sucursal_matriz['pais']) ? utf8_decode($sucursal_matriz['pais']) : ""), 0, "L");
        }
//        $pdf->headerMethod[]['SetXY'] = array($pdf->lMargin + ($fullw * .33), ($fullw * 0.10));
        if ($sucursal) {// * .33
            $pdf->headerMethod[]['MultiCell'] = array(($fullw), 3, "Expedida en:\n" .
                (isset($sucursal['nomcomercial']) ? utf8_decode($sucursal['nomcomercial']) . "\n" : "") .
                (isset($sucursal['nombre']) ? utf8_decode($sucursal['nombre']) . "\n" : "") .
                (isset($sucursal['rfc']) ? 'RFC: ' . $sucursal['rfc'] . "\n" : "") .
                (isset($sucursal['calle']) ? utf8_decode($sucursal['calle']) : "") .
                (isset($sucursal['noExterior']) ? " No. " . $sucursal['noExterior'] .
                        (!empty($sucursal['noInterior']) ?
                                " - " . $sucursal['noInterior'] : '') . "\n" : "") .
                (isset($sucursal['colonia']) ? utf8_decode($sucursal['colonia']) . "\n" : "") .
                (isset($sucursal['municipio']) ? utf8_decode($sucursal['municipio']) . "\n" : "") .
                (isset($sucursal['codigoPostal']) ? "C.P. " . $sucursal['codigoPostal'] . "\n" : "") .
                (isset($sucursal['estado']) ? utf8_decode($sucursal['estado'] . "\n") : "") .
                (isset($sucursal['pais']) ? utf8_decode($sucursal['pais']) : ""), 0, "C");
        }
//        $pdf->headerMethod[]['SetXY'] = array($pdf->lMargin + ($fullw * .66), ($fullw * 0.10));
        if ($receptor) {// * .33
            $pdf->headerMethod[]['MultiCell'] = array(($fullw), 3, "Receptor:\n" .
                (isset($receptor['nomcomercial']) ? utf8_decode($receptor['nomcomercial']) . "\n" : "") .
                (isset($receptor['nombre']) ? utf8_decode($receptor['nombre']) . "\n" : "") .
                (isset($receptor['rfc']) ? 'RFC: ' . $receptor['rfc'] . "\n" : "") .
                (isset($receptor['calle']) ? utf8_decode($receptor['calle']) : "") .
                (isset($receptor['noExterior']) ? " No. " . $receptor['noExterior'] .
                        (!empty($receptor['noInterior']) ?
                                " - " . $receptor['noInterior'] : '') . "\n" : "") .
                (isset($receptor['colonia']) ? utf8_decode($receptor['colonia']) . "\n" : "") .
                (isset($receptor['municipio']) ? utf8_decode($receptor['municipio']) . "\n" : "") .
                (isset($receptor['codigoPostal']) ? "C.P. " . $receptor['codigoPostal'] . "\n" : "") .
                (isset($receptor['estado']) ? utf8_decode($receptor['estado'] . "\n") : "") .
                (isset($receptor['pais']) ? utf8_decode($receptor['pais']) : ""), 0, "R");
        }

        $pdf->footerMethod[]['SetY'] = array(-15);
//        $pdf->footerMethod[]['Cell'] = array(($fullw * .4), 5, 'Nombre y Firma Encargado', 'T', 0, 'C');
//            $pdf->footerMethod[]['Cell'] = array($w5, 5, '', '', 0, 'C');
        $pdf->footerMethod[]['Page'] = array(($fullw * .2), 10);
//        $pdf->footerMethod[]['Cell'] = array(($fullw * .4), 5, 'Nombre y Firma Supervisor', 'T', 0, 'C');
//            $pdf->footerMethod[]['Ln'] = array(0);
//            $pdf->footerMethod[]['Page'] = array(0, 10);
        if (!empty($rows)) {
            $pdf->colTitle =
                    array(
                        'almacen' => 'ALMACEN', /* almacen */
                        'tipo' => 'TIPO', /* tipo */
                        'id' => 'ID', /* id */
                        'posicion' => 'POSICION', /* posicion */
                        'id2' => 'ID2', /* id2 */
                        'cantidad' => 'CANTIDAD', /* cantidad */
                        'unidad' => 'UNIDAD', /* unidad */
                        'descripcion' => 'DESCRIPCION', /* descripcion */
                        'costo' => 'COSTO', /* costo */
                        'precio' => 'PRECIO', /* precio */
                        'iva100' => 'IVA100', /* iva */
                        'importe' => 'IMPORTE', /* importe */
                        'descto' => 'DESCTO', /* descto */
                        'subtotal' => 'SUBTOTAL', /* subtotal */
                        'iva' => 'IVA', /* iva */
                        'total' => 'TOTAL', /* total */
                        'Moneda' => 'MONEDA' /* Moneda */
            );


            $pdf->colWidth =
                    array(
//                        'almacen' => 14, /* 14.001 almacen */
//                        'tipo' => 14, /* 14.703733333333 tipo */
//                        'id' => 14, /* 14.703733333333 id */
//                        'posicion' => 13, /* 13.763933333333 posicion */
                        'id2' => 16, /* 14.703733333333 id2 */
                        'cantidad' => 14, /* 14.001 cantidad */
                        'unidad' => 11, /* 13.528983333333 unidad */
//                        'descripcion' => 37, /* 37.179758333333 descripcion */
//                        'costo' => 11, /*11.2959 costo */
                        'precio' => 13, /* 11.763683333333 precio */
//                        'iva100' => 11,
                        'importe' => 16//, /* 13.649633333333 importe */
//                        'descto' => 15, /* 13.179758333333 descto */
//                        'subtotal' => 16, /* 13.649633333333 subtotal */
//                        'iva' => 15, /* 13.649633333333 iva */
//                        'total' => 16, /* 13.649633333333 total */
//                        'Moneda' => 17/* 11.882216666667 Moneda */
            );
            $pdf->colAlign =
                    array(
                        'almacen' => 'L', /* almacen */
                        'tipo' => 'L', /* tipo */
                        'id' => 'R', /* id */
                        'posicion' => 'R', /* posicion */
                        'id2' => 'L', /* id2 */
                        'cantidad' => 'R', /* cantidad */
                        'unidad' => 'L', /* unidad */
                        'descripcion' => 'L', /* descripcion */
                        'costo' => 'R', /* costo */
                        'precio' => 'R', /* precio */
                        'iva100' => 'R', /* iva100 */
                        'importe' => 'R', /* importe */
                        'descto' => 'R', /* descto */
                        'subtotal' => 'R', /* subtotal */
                        'iva' => 'R', /* iva */
                        'total' => 'R', /* total */
                        'Moneda' => 'L' /* Moneda */
            );

            if ($salida_id) {
                unset($pdf->colWidth['almacen']);
                unset($pdf->colWidth['tipo']);
                if (!$with_posicion)
                    unset($pdf->colWidth['posicion']);
                unset($pdf->colWidth['id']);
//                $compensation['descripcion'] = 1;
                $compensation['id2'] = 1;
            }else {
                if (!$with_almacen)
                    unset($pdf->colWidth['almacen']);
                if (!$with_tipo)
                    unset($pdf->colWidth['tipo']);
                unset($pdf->colWidth['posicion']);
                unset($pdf->colWidth['id']);
                unset($pdf->colWidth['id2']);
                unset($pdf->colWidth['cantidad']);
                unset($pdf->colWidth['unidad']);
                unset($pdf->colWidth['descripcion']);
                unset($pdf->colWidth['precio']);
                $compensation['iva100'] = 1;
            }
            if (!$detailed_totals) {
                unset($pdf->colWidth['iva100']);
                unset($pdf->colWidth['descto']);
                unset($pdf->colWidth['subtotal']);
                unset($pdf->colWidth['iva']);
                unset($pdf->colWidth['total']);
                unset($pdf->colWidth['Moneda']);
            }
//            if (!$with_image) {
//                unset($pdf->colWidth['imagen']);
//            }
//            $compensation['posicion'] = 0.10;
//            $compensation['id2'] = 0.02;
//            $compensation['cantidad'] = 0.10;
//            $compensation['unidad'] = 0.05;
//            $compensation['descripcion'] = 1;
//            $compensation['costo'] = 0.05;
//            $compensation['precio'] = 0.05;
//            $compensation['importe'] = 0.08;
//            $compensation['descto'] = 0.05;
//            $compensation['subtotal'] = 0.12;
//            $compensation['total'] = 0.04;
//            $compensation['Moneda'] = 0.04;
            $pdf->SetCompensation($pdf->colWidth, $compensation);

//            $pdf->headerMethod[]['SetXY'] = array($pdf->lMargin + (0), ($fullw * 0.33));
            $pdf->headerMethod[]['SetFont'] = array('Arial', 'B', 5);
            $pdf->headerMethod[]['Row'] = array($pdf->colTitle, 3, 'TRL', 1);
            $pdf->headerMethod[]['MultiCell'] = array(0, 3, 'DESCRIPCION', 'RBL', 'L');
            $pdf->headerMethod[]['SetFont'] = array('Arial', 'I', 5);

            $pdf->AddPage();
            $totales = array();
            $pdf->colFormat['precio'] = "%0.{$this->decimals}f";
            $pdf->colFormat['iva100'] = "%0.{$this->decimals}f";
            $pdf->colFormat['letra'] = "%s";
            $pdf->colFormat['importe'] = "%0.{$this->decimals}f";
            $pdf->colFormat['descto'] = "%0.{$this->decimals}f";
            $pdf->colFormat['subtotal'] = "%0.{$this->decimals}f";
            $pdf->colFormat['iva'] = "%0.{$this->decimals}f";
            $pdf->colFormat['total'] = "%0.{$this->decimals}f";
            $pdf->colFormat['Moneda'] = "%s";

            foreach ($rows as $i => $row) {
//                if($sucursal_id)
//                    $row['descripcion'] = "ALMACEN: " . $sucursal['almacen'] . "\n" .  $row['descripcion'];
                $row['Moneda'] = strtolower(plural($row['Moneda']));
                $monedas[$row['moneda_id']]['moneda_id'] = $row['moneda_id'];
                $monedas[$row['moneda_id']]['Moneda'] = $row['Moneda'];
                $monedas[$row['moneda_id']]['TipoCambio'] = $row['TipoCambio'];

                if ($detailed_totals)
                    $totales[$row['moneda_id']]['letra'] = '';
                else {
                    $totales[$row['moneda_id']]['Moneda'] = $row['Moneda'];
                }
                $totales[$row['moneda_id']]['importe'] +=$row['importe'];
                $totales[$row['moneda_id']]['descto'] +=$row['descto'];
                $totales[$row['moneda_id']]['subtotal'] +=$row['subtotal'];
                $totales[$row['moneda_id']]['iva'] +=$row['iva'];
                $totales[$row['moneda_id']]['total'] +=$row['total'];

//                $img = array('imagen' => array('sprintf' => "../archivos/jpg/%s.JPG", 'default' => ''));
                $h = $pdf->Row($row, 3, 'RL', 1);
//                $hd=$pdf->NbLines(0, $row['descripcion']) * 3;
                $pdf->MultiCell(0, 3, $row['descripcion'], 'RBL', 'L');
//                $pdf->Ln($hd);
//                $pdf->SetH($pdf->GetH() + $h + $hd);
//                $h = $pdf->ImageRow($row, 4, $img, 35, 'LBR');
            }
//            $h = $pdf->Row($totales,4,'LBR',1,$width);
//            array(0,$pdf->colWidth['importe'],$pdf->colWidth['descto'],$pdf->colWidth['subtotal'], $pdf->colWidth['iva'], $pdf->colWidth['total'],$pdf->colWidth['Moneda']);
//            $twidth['letra'] = array_sum($pdf->colWidth) - array_sum($width);
//            $pdf->FancyRow($totales[$moneda_id], $width, 4, array('LB','LB','LB','LB','LB','LB','LBR'), array('L','R','R','R','R','R','L'), array(), array('',"%0.{$this->decimals}f","%0.{$this->decimals}f","%0.{$this->decimals}f","%0.{$this->decimals}f","%0.{$this->decimals}f"));
            if ($detailed_totals) {
                $twidth['letra'] = 0;
                $twidth['importe'] = 0;
                $twidth['descto'] = 0;
                $twidth['subtotal'] = 0;
                $twidth['iva'] = 0;
                $twidth['total'] = 0;
                $twidth['Moneda'] = 0;
                foreach ($twidth as $field => $value) {
                    $twidth[$field] = $pdf->colWidth[$field];
                    $twidth['letra']+= $twidth[$field];
                    $tborder[$field] = 'LB';
                    $talign[$field] = $pdf->colAlign[$field];
                    $tstyle[$field] = '';
                    $tformat[$field] = $pdf->colFormat[$field];
                }
                $twidth['letra'] = array_sum($pdf->colWidth) - $twidth['letra'];
                $tborder['Moneda'] .= 'R';
//                $align['Moneda'] = 
                $talign['letra'] = 'L';
                $tstyle['letra'] = 'B';
//                $tformat['letra'] = $tformat['Moneda'] = "%s";
                foreach ($monedas as $moneda_id => $moneda) {
                    $totales[$moneda_id]['letra'] = 'Letra: ' . utf8_decode(num2letras(round2($totales[$moneda_id]['total'], 2), false, true, 'un', ' ' . strtolower($moneda['Moneda']) . ' con '));
                    $totales[$moneda_id]['Moneda'] = $moneda['Moneda'];
//                    ksort($totales[$moneda_id]);
                    $pdf->FancyRow($totales[$moneda_id], $twidth, 3, $tborder, $talign, $tstyle, $tformat);
                }
            } else {
                $m = count($totales) + 1;
                $wf = 14;
                $hf = 4;
                $x = $pdf->w - $pdf->rMargin - ($m * $wf);
                foreach (array('Moneda', 'importe', 'descto', 'subtotal', 'iva', 'total') as $field) {
                    $pdf->SetX($x);
                    $pdf->Cell($wf, $hf, $pdf->colTitle[$field] . ': ', 'LBR', 0, 'R');
                    foreach ($monedas as $moneda_id => $moneda) {
                        $pdf->Cell($wf, $hf, sprintf($pdf->colFormat[$field], $totales[$moneda_id][$field]), 'BR', 0, $pdf->colAlign[$field]);
                    }
                    $pdf->Ln($hf);
                }
                foreach ($monedas as $moneda_id => $moneda) {
                    $pdf->MultiCell(0, 3, 'Letra: ' . utf8_decode(num2letras(round2($totales[$moneda_id]['total'], 2), false, true, 'un', ' ' . strtolower($moneda['Moneda']) . '', ' M.N.'), 0, 'B'));
                }
            }
        }
//        $pdf->Output('Almacen.pdf','D');
        return $this->Output($pdf, 'TicketSalida', $dest);
    }

    function pdfEntrada($entrada_id =0, $with_posicion = FALSE, $with_tipo = FALSE, $with_almacen = FALSE, $detailed_totals = TRUE, $discount_included = FALSE, $dest = '') {
        if (empty($entrada_id)) {
//            if(!headers_sent())
//            header_remove ('Content-type');    
            echo "No seleccionó entrada";
            return;
        }

        $title = "";
        $subject = "";

        $where_entradas = "";
        $where_detalles_entradas = "";
        if (!empty($entrada_id)) {
            $where_entradas .= ( empty($where_entradas) ? " WHERE" : " AND") . " entradas.id = $entrada_id";
            $where_detalles_entradas .= ( empty($where_detalles_entradas) ? " WHERE" : " AND") . " de.entrada_id = $entrada_id";
        }
//        if (!empty($almacen_id)) {
//            $where_entradas .= ( empty($where_entradas) ? " WHERE" : " AND") . " al.id = $almacen_id";
//        } else if (!empty($sucursal_id))
//            $where_entradas .= ( empty($where_entradas) ? " WHERE" : " AND") . " e.id = $sucursal_id";
        $sql_entradas =
                "SELECT entradas.id
     , entradas.almacen_id
     , almacenes.nombre AS almacen
     , entradas.tipo_mov_id
     , tipo_mov.descripcion AS tipo
     , tipo_mov.tipoDeComprobante
     , tipo_mov.formaDePago
     , entradas.forma_de_pago AS metodoDePago
     , entradas.moneda_id
     , monedas.descripcion AS Moneda
     , monedas.abreviatura AS Moneda_Simbolo
     , entradas.valor_de_moneda AS TipoCambio
     , entradas.almacen_origen AS almacen_id_origen
     , almacenes_origen.nombre AS almacen_origen
     , entradas.proveedor_id
     , proveedores.rfc
     , proveedores.descripcion AS nombre
     , proveedores.nomcomercial
     , entradas.fecha
     , entradas.referencia1
     , entradas.referencia2
     , entradas.suma
     , entradas.descuento
     , entradas.subtotal
     , entradas.iva
     , entradas.total
     , entradas.abonos
     , entradas.devolucion
FROM
  entradas
INNER JOIN tipo_mov
ON entradas.tipo_mov_id = tipo_mov.id
INNER JOIN monedas
ON entradas.moneda_id = monedas.id
LEFT OUTER JOIN proveedores
ON entradas.proveedor_id = proveedores.id
INNER JOIN almacenes
ON entradas.almacen_id = almacenes.id
LEFT OUTER JOIN almacenes almacenes_origen
ON entradas.almacen_origen = almacenes_origen.id$where_entradas";

        $sql_detalles_entradas = "SELECT" .
                (empty($entrada_id) ?
                        ($with_almacen ? "
       al.nombre AS almacen," : "") .
                        ($with_tipo ? "
       tm.descripcion AS tipo," : "") . "
       e.id," : "
       de.mov,
       de.articulo_id,") .
                ($with_posicion ? "
       ar_al.posicion," : "") . "
       ar.id2,
       de.cantidad,
       unidades.descripcion AS unidad,
       ar.descripcion,
       " . ($discount_included ? "(de.costo * (1 - de.descuento))" : "de.costo") . " AS costo,
--       (de.descuento * 100) AS descto100,
       (de.iva * 100) AS iva100," .
                (empty($entrada_id) ? "SUM" : "") . "(de.cantidad * " . ($discount_included ? "(de.costo * (1 - de.descuento))" : "de.costo") . ") AS importe," .
                (empty($entrada_id) ? "SUM" : "") . "(de.cantidad * " . ($discount_included ? "0" : "(de.costo * (de.descuento))") . ") AS descto," .
                (empty($entrada_id) ? "SUM" : "") . "(de.cantidad * (de.costo * (1 - de.descuento))) AS subtotal," .
                (empty($entrada_id) ? "SUM" : "") . "(de.cantidad * (de.costo * (1 - de.descuento) * de.iva)) AS iva," .
                (empty($entrada_id) ? "SUM" : "") . "(de.cantidad * (de.costo * (1 - de.descuento) * (1 + de.iva))) AS total,
       de.moneda_id AS moneda_id,
       de.valor_moneda AS TipoCambio,
       monedas.descripcion AS Moneda
  FROM    (   (   (   (   (   (   articulos_almacenes ar_al
                               INNER JOIN
                                  articulos ar
                               ON (ar_al.articulo_id = ar.id))
                           INNER JOIN
                              entradas e
                           ON (ar_al.almacen_id = e.almacen_id))
                       INNER JOIN
                          almacenes al
                       ON (ar_al.almacen_id = al.id))
                   INNER JOIN
                      detalles_entradas de
                   ON (ar_al.articulo_id = de.articulo_id)
                      AND (e.id = de.entrada_id))
               INNER JOIN
                  monedas monedas
               ON (ar_al.moneda_id = monedas.id))
           INNER JOIN
              tipo_mov tm
           ON (e.tipo_mov_id = tm.id))
       INNER JOIN
          unidades unidades
       ON (ar.unidad_id = unidades.id)$where_detalles_entradas
GROUP BY-- e.id," .
                (empty($entrada_id) ? "" : "
    de.mov,") . "
        e.almacen_id," .
                (($with_tipo) ? "
    tm.id," : "") . "
         ar_al.moneda_id,
         de.iva";

//	pre($sql_entradas);
        $entrada = $this->query($sql_entradas);
        $entrada = $entrada[0];
        if (empty($entrada)) {
            exit("Entrada... $entrada_id No disponible");
        }
//        pre($entrada);
        $title = $entrada['almacen'] . ' Entrada ' . $entrada['id'] . ' ' . $entrada['tipo'] . ' ' . $entrada['referencia1'] . ' ' . date_format(date_create($entrada['fecha']), 'd/m/Y');
        // $sucursal_id=2;
        // $sucursal = $this->Sucursal($sucursal_id, $master['almacen_id'], TRUE);
        $receptor = $this->Sucursal(0, $entrada['almacen_id']);
        $receptor = $receptor[0];
//        pre($receptor);
        if ($entrada['proveedor_id']) {
            $emisor = $this->Proveedor($entrada['proveedor_id']);
            $emisor = $emisor[0];
        }
        if ($entrada['almacen_id_origen']) {
            $emisor = $this->Sucursal(0, $entrada['almacen_id_origen']);
            $emisor = $emisor[0];
            $subject .= "Origen: " . $emisor['almacen'] . " Destino: " . $receptor['almacen'];
        }
//        pre($emisor);
        // $sucursal = $sucursal[0];
        // pre($sucursal);
//        pre($sql_detalles_entradas);
        $rows = $this->query($sql_detalles_entradas);
//	pre($rows);
//        exit;

        $pdf = new PDF_Rilez();
        $pdf->SetProtection(array('print'));
        $pdf->SetFont('Arial', '', 6);
        $pdf->AliasNbPages();
//        $pdf->configureRow($rows);
//        $pdf->configureRow($rows, TRUE);
        $fullw = ($pdf->w - $pdf->rMargin - $pdf->lMargin);
        $w5 = $fullw / 5;
        $pdf->headerMethod[]['SetFont'] = array('Arial', 'B');
        $pdf->headerMethod[]['Image'] = array('logo_rilez.gif', $pdf->lMargin + ($fullw * 0.3), 1, ($fullw * 0.4));
        $pdf->headerMethod[]['SetY'] = array(($fullw * 0.07));
        $pdf->headerMethod[]['Cell'] = array(0, 3, utf8_decode($title), '0', 1, 'C');
        $pdf->headerMethod[]['Cell'] = array(0, 3, utf8_decode($subject), '0', 1, 'C');

        $pdf->headerMethod[]['SetXY'] = array($pdf->lMargin + ($fullw * .0), ($fullw * 0.10));
        if ($emisor) {
            $pdf->headerMethod[]['MultiCell'] = array(($fullw * .33), 4, "Emisor:\n" .
                (isset($emisor['nomcomercial']) ? utf8_decode($emisor['nomcomercial']) . "\n" : "") .
                (isset($emisor['nombre']) ? utf8_decode($emisor['nombre']) . "\n" : "") .
                (isset($emisor['rfc']) ? 'RFC: ' . $emisor['rfc'] . "\n" : "") .
                (isset($emisor['calle']) ? utf8_decode($emisor['calle']) : "") .
                (isset($emisor['noExterior']) ? " No. " . $emisor['noExterior'] .
                        (!empty($emisor['noInterior']) ?
                                " - " . $emisor['noInterior'] : '') . "\n" : "") .
                (isset($emisor['colonia']) ? utf8_decode($emisor['colonia']) . "\n" : "") .
                (isset($emisor['municipio']) ? utf8_decode($emisor['municipio']) . "\n" : "") .
                (isset($emisor['codigoPostal']) ? "C.P. " . $emisor['codigoPostal'] . "\n" : "") .
                (isset($emisor['estado']) ? utf8_decode($emisor['estado'] . "\n") : "") .
                (isset($emisor['pais']) ? utf8_decode($emisor['pais']) : ""), 0, "L");
        }
//        $pdf->headerMethod[]['SetXY'] = array($pdf->lMargin + ($fullw * .33), ($fullw * 0.10));
        $pdf->headerMethod[]['SetXY'] = array($pdf->lMargin + ($fullw * .66), ($fullw * 0.10));
        if ($receptor) {
            $pdf->headerMethod[]['MultiCell'] = array(($fullw * .33), 4, "Receptor:\n" .
                (isset($receptor['nomcomercial']) ? utf8_decode($receptor['nomcomercial']) . "\n" : "") .
                (isset($receptor['nombre']) ? utf8_decode($receptor['nombre']) . "\n" : "") .
                (isset($receptor['rfc']) ? 'RFC: ' . $receptor['rfc'] . "\n" : "") .
                (isset($receptor['calle']) ? utf8_decode($receptor['calle']) : "") .
                (isset($receptor['noExterior']) ? " No. " . $receptor['noExterior'] .
                        (!empty($receptor['noInterior']) ?
                                " - " . $receptor['noInterior'] : '') . "\n" : "") .
                (isset($receptor['colonia']) ? utf8_decode($receptor['colonia']) . "\n" : "") .
                (isset($receptor['municipio']) ? utf8_decode($receptor['municipio']) . "\n" : "") .
                (isset($receptor['codigoPostal']) ? "C.P. " . $receptor['codigoPostal'] . "\n" : "") .
                (isset($receptor['estado']) ? utf8_decode($receptor['estado'] . "\n") : "") .
                (isset($receptor['pais']) ? utf8_decode($receptor['pais']) : ""), 0, "R");
        }

        $pdf->footerMethod[]['SetY'] = array(-10);
        $pdf->footerMethod[]['Cell'] = array(($fullw * .4), 5, 'Nombre y Firma Encargado', 'T', 0, 'C');
//            $pdf->footerMethod[]['Cell'] = array($w5, 5, '', '', 0, 'C');
        $pdf->footerMethod[]['Page'] = array(($fullw * .2), 10);
        $pdf->footerMethod[]['Cell'] = array(($fullw * .4), 5, 'Nombre y Firma Supervisor', 'T', 0, 'C');
//            $pdf->footerMethod[]['Ln'] = array(0);
//            $pdf->footerMethod[]['Page'] = array(0, 10);

        if (!empty($rows)) {
            $pdf->colTitle =
                    array(
                        'almacen' => 'ALMACEN', /* almacen */
                        'tipo' => 'TIPO', /* tipo */
                        'id' => 'ID', /* id */
                        'posicion' => 'POSICION', /* posicion */
                        'id2' => 'ID2', /* id2 */
                        'cantidad' => 'CANTIDAD', /* cantidad */
                        'unidad' => 'UNIDAD', /* unidad */
                        'descripcion' => 'DESCRIPCION', /* descripcion */
                        'costo' => 'COSTO', /* costo */
                        'iva100' => 'IVA100', /* iva */
                        'importe' => 'IMPORTE', /* importe */
                        'descto' => 'DESCTO', /* descto */
                        'subtotal' => 'SUBTOTAL', /* subtotal */
                        'iva' => 'IVA', /* iva */
                        'total' => 'TOTAL', /* total */
                        'Moneda' => 'MONEDA' /* Moneda */
            );


            $pdf->colWidth =
                    array(
                        'id' => 14, /* 14.703733333333 id */
                        'almacen' => 14, /* 14.001 almacen */
                        'tipo' => 14, /* 14.703733333333 tipo */
                        'posicion' => 13, /* 13.763933333333 posicion */
                        'id2' => 16, /* 14.703733333333 id2 */
                        'cantidad' => 14, /* 14.001 cantidad */
                        'unidad' => 11, /* 13.528983333333 unidad */
                        'descripcion' => 37, /* 37.179758333333 descripcion */
                        'costo' => 11, /* 11.2959 costo */
                        'iva100' => 11,
                        'importe' => 16, /* 13.649633333333 importe */
                        'descto' => 15, /* 13.179758333333 descto */
                        'subtotal' => 16, /* 13.649633333333 subtotal */
                        'iva' => 15, /* 13.649633333333 iva */
                        'total' => 16, /* 13.649633333333 total */
                        'Moneda' => 17 /* 11.882216666667 Moneda */
            );

            $pdf->colAlign =
                    array(
                        'almacen' => 'L', /* almacen */
                        'tipo' => 'L', /* tipo */
                        'id' => 'R', /* id */
                        'posicion' => 'R', /* posicion */
                        'id2' => 'L', /* id2 */
                        'cantidad' => 'R', /* cantidad */
                        'unidad' => 'L', /* unidad */
                        'descripcion' => 'L', /* descripcion */
                        'costo' => 'R', /* costo */
                        'iva100' => 'R', /* iva100 */
                        'importe' => 'R', /* importe */
                        'descto' => 'R', /* descto */
                        'subtotal' => 'R', /* subtotal */
                        'iva' => 'R', /* iva */
                        'total' => 'R', /* total */
                        'Moneda' => 'L' /* Moneda */
            );

            if ($entrada_id) {
                unset($pdf->colWidth['almacen']);
                unset($pdf->colWidth['tipo']);
                if (!$with_posicion)
                    unset($pdf->colWidth['posicion']);
                unset($pdf->colWidth['id']);
                $compensation['descripcion'] = 1;
            }else {
                if (!$with_almacen)
                    unset($pdf->colWidth['almacen']);
                if (!$with_tipo)
                    unset($pdf->colWidth['tipo']);
                unset($pdf->colWidth['posicion']);
                // unset($pdf->colWidth['id']);
                unset($pdf->colWidth['id2']);
                unset($pdf->colWidth['cantidad']);
                unset($pdf->colWidth['unidad']);
                unset($pdf->colWidth['descripcion']);
                unset($pdf->colWidth['costo']);
                $compensation['iva100'] = 1;
            }
            if (!$detailed_totals) {
                unset($pdf->colWidth['iva100']);
                unset($pdf->colWidth['descto']);
                unset($pdf->colWidth['subtotal']);
                unset($pdf->colWidth['iva']);
                unset($pdf->colWidth['total']);
                unset($pdf->colWidth['Moneda']);
            }
//            if (!$with_image) {
//                unset($pdf->colWidth['imagen']);
//            }
            $pdf->SetCompensation($pdf->colWidth, $compensation);
            $pdf->headerMethod[]['SetXY'] = array($pdf->lMargin + (0), ($fullw * 0.33));
            $pdf->headerMethod[]['SetFont'] = array('Arial', 'B', 6);
            $pdf->headerMethod[]['Row'] = array($pdf->colTitle, 5, '1', 1);
//            $pdf->headerMethod[]['Cell'] = array(0, 5, 'IMAGEN', 'TRB', 1, 'C');
            $pdf->headerMethod[]['SetFont'] = array('Arial', 'I', 6);

            $pdf->AddPage();
            $totales = array();
//            $pdf->colFormat['costo'] = "%0.{$this->decimals}f";
            $pdf->colFormat['iva100'] = "%0.{$this->decimals}f";
            $pdf->colFormat['letra'] = "%s";
//            $pdf->colFormat['importe'] = "%0.{$this->decimals}f";
//            $pdf->colFormat['descto'] = "%0.{$this->decimals}f";
//            $pdf->colFormat['subtotal'] = "%0.{$this->decimals}f";
//            $pdf->colFormat['iva'] = "%0.{$this->decimals}f";
//            $pdf->colFormat['total'] = "%0.{$this->decimals}f";
            $pdf->colFormat['Moneda'] = "%s";
            foreach ($rows as $i => $row) {
                //Conversion monedas por detalles
                foreach (array('costo', 'precio', 'importe', 'descto', 'subtotal', 'iva', 'total') as $field) {
                    if (isset($row[$field]))
                        $row[$field] = $row[$field] * ($row['TipoCambio'] / $entrada['TipoCambio']);
                }
                $moneda_id = $entrada['moneda_id'];
                $Moneda = strtolower(plural($entrada['Moneda']));
                $row['Moneda'] = $Moneda;
                $TipoCambio = $entrada['TipoCambio'];

                $monedas[$moneda_id]['moneda_id'] = $moneda_id;
                $monedas[$moneda_id]['Moneda'] = $Moneda;
                $monedas[$moneda_id]['TipoCambio'] = $TipoCambio;

                if ($detailed_totals)
                    $totales[$moneda_id]['letra'] = '';
                else
                    $totales[$moneda_id]['Moneda'] = $Moneda;
                //Incrementar totales redondeando
                foreach (array('importe', 'descto', 'subtotal', 'iva', 'total') as $field) {
                    if (isset($row['moneda_id']) && isset($row[$field]))
                        $totales[$moneda_id][$field] +=round($row[$field], $this->decimals);
                }

                //Formateo a detalles
                foreach (array('costo', 'precio', 'importe', 'descto', 'subtotal', 'iva', 'total') as $field) {
                    if (isset($row[$field]))
                        $row[$field] = numberFormat(round($row[$field], $this->decimals), $this->decimals, '.', ',', 3);
                }
                $img = array('imagen' => array('sprintf' => "../archivos/jpg/%s.JPG", 'default' => ''));
                $h = $pdf->ImageRow($row, 4, $img, 35, 'LBR');
            }
            foreach ($monedas as $moneda_id => $moneda) {
                $totales[$moneda_id]['Moneda'] = ucfirst($moneda['Moneda']) . " ({$moneda['TipoCambio']})";
                //Total a letra
                $totales[$moneda_id]['letra'] = 'Letra: ' . utf8_decode(num2letras(round2($totales[$moneda_id]['total'], 2), false, true, 'un', ' ' . strtolower($moneda['Moneda']) . ' con '));
                //Formateo de totales
                foreach (array('importe', 'descto', 'subtotal', 'iva', 'total') as $field) {
                    if (isset($totales[$moneda_id][$field]))
                        $totales[$moneda_id][$field] = numberFormat(round($totales[$moneda_id][$field], $this->decimals), $this->decimals, '.', ',', 3);
                }
            }
            if ($detailed_totals) {
                $twidth['letra'] = 0;
                $twidth['importe'] = 0;
                $twidth['descto'] = 0;
                $twidth['subtotal'] = 0;
                $twidth['iva'] = 0;
                $twidth['total'] = 0;
                $twidth['Moneda'] = 0;
                foreach ($twidth as $field => $value) {
                    $twidth[$field] = $pdf->colWidth[$field];
                    $twidth['letra']+= $twidth[$field];
                    $tborder[$field] = 'LB';
                    $talign[$field] = $pdf->colAlign[$field];
                    $tstyle[$field] = '';
                    $tformat[$field] = $pdf->colFormat[$field];
                }
                $twidth['letra'] = array_sum($pdf->colWidth) - $twidth['letra'];
                $tborder['Moneda'] .= 'R';
                $talign['letra'] = 'L';
                $tstyle['letra'] = 'B';
                foreach ($monedas as $moneda_id => $moneda) {
                    $pdf->FancyRow($totales[$moneda_id], $twidth, 4, $tborder, $talign, $tstyle, $tformat);
                }
            } else {
                $m = count($totales) + 1;
                $wf = 14;
                $hf = 4;
                $x = $pdf->w - $pdf->rMargin - ($m * $wf);
                foreach (array('Moneda', 'importe', 'descto', 'subtotal', 'iva', 'total') as $field) {
                    $pdf->SetX($x);
                    $pdf->Cell($wf, $hf, $pdf->colTitle[$field] . ': ', 'LBR', 0, 'R');
                    foreach ($monedas as $moneda_id => $moneda) {
                        $pdf->Cell($wf, $hf, isset($pdf->colFormat[$field]) ? sprintf($pdf->colFormat[$field], $totales[$moneda_id][$field]) : $totales[$moneda_id][$field], 'BR', 0, $pdf->colAlign[$field]);
                    }
                    $pdf->Ln($hf);
                }
                foreach ($monedas as $moneda_id => $moneda) {
                    $pdf->MultiCell(0, $hf, $totales[$moneda_id]['letra'], 0, 'B');
                }
            }
        }
        return $this->Output($pdf, 'Entrada', $dest);
    }

    function pdfPoliza($poliza_id =0, $dest = '') {
        if (empty($poliza_id)) {
//            if(!headers_sent())
//            header_remove ('Content-type');    
            echo "No seleccionó poliza";
            return;
        }
        $title = "";
        $subject = "";

        $where_polizas = "";
        $where_bancos = "";
        $where_beneficiarios = "";
        $where_detalles_polizas = "";
        if (!empty($poliza_id)) {
            $where_polizas .= ( empty($where_polizas) ? " WHERE" : " AND") . " p.id = $poliza_id";
            $where_detalles_polizas .= ( empty($where_detalles_polizas) ? " WHERE" : " AND") . " d.poliza_id = $poliza_id";
        }

        $sql_polizas = "SELECT p.`folio`, p.`capturista_id`, p.`anio`, p.`mes`, p.`fecha`, p.`no_cheque`, p.`beneficiario_id`, p.`importe`, p.`concepto`, p.`conciliado`, p.`estatus_id`, t.`descripcion` AS tipo_de_poliza, t.`banco_id` FROM polizas p INNER JOIN tipos_de_polizas t ON p.`tipo_de_poliza_id` = t.`id`";
        $sql_sucursales = "SELECT s.`id`, s.`rfc`, s.`nomcomercial`, s.`descripcion`, s.`calle_id`, s.`numero_exterior`, s.`numero_interior`, s.`colonia_id`, s.`codigo_postal`, s.`poblacion_id`, s.`municipio_id` FROM capturistas c INNER JOIN almacenes a ON c.almacen_id = a.id INNER JOIN sucursales s ON a.sucursal_id = s.id";
        $sql_calles = "SELECT c.`id`, c.`descripcion` FROM calles c";
        $sql_colonias = "SELECT c.`id`, c.`descripcion` FROM colonias c";
        $sql_poblaciones = "SELECT p.`id`, p.`descripcion` FROM poblaciones p";
        $sql_municipios = "SELECT m.`descripcion` AS municipio, e.`descripcion` AS estado, p.`descripcion` AS pais FROM municipios m LEFT OUTER JOIN estados e ON m.estado_id=e.id LEFT OUTER JOIN paises p ON e.pais_id=p.id";
        $sql_bancos = "SELECT
  bancos.id,
  bancos.descripcion,
  bancos.no_de_cuenta,
  bancos.sucursal,
  bancos.no_de_cheque,
  bancos.saldo_inicial,
  sum(ifnull(
        saldos.cargo,
        0) -
      ifnull(
        saldos.abono,
        0))
    AS saldo
FROM
  bancos
  INNER JOIN catalogo
    ON bancos.cuenta_id = catalogo.id
  LEFT JOIN saldos
    ON saldos.cuenta_id = catalogo.id";
        $sql_beneficiarios = "SELECT b.`id`, b.`descripcion` FROM beneficiarios b";
        $sql_detalles_polizas =
                "SELECT
  c.`cta`,
  c.`scta`,
  c.`sscta`,
  c.`ssscta`,
  d.`referencia`,
  d.`concepto`,
  d.`cargo`,
  d.`abono`
FROM
  detalles_polizas d INNER JOIN catalogo c ON d.cuenta_id = c.id";
//        exit(pre($sql_polizas,true));

        $poliza = $this->query($sql_polizas . $where_polizas);
        $poliza = $poliza[0];
        if (empty($poliza)) {
            exit("poliza $poliza_id No disponible");
        }
//        echo pre($poliza, true);
        if (!empty($poliza['capturista_id'])) {
            $where_sucursales .= ( empty($where_sucursales) ? " WHERE" : " AND") . " c.id = '$poliza[capturista_id]'";
            $sucursal = $this->query($sql_sucursales . $where_sucursales);
            $sucursal = $sucursal[0];
            if (!empty($sucursal['calle_id'])) {
                foreach ($this->query($sql_calles . " WHERE c.id = '$sucursal[calle_id]'") as $calle) {
//                    foreach ($calle as $key => $value) {
                    $sucursal['calle'] = $calle['descripcion'];
//                    }
                }
            }
            if (!empty($sucursal['colonia_id'])) {
                foreach ($this->query($sql_colonias . " WHERE c.id = '$sucursal[colonia_id]'") as $colonia) {
//                    foreach ($colonia as $key => $value) {
                    $sucursal['colonia'] = $colonia['descripcion'];
//                    }
                }
            }
            if (!empty($sucursal['poblacion_id'])) {
                foreach ($this->query($sql_poblaciones . " WHERE p.id = '$sucursal[poblacion_id]'") as $poblacion) {
//                    foreach ($poblacion as $key => $value) {
                    $sucursal['poblacion'] = $poblacion['descripcion'];
//                    }
                }
            }
            if (!empty($sucursal['municipio_id'])) {
                foreach ($this->query($sql_municipios . " WHERE m.id = '$sucursal[municipio_id]'") as $municipio) {
                    foreach ($municipio as $key => $value) {
                        $sucursal[$key] = $value;
                    }
                }
            }
        }
        if (!empty($poliza['banco_id'])) {
            $where_bancos .= ( empty($where_bancos) ? " WHERE" : " AND") . " bancos.id = '$poliza[banco_id]'";
//            exit(pre($sql_bancos . $where_bancos, true));
            $banco = $this->query($sql_bancos . $where_bancos);
            $banco = $banco[0];
        }
//        exit(pre($banco, true));
        if (!empty($poliza['beneficiario_id'])) {
            $where_beneficiarios .= ( empty($where_beneficiarios) ? " WHERE" : " AND") . " b.id = '$poliza[beneficiario_id]'";
            $beneficiario = $this->query($sql_beneficiarios . $where_beneficiarios);
            $beneficiario = $beneficiario[0];
            $cheque = $banco;
            $cheque['no_de_cheque'] = $poliza['no_cheque'];
            $cheque['beneficiario'] = $beneficiario['descripcion'];
        }
//        exit(pre($sql_detalles_polizas, true));
        $rows = $this->query($sql_detalles_polizas . $where_detalles_polizas);
//        exit(pre($banco, true));
//        exit(pre($cheque, true));
//        pre($beneficiario);
//        pre($rows);
//        exit();
        $pdf = new PDF_Rilez();
        $pdf->SetProtection(array('print'));
        $pdf->SetFont('Arial', '', 6);
        $pdf->AliasNbPages();
        $pdf->SetCreator("JPGIT", true);
        $pdf->SetTopMargin(15);
        $pdf->SetAutoPageBreak(true, 25);
//        $pdf->SetFont('Arial', '', 6);
//        $pdf->configureRow($rows);
//        $pdf->configureRow($this->query($sql_polizas), TRUE);
        $fullw = ($pdf->w - $pdf->rMargin - $pdf->lMargin);
        $w2 = $fullw / 2;
        $w3 = $fullw / 3;
        $w5 = $fullw / 5;
        $w6 = $fullw / 6;
        $w7 = $fullw / 7;
        $w8 = $fullw / 8;
        $w10 = $fullw / 10;
        $w20 = $fullw / 20;

        $polizaTitle =
                array(
                    'folio' => 'FOLIO', /* folio */
                    'capturista_id' => 'CAPTURISTA ID', /* capturista_id */
                    'anio' => 'ANIO', /* anio */
                    'mes' => 'MES', /* mes */
                    'fecha' => 'FECHA', /* fecha */
                    'no_cheque' => 'NO CHEQUE', /* no_cheque */
                    'beneficiario_id' => 'BENEFICIARIO ID', /* beneficiario_id */
                    'importe' => 'IMPORTE', /* importe */
                    'concepto' => 'CONCEPTO', /* concepto */
                    'conciliado' => 'CONCILIADO', /* conciliado */
                    'estatus_id' => 'ESTATUS ID', /* estatus_id */
                    'tipo_de_poliza' => 'TIPO DE POLIZA', /* tipo_de_poliza */
                    'banco_id' => 'BANCO ID' /* banco_id */,
                    'beneficiario' => 'BENEFICIARIO'
        );


        $polizaWidth =
                array(
//                    'folio' => 9.8819666666667, /* folio */
//                    'capturista_id' => 15.871183333333, /* capturista_id */
//                    'anio' => 10.707466666667, /* anio */
//                    'mes' => 9.9983833333333, /* mes */
//                    'fecha' => 15.871183333333, /* fecha */
//                    'no_cheque' => 15.871183333333, /* no_cheque */
//                    'beneficiario_id' => 15.871183333333, /* beneficiario_id */
                    'beneficiario' => 0,
                    'importe' => 16/* 13.056966666667  importe */
//                    'concepto' => 15.871183333333, /* concepto */
//                    'conciliado' => 15.871183333333, /* conciliado */
//                    'estatus_id' => 15.871183333333, /* estatus_id */
//                    'tipo_de_poliza' => 15.871183333333, /* tipo_de_poliza */
//                    'tipo' => 9.4120666666667, /* tipo */
//                    'banco_id' => 15.871183333333 /* banco_id */
        );


        $polizaAlign =
                array(
                    'folio' => 'R', /* folio */
                    'capturista_id' => 'R', /* capturista_id */
                    'anio' => 'R', /* anio */
                    'mes' => 'R', /* mes */
                    'fecha' => 'L', /* fecha */
                    'no_cheque' => 'R', /* no_cheque */
                    'beneficiario_id' => 'R', /* beneficiario_id */
                    'importe' => 'R', /* importe */
                    'concepto' => 'L', /* concepto */
                    'conciliado' => 'L', /* conciliado */
                    'estatus_id' => 'R', /* estatus_id */
                    'tipo_de_poliza' => 'L', /* tipo_de_poliza */
                    'banco_id' => 'R' /* banco_id */
        );

        $importeWidth['label'] = $fullw - $polizaWidth['fecha'] - $polizaWidth['importe'];
        $importeWidth['importe'] = $polizaWidth['importe'];
        $importeWidth['fecha'] = $polizaWidth['fecha'];
        $importeBorder['label'] = "B";
        $importeBorder['importe'] = "LB";
        $importeBorder['fecha'] = "LB";
        $importeAlign['label'] = "L";
        $importeAlign['importe'] = $polizaAlign['importe'];
        $importeAlign['fecha'] = $polizaAlign['fecha'];
        $cellh = 4;
        $chequeTitle = array(
            'cta' => 'CTA', /* cta */
            'scta' => 'SCTA', /* scta */
            'sscta' => 'SSCTA', /* sscta */
            'ssscta' => 'SSSCTA', /* ssscta */
            'descripcion' => 'BANCO', /* descripcion */
            'no_de_cuenta' => 'NO DE CUENTA', /* no_de_cuenta */
            'sucursal' => 'SUCURSAL', /* sucursal */
            'no_de_cheque' => 'NO DE CHEQUE' /* no_de_cheque */
        );
        $chequeWidth = array(
//                        'cta' => 11, /* cta */
//                        'scta' => 11, /* scta */
//                        'sscta' => 11, /* sscta */
//                        'ssscta' => 11, /* ssscta */
            'descripcion' => $w8, /* descripcion */
            'sucursal' => $w8, /* sucursal */
            'no_de_cuenta' => $w8, /* no_de_cuenta */
            'no_de_cheque' => $w8 /* no_de_cheque */
        );
        $chequeAlign = array(
            'cta' => 'R', /* cta */
            'scta' => 'R', /* scta */
            'sscta' => 'R', /* sscta */
            'ssscta' => 'R', /* ssscta */
            'descripcion' => 'L', /* descripcion */
            'no_de_cuenta' => 'R', /* no_de_cuenta */
            'sucursal' => 'L', /* sucursal */
            'no_de_cheque' => 'R' /* no_de_cheque */
        );
        $chequeBorder = array(
            'cta' => '', /* cta */
            'scta' => '', /* scta */
            'sscta' => '', /* sscta */
            'ssscta' => '', /* ssscta */
            'descripcion' => '', /* descripcion */
            'no_de_cuenta' => '', /* no_de_cuenta */
            'sucursal' => '', /* sucursal */
            'no_de_cheque' => '' /* no_de_cheque */
        );
        $pdf->headerMethod[]['SetFont'] = array('Arial', 'B');
//        $pdf->headerMethod[]['Image'] = array('logo_rilez.gif', $pdf->lMargin + ($fullw * 0.3), 1, ($fullw * 0.4), 13);
//        $pdf->headerMethod[]['SetY'] = array(($fullw * 0.07));
//        $pdf->headerMethod[]['Cell'] = array(0, 3, utf8_decode($title), '0', 1, 'C');
//        $pdf->headerMethod[]['Cell'] = array(0, 3, utf8_decode($subject), '0', 1, 'C');
//        $pdf->headerMethod[]['SetXY'] = array($pdf->lMargin + (0), ($fullw * 0.10));
        $pdf->headerMethod[]['RoundedRect'] = array($pdf->lMargin, $pdf->tMargin, $fullw, 24, 2, '1234');
//        $pdf->headerMethod[]['Row'] = array(array('label' => "", 'fecha' => "FECHA"), 4, $polizaBorder, '1', array('label' => "", 'importe' => $polizaWidth['importe']), $polizaAlign);
//        $n = sscanf($poliza['fecha'], "%4s-%2s-%2s", $year, $month, $day);
//        $pdf->headerMethod[]['Row'] = array(array('label' => utf8_decode(num2letras(round2($poliza['importe'], 2), false, true, 'un', ' ' . strtolower('pesos'), " M.N."), 'importe' => $poliza['importe'], 'fecha' => date_format(date_create($poliza['fecha']), 'd/m/Y')), 4, $importeBorder, '1', $importeWidth, $importeAlign));
        if ($cheque) {
            $pdf->headerMethod[]['SetX'] = array($pdf->lMargin + $w7 * 6);
            $pdf->headerMethod[]['Cell'] = array($w8, 4, date_format(date_create($poliza['fecha']), 'd/m/Y'), '', 1, 'R');
            $pdf->headerMethod[]['Cell'] = array($w5 * 4, 4, $cheque['beneficiario'], '', 0, 'L');
//            NumberFormatter::create('es_MX', NumberFormatter::CURRENCY)->formatCurrency($poliza['importe'], 'MXP');
//            $fmt = new NumberFormatter( 'de_DE', NumberFormatter::CURRENCY );
//            echo $fmt->formatCurrency(1234567.891234567890000, "EUR")."\n";
//            echo $fmt->formatCurrency(1234567.891234567890000, "RUR")."\n";
//            if (intl_is_failure(numfmt_get_error_code($fmt))) {
//                report_error("Formatter error");
//            }
//            echo number_format($poliza['importe'], 2);
            $pdf->headerMethod[]['Cell'] = array($w7, 4, "$ " . numberFormat($poliza['importe'], 2), '', 1, 'R');
            $pdf->headerMethod[]['Cell'] = array($w5 * 4, 4, "Son: ( " . utf8_decode(num2letras(round2($poliza['importe'], 2), false, true, 'un', ' ' . strtolower('pesos'), " M.N.")) . " )", '', 1, 'L');
            $pdf->headerMethod[]['Ln'] = array();
            $pdf->headerMethod[]['Row'] = array(
                $chequeTitle,
                $cellh,
                $chequeBorder,
                1,
                $chequeWidth,
                $chequeAlign
            );
            $pdf->headerMethod[]['Row'] = array(
                $cheque,
                $cellh,
                $chequeBorder,
                1,
                $chequeWidth,
                $chequeAlign
            );
        } elseif (empty($poliza['beneficiario_id'])) {
            $domicilio = "" . $sucursal['calle'] . " " . $sucursal['numero_exterior'] . ($sucursal['numero_interior'] ? "-" . $sucursal['numero_interior'] : "") . "\n"
                    . "" . $sucursal['colonia'] . " " . $sucursal['codigo_postal'] . "\n"
                    . $sucursal['municipio'] . " " . $sucursal['estado'] . " " . $sucursal['pais'] . "\n";

            $pdf->headerMethod[]['Cell'] = array($w7 * 6, 24,
                $sucursal['nomcomercial'] . "\n"
                . $sucursal['descripcion'] . "\n"
                . $sucursal['rfc'] . "\n"
                . $domicilio, '', 0, 'L');
            $pdf->headerMethod[]['Cell'] = array($w8, 4, date_format(date_create($poliza['fecha']), 'd/m/Y'), '', 1, 'R');
        }
        $pdf->headerMethod[]['SetXY'] = array($pdf->lMargin, 40);
        $pdf->headerMethod[]['RoundedRect'] = array($pdf->lMargin, 40, $w2 - 1, 19, 2, '1234');
        $pdf->headerMethod[]['RoundedRect'] = array($pdf->lMargin + $w2 + 1, 40, $w2 - 1, 19, 2, '1234');
        $pdf->headerMethod[]['Row'] = array(
            array('concepto' => "CONCEPTO DE LA POLIZA\n" . ($poliza['concepto']), 'atentamente' => "ATENTAMENTE\n"),
            $cellh,
            array('concepto' => '', 'atentamente' => ''),
            0,
            array('concepto' => $w2, 'atentamente' => $w2),
            array('concepto' => 'J', 'atentamente' => 'C')
        );
        $pdf->headerMethod[]['SetXY'] = array($pdf->lMargin, 60);

        $pdf->colTitle =
                array(
                    'cta' => 'CTA', /* cta */
                    'scta' => 'SCTA', /* scta */
                    'sscta' => 'SSCTA', /* sscta */
                    'ssscta' => 'SSSCTA', /* ssscta */
                    'referencia' => 'REFERENCIA', /* referencia */
                    'concepto' => 'CONCEPTO', /* concepto */
                    'cargo' => 'CARGO', /* cargo */
                    'abono' => 'ABONO' /* abono */
        );


        $pdf->colWidth =
                array(
                    'cta' => 23, /* cta */
//                    'scta' => 11, /* scta */
//                    'sscta' => 11, /* sscta */
//                    'ssscta' => 11, /* ssscta */
                    'referencia' => 16.117666666667, /* referencia */
                    'concepto' => 15.408583333333, /* concepto */
                    'cargo' => 24, /* cargo */
                    'abono' => 24 /* abono */
        );


        $pdf->colAlign =
                array(
                    'cta' => 'L', /* cta */
                    'scta' => 'R', /* scta */
                    'sscta' => 'R', /* sscta */
                    'ssscta' => 'R', /* ssscta */
                    'referencia' => 'L', /* referencia */
                    'concepto' => 'L', /* concepto */
                    'cargo' => 'R', /* cargo */
                    'abono' => 'R' /* abono */
        );


        $colBorder =
                array(
                    'cta' => 'B', /* cta */
                    'scta' => 'B', /* scta */
                    'sscta' => 'B', /* sscta */
                    'ssscta' => 'B', /* ssscta */
                    'referencia' => 'B', /* referencia */
                    'concepto' => 'B', /* concepto */
                    'cargo' => 'B', /* cargo */
                    'abono' => 'B' /* abono */
        );
        $compensation['concepto'] = 1;
        $pdf->SetCompensation($pdf->colWidth, $compensation);

        $pdf->headerMethod[]['RoundedRect'] = array($pdf->lMargin, 60, $fullw, $pdf->h - $pdf->bMargin - 60 + 4, 2, '1234');
        $x = $pdf->lMargin;
//        $xf = $pdf->colWidth['abono'];
//        unset($pdf->colWidth['abono']);
        foreach ($pdf->colWidth as $field => $w) {
            $x+=$w;
            if ($field != "abono")
                $pdf->headerMethod[]['Line'] = array($x, 60, $x, $pdf->h - $pdf->bMargin);
//            $pdf->headerMethod[]['RoundedRect'] = array($pdf->lMargin, 50, $fullw, $pdf->h - $pdf->bMargin - 50 + 4, 2, '1234');
        }
//        $pdf->colWidth['abono'] = $xf;
        $pdf->headerMethod[]['SetFont'] = array('Arial', 'B', 6);
        $pdf->headerMethod[]['Row'] = array($pdf->colTitle, 5, $colBorder, 1);
        $pdf->headerMethod[]['SetFont'] = array('Arial', 'I', 6);

        $pdf->footerMethod[]['SetY'] = array(- $pdf->bMargin);
        $pdf->footerMethod[]['FooterTotales'] = array();
        $pdf->footerMethod[]['SetY'] = array($pdf->h - 15);

//        $pdf->AddPage();
        $pdf->footerMethod[]['RoundedRect'] = array($pdf->lMargin, $pdf->h - 15, $fullw, 10, 2, '1234');
        $pdf->footerMethod[]['Cell'] = array(($w7), 5, utf8_decode("Formuló\n"), '', 0, 'C');
        $pdf->footerMethod[]['Cell'] = array(($w7), 5, utf8_decode("Revisó\n"), 'L', 0, 'C');
        $pdf->footerMethod[]['Cell'] = array(($w7), 5, utf8_decode("Autorizó\n"), 'L', 0, 'C');
        $pdf->footerMethod[]['Cell'] = array(($w7), 5, utf8_decode("Aut. Asiento\n"), 'L', 0, 'C');
        $pdf->footerMethod[]['Cell'] = array(($w7), 5, utf8_decode("Operó\n Aux"), 'L', 0, 'C');
//        $pdf->footerMethod[]['Cell'] = array(($w7), 5, utf8_decode("Pasó al\n Diario"), 'L', 0, 'C');
        $pdf->footerMethod[]['Cell'] = array(($w7 * 2), 5, utf8_decode("Póliza\n"), 'L', 1, 'C');
        $pdf->footerMethod[]['Cell'] = array(($w7), 5, utf8_decode(""), 'T', 0, 'C');
        $pdf->footerMethod[]['Cell'] = array(($w7), 5, utf8_decode(""), 'LT', 0, 'C');
        $pdf->footerMethod[]['Cell'] = array(($w7), 5, utf8_decode(""), 'LT', 0, 'C');
        $pdf->footerMethod[]['Cell'] = array(($w7), 5, utf8_decode(""), 'LT', 0, 'C');
        $pdf->footerMethod[]['Cell'] = array(($w7), 5, utf8_decode(""), 'LT', 0, 'C');
        $pdf->footerMethod[]['Cell'] = array(($w7), 5, utf8_decode($poliza['tipo_de_poliza']), 'LT', 0, 'C');
        $pdf->footerMethod[]['Cell'] = array(($w7), 5, utf8_decode($poliza['folio']), 'LT', 1, 'C');
//            $pdf->footerMethod[]['Cell'] = array($w5, 5, '', '', 0, 'C');
        $pdf->footerMethod[]['SetY'] = array(-5);
        $pdf->footerMethod[]['Page'] = array(($fullw), 5);
//        $pdf->footerMethod[]['Row'] = array
//        $pdf->FooterPoliza
//        $pdf->footerMethod[]['FooterPoliza'] = array($poliza, $w8);
        $pdf->AddPage();

        $pdf->totalesWidth['label'] = $fullw - $pdf->colWidth['abono'] - $pdf->colWidth['cargo'];
        $pdf->totalesWidth['cargo'] = $pdf->colWidth['cargo'];
        $pdf->totalesWidth['abono'] = $pdf->colWidth['abono'];
        $pdf->totalesBorder['label'] = "T";
        $pdf->totalesBorder['cargo'] = "TL";
        $pdf->totalesBorder['abono'] = "TL";
        $pdf->totalesAlign['label'] = "R";
        $pdf->totalesAlign['cargo'] = $pdf->colAlign['cargo'];
        $pdf->totalesAlign['abono'] = $pdf->colAlign['abono'];
        if (!empty($rows)) {

            $pdf->colFormat['cta'] = "%'0+4s";
            $pdf->colFormat['scta'] = "%'0+4s";
            $pdf->colFormat['sscta'] = "%'0+4s";
            $pdf->colFormat['ssscta'] = "%'0+4s";
            $pdf->colFormat['cargo'] = "%0.{$this->decimals}f";
            $pdf->colFormat['abono'] = "%0.{$this->decimals}f";
            $pdf->totales = array();
            $pdf->totales['label'] = "Sumas Iguales";
            foreach ($rows as $i => $row) {
                $pdf->totales['cargo']+=$row['cargo'];
                $pdf->totales['abono']+=$row['abono'];
                $cta_a = array(
                    $row['cta'],
                    $row['scta'],
                    $row['sscta'],
                    $row['ssscta']
                );

                $row['concepto'] = $this->descripcionCuentas($this->sqlSelectCuentasCatalogo($this->cuentasPrecedentes($cta_a, 1)), '[', ']') .
                        "\n" .
                        $row['concepto'];
                $s = 0;
                $cta_s = '';
                for ($s = 0, $cta = str_repeat('s', $s) . "cta"; isset($row[$cta]); $s++, $cta = str_repeat('s', $s) . "cta") {
                    $cta_s = ($cta_s ? $cta_s . '.' : '') . sprintf("%'0+4s", $row[$cta]);
                }
                $row['cta'] = $cta_s;
                //Formateo a detalles
                foreach (array('cargo', 'abono') as $field) {
                    if (isset($row[$field]))
                        $row[$field] = numberFormat(round($row[$field], $this->decimals), $this->decimals, '.', ',', 3);
                }
                $h = $pdf->Row($row, 4, $colBorder, '1');
            }
//            $totales['cargo'] = 1.00;
//            $totales['abono'] = 1.00;
//            $pdf->SetY( $pdf->GetH() - $pdf->bMargin - 5);
//            $h = $pdf->Row($this->totales, 4, $totalesBorder, '1', $totalesWidth, $totalesAlign);
        }
//        $pdf->Output('Almacen.pdf','D');
        return $this->Output($pdf, 'Poliza', $dest);
    }

    function pdfBalanzaComprobacion($anio=2011, $mes=9, $cuenta_inicial="1000.0.0.0", $cuenta_final="7000.0.0.0", $s=1, $dest='I') {
        $where_balanza_comprobacion = " WHERE saldos.anio = '$anio' AND saldos.mes <= '$mes'";
        $cuenta_inicial = explode(".", $cuenta_inicial);
        $cuenta_final = explode(".", $cuenta_final);
        if (is_array($cuenta_inicial) && is_array($cuenta_final)) {
            $li = count($cuenta_inicial);
            $lf = count($cuenta_final);
            if ($li != 4)
                die("Formato (#.#.#.#) de cuenta inicial incorrecto");
            if ($lf != 4)
                die("Formato (#.#.#.#) de cuenta final incorrecto");
//            if(!($li = 4 && $li == $lf )) die("Formato (#.#.#.#) de cuentas incorrecto");
            //La cuenta inicial debe ser menor a la final
            if ($cuenta_final[0] < $cuenta_inicial[0]) {
                $aux = $cuenta_final;
                $cuenta_final = $cuenta_inicial;
                $cuenta_inicial = $aux;
            }
            //formateo a 4 digitos
            foreach ($cuenta_inicial as $i => $value) {
                $cuenta_inicial[$i] = sprintf("%'0+4s", $value);
            }
            foreach ($cuenta_final as $i => $value) {
                $cuenta_final[$i] = sprintf("%'0+4s", $value);
            }
//            pre($cuenta_inicial);
//            pre($cuenta_final);
//            $where_balanza_comprobacion.= ( empty($where_balanza_comprobacion) ? " WHERE " : " AND ") . "cta != '0";
//            $s = 1, 
            for ($cta = str_repeat('s', $s) . "cta"; $s < 4; $s++, $cta = str_repeat('s', $s) . "cta") {
                $where_balanza_comprobacion.= ( empty($where_balanza_comprobacion) ? " WHERE " : " AND ") . "$cta = '0'";
            }
            $where_balanza_comprobacion.= ( empty($where_balanza_comprobacion) ? " WHERE" : " AND") . " CONVERT(CONCAT(LPAD(cta, 4, '0'), LPAD(scta, 4, '0'), LPAD(sscta, 4, '0'), LPAD(ssscta, 4, '0')), UNSIGNED INTEGER) between " . implode("", $cuenta_inicial) . " AND " . implode("", $cuenta_final) . "";
        }
        $sql_balanza_comprobacion = "
 SELECT -- CONVERT(CONCAT(LPAD(cta, 4, '0'), LPAD(scta, 4, '0'), LPAD(sscta, 4, '0'), LPAD(ssscta, 4, '0')), UNSIGNED INTEGER) AS cta,
--  catalogo.id,
  catalogo.cta,
  catalogo.scta,
  catalogo.sscta,
  catalogo.ssscta,
  afectable,
  descripcion,
--  edo_fin,
--  naturaleza,
--  ifnull(saldos.anio,0) AS anio,
--  ifnull(saldos.mes,0) AS mes,
-- concat(ifnull(saldos.anio,0),'-',ifnull(saldos.mes,0)),
--  STR_TO_DATE(concat(ifnull('$anio',0),'-',ifnull('$mes',0),'-','01'), '%Y-%m-%d') AS de,
--  LAST_DAY(STR_TO_DATE(concat(ifnull('$anio',0),'-',ifnull('$mes',0)), '%Y-%m')) AS hasta,
sum(IF(
        catalogo.naturaleza = 'D' && ifnull(saldos.mes,0) != '$mes',
        ifnull(saldos.cargo,0) - ifnull(saldos.abono,0),
        0))
    AS deudorai,
  sum(IF(
        catalogo.naturaleza = 'A' && ifnull(saldos.mes,0) != '$mes',
        ifnull(saldos.abono,0) - ifnull(saldos.cargo,0),
        0))
    AS acreedorai,
  sum(if(ifnull(saldos.mes,0) = '$mes',ifnull(saldos.cargo,0),0)) AS cargos,
  sum(if(ifnull(saldos.mes,0) = '$mes',ifnull(saldos.abono,0),0)) AS abonos,
  sum(IF(
        catalogo.naturaleza = 'D',
        ifnull(saldos.cargo,0) - ifnull(saldos.abono,0),
        0))
    AS deudoraf,
  sum(IF(
        catalogo.naturaleza = 'A',
        ifnull(saldos.abono,0) - ifnull(saldos.cargo,0),
        0))
    AS acreedoraf
FROM
  catalogo LEFT JOIN saldos ON saldos.cuenta_id = catalogo.id$where_balanza_comprobacion
GROUP BY
  catalogo.cta,
  catalogo.scta,
  catalogo.sscta,
  catalogo.ssscta,
  saldos.anio";
//        die(pre ($sql_balanza_comprobacion,true));
        $rows = $this->query($sql_balanza_comprobacion, TRUE);
//        die(pre($rows, TRUE));
        $pdf = new PDF_Rilez();
        $pdf->AliasNbPages();
        $pdf->SetCreator("JPGIT", true);
        $pdf->SetProtection(array('print'));
        $pdf->SetTopMargin(15);
        $pdf->SetAutoPageBreak(true, 25);
        $pdf->SetFont('Arial', '', 6);

        $fullw = ($pdf->w - $pdf->rMargin - $pdf->lMargin);
        $w2 = $fullw / 2;
        $cellh = 4;
//        $pdf->configureRow($rows, TRUE);
        $pdf->colTitle =
                array(
                    'cta' => 'CTA', /* cta */
                    'afectable' => 'AFECTABLE', /* afectable */
                    'descripcion' => 'DESCRIPCION', /* descripcion */
                    'deudorai' => 'DEUDORAI', /* deudorai */
                    'acreedorai' => 'ACREEDORAI', /* acreedorai */
                    'cargos' => 'CARGOS', /* cargos */
                    'abonos' => 'ABONOS', /* abonos */
                    'deudoraf' => 'DEUDORAF', /* deudoraf */
                    'acreedoraf' => 'ACREEDORAF' /* acreedoraf */
        );

        $pdf->colWidth =
                array(
                    'cta' => 23, /* 52.95 cta */
//		'afectable' => 15, /*14.59 afectable */
                    'descripcion' => 52.95, /* descripcion */
                    'deudorai' => 17, /* 14.94 deudorai */
                    'acreedorai' => 17, /* 16.71 acreedorai */
                    'cargos' => 14, /* 12.94 cargos */
                    'abonos' => 14, /* 13.53 abonos */
                    'deudoraf' => 17, /* 15.06 deudoraf */
                    'acreedoraf' => 17 /* 16.82 acreedoraf */
        );

        $pdf->colAlign =
                array(
                    'cta' => 'L', /* cta */
                    'afectable' => 'L', /* afectable */
                    'descripcion' => 'L', /* descripcion */
                    'deudorai' => 'R', /* deudorai */
                    'acreedorai' => 'R', /* acreedorai */
                    'cargos' => 'R', /* cargos */
                    'abonos' => 'R', /* abonos */
                    'deudoraf' => 'R', /* deudoraf */
                    'acreedoraf' => 'R' /* acreedoraf */
        );

        $pdf->colBorder =
                array(
                    'cta' => 'B', /* cta */
                    'afectable' => 'B', /* afectable */
                    'descripcion' => 'B', /* descripcion */
                    'deudorai' => 'B', /* deudorai */
                    'acreedorai' => 'B', /* acreedorai */
                    'cargos' => 'B', /* cargos */
                    'abonos' => 'B', /* abonos */
                    'deudoraf' => 'B', /* deudoraf */
                    'acreedoraf' => 'B' /* acreedoraf */
        );
        $compensation['descripcion'] = 1;
        $pdf->SetCompensation($pdf->colWidth, $compensation);
        $pdf->headerMethod[]['Image'] = array('logo_rilez.gif', $pdf->lMargin + ($fullw * 0.3), 1, ($fullw * 0.4), 13);
        $pdf->headerMethod[]['RoundedRect'] = array($pdf->lMargin, $pdf->tMargin, $fullw, $pdf->h - $pdf->tMargin - $pdf->bMargin, 2, '1234');
        $pdf->headerMethod[]['RoundedRect'] = array($pdf->lMargin, $pdf->tMargin, $fullw, $pdf->h - $pdf->tMargin - $pdf->bMargin, 2, '1234');
        $x = $pdf->lMargin;
        foreach ($pdf->colWidth as $field => $w) {
            $x+=$w;
            if ($field != "acreedoraf")
                $pdf->headerMethod[]['Line'] = array($x, $pdf->tMargin, $x, $pdf->h - $pdf->bMargin);
        }
        $pdf->headerMethod[]['SetFont'] = array('Arial', 'B', 6);
        $pdf->headerMethod[]['Row'] = array($pdf->colTitle, 5, $pdf->colBorder, 1);
        $pdf->headerMethod[]['SetFont'] = array('Arial', 'I', 6);

//        $pdf->footerMethod[]['SetY'] = array( $this->h - $this->bMargin);
//        $pdf->footerMethod[]['SetY'] = array( -20 );
//        $pdf->footerMethod[]['FooterTotales'] = array();
//        $pdf->footerMethod[]['SetY'] = array($pdf->h - 15);
        $pdf->footerMethod[]['SetY'] = array(-5);
        $pdf->footerMethod[]['Page'] = array(($fullw), 5);

        $pdf->totalesWidth['label'] = 0;
        $pdf->totalesBorder['label'] = "B";
        $pdf->totalesAlign['label'] = "R";
        foreach (array('deudorai', 'acreedorai', 'cargos', 'abonos', 'deudoraf', 'acreedoraf') as $field) {
            if (isset($pdf->colWidth[$field])) {
                $pdf->totalesWidth[$field] = $pdf->colWidth[$field];
                $pdf->totalesAlign[$field] = $pdf->colAlign[$field];
                $pdf->totalesBorder[$field] = "B";
            }
        }
        $pdf->totalesWidth['label'] = $fullw - array_sum($pdf->totalesWidth);
        $pdf->totales = array();
        $pdf->AddPage();
        if (!empty($rows)) {
            $co = array();
            $cm = array();
            $cc = array();
            $o = 0;
            $m = 0;
            $c = 0;
            $impresion = array();
            foreach ($rows as $row) {
//                $pdf->colFormat['cta'] = "%'0+4s";
//                $pdf->colFormat['scta'] = "%'0+4s";
//                $pdf->colFormat['sscta'] = "%'0+4s";
//                $pdf->colFormat['ssscta'] = "%'0+4s";
//            $pdf->colFormat['cargo'] = "%0.{$this->decimals}f";
//            $pdf->colFormat['abono'] = "%0.{$this->decimals}f";
//                if(strtoupper($row['afectable']) == 'S'){
//                    $pdf->totales['label'] = "Sumas iguales cta :" . $row['cta'];
//                    foreach (array('deudorai', 'acreedorai', 'cargos', 'abonos', 'deudoraf', 'acreedoraf') as $field) {
//                        if (isset($row[$field])) {
//                            $pdf->totales[$field] += $row[$field];
//                            $cm[$field] += $row[$field];
//                            $co[$field] += $row[$field];
//                        }
//                    }
//                }else{
//                $cta=$row['cta'];
                if (empty($row['scta']) && empty($row['sscta']) && empty($row['ssscta'])) {
//                            else if($row['scta'] == '0' && $row['sscta'] == '0' && $row['ssscta'] == '0'){
                    $mod_o = ($row['cta'] % 1000);
                    if ($mod_o == 0) {
                        if ($o > 0) {
                            $impresion = $co[$o];
                            //Formateo a impresion
                            foreach (array('deudorai', 'acreedorai', 'cargos', 'abonos', 'deudoraf', 'acreedoraf') as $field) {
                                if (isset($impresion[$field]))
                                    $impresion[$field] = numberFormat(round($impresion[$field], $this->decimals), $this->decimals, '.', ',', 3);
                            }
        $pdf->totalesAlign['label'] = "L";
//                        $pdf->FooterTotales($cellh, 1, array(), $impresion, array(), array());
                            $pdf->FancyRow($impresion, $pdf->totalesWidth, $cellh, $pdf->totalesBorder, $pdf->totalesAlign, array(), $pdf->colFormat);
                        }
                        $o = $row['cta'];
                        $co[$o] = array();
                        $co[$o]['label'] = $row['descripcion'] . " :Sumas Iguales";
                        foreach (array('deudorai', 'acreedorai', 'cargos', 'abonos', 'deudoraf', 'acreedoraf') as $field) {
                            if (isset($row[$field])) {
                                $co[$o][$field] += $row[$field];
                            }
                        }
                    } else {
                        $mod_m = ($mod_o % 100);
                        if ($mod_m == 0) {
                            if ($m > 0) {
                                $impresion = $cm[$m];
                                //Formateo a impresion
                                foreach (array('deudorai', 'acreedorai', 'cargos', 'abonos', 'deudoraf', 'acreedoraf') as $field) {
                                    if (isset($impresion[$field]))
                                        $impresion[$field] = numberFormat(round($impresion[$field], $this->decimals), $this->decimals, '.', ',', 3);
                                }
        $pdf->totalesAlign['label'] = "R";
//                            $pdf->FooterTotales($cellh, 1, array(), $impresion, array(), array());
                                $pdf->FancyRow($impresion, $pdf->totalesWidth, $cellh, $pdf->totalesBorder, $pdf->totalesAlign, array(), $pdf->colFormat);
                            }
                            $m = $row['cta'];
                            $cm[$m] = array();
                            $cm[$m]['label'] = $row['descripcion'] . " :Sumas Iguales";
                            foreach (array('deudorai', 'acreedorai', 'cargos', 'abonos', 'deudoraf', 'acreedoraf') as $field) {
                                if (isset($row[$field])) {
                                    $cm[$m][$field] += $row[$field];
                                }
                            }
                        }/* else {
                            $c = $row['cta'];
                            $cc[$c] = array();
                            $cc[$c]['label'] = $row['descripcion'] . " :Sumas Iguales";
                            foreach (array('deudorai', 'acreedorai', 'cargos', 'abonos', 'deudoraf', 'acreedoraf') as $field) {
                                if (isset($row[$field])) {
                                    $cc[$c][$field] += $row[$field];
                                }
                            }
                            if ($c > 0) {
                                $impresion = $cc[$c];
                                //Formateo a impresion
                                foreach (array('deudorai', 'acreedorai', 'cargos', 'abonos', 'deudoraf', 'acreedoraf') as $field) {
                                    if (isset($impresion[$field]))
                                        $impresion[$field] = numberFormat(round($impresion[$field], $this->decimals), $this->decimals, '.', ',', 3);
                                }
//                            $pdf->FooterTotales($cellh, 1, array(), $impresion, array(), array());
                                $pdf->FancyRow($impresion, $pdf->totalesWidth, $cellh, $pdf->totalesBorder, $pdf->totalesAlign, array(), $pdf->colFormat);
                            }
                        }*/
                    }
                }
                //Formateo a detalles
                foreach (array('deudorai', 'acreedorai', 'cargos', 'abonos', 'deudoraf', 'acreedoraf') as $field) {
                    if (isset($row[$field]))
                        $row[$field] = numberFormat(round($row[$field], $this->decimals), $this->decimals, '.', ',', 3);
                }
//                $row['cta'] = ($row['cta']?$row['cta']:'') . ($row['scta']?'.' . $row['scta']:'') . ($row['sscta']?'.' . $row['sscta']:'') . ($row['ssscta']?'.' . $row['ssscta']:'');
                $s = 0;
                $cta_s = '';
                for ($cta = str_repeat('s', $s) . "cta"; isset($row[$cta]); $s++, $cta = str_repeat('s', $s) . "cta") {
                    $cta_s = ($cta_s ? $cta_s . '.' : '') . sprintf("%'0+4s", $row[$cta]);
                }
                $row['cta'] = $cta_s;
                $h = $pdf->Row($row, 4, $pdf->colBorder, '1');
//                $pdf->FancyRow($row, $pdf->colWidth, $cellh, $pdf->colBorder, $pdf->colAlign, array(), $pdf->colFormat);
            }
            if ($m > 0) {
                $impresion = $cm[$m];
                //Formateo a impresion
                foreach (array('deudorai', 'acreedorai', 'cargos', 'abonos', 'deudoraf', 'acreedoraf') as $field) {
                    if (isset($impresion[$field]))
                        $impresion[$field] = numberFormat(round($impresion[$field], $this->decimals), $this->decimals, '.', ',', 3);
                }
        $pdf->totalesAlign['label'] = "L";
//                $pdf->FooterTotales($cellh, 1, array(), $impresion, array(), array());
                $pdf->FancyRow($impresion, $pdf->totalesWidth, $cellh, $pdf->totalesBorder, $pdf->totalesAlign, array(), $pdf->colFormat);
            }
            if ($o > 0) {
                $impresion = $co[$o];
                //Formateo a impresion
                foreach (array('deudorai', 'acreedorai', 'cargos', 'abonos', 'deudoraf', 'acreedoraf') as $field) {
                    if (isset($impresion[$field]))
                        $impresion[$field] = numberFormat(round($impresion[$field], $this->decimals), $this->decimals, '.', ',', 3);
                }
        $pdf->totalesAlign['label'] = "R";
//                $pdf->FooterTotales($cellh, 1, array(), $impresion, array(), array());
                $pdf->FancyRow($impresion, $pdf->totalesWidth, $cellh, $pdf->totalesBorder, $pdf->totalesAlign, array(), $pdf->colFormat);
            }
        }else {
            $pdf->Cell(0, $cellh, "Sin resultados", 0, 1, 'C');
        }
        return $this->Output($pdf, 'BalanzaComprobacion', $dest);
    }

    function pdfAuxiliar($de='2011-09-01', $hasta='2011-09-30', $cuenta_inicial="1120.1.0.0", $cuenta_final="1200.0.0.0", $dest='I') {
        $cuenta_inicial = explode(".", $cuenta_inicial);
        $cuenta_final = explode(".", $cuenta_final);
        if (is_array($cuenta_inicial) && is_array($cuenta_final)) {
            $l = count($cuenta_inicial);
            if (!($l = 4 && $l == count($cuenta_final)))
                die("Formato (#.#.#.#) de cuentas incorrecto");
            //La cuenta inicial debe ser menor a la final
            if ($cuenta_final[0] < $cuenta_inicial[0]) {
                $aux = $cuenta_final;
                $cuenta_final = $cuenta_inicial;
                $cuenta_inicial = $aux;
            }
            //formateo a 4 digitos
            foreach ($cuenta_inicial as $i => $value) {
                $cuenta_inicial[$i] = sprintf("%'0+4s", $value);
            }
            foreach ($cuenta_final as $i => $value) {
                $cuenta_final[$i] = sprintf("%'0+4s", $value);
            }
//            pre($cuenta_inicial);
//            pre($cuenta_final);
            $where_auxiliar = '';
            $where_auxiliar.= ( empty($where_auxiliar) ? " WHERE " : " AND ") . "polizas.anio = YEAR('$hasta') AND polizas.fecha <= '$hasta'";

            $where_auxiliar.= ( empty($where_auxiliar) ? " WHERE" : " AND") . " CONVERT(CONCAT(LPAD(cta, 4, '0'), LPAD(scta, 4, '0'), LPAD(sscta, 4, '0'), LPAD(ssscta, 4, '0')), UNSIGNED INTEGER) between " . implode("", $cuenta_inicial) . " AND " . implode("", $cuenta_final) . "";
        }
        $sql_auxiliar =
                "SELECT-- CONVERT(CONCAT(LPAD(cta, 4, '0'), LPAD(scta, 4, '0'), LPAD(sscta, 4, '0'), LPAD(ssscta, 4, '0')), UNSIGNED INTEGER) AS cta,
--  catalogo.id,
  catalogo.cta,
  catalogo.scta,
  catalogo.sscta,
  catalogo.ssscta,
  afectable,
  polizas.id, detalles_polizas.mov,
  catalogo.descripcion,
  detalles_polizas.concepto,
--  '2011-09-01' AS de,
--  '2011-09-01' AS hasta,
  sum(IF(
        catalogo.naturaleza = 'D' &&
        polizas.fecha < '$de',
        ifnull(
          detalles_polizas.cargo,
          0) -
        ifnull(
          detalles_polizas.abono,
          0),
        0))
    AS deudorai,
  sum(IF(
        catalogo.naturaleza = 'A' &&
        polizas.fecha < '$de',
        ifnull(
          detalles_polizas.abono,
          0) -
        ifnull(
          detalles_polizas.cargo,
          0),
        0))
    AS acreedorai,
  sum(if(
        '$de' <= polizas.fecha AND polizas.fecha <= '$hasta',
        ifnull(
          detalles_polizas.cargo,
          0),
        0))
    AS cargos,
  sum(if(
        '$de' <= polizas.fecha AND polizas.fecha <= '$hasta',
        ifnull(
          detalles_polizas.abono,
          0),
        0))
    AS abonos,
  sum(IF(
        catalogo.naturaleza = 'D',
        ifnull(
          detalles_polizas.cargo,
          0) -
        ifnull(
          detalles_polizas.abono,
          0),
        0))
    AS deudoraf,
  sum(IF(
        catalogo.naturaleza = 'A',
        ifnull(
          detalles_polizas.abono,
          0) -
        ifnull(
          detalles_polizas.cargo,
          0),
        0))
    AS acreedoraf
FROM
  detalles_polizas
  INNER JOIN polizas
    ON detalles_polizas.poliza_id = polizas.id
  INNER JOIN catalogo
    ON detalles_polizas.cuenta_id = catalogo.id
    $where_auxiliar
    GROUP BY catalogo.cta, catalogo.scta, catalogo.sscta, catalogo.ssscta, polizas.fecha, polizas.id, detalles_polizas.mov";
//        die(pre($sql_auxiliar, true));
        $rows = $this->query($sql_auxiliar);
//        die(pre($rows,TRUE));
        $pdf = new PDF_Rilez();
        $pdf->AliasNbPages();
        $pdf->SetCreator("JPGIT", true);
        $pdf->SetProtection(array('print'));
        $pdf->SetTopMargin(15);
        $pdf->SetAutoPageBreak(true, 25);
        $pdf->SetFont('Arial', '', 6);

        $fullw = ($pdf->w - $pdf->rMargin - $pdf->lMargin);
        $w2 = $fullw / 2;
        $cellh = 4;
//        $pdf->configureRow($rows, TRUE);

        $pdf->colWidth =
                array(
                    'cta' => 23, /* 52.95 cta */
//                    'afectable' => 15, /* 14.59 afectable */
                    'id' => 7, /* 52.95 cta */
                    'mov' => 7, /* 52.95 cta */
                    'descripcion' => 52.95, /* descripcion */
                    'concepto' => 30, /* concepto */
                    'deudorai' => 17, /* 14.94 deudorai */
                    'acreedorai' => 17, /* 16.71 acreedorai */
                    'cargos' => 14, /* 12.94 cargos */
                    'abonos' => 14, /* 13.53 abonos */
                    'deudoraf' => 17, /* 15.06 deudoraf */
                    'acreedoraf' => 17 /* 16.82 acreedoraf */
        );

        $pdf->colBorder =
                array(
                    'cta' => 'B', /* cta */
                    'afectable' => 'B', /* afectable */
                    'id' => 'B',
                    'mov' => 'B',
                    'descripcion' => 'B', /* descripcion */
                    'concepto' => 'B', /* concepto */
                    'deudorai' => 'B', /* deudorai */
                    'acreedorai' => 'B', /* acreedorai */
                    'cargos' => 'B', /* cargos */
                    'abonos' => 'B', /* abonos */
                    'deudoraf' => 'B', /* deudoraf */
                    'acreedoraf' => 'B' /* acreedoraf */
        );


        $pdf->colTitle =
                array(
                    'cta' => 'CTA', /* cta */
                    'afectable' => 'AFECTABLE', /* afectable */
                    'id' => 'ID',
                    'mov' => 'MOV',
                    'descripcion' => 'DESCRIPCION', /* descripcion */
                    'concepto' => 'CONCEPTO', /* concepto */
                    'deudorai' => 'DEUDORAI', /* deudorai */
                    'acreedorai' => 'ACREEDORAI', /* acreedorai */
                    'cargos' => 'CARGOS', /* cargos */
                    'abonos' => 'ABONOS', /* abonos */
                    'deudoraf' => 'DEUDORAF', /* deudoraf */
                    'acreedoraf' => 'ACREEDORAF' /* acreedoraf */
        );

        $pdf->colAlign =
                array(
                    'cta' => 'L', /* cta */
                    'afectable' => 'L', /* afectable */
                    'id' => 'C',
                    'mov' => 'C',
                    'descripcion' => 'L', /* descripcion */
                    'concepto' => 'L', /* concepto */
                    'deudorai' => 'R', /* deudorai */
                    'acreedorai' => 'R', /* acreedorai */
                    'cargos' => 'R', /* cargos */
                    'abonos' => 'R', /* abonos */
                    'deudoraf' => 'R', /* deudoraf */
                    'acreedoraf' => 'R' /* acreedoraf */
        );
        $compensation['descripcion'] = 1;
        $pdf->SetCompensation($pdf->colWidth, $compensation);
        $pdf->headerMethod[]['Image'] = array('logo_rilez.gif', $pdf->lMargin + ($fullw * 0.3), 1, ($fullw * 0.4), 13);
        $pdf->headerMethod[]['RoundedRect'] = array($pdf->lMargin, $pdf->tMargin, $fullw, $pdf->h - $pdf->tMargin - $pdf->bMargin, 2, '1234');
        $pdf->headerMethod[]['RoundedRect'] = array($pdf->lMargin, $pdf->tMargin, $fullw, $pdf->h - $pdf->tMargin - $pdf->bMargin, 2, '1234');
        $x = $pdf->lMargin;
        foreach ($pdf->colWidth as $field => $w) {
            $x+=$w;
            if ($field != "acreedoraf")
                $pdf->headerMethod[]['Line'] = array($x, $pdf->tMargin, $x, $pdf->h - $pdf->bMargin);
        }
        $pdf->headerMethod[]['SetFont'] = array('Arial', 'B', 6);
        $pdf->headerMethod[]['Row'] = array($pdf->colTitle, 5, $pdf->colBorder, 1);
        $pdf->headerMethod[]['SetFont'] = array('Arial', 'I', 6);

//        $pdf->footerMethod[]['SetY'] = array( $this->h - $this->bMargin);
//        $pdf->footerMethod[]['SetY'] = array( -20 );
//        $pdf->footerMethod[]['FooterTotales'] = array();
//        $pdf->footerMethod[]['SetY'] = array($pdf->h - 15);
        $pdf->footerMethod[]['SetY'] = array(-5);
        $pdf->footerMethod[]['Page'] = array(($fullw), 5);

        $pdf->totalesWidth['label'] = 0;
        $pdf->totalesBorder['label'] = "B";
        $pdf->totalesAlign['label'] = "R";
        foreach (array('deudorai', 'acreedorai', 'cargos', 'abonos', 'deudoraf', 'acreedoraf') as $field) {
            if (isset($pdf->colWidth[$field])) {
                $pdf->totalesWidth[$field] = $pdf->colWidth[$field];
                $pdf->totalesAlign[$field] = $pdf->colAlign[$field];
                $pdf->totalesBorder[$field] = "B";
            }
        }
        $pdf->totalesWidth['label'] = $fullw - array_sum($pdf->totalesWidth);
        $pdf->totales = array();
        $pdf->AddPage();
        if (!empty($rows)) {
            $co = array();
            $cm = array();
            $o = 0;
            $m = 0;
            $c = 0;
            foreach ($rows as $row) {
//                $pdf->colFormat['cta'] = "%'0+4s";
//                $pdf->colFormat['scta'] = "%'0+4s";
//                $pdf->colFormat['sscta'] = "%'0+4s";
//                $pdf->colFormat['ssscta'] = "%'0+4s";
//            $pdf->colFormat['cargo'] = "%0.{$this->decimals}f";
//            $pdf->colFormat['abono'] = "%0.{$this->decimals}f";

                if ($c != $row['cta']) {
                    if ($c) {
                        $pdf->totales['label'] = "Sumas :$c";
                        //Formateo a detalles
                        foreach (array('deudorai', 'acreedorai', 'cargos', 'abonos', 'deudoraf', 'acreedoraf') as $field) {
                            if (isset($pdf->totales[$field]))
                                $pdf->totales[$field] = numberFormat(round($pdf->totales[$field], $this->decimals), $this->decimals, '.', ',', 3);
                        }
                        $pdf->FancyRow($pdf->totales, $pdf->totalesWidth, $cellh, $pdf->totalesBorder, $pdf->totalesAlign, array(), $pdf->colFormat);
                        $pdf->totales = array();
                    }
                    $c = $row['cta'];
                }
//                    $pdf->totales['label'] = "Sumas iguales cta :" . $row['cta'];
                foreach (array('deudorai', 'acreedorai', 'cargos', 'abonos', 'deudoraf', 'acreedoraf') as $field) {
                    if (isset($row[$field])) {
                        $pdf->totales[$field] += $row[$field];
                        $cm[$field] += $row[$field];
                        $co[$field] += $row[$field];
                    }
                }
//                }
//                if (strtoupper($row['afectable']) == 'S') {
////                    $pdf->totales['label'] = "Sumas iguales cta :" . $row['cta'];
//                    foreach (array('deudorai', 'acreedorai', 'cargos', 'abonos', 'deudoraf', 'acreedoraf') as $field) {
//                        if (isset($row[$field])) {
//                            $pdf->totales[$field] += $row[$field];
//                            $cm[$field] += $row[$field];
//                            $co[$field] += $row[$field];
//                        }
//                    }
//                }
                //Formateo a detalles
                foreach (array('deudorai', 'acreedorai', 'cargos', 'abonos', 'deudoraf', 'acreedoraf') as $field) {
                    if (isset($row[$field]))
                        $row[$field] = numberFormat(round($row[$field], $this->decimals), $this->decimals, '.', ',', 3);
                }
//                $row['cta'] = ($row['cta']?$row['cta']:'') . ($row['scta']?'.' . $row['scta']:'') . ($row['sscta']?'.' . $row['sscta']:'') . ($row['ssscta']?'.' . $row['ssscta']:'');
                $s = 0;
                $cta_s = '';
                for ($s = 0, $cta = str_repeat('s', $s) . "cta"; !empty($row[$cta]); $s++, $cta = str_repeat('s', $s) . "cta") {
                    $cta_s = ($cta_s ? $cta_s . '.' : '') . sprintf("%s", $row[$cta]); //'0+4
                }
                $row['cta'] = $cta_s;
                $pdf->FancyRow($row, $pdf->colWidth, $cellh, $pdf->colBorder, $pdf->colAlign, array(), $pdf->colFormat);
            }
            if ($c) {
                $pdf->totales['label'] = "Sumas :$c";
                //Formateo a detalles
                foreach (array('deudorai', 'acreedorai', 'cargos', 'abonos', 'deudoraf', 'acreedoraf') as $field) {
                    if (isset($pdf->totales[$field]))
                        $pdf->totales[$field] = numberFormat(round($pdf->totales[$field], $this->decimals), $this->decimals, '.', ',', 3);
                }
                $pdf->FancyRow($pdf->totales, $pdf->totalesWidth, $cellh, $pdf->totalesBorder, $pdf->totalesAlign, array(), $pdf->colFormat);
                $pdf->totales = array();
//                $pdf->totales['label'] = "Sumas iguales";
            }
            if ($m > 0) {
//                $pdf->Cell(0, $cellh, $mc, 0, 1, 'C');
                if (!empty($cm['label'])) {
                    //Formateo a detalles
                    foreach (array('deudorai', 'acreedorai', 'cargos', 'abonos', 'deudoraf', 'acreedoraf') as $field) {
                        if (isset($cm[$field]))
                            $cm[$field] = numberFormat(round($cm[$field], $this->decimals), $this->decimals, '.', ',', 3);
                    }
                    $pdf->FancyRow($cm, $pdf->totalesWidth, $cellh, $pdf->totalesBorder, $pdf->totalesAlign, array(), $pdf->colFormat);
                }
            }
            if ($o > 0) {
//                $pdf->Cell(0, $cellh, $oc, 0, 1, 'C');
                if (!empty($co['label'])) {
                    //Formateo a detalles
                    foreach (array('deudorai', 'acreedorai', 'cargos', 'abonos', 'deudoraf', 'acreedoraf') as $field) {
                        if (isset($co[$field]))
                            $co[$field] = numberFormat(round($co[$field], $this->decimals), $this->decimals, '.', ',', 3);
                    }
                    $pdf->FancyRow($co, $pdf->totalesWidth, $cellh, $pdf->totalesBorder, $pdf->totalesAlign, array(), $pdf->colFormat);
                }
            }
        }else {
            $pdf->Cell(0, $cellh, "Sin resultados", 0, 1, 'C');
        }
        return $this->Output($pdf, 'Auxiliar', $dest);
    }

    function __destruct() {
        if (is_resource($this->conn))
            mysql_close($this->conn);
    }

}

function pre($pre, $return=null) {
    echo "<pre>";
    if (is_array($pre))
        if ($return)
            return print_r($pre, $return);
        else
            print_r($pre, $return);
    else
        echo $pre;
    echo "</pre>";
}

header('Content-type: text/html; charset=utf-8');

$action = $_REQUEST['action'];
//$action = 'balanzacomprobacion';
if (empty($action))
    die("Seleccione Reporte");
$rpt = new reporte($server, $username, $password, $database_name);
//pre($rpt->query("SELECT * FROM articulos", TRUE));
$sucursal_id = intval($_REQUEST['sucursal_id']);
$almacen_id = intval($_REQUEST['almacen_id']);

$linea_id = intval($_REQUEST['linea_id']);
$sublinea_id = intval($_REQUEST['sublinea_id']);

$with_image = empty($_REQUEST['with_image']) ? FALSE : $_REQUEST['with_image'];

$detailed_totals = isset($_REQUEST['detailed_totals']) ? $_REQUEST['detailed_totals'] : FALSE;
$discount_included = isset($_REQUEST['discount_included']) ? $_REQUEST['discount_included'] : TRUE;
$vendible = isset($_REQUEST['vendible']) ? $_REQUEST['vendible'] : 'S';
$dest = empty($_REQUEST['dest']) ? 'I' : $_REQUEST['dest'];
switch (strtoupper($action)) {
    case 'CATALOGOALMACEN':
        $name = $rpt->pdfCatalogoAlmacen($almacen_id, $linea_id, $sublinea_id, $vendible, $with_image, $dest);
        break;
    case 'INVENTARIO':
        $stock_minimo = ($_REQUEST['stock_minimo']);
        $stock_maximo = ($_REQUEST['stock_maximo']);

        $with_linea = ($_REQUEST['with_linea']);
        $with_sublinea = ($_REQUEST['with_sublinea']);
        $with_existencia = ($_REQUEST['with_existencia']);

        $name = $rpt->pdfInventario($sucursal_id, $almacen_id, $linea_id, $sublinea_id, $stock_minimo, $stock_maximo, $with_linea, $with_sublinea, $with_existencia, $vendible, $with_image, $dest);
        break;
    case 'SALIDAS':
        $name = $rpt->pdfSalidas($sucursal_id, $almacen_id, $cliente_id, $agente_id, $dest);
        break;
    case 'SALIDA':
        $salida_id = intval($_REQUEST['salida_id']);
        $with_posicion = empty($_REQUEST['with_posicion']) ? FALSE : $_REQUEST['with_posicion'];
        $with_tipo = empty($_REQUEST['with_tipo']) ? FALSE : $_REQUEST['with_tipo'];
        $with_almacen = empty($_REQUEST['with_almacen']) ? FALSE : $_REQUEST['with_almacen'];
        $name = $rpt->pdfSalida($salida_id, $with_posicion, $with_tipo, $with_almacen, $detailed_totals, $discount_included, $dest);
        break;
    case 'TICKETSALIDA':
        $salida_id = intval($_REQUEST['salida_id']);
        $with_posicion = empty($_REQUEST['with_posicion']) ? FALSE : $_REQUEST['with_posicion'];
        $with_tipo = empty($_REQUEST['with_tipo']) ? FALSE : $_REQUEST['with_tipo'];
        $with_almacen = empty($_REQUEST['with_almacen']) ? FALSE : $_REQUEST['with_almacen'];
        $name = $rpt->pdfTicketSalida($salida_id, $with_posicion, $with_tipo, $with_almacen, FALSE, $discount_included, $dest);
        break;
    case 'ENTRADA':
        $entrada_id = intval($_REQUEST['entrada_id']);
        $with_posicion = empty($_REQUEST['with_posicion']) ? FALSE : $_REQUEST['with_posicion'];
        $with_tipo = empty($_REQUEST['with_tipo']) ? FALSE : $_REQUEST['with_tipo'];
        $with_almacen = empty($_REQUEST['with_almacen']) ? FALSE : $_REQUEST['with_almacen'];
        $name = $rpt->pdfEntrada($entrada_id, $with_posicion, $with_tipo, $with_almacen, $detailed_totals, $discount_included, $dest);
        break;
    case 'POLIZA':
        $name = $rpt->pdfPoliza($_REQUEST['poliza_id'], $dest);
        break;
    case 'XML_SALIDAS':
        $sql->action = 'Select';
        $sql->fields[] = 'salidas.*';
        $sql->table = 'salidas';
        $salidas = $rpt->query($rpt->sql($sql));

        foreach ($salidas as $salida) {
            foreach ($salida as $field => $value) {
                ;
            };
        }
        break;
    case 'BALANZACOMPROBACION':
        $name = $rpt->pdfBalanzaComprobacion();
        break;
    case 'AUXILIAR':
        $name = $rpt->pdfAuxiliar();
        break;
    default:
        die("Reporte ($action) no disponible");
        break;
}
//pre($rpt->Sucursal($sucursal_id, $almacen_id));exit;
//$rpt->pdfCatalogoAlmacen($almacen_id, $linea_id, $sublinea_id);

if (file_exists($name)) {
    if (isset($email) && !empty($email)) {
        echo "Correo enviado";
    }else
        header('Location: ' . $name);
}
?>