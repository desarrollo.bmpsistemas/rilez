SELECT
    sucursales.nomcomercial AS nombre,
    calles.descripcion AS calle,
    numero_exterior AS noExterior,
    numero_interior AS noInterior,
    colonias.descripcion AS colonia,
    referencia,
    municipios.descripcion AS municipio,
    estados.descripcion AS estado,
    paises.descripcion AS pais,
    sucursales.codigo_postal AS codigoPostal
FROM almacenes
INNER JOIN sucursales ON sucursales.id = almacenes.sucursal_id
INNER JOIN municipios ON sucursales.municipio_id = municipios.id
INNER JOIN estados ON estados.id = municipios.estado_id
INNER JOIN paises ON paises.id = estados.pais_id
INNER JOIN calles ON sucursales.calle_id = calles.id
INNER JOIN colonias ON colonias.id = sucursales.colonia_id
where almacenes.id=1

SELECT
unidades.descripcion AS unidad,
articulos.id,
articulos.id2,
articulos.descripcion,
detalles_tmp_entradas.cantidad*detalles_tmp_entradas.costo AS importe,
detalles_tmp_entradas.iva,
detalles_tmp_entradas.cantidad* detalles_tmp_entradas.descuento AS descuento
FROM dbrilez.detalles_tmp_entradas
INNER JOIN dbrilez.articulos ON detalles_tmp_entradas.articulo_id = articulos.id
INNER JOIN unidades ON articulos.unidad_id = unidades.id
where detalles_tmp_entradas.capturista_id=1 and detalles_tmp_entradas.tipo_mov_id=1 and almacen_id=1

SELECT
-- detalles_pedidos.id,
-- detalles_pedidos.pedido_id,
-- detalles_pedidos.articulo_id,
-- articulos.id,
-- articulos.id2,
-- articulos.sublinea_id,
-- articulos.unidad_id,
unidades.descripcion AS unidad,
articulos.descripcion,
-- articulos.produccion,
-- articulos.creado,
-- articulos.modificado,
-- articulos.capturista_id,
-- articulos.usuario_modificador,
-- detalles_pedidos.cantidad,
-- detalles_pedidos.importe,
-- detalles_pedidos.descuento,
-- detalles_pedidos.iva
-- detalles_tmp_entradas.capturista_id,
detalles_tmp_entradas.cantidad*detalles_tmp_entradas.costo AS importe,
detalles_tmp_entradas.iva,
detalles_tmp_entradas.cantidad* detalles_tmp_entradas.descuento AS descuento
FROM dbrilez.detalles_tmp_entradas
INNER JOIN dbrilez.articulos ON detalles_tmp_entradas.articulo_id = articulos.id
INNER JOIN unidades ON articulos.unidad_id = unidades.id
-- WHERE detalles_pedidos.pedido_id=$pedido_id;
where detalles_tmp_entradas.capturista_id=1 and detalles_tmp_entradas.tipo_mov_id=1 and almacen_id=1