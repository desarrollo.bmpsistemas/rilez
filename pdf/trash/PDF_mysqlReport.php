<?php
require('fpdf.php');
class PDF_Javascript extends FPDF {

    var $javascript;
    var $n_js;

    function __construct($orientation='P', $uni='mm', $format='Letter') {
        parent::__construct($orientation, $uni, $format);
    }

    function IncludeJS($script) {
        $this->javascript = $script;
    }

    function _putjavascript() {
        $this->_newobj();
        $this->n_js = $this->n;
        $this->_out('<<');
        $this->_out('/Names [(EmbeddedJS) ' . ($this->n + 1) . ' 0 R ]');
        $this->_out('>>');
        $this->_out('endobj');
        $this->_newobj();
        $this->_out('<<');
        $this->_out('/S /JavaScript');
        $this->_out('/JS ' . $this->_textstring($this->javascript));
        $this->_out('>>');
        $this->_out('endobj');
    }

    function _putresources() {
        parent::_putresources();
        if (!empty($this->javascript)) {
            $this->_putjavascript();
        }
    }

    function _putcatalog() {
        parent::_putcatalog();
        if (isset($this->javascript)) {
            $this->_out('/Names <</JavaScript ' . ($this->n_js) . ' 0 R>>');
        }
    }

}

class PDF_mysqlReport extends PDF_Javascript {

    var $tablewidths;
    var $headerset;
    var $footerset;
    function __construct($orientation='P', $unit='pt', $format='Letter'){
        parent::__construct($orientation, $unit, $format);
        $this->SetCreator("JPGIT-mysql_report");
//        $this->creator = "JPGIT-mysql_report";
//        $this->SetFont('Arial', '', 8);
    }

    function AutoPrint($dialog=false)
    {
        $param=($dialog ? 'true' : 'false');
        $script="print(".$param.");";
        $this->IncludeJS($script);
    }
    
    function SetLogo($logo='logo_pb.png') {
        $this->logo = $logo;
        $this->logoh = 1;
    }

    function SetAddress($address = "Domicilio") {
        $this->address = utf8_decode($address);
        $this->addressW=0;
        $this->addressH=0;
        $this->addressBorder=0;
        $this->addressLn=1;
        $this->addressAlign='C';
    }

    function SetTitle($title="Titulo", $isUTF8=false) {
        parent::SetTitle(utf8_decode($title), $isUTF8);
        $this->titleW=0;
        $this->titleH=0;
        $this->titleBorder=0;
        $this->titleLn=1;
        $this->titleAlign='C';
    }

    function SetSubject($subject="Asunto", $isUTF8=false){
        parent::SetSubject(utf8_decode($subject), $isUTF8);
        $this->subjectW=0;
        $this->subjectH=0;
        $this->subjectBorder=0;
        $this->subjectLn=1;
        $this->subjectAlign='C';
    }

    function SetAuthor($author = "Empresa", $isUTF8=false) {
        parent::SetAuthor(utf8_decode($author), $isUTF8);
        $this->authorW=0;
        $this->authorH=0;
        $this->authorBorder=0;
        $this->authorLn=1;
        $this->authorAlign='C';
    }

    function SetParameter($parameter, $val){
        $this->$parameter = is_string($val)?utf8_decode($val): $val;
    }

    function GetParameter($parameter){
        return $this->$parameter;
    }

    function _beginpage($orientation, $format) {
        $this->page++;
        if (!$this->pages[$this->page]) // solves the problem of overwriting a page if it already exists
            $this->pages[$this->page] = '';
        $this->state = 2;
        $this->x = $this->lMargin;
        $this->y = $this->tMargin;
        $this->FontFamily = '';
        //Check page size
        if ($orientation == '')
            $orientation = $this->DefOrientation;
        else
            $orientation=strtoupper($orientation[0]);
        if ($format == '')
            $format = $this->DefPageFormat;
        else {
            if (is_string($format))
                $format = $this->_getpageformat($format);
        }
        if ($orientation != $this->CurOrientation || $format[0] != $this->CurPageFormat[0] || $format[1] != $this->CurPageFormat[1]) {
            //New size
            if ($orientation == 'P') {
                $this->w = $format[0];
                $this->h = $format[1];
            } else {
                $this->w = $format[1];
                $this->h = $format[0];
            }
            $this->wPt = $this->w * $this->k;
            $this->hPt = $this->h * $this->k;
            $this->PageBreakTrigger = $this->h - $this->bMargin;
            $this->CurOrientation = $orientation;
            $this->CurPageFormat = $format;
        }
        if ($orientation != $this->DefOrientation || $format[0] != $this->DefPageFormat[0] || $format[1] != $this->DefPageFormat[1])
            $this->PageSizes[$this->page] = array($this->wPt, $this->hPt);
    }

    function Header() {
        global $maxY;
        // Check if header for this page already exists
        if (!$this->headerset[$this->page]) {
            $plbl = "cell";
            $pFontFamily=$plbl."FontFamily";$pFontStyle=$plbl."FontStyle";$pFontSize=$plbl."FontSize";
            $this->SetFont($this->$pFontFamily?$this->$pFontFamily:'Arial', $this->$pFontStyle, $this->$pFontSize?$this->$pFontSize:8);
            $this->cellFontFamily = $this->FontFamily;
            $this->cellFontSize = $this->FontSizePt;
            $this->SetY(0);
            $plbl="author";
            if(isset ($this->$plbl)){//JPG
                $pFontFamily=$plbl."FontFamily";$pFontStyle=$plbl."FontStyle";$pFontSize=$plbl."FontSize";
                $this->SetFont($this->$pFontFamily?$this->$pFontFamily:$this->cellFontFamily, $this->$pFontStyle, $this->$pFontSize?$this->$pFontSize:$this->cellFontSize);
                $pW=$plbl."W";$pH=$plbl."H";$pBorder=$plbl."Border";$pLn=$plbl."Ln";$pAlign=$plbl."Align";
                $this->Cell($this->$pW, $this->$pH?$this->$pH:$this->FontSizePt, $this->$plbl, $this->$pBorder, $this->$pLn, $this->$pAlign );//JPG
            }

            $plbl="address";
            if(isset ($this->$plbl)){//JPG
                $pFontFamily=$plbl."FontFamily";$pFontStyle=$plbl."FontStyle";$pFontSize=$plbl."FontSize";
                $this->SetFont($this->$pFontFamily?$this->$pFontFamily:$this->cellFontFamily, $this->$pFontStyle, $this->$pFontSize?$this->$pFontSize:$this->cellFontSize);
                $pW=$plbl."W";$pH=$plbl."H";$pBorder=$plbl."Border";$pLn=$plbl."Ln";$pAlign=$plbl."Align";
                $this->MultiCell($this->$pW, $this->$pH?$this->$pH:$this->FontSizePt, $this->$plbl, $this->$pBorder, $this->$pAlign);
            }

            $plbl="title";
            if(isset ($this->$plbl)){//JPG
                $pFontFamily=$plbl."FontFamily";$pFontStyle=$plbl."FontStyle";$pFontSize=$plbl."FontSize";
                $this->SetFont($this->$pFontFamily?$this->$pFontFamily:$this->cellFontFamily, $this->$pFontStyle, $this->$pFontSize?$this->$pFontSize:$this->cellFontSize);
                $pW=$plbl."W";$pH=$plbl."H";$pBorder=$plbl."Border";$pLn=$plbl."Ln";$pAlign=$plbl."Align";
                $this->Cell($this->$pW, $this->$pH?$this->$pH:$this->FontSizePt, $this->$plbl, $this->$pBorder, $this->$pLn, $this->$pAlign );//JPG
            }

            $plbl = "subject";
            if(isset ($this->$plbl)){//JPG
                $pFontFamily=$plbl."FontFamily";$pFontStyle=$plbl."FontStyle";$pFontSize=$plbl."FontSize";
                $this->SetFont($this->$pFontFamily?$this->$pFontFamily:$this->cellFontFamily, $this->$pFontStyle, $this->$pFontSize?$this->$pFontSize:$this->cellFontSize);
                $pW=$plbl."W";$pH=$plbl."H";$pBorder=$plbl."Border";$pLn=$plbl."Ln";$pAlign=$plbl."Align";
                $this->MultiCell($this->$pW, $this->$pH?$this->$pH:$this->FontSizePt, $this->$plbl, $this->$pBorder, $this->$pAlign);
            }
            if($this->tMargin < $this->GetY()){
                $this->tMargin = $this->GetY();
            }
            //Logo
            if (isset($this->logo)){
                //$this->SetX($this->tMargin + $this->GetY());
                $this->logoh = doubleval($this->logoh);                
                $this->logow = doubleval($this->logow);
                $this->logoy = doubleval($this->logoy);                
                $this->logox = doubleval($this->logox);
                if($this->logoh < 1){
                        $this->logoh = $this->tMargin*$this->logoh;
                }
                if($this->logow < 1){
                        $this->logow = ($this->w - $this->rMargin - $this->lMargin)*$this->logow;
                }
                $this->tMargin = $this->logoh> $this->tMargin?$this->logoh:$this->tMargin;
                $this->SetY(($this->tMargin - $this->logoh) / 2);
                if(empty($this->logoy))
                        $this->logoy = $this->GetY ();
                if(empty($this->logox))
                    $this->logox = $this->lMargin;// + $this->logoy;
                $this->Image($this->logo,  $this->logox, $this->logoy, $this->logow, $this->logoh);
            }

            foreach ($this->tablewidths as $width) {
                $fullwidth += $width;
            }
            $l = ($this->lMargin);
            $plbl = "cellHeader";
            $pFontFamily=$plbl."FontFamily";$pFontStyle=$plbl."FontStyle";$pFontSize=$plbl."FontSize";
            $this->SetFont($this->$pFontFamily?$this->$pFontFamily:$this->cellFontFamily, $this->$pFontStyle, $this->$pFontSize?$this->$pFontSize:$this->cellFontSize);
            $pW=$plbl."W";$pH=$plbl."H";$pBorder=$plbl."Border";$pLn=$plbl."Ln";$pAlign=$plbl."Align";$pFill=$plbl."Fill";
            //calcular altura max de Y
            foreach ($this->colTitles as $col => $txt) {
                //$this->SetXY($l, ($this->tMargin));
                //$this->MultiCell($this->tablewidths[$col], $this->FontSizePt, $txt);//JPG
                /*Si se tiene un ancho de celda y se sabe cuanto abarca el ancho del texto, entonces */
                $cellH = ceil($this->GetStringWidth($txt) / $this->tablewidths[$col]) * $this->FontSizePt;
                //$l += $this->tablewidths[$col];
                //$maxY = ($maxY < $this->GetY()) ? $this->GetY() : $maxY;
                if($maxY < ($this->tMargin + $cellH))$maxY = ($this->tMargin + $cellH);
            }
            $this->SetXY($this->lMargin, $this->tMargin);
            $this->SetFillColor(200, 200, 200);
            $l = ($this->lMargin);
            foreach ($this->colTitles as $col => $txt) {
                $this->SetXY($l, $this->tMargin);
                $this->cell($this->tablewidths[$col], $maxY - ($this->tMargin), '', $this->$pBorder, 0, 'L', $this->$pFill);//JPG fill 1 a 0
                $this->SetXY($l, $this->tMargin);
                $this->MultiCell($this->tablewidths[$col], $this->FontSizePt, $txt, 0, $this->$pAlign?$this->$pAlign:'C');
                $l += $this->tablewidths[$col];
            }
            $this->SetFillColor(255, 255, 255);
//            if($this->tMargin > $maxY){
//                $this->tMargin = $maxY;
//            }
//            $this->tMargin = $maxY;
            $this->SetY($maxY);
            // set headerset
            $this->headerset[$this->page] = 1;
        }
        
//        $this->tMargin = $maxY;
//        $this->SetY($maxY);
    }

    function Footer() {
        // Check if footer for this page already exists
        if (!$this->footerset[$this->page]) {
            $this->SetY(-15);
            //Page number
//            if(count($this->pages) > 1)//If have more than 1 page, print footer
            $this->Cell(0, 10, $this->PageNo() . '/{nb}', 0, 0, 'C');
            // set footerset
            $this->footerset[$this->page] = 1;
        }
    }

    function morepagestable($lineheight=8) {
        // some things to set and 'remember'
        $l = $this->lMargin;
        $startheight = $h = $this->GetY();
        $startpage = $currpage = $this->page;

        // calculate the whole width
        foreach ($this->tablewidths as $width) {
            $fullwidth += $width;
        }

        // Now let's start to write the table
        $row = 0;
        while ($data = mysql_fetch_row($this->results)) {
            $this->page = $currpage;
            // write the horizontal borders
//            $this->Line($l, $h, $fullwidth + $l, $h);//JPG
            // write the content and remember the height of the highest col
            foreach ($data as $col => $txt) {

                $this->page = $currpage;
                $this->SetXY($l, $h);
                $this->MultiCell($this->tablewidths[$col], $lineheight, $txt, $this->cellBorder, $this->colAlign[$col]);//JPG Border

                $l += $this->tablewidths[$col];

                if ($tmpheight[$row . '-' . $this->page] < $this->GetY()) {
                    $tmpheight[$row . '-' . $this->page] = $this->GetY();
                }
                if ($this->page > $maxpage)
                    $maxpage = $this->page;
                //suma las cantidades de las columnas jpg
                if(isset($this->sum)){
                    if(isset($this->sum[$this->colTitles[$col]])){
                        $this->sum[$this->colTitles[$col]]+=$txt;
                    }
                }
                unset($data[$col]);
            }
            // get the height we were in the last used page
            $h = $tmpheight[$row . '-' . $maxpage];
            // set the "pointer" to the left margin
            $l = $this->lMargin;
            // set the $currpage to the last page
            $currpage = $maxpage;
            unset($data[$row]);
            $row++;
        }
        //draw the sum('s)
        $plbl = 'sum';
        if (isset($this->sum)) {
            $l = ($this->lMargin);
            //$this->SetFont('Arial', '', $this->cellFontSize);
            foreach ($this->colTitles as $col => $txt) {
                $this->SetXY($l, $h);
                if (isset($this->sum[$txt])) {
                    $pVisible = $plbl . $txt . 'Visible';
                    if ($this->$pVisible) {
                        $pBorder = $plbl . $txt . 'Border';
                        $this->MultiCell($this->tablewidths[$col], $this->FontSizePt, $this->sum[$txt], $this->$pBorder, $this->colAlign[$col]);
                    }
                }
                $l += $this->tablewidths[$col];
                //$maxY = ($maxY < $this->GetY()) ? $this->GetY() : $maxY;
            }
        }
        // draw the borders
        // we start adding a horizontal line on the last page
        $this->page = $maxpage;
        $this->Line($this->lMargin, $h, $l, $h);//JPG Ultima linea detalles
//        $this->Line($l, $h, $fullwidth + $l, $h);//JPG
/*        // now we start at the top of the document and walk down
        for ($i = $startpage; $i <= $maxpage; $i++) {
            $this->page = $i;
            $l = $this->lMargin;
            $t = ($i == $startpage) ? $startheight : $this->tMargin;
            $lh = ($i == $maxpage) ? $h : $this->h - $this->bMargin;
            //$this->Line($l, $t, $l, $lh);//1ra vertical
            foreach ($this->tablewidths as $width) {
                $l += $width;
                //$this->Line($l, $t, $l, $lh);//vertical
            }
        }
 */
        // set it to the last page, if not it'll cause some problems
        $this->page = $maxpage;
    }

    function morepagestableimage($lineheight=0,$img_h=0,$img_array = array(), $img_default = 'no_img.jpg') {
        if(empty($lineheight))
            $lineheight = $this->FontSize;
        // some things to set and 'remember'
        $l = $this->lMargin;
        $startheight = $h = $this->GetY();
        $startpage = $currpage = $this->page;

        // calculate the whole width
        foreach ($this->tablewidths as $width) {
            $fullwidth += $width;
        }

        // Now let's start to write the table
        $row = 0;
        while ($data = mysql_fetch_row($this->results)) {
            if ($this->GetY() + ($lineheight > $img_h?$lineheight : $img_h) > $this->h - $this->bMargin) {
                // write the horizontal borders
//                    $this->Line($l, $h, $fullwidth + $l, $h);//JPG
//                    $this->AddPage();
//                    $currpage = $this->page;
//                    $tmpheight[$row . '-' . $this->page] = $h = $this->GetY();
            }
            $this->page = $currpage;
            // write the horizontal borders
//            $this->Line($l, $h, $fullwidth + $l, $h);//JPG
            // write the content and remember the height of the highest col
            foreach ($data as $col => $txt) {
                $this->page = $currpage;
                $this->SetXY($l, $h);
                if(in_array($this->colTitles[$col], $img_array)){
                     if(is_file($txt)){
                         if ($tmpheight[$row . '-' . $this->page] < ($h + $img_h)) {
                            $tmpheight[$row . '-' . $this->page] = $h + $img_h;
                         }
                         $this->Image(is_file($txt)?$txt:$img_default, $l +1, $h+1, 0, $img_h?$img_h - 2:$img_h);/*$this->tablewidths[$col] - 2*/
                     }
                }else
                    $this->MultiCell($this->tablewidths[$col], $lineheight, isset ($this->colf[$this->colTitles[$col]]) ?sprintf($this->colf[$this->colTitles[$col]],$txt):$txt, 0, $this->colAlign[$col]);//JPG Border

                $l += $this->tablewidths[$col];
                if(($this->GetY() - $h) < $img_h ){
//                    $this->SetY($h + $img_h);
                }                
                if ($tmpheight[$row . '-' . $this->page] < $this->GetY()) {//+ $img_h
                    $tmpheight[$row . '-' . $this->page] = $this->GetY();//+ $img_h
                }
                if ($this->page > $maxpage){
                    $maxpage = $this->page;
                }
                //suma las cantidades de las columnas jpg
                if(isset($this->sum)){
                    if(isset($this->sum[$this->colTitles[$col]])){
                        $this->sum[$this->colTitles[$col]]+=$txt;
                    }
                }
                unset($data[$col]);
            }
            // get the height we were in the last used page
            $h = $tmpheight[$row . '-' . $maxpage];
            // set the "pointer" to the left margin
            $l = $this->lMargin;
            // set the $currpage to the last page
            $currpage = $maxpage;
//            $this->Line($l, $h, $fullwidth, $h);
            unset($data[$row]);
            $row++;
        }
        //draw the sum('s)
        $plbl = 'sum';
        if (isset($this->sum)) {
            $l = ($this->lMargin);
            //$this->SetFont('Arial', '', $this->cellFontSize);
            foreach ($this->colTitles as $col => $txt) {
                $this->SetXY($l, $h);
                if (isset($this->sum[$txt])) {
                    $pVisible = $plbl . $txt . 'Visible';
                    if ($this->$pVisible) {
                        $pBorder = $plbl . $txt . 'Border';
                        $this->MultiCell($this->tablewidths[$col], $this->FontSizePt, isset ($this->colf[$txt]) ?sprintf($this->colf[$txt],$this->sum[$txt]):$this->sum[$txt], $this->$pBorder, $this->colAlign[$col]);
                    }
                }
                $l += $this->tablewidths[$col];
                //$maxY = ($maxY < $this->GetY()) ? $this->GetY() : $maxY;
            }
        }
        // draw the borders
        // we start adding a horizontal line on the last page
        $this->page = $maxpage;
//        $this->Line($this->lMargin, $h, $l, $h);//JPG Ultima linea detalles
//        $this->Line($l, $h, $fullwidth + $l, $h);//JPG
        // now we start at the top of the document and walk down
        for ($i = $startpage; $i <= $maxpage; $i++) {
            $this->page = $i;
            $l = $this->lMargin;
            $t = ($i == $startpage) ? $startheight : $startheight;//$this->tMargin;
            $lh = ($i == $maxpage) ? $h : $this->h - $this->bMargin;
            $this->Line($l, $t, $l, $lh);//1ra vertical
            foreach ($this->tablewidths as $width) {
                $l += $width;
                $this->Line($l, $t, $l, $lh);//vertical
            }
            $this->Line($this->lMargin, $t, $l, $t);//top
            $this->Line($this->lMargin, $lh, $l, $lh);//bMargin
        }
 
        // set it to the last page, if not it'll cause some problems
        $this->page = $maxpage;
    }
    
    function connect($host='localhost', $username='', $password='', $db='') {
        $this->conn = mysql_connect($host, $username, $password) or die(mysql_error());
        mysql_select_db($db, $this->conn) or die(mysql_error());
        return true;
    }

    function query($query) {
        $this->results = mysql_query($query, $this->conn);
        $this->numFields = mysql_num_fields($this->results);
        $this->numRows = mysql_num_rows($this->results);
    }

    function mysql_report($query, $dump=false) {

//        foreach ($attr as $key => $val) {
//            $this->$key = is_string($val)?utf8_decode($val): $val;
//        }
        $this->query($query);

        if($this->FontFamily =='') $this->SetFont('Arial', '', 8);
        // if column widths not set
        if (!isset($this->tablewidths)) {

            // starting col width
            $this->sColWidth = (($this->w - $this->lMargin - $this->rMargin)) / $this->numFields;

            // loop through results header and set initial col widths/ titles/ alignment
            // if a col title is less than the starting col width / reduce that column size
            for ($i = 0; $i < $this->numFields; $i++) {
                $stringWidth = $this->getstringwidth(mysql_field_name($this->results, $i)) + 6;
                if (($stringWidth) < $this->sColWidth) {
                    $colFits[$i] = $stringWidth;
                    // set any column titles less than the start width to the column title width
                }
                $this->colTitles[$i] = mysql_field_name($this->results, $i);
                switch (mysql_field_type($this->results, $i)) {
                    case 'int':
                        $this->colAlign[$i] = 'R';
                        break;
                     case 'real'://JPG
                        $this->colAlign[$i] = 'R';
                        break;
                    default:
                        $this->colAlign[$i] = 'L';
                }
            }

            // loop through the data, any column whose contents is bigger that the col size is
            // resized
            while ($row = mysql_fetch_row($this->results)) {
                foreach ($colFits as $key => $val) {
                    $stringWidth = $this->Getstringwidth($row[$key]) + 6;
                    if (($stringWidth) > $this->sColWidth) {
                        // any col where row is bigger than the start width is now discarded
                        unset($colFits[$key]);
                    } else {
                        // if text is not bigger than the current column width setting enlarge the column
                        if (($stringWidth) > $val) {
                            $colFits[$key] = ($stringWidth);
                        }
                    }
                }
            }

            foreach ($colFits as $key => $val) {
                // set fitted columns to smallest size
                $this->tablewidths[$key] = $val;
                // to work out how much (if any) space has been freed up
                $totAlreadyFitted += $val;
            }

            $surplus = (sizeof($colFits) * $this->sColWidth) - ($totAlreadyFitted);
            for ($i = 0; $i < $this->numFields; $i++) {
                if (!in_array($i, array_keys($colFits))) {
                    $this->tablewidths[$i] = $this->sColWidth + ($surplus / (($this->numFields) - sizeof($colFits)));
                }
            }

            ksort($this->tablewidths);

            if ($dump) {
                Header('Content-type: text/plain');
                for ($i = 0; $i < $this->numFields; $i++) {
                    if (strlen(mysql_field_name($this->results, $i)) > $flength) {
                        $flength = strlen(mysql_field_name($this->results, $i));
                    }
                }
                switch ($this->k) {
                    case 72 / 25.4:
                        $unit = 'millimeters';
                        break;
                    case 72 / 2.54:
                        $unit = 'centimeters';
                        break;
                    case 72:
                        $unit = 'inches';
                        break;
                    default:
                        $unit = 'points';
                }
                print "All measurements in $unit\n\n";
                for ($i = 0; $i < $this->numFields; $i++) {
                    printf("%-{$flength}s : %-10s : %10f\n",
                            mysql_field_name($this->results, $i),
                            mysql_field_type($this->results, $i),
                            $this->tablewidths[$i]);
                }
                print "\n\n";
                print "\$pdf->tablewidths=\n\tarray(\n\t\t";
                for ($i = 0; $i < $this->numFields; $i++) {
                    ($i < ($this->numFields - 1)) ?
                                    print $this->tablewidths[$i] . ", /* " . mysql_field_name($this->results, $i) . " */\n\t\t" :
                                    print $this->tablewidths[$i] . " /* " . mysql_field_name($this->results, $i) . " */\n\t\t";
                }
                print "\n\t);\n";
                exit;
            }
        } else { // end of if tablewidths not defined
            for ($i = 0; $i < $this->numFields; $i++) {
                $this->colTitles[$i] = mysql_field_name($this->results, $i);
                switch (mysql_field_type($this->results, $i)) {
                    case 'int':
                        $this->colAlign[$i] = 'R';
                        break;
                    case 'real'://JPG
                        $this->colAlign[$i] = 'R';
                        break;
                    default:
                        $this->colAlign[$i] = 'L';
                }
            }
        }

        if($this->numRows){
            mysql_data_seek($this->results, 0);
            $this->AliasNbPages();
//            $this->SetY($this->tMargin);
            $this->AddPage();
            $this->morepagestable($this->FontSizePt);
        }
    }
    function mysql_report_image($query, $dump=false, $lineheight=0, $img_h = 0, $img_array = array(), $img_default = 'no_img.jpg') {

        $this->query($query);

        if($this->FontFamily =='') $this->SetFont('Arial', '', 8);
        // if column widths not set
        if (!(isset($this->tablewidths))) {

            // starting col width
            $this->sColWidth = (($this->w - $this->lMargin - $this->rMargin)) / $this->numFields;

            // loop through results header and set initial col widths/ titles/ alignment
            // if a col title is less than the starting col width / reduce that column size
            for ($i = 0; $i < $this->numFields; $i++) {
                $stringWidth = $this->getstringwidth(mysql_field_name($this->results, $i)) + 6;
                if (($stringWidth) < $this->sColWidth) {
                    $colFits[$i] = $stringWidth;
                    // set any column titles less than the start width to the column title width
                }
                $this->colTitles[$i] = mysql_field_name($this->results, $i);
                switch (mysql_field_type($this->results, $i)) {
                    case 'int':
                        $this->colAlign[$i] = 'R';
                        break;
                     case 'real'://JPG
                        $this->colAlign[$i] = 'R';
                        break;
                    default:
                        $this->colAlign[$i] = 'L';
                }
            }

            // loop through the data, any column whose contents is bigger that the col size is
            // resized
            while ($row = mysql_fetch_row($this->results)) {
                foreach ($colFits as $key => $val) {
                    $stringWidth = $this->Getstringwidth($row[$key]) + 6;
                    if (($stringWidth) > $this->sColWidth) {
                        // any col where row is bigger than the start width is now discarded
                        unset($colFits[$key]);
                    } else {
                        // if text is not bigger than the current column width setting enlarge the column
                        if (($stringWidth) > $val) {
                            $colFits[$key] = ($stringWidth);
                        }
                    }
                }
            }

            foreach ($colFits as $key => $val) {
                // set fitted columns to smallest size
                $this->tablewidths[$key] = $val;
                // to work out how much (if any) space has been freed up
                $totAlreadyFitted += $val;
            }

            $surplus = (sizeof($colFits) * $this->sColWidth) - ($totAlreadyFitted);
            for ($i = 0; $i < $this->numFields; $i++) {
                if (!in_array($i, array_keys($colFits))) {
                    $this->tablewidths[$i] = $this->sColWidth + ($surplus / (($this->numFields) - sizeof($colFits)));
                }
            }

            ksort($this->tablewidths);

            if ($dump) {
                Header('Content-type: text/plain');
                for ($i = 0; $i < $this->numFields; $i++) {
                    if (strlen($this->colTitles[$i]) > $flength) {
                        $flength = strlen($this->colTitles[$i]);
                    }
                }
                switch ($this->k) {
                    case 72 / 25.4:
                        $unit = 'millimeters';
                        break;
                    case 72 / 2.54:
                        $unit = 'centimeters';
                        break;
                    case 72:
                        $unit = 'inches';
                        break;
                    default:
                        $unit = 'points';
                }
                print "All measurements in $unit\n\n";
//                for ($i = 0; $i < $this->numFields; $i++) {
//                    printf("%-{$flength}s : %-10s : %10f\n",
//                            mysql_field_name($this->results, $i),
//                            mysql_field_type($this->results, $i),
//                            $this->tablewidths[$i]);
//                }
                $txt_Titles = "\n\n";
                $txt_Titles .= "\$pdf->colTitles=\n\tarray(\n\t\t";
                $txt_widths = "\n\n";
                $txt_widths .= "\$pdf->tablewidths=\n\tarray(\n\t\t";
                $txt_Align = "\n\n";
                $txt_Align .= "\$pdf->colAlign=\n\tarray(\n\t\t";
                for ($i = 0; $i < $this->numFields; $i++) {
                    printf("%-{$flength}s : %-10s : %10f\n",
                            mysql_field_name($this->results, $i),
                            mysql_field_type($this->results, $i),
                            $this->tablewidths[$i]);
                    $txt_Titles .=($i < ($this->numFields - 1)) ?
                                    "'". $this->colTitles[$i] . "', /* " . $this->colTitles[$i] . " */\n\t\t" :
                                    "'". $this->colTitles[$i] . "' /* " . $this->colTitles[$i] . " */";
                    $txt_widths .= ($i < ($this->numFields - 1)) ?
                                    $this->tablewidths[$i] . ", /* " . $this->colTitles[$i] . " */\n\t\t" :
                                    $this->tablewidths[$i] . " /* " . $this->colTitles[$i] . " */";
                    $txt_Align .= ($i < ($this->numFields - 1)) ?
                                    "'". $this->colAlign[$i] . "', /* " . $this->colTitles[$i] . " */\n\t\t" :
                                    "'". $this->colAlign[$i] . "' /* " . $this->colTitles[$i] . " */";
                }
                $txt_Titles .= "\n\t);\n";
                $txt_widths .= "\n\t);\n";
                $txt_Align .= "\n\t);\n";
                echo $txt_Titles, $txt_widths,$txt_Align;
                exit;
            }
        }
        else if(!isset( $this->colTitles) && !isset(  $this->colAlign)){ // end of if tablewidths not defined
            for ($i = 0; $i < $this->numFields; $i++) {
                $this->colTitles[$i] = mysql_field_name($this->results, $i);
                switch (mysql_field_type($this->results, $i)) {
                    case 'int':
                        $this->colAlign[$i] = 'R';
                        break;
                    case 'real'://JPG
                        $this->colAlign[$i] = 'R';
                        break;
                    default:
                        $this->colAlign[$i] = 'L';
                }
            }
        }

        if($this->numRows){
            mysql_data_seek($this->results, 0);
            $this->AliasNbPages();
            $this->SetY($this->tMargin);
            $this->AddPage();
            for ($col = 0; $col < $this->numFields; $col++) {
                if(in_array($this->colTitles[$col], $img_array) && empty($img_h)){
                    $img_h = 30;//$this->tablewidths[$col];
                }
            }
            $this->morepagestableimage($lineheight,$img_h,$img_array, $img_default);
        }
    }
        /**
     *
     * @param type $row
     * @param type $cellh
     * @param type $widths
     * @param type $aligns 
     */
    function Row($row, $cellh = 4, $widths = array(), $aligns = array()) {
        $nb = 0;
        for ($i = 0; $i < count($row); $i++)
            $nb = max($nb, $this->NbLines(empty($widths) ? $this->widths[$i] : $widths[$i], $row[$i]));
        $h = $cellh * $nb;
        $this->CheckPageBreak($h);
        //for ($i = 0; $i < count($row); $i++) {
        foreach ($row as $i => $value) {
            $w = empty($widths) ? $this->widths[$i] : $widths[$i];
            $a = empty($aligns) ? (isset($this->aligns[$i]) ? $this->aligns[$i] : 'L') : (isset($aligns[$i]) ? $aligns[$i] : 'L');
            $x = $this->GetX();
            $y = $this->GetY();
            $this->Rect($x, $y, $w, $h);
            $this->MultiCell($w, $cellh, $row[$i], 0, $a);
            $this->SetXY($x + $w, $y);
        }
        $this->Ln($h);
    }

    /**
     *
     * @param type $h 
     */
    function CheckPageBreak($h) {
        if ($this->GetY() + $h > $this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    /**
     *
     * @param type $w
     * @param type $txt
     * @return int 
     */
    function NbLines($w, $txt) {
        $cw = &$this->CurrentFont['cw'];
        if ($w == 0)
            $w = $this->w - $this->rMargin - $this->x;
        $wmax = ($w - 2 * $this->cMargin) * 1000 / $this->FontSize;
        $s = str_replace("\r", '', $txt);
        $nb = strlen($s);
        if ($nb > 0 and $s[$nb - 1] == "\n")
            $nb--;
        $sep = -1;
        $i = 0;
        $j = 0;
        $l = 0;
        $nl = 1;
        while ($i < $nb) {
            $c = $s[$i];
            if ($c == "\n") {
                $i++;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
                continue;
            }
            if ($c == ' ')
                $sep = $i;
            $l+=$cw[$c];
            if ($l > $wmax) {
                if ($sep == -1) {
                    if ($i == $j)
                        $i++;
                }
                else
                    $i=$sep + 1;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
?>