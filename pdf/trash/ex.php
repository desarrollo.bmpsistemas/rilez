<?php
require('fpdf.php');
require('PDF_mysqlReport.php');
require 'pdf_helper.php';
$pdf = new PDF_mysqlReport();
$pdf->AutoPrint(true);
//$pdf->SetFont('Arial','',8);
$pdf->setLogo('Logo.JPG');
$pdf->setParameter('logoh', .90);
$pdf->setAuthor("Hard & Soft Computacional");
$pdf->setParameter('authorFontSize', 14);
$pdf->setParameter('authorFontStyle', 'BIU');
//$pdf->setAddress("Enrique Felix Castro No. 2737\n Col. Humaya\nCuliacán, Sinaloa 7508800");
//$pdf->setParameter('addressFontSize', 10);
$pdf->setTitle("Clientes");
//$pdf->setParameter('titleFontSize', 13);
$pdf->setSubject("Detalles");
//$pdf->setParameter('subjectFontSize', 10);
//$pdf->setParameter('cellFontSize', 10);
$pdf->setParameter('sum', array('DESCUENTO'=>0, 'ID'=>0));
$pdf->setParameter('DESCUENTOBorder', 1);
$pdf->setParameter('IDBorder', 1);
$pdf->connect('localhost','root','root','rilez');
$pdf->mysql_report("SELECT id AS ID, rfc AS RFC, descripcion AS DESCRIPCION, nomcomercial AS NOMBRE, telefono AS TELEFONO, descuento AS DESCUENTO FROM clientes LIMIT 100");
//$pdf->AutoPrint(true);
//$pdf->Output();
$buf=$pdf->Output("", "S");
unset($pdf);
$print_headers = true;
if ($print_headers) {
    $len = strlen($buf);
    header('Cache-Control: no-cache, no-store, max-age=0, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Pragma: no-cache');

    header("Content-type: application/pdf");
    header("Content-Length: $len");
    header("Content-Disposition: inline; filename=pdf.pdf");
    echo $buf;
    exit;
}
echo $buf;
//CleanFiles('.');
////Determinar un nombre temporal de fichero en el directorio actual
//$file = 'reporte';//basename(tempnam('.', 'tmp'));
//rename($file, $file . '.pdf');
//$file .= '.pdf';
////Guardar el PDF en un fichero
//$pdf->Output($file, 'F');
////Redirección
//header('Location: ' . $file);
?>