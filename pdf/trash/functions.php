<?php
/**
 *
 * @param type $haystack
 * @param type $char_list
 * @return type 
 */
function strpbrkpos($haystack, $char_list) {
     $result = strcspn($haystack, $char_list);
     if ($result != strlen($haystack)) {
         return $result;
     }
     return false;
 }
 // array mysql_fetch_all(query:resource [, kind:string (default:'assoc' | 'row')])
 function mysql_fetch_all($query, $kind = 'assoc') {
     $result = array();
     $kind = $kind === 'assoc' ? $kind : 'row';
     eval('while(@$r = mysql_fetch_'.$kind.'($query)) array_push($result, $r);');
     return $result;
 }
 function nextautoindex($table) {
    $res = mysql_query("SHOW TABLE STATUS");
    if (@mysql_num_rows($res) > 0) {
        while ($row = @mysql_fetch_array($res, MYSQL_ASSOC)) {
            if ($table == $row["name"])
                return $row["Auto_increment"];
        }
    }
    return false;
}

/* Example usage: 
  $newID = nextautoindex("user"); // output: 12

  // Now you can insert multiple related records without guessing the id
 */
/*
 * function from_array($x=null) {
   if(!isset($x)) return null;
   else if(is_string($x)) return mysql_real_escape_string($x);
   else if(is_array($x)) {
     foreach($x as $k=>$v) {
       $k2=mysql_real_escape_string($k);
       if($k!=$k2) unset($x[$k]);
       $x[$k2]=from_array($v);
     }
     return $x;
   }
 }
 
// call it with $x=from_array($_POST['key_name']);
 // or even $_POST=from_array($_POST); 
 * 
 */
/*
 * // copy content of the record you wish to clone 
$entity = mysql_fetch_array(mysql_query("SELECT * FROM table_name WHERE id='$id_to_be cloned'"), MYSQL_ASSOC) or die("Could not select original record");
 
// set the auto-incremented id's value to blank. If you forget this step, nothing will work because we can't have two records with the same id
 $entity["id"] = ""; 

// insert cloned copy of the original  record 
mysql_query("INSERT INTO table_name (".implode(", ",array_keys($entity)).") VALUES ('".implode("', '",array_values($entity))."')");
 
// if you want the auto-generated id of the new cloned record, do the following 
$newid = mysql_insert_id();
 */
/*
 * Here's a quick way to duplicate or clone a record to the same table using only 4 lines of code:
 
// first, get the highest id number, so we can calc the new id number for the dupe
 // second, get the original entity
 // third, increment the dupe record id to 1 over the max
 // finally insert the new record - voila - 4 lines!
 
$id_max = mysql_result(mysql_query("SELECT MAX(id) FROM table_name"),0,0) or die("Could not execute query"); 
$entity = mysql_fetch_array(mysql_query("SELECT * FROM table." WHERE id='$id_original'),MYSQL_ASSOC) or die("Could not select original record"); // MYSQL_ASSOC forces a purely associative array and blocks twin key dupes, vitally, it brings the keys out so they can be used in line 4
 $entity["id"]=$id_max+1; 
mysql_query("INSERT INTO it_pages (".implode(", ",array_keys($Entity)).") VALUES ('".implode("', '",array_values($Entity))."')");
 
 */
/*
  * Para enviar mensajes de textos a un celular Telcel:

http://www.telcel.com/portal/telcel.portal;jsessionid=GpFGc4pTyQn7NyBSLL6yBhNbvDtsfJxzdpQyc1CxQWc12w6bgLB9!-932978899?_nfpb=true&_pageLabel=GRAL_DespliegueContenidoPage&seccion=47&pathContenido=/contenido/mensajes_escritos.html

Unefon:

http://www.iusacell.com.mx/

Para enviar mensajes a móviles Nextel de México:

http://mexmessag.nextelinternational.com/cgi/iPageExt.dll?cmd=buildIndAddressPage

Para celulares de Telefónica Movistar:

http://www.movistar.com.mx/home/index.html

Iusacell:

http://www.iusacell.com.mx/

Para enviar mensajes a celulares Coditel:

http://www.coditel.com.mx/envio.php
 * 
 * <? 
if (($mail) 
&& ($numero) && ($mensaje) && ($submit)) { 
mail("$numero@correo.movistar.net","",$mensaje,"From:

$mail\r\nReply-To: $mail\r\n"); 

echo "<b>Mensaje enviado al servidor</b><br>"; 
} 

?> 
<form method="post" action="sms.php">

<input type="text" name="mail" value="e-mail"><br> 
<input type="text" name="numero" value="numero"><br> 

<input type="text" name="mensaje" value="mensaje"><br> 
<input type="submit" value="enviar" name="submit"> 

</form>
  */
?>
