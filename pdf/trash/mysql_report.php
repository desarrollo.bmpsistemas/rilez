<?php
class PDF_Javascript extends FPDF {

    var $javascript;
    var $n_js;

    function __construct($orientation='P',$uni='mm',$format='Letter') {
        parent::__construct($orientation,$uni,$format);
    }
    function IncludeJS($script) {
        $this->javascript=$script;
    }
    function _putjavascript() {
        $this->_newobj();
        $this->n_js=$this->n;
        $this->_out('<<');
        $this->_out('/Names [(EmbeddedJS) '.($this->n+1).' 0 R ]');
        $this->_out('>>');
        $this->_out('endobj');
        $this->_newobj();
        $this->_out('<<');
        $this->_out('/S /JavaScript');
        $this->_out('/JS '.$this->_textstring($this->javascript));
        $this->_out('>>');
        $this->_out('endobj');
    }
    function _putresources() {
        parent::_putresources();
        if (!empty($this->javascript)) {
            $this->_putjavascript();
        }
    }
    function _putcatalog() {
        parent::_putcatalog();
        if (isset($this->javascript)) {
            $this->_out('/Names <</JavaScript '.($this->n_js).' 0 R>>');
        }
    }
}

class PDF extends PDF_Javascript {

    var $tablewidths;
    var $headerset;
    var $footerset;
    function PDF($orientation='P', $unit='pt', $format='Letter'){
        parent::__construct($orientation, $unit, $format);
        $this->creator="JPGIT-mysql_report";
//        $this->setFont('Arial', '', 8);
    }

    function AutoPrint($dialog=false)
    {
        $param=($dialog ? 'true' : 'false');
        $script="print(".$param.");";
        $this->IncludeJS($script);
    }

    function setLogo($logo='logo_pb.png') {
        $this->logo = $logo;
        $this->logoh = 1;
    }

    function setAuthor($empresa = "Empresa") {
        parent::setAuthor(utf8_decode($empresa));
        $this->authorW=0;
        $this->authorH=0;
        $this->authorBorder=0;
        $this->authorLn=1;
        $this->authorAlign='C';
    }

    function setAddress($domicilio = "Domicilio") {
        $this->address = utf8_decode($domicilio);
        $this->addressW=0;
        $this->addressH=0;
        $this->addressBorder=0;
        $this->addressLn=1;
        $this->addressAlign='C';
    }

    function setTitle($titulo="Titulo") {
        parent::setTitle(utf8_decode($titulo));
        $this->titleW=0;
        $this->titleH=0;
        $this->titleBorder=0;
        $this->titleLn=1;
        $this->titleAlign='C';
    }

    function setSubject($asunto="Asunto"){
        parent::setSubject(utf8_decode($asunto));
        $this->subjectW=0;
        $this->subjectH=0;
        $this->subjectBorder=0;
        $this->subjectLn=1;
        $this->subjectAlign='C';
    }

    function setParameter($parameter, $val){
        $this->$parameter = is_string($val)?utf8_decode($val): $val;
    }
    function _beginpage($orientation, $format) {
        $this->page++;
        if (!$this->pages[$this->page]) // solves the problem of overwriting a page if it already exists
            $this->pages[$this->page] = '';
        $this->state = 2;
        $this->x = $this->lMargin;
        $this->y = $this->tMargin;
        $this->FontFamily = '';
        //Check page size
        if ($orientation == '')
            $orientation = $this->DefOrientation;
        else
            $orientation=strtoupper($orientation[0]);
        if ($format == '')
            $format = $this->DefPageFormat;
        else {
            if (is_string($format))
                $format = $this->_getpageformat($format);
        }
        if ($orientation != $this->CurOrientation || $format[0] != $this->CurPageFormat[0] || $format[1] != $this->CurPageFormat[1]) {
            //New size
            if ($orientation == 'P') {
                $this->w = $format[0];
                $this->h = $format[1];
            } else {
                $this->w = $format[1];
                $this->h = $format[0];
            }
            $this->wPt = $this->w * $this->k;
            $this->hPt = $this->h * $this->k;
            $this->PageBreakTrigger = $this->h - $this->bMargin;
            $this->CurOrientation = $orientation;
            $this->CurPageFormat = $format;
        }
        if ($orientation != $this->DefOrientation || $format[0] != $this->DefPageFormat[0] || $format[1] != $this->DefPageFormat[1])
            $this->PageSizes[$this->page] = array($this->wPt, $this->hPt);
    }

    function Header() {
        global $maxY;
        // Check if header for this page already exists
        if (!$this->headerset[$this->page]) {
            $plbl = "cell";
            $pFontFamily=$plbl."FontFamily";$pFontStyle=$plbl."FontStyle";$pFontSize=$plbl."FontSize";
            $this->SetFont($this->$pFontFamily?$this->$pFontFamily:'Arial', $this->$pFontStyle, $this->$pFontSize?$this->$pFontSize:8);
            $this->cellFontFamily = $this->FontFamily;
            $this->cellFontSize = $this->FontSizePt;
            $this->setY(0);
            $plbl="author";
            if(isset ($this->$plbl)){//JPG
                $pFontFamily=$plbl."FontFamily";$pFontStyle=$plbl."FontStyle";$pFontSize=$plbl."FontSize";
                $this->SetFont($this->$pFontFamily?$this->$pFontFamily:$this->cellFontFamily, $this->$pFontStyle, $this->$pFontSize?$this->$pFontSize:$this->cellFontSize);
                $pW=$plbl."W";$pH=$plbl."H";$pBorder=$plbl."Border";$pLn=$plbl."Ln";$pAlign=$plbl."Align";
                $this->Cell($this->$pW, $this->$pH?$this->$pH:$this->FontSizePt, $this->$plbl, $this->$pBorder, $this->$pLn, $this->$pAlign );//JPG
            }

            $plbl="address";
            if(isset ($this->$plbl)){//JPG
                $pFontFamily=$plbl."FontFamily";$pFontStyle=$plbl."FontStyle";$pFontSize=$plbl."FontSize";
                $this->SetFont($this->$pFontFamily?$this->$pFontFamily:$this->cellFontFamily, $this->$pFontStyle, $this->$pFontSize?$this->$pFontSize:$this->cellFontSize);
                $pW=$plbl."W";$pH=$plbl."H";$pBorder=$plbl."Border";$pLn=$plbl."Ln";$pAlign=$plbl."Align";
                $this->MultiCell($this->$pW, $this->$pH?$this->$pH:$this->FontSizePt, $this->$plbl, $this->$pBorder, $this->$pAlign);
            }

            $plbl="title";
            if(isset ($this->$plbl)){//JPG
                $pFontFamily=$plbl."FontFamily";$pFontStyle=$plbl."FontStyle";$pFontSize=$plbl."FontSize";
                $this->SetFont($this->$pFontFamily?$this->$pFontFamily:$this->cellFontFamily, $this->$pFontStyle, $this->$pFontSize?$this->$pFontSize:$this->cellFontSize);
                $pW=$plbl."W";$pH=$plbl."H";$pBorder=$plbl."Border";$pLn=$plbl."Ln";$pAlign=$plbl."Align";
                $this->Cell($this->$pW, $this->$pH?$this->$pH:$this->FontSizePt, $this->$plbl, $this->$pBorder, $this->$pLn, $this->$pAlign );//JPG
            }

            $plbl = "subject";
            if(isset ($this->$plbl)){//JPG
                $pFontFamily=$plbl."FontFamily";$pFontStyle=$plbl."FontStyle";$pFontSize=$plbl."FontSize";
                $this->SetFont($this->$pFontFamily?$this->$pFontFamily:$this->cellFontFamily, $this->$pFontStyle, $this->$pFontSize?$this->$pFontSize:$this->cellFontSize);
                $pW=$plbl."W";$pH=$plbl."H";$pBorder=$plbl."Border";$pLn=$plbl."Ln";$pAlign=$plbl."Align";
                $this->MultiCell($this->$pW, $this->$pH?$this->$pH:$this->FontSizePt, $this->$plbl, $this->$pBorder, $this->$pAlign);
            }
            if($this->tMargin < $this->getY()){
                $this->tMargin = $this->getY();
            }
            //Logo
            if (isset($this->logo)){
                //$this->setX($this->tMargin + $this->getY());
                $lh=$this->logoh>1?$this->logoh: $this->tMargin*$this->logoh;
                $this->tMargin = $this->logoh> $this->tMargin?$this->logoh:$this->tMargin;
                $this->setY(($this->tMargin - $lh) / 2);
                $this->Image($this->logo,  $this->lMargin + $this->y, $this->y, 0, $lh);
            }

            foreach ($this->tablewidths as $width) {
                $fullwidth += $width;
            }
            $l = ($this->lMargin);
            $plbl = "cellHeader";
            $pFontFamily=$plbl."FontFamily";$pFontStyle=$plbl."FontStyle";$pFontSize=$plbl."FontSize";
            $this->SetFont($this->$pFontFamily?$this->$pFontFamily:$this->cellFontFamily, $this->$pFontStyle, $this->$pFontSize?$this->$pFontSize:$this->cellFontSize);
            //$this->SetFont('Arial', '', $this->cellFontSize);//JPG
            foreach ($this->colTitles as $col => $txt) {
                $this->SetXY($l, ($this->tMargin));
                $this->MultiCell($this->tablewidths[$col], $this->FontSizePt, $txt);
                $l += $this->tablewidths[$col];
                $maxY = ($maxY < $this->getY()) ? $this->getY() : $maxY;
            }
            $this->SetXY($this->lMargin, $this->tMargin);
            $this->setFillColor(200, 200, 200);
            $l = ($this->lMargin);
            foreach ($this->colTitles as $col => $txt) {
                $this->SetXY($l, $this->tMargin);
                $this->cell($this->tablewidths[$col], $maxY - ($this->tMargin), '', 1, 0, 'L', 1);
                $this->SetXY($l, $this->tMargin);
                $this->MultiCell($this->tablewidths[$col], $this->FontSizePt, $txt, 0, 'C');
                $l += $this->tablewidths[$col];
            }
            $this->setFillColor(255, 255, 255);
            // set headerset
            $this->headerset[$this->page] = 1;
            
        }

        $this->SetY($maxY);
    }

    function Footer() {
        // Check if footer for this page already exists
        if (!$this->footerset[$this->page]) {
            $this->SetY(-15);
            //Page number
            $this->Cell(0, 10, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
            // set footerset
            $this->footerset[$this->page] = 1;
        }
    }

    function morepagestable($lineheight=8) {
        // some things to set and 'remember'
        $l = $this->lMargin;
        $startheight = $h = $this->GetY();
        $startpage = $currpage = $this->page;

        // calculate the whole width
        foreach ($this->tablewidths as $width) {
            $fullwidth += $width;
        }

        // Now let's start to write the table
        $row = 0;
        while ($data = mysql_fetch_row($this->results)) {
            $this->page = $currpage;
            // write the horizontal borders
            $this->Line($l, $h, $fullwidth + $l, $h);
            // write the content and remember the height of the highest col
            foreach ($data as $col => $txt) {

                $this->page = $currpage;
                $this->SetXY($l, $h);
                $this->MultiCell($this->tablewidths[$col], $lineheight, $txt, 0, $this->colAlign[$col]);

                $l += $this->tablewidths[$col];

                if ($tmpheight[$row . '-' . $this->page] < $this->GetY()) {
                    $tmpheight[$row . '-' . $this->page] = $this->GetY();
                }
                if ($this->page > $maxpage)
                    $maxpage = $this->page;
                //suma las cantidades de las columnas jpg
                if(isset($this->sum)){
                    if(isset($this->sum[$this->colTitles[$col]])){
                        $this->sum[$this->colTitles[$col]]+=$txt;
                    }
                }
                unset($data[$col]);
            }
            // get the height we were in the last used page
            $h = $tmpheight[$row . '-' . $maxpage];
            // set the "pointer" to the left margin
            $l = $this->lMargin;
            // set the $currpage to the last page
            $currpage = $maxpage;
            unset($data[$row]);
            $row++;
        }

        // draw the borders
        // we start adding a horizontal line on the last page
        $this->page = $maxpage;
        $this->Line($l, $h, $fullwidth + $l, $h);
        // now we start at the top of the document and walk down
        for ($i = $startpage; $i <= $maxpage; $i++) {
            $this->page = $i;
            $l = $this->lMargin;
            $t = ($i == $startpage) ? $startheight : $this->tMargin;
            $lh = ($i == $maxpage) ? $h : $this->h - $this->bMargin;
            $this->Line($l, $t, $l, $lh);//1ra vertical
            foreach ($this->tablewidths as $width) {
                $l += $width;
                $this->Line($l, $t, $l, $lh);//vertical
            }
        }
        // set it to the last page, if not it'll cause some problems
        $this->page = $maxpage;
        //draw the sum('s)
        if (isset($this->sum)) {
            $l = ($this->lMargin);
            //$this->SetFont('Arial', '', $this->cellFontSize);
            foreach ($this->colTitles as $col => $txt) {
                $this->SetXY($l, $h);
                if ($this->sum[$txt]) {
                    $txtBorder = $txt.'Border';
//                    $this->$txtBorder;
                    $this->MultiCell($this->tablewidths[$col], $this->FontSizePt, $this->sum[$txt], $this->$txtBorder, $this->colAlign[$col]);
                }
                $l += $this->tablewidths[$col];
                //$maxY = ($maxY < $this->getY()) ? $this->getY() : $maxY;
            }
        }
    }

    function connect($host='localhost', $username='', $password='', $db='') {
        $this->conn = mysql_connect($host, $username, $password) or die(mysql_error());
        mysql_select_db($db, $this->conn) or die(mysql_error());
        return true;
    }

    function query($query) {
        $this->results = mysql_query($query, $this->conn);
        $this->numFields = mysql_num_fields($this->results);
    }

    function mysql_report($query, $dump=false, $attr=array()) {

        foreach ($attr as $key => $val) {
            $this->$key = is_string($val)?utf8_decode($val): $val;
        }
        $this->query($query);

        if($this->FontFamily =='') $this->setFont('Arial', '', 8);
        // if column widths not set
        if (!isset($this->tablewidths)) {

            // starting col width
            $this->sColWidth = (($this->w - $this->lMargin - $this->rMargin)) / $this->numFields;

            // loop through results header and set initial col widths/ titles/ alignment
            // if a col title is less than the starting col width / reduce that column size
            for ($i = 0; $i < $this->numFields; $i++) {
                $stringWidth = $this->getstringwidth(mysql_field_name($this->results, $i)) + 6;
                if (($stringWidth) < $this->sColWidth) {
                    $colFits[$i] = $stringWidth;
                    // set any column titles less than the start width to the column title width
                }
                $this->colTitles[$i] = mysql_field_name($this->results, $i);
                switch (mysql_field_type($this->results, $i)) {
                    case 'int':
                        $this->colAlign[$i] = 'R';
                        break;
                     case 'real'://JPG
                        $this->colAlign[$i] = 'R';
                        break;
                    default:
                        $this->colAlign[$i] = 'L';
                }
            }

            // loop through the data, any column whose contents is bigger that the col size is
            // resized
            while ($row = mysql_fetch_row($this->results)) {
                foreach ($colFits as $key => $val) {
                    $stringWidth = $this->getstringwidth($row[$key]) + 6;
                    if (($stringWidth) > $this->sColWidth) {
                        // any col where row is bigger than the start width is now discarded
                        unset($colFits[$key]);
                    } else {
                        // if text is not bigger than the current column width setting enlarge the column
                        if (($stringWidth) > $val) {
                            $colFits[$key] = ($stringWidth);
                        }
                    }
                }
            }

            foreach ($colFits as $key => $val) {
                // set fitted columns to smallest size
                $this->tablewidths[$key] = $val;
                // to work out how much (if any) space has been freed up
                $totAlreadyFitted += $val;
            }

            $surplus = (sizeof($colFits) * $this->sColWidth) - ($totAlreadyFitted);
            for ($i = 0; $i < $this->numFields; $i++) {
                if (!in_array($i, array_keys($colFits))) {
                    $this->tablewidths[$i] = $this->sColWidth + ($surplus / (($this->numFields) - sizeof($colFits)));
                }
            }

            ksort($this->tablewidths);

            if ($dump) {
                Header('Content-type: text/plain');
                for ($i = 0; $i < $this->numFields; $i++) {
                    if (strlen(mysql_field_name($this->results, $i)) > $flength) {
                        $flength = strlen(mysql_field_name($this->results, $i));
                    }
                }
                switch ($this->k) {
                    case 72 / 25.4:
                        $unit = 'millimeters';
                        break;
                    case 72 / 2.54:
                        $unit = 'centimeters';
                        break;
                    case 72:
                        $unit = 'inches';
                        break;
                    default:
                        $unit = 'points';
                }
                print "All measurements in $unit\n\n";
                for ($i = 0; $i < $this->numFields; $i++) {
                    printf("%-{$flength}s : %-10s : %10f\n",
                            mysql_field_name($this->results, $i),
                            mysql_field_type($this->results, $i),
                            $this->tablewidths[$i]);
                }
                print "\n\n";
                print "\$pdf->tablewidths=\n\tarray(\n\t\t";
                for ($i = 0; $i < $this->numFields; $i++) {
                    ($i < ($this->numFields - 1)) ?
                                    print $this->tablewidths[$i] . ", /* " . mysql_field_name($this->results, $i) . " */\n\t\t" :
                                    print $this->tablewidths[$i] . " /* " . mysql_field_name($this->results, $i) . " */\n\t\t";
                }
                print "\n\t);\n";
                exit;
            }
        } else { // end of if tablewidths not defined
            for ($i = 0; $i < $this->numFields; $i++) {
                $this->colTitles[$i] = mysql_field_name($this->results, $i);
                switch (mysql_field_type($this->results, $i)) {
                    case 'int':
                        $this->colAlign[$i] = 'R';
                        break;
                    case 'real'://JPG
                        $this->colAlign[$i] = 'R';
                        break;
                    default:
                        $this->colAlign[$i] = 'L';
                }
            }
        }

        mysql_data_seek($this->results, 0);
        $this->AliasNbPages();
        $this->SetY($this->tMargin);
        $this->AddPage();
        $this->morepagestable($this->FontSizePt);
    }
}
?>