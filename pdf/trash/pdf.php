<?php

require('fpdf.php');
require_once 'pdf_helper.php';

class PDF extends FPDF {

    private $logo;
    public $domicilio;
    
    function setLogo($logo='logo_pb.png'){
        $this->logo = $logo;
    }
    function setEmpresa($empresa = "Empresa"){
        parent::setAuthor(utf8_decode($empresa));
    }
    function setDomicilio($domicilio = "Domicilio"){
        $this->domicilio=utf8_decode($domicilio);
    }
    function setTitulo($titulo="Titulo"){
        parent::setTitle(utf8_decode($titulo));
    }

//Cabecera de página
    function Header() {
        //Empresa
        if (isset($this->author)) {
            //Arial bold 15
            $this->SetFont('Arial', 'B', 15);
            //Calculamos ancho y posición del título.
            $w = $this->GetStringWidth($this->author) + 6;
            $this->SetX((210 - $w) / 2);
            //Colores de los bordes, fondo y texto
            $this->SetDrawColor(0, 80, 180);
            $this->SetFillColor(256, 256, 256);
            $this->SetTextColor(0, 0, 0);
            //Ancho del borde (1 mm)
            $this->SetLineWidth(1);
            //Título
            $this->Cell($w, 4, $this->author, 0, 1, 'C', true);
            //Salto de línea
            $this->Ln(1);
        }
        if (isset($this->domicilio)) {
            //Arial bold 5
            $this->SetFont('Arial', '', 5);
            //Colores de los bordes, fondo y texto
            $this->SetDrawColor(0, 80, 180);
            $this->SetFillColor(256, 256, 256);
            $this->SetTextColor(0, 0, 0);
            //Ancho del borde (1 mm)
            $this->SetLineWidth(.1);
            //Domicilio
            $this->MultiCell(0, 1.5, utf8_decode($this->domicilio), 'B', 'C');
        }
        //Logo
        if (isset($this->logo))
            $this->Image($this->logo, 10, 5, 0, $this->getY()- 5);
        //Salto de línea
        $this->Ln(5);
        if(isset($this->title)){
        //Arial bold 15
            $this->SetFont('Arial', 'B', 15);
            //Calculamos ancho y posición del título.
            $w = $this->GetStringWidth($this->title) + 6;
            $this->SetX((210 - $w) / 2);
            //Colores de los bordes, fondo y texto
            $this->SetDrawColor(0, 80, 180);
            $this->SetFillColor(256, 256, 256);
            $this->SetTextColor(0, 0, 0);
            //Ancho del borde (1 mm)
            $this->SetLineWidth(1);
            //Título
            $this->Cell($w, 9, $this->title, 1, 1, 'C', true);
            //Salto de línea
            $this->Ln(5);
        }
    }

//Pie de página
    function Footer() {
        //Posición: a 1,5 cm del final
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        //Número de página
        $this->Cell(0, 10, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }
    //Cargar los datos
    function LoadData($file) {
        //Leer las líneas del fichero
        $lines = file($file);
        $data = array();
        foreach ($lines as $line)
            $data[] = explode(';', utf8_decode (chop($line)));
        return $data;
    }

//Tabla simple
    function BasicTable($header, $data) {
        //Cabecera
        foreach ($header as $col)
            $this->Cell(40, 7, $col, 1);
        $this->Ln();
        //Datos
        foreach ($data as $row) {
            foreach ($row as $col)
                $this->Cell(40, 6, $col, 1);
            $this->Ln();
        }
    }

//Una tabla más completa
    function ImprovedTable($header, $data) {
        //Anchuras de las columnas
        $w = array(40, 35, 40, 45);
        //Cabeceras
        for ($i = 0; $i < count($header); $i++)
            $this->Cell($w[$i], 7, $header[$i], 1, 0, 'C');
        $this->Ln();
        //Datos
        foreach ($data as $row) {
            $this->Cell($w[0], 6, $row[0], 'LR');
            $this->Cell($w[1], 6, $row[1], 'LR');
            $this->Cell($w[2], 6, number_format($row[2]), 'LR', 0, 'R');
            $this->Cell($w[3], 6, number_format($row[3]), 'LR', 0, 'R');
            $this->Ln();
        }
        //Línea de cierre
        $this->Cell(array_sum($w), 0, '', 'T');
    }

//Tabla coloreada
    function FancyTable($data, $header, $w = array(40, 35, 40, 45)) {
        //Colores, ancho de línea y fuente en negrita
        $this->SetFillColor(255, 0, 0);
        $this->SetTextColor(255);
        $this->SetDrawColor(128, 0, 0);
        $this->SetLineWidth(.3);
        $this->SetFont('', 'B');
        //Cabecera
        //$w = array(40, 35, 40, 45);
        for ($i = 0; $i < count($header); $i++)
            $this->Cell($w[$i], 7, $header[$i], 1, 0, 'C', 1);
        $this->Ln();
        //Restauración de colores y fuentes
        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetFont('');
        //Datos
        $fill = false;
        foreach ($data as $row) {
            $this->Cell($w[0], 6, $row[0], 'LR', 0, 'L', $fill);
            $this->Cell($w[1], 6, $row[1], 'LR', 0, 'L', $fill);
            $this->Cell($w[2], 6, number_format($row[2]), 'LR', 0, 'R', $fill);
            $this->Cell($w[3], 6, number_format($row[3]), 'LR', 0, 'R', $fill);
            $this->Ln();
            $fill = !$fill;
        }
        $this->Cell(array_sum($w), 0, '', 'T');
    }
}

//Creación del objeto de la clase heredada
$pdf = new PDF();
$pdf->setLogo('Logo.jpg');
$pdf->setDomicilio("ENRIQUE FELIX CASTRO NO. 2737\n COL. HUMAYA\nCULIACAN, SINALOA 7508800");
$pdf->setEmpresa("Hard & Soft Computacional");
$pdf->setTitulo("Pedidos");
$pdf->AliasNbPages();
//$pdf->AddPage();
//$pdf->SetFont('Times', '', 12);
//for ($i = 1; $i <= 40; $i++)
//    $pdf->Cell(0, 10, utf8_decode('Imprimiendo línea número ') . $i, 0, 1);
//Títulos de las columnas
$header = array(utf8_decode('País'), 'Capital', 'Superficie (km2)', 'Pobl. (en miles)');

//Carga de datos
$data = $pdf->LoadData('paises.txt');
$pdf->SetFont('Arial', '', 14);
$pdf->AddPage();
$pdf->BasicTable($header, $data);
$pdf->AddPage();
$pdf->ImprovedTable($header, $data);
$pdf->AddPage();
$pdf->FancyTable($data, $header);
CleanFiles('.');
//Determinar un nombre temporal de fichero en el directorio actual
$file ='pdf';// basename(tempnam('.', 'tmp'));
rename($file, $file . '.pdf');
$file .= '.pdf';
//Guardar el PDF en un fichero
$pdf->Output($file, 'F');
//Redirección
header('Location: ' . $file);
?>