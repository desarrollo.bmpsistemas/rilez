<?php
function CleanFiles($dir,$prefix='tmp', $ext = '.pdf') {
    //Borrar los ficheros temporales
    $t = time();
    $h = opendir($dir);
    while ($file = readdir($h)) {
        if (substr($file, 0, strlen($prefix)) == $prefix && substr($file, -1 * strlen($ext)) == $ext) {
            $path = $dir . '/' . $file;
            if ($t - filemtime($path) > 3600){
                @chmod($path,0777);
                @unlink($path);
            }
        }
    }
    closedir($h);
}
?>