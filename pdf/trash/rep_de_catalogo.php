<?php
require('fpdf.php');

class PDF extends FPDF
{
    function PDF($orientation='P', $unit='mm', $format='Letter')
    {
        parent::__construct($orientation,$unit,$format);
    }
//Cabecera de página
function Header()
{

    global $empresa,$nombrecomercial,$calle,$numeroexterior,
           $numerointerior,$colonia,$municipio,$poblacion,$estado,
           $pais,$codigo_postal,$domicilio,$lugar,$campo;
    global $ancho,$alto,$borde,$salto,$alinear,$llenar,$salto;
    //Logo
    
    $this->Image('LOGO.JPG',10,1,33);
    $this->SetY(1);
    //Arial bold 15
    $this->SetFont('Arial','I',9);
    //$ancho=$this->GetStringWidth($empresa)+6;
  
    $ancho=0;
    $alto=5;
    $borde=1;
    $salto=1;
    $alinear='C';
    $llenar=false;
    //$ancho=50;
    $this->Cell($ancho,$alto,$empresa,$borde,$salto,$alinear,$llenar);
    $ancho=0;
//    $this->Ln(5); // salto de linea
    $ancho=0;
    $this->Cell(0,$alto,$nombrecomercial,$borde,$salto,$alinear,$llenar);
//    $this->Ln(5);
    $domicilio=$calle." ".$numeroexterior. " ".$numerointerior." Col.".$colonia ."C.P.".$codigo_postal;
    $this->Cell(0,$alto,$domicilio,$borde,$salto,$alinear,$llenar);
//    $this->Ln(5);
    $lugar=$municipio.", ".$poblacion.", ".$estado.", ".$pais;
    $this->Cell(0,$alto,$lugar,$borde,$salto,$alinear,$llenar);
//    $this->Ln(5);
    //echo round($ancho);
   //$pdf->Cell(10);
    $x=$this->GetX();
    $y=$this->GetY();
//    $this->SetXY($x, $y);

   $this->Cell(40,10,'Title',1,0,'C');
   $this->Cell(40,10,'Title',1,0,'C');
   $this->Cell(40,10,'Title',1,0,'C');
   $this->Cell(40,10,'Title',1,0,'C');
   $this->Cell(0,10,'Title',1,1,'C');
    //Movernos a la derecha
    $this->Ln(1);
    $this->tMargin = $this->GetY();
    //Título
    
    
    
    //Salto de línea
    
}

//Pie de página
function Footer()
{
    //Posición: a 1,5 cm del final
    $this->SetY(-15);
    //Arial italic 8
    $this->SetFont('Arial','I',8);
    //Número de página
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
}

//Creación del objeto de la clase heredada
$pdf=new PDF();
$pdf->AliasNbPages();
$empresa="SERGIO AVILES LARA";
$nombrecomercial="HARD AND SOFT COMPUTACIONAL";
$calle="BUELNA ";
$numeroexterior="551 OTE.";
$numerointerior="B";
$colonia="CENTRO";
$codigo_postal="80160";
$municipio="CULIACAN";
$estado="SINALOA";
$poblacion="CULIACAN";
$pais="MEXICO";
$pdf->AddPage();
$pdf->SetFont('Times','',12);
//utf8_decode($data);
$cadena="Imprimiendo línea número ";
$salida=utf8_decode($cadena);
$fotos_path = "../fotos/";

$x=$pdf->GetX();
//$y=$pdf->GetY();
//substr("hola mundo", 1, 4);
for($i=1;$i<=100;$i++){
//    while ($row = mysql_fetch_object($query)) {
//
    $row['id2']="APM";
    $y=$pdf->GetY();
//    $pdf->SetXY($x,$y);
//    $pdf->Image('LOGO.JPG',$x+160,$y,($pdf->w-$pdf->rMargin)-(160),30);
    $pdf->Cell(40,30,$y.' -'.$pdf->GetY(),1,0,'');
//    if($pdf->GetY() > $yMAX)
//            $yMAX = $pdf->GetY ();
//    $pdf->SetXY($x+40,$y);
    $pdf->Cell(40,30,'refe -'.$pdf->GetY(),1,0,'');
//    if($pdf->GetY() > $yMAX)
//            $yMAX = $pdf->GetY ();
//    $pdf->SetXY($x+80,$y);
    $pdf->Cell(40,30,substr('descripcion descripcion -',1,15).$pdf->GetY(),1,0,'');
//    if($pdf->GetY() > $yMAX)
//            $yMAX = $pdf->GetY ();
//    $pdf->SetXY($x+120,$y);
//    $pdf->SetXY($x+160,$y);
    $yMAX=$pdf->GetY();
    $pdf->Cell(40,30,$fotos_path."$row[id2].JPG",1,1,'');
    if(is_file($fotos_path."$row[id2].JPG")){
            $pdf->Image($fotos_path."$row[id2].JPG",$x+160,$yMAX,36,30);//($pdf->w-$pdf->rMargin)-($x+120)
    }
//    if($pdf->GetY() > $yMAX)
//            $yMAX = $pdf->GetY ();
//    $pdf->SetX($x+160,$y);
    
//    $pdf->Cell(0,30,'',1,1,'');
//    $pdf->Ln();
//    $pdf->Cell(0,30,'234523-'.$pdf->GetY(),1,1,'');
//    if($pdf->GetY() > $yMAX)
//            $yMAX = $pdf->GetY ();
//    if($yMAX > $pdf->h)
//            $yMAX = $pdf->GetY();
}
//    $pdf->Cell(0,10,$salida.$i,1,1);
$pdf->Ln();
$pdf->Output();
?>