<?php
require('../cfd/config.php');
require('PDF_mysqlReport.php');
require 'pdf_helper.php';
//if(@$_REQUEST['sucursal_id']){
$pdf = new PDF_mysqlReport('P');
//$sucursal_id=0;
//$pedido_id=0;
//if(!(isset ($_REQUEST['entrada_id'])))
//    die("Faltan id de entrada");
//$sucursal_id = intval($_REQUEST['sucursal_id']);
//if(isset ($_REQUEST['pedido_id']))
$entrada_id=intval($_REQUEST['entrada_id']);

//$pdf->connect('localhost','root','','dbrilez');
$pdf->connect($server,$username,$password,$database_name);

$query="SELECT
    ".(empty($entrada_id)?"":"tipo_mov.descripcion AS tipo,")."
    sucursales.nomcomercial AS nombre,
    sucursales.rfc,
    calles.descripcion AS calle,
    numero_exterior AS noExterior,
    numero_interior AS noInterior,
    colonias.descripcion AS colonia,
    referencia,
    municipios.descripcion AS municipio,
    estados.descripcion AS estado,
    paises.descripcion AS pais,
    sucursales.codigo_postal AS codigoPostal,
    sucursales.telefono
FROM ".(empty($entrada_id)?"":"entradas
INNER JOIN tipo_mov ON entradas.tipo_mov_id = tipo_mov.id
INNER JOIN ")."almacenes".(empty($entrada_id)?"":" ON entradas.almacen_id = almacenes.id")."
INNER JOIN sucursales ON sucursales.id = almacenes.sucursal_id
INNER JOIN municipios ON sucursales.municipio_id = municipios.id
INNER JOIN estados ON estados.id = municipios.estado_id
INNER JOIN paises ON paises.id = estados.pais_id
INNER JOIN calles ON sucursales.calle_id = calles.id
INNER JOIN colonias ON colonias.id = sucursales.colonia_id
where ".(empty($entrada_id)?"sucursales.matriz_id=0":"entradas.id=$entrada_id");
$digitos = 2;
$sql_entradas = "SELECT  e.id,
       tm.descripcion AS tipo,
       e.fecha,
--       de.articulo_id,
--       a.id2,
--       TRUNCATE(SUM(de.cantidad),$digitos) AS cantidad,
--       u.descripcion AS unidad,
--       a.descripcion,
--       TRUNCATE(de.costo,$digitos) AS costo,
       TRUNCATE(SUM((de.cantidad * de.costo * de.descuento)),$digitos) AS descto,
       TRUNCATE(SUM(de.cantidad * de.costo * (1 - de.descuento)),$digitos) AS importe,
       TRUNCATE(SUM(de.cantidad * de.costo * (1 - de.descuento) * de.iva),$digitos) AS iva,
       TRUNCATE(SUM(de.cantidad * de.costo * (1 - de.descuento) * (1 + de.iva)),$digitos) AS total
  FROM    (   (   (   articulos a
                   INNER JOIN
                      unidades u
                   ON (a.unidad_id = u.id))
               INNER JOIN
                  detalles_entradas de
               ON (de.articulo_id = a.id))
           INNER JOIN
              entradas e
           ON (e.id = de.entrada_id))
       INNER JOIN
          tipo_mov tm
       ON (e.tipo_mov_id = tm.id)
GROUP BY e.id
-- where e.id=$entrada_id";
$sql_det_entradas = "SELECT  e.id,
       tm.descripcion AS tipo,
--       de.articulo_id,
--       a.id2,
       TRUNCATE(de.cantidad,$digitos) AS cantidad,
       u.descripcion AS unidad,
       a.descripcion,
       TRUNCATE(de.costo,$digitos) AS costo,
       TRUNCATE((de.cantidad * de.costo * de.descuento),$digitos) AS descto,
       TRUNCATE((de.cantidad * de.costo * (1 - de.descuento)),$digitos) AS importe,
       TRUNCATE((de.cantidad * de.costo * (1 - de.descuento) * de.iva),$digitos) AS iva,
       TRUNCATE((de.cantidad * de.costo * (1 - de.descuento) * (1 + de.iva)),$digitos) AS total
  FROM    (   (   (   articulos a
                   INNER JOIN
                      unidades u
                   ON (a.unidad_id = u.id))
               INNER JOIN
                  detalles_entradas de
               ON (de.articulo_id = a.id))
           INNER JOIN
              entradas e
           ON (e.id = de.entrada_id))
       INNER JOIN
          tipo_mov tm
       ON (e.tipo_mov_id = tm.id)
where e.id=$entrada_id";
$_sql_entradas = "SELECT e.id,
       tm.descripcion AS tipo,
--       e.almacen_id,
       de.articulo_id,
       a.id2,
       de.cantidad,
       u.descripcion AS unidad,
       a.descripcion,
       de.costo,
       (de.cantidad * de.costo * de.descuento) AS descto,
       (de.cantidad * de.costo * (1 - de.descuento)) AS importe,
       (de.cantidad * de.costo * (1 - de.descuento) * de.iva) AS iva,
       (de.cantidad * de.costo * (1 - de.descuento) * (1 + de.iva)) AS total
  FROM    (   (   (   articulos a
                   INNER JOIN
                      unidades u
                   ON (a.unidad_id = u.id))
               INNER JOIN
                  detalles_entradas de
               ON (de.articulo_id = a.id))
           INNER JOIN
              entradas e
           ON (e.id = de.entrada_id))
       INNER JOIN
          tipo_mov tm
       ON (e.tipo_mov_id = tm.id)";
//die ($query);
$pdf->query($query);
$result = $pdf->getParameter("results");
if(($entrada = mysql_fetch_assoc($result))){
    $pdf->SetAuthor($entrada["nombre"]);
    $pdf->SetAddress("$entrada[calle] $entrada[noExterior]" . (empty($entrada['noInterior'])?"":" - $entrada[noInterior]") .
        " $entrada[colonia]\n $entrada[municipio], $entrada[estado], $entrada[pais] " .
        (empty($entrada['codigoPostal'])?"":"C.P.: $entrada[codigoPostal]") .
        "\nRFC: $entrada[rfc]".(empty($entrada['telefono'])?"":" tel: $entrada[telefono]"));
    $pdf->SetTitle("MERCANCIAS");
    $pdf->SetSubject("Informe de entrada".(empty($entrada_id)?"s":" por $entrada[tipo]: $entrada_id"));
    //Asignar Font
//    $pdf->SetFont('Arial','',8);
    $pdf->SetLogo('Logo.JPG');
    $pdf->SetParameter('logoh', .9);
    $pdf->SetParameter('authorFontSize', 14);
    $pdf->SetParameter('authorFontStyle', 'BIU');
    $pdf->SetParameter('authorAlign', 'R');
    $pdf->SetParameter('addressFontSize', 7);
    $pdf->SetParameter('addressH', 3);
    $pdf->SetParameter('titleFontSize', 13);
    $pdf->SetParameter('titleH', 7);
    $pdf->SetParameter('subjectFontSize', 12);
    $pdf->SetParameter('subjectH', 7);
}
$pdf->SetParameter('cellHeaderBorder', 'T');//Borde de Header
$pdf->SetParameter('cellHeaderAlign', 'L');
$pdf->SetParameter('cellFontSize', 10);
$pdf->SetParameter('sum', array('cantidad'=>0.0,'importe'=>0.0, 'descto'=>0.0,'subtotal'=>0.0,'iva'=>0.0,'total'=>0.0));
$pdf->SetParameter('sumcantidadVisible',1);
$pdf->SetParameter('sumimporteVisible',1);
$pdf->SetParameter('sumdesctoVisible',1);
$pdf->SetParameter('sumsubtotalVisible',1);
$pdf->SetParameter('sumivaVisible',1);
$pdf->SetParameter('sumtotalVisible',1);
//$pdf->SetParameter('sumimporteBorder',1);
//$pdf->SetParameter('DESCUENTOBorder', 1);
//$pdf->SetParameter('IDBorder', 1);
$pdf->colf=
	array(
		'cantidad' => "%01.2f",
                'descto' => "%01.2f",
                'importe' => "%01.2f",
                'iva' => "%01.2f",
                'total' => "%01.2f"
	);
//Asignar la consulta a mostrar como reporte
$pdf->mysql_report_image((empty($entrada_id)?$sql_entradas:$sql_det_entradas),FALSE, 4,0,array('img'),'Logo.JPG');

//$sum = $pdf->GetParameter('sum');
//$pdf->Cell(0, 10, 'Total:' . $sum['importe'], 0, 0, 'C');
$print_headers = false;
if ($print_headers) {
    $pdf->AutoPrint(true);
    $buf=$pdf->Output("", "S");
    unset($pdf);
    header('Cache-Control: no-cache, no-store, max-age=0, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Pragma: no-cache');

    header('Content-type: application/pdf');
    header('Content-Length:'.  strlen($buf));
    header('Content-Disposition: inline; filename="'.'pdf.pdf'.'"');
    echo $buf;
}else{
//    $pdf->AutoPrint(true);
    $prefix = 'entrada';
    CleanFiles('.',$prefix);
    //Determinar un nombre temporal de fichero en el directorio actual
    $file = $prefix. time() .'.pdf';
    //Guardar el PDF en un fichero
    $pdf->Output($file, 'F');
    chmod($file,0777);
    //$pdf->Output($file);
    //Redirección
    header('Location: ' . $file);
}
?>