<?php
$sql_articulos = "SELECT articulos_almacenes.almacen_id,
       articulos_almacenes.posicion,
       articulos.id,
       articulos.id2,
       unidades.descripcion AS unidad,
       articulos.descripcion,
       (articulos_almacenes.entradas +
       articulos_almacenes.salidas) AS existencia
  FROM    (   articulos articulos
           INNER JOIN
              unidades unidades
           ON (articulos.unidad_id = unidades.id))
       INNER JOIN
          articulos_almacenes articulos_almacenes
       ON (articulos_almacenes.articulo_id = articulos.id)
 -- WHERE (articulos_almacenes.almacen_id = 1)
       ORDER BY articulos_almacenes.almacen_id, articulos_almacenes.posicion ASC";
$sql_articulos = "SELECT
--       articulos.sublinea_id,
--       articulos.orden,
--       articulos.id,
       articulos.id2,
       articulos.descripcion
  FROM articulos articulos
ORDER BY articulos.sublinea_id ASC, articulos.orden ASC";
$sql_articulos_image = "SELECT
    CONCAT('../fotos/',articulos.id2,'.JPG') AS img,
--    CONCAT('Logo','.jpg') AS img,
--       articulos.sublinea_id,
--       articulos.orden,
--       articulos.id,
       articulos.id2,
       articulos.descripcion
  FROM articulos articulos
ORDER BY articulos.sublinea_id ASC, articulos.orden ASC";
require('../cfd/config.php');
require('PDF_mysqlReport.php');
require 'pdf_helper.php';
//if(@$_REQUEST['sucursal_id']){
$pdf = new PDF_mysqlReport('P');
//$sucursal_id=0;
//$pedido_id=0;
$sucursal_id = intval($_REQUEST['sucursal_id']);
//if(isset ($_REQUEST['pedido_id']))

//$pdf->connect('localhost','root','','dbrilez');
$pdf->connect($server,$username,$password,$database_name);

$query="SELECT
    sucursales.nomcomercial AS nombre,
    sucursales.rfc,
    calles.descripcion AS calle,
    numero_exterior AS noExterior,
    numero_interior AS noInterior,
    colonias.descripcion AS colonia,
    referencia,
    municipios.descripcion AS municipio,
    estados.descripcion AS estado,
    paises.descripcion AS pais,
    sucursales.codigo_postal AS codigoPostal,
    sucursales.telefono
FROM sucursales
INNER JOIN municipios ON sucursales.municipio_id = municipios.id
INNER JOIN estados ON estados.id = municipios.estado_id
INNER JOIN paises ON paises.id = estados.pais_id
INNER JOIN calles ON sucursales.calle_id = calles.id
INNER JOIN colonias ON colonias.id = sucursales.colonia_id
where sucursales.matriz_id=$sucursal_id";
//die ($query);
$pdf->query($query);
$result = $pdf->getParameter("results");
if(($salida = mysql_fetch_assoc($result))){
    $pdf->SetAuthor($salida["nombre"]);
    $pdf->SetAddress("$salida[calle] $salida[noExterior]" . (empty($salida['noInterior'])?"":" - $salida[noInterior]") .
        " $salida[colonia]" .
        (empty($salida['codigoPostal'])?"":" C.P.: $salida[codigoPostal]") .
        "\n $salida[municipio], $salida[estado], $salida[pais] \nRFC: $salida[rfc]".(empty($salida['telefono'])?"":" tel: $salida[telefono]"));
    $pdf->SetTitle("ARTICULOS");
    $pdf->SetSubject("Catálogo de Articulos");
    //Asignar Font
//    $pdf->SetFont('Arial','',8);
    $pdf->SetLogo('logo_rilez.gif');
    $pdf->SetParameter('logox', 20);
    $pdf->SetParameter('logoy', 5);
    $pdf->SetParameter('logow', 230);
    $pdf->SetParameter('logoh', 0);
    $pdf->SetParameter('authorAlign', 'R');
    $pdf->SetParameter('authorFontSize', 14);
    $pdf->SetParameter('authorH', 14);
    $pdf->SetParameter('authorFontStyle', 'BIU');
    $pdf->SetParameter('addressAlign', 'R');
    $pdf->SetParameter('addressFontSize', 7);
    $pdf->SetParameter('addressH', 0);
    $pdf->SetParameter('titleFontSize', 13);
    $pdf->SetParameter('titleH', 0);
    $pdf->SetParameter('subjectFontSize', 12);
    $pdf->SetParameter('subjectH', 0);
}
$pdf->SetParameter('cellHeaderBorder', "TB");//Borde de Header
$pdf->SetParameter('cellHeaderAlign', 'L');
$pdf->SetParameter('cellH', 20);
$pdf->SetParameter('cellBorder', "");//Borde de Header
$pdf->SetParameter('cellFontSize', 10);
$pdf->tablewidths=
	array(
		67.816, /* id2 */
		239.384 /* descripcion */
		
	);
//Asignar la consulta a mostrar como reporte
//$pdf->mysql_report_image($sql_articulos);
$pdf->mysql_report($sql_articulos,TRUE,8, 0,array('img'),'Logo.JPG');

//$sum = $pdf->GetParameter('sum');
//$pdf->Cell(0, 10, 'Total:' . $sum['importe'], 0, 0, 'C');
$print_headers = false;
if ($print_headers) {
    $pdf->AutoPrint(true);
    $buf=$pdf->Output("", "S");
    unset($pdf);
    header('Cache-Control: no-cache, no-store, max-age=0, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Pragma: no-cache');

    header('Content-type: application/pdf');
    header('Content-Length:'.  strlen($buf));
    header('Content-Disposition: inline; filename="'.'pdf.pdf'.'"');
    echo $buf;
}else{
//    $pdf->AutoPrint(true);
    $prefix = 'articulo';
    CleanFiles('.',$prefix);
    //Determinar un nombre temporal de fichero en el directorio actual
    $file = $prefix. time() .'.pdf';
    //Guardar el PDF en un fichero
    $pdf->Output($file, 'F');
    chmod($file,0777);
    //$pdf->Output($file);
    //Redirección
    header('Location: ' . $file);
}
?>