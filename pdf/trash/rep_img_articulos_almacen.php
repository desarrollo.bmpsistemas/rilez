<?php
$title = "CATÁLOGO";
$subject = "ARTICULOS";
$where = "";
$where_articulos = "";
$sucursal_id = intval($_REQUEST['sucursal_id']);
$almacen_id = intval($_REQUEST['almacen_id']);
if(!empty ($sucursal_id)){
    $where = (empty ($where)?" WHERE":" AND"). " s.id = $sucursal_id";
    $where_articulos = (empty ($where_articulos)?" WHERE":" AND"). " s.id = $sucursal_id";
}elseif(!empty ($almacen_id)){
    $title .= " ALMACÉN: $almacen_id";
    $where = (empty ($where)?" WHERE":" AND"). " al.id = $almacen_id";
    $where_articulos = (empty ($where_articulos)?" WHERE":" AND"). " al.id = $almacen_id";
}  else {
    $where = " WHERE s.matriz_id = 0";
}
$linea_id = intval($_REQUEST['linea_id']);
if(!empty ($linea_id)){
    $subject .= "\nLINEA: $linea_id";
    $where_articulos = (empty ($where_articulos)?" WHERE":" AND"). " l.id = $linea_id";
}
$sublinea_id = intval($_REQUEST['sublinea_id']);
if(!empty ($sublinea_id)){
    $subject .= "\nSUBLINEA: $sublinea_id";
    $where_articulos = (empty ($where_articulos)?" WHERE ":" AND"). " sl.id = $sublinea_id";
}
$img_array = array();//'img'
$sql_articulos_almacen = "SELECT ar.id2 AS modelo,
       CONCAT('LINEA: ', l.descripcion, '\nSUBLINEA: ',
       sl.descripcion, '\nDESCRIPCION: ',
       ar.descripcion) AS descripcion,
       ar_al.precio,
       ar_al.iva,
       m.descripcion AS divisa
  FROM    (   (   (   (   (   (   sublineas sl
                               INNER JOIN
                                  lineas l
                               ON (sl.linea_id = l.id))
                           INNER JOIN
                              articulos ar
                           ON (ar.sublinea_id = sl.id))
                       INNER JOIN
                          articulos_almacenes ar_al
                       ON (ar_al.articulo_id = ar.id))
                   INNER JOIN
                      almacenes al
                   ON (ar_al.almacen_id = al.id))
               INNER JOIN
                  sucursales s
               ON (al.sucursal_id = s.id))
           INNER JOIN
              monedas m
           ON (ar_al.moneda_id = m.id))
       INNER JOIN
          unidades u
       ON (ar.unidad_id = u.id)$where_articulos
       ORDER BY s.id,al.id,l.id,sl.id,ar.orden";
$sql_articulos_almacen_img = "SELECT CONCAT('../fotos/',articulos.id2,'.JPG') AS img,
       ar.id2 AS modelo,
       CONCAT('LINEA: ', l.descripcion, '\nSUBLINEA: ',
       sl.descripcion, '\nDESCRIPCION: ',
       ar.descripcion) AS descripcion,
       ar_al.precio,
       ar_al.iva,
       m.descripcion AS divisa
  FROM    (   (   (   (   (   (   sublineas sl
                               INNER JOIN
                                  lineas l
                               ON (sl.linea_id = l.id))
                           INNER JOIN
                              articulos ar
                           ON (ar.sublinea_id = sl.id))
                       INNER JOIN
                          articulos_almacenes ar_al
                       ON (ar_al.articulo_id = ar.id))
                   INNER JOIN
                      almacenes al
                   ON (ar_al.almacen_id = al.id))
               INNER JOIN
                  sucursales s
               ON (al.sucursal_id = s.id))
           INNER JOIN
              monedas m
           ON (ar_al.moneda_id = m.id))
       INNER JOIN
          unidades u
       ON (ar.unidad_id = u.id)
       ORDER BY s.id,al.id,l.id,sl.id,ar.orden";
$sql_articulos = "SELECT
--       articulos.sublinea_id,
--       articulos.orden,
--       articulos.id,
       articulos.id2,
       articulos.descripcion
  FROM articulos articulos
ORDER BY articulos.sublinea_id ASC, articulos.orden ASC";
$sql_articulos_image = "SELECT
    CONCAT('../fotos/',articulos.id2,'.JPG') AS img,
--    CONCAT('Logo','.jpg') AS img,
--       articulos.sublinea_id,
--       articulos.orden,
--       articulos.id,
       articulos.id2,
       articulos.descripcion
  FROM articulos articulos
ORDER BY articulos.sublinea_id ASC, articulos.orden ASC";
require('../cfd/config.php');
require('PDF_mysqlReport.php');
require 'pdf_helper.php';
//if(@$_REQUEST['sucursal_id']){
$pdf = new PDF_mysqlReport('P');
//$sucursal_id=0;
//$pedido_id=0;
$sucursal_id = intval($_REQUEST['sucursal_id']);
//if(isset ($_REQUEST['pedido_id']))

//$pdf->connect('localhost','root','','dbrilez');
$pdf->connect($server,$username,$password,$database_name);

$query = "SELECT s.codigo_postal AS codigoPostal,
       paises.descripcion AS pais,
       estados.descripcion AS estado,
       municipios.descripcion AS municipio,
       s.referencia,
       IF(s.poblacion_id = 1 OR poblaciones.descripcion is null,'',poblaciones.descripcion) AS poblacion,
       colonias.descripcion AS colonia,
       calles.descripcion AS calle,
       s.numero_interior AS noInterior,
       s.numero_exterior AS noExterior,
       s.rfc,
       s.nomcomercial AS nombre,
       s.telefono
  FROM    (   (   (   (   (   sucursales s
                           LEFT JOIN
                              poblaciones poblaciones
                           ON (s.poblacion_id = poblaciones.id))
                       INNER JOIN
                          municipios municipios
                       ON (s.municipio_id = municipios.id))
                   INNER JOIN
                      estados estados
                   ON (municipios.estado_id = estados.id)
                      AND (estados.id = municipios.estado_id))
               INNER JOIN
                  paises paises
               ON (estados.pais_id = paises.id)
                  AND (paises.id = estados.pais_id))
           INNER JOIN
              calles calles
           ON (s.calle_id = calles.id))
       INNER JOIN
          colonias colonias
       ON (s.colonia_id = colonias.id) $where";
$query="SELECT 
       s.descripcion AS sucursal, ".(empty($almacen_id)?"":"al.nombre AS almacen,")."
       s.codigo_postal AS codigoPostal,
       paises.descripcion AS pais,
       estados.descripcion AS estado,
       municipios.descripcion AS municipio,
       s.referencia,
       IF(s.poblacion_id = 1 OR poblaciones.descripcion is null,'',poblaciones.descripcion) AS poblacion,
       colonias.descripcion AS colonia,
       calles.descripcion AS calle,
       s.numero_interior AS noInterior,
       s.numero_exterior AS noExterior,
       s.rfc,
       s.nomcomercial AS nombre,
       s.telefono
  FROM    (   (   (   (   (   ".(empty($almacen_id)?"":"(   almacenes al
                                  INNER JOIN ")."
                                      sucursales s
                                  ".(empty($almacen_id)?"":"ON ( al.sucursal_id = s.id))")."
                           LEFT JOIN
                              poblaciones poblaciones
                           ON (s.poblacion_id = poblaciones.id))
                       INNER JOIN
                          municipios municipios
                       ON (s.municipio_id = municipios.id))
                   INNER JOIN
                      estados estados
                   ON (municipios.estado_id = estados.id)
                      AND (estados.id = municipios.estado_id))
               INNER JOIN
                  paises paises
               ON (estados.pais_id = paises.id)
                  AND (paises.id = estados.pais_id))
           INNER JOIN
              calles calles
           ON (s.calle_id = calles.id))
       INNER JOIN
          colonias colonias
       ON (s.colonia_id = colonias.id)$where";
//die ($query);
//die(empty($img_array)?$sql_articulos_almacen:$sql_articulos_almacen_img);
$pdf->query($query);
$result = $pdf->getParameter("results");
if(($salida = mysql_fetch_assoc($result))){
    $pdf->SetAuthor($salida["nombre"]);
    $pdf->SetAddress("$salida[calle] $salida[noExterior]" . (empty($salida['noInterior'])?"":" - $salida[noInterior]") .
        " $salida[colonia]\n $salida[municipio], $salida[estado], $salida[pais] " .
        (empty($salida['codigoPostal'])?"":"C.P.: $salida[codigoPostal]") .
        "\nRFC: $salida[rfc]".(empty($salida['telefono'])?"":" tel: $salida[telefono]"));
    $pdf->SetTitle($title);
    $pdf->SetSubject("Catálogo de Articulos");
    //Asignar Font
//    $pdf->SetFont('Arial','',8);
    $pdf->SetLogo('Logo.JPG');
    $pdf->SetParameter('logox', 0);
    $pdf->SetParameter('logoy', 0);
    $pdf->SetParameter('logow', 0);
    $pdf->SetParameter('logoh', 0.9);
    $pdf->SetParameter('authorAlign', 'R');
    $pdf->SetParameter('authorFontSize', 14);
    $pdf->SetParameter('authorFontStyle', 'BIU');
    $pdf->SetParameter('addressAlign', 'R');
    $pdf->SetParameter('addressFontSize', 7);
    $pdf->SetParameter('addressH', 3);
    $pdf->SetParameter('titleFontSize', 13);
    $pdf->SetParameter('titleH', 7);
    $pdf->SetParameter('subjectFontSize', 12);
    $pdf->SetParameter('subjectH', 7);
}
$pdf->SetParameter('cellHeaderBorder', "T");//Borde de Header
$pdf->SetParameter('cellHeaderAlign', 'L');
$pdf->SetParameter('cellH', 20);
$pdf->SetParameter('cellFontSize', 10);

//Asignar la consulta a mostrar como reporte
//$pdf->mysql_report_image($sql_articulos_almacen);
$pdf->mysql_report_image(empty($img_array)?$sql_articulos_almacen:$sql_articulos_almacen_img,FALSE,4, 0,$img_array,'Logo.JPG');

//$sum = $pdf->GetParameter('sum');
//$pdf->Cell(0, 10, 'Total:' . $sum['importe'], 0, 0, 'C');
$print_headers = false;
if ($print_headers) {
    $pdf->AutoPrint(true);
    $buf=$pdf->Output("", "S");
    unset($pdf);
    header('Cache-Control: no-cache, no-store, max-age=0, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Pragma: no-cache');

    header('Content-type: application/pdf');
    header('Content-Length:'.  strlen($buf));
    header('Content-Disposition: inline; filename="'.'pdf.pdf'.'"');
    echo $buf;
}else{
//    $pdf->AutoPrint(true);
    $prefix = 'articulo';
    CleanFiles('.',$prefix);
    //Determinar un nombre temporal de fichero en el directorio actual
    $file = $prefix. time() .'.pdf';
    //Guardar el PDF en un fichero
    $pdf->Output($file, 'F');
    chmod($file,0777);
    //$pdf->Output($file);
    //Redirección
    header('Location: ' . $file);
}
?>