<?php
require('../cfd/config.php');
require('PDF_mysqlReport.php');
require 'pdf_helper.php';
//if(@$_REQUEST['sucursal_id']){
$pdf = new PDF_mysqlReport('P');
//$sucursal_id=0;
//$pedido_id=0;
if(!(isset ($_REQUEST['salida_id'])))
    die("Faltan id de salida");
//$sucursal_id = intval($_REQUEST['sucursal_id']);
//if(isset ($_REQUEST['pedido_id']))
$salida_id=intval($_REQUEST['salida_id']);

//$pdf->connect('localhost','root','','dbrilez');
$pdf->connect($server,$username,$password,$database_name);

$query="SELECT
    tipo_mov.descripcion AS tipo,
    sucursales.nomcomercial AS nombre,
    sucursales.rfc,
    calles.descripcion AS calle,
    numero_exterior AS noExterior,
    numero_interior AS noInterior,
    colonias.descripcion AS colonia,
    referencia,
    municipios.descripcion AS municipio,
    estados.descripcion AS estado,
    paises.descripcion AS pais,
    sucursales.codigo_postal AS codigoPostal,
    sucursales.telefono
FROM salidas
INNER JOIN tipo_mov ON salidas.tipo_mov_id = tipo_mov.id
INNER JOIN almacenes ON salidas.almacen_id = almacenes.id
INNER JOIN sucursales ON sucursales.id = almacenes.sucursal_id
INNER JOIN municipios ON sucursales.municipio_id = municipios.id
INNER JOIN estados ON estados.id = municipios.estado_id
INNER JOIN paises ON paises.id = estados.pais_id
INNER JOIN calles ON sucursales.calle_id = calles.id
INNER JOIN colonias ON colonias.id = sucursales.colonia_id
where salidas.id=$salida_id";
$digitos = 2;
$sql_salidas1 = "select id2,descripcion,TRUNCATE(cantidad,$digitos) AS cantidad ,TRUNCATE(costo,$digitos) AS costo,TRUNCATE(importe,$digitos) AS importe,TRUNCATE(descuento,$digitos) AS descuento,TRUNCATE((importe - descuento),$digitos) as subtotal,TRUNCATE(iva,$digitos) AS iva,TRUNCATE((importe - descuento) + iva,$digitos) as total
from vista_salida where salida_id=$salida_id";
$sql_salidas = "SELECT s.id,
       tm.descripcion AS tipo,
--       s.almacen_id,
--       ds.articulo_id,
--       a.id2,
       TRUNCATE(ds.cantidad,$digitos) AS cantidad,
       u.descripcion AS unidad,
       a.descripcion,
       TRUNCATE(ds.costo,$digitos) AS costo,
       TRUNCATE(ds.precio,$digitos) AS precio,
       TRUNCATE((ds.cantidad * ds.precio * ds.descuento),$digitos) AS descto,
       TRUNCATE((ds.cantidad * ds.precio * (1 - ds.descuento)),$digitos) AS importe,
       TRUNCATE((ds.cantidad * ds.precio * (1 - ds.descuento) * ds.iva),$digitos) AS iva,
       TRUNCATE((ds.cantidad * ds.precio * (1 - ds.descuento) * (1 + ds.iva)),$digitos) AS total
  FROM    (   (   (   articulos a
                   INNER JOIN
                      unidades u
                   ON (a.unidad_id = u.id))
               INNER JOIN
                  detalles_salidas ds
               ON (ds.articulo_id = a.id))
           INNER JOIN
              salidas s
           ON (s.id = ds.salida_id))
       INNER JOIN
          tipo_mov tm
       ON (s.tipo_mov_id = tm.id)
where s.id=$salida_id";
//die ($query);
$pdf->query($query);
$result = $pdf->getParameter("results");
if(($salida = mysql_fetch_assoc($result))){
    $pdf->SetAuthor($salida["nombre"]);
    $pdf->SetAddress("$salida[calle] $salida[noExterior]" . (empty($salida['noInterior'])?"":" - $salida[noInterior]") .
        " $salida[colonia]\n $salida[municipio], $salida[estado], $salida[pais] " .
        (empty($salida['codigoPostal'])?"":"C.P.: $salida[codigoPostal]") .
        "\nRFC: $salida[rfc]".(empty($salida['telefono'])?"":" tel: $salida[telefono]"));
    $pdf->SetTitle("MERCANCIAS");
    $pdf->SetSubject("Informe de salida por " . $salida['tipo'] . ": $salida_id");
    //Asignar Font
//    $pdf->SetFont('Arial','',8);
    $pdf->SetLogo('Logo.JPG');
    $pdf->SetParameter('logoh', .9);
    $pdf->SetParameter('authorFontSize', 14);
    $pdf->SetParameter('authorFontStyle', 'BIU');
    $pdf->SetParameter('authorAlign', 'R');
    $pdf->SetParameter('addressFontSize', 7);
    $pdf->SetParameter('addressH', 3);
    $pdf->SetParameter('titleFontSize', 13);
    $pdf->SetParameter('titleH', 7);
    $pdf->SetParameter('subjectFontSize', 12);
    $pdf->SetParameter('subjectH', 7);
}
$pdf->SetParameter('cellHeaderBorder', 'T');//Borde de Header
$pdf->SetParameter('cellHeaderAlign', 'L');
$pdf->SetParameter('cellFontSize', 10);
$pdf->SetParameter('sum', array('cantidad'=>0.0,'importe'=>0.0, 'descto'=>0.0,'subtotal'=>0.0,'iva'=>0.0,'total'=>0.0));
$pdf->SetParameter('sumcantidadVisible',1);
$pdf->SetParameter('sumimporteVisible',1);
$pdf->SetParameter('sumdesctoVisible',1);
$pdf->SetParameter('sumsubtotalVisible',1);
$pdf->SetParameter('sumivaVisible',1);
$pdf->SetParameter('sumtotalVisible',1);
//$pdf->SetParameter('sumimporteBorder',1);
//$pdf->SetParameter('DESCUENTOBorder', 1);
//$pdf->SetParameter('IDBorder', 1);
$pdf->colf=
	array(
		'cantidad' => "%01.2f",
                'descto' => "%01.2f",
                'importe' => "%01.2f",
                'iva' => "%01.2f",
                'total' => "%01.2f"
	);
//Asignar la consulta a mostrar como reporte
$pdf->mysql_report_image($sql_salidas,FALSE, 4,0,array('img'),'Logo.JPG');

//$sum = $pdf->GetParameter('sum');
//$pdf->Cell(0, 10, 'Total:' . $sum['importe'], 0, 0, 'C');
$print_headers = false;
if ($print_headers) {
    $pdf->AutoPrint(true);
    $buf=$pdf->Output("", "S");
    unset($pdf);
    header('Cache-Control: no-cache, no-store, max-age=0, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Pragma: no-cache');

    header('Content-type: application/pdf');
    header('Content-Length:'.  strlen($buf));
    header('Content-Disposition: inline; filename="'.'pdf.pdf'.'"');
    echo $buf;
}else{
//    $pdf->AutoPrint(true);
    $prefix = 'salida';
    CleanFiles('.',$prefix);
    //Determinar un nombre temporal de fichero en el directorio actual
    $file = $prefix. time() .'.pdf';
    //Guardar el PDF en un fichero
    $pdf->Output($file, 'F');
    chmod($file,0777);
    //$pdf->Output($file);
    //Redirección
    header('Location: ' . $file);
}
?>