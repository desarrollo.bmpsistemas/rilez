<?php
//require('fpdf.php');
require('PDF_mysqlReport.php');
require 'pdf_helper.php';
//if(@$_REQUEST['sucursal_id']){
$pdf = new PDF_mysqlReport('L');
//$sucursal_id=0;
//$pedido_id=0;
if(!(isset ($_REQUEST['capturista_id'])))
    die("Faltan capturista id");
if(!(isset ($_REQUEST['tipo_mov_id'])))
    die("Faltan tipo de movimiento id");
if(!(isset ($_REQUEST['almacen_id'])))
    die("Faltan almacen id");
//$sucursal_id = intval($_REQUEST['sucursal_id']);
//if(isset ($_REQUEST['pedido_id']))
$capturista_id=intval($_REQUEST['capturista_id']);
$tipo_mov_id=intval($_REQUEST['tipo_mov_id']);
$almacen_id=intval($_REQUEST['almacen_id']);

$pdf->connect('localhost','root','','dbrilez');

$query="SELECT
    sucursales.nomcomercial AS nombre,
    calles.descripcion AS calle,
    numero_exterior AS noExterior,
    numero_interior AS noInterior,
    colonias.descripcion AS colonia,
    referencia,
    municipios.descripcion AS municipio,
    estados.descripcion AS estado,
    paises.descripcion AS pais,
    sucursales.codigo_postal AS codigoPostal
FROM almacenes
INNER JOIN sucursales ON sucursales.id = almacenes.sucursal_id
INNER JOIN municipios ON sucursales.municipio_id = municipios.id
INNER JOIN estados ON estados.id = municipios.estado_id
INNER JOIN paises ON paises.id = estados.pais_id
INNER JOIN calles ON sucursales.calle_id = calles.id
INNER JOIN colonias ON colonias.id = sucursales.colonia_id
where almacenes.id=$almacen_id";
//die ($query);
$pdf->query($query);
$result = $pdf->getParameter("results");
if(($cliente = mysql_fetch_assoc($result))){
    $pdf->SetAuthor($cliente["nombre"]);
    $pdf->SetAddress("$cliente[calle] #$cliente[numero_exterior] - $cliente[numero_interior]\n$cliente[colonia] $cliente[municipio] $cliente[estado]\nrfc: $cliente[rfc] tel: $cliente[telefono]");
    $pdf->SetTitle("MERCANCIAS");
    $pdf->SetSubject("Informe de entradas");
    //Asignar Font
//    $pdf->SetFont('Arial','',8);
    $pdf->SetLogo('Logo.JPG');
    $pdf->SetParameter('logoh', .9);
    $pdf->SetParameter('authorFontSize', 14);
    $pdf->SetParameter('authorFontStyle', 'BIU');
    $pdf->SetParameter('addressFontSize', 7);
    $pdf->SetParameter('addressH', 3);
    $pdf->SetParameter('titleFontSize', 13);
    $pdf->SetParameter('titleH', 7);
    $pdf->SetParameter('subjectFontSize', 12);
    $pdf->SetParameter('subjectH', 7);
}
$pdf->SetParameter('cellHeaderBorder', 0);//Borde de Header
$pdf->SetParameter('cellHeaderAlign', 'L');
$pdf->SetParameter('cellFontSize', 10);
$pdf->SetParameter('sum', array('importe'=>0.0, 'descuento'=>0.0));
$pdf->SetParameter('sumimporteVisible',1);
$pdf->SetParameter('sumdescuentoVisible',1);
//$pdf->SetParameter('sumimporteBorder',1);
//$pdf->SetParameter('DESCUENTOBorder', 1);
//$pdf->SetParameter('IDBorder', 1);
//$pdf->tablewidths=
//	array(
//		9.1383111111111, /* id */
//		150.0 /* descripcion */
//
//	);

//Asignar la consulta a mostrar como reporte
$pdf->mysql_report("SELECT
unidades.descripcion AS unidad,
articulos.id,
articulos.id2,
articulos.descripcion,
detalles_tmp_entradas.cantidad,
detalles_tmp_entradas.costo,
detalles_tmp_entradas.cantidad*detalles_tmp_entradas.costo AS importe,
detalles_tmp_entradas.iva,
detalles_tmp_entradas.cantidad* detalles_tmp_entradas.descuento AS descuento
FROM detalles_tmp_entradas
INNER JOIN articulos ON detalles_tmp_entradas.articulo_id = articulos.id
INNER JOIN unidades ON articulos.unidad_id = unidades.id
where detalles_tmp_entradas.capturista_id=$capturista_id and detalles_tmp_entradas.tipo_mov_id=$tipo_mov_id and almacen_id=$almacen_id");

//$sum = $pdf->GetParameter('sum');
//$pdf->Cell(0, 10, 'Total:' . $sum['importe'], 0, 0, 'C');
//$pdf->AutoPrint(true);
$print_headers = false;
if ($print_headers) {
    $buf=$pdf->Output("", "S");
    unset($pdf);
    header('Cache-Control: no-cache, no-store, max-age=0, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Pragma: no-cache');

    header('Content-type: application/pdf');
    header('Content-Length:'.  strlen($buf));
    header('Content-Disposition: inline; filename="'.'pdf.pdf'.'"');
    echo $buf;
}else{
    //$pdf->Output();
CleanFiles('.');
//Determinar un nombre temporal de fichero en el directorio actual
$file = basename(tempnam('.', 'tmp'));//'reporte';
rename($file, $file . '.pdf');
$file .= '.pdf';
//Guardar el PDF en un fichero
$pdf->Output($file, 'F');
//$pdf->Output($file);
//Redirección
header('Location: ' . $file);
}
?>