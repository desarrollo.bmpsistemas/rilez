<?php
//numtoletras
GLOBAL $variable;
GLOBAL $moneda_id;
require_once('../pdf/fpdf.php');
require_once('../pdf/letras.php');
require('conecta_db.php');
session_start();
if (!isset($_SESSION["nombre"]))
{
  header("location: ../vistas/login.html");
}
class PDF extends FPDF
{
    function Lineas($w, $h, $txt, $border=0, $align='J', $fill=false,$NumLin)
    {
    	//Output text with automatic or explicit line breaks
    	$cw=&$this->CurrentFont['cw'];
    	if($w==0)
    		$w=$this->w-$this->rMargin-$this->x;
    	$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
    	$s=str_replace("\r",'',$txt);
    	$nb=strlen($s);
    	if($nb>0 && $s[$nb-1]=="\n")
    		$nb--;
    	$b=0;
    	if($border)
    	{
    		if($border==1)
    		{
    			$border='LTRB';
    			$b='LRT';
    			$b2='LR';
    		}
    		else
    		{
    			$b2='';
    			if(strpos($border,'L')!==false)
    				$b2.='L';
    			if(strpos($border,'R')!==false)
    				$b2.='R';
    			$b=(strpos($border,'T')!==false) ? $b2.'T' : $b2;
    		}
    	}
    	$sep=-1;
    	$i=0;
    	$j=0;
    	$l=0;
    	$ns=0;
    	$nl=1;
            
    	while($i<$nb)
    	{
    		//Get next character
    		$c=$s[$i];
    		if($c=="\n")
    		{
    			//Explicit line break
    			if($this->ws>0)
    			{
    				$this->ws=0;
    				$this->_out('0 Tw');
    			}
                            
    			$this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
                            if ($nl==$NumLin) return($nl);
    			$i++;
    			$sep=-1;
    			$j=$i;
    			$l=0;
    			$ns=0;
                            
    			$nl++;
                            if ($nl==$NumLin) return($nl);
    			if($border && $nl==2)
    				$b=$b2;
    			continue;
    		}
    		if($c==' ')
    		{
    			$sep=$i;
    			$ls=$l;
    			$ns++;
    		}
    		$l+=$cw[$c];
    		if($l>$wmax)
    		{
    			//Automatic line break
    			if($sep==-1)
    			{
    				if($i==$j)
    					$i++;
    				if($this->ws>0)
    				{
    					$this->ws=0;
    					$this->_out('0 Tw');
    				}
                                    
    				$this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
                                    if ($nl==$NumLin) return($nl);
    			}
    			else
    			{
    				if($align=='J')
    				{
    					$this->ws=($ns>1) ? ($wmax-$ls)/1000*$this->FontSize/($ns-1) : 0;
    					$this->_out(sprintf('%.3F Tw',$this->ws*$this->k));
    				}
                                    
    				$this->Cell($w,$h,substr($s,$j,$sep-$j),$b,2,$align,$fill);
                                    if ($nl==$NumLin) return($nl);
    				$i=$sep+1;
                                    
    			}
    			$sep=-1;
    			$j=$i;
    			$l=0;
    			$ns=0;
                            $nl++;
                            if($border && $nl==2)
    				$b=$b2;
    		}
    		else
    			$i++;
    	}
    	//Last chunk
    	if($this->ws>0)
    	{
    		$this->ws=0;
    		$this->_out('0 Tw');
    	}
    	if($border && strpos($border,'B')!==false)
    		$b.='B';
            if ($nl==$NumLin) return($nl);
    	$this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
            if ($nl==$NumLin) return($nl);
    	$this->x=$this->lMargin;
    	return $nl; //THIS RETURNS THE NUMBER OF LINES!
    }
    ////--------------    
        
    function BasicTable($header, $data)
    {

    }   
    function RoundedRect($x, $y, $w, $h, $r, $style = '')
    {
            $k = $this->k;
            $hp = $this->h;
            if($style=='F')
                $op='f';
            elseif($style=='FD' || $style=='DF')
                $op='B';
            else
                $op='S';
            $MyArc = 4/3 * (sqrt(2) - 1);
            $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));
            $xc = $x+$w-$r ;
            $yc = $y+$r;
            $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));

            $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
            $xc = $x+$w-$r ;
            $yc = $y+$h-$r;
            $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
            $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
            $xc = $x+$r ;
            $yc = $y+$h-$r;
            $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
            $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
            $xc = $x+$r ;
            $yc = $y+$r;
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
            $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
            $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
    {
            $h = $this->h;
            $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
                $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }    

    function Header()
    {
        global $nombre_comercial_s;
        global $nombre_s;
        global $municipio_s;
        global $estado_s;
        global $calles_s;
        global $colonia_s;
        global $rfc_s;
        global $telefono_s;
        global $numero_exterior_s;
        global $numero_interior_s;
        global $cp_s;
        global $solicitante;
        global $titulo;
        
        // Loo
        
        
        $pdf=new FPDF('P','mm','Letter'); /*Se define las propiedades de la página */
        
        $this->SetFillColor(260);
        
        $this->RoundedRect(5, 5, 203, 23, 2.5, 'DF'); // col inz., ren,largo,ancho,
        $this->RoundedRect(5, 29, 203, 9, 2.5, 'DF');
        $this->RoundedRect(5, 40, 203, 4, 2.5, 'DF');
        
        if (file_exists("../reportes/logo.jpg"))
            $this->Image('../reportes/logo.jpg',8,9,45,15);
        //$this->Image('../archivos/jpg/logo.jpg',8,9,45,15);
        //$this->Image('../archivos/jpg/logo_ilutec.jpg',8,9,45,15);
        
        //$this->Ln(3);
        $this->SetY(1);
        
        $this->SetFont('Arial','B',10);
        $this->Ln(6);
        $this->SetFont('Arial','B',8);
        $this->Cell(50);
        $this->Cell(10,1,$nombre_comercial_s,0,0,'L'); 
        $this->Cell(80);
        $this->Cell(10,1,"SOLICITANTE : ".$solicitante,0,0,'L'); 
        $this->Ln(3);
        $this->Cell(50);
        $this->Cell(10,1,$nombre_s,0,0,'L'); 
        $this->Ln(3);
        $this->Cell(50);
        $this->Cell(10,1,$calles_s." Num. ".$numero_exterior_s." ".$numero_interior_s."  ".$colonia_s,0,0,'L');
        $this->Ln(3);
        $this->Cell(50);
        $this->Cell(10,1,$municipio_s." ".$estado_s,0,0,'L');
        $this->Ln(3);
        $this->Cell(50);
        $this->Cell(10,1,"C.P.".$cp_s." Rfc: ".$rfc_s." Tel.:".$telefono_s,0,0,'L');
        $this->Ln(3);
        $this->Cell(50);
        $this->Cell(2,1, "Fecha/Hora ".date("d-m-Y H:i:s"),0,0,'L'); 

        $this->Ln(9);
        $this->SetFont('Arial','B',10);
        $this->Cell(-3);
        $this->SetFont('Arial','B',8);
        $this->Cell(5,1,"TITULO :  ",0,0,'L');
        $this->Cell(15);
        $this->SetFont('Arial','I',6);
        $this->Ln(4);
        $this->Cell(-3);
        
        $this->Cell(10);
        //$fecha="22/02/2012";
        $this->Ln(6);
        $this->SetFont('Arial','B',7);
        
        
        //$this->Ln(35);
        $this->Cell(1);
        $this->SetFont('Arial','I',7);
        $this->Cell(-4);
        $this->Cell(5,1,"ID",0,0,'L');
        $this->Cell(4);
        $this->Cell(10,1,"CODIGO",0,0,'L');
        $this->Cell(25);
        $this->Cell(10,1,"DESCRIPCION",0,0,'L');
        $this->Cell(100);
        $this->Cell(7,1,"MEDIDA",0,0,'L');
        $this->Cell(7);
        $this->Cell(10,1,"PROD-SERV",0,0,'L');
        $this->Cell(7);
        $this->Cell(10,1,"ESTATUS",0,0,'L');

        $this->SetFillColor(280,320,320);
        //body de pagina cuadritos
        //$this->RoundedRect(5, 44, 42, 220, 2.5, 'DF'); // col,ren,ancho,alto
        //$this->RoundedRect(47, 44, 115, 220, 2.5, 'DF');
        //$this->RoundedRect(162, 44, 23, 220, 2.5, 'DF');
        //$this->RoundedRect(185, 44, 23, 220, 2.5, 'DF');
        //$this->RoundedRect(185, 44, 23, 195, 2.5, 'DF');
        //$this->Cell(1);
        //----------------------------------
        // encabezados
        

        $this->Ln(5);
        $this->SetTextColor(0);

    }

    // Pie de página
    function Footer()
    {
        global $suma;
        global $descuento;
        global $subtotal;
        global $iva;
        global $ivaaplicado;
        global $total;
        global $folio;
        global $moneda_id;
        // Posición: a 1,5 cm del final
        //$this->SetY(-30);
        $iNumPagina = $this->PageNo();
        //Posición: a 15mm (1,5) cm del final
        $this->SetY(-10);
        $this->Cell(170);
        $this->Cell(10,1,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }


}

// Creación del objeto de la clase heredada
$pdf = new PDF();

$pdf ->FPDF( 'P', 'mm', 'Letter' );

/*$lineai=$_REQUEST[ 'lineai'];
$lineaf=$_REQUEST[ 'lineaf'];
$slineai=$_REQUEST[ 'slineai'];
$slineaf=$_REQUEST[ 'slineaf'];*/
$orden="nombre";
$qq=" articulos.idarticulos>0 ";
$q=" ";
$op=0;
$orden=$_REQUEST[ 'orden'];
if ($orden=="nombre")
{
    $q=" ORDER BY articulos.nombre";
    $op=1;
}
if ($orden=="linea")
{
     $q=" ORDER BY lineas.nombre,sublineas.nombre,articulos.nombre";
}
if ($orden=="codigo")
{
    $q=" ORDER BY articulos.codigo";
    $op=1;
}
if ($orden=="ACTIVO")
{
    $q=" ORDER BY lineas.nombre,articulos.nombre";
    $qq=" articulos.idestatus=1";
}
if ($orden=="ENPROCESO")
{
    $q=" ORDER BY lineas.nombre,articulos.nombre";
    $qq=" articulos.idestatus=4";
}
$lineai=1;
$lineaf=999;
$slineai=1;
$slineaf=999;
$solicitante=$_SESSION["nombre"];

$conecta =new Conecta();

//consultar datos generales de la matriz

if (empty($lineai) || empty($lineaf) || empty($slineai) || empty($slineaf) )
{
    echo "datos insuficientes para realizar consulta ... ";
    return;
} 

//hacer consulta de sucursal 
$sql = "SELECT idsucursales FROM sucursales WHERE  matriz='S' ";
$rs=($conecta->query($sql));
while ($fila = mysqli_fetch_assoc($rs))
$idsucursales=$fila["idsucursales"];



$sql = "SELECT
sucursales.idsucursales
, sucursales.nombre
, sucursales.nombrecomercial
, sucursales.rfc
, estados.nombre AS estado
, municipios.nombre AS municipio
, colonias.nombre AS colonia
, calles.nombre AS calle
, sucursales.numero_interior
, sucursales.numero_exterior
, sucursales.referencia
, sucursales.codigo_postal
, sucursales.telefono
FROM  sucursales
INNER JOIN municipios ON (municipios.idmunicipios = sucursales.idmunicipios )
INNER JOIN estados ON (estados.idestados = municipios.idestados )
INNER JOIN colonias ON (sucursales.idcolonias = colonias.idcolonias)
INNER JOIN calles ON (sucursales.idcalles = calles.idcalles) 
WHERE sucursales.idsucursales = $idsucursales";
//print_r("consulta datos de la sucursal $sql");
//return;
$rs=($conecta->query($sql));

while ($fila = mysqli_fetch_assoc($rs)) {
    
    $nombre_s=$fila["nombre"];
    $nombre_comercial_s=$fila["nombrecomercial"];//$fila["nomComercial"];
    $numero_exterior_s=$fila["numero_exterior"];
    $numero_interior_s=$fila["numero_interior"];
    $cp_s=$fila["codigo_postal"];
    $estado_s=$fila["estado"];
    $municipio_s=$fila["municipio"];
    $colonia_s=$fila["colonia"];
    $calles_s=$fila["calle"];
    $rfc_s=$fila["rfc"];
    $telefono_s=$fila["telefono"];
} 
//----------------------

//-------------------------------
//leer detalle de matriz

$sql="SELECT articulos.idarticulos,articulos.codigo,articulos.nombre AS articulo,lineas.nombre AS linea,sublineas.nombre AS sublinea,
articulos.idestatus,articulos.condicion,sat_unidad_medidas.clave AS um,sat_prod_servs.clave AS prod_serv,
estatus.nombre as estatus
FROM articulos
INNER JOIN sublineas ON sublineas.idsublineas=articulos.idsublineas 
INNER JOIN lineas ON lineas.idlineas=sublineas.idlineas
INNER JOIN sat_unidad_medidas ON sat_unidad_medidas.idsat_unidad_medidas=articulos.idsat_unidad_medidas
INNER JOIN sat_prod_servs ON sat_prod_servs.idsat_prod_servs=articulos.idsat_prod_servs
INNER JOIN estatus ON estatus.idestatus=articulos.idestatus 
WHERE ".$qq." ".$q;
//articulos.idsublineas BETWEEN $slineai AND $slineaf AND lineas.idlineas BETWEEN $lineai AND $lineaf 
//echo "orden $orden $sql";
$pdf->AliasNbPages();

$pdf->SetFont('Times','B',8);

$pdf->AddPage(); 


$r=280;$g=320;$b=320;
$r=240;$g=300;$b=320;
$pdf->SetFillColor($r,$g,$b);
$con=0;
$pdf->SetFillColor(280,320,320);

$nlin="";$nslin=""; 
$pdf->SetFont('Arial','I',7);
$rs=($conecta->query($sql));
while ($fila = mysqli_fetch_assoc($rs)) 
{
    $idarticulos=$fila["idarticulos"];
    $codigo=$fila["codigo"];
    $articulo=$fila["articulo"];
    $linea=$fila["linea"];
    $sublinea=$fila["sublinea"];
    $idestatus=$fila["idestatus"];
    $condicion=$fila["condicion"];
    $um=$fila["um"];
    $prod_serv=$fila["prod_serv"];
    $estatus=$fila["estatus"];
    $pdf->Ln(4);
    $pdf->Cell(-4);
    if ($nlin != $fila["linea"]  && $op==0)
    {
        $pdf->Ln(2);
        $pdf->SetFont('Times','B',10);    
        $pdf->Cell(40);
        $pdf->Cell(10,4,substr($linea,0,40),0,0,'L',false);
        $pdf->Cell(65);
        $pdf->Cell(10,4,$sublinea,0,0,'L',false);
        $pdf->Ln(5);
        $pdf->Cell(-4);
        
    }
    $pdf->Ln(2);
    $pdf->SetFont('Times','B',8);    
    $pdf->Cell(-3);
    $pdf->Cell(5,4,$idarticulos,0,0,'L',false);
    $pdf->Cell(5);
    $pdf->Cell(10,4,$codigo,0,0,'L',false);
    $pdf->Cell(25);
    //$pdf->Cell(10,4,substr($articulo,0,80),0,0,'L',false);
    //$pdf->Multicell(80,2,$articulo,1,'L',0);

    $Posx=$pdf->GetX();$Posy=$pdf->GetY();
    $pdf->Multicell(100,3,$articulo,0,'L',0);
    $pdf->SetXY($Posx, $Posy);

    $pdf->Cell(110);
    $pdf->Cell(7,4,$um,0,0,'L',false);
    $pdf->Cell(7);
    $pdf->Cell(7,4,$prod_serv,0,0,'L',false);
    $pdf->Cell(7);
    $pdf->Cell(7,4,$estatus,0,0,'L',false);
    $nlin=$fila["linea"];
    $nslin=$fila["sublinea"];
    
}

$pdf->Output();
?>


