<?php
//numtoletras
session_start();
if (!isset($_SESSION["nombre"]))
{
  header("location: ../vistas/login.html");
}

//require('../pdf/fpdf.php');
require('../pdf/pdf_js.php');
//exit;

class PDF_AutoPrint extends PDF_JavaScript
{
    function AutoPrint($dialog=false)
    {
      //Open the print dialog or start printing immediately on the standard printer
      $param=($dialog ? 'true' : 'false');
      $script="print($param);";
      $this->IncludeJS($script);
    }

    function AutoPrintToPrinter($server, $printer, $dialog=false)
    {
      //Print on a shared printer (requires at least Acrobat 6)
      $script = "var pp = getPrintParams();";
      if($dialog)
        $script .= "pp.interactive = pp.constants.interactionLevel.full;";
      else
        $script .= "pp.interactive = pp.constants.interactionLevel.automatic;";
      $script .= "pp.printerName = '\\\\\\\\".$server."\\\\".$printer."';";
      $script .= "print(pp);";
      $this->IncludeJS($script);
    }
}


//require('../pdf/rounded_rect.php');
class PDF extends FPDF
{

    function Lineas($w, $h, $txt, $border=0, $align='J', $fill=false,$NumLin)
    {
      //Output text with automatic or explicit line breaks
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 && $s[$nb-1]=="\n")
            $nb--;
        $b=0;
        if($border)
        {
            if($border==1)
            {
                $border='LTRB';
                $b='LRT';
                $b2='LR';
            }
            else
            {
                $b2='';
                if(strpos($border,'L')!==false)
                    $b2.='L';
                if(strpos($border,'R')!==false)
                    $b2.='R';
                $b=(strpos($border,'T')!==false) ? $b2.'T' : $b2;
            }
        }
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $ns=0;
        $nl=1;

        while($i<$nb)
        {
            //Get next character
            $c=$s[$i];
            if($c=="\n")
            {
                //Explicit line break
                if($this->ws>0)
                {
                    $this->ws=0;
                    $this->_out('0 Tw');
                }

                $this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
                            if ($nl==$NumLin) return($nl);
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $ns=0;

                $nl++;
                            if ($nl==$NumLin) return($nl);
                if($border && $nl==2)
                    $b=$b2;
                continue;
            }
            if($c==' ')
            {
                $sep=$i;
                $ls=$l;
                $ns++;
            }
            $l+=$cw[$c];
            if($l>$wmax)
            {
                //Automatic line break
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                    if($this->ws>0)
                    {
                        $this->ws=0;
                        $this->_out('0 Tw');
                    }

                    $this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
                                    if ($nl==$NumLin) return($nl);
                }
                else
                {
                    if($align=='J')
                    {
                        $this->ws=($ns>1) ? ($wmax-$ls)/1000*$this->FontSize/($ns-1) : 0;
                        $this->_out(sprintf('%.3F Tw',$this->ws*$this->k));
                    }

                    $this->Cell($w,$h,substr($s,$j,$sep-$j),$b,2,$align,$fill);
                                    if ($nl==$NumLin) return($nl);
                    $i=$sep+1;

                }
                $sep=-1;
                $j=$i;
                $l=0;
                $ns=0;
                            $nl++;
                            if($border && $nl==2)
                    $b=$b2;
            }
            else
                $i++;
        }
        //Last chunk
        if($this->ws>0)
        {
            $this->ws=0;
            $this->_out('0 Tw');
        }
        if($border && strpos($border,'B')!==false)
            $b.='B';
            if ($nl==$NumLin) return($nl);
        $this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
            if ($nl==$NumLin) return($nl);
        $this->x=$this->lMargin;
        return $nl; //THIS RETURNS THE NUMBER OF LINES!
    }

    function BasicTable($header, $data)
    {
         
    }
    function RoundedRect($x, $y, $w, $h, $r, $style = '')
    {
      $k = $this->k;
      $hp = $this->h;
      if($style=='F')
          $op='f';
      elseif($style=='FD' || $style=='DF')
          $op='B';
      else
          $op='S';
      $MyArc = 4/3 * (sqrt(2) - 1);
      $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));
      $xc = $x+$w-$r ;
      $yc = $y+$r;
      $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));

      $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
      $xc = $x+$w-$r ;
      $yc = $y+$h-$r;
      $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
      $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
      $xc = $x+$r ;
      $yc = $y+$h-$r;
      $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
      $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
      $xc = $x+$r ;
      $yc = $y+$r;
      $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
      $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
      $this->_out($op);       
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
    {
      $h = $this->h;
      $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,$x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));        
    }

  function Header()
  {
      
      
      
      $this->RoundedRect(5, 9, 203, 17, 2.5, 'DF'); // col inz., ren,largo,ancho,
      $this->RoundedRect(5, 29, 203, 9, 2.5, 'DF');
      $this->RoundedRect(5, 40, 203, 4, 2.5, 'DF');
      

  }
  function Footer()
  {
    $this->Cell(10,1,"PIE DE PAGINA ",0,0,'L');
     $this->RoundedRect(5, 9, 203, 17, 2.5, 'DF'); // col inz., ren,largo,ancho,
      $this->RoundedRect(5, 29, 203, 9, 2.5, 'DF');
      $this->RoundedRect(5, 40, 203, 4, 2.5, 'DF');
  }


   

}



error_reporting(E_ALL);
ini_set('display_errors', '1');

$pdf = new PDF();
$pdf->SetFont('Times','B',7);
$pdf->AddPage();
$pdf ->FPDF( 'P', 'mm', 'Letter' );
$pdf->AddPage();
 $pdf->RoundedRect(5, 9, 203, 17, 2.5, 'DF'); // col inz., ren,largo,ancho,
 $pdf->RoundedRect(5, 29, 203, 9, 2.5, 'DF');
 $pdf->RoundedRect(5, 40, 203, 4, 2.5, 'DF');


$usuario=$_REQUEST[ 'username' ];
$password=$_REQUEST[ 'password' ];
$orden=$_REQUEST[ 'orden' ];
$usuario = base64_decode( $usuario );
$password = base64_decode( $password );
$orden = base64_decode( $orden );

//require_once "../modelos/Usuarios.php"; 
//$usuarios = new Usuarios();
require_once "../modelos/Aperturas.php";
$usuarios = new Aperturas();

$password=md5($password);
$registros = $usuarios->verificar($usuario,$password);


$cantidad=0;
$idusuarios=0;
$nombre="";
while ($row = $registros->fetch_object()) 
{
    $idusuarios=$row->idusuarios;
    $nombre=$row->nombre;
}
//echo "usarios $nombre";
//return;

if (empty($idusuarios))
{
   header("location: ../vistas/login.html");
}

//obtener datos para impresion
require_once "../modelos/Articulos.php";
$articulos = new Articulos();

$registros = $articulos->listarActivosPorOrden($orden);
while ($row = $registros->fetch_object()) 
{
    $idarticulos=$row->idarticulos;
    $articulo=$row->articulo;
    $codigo=$row->codigo;
    $linea=$row->linea;
    $sublinea=$row->sublinea;
}


$pdf=new PDF_AutoPrint();
$pdf->AddPage();
$pdf->SetFont('Arial','B',12);
$pdf->Cell(-3);
$pdf->Cell(4,4,"APERTURA DE CAJA ".date("H:i:s")."usuario ".$usuario." ".$articulo,0,0,'L');//." password ".$password,*/
$pdf->AddPage();
$pdf->Cell(-3);
$pdf->Cell(4,4,"APERTURA DE CAJA ".date("H:i:s")."usuario ".$usuario." ".$articulo,0,0,'L');//." password ".$password,*/
$pdf->AddPage();
$pdf->Cell(-3);
$pdf->Cell(4,4,"APERTURA DE CAJA ".date("H:i:s")."usuario ".$usuario." ".$articulo,0,0,'L');//." password ".$password,*/
$pdf->AddPage();
$pdf->Cell(-3);
$pdf->Cell(4,4,"APERTURA DE CAJA ".date("H:i:s")."usuario ".$usuario." ".$articulo,0,0,'L');//." password ".$password,*/
$pdf->AddPage();
$pdf->Cell(-3);
$pdf->Cell(4,4,"APERTURA DE CAJA ".date("H:i:s")."usuario ".$usuario." ".$articulo,0,0,'L');//." password ".$password,*/

//**************************************************
$x=1;
if ($x ==0)
{
    //$pdf->Ln(20);
      $folio_id=6;
      $pdf->SetFont('Arial','B',12);
      $pdf->Cell(-3);
      $pdf->Cell(10,1,"APERTURA DE CAJA ".date("H:i:s"),0,1,'L');
     
      $ruta_general = 'http://' . $_SERVER['HTTP_HOST'] . '/php/archivos/';
        $file=$folio_id.".pdf";
        $dir="../archivos";
        //$pdf->Output($file,'I'); //$path,'F'
        $pdf->Output("../archivos/".$file,"F");
        $archivo=$ruta_general.$file;
        $url = $archivo;
        //echo $url;
        
        //Lo codificamos en base 64 para que la aplicación local lo lea
        $url = base64_encode($url);
        //echo $url;

}
  
$pdf->AutoPrint(true);
$pdf->Output();

?>

<!--<html> 
    <head> 
            <script>
                    function EjecutaImpresion() {
                            window.location.href='primprimir://<?php echo $url; ?>';
                            setTimeout(window.close,900);
                    }
            </script>
    </head> 
    
    <body onload="EjecutaImpresion();">

        
    </body> 
</html>-->


