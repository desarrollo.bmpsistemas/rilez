<?php
//Activamos el almacenamiento en el buffer
ob_start();
if (strlen(session_id()) < 1) 
  session_start();
$_SESSION["nombre"]="admin";
$_SESSION["ventas"]=1;

if (!isset($_SESSION["nombre"]))
{
  echo 'Debe ingresar al sistema correctamente para visualizar el reporte';
}
else
{
if ($_SESSION['ventas']==1)
{
?>
<body onload="window.print();">
<?php

//Incluímos la clase Venta
require_once "../modelos/Articulos.php";
//Instanaciamos a la clase con el objeto venta
$articulos = new Articulos();
//En el objeto $rspta Obtenemos los valores devueltos del método ventacabecera del modelo
//$rspta = $venta->ventacabecera($_GET["id"]);
//Recorremos todos los valores obtenidos
//$reg = $rspta->fetch_object();

//Establecemos los datos de la empresa
$empresa = "Proveedora de material de cobre S.A";
$documento = "PMC111111111";
$direccion = "CULIACAN SINALOA";
$telefono = "9921471";
$email = "culiacan@rilez.mx";

?>
<div class="zona_impresion">
<!-- codigo imprimir -->
<br>
<table border="0" align="center" width="300px">
    <tr>
        <td align="center">
        <!-- Mostramos los datos de la empresa en el documento HTML -->
        .::<strong> <?php echo $empresa; ?></strong>::.<br>
        <?php echo $documento; ?><br>
        <?php echo $direccion .' - '.$telefono; ?><br>
        </td>
    </tr>
    <tr>
        <td align="center"><?php echo "fecha"; ?></td>
    </tr>
    <tr>
      <td align="center"></td>
    </tr>
    <tr>
        <!-- Mostramos los datos del cliente en el documento HTML -->
        <td>Cliente: <?php echo "cliente"; ?></td>
    </tr>
    <tr>
        <td><?php echo "tipo_documento." ?></td>
    </tr>
    <tr>
        <td>Nº de venta: <?php echo "comprobante." ?></td>
    </tr>    
</table>
<br>
<!-- Mostramos los detalles de la venta en el documento HTML -->
<table border="0" align="center" width="300px">
    <tr>
        <td>CANT.</td>
        <td>DESCRIPCIÓN</td>
        <td align="right">IMPORTE</td>
    </tr>
    <tr>
      <td colspan="3">==========================================</td>
    </tr>
    <?php
    
    $referencia="";
    $Opcion=0;
    

    $rsptad = $articulos->listar($Opcion,$referencia);
    $cantidad=0;
    while ($regd = $rsptad->fetch_object()) {
        echo "<tr>";
        echo "<td>".$regd->idarticulos."</td>";
        echo "<td>".$regd->articulo;
        echo "<td align='right'>S/ ".$regd->codigo."</td>";
        echo "</tr>";
        $cantidad++;
    }
    ?>
    <!-- Mostramos los totales de la venta en el documento HTML -->
    <tr>
    <td>&nbsp;</td>
    <td align="right"><b>TOTAL:</b></td>
    <td align="right"><b>S/  <?php echo "total".$cantidad;  ?></b></td>
    </tr>
    <tr>
      <td colspan="3">Nº de artículos: <?php echo $cantidad; ?></td>
    </tr>
    <tr>
      <td colspan="3">&nbsp;</td>
    </tr>      
    <tr>
      <td colspan="3" align="center">¡Gracias por su compra!</td>
    </tr>
    <tr>
      <td colspan="3" align="center">IncanatoIT</td>
    </tr>
    <tr>
      <td colspan="3" align="center">Chiclayo - Perú</td>
    </tr>
    
</table>
<br>
</div>
<p>&nbsp;</p>

</body>
</html>
<?php 
}
else
{
  echo 'No tiene permiso para visualizar el reporte';
}

}
ob_end_flush();
?>