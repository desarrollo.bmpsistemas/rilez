<?php

class Conecta
{
    private $_servidor;
    private $_base;
    private $_usuario;
    private $_clave;
    var $conn;

    function __construct() {
        $this->_servidor    = 'localhost';
        $this->_base        = 'dbrilezmexicana';
        $this->_usuario     = 'root';
        $this->_clave       = '';

        $this->conn = mysqli_connect( $this->_servidor, $this->_usuario, $this->_clave ) or die( mysqli_error($this->conn) );
        mysqli_select_db($this->conn, $this->_base) or die(mysqli_error($this->conn) );
        return true;
    }

    function query($query){
        $this->results = mysqli_query($this->conn,$query) or die( mysqli_error($this->conn) );
        //$this->numFields = mysqli_num_fields($this->results);
        return ($this->results);
    }
    
    function __destruct(){
        mysqli_close($this->conn);
        return true;
    }
    
    //Ejemplos de como usar las funciones de arriba
    //$conecta = new Conecta(); 
    //($conecta->query($sql));
}
