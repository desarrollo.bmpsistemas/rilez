<?php
//numtoletras
GLOBAL $variable;
GLOBAL $moneda_id;
require_once('../pdf/fpdf.php');
require_once('../pdf/letras.php');
require('conecta_db.php');
session_start();
if (!isset($_SESSION["nombre"]))
{
  header("location: ../vistas/login.html");
}
class PDF extends FPDF
{
    function Lineas($w, $h, $txt, $border=0, $align='J', $fill=false,$NumLin)
    {
    	//Output text with automatic or explicit line breaks
    	$cw=&$this->CurrentFont['cw'];
    	if($w==0)
    		$w=$this->w-$this->rMargin-$this->x;
    	$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
    	$s=str_replace("\r",'',$txt);
    	$nb=strlen($s);
    	if($nb>0 && $s[$nb-1]=="\n")
    		$nb--;
    	$b=0;
    	if($border)
    	{
    		if($border==1)
    		{
    			$border='LTRB';
    			$b='LRT';
    			$b2='LR';
    		}
    		else
    		{
    			$b2='';
    			if(strpos($border,'L')!==false)
    				$b2.='L';
    			if(strpos($border,'R')!==false)
    				$b2.='R';
    			$b=(strpos($border,'T')!==false) ? $b2.'T' : $b2;
    		}
    	}
    	$sep=-1;
    	$i=0;
    	$j=0;
    	$l=0;
    	$ns=0;
    	$nl=1;
            
    	while($i<$nb)
    	{
    		//Get next character
    		$c=$s[$i];
    		if($c=="\n")
    		{
    			//Explicit line break
    			if($this->ws>0)
    			{
    				$this->ws=0;
    				$this->_out('0 Tw');
    			}
                            
    			$this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
                            if ($nl==$NumLin) return($nl);
    			$i++;
    			$sep=-1;
    			$j=$i;
    			$l=0;
    			$ns=0;
                            
    			$nl++;
                            if ($nl==$NumLin) return($nl);
    			if($border && $nl==2)
    				$b=$b2;
    			continue;
    		}
    		if($c==' ')
    		{
    			$sep=$i;
    			$ls=$l;
    			$ns++;
    		}
    		$l+=$cw[$c];
    		if($l>$wmax)
    		{
    			//Automatic line break
    			if($sep==-1)
    			{
    				if($i==$j)
    					$i++;
    				if($this->ws>0)
    				{
    					$this->ws=0;
    					$this->_out('0 Tw');
    				}
                                    
    				$this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
                                    if ($nl==$NumLin) return($nl);
    			}
    			else
    			{
    				if($align=='J')
    				{
    					$this->ws=($ns>1) ? ($wmax-$ls)/1000*$this->FontSize/($ns-1) : 0;
    					$this->_out(sprintf('%.3F Tw',$this->ws*$this->k));
    				}
                                    
    				$this->Cell($w,$h,substr($s,$j,$sep-$j),$b,2,$align,$fill);
                                    if ($nl==$NumLin) return($nl);
    				$i=$sep+1;
                                    
    			}
    			$sep=-1;
    			$j=$i;
    			$l=0;
    			$ns=0;
                            $nl++;
                            if($border && $nl==2)
    				$b=$b2;
    		}
    		else
    			$i++;
    	}
    	//Last chunk
    	if($this->ws>0)
    	{
    		$this->ws=0;
    		$this->_out('0 Tw');
    	}
    	if($border && strpos($border,'B')!==false)
    		$b.='B';
            if ($nl==$NumLin) return($nl);
    	$this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
            if ($nl==$NumLin) return($nl);
    	$this->x=$this->lMargin;
    	return $nl; //THIS RETURNS THE NUMBER OF LINES!
    }
    ////--------------    
        
    function BasicTable($header, $data)
    {

    }   
    function RoundedRect($x, $y, $w, $h, $r, $style = '')
    {
            $k = $this->k;
            $hp = $this->h;
            if($style=='F')
                $op='f';
            elseif($style=='FD' || $style=='DF')
                $op='B';
            else
                $op='S';
            $MyArc = 4/3 * (sqrt(2) - 1);
            $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));
            $xc = $x+$w-$r ;
            $yc = $y+$r;
            $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));

            $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
            $xc = $x+$w-$r ;
            $yc = $y+$h-$r;
            $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
            $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
            $xc = $x+$r ;
            $yc = $y+$h-$r;
            $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
            $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
            $xc = $x+$r ;
            $yc = $y+$r;
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
            $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
            $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
    {
            $h = $this->h;
            $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
                $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }    

    function Header()
    {
        global $nombre_comercial_s;
        global $nombre_s;
        global $municipio_s;
        global $estado_s;
        global $calles_s;
        global $colonia_s;
        global $rfc_s;
        global $telefono_s;
        global $numero_exterior_s;
        global $numero_interior_s;
        global $cp_s;
        global $solicitante;
        global $titulo;

        global $fecha;
        global $vencimiento;
        global $documento;
        global $referencia;
        global $origen;
        global $destino;
        global $estatus;
        global $ident_sal;
        
        // Loo
        
        
        $pdf=new FPDF('P','mm','Letter'); /*Se define las propiedades de la página */
        
        $this->SetFillColor(260);
        
        $this->RoundedRect(5, 5, 203, 23, 2.5, 'DF'); // col inz., ren,largo,ancho,
        $this->RoundedRect(5, 29, 203, 9, 2.5, 'DF');
        $this->RoundedRect(5, 40, 203, 4, 2.5, 'DF');
        
        if (file_exists("../reportes/logo.jpg"))
            $this->Image('../reportes/logo.jpg',8,9,45,15);
        //$this->Image('../archivos/jpg/logo.jpg',8,9,45,15);
        //$this->Image('../archivos/jpg/logo_ilutec.jpg',8,9,45,15);
        
        //$this->Ln(3);
        $this->SetY(1);
        
        $this->SetFont('Arial','B',10);
        $this->Ln(6);
        $this->SetFont('Arial','B',8);
        $this->Cell(50);
        $this->Cell(10,1,$nombre_comercial_s,0,0,'L'); 
        $this->Cell(80);
        $this->Cell(10,1,"SOLICITANTE : ".$solicitante,0,0,'L'); 
        $this->Ln(3);
        $this->Cell(50);
        $this->Cell(10,1,$nombre_s,0,0,'L'); 
        $this->Ln(3);
        $this->Cell(50);
        $this->Cell(10,1,$referencia,0,0,'L');
        $this->Ln(3);
        $this->Cell(50);
        $this->Cell(10,1,"",0,0,'L');
        $this->Ln(3);
        $this->Cell(50);
        $this->Cell(10,1,"",0,0,'L');
        $this->Ln(3);
        $this->Cell(50);
        $this->Cell(2,1, "Fecha/Hora ".date("d-m-Y H:i:s"),0,0,'L'); 

        $this->Ln(9);
        $this->SetFont('Arial','B',8);
        $this->Cell(-3);
        
        $this->Cell(5,1,"DOCUMENTO",0,0,'L');
        $this->Cell(25);
        $this->Cell(10,1,"FOLIO",0,0,'L');
        $this->Cell(12);
        $this->Cell(10,1,"FECHA",0,0,'L');
        $this->Cell(10);
        $this->Cell(10,1,"VENCE",0,0,'L');
        $this->Cell(12);
        $this->Cell(10,1,"REF.",0,0,'L');
        $this->Cell(10);
        $this->Cell(10,1,"ORIGEN",0,0,'L');
        $this->Cell(25);
        $this->Cell(10,1,"DESTINO",0,0,'L');
        $this->Cell(18);
        $this->Cell(10,1,"ESTATUS",0,0,'L');
        $this->SetFont('Arial','I',8);
        $this->Ln(4);
        $this->Cell(-3);
        $this->Cell(5,1,substr($documento,0,20),0,0,'L');
        $this->Cell(25);
        $this->Cell(10,1,$ident_sal,0,0,'L');
        $this->Cell(12);
        $this->Cell(10,1,$fecha,0,0,'L');
        $this->Cell(10);
        $this->Cell(10,1,$vencimiento,0,0,'L');
        $this->Cell(12);
        $this->Cell(10,1,$referencia,0,0,'L');
        $this->Cell(10);
        $this->Cell(10,1,substr($origen,0,20),0,0,'L');
        $this->Cell(25);
        $this->Cell(15,1,substr($destino,0,20),0,0,'L');
        $this->Cell(15);
        $this->Cell(10,1,substr($estatus,0,10),0,0,'L');
        $this->Cell(10);
        //$fecha="22/02/2012";
        $this->Ln(6);
        $this->SetFont('Arial','B',7);
        
        
        //$this->Ln(35);
        $this->Cell(1);
        $this->SetFont('Arial','I',7);
        $this->Cell(-4);
        $this->Cell(5,1,"CODIGO",0,0,'L');
        $this->Cell(18);
        $this->Cell(10,1,"MEDIDA",0,0,'L');
        $this->Cell(6);
        $this->Cell(10,1,"DESCRIPCION",0,0,'L');
        $this->Cell(102);
        $this->Cell(7,1,"CANTIDAD",0,0,'L');
        $this->Cell(10);
        $this->Cell(10,1,"PRECIO",0,0,'L');
        $this->Cell(7);
        $this->Cell(10,1,"IMPORTE",0,0,'L');

        $this->SetFillColor(280,320,320);
        //body de pagina cuadritos
        //$this->RoundedRect(5, 44, 42, 220, 2.5, 'DF'); // col,ren,ancho,alto
        //$this->RoundedRect(47, 44, 115, 220, 2.5, 'DF');
        //$this->RoundedRect(162, 44, 23, 220, 2.5, 'DF');
        //$this->RoundedRect(185, 44, 23, 220, 2.5, 'DF');
        //$this->RoundedRect(185, 44, 23, 195, 2.5, 'DF');
        //$this->Cell(1);
        //----------------------------------
        // encabezados
        

        $this->Ln(5);
        $this->SetTextColor(0);

    }

    // Pie de página
    function Footer()
    {
        
        global $Subtotal;
        global $Iva;
        global $ivaaplicado;
        global $Total;
        global $folio;
        global $moneda_id;
        // Posición: a 1,5 cm del final
        $this->SetY(-25);
        $this->Cell(188);
        $this->Cell(7,1,number_format($Subtotal,2),0,0,'R');
        $this->Ln(3);
        $this->Cell(158);
        $this->Cell(10,1,"IVA  ".$ivaaplicado." %",0,0,'L');
        $this->Cell(17);
        $this->Cell(10,1,number_format($Iva,2),0,0,'R');
        $this->Ln(3);
        $this->Cell(185);
        $this->Cell(10,1,number_format($Total,2),0,0,'R');
        $this->Ln(3);
        $this->Line(5, 250 , 210, 250);  //Horizontal
        $this->Ln(8);
        $this->Line(5, 265 , 210, 265);  //Horizontal
        //Posición: a 15mm (1,5) cm del final
        $this->SetY(-10);
        $this->Cell(180);
        $this->Cell(10,1,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }


}

// Creación del objeto de la clase heredada
$pdf = new PDF();

$pdf ->FPDF( 'P', 'mm', 'Letter' );



$conecta =new Conecta();

//consultar datos generales de la matriz
$ident_sal=$_REQUEST[ 'ident_sal'];
$tipo_mov=$_REQUEST[ 'tipo_mov'];

$solicitante=$_SESSION["nombre"];

if (empty($ident_sal) || !isset($solicitante) || !isset($tipo_mov) || empty($tipo_mov) )
{
    echo "datos insuficientes para realizar consulta ... ";
    return;
} 
//$documento,$fecha,$vencimiento,$referencia,$origen,$destino,$estatus,$id_ent_sal
//hacer consulta de sucursal ,totaldocumento,nota
if ($tipo_mov=='E')
{
    $sql = "SELECT almacen_id as idalmacenes,almacen_origen as idalmacenesd,subtotal,iva,total,ivaaplicado,fecha,vencimiento,tipo_mov_id as idtipo_movs,estatus_id as idestatus,referencia,proveedor_id as idpersonas FROM entradas WHERE id=$ident_sal";
    $rs=($conecta->query($sql));
    while ($fila = mysqli_fetch_assoc($rs))
    {
        $idalmacenes=$fila["idalmacenes"];
        $idalmacenesd=$fila["idalmacenesd"];
        $subtotal=$fila["subtotal"];
        $iva=$fila["iva"];
        $total=$fila["total"];
        $ivaaplicado=$fila["ivaaplicado"];
        $fecha=$fila["fecha"];
        $vencimiento=$fila["vencimiento"];
        $idtipo_movs=$fila["idtipo_movs"];
        $idestatus=$fila["idestatus"];
        $referencia=$fila["referencia"];
        $idpersonas=$fila["idpersonas"];
        //$totaldocumento=$fila["totaldocumento"];
        //$nota=$fila["nota"];
    }
}
else
{
    $sql = "SELECT almacen_id as idalmacenes,almacen_destino as idalmacenesd,subtotal,iva,total,ivaaplicado,fecha,vencimiento,tipo_mov_id as idtipo_movs,estatus_id as idestatus,referencia,cliente_id as idpersonas FROM salidas WHERE id=$ident_sal";
    $rs=($conecta->query($sql));
    while ($fila = mysqli_fetch_assoc($rs))
    {
        $idalmacenes=$fila["idalmacenes"];
        $idalmacenesd=$fila["idalmacenesd"];
        $Subtotal=$fila["subtotal"];
        $Iva=$fila["iva"];
        $Total=$fila["total"];
        $ivaaplicado=$fila["ivaaplicado"];
        $fecha=$fila["fecha"];
        $vencimiento=$fila["vencimiento"];
        $idtipo_movs=$fila["idtipo_movs"];
        $idestatus=$fila["idestatus"];
        $referencia=$fila["referencia"];
        $idpersonas=$fila["idpersonas"];
        //$totaldocumento=$fila["totaldocumento"];
        //$nota=$fila["nota"];
    }
}
//echo "$sql";
$sql = "SELECT descripcion as nombre FROM tipo_mov WHERE  id='$idtipo_movs' ";
$rs=($conecta->query($sql));
while ($fila = mysqli_fetch_assoc($rs))
$documento=$fila["nombre"];

$sql = "SELECT nombre FROM almacenes WHERE  id='$idalmacenes' ";
$rs=($conecta->query($sql));
while ($fila = mysqli_fetch_assoc($rs))
$origen=$fila["nombre"];

$sql = "SELECT nombre FROM almacenes WHERE  id='$idalmacenesd' ";
$rs=($conecta->query($sql));
while ($fila = mysqli_fetch_assoc($rs))
$destino=$fila["nombre"];

$sql = "SELECT descripcion as nombre FROM estatus WHERE  id='$idestatus' ";
$rs=($conecta->query($sql));
while ($fila = mysqli_fetch_assoc($rs))
$estatus=$fila["nombre"];


//echo "almacen id $idalmacenes";

$sql = "SELECT sucursal_id as idsucursales FROM almacenes WHERE  id='$idalmacenes' ";
$rs=($conecta->query($sql));
while ($fila = mysqli_fetch_assoc($rs))
$idsucursales=$fila["idsucursales"];
//echo "sucursal id $idsucursales";

//echo "aqui va ";
//exit;
if ($tipo_mov=='E')
{
    $sql = "SELECT descripcion as nombre,telefono,correo,rfc,referencia FROM proveedores WHERE  id='$idpersonas' ";
    $rs=($conecta->query($sql));
    while ($fila = mysqli_fetch_assoc($rs))
    {
        $nombre_p=$fila["nombre"];    
        $telefono_p=$fila["telefono"];
        $correo_p=$fila["correo"];
        $referencia_p=$fila["referencia"];            
    }
    
}
else
{
    $sql = "SELECT descripcion as nombre,telefono,correo,rfc,referencia FROM clientes WHERE  id='$idpersonas' ";
    $rs=($conecta->query($sql));
    while ($fila = mysqli_fetch_assoc($rs))
    {
        $nombre_p=$fila["nombre"];    
        $telefono_p=$fila["telefono"];
        $correo_p=$fila["correo"];
        $referencia_p=$fila["referencia"];            
    }
}

$sql = "SELECT
sucursales.id AS idsucursales
, sucursales.descripcion AS nombre
, sucursales.nomComercial AS nombrecomercial
, sucursales.rfc
, sucursales.referencia
, sucursales.telefono
, sucursales.referencia
FROM  sucursales
WHERE sucursales.id = $idsucursales";
//print_r("consulta datos de la sucursal $sql");
//return;
$rs=($conecta->query($sql));

while ($fila = mysqli_fetch_assoc($rs)) {
    
    $nombre_s=$fila["nombre"];
    $nombre_comercial_s=$fila["nombrecomercial"];//$fila["nomComercial"];
    $rfc_s=$fila["rfc"];
    $telefono_s=$fila["telefono"];
    $referencia_s=$fila["referencia"];
} 
//----------------------

//-------------------------------
//leer detalle de matriz
if ($tipo_mov=='S')
    $sql="SELECT articulos.id2 as codigo,sat_unidad_medida.clave,articulos.descripcion AS articulo,detalles_salidas.cantidad,detalles_salidas.precio as valor,detalles_salidas.iva  FROM detalles_salidas 
    INNER JOIN articulos ON articulos.id=detalles_salidas.articulo_id
    INNER JOIN sat_unidad_medida ON sat_unidad_medida.id = articulos.sat_unidad_medida_id
    WHERE salida_id =$ident_sal ";
else
    $sql="SELECT articulos.id2 as codigo,sat_unidad_medida.clave,articulos.descripcion AS articulo,detalles_entradas.cantidad,detalles_entradas.costo as valor,detalles_entradas.iva  FROM detalles_entradas 
    INNER JOIN articulos ON articulos.id=detalles_entradas.articulo_id
    INNER JOIN sat_unidad_medida ON sat_unidad_medida.id = articulos.sat_unidad_medida_id
    WHERE entrada_id =$ident_sal ";

//echo "consulta tipo mov $tipo_mov $sql";
$pdf->AliasNbPages();

$pdf->SetFont('Times','B',8);

$pdf->AddPage(); 


$r=280;$g=320;$b=320;
$r=240;$g=300;$b=320;
$pdf->SetFillColor($r,$g,$b);
$con=0;
$pdf->SetFillColor(280,320,320);
$pdf->SetFont('Times','B',8);    


$pdf->SetFont('Arial','I',8);
$rs=($conecta->query($sql));
while ($fila = mysqli_fetch_assoc($rs)) 
{
    
    $codigo=$fila["codigo"];
    $articulo=$fila["articulo"];
    $um=$fila["clave"];
    $cantidad=$fila["cantidad"];
    $precio=$fila["valor"];
    $iva=$fila["iva"];
    $pdf->Ln(4);
    $pdf->Cell(-4);
    
    $pdf->Cell(5,4,$codigo,0,0,'L',false);
    $pdf->Cell(20);
    $pdf->Cell(5,4,$um,0,0,'L',false);
    $pdf->Cell(10);

    $Posx=$pdf->GetX();$Posy=$pdf->GetY();
    $pdf->Ln(1);
    $pdf->Cell(37);
    $pdf->Multicell(100,2,$articulo,0,'L',0);
    
    $pdf->SetXY($Posx, $Posy);
    
    //$pdf->Cell(50);
    $importe=$cantidad*$precio;
    $pdf->Cell(118);
    $pdf->Cell(8,4,$cantidad,0,0,'R',false);
    $pdf->Cell(6);
    $pdf->Cell(8,4,number_format($precio,2),0,0,'R',false);
    $pdf->Cell(11);
    $pdf->Cell(8,4,number_format($importe,2),0,0,'R',false);
    
    
}

$pdf->Output();
?>


