<?php
//numtoletras
GLOBAL $variable;
GLOBAL $moneda_id;
GLOBAL $moneda;
require_once('../pdf/fpdf.php');
require_once('../pdf/letras2.php');

require('conecta_db.php');
session_start();
if (!isset($_SESSION["nombre"]))
{
  header("location: ../vistas/login.html");
}

function redondeo ($numero, $decimales) { 
$factor = pow(10, $decimales);
return (round($numero*$factor)/$factor); }

 

class PDF extends FPDF
{
//--------------
    
function Lineas($w, $h, $txt, $border=0, $align='J', $fill=false,$NumLin)
{
    //Output text with automatic or explicit line breaks
    $cw=&$this->CurrentFont['cw'];
    if($w==0)
        $w=$this->w-$this->rMargin-$this->x;
    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
    $s=str_replace("\r",'',$txt);
    $nb=strlen($s);
    if($nb>0 && $s[$nb-1]=="\n")
        $nb--;
    $b=0;
    if($border)
    {
        if($border==1)
        {
            $border='LTRB';
            $b='LRT';
            $b2='LR';
        }
        else
        {
            $b2='';
            if(strpos($border,'L')!==false)
                $b2.='L';
            if(strpos($border,'R')!==false)
                $b2.='R';
            $b=(strpos($border,'T')!==false) ? $b2.'T' : $b2;
        }
    }
    $sep=-1;
    $i=0;
    $j=0;
    $l=0;
    $ns=0;
    $nl=1;
        
    while($i<$nb)
    {
        //Get next character
        $c=$s[$i];
        if($c=="\n")
        {
            //Explicit line break
            if($this->ws>0)
            {
                $this->ws=0;
                $this->_out('0 Tw');
            }
                        
            $this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
                        if ($nl==$NumLin) return($nl);
            $i++;
            $sep=-1;
            $j=$i;
            $l=0;
            $ns=0;
                        
            $nl++;
                        if ($nl==$NumLin) return($nl);
            if($border && $nl==2)
                $b=$b2;
            continue;
        }
        if($c==' ')
        {
            $sep=$i;
            $ls=$l;
            $ns++;
        }
        $l+=$cw[$c];
        if($l>$wmax)
        {
            //Automatic line break
            if($sep==-1)
            {
                if($i==$j)
                    $i++;
                if($this->ws>0)
                {
                    $this->ws=0;
                    $this->_out('0 Tw');
                }
                                
                $this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
                                if ($nl==$NumLin) return($nl);
            }
            else
            {
                if($align=='J')
                {
                    $this->ws=($ns>1) ? ($wmax-$ls)/1000*$this->FontSize/($ns-1) : 0;
                    $this->_out(sprintf('%.3F Tw',$this->ws*$this->k));
                }
                                
                $this->Cell($w,$h,substr($s,$j,$sep-$j),$b,2,$align,$fill);
                                if ($nl==$NumLin) return($nl);
                $i=$sep+1;
                                
            }
            $sep=-1;
            $j=$i;
            $l=0;
            $ns=0;
                        $nl++;
                        if($border && $nl==2)
                $b=$b2;
        }
        else
            $i++;
    }
    //Last chunk
    if($this->ws>0)
    {
        $this->ws=0;
        $this->_out('0 Tw');
    }
    if($border && strpos($border,'B')!==false)
        $b.='B';
        if ($nl==$NumLin) return($nl);
    $this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
        if ($nl==$NumLin) return($nl);
    $this->x=$this->lMargin;
    return $nl; //THIS RETURNS THE NUMBER OF LINES!
}
////--------------    
   
 function BasicTable($header, $data)
{
   
}   
 function RoundedRect($x, $y, $w, $h, $r, $style = '')
    {
        $k = $this->k;
        $hp = $this->h;
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='B';
        else
            $op='S';
        $MyArc = 4/3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));
        $xc = $x+$w-$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));

        $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
        $xc = $x+$w-$r ;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
        $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
        $xc = $x+$r ;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
        $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
        $xc = $x+$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
        $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
    {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
            $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }    
// Cabecera de pÃ¡gina
function Header()
{
    global $nombre_comercial_m;
    global $nombre_m;
    global $referencia_m;
    global $telefono_m;
    global $correo_m;
    global $rfc_m;

    global $nombre_comercial_s;
    global $nombre_s;
    global $referencia_s;
    global $telefono_s;
    global $correo_s;
    global $rfc_s;

    global $nombre_comercial_p;
    global $nombre_p;
    global $referencia_p;
    global $telefono_p;
    global $correo_p;
    global $rfc_p;

    global $capturo;
    global $solicito;
    global $documento;
    global $numero;
    global $fecha;
    global $vencimiento;
    global $forma_de_pago;
    global $moneda;
    global $valor_de_moneda;


    global $vendedor;

   
  
    global $almacen_origen;    

   
    
    
    global $referencia;
    global $observaciones;
    
    
    $pdf=new FPDF('P','mm','Letter'); /*Se define las propiedades de la pÃ¡gina */
    //$this->AddPage('P','mm','Letter'); 
    //$pdf->AddPage();
    // cuadros para encabezados
    $this->SetFillColor(260);
    $this->RoundedRect(5, 8, 203, 17, 2.5, 'DF'); // col inz., ren,largo,ancho,
    $this->RoundedRect(5, 26, 203, 9, 2.5, 'DF');
    
    $this->RoundedRect(5, 39, 101, 19, 2.5, 'DF');
    $this->RoundedRect(106, 39, 102, 19, 2.5, 'DF');
    
     $this->RoundedRect(5, 60, 203, 4, 2.5, 'DF');
    //$this->RoundedRect(126, 78, 82, 20, 2.5, 'DF');
    //$this->RoundedRect(5, 100, 203, 8, 2.5, 'DF');
    $this->RoundedRect(5, 65, 203, 193, 2.5, 'DF');
    
    
    
    if (file_exists("../reportes/logo.jpg"))
            $this->Image('../reportes/logo.jpg',8,9,45,15);
    
    
    
    $this->SetY(1);
    $this->Ln(4);
    //$this->SetFont('Arial','B',9);
    
    $this->SetFont('Arial','B',8);
    $this->Cell(50);
    $this->Cell(10,1,"                                Capturo : ".$capturo." Solicito : ".$_SESSION["nombre"],0,1,'L'); 
    $this->Ln(4);
    $this->Cell(50);
    $this->Cell(10,1,$nombre_comercial_m,0,1,'L'); 
    $this->Ln(3);
    $this->Cell(50);
    $this->Cell(10,1,$nombre_m,0,1,'L'); 
    $this->Ln(3);
    $this->Cell(50);
    $this->Multicell(100,3,$referencia_m,0,'L',0);
    $this->Ln(2);
    $this->Cell(50);
    $this->Cell(10,1,$rfc_m." Tel.:".$telefono_m." Correo ".$correo_m."Fecha :".date("d-m-Y H:i:s"),0,1,'L');
   
    
    $this->Ln(4);
    $this->SetFont('Arial','B',6);
    $this->Cell(-3);
    $this->Cell(15,1,"DOCUMENTO",0,0,'L');
    $this->Cell(30);
    $this->Cell(10,1,"NUMERO",0,0,'L');
    $this->Cell(10);
    $this->Cell(10,1,"FECHA",0,0,'L');
    $this->Cell(10);
    $this->Cell(10,1,"VENCIMIENTO",0,0,'L');
    $this->Cell(10);
    $this->Cell(10,1,"FORMA DE PAGO",0,0,'L');
    $this->Cell(15);
    $this->Cell(10,1,"MONEDA",0,0,'L');
    $this->Cell(10);
    $this->Cell(10,1,"VALOR",0,0,'L');
    $this->Cell(10);
    $this->Cell(10,1,"REF",0,0,'L');

    $this->SetFont('Arial','I',6);
    $this->Ln(2);
    $this->Cell(-3);
    //$documento="FACTURA";
    $this->Cell(15,1,$documento,0,0,'L');
    $this->Cell(30);
    //$numero="12345678910";
    $this->Cell(10,1,$numero,0,0,'L');
    $this->Cell(10);
    //$fecha="22/02/2012";
    $this->Cell(10,1,$fecha,0,0,'L');
    $this->Cell(10);
    //$vencimiento="22/02/2012";
    $this->Cell(10,1,$vencimiento,0,0,'L');
    $this->Cell(10);
    //$formadepago="CREDITO";
    $this->Cell(10,1,$forma_de_pago,0,0,'L');
    $this->Cell(15);
    //$moneda="PESO";
    $this->Cell(10,1,$moneda,0,0,'L');
    $this->Cell(10);
    //$valor="1";
    $this->Cell(10,1,$valor_de_moneda,0,0,'L');
    $this->Cell(10);
    $this->Cell(10,1,$referencia,0,1,'L');
    $this->Cell(10);
    //$lugar=$domicilio3_s;
    //$this->Cell(10,1,$lugar,0,1,'L');
    $this->Ln(6);
    $this->SetFont('Arial','B',7);
    $this->Cell(-3);
    $this->Cell(10,1,"DOMICILIO DE RECEPCION",0,0,'L');
    $this->Cell(93);
    $this->Cell(10,1,"DATOS DEL EMISOR",0,0,'L');
    $this->SetFont('Arial','I',7);
    $this->Ln(4);
    $this->Cell(-3);
    $this->Cell(15,1,$nombre_comercial_s,0,0,'L'); 
    $this->Cell(90);
    $this->Cell(15,1,$nombre_comercial_p,0,1,'L'); 
    $this->Ln(3);
    $this->Cell(-3);
    $this->Cell(15,1,$nombre_s,0,0,'L'); 
    $this->Cell(90);
    $this->Cell(15,1,$nombre_p,0,1,'L'); 
    $this->Ln(3);
    $this->Cell(-3);
    $this->Cell(15,1,substr($referencia_s,0,60),0,0,'L');
    $this->Cell(90);
    $this->Cell(15,1,substr($referencia_p,0,60),0,0,'L');
    $this->Ln(3);
    $this->Cell(-3);
    $this->Cell(15,1,substr($referencia_s,60,120),0,0,'L');
    $this->Cell(90);
    $this->Cell(15,1,substr($referencia_p,60,120),0,0,'L');
    $this->Ln(3);
    $this->Cell(-3);
    $this->Cell(15,1,$rfc_s." Tel.:".$telefono_s." Correo : ".$correo_s,0,0,'L');
    $this->Cell(90);
    $this->Cell(15,1,$rfc_p." Tel.:".$telefono_p." Correo : ".$correo_p,0,1,'L');
    //$this->Cell(5);
    //$this->Cell(10,1,"DESCRIPCION DE LA POLIZA ",0,0,'L');
    $this->Ln(6);
    
    
    //$this->Ln(35);
    $this->Cell(1);
    $this->SetFont('Arial','I',7);
    $this->Cell(-4);
    $this->Cell(10,1,"CLAVE",0,0,'L');
    $this->Cell(12);
    $this->Cell(10,1,"MEDIDA",0,0,'L');
    $this->Cell(5);
    $this->Cell(10,1,"DESCRIPCION",0,0,'L');
    $this->Cell(103);
    $this->Cell(10,1,"CANTIDAD",0,0,'L');
    $this->Cell(9);
    $this->Cell(10,1,"COSTO",0,0,'L');
    $this->Cell(7);
    $this->Cell(10,1,"IMPORTE",0,0,'L');
    
    $this->SetFillColor(280,320,320);
    //body de pagina cuadritos
    //$this->RoundedRect(5, 65, 25, 130, 2.5, 'DF'); // col,ren,ancho,alto
    //$this->RoundedRect(30, 65, 13, 130, 2.5, 'DF');
    //$this->RoundedRect(43, 65, 100, 130, 2.5, 'DF');
    //$this->RoundedRect(143, 65, 19, 130, 2.5, 'DF');
    //$this->RoundedRect(162, 65, 23, 130, 2.5, 'DF');
    //$this->RoundedRect(185, 65, 23, 130, 2.5, 'DF');
    $this->Ln(3); //14
    //----------------------------------
    // encabezados
    

    
    $this->SetTextColor(0);

}

// Pie de pÃ¡gina
function Footer()
{
    global $Suma;
    global $Descuento;
    global $Subtotal;
    global $Iva;
    global $Ivaaplicado;
    global $Total;
    global $moneda_id;
    
    $iNumPagina = $this->PageNo();
   
    $this->SetY(-22);
    
    $this->SetFillColor(210,220,280);
 
    $this->SetFillColor(260);
    //cadena original y sello digital
    //$this->RoundedRect(5, 218, 203, 38, 2.5, 'DF');
    $this->RoundedRect(5, 258, 203, 12, 2.5, 'DF');

    
    //$this->Line(5,257, 208,257);
    $this->SetFont('Arial','B',6);
     
   
    $this->Ln(2); //14
    $this->Cell(163);
    $this->Cell(10,1,"SUBTOTAL",0,0,'L');
    $this->Cell(13);
    $this->Cell(10,1,number_format($Subtotal,2),0,1,'R');
    $this->Ln(2); 
    $this->Cell(163);
    $this->Cell(10,1,"IVA ".$Ivaaplicado." % ",0,0,'R');
    $this->Cell(13);
    $this->Cell(10,1,number_format($Iva,2),0,1,'R');

    $Total=redondeo($Total,2); 

    if ($moneda_id==1)
    $Letras=numtoletras($Total,1);  
    else
    $Letras=numtoletras($Total,2);    
    $this->Ln(3);
    $this->Cell(10,1,"LETRA : ".$Letras,0,0,'L');
    $this->Cell(130);
    $this->Cell(10,1,'Page '.$this->PageNo().'/{nb}',0,0,'C');

    $this->Cell(15);
    $this->Cell(10,1,"TOTAL",0,0,'L');
    $this->Cell(10);
    $this->Cell(11,1,number_format($Total,2),0,1,'R');
    
    //$Total=round($Total,2);
    
    $this->Ln(5);
    
    //$this->RoundedRect(87, 264, 39, 10, 2.5, 'DF');
    //$this->RoundedRect(128, 264, 39, 10, 2.5, 'DF');
    //$this->RoundedRect(168, 264, 39, 5, 2.5, 'DF');

    
}

}
 

// CreaciÃ³n del objeto de la clase heredada
$pdf = new PDF();
//abrir base de datos
//$pdf->connect('localhost','root','','hspruebas');

$pdf ->FPDF( 'P', 'mm', 'Letter' );
$conecta =new Conecta();

//consultar datos generales de la matriz
$ident_sal=$_REQUEST[ 'ident_sal'];
$tipo_mov=$_REQUEST[ 'tipo_mov'];
$numero=$ident_sal;
$solicitante=$_SESSION["nombre"];

if (empty($ident_sal) || !isset($solicitante) || !isset($tipo_mov) || empty($tipo_mov) )
{
    echo "datos insuficientes para realizar consulta ... ";
    return;
}    

$folio_id=$ident_sal;

$sql = "SELECT almacen_id as idalmacenes,almacen_origen as idalmacenesd,subtotal,iva,total,ivaaplicado,fecha,vencimiento,tipo_mov_id as idtipo_movs,estatus_id as idestatus,referencia,proveedor_id as idpersonas,entradas.moneda_id,entradas.capturista_id,forma_de_pago,valor_de_moneda FROM entradas WHERE id=$ident_sal";
$rs=($conecta->query($sql));
while ($fila = mysqli_fetch_assoc($rs))
{
    $idalmacenes=$fila["idalmacenes"];
    $idalmacenesd=$fila["idalmacenesd"];
    $Subtotal=$fila["subtotal"];
    $Iva=$fila["iva"];
    $Total=$fila["total"];
    $Ivaaplicado=$fila["ivaaplicado"];
    $fecha=$fila["fecha"];
    $vencimiento=$fila["vencimiento"];
    $idtipo_movs=$fila["idtipo_movs"];
    $idestatus=$fila["idestatus"];
    $referencia=$fila["referencia"];
    $idpersonas=$fila["idpersonas"];
    $moneda_id=$fila["moneda_id"];
    $capturista_id=$fila["capturista_id"];
    $valor_de_moneda=$fila["valor_de_moneda"];
    $forma_de_pago=$fila["forma_de_pago"];
}

$sql="SELECT sucursal_id FROM almacenes where id=$idalmacenes limit 1 ";
$rs=($conecta->query($sql));
while ($fila = mysqli_fetch_assoc($rs)) 
$sucursal_id=$fila["sucursal_id"];

$sql="SELECT descripcion as nombre  FROM monedas where id=$moneda_id limit 1 ";
$rs=($conecta->query($sql));
while ($fila = mysqli_fetch_assoc($rs)) 
$moneda=$fila["nombre"];

$sql="SELECT descripcion as nombre  FROM forma_de_pago where id=$forma_de_pago limit 1 ";
$rs=($conecta->query($sql));
while ($fila = mysqli_fetch_assoc($rs)) 
$forma_de_pago=$fila["nombre"];

// Datos de la matriz

$rs=($conecta->query($sql));
$sql="SELECT matriz_id FROM sucursales LIMIT 1 ";
$rs=($conecta->query($sql));
while ($fila = mysqli_fetch_assoc($rs)) 
$matriz_id=$fila["matriz_id"];


// datos de la matriz
$sqlm = "SELECT
sucursales.id AS idsucursales
, sucursales.descripcion AS nombre
, sucursales.nomComercial AS nombrecomercial
, sucursales.rfc
, sucursales.referencia
, sucursales.telefono
, sucursales.correo
FROM  sucursales
WHERE id = $matriz_id ";
//print_r("consulta datos de la sucursal $sql");
//echo "$sqlm ";
$rs=($conecta->query($sqlm));

while ($fila = mysqli_fetch_assoc($rs)) {
    
    $nombre_m=$fila["nombre"];
    $nombre_comercial_m=$fila["nombrecomercial"];//$fila["nomComercial"];
    $rfc_m=$fila["rfc"];
    $telefono_m=$fila["telefono"];
    $referencia_m=$fila["referencia"];
    $correo_m=$fila["correo"];
}

// datos de la sucursal
$sqls = "SELECT
sucursales.id AS idsucursales
, sucursales.descripcion AS nombre
, sucursales.nomComercial AS nombrecomercial
, sucursales.rfc
, sucursales.referencia
, sucursales.telefono
, sucursales.correo
FROM  sucursales
WHERE id = $sucursal_id ";
//echo "$sqls ";
//print_r("consulta datos de la sucursal $sql");
//return;
$rs=($conecta->query($sqls));

while ($fila = mysqli_fetch_assoc($rs)) {
    
    $nombre_s=$fila["nombre"];
    $nombre_comercial_s=$fila["nombrecomercial"];//$fila["nomComercial"];
    $rfc_s=$fila["rfc"];
    $telefono_s=$fila["telefono"];
    $referencia_s=$fila["referencia"];
    $correo_s=$fila["correo"];
}

// datos del destino

$sqlp = "SELECT descripcion as nombre,nomComercial,telefono,correo,rfc,referencia FROM proveedores WHERE  id='$idpersonas' ";
//echo "$sqlp";
$rs=($conecta->query($sqlp));
while ($fila = mysqli_fetch_assoc($rs))
{
    $nombre_p=$fila["nombre"];    
    $telefono_p=$fila["telefono"];
    $correo_p=$fila["correo"];
    $referencia_p=$fila["referencia"];
    $nombre_comercial_p=$fila["nomComercial"];//$fila["nomComercial"];            
}
$sql = "SELECT descripcion as capturo FROM capturistas WHERE  id='$capturista_id' ";
$rs=($conecta->query($sql));
while ($fila = mysqli_fetch_assoc($rs))
$capturo=$fila["capturo"];

$sql = "SELECT descripcion as nombre FROM tipo_mov WHERE  id='$idtipo_movs' ";
$rs=($conecta->query($sql));
while ($fila = mysqli_fetch_assoc($rs))
$documento=$fila["nombre"];

$sql = "SELECT nombre FROM almacenes WHERE  id='$idalmacenes' ";
$rs=($conecta->query($sql));
while ($fila = mysqli_fetch_assoc($rs))
$origen=$fila["nombre"];

$sql = "SELECT nombre FROM almacenes WHERE  id='$idalmacenesd' ";
$rs=($conecta->query($sql));
while ($fila = mysqli_fetch_assoc($rs))
$destino=$fila["nombre"];

$sql = "SELECT descripcion as nombre FROM estatus WHERE  id='$idestatus' ";
$rs=($conecta->query($sql));
while ($fila = mysqli_fetch_assoc($rs))
$estatus=$fila["nombre"];

 
// hacer consulta a detalles de entradas

$sql="SELECT articulos.id2 as codigo,sat_unidad_medida.clave,articulos.descripcion AS articulo,detalles_entradas.cantidad,detalles_entradas.costo as valor,detalles_entradas.iva  FROM detalles_entradas 
    INNER JOIN articulos ON articulos.id=detalles_entradas.articulo_id
    INNER JOIN sat_unidad_medida ON sat_unidad_medida.id = articulos.sat_unidad_medida_id
    WHERE entrada_id =$ident_sal ";

        


$pdf->AliasNbPages();


$pdf->SetFont('Times','B',8);

$pdf->AddPage(); 

$r=280;$g=320;$b=320;
$r=240;$g=300;$b=320;
$pdf->SetFillColor($r,$g,$b);
$con=0;
$pdf->SetFillColor(280,320,320);
$suma_subt=0;$suma_iva=0;
$cl=0;  
//$pdf->Ln(0);
$bandera=true;

$rs=($conecta->query($sql));
while ($fila = mysqli_fetch_assoc($rs)) 
{
    
    $codigo=$fila["codigo"];
    $articulo=$fila["articulo"];
    $um=$fila["clave"];
    $cantidad=$fila["cantidad"];
    $costo=$fila["valor"];
    $iva=$fila["iva"];
    $pdf->Ln(4);
    $pdf->Cell(-4);
    
    $pdf->Cell(5,4,$codigo,0,0,'L',false);
    $pdf->Cell(20);
    $pdf->Cell(5,4,$um,0,0,'L',false);
    $pdf->Cell(10);

    $Posx=$pdf->GetX();$Posy=$pdf->GetY();
    $pdf->Ln(1);
    $pdf->Cell(37);
    $pdf->Multicell(100,2,$articulo,0,'L',0);
    
    $pdf->SetXY($Posx, $Posy);
    
    //$pdf->Cell(50);
    $importe=$cantidad*$costo;
    $pdf->Cell(118);
    $pdf->Cell(8,4,$cantidad,0,0,'R',false);
    $pdf->Cell(8);
    $pdf->Cell(8,4,number_format($costo,2),0,0,'R',false);
    $pdf->Cell(11);
    $pdf->Cell(8,4,number_format($importe,2),0,0,'R',false);
    
    
}
$pdf->Output();
?>
