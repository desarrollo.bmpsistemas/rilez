<?php
//activamos el almaceneamiento en el buffer
ob_start();
session_start();

if (!isset($_SESSION["nombre"]))  
{
  header("location: login.html");
}
else
{
require 'header.php';
if (isset($_SESSION['capturista_id']) && $_SESSION['capturista_id']>0)
{
?>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content --> 
      <div class="content-wrapper">        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">
                       <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                          <label>Selecione usuario </label> <!--mostrarMovEntSal -->
                          <div class="input-group">
                            <select  id="idusuario"  name="idusuario" required onchange="buscarcodigo()" class="form-control selectpicker"  data-live-search="true" required  > </select>
                            <div class="input-group-btn">
                              <button class="btn btn-default" data-toggle="dropdown"  data-placement="left" title="Procesar moneda " onclick="mostrarform(true)" type="button"> <i class="fa fa-search-plus" aria-hidden="true" ></i>Modulos </button>
                            </div>
                            <div class="input-group-btn">
                              <button class="btn btn-default" data-toggle="dropdown"  data-placement="left" title="Procesar moneda " onclick="asignarAlmacenes()" type="button"> <i class="fa fa-search-plus" aria-hidden="true" ></i>Almacenes </button>
                            </div>
                          </div>
                      </div>
                    </div>
                      

                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoregistros">
                        <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">
                          <thead>
                            <th>Opciones</th>
                            <th>Modulo</th>
                            <th>Usuario</th>
                            <th>Estado</th>
                          </thead>
                          <tbody>                            
                          </tbody>
                          <tfoot>
                            <th>Opciones</th>
                            <th>Modulo</th>
                            <th>Usuario</th>
                            <th>Estado</th>
                          </tfoot>
                        </table>
                    </div>
                    <div class="panel-body" style="height: 400px;" id="formularioregistros">
                        <!--<form name="formulario" id="formulario" method="POST"> -->
                          
                          <div class="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <label>Modulos(*):</label>
                            <input type="hidden" name="idusuarios" id="idusuarios">
                            <input type="hidden" name="idaccesos" id="idaccesos">
                            <select id="idmodulos" name="idmodulos" class="form-control selectpicker" data-live-search="true" required></select>
                          </div>
                         
                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <button class="btn btn-primary" onclick="guardaryeditar()" type="button" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>

                            <button class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
                          </div>
                        <!--</form>-->
                    </div>
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->

          <div class="modal fade" id="myModalAlmacenes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
            <div class="modal-dialog" >
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Asignar Almacenes</h4>
                  </div>
                  <div class="modal-body">
                    
                      <tbody>
                        <form id="myForm">
                        <div class="panel-body"  id="formulariorAlmacenes">
                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>Agregar Almacen </label> <!--mostrarMovEntSal -->
                            <div class="input-group">
                              <select  id="idalmacenesa"  name="idalmacenesa" required  class="form-control selectpicker"  data-live-search="true" required  > </select>
                              <div class="input-group-btn">
                                <button class="btn btn-default" data-toggle="dropdown"  data-placement="left" title="Asignar almacen al usuario actual " onclick="agregarAlmacenAlUsuario()" type="button"> <i class="fa fa-check" aria-hidden="true" ></i>Asignar</button>
                              </div>
                              
                            </div>
                          </div>  
                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>Quitar Almacen</label> <!--mostrarMovEntSal -->
                            <div class="input-group">
                              <select  id="idalmacenesq"  name="idalmacenesq" required  class="form-control selectpicker"  data-live-search="true" required  > </select>
                              
                              <div class="input-group-btn">
                                <button class="btn btn-default" data-toggle="dropdown"  data-placement="left" title="Quintar almacen al usuario actual " onclick="quitarAlmacenAlUsuario()" type="button"> <i class="fa fa-trash" aria-hidden="true" ></i>Quitar </button>
                              </div>
                            </div>
                          </div>  
                            
                        </div>
                        </form>
                          
                      </tbody>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  </div>        
                </div>
            </div>
          </div>


      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
<?php
}
else
{
  require'noacceso.php'; 
}

require 'footer.php';
?>
<script type="text/javascript" src="scripts/accesos.js"></script>
<?php 
  }
  ob_end_flush();
?>