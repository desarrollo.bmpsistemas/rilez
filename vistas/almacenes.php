<?php
//activamos el almaceneamiento en el buffer  
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) 
{
  header("location: login.html");
}
else
{
require 'header.php';
if (isset($_SESSION['capturista_id']) && $_SESSION['capturista_id']>0)
{
?>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">
                          <h1 class="box-title">Almacenes <button class="btn btn-success" id="btnagregar" onclick="mostrarform(true)"><i class="fa fa-plus-circle"></i> Agregar</button></h1>
                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoregistros">
                        <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">
                          <thead>
                            <th>Opciones</th>
                            <th>Sucursal</th>
                            <th>Almacen</th>
                            <th>Moneda</th>
                            <th>Estatus</th>
                            <th>Tope</th>
                            <th>Estado</th>
                          </thead>
                          <tbody>                            
                          </tbody>
                          <tfoot>
                            <th>Opciones</th>
                            <th>Sucursal</th>
                            <th>Almacen</th>
                            <th>Moneda</th>
                            <th>Estatus</th>
                            <th>Tope</th>
                            <th>Estado</th>
                          </tfoot>
                        </table>
                    </div>
                    <div class="panel-body" style="height: 400px;" id="formularioregistros">
                        <form name="formulario" id="formulario" method="POST">
                          <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>Sucursal(*): </label>
                            <select id="idsucursales" name="idsucursales" class="form-control selectpicker" value="idsucursales" data-live-search="true" required onchange="listarestados()" >
                            </select>
                          </div>
                          <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label>Moneda(*): </label>
                            <select id="idmonedas" name="idmonedas" class="form-control selectpicker" value="idmonedas" data-live-search="true" required onchange="buscarMoneda()" >
                            </select>
                          </div>
                          <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label>Estatus(*): </label>
                            <select id="idestatus" name="idestatus" class="form-control selectpicker"  data-live-search="true" required onchange="listarestados()" >
                            </select>
                          </div>
                          <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>Almacen(*):</label>
                            <input type="hidden" name="idalmacenes" id="idalmacenes">
                            <input type="text" class="form-control" name="almacen" id="almacen" maxlength="50" placeholder="Nombre" required>
                          </div>
                          <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label>Tope(*):</label>
                            <input type="number" class="form-control" name="tope" id="tope" maxlength="10" placeholder="Tope" required>
                          </div>
                          
                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>

                            <button class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
                          </div>
                        </form>
                    </div>
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
<?php
}
else
{
  require'noacceso.php'; 
}

require 'footer.php';
?>
<script type="text/javascript" src="scripts/almacenes.js"></script>
<?php 
  }
  ob_end_flush();
?>