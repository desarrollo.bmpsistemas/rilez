<?php
//activamos el almaceneamiento en el buffer  
ob_start();
session_start();

if (!isset($_SESSION["nombre"]))
{
  header("location: login.html");
}
else
{
require 'header.php';
if (isset($_SESSION['capturista_id']) && $_SESSION['capturista_id']>0)
{

?>
<!--<div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-12">
      </button> <a href="../reportes/catalogo.php" target="_blank"><button class="btn btn-info">Reporte</button></a>
</div>
Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <!--<div class="box">
                    <div class="box-header with-border">-->
                    <button type="button" data-toggle="collapse" class="btn btn-primary btn-lg btn-block"  data-target="#consultar"> Consultar Productos</button>
                    <div id="consultar" class="collapse">
                      
                    <div id="divBuscar" name="divBuscar" class="row">  
                      <div  class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label>Lineas:</label>
                            <select id="idlineass" onchange="selectsublinea(1)" name="idlineass" class="form-control selectpicker"  data-live-search="true" required >
                            </select>
                      </div>
                      <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label>SubLineas:</label>
                            <select id="idsublineass" onchange="focusInSublineas(1)" name="idsublineass" class="form-control selectpicker"  data-live-search="true" required >
                            </select>
                      </div>  

                      <div class="form-group col-lg-2 col-md-2 col-sm-4 col-xs-12">
                          <label>Codigo:</label>
                          <input type="hidden" name="session" id="session" value="<?php echo $_SESSION['nombre']; ?> ">
                          <input type="text" onfocusin="selectFocus(1)" class="form-control" name="codigob" id="codigob" maxlength="20" placeholder="Codigo" required>
                      </div>
                      <div class="form-group col-lg-6 col-md-6 col-sm-8 col-xs-12">
                          <label>Descripcion:</label>
                          <input type="text" class="form-control" onfocusin="selectFocus(2)" name="nombreb" id="nombreb" maxlength="100" placeholder="Nombre" required>
                      </div>
                     
                      <div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-6">
                         <h1><button onclick="buscarArticulo()" class="btn btn-primary" id="btnbuscar" name="btnbuscar" >Buscar</button></h1>
                      </div>
                      <!--<div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-6">
                        <h1> <button class="btn btn-success" id="btnagregar" onclick="mostrarform(true)"><i class="fa fa-plus-circle"></i> Agregar</button></h1>
                        <div class="box-tools pull-right">
                        </div>    
                      </div>-->
                      
                      
                   
                      
                    </div>
                    <!-- /.box-header -->
                    <!-- centro id,nombre,codigo,vendible,inventa,campre,cond -->
                    <div class="panel-body table-responsive" id="listadoregistros">
                        <table id="tbllistado" class="display compact nowrap">
                          <thead>
                            <th>Opciones</th>
                            <th>Código</th>
                            <th>Nombre</th>
                            <th>Vendible</th>
                            <th>Invent.</th>
                            <th>CambPrec</th>
                            <th>Estado</th>
                          </thead>
                          <tbody>                            
                          </tbody>
                          <tfoot>
                            <th>Opciones</th>
                            <th>Código</th>
                            <th>Nombre</th>
                            <th>Vendible</th>
                            <th>Invent.</th>
                            <th>CambPrec</th>
                            <th>Estado</th>
                          </tfoot>
                        </table>
                    </div>
                  </div>
            </div>  
          </div>
          <!-- alta de productos -->
          <button type="button" data-toggle="collapse" class="btn btn-primary btn-lg btn-block"  data-target="#productos"> Agregar Productos</button>
          <div id="productos" class="collapse">
                <form name="formulario" id="formulario" method="POST"> 

                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <input type="hidden"  id="idsublineas" name="idsublineas">
                    <label>Codigo(*):</label>
                    <input type="text" class="form-control" name="codigo" id="codigo" maxlength="100" placeholder="Codigo" required>
                  </div>
                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label>Nombre(*):</label>
                    <input type="hidden" name="idarticulos" id="idarticulos">
                    <input type="text" class="form-control" name="nombre" id="nombre" maxlength="100" placeholder="Nombre" required>
                  </div>

                  <div class="form-group col-lg-4 col-md-8 col-sm-12 col-xs-12">
                    <label>Unidad de Medida(*)</label>
                    <select onchange="selectunidad()"  id="idsat_unidad_medidas" name="idsat_unidad_medidas" class="form-control selectpicker" data-live-search="true" required></select>
                  </div>
                  <div class="form-group col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <label>Sat Prod y Serv(*)</label>
                    <select  id="idsat_prod_servs" name="idsat_prod_servs" class="form-control selectpicker" data-live-search="true" required></select>
                  </div>
                  
                  <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                    <label>Producible(*):</label>
                    <select class="form-control select-picker" name="producible" id="producible"  required>
                      <option value="N">N</option>
                      <option value="S">S</option>
                    </select>
                  </div>
                  <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                    <label>Vendible(*):</label>
                    <select class="form-control select-picker" name="vendible" id="vendible"  required>
                      <option value="N">N</option>
                      <option value="S">S</option>
                    </select>
                  </div>
                  <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                    <label>Inventariable(*):</label>
                    <select class="form-control select-picker" name="inventariable" id="inventariable"  required>
                      <option value="N">N</option>
                      <option value="S">S</option>
                    </select>
                  </div>
                  <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                    <label>Cambio Precio(*):</label>
                    <select class="form-control select-picker" name="cambio_precio" id="cambio_precio"  required>
                      <option value="N">N</option>
                      <option value="S">S</option>
                    </select>
                  </div>
                  <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                    <label>Volumen(*):</label>
                    <input type="text" class="form-control" value="0" name="volumen" id="volumen" maxlength="100" placeholder="Volumen" required>
                  </div>
                  <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                    <label>Peso(*):</label>
                    <input type="text" class="form-control" value="0" name="peso" id="peso" maxlength="100" placeholder="Peso" required>
                  </div>

                  <div class="form-group col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <label>Imagen:</label>
                    <input type="file" class="form-control" name="imagen" id="imagen">
                    <input type="hidden" name="imagenactual" id="imagenactual">
                    <img src="" width="150px" height="120px" id="imagenmuestra">
                  </div>
                  
                  <div class="form-group col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <label>Estatus(*):</label>
                    <select id="idestatus" name="idestatus" class="form-control selectpicker" data-live-search="true" required></select>
                  </div>

                  <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-6">
                    <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>
                  </div>
                  

                </form>
          </div>

          

          <!-- alta de articulos por almacen -->
          <button type="button" data-toggle="collapse" class="btn btn-primary btn-lg btn-block"  data-target="#articulosporalmacen"> Agregar articulos al almacen</button>
          <div id="articulosporalmacen" class="collapse">
            <div id="divAlmacenes" class="row">  
                <!--<form name="formularioAlmacen" id="formularioAlmacen" method="POST">  -->
                  <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <input type="hidden" name="idarticulo" id="idarticulo">
                    <input type="hidden" name="idarticulos_almacenes" id="idarticulos_almacenes">
                    <label>Almacenes: </label>
                    <select  id="idalmacenesA" name="idalmacenesA" class="form-control selectpicker"  data-live-search="true" required >
                    </select>
                  </div>
                
                    <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                      <label>Monedas: </label>
                      <select id="idmonedas" name="idmonedas" class="form-control selectpicker" value="idmonedas" data-live-search="true" required >
                      </select>
                    </div>
                     <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                      <label>Afectacion(*):</label>
                      <select class="form-control select-picker" name="afectacion" id="afectacion"  required>
                        <option value="A">Almacen Actual</option>
                        <option value="P">Pesos</option>
                        <option value="D">Dolares</option>
                        <option value="T">Todos</option>
                      </select>
                    </div>

                    <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                      <label>% Iva: </label>
                        <input type="number" class="form-control" name="iva" id="iva" maxlength="4" placeholder="Iva %" required>
                    </div>
                    <div  class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <label>Lineas:</label>
                        <select id="idlineasA" onchange="selectsublinea(3)" name="idlineasA" class="form-control selectpicker"  data-live-search="true" required >
                        </select>
                    </div>
                    <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <label>SubLineas:</label>
                        <select id="idsublineasA"  onchange="selectProductos(6)" name="idsublineasA" class="form-control selectpicker"  data-live-search="true" required >
                        </select>
                    </div>  
                    
                    
                    <div class="form-group col-lg-2 col-md-6 col-sm-12 col-xs-12">
                      <label>Id(*):</label>
                      <input type="text" class="form-control" onfocusin="selectFocus(4)" name="idarticuloA" id="idarticuloA" maxlength="20" placeholder="Codigo" required>
                    </div>
                    <div class="form-group col-lg-3 col-md-6 col-sm-12 col-xs-12">
                      <label>Descripcion:</label>
                      <input type="text" class="form-control" onfocusin="selectFocus(5)" name="articuloA" id="articuloA" maxlength="20" placeholder="Descripcion" required>
                    </div>
                    <div class="form-group col-lg-1 col-md-12 col-sm-12 col-xs-12">
                      <br><button class="btn btn-primary" onclick="selectProductos()"  type="button" id="btnSelectProductos"><i class="fa fa-search-plus" aria-hidden="true"></i>  Buscar</button>
                    </div>
                    <div class="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <label>Productos:</label>
                        <select id="idproductosA" onchange="buscarArticuloEnAlmacen()" name="idprodcutosA" class="form-control selectpicker"  data-live-search="true" required >
                        </select>
                    </div>


                    <div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-12">
                      <label>Costo(*):</label>
                      <input type="number" step="0.01" class="form-control" name="costoA" id="costoA" maxlength="10" placeholder="Costo" required>
                    </div>
                    <div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-12">
                      <label>Por % Utilidad(*):</label>
                      <input type="number" step="0.01" class="form-control" name="porcentaje_utilidad" id="porcentaje_utilidad" maxlength="10" placeholder="% Utilidad" required>
                    </div>


                    <div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-12">
                      <label>Precio(*):</label>
                      <input type="number" step="0.01" class="form-control" name="precio" id="precio" maxlength="10" placeholder="Precio" required>
                    </div>

                    <div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-12">
                      <label>Stock(*):</label>
                      <input type="text" class="form-control" name="stock" id="stock" maxlength="10" placeholder="Stock" required>
                    </div>
                    <div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-12">
                      <label>Ubicacion(*):</label>
                      <input type="text" class="form-control" name="ubicacion" id="ubicacion" maxlength="10" placeholder="Ubicacion" required>
                    </div>

                    <div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-12">
                      <button onclick="grabarArticuloPorAlmacen()" class="btn btn-primary" type="button" id="btnGuardarArtAlm"><i class="fa fa-save"></i> Guardar</button>
                    </div>

                <!--</form>-->
            </div>
          </div>
          

          <!-- alta de articulos por proveedor -->
          <button type="button" data-toggle="collapse" class="btn btn-primary btn-lg btn-block"  data-target="#articulosporproveedor"> Agregar articulos al proveedor</button>
          <div id="articulosporproveedor" class="collapse">
    
              <div id="divProveedores" class="row">  
                 <!-- <form name="formularioProveedores" id="formularioProveedores" method="POST">-->
                    
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      <label>Descripcion:</label>
                      <input type="text" class="form-control" onfocusin="selectFocus(3)" name="persona" id="persona" maxlength="20" placeholder="Proveedor" >
                    </div>
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      <br><button class="btn btn-primary" id="btnBuscarp" onclick="selectProveedores()"> <i class="fa fa-search-plus" aria-hidden="true">Personas</i> </button></h1>
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label>Proveedores:</label>
                        <select id="idproveedoresP"  name="idproveedoresP" class="form-control selectpicker"  data-live-search="true" required >
                        </select>
                    </div>  
                    <div  class="form-group col-lg-2 col-md-6 col-sm-12 col-xs-12">
                      <label>Id(*):</label>
                      <input type="text" class="form-control" onfocusin="selectFocus(2)" name="idarticuloP" id="idarticuloP" maxlength="20" placeholder="Codigo" >
                    </div>
                    <div class="form-group col-lg-5 col-md-6 col-sm-12 col-xs-12">
                      <label>Descripcion:</label>
                      <input type="text" class="form-control" onfocusin="selectFocus(3)" name="articuloP" id="articuloP" maxlength="20" placeholder="Descripcion" >
                    </div>
                    <div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-6">
                      <br><button class="btn btn-primary" id="btnBuscarc" onclick="selectProductos()"> <i class="fa fa-search-plus" aria-hidden="true">Articulos</i> </button></h1>
                    </div>
                    

                    <div class="form-group col-lg-8 col-md-4 col-sm-12 col-xs-12">
                        <label>Productos:</label>
                        <select id="idproductosP" onchange="buscarArticuloPorProveedor(0)" name="idproductosP" class="form-control selectpicker"  data-live-search="true" required >
                        </select>
                    </div>   

                     <div class="form-group col-lg-2 col-md-6 col-sm-12 col-xs-12">
                      <label>Cod Prov(*):</label>
                      <input type="text" class="form-control" name="IdCodProv" id="IdCodProv" maxlength="20" placeholder="Proveedor" required>
                    </div>
                    
                    <!-- style="display: none" -->
                    <div class="form-group col-lg-2 col-md-6 col-sm-12 col-xs-12">
                      <label>Costo(*):</label>
                      <input type="text" class="form-control" name="costo" id="costo" maxlength="10" placeholder="Costo" required>
                    </div>

                   

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <button class="btn btn-primary" onclick="guardarDatosProv()" type="button" id="btnGuardarDP"><i class="fa fa-save"></i> Guardar</button>
                    </div>
                  <!--</form>-->
              </div> 
          </div>
          <!--Fin centro -->
       
          
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
<?php
}
else
{
  require'noacceso.php'; 
}
require 'footer.php';
?>

<script type="text/javascript" src="scripts/articulos.js"></script>
<?php 
  }
  ob_end_flush();
?>