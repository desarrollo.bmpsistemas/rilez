<?php
//activamos el almaceneamiento en el buffer
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) 
{
  header("location: login.html"); 
}
else
{
require 'header.php';
if (isset($_SESSION['almacen']) && $_SESSION['almacen']==1)
{
?>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">    
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">
                      <div class="form-group col-lg-2 col-md-3 col-sm-3 col-xs-4">
                          <label>Codigo:</label>
                          <input type="text" class="form-control" name="codigob" id="codigob" maxlength="20" placeholder="Codigo" required>
                      </div>
                      <div class="form-group col-lg-3 col-md-5 col-sm-6 col-xs-5">
                          <label>Descripcion:</label>
                          <input type="text" class="form-control" name="nombreb" id="nombreb" maxlength="100" placeholder="Nombre" required>
                      </div>
                      <div class="form-group col-lg-1 col-md-4 col-sm-3 col-xs-3">
                         <h1><button onclick="buscarArticulo()" class="btn btn-primary" id="btnbuscar" name="btnbuscar" >Buscar</button></h1>
                      </div>

                      <div  class="form-group col-lg-4 col-md-8 col-sm-10 col-xs-9">
                            <label>Selecione Producto  </label>
                            <select  id="idarticulos" name="idarticulos" class="form-control selectpicker" value="idarticulos" data-live-search="true" required onchange="buscarcodigo()" ></select>
                      </div>   

                      <div class="form-group col-lg-2 col-md-4 col-sm-2 col-xs-3">
                             <h1><button class="btn btn-success" id="btnagregar" onclick="mostrarform(true)" data-toggle="tooltip" title="Agregar Producto a Otro Almacen"><i class="fa fa-plus-circle"></i> Nuevo</a></button>  </h1>
                        <div class="box-tools pull-right">
                        </div>

                      </div>   
                    </div>
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoregistros">
                        <table id="tbllistado"  class="display compact nowrap">
                          <thead>
                            
                            <th>Opciones</th>
                            <th>Almacen</th>
                            <th>Moneda</th>
                            <th>Costo</th>
                            <th>Utilidad</th>
                            <th>Precio</th>
                            <th>Estado</th>
                          </thead>
                          <tbody>                            
                          </tbody>
                          <tfoot>
                            <th>Opciones</th>
                            <th>Almacen</th>
                            <th>Moneda</th>
                            <th>Costo</th>
                            <th>Utilidad</th>
                            <th>Precio</th>
                            <th>Estado</th>
                          </tfoot>
                        </table>
                    </div>
                    <div class="panel-body" style="height: 400px;" id="formularioregistros">
                        <form name="formulario" id="formulario" method="POST">
                           <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <input type="hidden" name="idarticulo" id="idarticulo">
                            <input type="hidden" name="idarticulos_almacenes" id="idarticulos_almacenes">
                            <label>Almacenes: </label>
                            <select onchange="leerIva_y_Moneda()" id="idalmacenes" name="idalmacenes" class="form-control selectpicker" value="idalmacenes" data-live-search="true" required >
                            </select>
                          </div>
                          
                          <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <label>Monedas: </label>
                            <select id="idmonedas" name="idmonedas" class="form-control selectpicker" value="idmonedas" data-live-search="true" required >
                            </select>
                          </div>
                           <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <label>Afectacion(*):</label>
                            <select class="form-control select-picker" name="afectacion" id="afectacion"  required>
                              <option value="A">Almacen Actual</option>
                              <option value="P">Pesos</option>
                              <option value="D">Dolares</option>
                              <option value="T">Todos</option>
                            </select>
                          </div>

                          <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <label>Ivas: </label>
                            <select id="idivas" name="idivas" class="form-control selectpicker" value="idivas" data-live-search="true" required >
                            </select>
                          </div>

                          <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label>Costo(*):</label>
                            <input type="number" step="0.01" class="form-control" name="costo" id="costo" maxlength="10" placeholder="Costo" required>
                          </div>
                          <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label>Por % Utilidad(*):</label>
                            <input type="number" step="0.01" class="form-control" name="utilidad" id="utilidad" maxlength="10" placeholder="% Utilidad" required>
                          </div>


                          <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label>Precio(*):</label>
                            <input type="number" step="0.01" class="form-control" name="precio" id="precio" maxlength="10" placeholder="Precio" required>
                          </div>

                          <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label>Stock(*):</label>
                            <input type="text" class="form-control" name="stock" id="stock" maxlength="10" placeholder="Stock" required>
                          </div>
                          <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label>Ubicacion(*):</label>
                            <input type="text" class="form-control" name="ubicacion" id="ubicacion" maxlength="10" placeholder="Ubicacion" required>
                          </div>

                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>

                            <button class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
                          </div>
                        </form>
                    </div>
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
<?php
}
else
{
  require'noacceso.php'; 
}

require 'footer.php';
?>
<script type="text/javascript" src="scripts/articulos_almacenes.js"></script>
<?php 
  }
  ob_end_flush();
?>