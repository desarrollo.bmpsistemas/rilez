<?php
//activamos el almaceneamiento en el buffer 
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) 
{
  header("location: login.html");
}
else
{
require 'header.php';
if (isset($_SESSION['capturista_id']) && $_SESSION['capturista_id']>0)
{

?>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">
                       <form name="formulario" id="formulario" method="POST">
                        <div class="form-group col-lg-3 col-md-4 col-sm-12 col-xs-12">
                            <label>Grupo de gastos </label> 
                            <select  id="idgrupo_de_gastos" onchange="buscar(0)" name="idgrupo_de_gastos" class="form-control selectpicker"  data-live-search="true" required  >
                            </select>
                        </div> 

						            <div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-12">
                          
                          <label>Id:</label>
                          <input type="number" value="0" class="form-control" name="idcat_de_gastos" id="idcat_de_gastos" maxlength="10" placeholder="Id" required>
                      	</div>

                      	
                        
                        <div class="form-group col-lg-7 col-md-6 col-sm-9 col-xs-12">
                          <label>Gastos:</label>
                          <div class="input-group">
                            <input type="text" class="form-control" name="nombre" id="nombre" maxlength="30" placeholder="Descripcion" required>
                            <div class="input-group-btn">
                              <button class="btn btn-default" data-toggle="dropdown"  data-placement="left" title="Buscar catalogo " onclick="buscar(1)" type="button"> <i class="fa fa-search-plus" aria-hidden="true" ></i> </button>
                            </div>
                            <div class="input-group-btn">
                              <button class="btn btn-default" data-toggle="dropdown"  data-placement="left" title="Grabar un gasto nuevo " onclick="guardar(0)" type="button"> <i class="fa fa-save" aria-hidden="true" ></i> </button>
                            </div>
                            <div class="input-group-btn">
                              <button class="btn btn-default" data-toggle="dropdown"  data-placement="left" title="Actualizar gasto " onclick="guardar(1)" type="button"> <i class="fa fa-check" aria-hidden="true" ></i> </button>
                            </div>
                          </div>
                        </div>


                        
                     </form>   
                      
                    </div>
                 
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoregistros">
                        <table id="tbllistado" class="display compact nowrap">
                          <thead>
                            <th>Opciones</th>
                            <th>Id</th>
                            <th>Gasto</th>
                            <th>Grupo</th>
                            <th>Estado</th>
                          </thead>
                          <tbody>                            
                          </tbody>
                          <tfoot>
                            <th>Opciones</th>
                            <th>Id</th>
                            <th>Gasto</th>
                            <th>Grupo</th>
                            <th>Estado</th>
                          </tfoot>
                        </table>
                    </div>
                    
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
<?php
}
else
{
  require'noacceso.php'; 
}

require 'footer.php';
?>
<script type="text/javascript" src="scripts/cat_de_gastos.js"></script>
<?php 
  }
  ob_end_flush();
?>