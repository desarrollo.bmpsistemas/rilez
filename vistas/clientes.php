<?php
error_reporting(E_ALL);
ini_set('display_errors', '1'); 
ob_start();
session_start();

if (!isset($_SESSION["nombre"]))
{
  header("location: login.html");
}
else
{
require_once ('header.php');
if (isset($_SESSION['capturista_id']) && $_SESSION['capturista_id']>0)
{

?>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">
                      <div   class="form-check form-group col-lg-4 col-md-8 col-sm-8 col-xs-8">
                         <label>Busqueda de Persona </label> <!--mostrarMovEntSal -->
                          
                          <div class="input-group">
                            <div class="input-group-btn"> 
                              <button onclick="MostrarModalOpcionPersona()" class="btn btn-default" data-toggle="dropdown"  data-placement="left" title="Selecione tipo de busqueda"  type="radio"> <i class="fa fa-question-circle" aria-hidden="true"></i></button>
                            </div>
                           
                            <input type="text" class="form-control" placeholder="Search"  name="busqueda" id="busqueda" maxlength="100" placeholder="id-rfc-nombre-tel" required>
                            <div class="input-group-btn">
                              <button class="btn btn-default" onclick="listar()" type="button"> <i class="fa fa-search-plus" aria-hidden="true"></i></button>
                            </div>
                             <div class="input-group-btn">
                              <button class="btn btn-default" onclick="mostrarform(true)" type="button"> <i class="fa fa-pencil" aria-hidden="true"></i></button>
                            </div>
                          
                      </div>

                      
                       
                     
                    </div>
                    </div>
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoregistros">
                        <table id="tbllistado" class="display compact nowrap">
                          <thead>
                            <th>Opciones</th>
                            <th>Cliente</th>
                            <th>NombreComercial</th>
                            <th>Rfc</th>
                            <th>Teléfono</th>
                            <th>Estado</th>
                          </thead>
                          <tbody>                            
                          </tbody>
                          <tfoot>
                            <th>Opciones</th>
                            <th>Cliente</th>
                            <th>NombreComercial</th>
                            <th>Rfc</th>
                            <th>Teléfono</th>
                            <th>Estado</th>
                          </tfoot>
                        </table>
                    </div>
                    <div class="panel-body" style="height: 400px;" id="formularioregistros">
                        <form name="formulario" id="formulario" method="POST">
                          
                          <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label>id:</label>

                            <input type="text" class="form-control" readonly name="idpersonas" id="idpersonas" maxlength="10" placeholder="Id" required>
                          </div>

                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Nombre:</label>
                            <input type="text" class="form-control" name="nombre" id="nombre" maxlength="50" placeholder="Nombre" required>
                          </div>
                          <div class="form-group col-lg-4 col-md-12 col-sm-12 col-xs-12">
                            <label>Nombre Comercial:</label>
                            <input type="text" class="form-control" name="nombrecomercial" id="nombrecomercial" maxlength="100" placeholder="Nombre Comercial"  required>
                          </div>

                          

                          <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>RFC:</label>
                            <input type="text" class="form-control" name="rfc" id="rfc" maxlength="20" placeholder="Rfc"  required>
                          </div>
                          
                          <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>Teléfono:</label>
                            <input type="text" class="form-control" name="telefono" id="telefono" maxlength="20" placeholder="Teléfono"  required>
                          </div>

                          <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>Correo:</label>
                            <input type="email" class="form-control" name="correo" id="correo" maxlength="50" placeholder="Correo"  required>
                          </div>

                           <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>Vendedores(*):</label>
                            <select id="idvendedores" name="idvendedores" class="form-control selectpicker" data-live-search="true" required></select>
                          </div>

                          <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label>Dias Credito:</label>
                            <input type="text" class="form-control" name="diascredito" id="diascredito" maxlength="50" placeholder="Dias de Credito">
                          </div>
                          <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label>Limite Credito:</label>
                            <input type="text" class="form-control" name="limitecredito" id="limitecredito" maxlength="50" placeholder="Limite de Credito"  required>
                          </div>
                          <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label>Descto Aut:</label>
                            <input type="number" step="0.01" onfocusout="checarDescuento()" class="form-control" step="0.00" name="descuento" id="descuento" maxlength="4" placeholder="Descuento Aut."  required>
                          </div>

                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>Referencia / Domicilio .:</label>
                            <textarea class="form-control rounded-0" id="referencia" name="referencia" rows="3"  required></textarea>
                          </div>
                          


                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>

                            <button class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
                          </div>
                        </form>
                    </div>
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
    

  

  <div class="modal fade" id="myModalBusquedaPersona" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
      <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Selecionar tipo de busqueda de persona </h4>
            </div>
            <div class="modal-body">
              
                <tbody>
                    <div class="panel-body" id="formularioPer" >
                      <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <input type="radio" onchange="checarRadioButton(0)" class="form-check-input"  name="rbpersonas" value="0" >
                          <label class="form-check-label" for="materialchecked">Buscar por Id</label>  
                      </div>  
                      <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <input type="radio" onchange="checarRadioButton(1)" class="form-check-input" name="rbpersonas" value="1" >
                          <label class="form-check-label" for="materialchecked">Buscar por Rfc</label>
                      </div>      
                      <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <input type="radio" onchange="checarRadioButton(2)" class="form-check-input" name="rbpersonas"  value="2">
                          <label class="form-check-label" for="materialchecked">Buscar por Nombre</label>
                      </div>
                      <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">  
                          <input type="radio" onchange="checarRadioButton(3)" class="form-check-input" name="rbpersonas"  value="3" >
                          <label class="form-check-label" for="materialchecked">Buscar por Telefono</label> 
                      </div>
                  </div>
                </tbody>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>        
          </div>
      </div>
    </div>

  <!--Fin-Contenido-->
<?php
}
else
{
  require 'noacceso.php';
}
require 'footer.php';
?>
<script type="text/javascript" src="scripts/clientes.js"></script>
<?php 
}
ob_end_flush();
?>