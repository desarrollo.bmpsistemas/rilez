<?php
//activamos el almaceneamiento en el buffer
ob_start();
session_start();

if (!isset($_SESSION["nombre"]))
{
  header("location: login.html");
}
else
{
require 'header.php';
if (isset($_SESSION['almacen']) && $_SESSION['almacen']==1)
{
?>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              	<div class="col-md-12">
                  	<div class="box">
	                    <div class="box-header with-border">
	                          <h1 class="box-title">Comisiones <button class="btn btn-success" id="btnagregar" onclick="mostrarform(true)"><i class="fa fa-plus-circle"></i> Agregar</button></h1>

	                            <a data-toggle="modal" href="#myModal">           
	                              <button onclick="llenarVendedores()" id="btnAgregarCom" type="button" class="btn btn-primary"> <span class="fa fa-plus"></span> Agregar Datos </button>
	                            </a>
	                          
	                        <div class="box-tools pull-right">
	                        </div>
	                    </div>
	                    <!-- /.box-header -->
	                    <!-- centro -->
	                    <div class="panel-body table-responsive" id="listadoregistros">
	                        <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">
	                          <thead>
	                            <th>Opciones</th>
	                            <th>Vendedor</th>
	                            <th>Lim Inf</th>
	                            <th>Lim Sup</th>
	                            <th>Por% Vend</th>
	                            <th>Por% Ger.</th>
	                          </thead>
	                          <tbody>                            
	                          </tbody>
	                          <tfoot>
	                            <th>Opciones</th>
	                            <th>Vendedor</th>
	                            <th>Lim Inf</th>
	                            <th>Lim Sup</th>
	                            <th>Por% Vend</th>
	                            <th>Por% Ger.</th>
	                          </tfoot>
	                        </table>
	                    </div>
	                    <div class="panel-body" style="height: 400px;" id="formularioregistros">
	                        <form name="formulario" id="formulario" method="POST">
	                          
	                          <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
	                            <label>Vendedores: </label>
	                            <input type="hidden" name="idcomisiones" id="idcomisiones">
	                            <select id="idvendedores" name="idvendedores" class="form-control selectpicker" value="idvendedores" data-live-search="true" required >
	                            </select>
	                          </div>
	                          <div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-12">
	                            <label>Lim Inf(*):</label>
	                            <input type="number" class="form-control" name="lim_inf" id="lim_inf" maxlength="10" placeholder="Limite Inferior" required>
	                          </div>
	                          <div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-12">
	                            <label>Lim Sup(*):</label>
	                            <input type="number" class="form-control" name="lim_sup" id="lim_sup" maxlength="10" placeholder="Limite Superior" required>
	                          </div>
	                          <div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-12">
	                            <label>Por% Ven(*):</label>
	                            <input type="number" class="form-control" name="por_vendedor" id="por_vendedor" maxlength="10" placeholder="Porciento Vendedor" required>
	                          </div>
	                          <div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-12">
	                            <label>Por% Ger(*):</label>
	                            <input type="number" class="form-control" name="por_gerente" id="por_gerente" maxlength="10" placeholder="Poriento Gerente" required>
	                          </div>

	                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>

	                            <button class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
	                          </div>
	                        </form>
	                    </div>
                    
					</div>

              	</div><!-- /.col -->
          	</div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
	  <!--Fin-Contenido-->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	    <div class="modal-dialog" style="width: 65% !important;">
	      	<div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		          <h4 class="modal-title">Seleccione Valores de Comisiones</h4>
		        </div>
		        <div class="modal-body">
		          
		            <tbody>
		              	<div class="panel-body" style="height: 400px;" id="formularioregistross">
			                
			                  <a data-controls-modal="myModal" data-backdrop="static" data-keyboard="false" href="#">
			                  <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
			                    <label>Vendedores: </label>
			                    <input type="hidden" name="idcomisioness" id="idcomisioness">
			                    <select id="idvendedoress" name="idvendedoress" class="form-control selectpicker" value="idvendedoress" data-live-search="true" required >
			                    </select>
			                  </div>
			                  <div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-12">
			                    <label>Lim Inf(*):</label>
			                    <input type="number" class="form-control" name="lim_infs" id="lim_infs" maxlength="10" placeholder="Limite Inferior" required>
			                  </div>
			                  <div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-12">
			                    <label>Lim Sup(*):</label>
			                    <input type="number" class="form-control" name="lim_sups" id="lim_sups" maxlength="10" placeholder="Limite Superior" required>
			                  </div>
			                  <div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-12">
			                    <label>Por% Ven(*):</label>
			                    <input type="number" class="form-control" name="por_vendedors" id="por_vendedors" maxlength="10" placeholder="Porciento Vendedor" required>
			                  </div>
			                  <div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-12">
			                    <label>Por% Ger(*):</label>
			                    <input type="number" class="form-control" name="por_gerentes" id="por_gerentes" maxlength="10" placeholder="Poriento Gerente" required>
			                  </div>

			                  <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			                    <button  class="btn btn-primary" onclick="agregarComision()" id="btnGuardarComision"><i class="fa fa-save"></i> Guardar</button>
			                  </div>
			                
			            </div>
		            </tbody>
		        </div>
		        <div class="modal-footer">
		          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		        </div>        
	      	</div>
	    </div>
	</div> 
<?php
}
else
{
  require'noacceso.php'; 
}

require 'footer.php';
?>
<script type="text/javascript" src="scripts/comisiones.js"></script>
<?php 
  }
  ob_end_flush();
?>