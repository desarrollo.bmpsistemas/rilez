<?php
//activamos el almaceneamiento en el buffer
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) 
{
  header("location: login.html");
}
else
{
require 'header.php';
if (isset($_SESSION['almacen']) && $_SESSION['almacen']==1)
{
?>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">

                    <div id="divCaptura1" class="row">
                      <div class="form-group col-lg-3 col-md-6 col-sm-12 col-xs-12">
                          <label>Clave:</label>
                          <input type="text" class="form-control" onfocusin="establecerFocus(1)" name="codigo" id="codigo" maxlength="20" placeholder="Codigo" required>
                      </div>

                      <div class="form-group col-lg-3 col-md-6 col-sm-12 col-xs-12">
                          <label>Descripcion:</label>
                          <input type="text" class="form-control"  onfocusin="establecerFocus(2)" name="nombre" id="nombre" maxlength="20" placeholder="Descripcion" required>
                      </div>
                      <div class="form-group col-lg-1 col-md-2 col-sm-6 col-xs-6">
                          <label>Buscar:</label>
                          <button class="btn btn-primary" id="btnBuscarc" onclick="buscarArticulo(1)"> <i class="fa fa-search-plus" aria-hidden="true"></i></button>
                      </div>

                       <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                          <label>Selecione Matriz  </label>
                          <select  id="idmatrices" name="idmatrices" class="form-control selectpicker"  data-live-search="true"  onchange="listarDetallesDeMatriz()" >
                          </select>
                      </div>   
                      <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                          <h1><button onclick="mostrarform(true)" class="btn btn-primary" id="btnbuscar" name="btnbuscar" >Agregar</button></h1>
                      </div>
                    </div>  
                    
                    </div>
                  </div>
                </div> 
              </div>   
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoregistros">
                        <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">
                          <thead>
                            <th>Opciones</th>
                            <th>Codigo</th>
                            <th>Producto</th>
                            <th>Costo</th>
                            <th>Cantidad</th>
                            <th>Estado</th>
                          </thead>
                          <tbody>                            
                          </tbody>
                          <tfoot>
                            <th>Opciones</th>
                            <th>Codigo</th>
                            <th>Producto</th>
                            <th>Costo</th>
                            <th>Cantidad</th>
                            <th>Estado</th>
                          </tfoot>
                        </table>
                    </div>
                    <div class="panel-body" style="height: 400px;" id="formularioregistros">
                    <!-- <form name="formulario" id="formulario" method="POST"> -->

                    <div id="divCaptura2" class="row">      
                      <div class="form-group col-lg-3 col-md-6 col-sm-12 col-xs-12">
                          <label>Clave:</label>
                          <input type="text" class="form-control" onfocusin="establecerFocus(3)" name="codigo2" id="codigo2" maxlength="20" placeholder="Codigo" required>
                      </div>
                      <div class="form-group col-lg-5 col-md-6 col-sm-12 col-xs-12">
                          <label>Descripcion:</label>
                          <input type="hidden"  id="iddetalles_matrices" name="iddetalles_matrices">
                          <input type="text" class="form-control" onfocusin="establecerFocus(4)" name="nombre2" id="nombre2" maxlength="20" placeholder="Descripcion" required>
                      </div>
                      <div class="form-group col-lg-1 col-md-2 col-sm-6 col-xs-6">
                          <label>Buscar:</label>
                          <button class="btn btn-primary" id="btnBuscarA" onclick="buscarArticulo(2)"> <i class="fa fa-search-plus" aria-hidden="true"></i></button>
                      </div>

                       <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                          <label>Selecione Producto  </label>
                          <select  id="idarticulos" name="idarticulos" class="form-control selectpicker" value="idmatrices" data-live-search="true"  onchange="listarDetallesDeMatriz()" >
                          </select>
                      </div> 

                      <div class="form-group col-lg-3 col-md-6 col-sm-12 col-xs-12">
                        <label>Costo(*):</label>
                        <input type="number" step="0.01" class="form-control" name="costo" id="costo" maxlength="10" placeholder="Costo" required>
                      </div>

                      <div class="form-group col-lg-3 col-md-6 col-sm-12 col-xs-12">
                        <label>Cantidad(*):</label>
                        <input type="number" step="0.01" class="form-control" name="cantidad" id="cantidad" maxlength="10" placeholder="Cantidad" required>
                      </div>

                      <div class="form-group col-lg-3 col-md-2 col-sm-6 col-xs-6">
                          
                          <h1><button class="btn btn-primary" onclick="guardarDetalles(0)" type="button" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button></h1>
                      </div>

                      <div class="form-group col-lg-3 col-md-12 col-sm-12 col-xs-12">
                        
                        <h1><button class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button></h1>
                      </div>
                    <div>        
                        <!-- </form> -->
                    </div>
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
<?php
}
else
{
  require'noacceso.php'; 
}

require 'footer.php';
?>
<script type="text/javascript" src="scripts/detalles_matrices.js"></script>
<?php 
  }
  ob_end_flush();
?>