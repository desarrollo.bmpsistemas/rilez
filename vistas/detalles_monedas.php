<?php
//activamos el almaceneamiento en el buffer
ob_start();
session_start();

if (!isset($_SESSION["nombre"]))
{
  header("location: login.html");
}
else
{
require 'header.php';
if (isset($_SESSION['almacen']) && $_SESSION['almacen']==1)
{
?>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">
                          <h1 class="box-title">Valor del Dolar <button class="btn btn-success" id="btnagregar" onclick="mostrarform(true)">
                            <i class="fa fa-plus-circle"></i> Agregar Valor</button>
                          </h1>

                           <h1 class="box-title">Fecha <input type="date" name="fecha"  id="fecha" onchange="buscarporfecha()">
                            
                          </h1>
                        
                    </div>

                      
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoregistros">
                        <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">
                          <thead>
                            <th>Opciones</th>
                            <th>Usuario</th>
                            <th>Fecha</th>
                            <th>Valor</th>
                          </thead>

                          <tbody>                            
                          </tbody>

                          <tfoot>
                            <th>Opciones</th>
                            <th>Usuario</th>
                            <th>Fecha</th>
                            <th>Valor</th>
                          </tfoot>
                        </table>
                    </div>
                    <div class="panel-body" id="formularioregistros">
                        <form name="formulario" id="formulario" method="POST">
                          
                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Usuarios(*):</label>
                            <select id="idusuarios" name="idusuarios" class="form-control selectpicker" data-live-search="true" required></select>
                          </div>
                          
                           <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                             <label>Fecha(*)</label>
                             <input type="date" class="form-control" name="fecha" id="fecha" value="<?php echo date("Y-m-d"); ?>">
                          </div>
                         

                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Tipo de Cambio(*):</label>
                            <input type="number"  step="0.01" class="form-control" name="valor" id="valor" maxlength="10" placeholder="Tipo de Cambio" required>
                          </div>

                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>

                            <button class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
                          </div>
                        </form>
                    </div>
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
<?php
}
else
{
  require'noacceso.php'; 
}
require 'footer.php';
?>

<script type="text/javascript" src="scripts/detalles_monedas.js"></script>
<?php 
  }
  ob_end_flush();
?>