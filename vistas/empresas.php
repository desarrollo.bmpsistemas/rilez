  <?php
//Activamos el almacenamiento en el buffer
ob_start();
session_start();

if (!isset($_SESSION["nombre"]))
{
  header("Location: login.html");
}
else
{
require 'header.php';
if ($_SESSION['ventas']==1)
{
?>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">
                          <h1 class="box-title">Clientes <button class="btn btn-success" id="btnagregar" onclick="mostrarform(true)"><i class="fa fa-plus-circle"></i> Agregar</button></h1>
                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoregistros">
                        <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">
                          <thead>
                            <th>Opciones</th>
                            <th>Nombre</th>
                            <th>NombreComercial</th>
                            <th>Rfc</th>
                            <th>Teléfono</th>
                            <th>Estado</th>
                          </thead>
                          <tbody>                            
                          </tbody>
                          <tfoot>
                            <th>Opciones</th>
                            <th>Nombre</th>
                            <th>NombreComercial</th>
                            <th>Rfc</th>
                            <th>Teléfono</th>
                            <th>Estado</th>
                          </tfoot>
                        </table>
                    </div>
                    <div class="panel-body" style="height: 400px;" id="formularioregistros">
                        <form name="formulario" id="formulario" method="POST">
                         
                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Nombre:</label>
                            <input type="hidden" name="idpersonas" id="idpersonas">
                            <input type="hidden" name="tipo_persona" id="tipo_persona" value="Cliente">
                            <input type="text" class="form-control" name="nombre" id="nombre" maxlength="50" placeholder="Nombre del Cliente" required>
                          </div>

                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Nombre Comercial:</label>
                            <input type="text" class="form-control" name="nombrecomercial" id="nombrecomercial" maxlength="100" placeholder="Nombre Comercial">
                          </div>

                          <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>Tipo Persona:</label>
                            <select class="form-control select-picker" name="tipodepersona" id="tipodepersona" required>
                              <option value="Proveedor">Proveedor</option>
                              <option value="Cliente">Cliente</option>
                            </select>
                          </div>

                          <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>RFC:</label>
                            <input type="text" class="form-control" name="rfc" id="rfc" maxlength="20" placeholder="Rfc">
                          </div>
                          
                          <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>Teléfono:</label>
                            <input type="text" class="form-control" name="telefono" id="telefono" maxlength="20" placeholder="Teléfono">
                          </div>

                          <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>Correo:</label>
                            <input type="email" class="form-control" name="correo" id="correo" maxlength="50" placeholder="Correo">
                          </div>

                          <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>Municipio(*):</label>
                            <select id="idmunicipios" name="idmunicipios" class="form-control selectpicker" data-live-search="true" required></select>
                          </div>
                         
                          <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>Colonias(*):</label>
                            <select id="idcolonias" name="idcolonias" class="form-control selectpicker" data-live-search="true" required></select>
                          </div>

                          <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>Calles(*):</label>
                            <select id="idcalles" name="idcalles" class="form-control selectpicker" data-live-search="true" required></select>
                          </div>

                           <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>Vendedores(*):</label>
                            <select id="idvendedores" name="idvendedores" class="form-control selectpicker" data-live-search="true" required></select>
                          </div>

                          <div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-12">
                            <label>Numero Ext:</label>
                            <input type="text" class="form-control" name="numero_exterior" id="numero_exterior" maxlength="50" placeholder="Numero Exterior">
                          </div>
                          <div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-12">
                            <label>Numero Int:</label>
                            <input type="text" class="form-control" name="numero_interior" id="numero_interior" maxlength="50" placeholder="Numero Interior">
                          </div>
                          <div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-12">
                            <label>Cod. Postal:</label>
                            <input type="text" class="form-control" name="codigo_postal" id="codigo_postal" maxlength="50" placeholder="Codigo Postal">
                          </div>
                          <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label>Dias Credito:</label>
                            <input type="text" class="form-control" name="diascredito" id="diascredito" maxlength="50" placeholder="Dias de Credito">
                          </div>
                          <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label>Limite Credito:</label>
                            <input type="text" class="form-control" name="limitecredito" id="limitecredito" maxlength="50" placeholder="Limite de Credito">
                          </div>


                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>Referencia.:</label>
                            <input type="text" class="form-control" name="referencia" id="referencia" maxlength="50" placeholder="Referencia">
                          </div>
                          


                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>

                            <button class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
                          </div>
                        </form>
                    </div>
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
<?php
}
else
{
  require 'noacceso.php';
}
require 'footer.php';
?>
<script type="text/javascript" src="scripts/clientes.js"></script>
<?php 
}
ob_end_flush();
?>