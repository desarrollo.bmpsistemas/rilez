<?php
//activamos el almaceneamiento en el buffer
ob_start();
session_start(); 

if (!isset($_SESSION["nombre"]))  
{
  header("location: login.html");
}
else
{
require 'header.php';
if (isset($_SESSION['capturista_id']) && $_SESSION['capturista_id']>0)
{

?>

<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="box-header with-border">
                            <h1 class="box-title">File Xml <button class="btn btn-success" id="btnagregar" onclick="mostrarform(true)"><i class="fa fa-plus-circle"></i> Agregar</button> </h1>
                      </div>    
                    </div>

                    
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoregistros">
                        <table id="tbllistado" class="display compact nowrap">
                          <thead>
                            <th>Opciones</th>
                            <th>Id</th>
                            <th>ClaveU</th>
                            <th>CvePS</th>
                            <th>Unidad</th>
                            <th>Descripcion</th>
                            <th>Valor</th>
                            <th>Estado</th>
                          </thead>
                          <tbody>                            
                          </tbody>
                          <tfoot>
                            <th>Opciones</th>
                            <th>Id</th>
                            <th>ClaveU</th>
                            <th>CvePS</th>
                            <th>Unidad</th>
                            <th>Descripcion</th>
                            <th>Valor</th>
                            <th>Estado</th>
                          </tfoot>
                        </table>
                    </div>
                    
                    <div class="panel-body" style="height: 600px;" id="formularioregistros">
                        <form name="formulario" id="formulario" method="POST">
                          <div class="form-group col-lg-6 col-md-4 col-sm-12 col-xs-12">
                            <label>Archivo Xml:</label>
                            <input type="file" class="form-control" name="imagen" id="imagen">
                          </div>
                          <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <label>Almacenes(*):</label>
                            <select id="idalmacenes" name="idalmacenes" class="form-control selectpicker" data-live-search="true" required></select>
                          </div>

                          <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <label>Afectacion(*):</label>
                            <select class="form-control select-picker" name="afectacion" id="afectacion"  required>
                              <option value="A">Almacen Actual</option>
                              <option value="P">Por Moneda Pesos</option>
                              <option value="D">Por Moneda Dolares</option>
                              <option value="T">Todos</option>
                            </select>
                          </div>
                          
                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Extraer</button>
                            <button class="btn btn-success" id="btnagregarArticulo" onclick="migrarArticulos()"><i class="fa fa-plus-circle"></i> Articulos
                            </button>
                            <button class="btn btn-success" id="btnagregarPrecios" onclick="migrarPrecios()"><i class="fa fa-plus-circle"></i> Precios
                            </button>

                            <button class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Regresar</button>
                          </div>
                        </form>
                    </div>
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
<?php
}
else
{
  require'noacceso.php'; 
}
require 'footer.php';
?>

<script type="text/javascript" src="scripts/filexml.js"></script>
<?php 
  }
  ob_end_flush();
?>