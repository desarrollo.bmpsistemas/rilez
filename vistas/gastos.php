<?php
//activamos el almaceneamiento en el buffer
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) 
{
  header("location: login.html");
}
else
{
require 'header.php';
if (isset($_SESSION['capturista_id']) && $_SESSION['capturista_id']>0)
{

?>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      
      <div class="content-wrapper">        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">
                      <div class="box-header with-border">
                        <form name="formulario" id="formulario" method="POST">
                          
                          
                        <div  class="row">  
                          <div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-12">
                            <input type="hidden" name="idusuarios" id="idusuarios" value="<?php echo $_SESSION['capturista_id']; ?> ">
                            <input type="hidden" name="Sidalmacenes" id="Sidalmacenes" value="<?php echo $_SESSION['almacen_id']; ?> ">
                            <input type="hidden" name="idgastos" id="idgastos" value="0" required>
                            <label>Id:</label>
                            <div class="input-group">
                              
                              <input type="text" class="form-control"  name="id" id="id" maxlength="10" placeholder="Id">
                              <div class="input-group-btn">
                                <button class="btn btn-default" data-toggle="dropdown"  data-placement="left" title="Nuevo gasto" onclick="cambiarEstatus(0)" type="button"> <i class="fa fa-folder-open" aria-hidden="true" ></i> </button>
                              </div>
                            </div>

                          </div>
                          <div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-12">
                              <label>Fecha(*):</label>
                              <input type="date" class="form-control" format='YYYY-MM-DD' name="fecha" id="fecha" maxlength="10" placeholder="Fecha" required>
                          </div>  
                          <div class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-12">
                              <label>Almacen: </label> 
                              <select onchange="listarGastos()"  id="idalmacenes" name="idalmacenes" class="form-control selectpicker"  data-live-search="true" required >
                              </select>
                          </div>  
                         
                          <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                              <label>Referencia:</label>  
                              <input type="text" class="form-control" name="referencia" id="referencia" maxlength="30" value="" placeholder="Referencia" required>
                          </div>
                          

                          <div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <label>Tipo de gasto:</label>
                            <div class="input-group">
                              <select class="form-control select-picker" onchange="cambiarEstatusCampos()" name="tipodegasto" id="tipodegasto"  required>
                                <option value="S">Deducible</option>
                                <option value="N">No Deducible</option>
                              </select>
                              <div class="input-group-btn">
                                <button class="btn btn-default" data-toggle="dropdown"  data-placement="left" title="Extraer datos de Xml " onclick="subirXml()" type="button"> <i class="fa fa-file-excel-o" aria-hidden="true" ></i> </button>
                              </div>
                              <div class="input-group-btn">
                                <button class="btn btn-default" data-toggle="dropdown"  data-placement="left" title="Subir pdf del gastos " onclick="subirImagen()" type="button"> <i class="fa fa-picture-o" aria-hidden="true" ></i> </button>
                              </div>
                              <div class="input-group-btn">
                                <button class="btn btn-default" data-toggle="dropdown"  data-placement="left" title="Ver pdf del gastos " onclick="verPdf(30)" type="button"> <i class="fa fa-file-pdf-o" aria-hidden="true" ></i> </button>
                              </div>
                            </div>
                          </div>

                          

                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                              <label>Proveedor:</label>
                              <input type="text"   class="form-control" name="nombre" id="nombre"  maxlength="120" placeholder="Nombre" required>
                          </div>
                          
                          <div class="form-group col-lg-2 col-md-4 col-sm-6 col-xs-12">
                              <label>Rfc Proveedor: </label> 
                              <input type="text"   class="form-control" name="rfc" id="rfc"  maxlength="13" placeholder="Rfc" required>
                          </div>
                          <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                              <label>UUID: </label> 
                              <input type="text"   class="form-control" name="uuid" id="uuid"  maxlength="50" placeholder="UUID" required>
                          </div>
                          <div class="form-group col-lg-2 col-md-4 col-sm-6 col-xs-12">
                              <label>Folio </label> 
                              <input type="text"   class="form-control" name="folio" id="folio"  maxlength="20" placeholder="Folio Docto" required>
                          </div>
                          <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                              <label>Gastos: </label> 
                              <select  id="idcat_de_gastos" name="idcat_de_gastos" class="form-control selectpicker"  data-live-search="true" required >
                              </select>
                          </div>  
                          <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                              <label>Concepto del gasto:</label>
                              <input type="text"   class="form-control" name="concepto" id="concepto"  maxlength="50" placeholder="Concepto del gasto" required>
                          </div>  
                          <div class="form-group col-lg-2 col-md-4 col-sm-6 col-xs-12">
                              <label>Tipo de pago: </label> 
                              <select  id="idformasdepagos" name="idformasdepagos" class="form-control selectpicker"  data-live-search="true" required >
                              </select>
                          </div>
                          <div class="form-group col-lg-2 col-md-4 col-sm-6 col-xs-12">
                              <label>Cve pago: </label> 
                              <input type="text"   class="form-control" name="SatClaveFormaDePago" id="SatClaveFormaDePago"  maxlength="50" placeholder="Forma Pago" required>
                          </div>
                         
                          <div class="form-group col-lg-2 col-md-4 col-sm-6 col-xs-12">
                              <label>Forma de pago </label> 
                              <input type="text"   class="form-control" name="SatNombreFormaDePago" id="SatNombreFormaDePago"  maxlength="50" placeholder="Forma Pago" required>
                          </div>
                          <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                              <label>Importe:</label>
                              <input type="number" step="0.01" value='0'  class="form-control" name="importe" id="importe" step="0.01" maxlength="10" placeholder="Importe" required>
                          </div>
                          <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                              <label>Iva:</label>
                              <input type="number" step="0.01"  value='0'  class="form-control" name="iva" id="iva" step="0.01" maxlength="10" placeholder="Importe" required>
                          </div>
                          <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                              <label>Ajuste:</label>
                              <input type="number" step="0.01"  value='0' class="form-control" name="ajuste" id="ajuste" step="0.01" maxlength="10" placeholder="Importe" required>
                          </div>
                          <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                              <label>Subtotal</label>
                              <input type="number" value='0' step="0.01"  class="form-control" name="subtotal" id="subtotal" step="0.01" maxlength="10" placeholder="Importe" required>
                          </div>
                          

                          <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                              <label>Isr Retenido</label>
                              <input type="number"  value='0' step="0.01" class="form-control" name="isrretenido" id="isrretenido" step="0.01" maxlength="10" placeholder="Importe" required>
                          </div>
                          <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                              <label>Iva Retenido</label>
                              <input type="number" value='0' step="0.01" class="form-control" name="ivaretenido" id="ivaretenido" step="0.01" maxlength="10" placeholder="Importe" required>
                          </div>
                          <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                              <label>Total:</label>
                              <input type="number"  value='0'  class="form-control" name="total" id="total" step="0.01" maxlength="10" placeholder="Importe" required>
                          </div>
                          <div class="form-group col-lg-1 col-md-6 col-sm-6 col-xs-12">
                            <label>Moneda:</label>
                            <input type="text"  value='MXN'  class="form-control" name="moneda" id="moneda"  maxlength="20" placeholder="Moneda" required>
                          </div>
                          <div class="form-group col-lg-1 col-md-4 col-sm-6 col-xs-12">
                              <label>Tasa</label> 
                              <input type="number"  value='0.16000'  class="form-control" name="tasaocuota" id="tasaocuota"  maxlength="10" placeholder="Tasa o Cuota" required>
                          </div>
                          <div class="form-group col-lg-1 col-md-6 col-sm-6 col-xs-12">
                            <label id="lblCantidad">Nuevo</label><br>
                            <button  class="btn btn-primary" type="submit" onclick="guardaryeditar(1)" id="btnGrabargasto1"><i class="fa fa-save"></i></button>
                          </div>
                          <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                              <label>Destinos </label> 
                              <select  id="iddestinos" name="iddestinos" class="form-control selectpicker"  data-live-search="true" required >
                              </select>
                          </div>
                          
                        </div>  

                        </form>
                      </div>
                    </div>
                  
                    <!-- /.box-header table table-striped table-bordered table-condensed table-hover -->
                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoregistros">
                        <table id="tbllistado" class="display compact nowrap">
                          <thead>
                            <th>Opciones</th>
                            <th>Id</th>
                            <th>Folio</th>
                            <th>Referencia</th>
                            <th>Fecha</th>
                            <th>Concepto</th>
                            <th>Total</th>
                            <th>Abonos</th>
                            <th>Proveedor</th>
                            <th>Condicion</th>
                          </thead>
                          <tbody>                            
                          </tbody>
                          
                        </table>
                    </div>
                    <div class="panel-body table-responsive" id="listadoregistrosp">
                        <table id="tbllistadop" class="display compact nowrap">
                          <thead>
                            <th>Opciones</th>
                            <th>Np</th>
                            <th>Fecha</th>
                            <th>Referencia</th>
                            <th>Importe</th>
                            <th>Forma de pago</th>
                            <th>Condicion</th>
                          </thead>
                          <tbody>                            
                          </tbody>
                          
                        </table>
                    </div>


                    
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->

              <div class="modal fade" id="myModalSubirXml" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                <div class="modal-dialog" >
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Subir Xml </h4>
                      </div>
                      <form name="formularioXml" id="formularioXml" method="POST">
                        <div class="modal-body">
                            <tbody>

                              <div class="panel-body" id="formulariorXml">
                                  
                                  <a data-controls-modal="myModal" data-backdrop="static" data-keyboard="false" href="#">
                                  <div class="form-group col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                    <label>Xml:</label>
                                    <input type="file" class="form-control" name="imagenXml" id="imagenXml">
                                    <input type="hidden" name="imagenactualXml" id="imagenactualXml">
                                    <img src="" width="150px" height="120px" id="imagenmuestraXml">
                                  </div>  
                                  <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                                    <label>Subir</label><br>
                                    <button type="button"  class="btn btn-primary" onclick="enviarXml()" id="btnEnviarXml"><i class="fa fa-save"></i> Enviar </button>
                                  </div>
                                  
                              </div>
                              <div class="modal-footer">
                                <button id="btnCerrarModalXml" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                              </div> 
                            </tbody>
                        </div>
                               
                    </form>
                </div>
              </div>
            </div>

            <div class="modal fade" id="myModalSubirImagen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                <div class="modal-dialog" >
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Subir Imagen </h4>
                      </div>
                      <form name="formularioPdf" id="formularioPdf" method="POST">
                        <div class="modal-body">
                            <tbody>

                              <div class="panel-body"  id="formulariorpdf">
                                  
                                  <a data-controls-modal="myModal" data-backdrop="static" data-keyboard="false" href="#">
                                  <div class="form-group col-lg-10 col-md-8 col-sm-12 col-xs-12">
                                    <label>Imagen:</label>
                                    <input type="file" class="form-control" name="imagen" id="imagen">
                                    <input type="hidden" name="imagenactual" id="imagenactual">
                                    <img src="" width="150px" height="120px" id="imagenmuestra">
                                  </div>  
                                  <div class="form-group col-lg-2 col-md-4 col-sm-12 col-xs-12">
                                    <label>Subir</label><br>
                                    <button type="button"  class="btn btn-primary" onclick="enviarImagen()" id="btnEnviarImagen"><i class="fa fa-save"></i> Enviar </button>
                                  </div>
                                  
                              </div>
                              <div class="modal-footer">
                                <button id="btnCerrarModalImagen" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                              </div> 
                            </tbody>
                        </div>
                               
                    </form>
                </div>
              </div>
            </div>


            <div class="modal fade" id="myModalPagos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                <div class="modal-dialog" >
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Subir Xml </h4>
                      </div>
                      <form name="formularioPagos" id="formulariopagos" method="POST">
                        <div class="modal-body">
                            <tbody>

                              <div class="panel-body" id="formulariorPagos">
                                  
                                  <a data-controls-modal="myModal" data-backdrop="static" data-keyboard="false" href="#">
                                  
                                  
                              </div>
                              <div class="modal-footer">
                                <button id="btnCerrarModalPagos" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                              </div> 
                            </tbody>
                        </div>
                               
                    </form>
                </div>
              </div>
            </div>


          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
<?php
}
else
{
  require'noacceso.php'; 
}

require 'footer.php';
?>
<script type="text/javascript" src="scripts/gastos.js"></script>
<?php 
  }
  ob_end_flush();
?>