<?php
//activamos el almaceneamiento en el buffer
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) 
{
  header("location: login.html");
}
else
{
require 'header.php';
if (isset($_SESSION['capturista_id']) && $_SESSION['capturista_id']>0)
{

?>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">
                      
						<div class="form-group col-lg-2 col-md-8 col-sm-8 col-xs-7">
                          <label>Id:</label>
                          <input type="number" value="0" class="form-control" name="idgrupo_de_gastos" id="idgrupo_de_gastos" maxlength="10" placeholder="Id" required>
                      	</div>

                      	<div class="form-group col-lg-4 col-md-8 col-sm-8 col-xs-7">
                          <label>Gastos:</label>
                          <input type="text" class="form-control" name="nombre" id="nombre" maxlength="30" placeholder="Descripcion" required>
                      	</div>
                        <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        	<label>Buscar:</label><br>
                         	<button onclick="buscar()" class="btn btn-primary" id="btnbuscar" name="btnbuscar" >Buscar</button>
                        </div>
                        <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-3">
                        	<label>Nuevo:</label><br>
                            <button onclick="guardar(0)" class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Aceptar</button>
                        </div>
                        <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-3">
                          <label>Actualizar:</label><br>
                            <button onclick="guardar(1)" class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Aceptar</button>
                        </div>
                        
                      
                    </div>
                 
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoregistros">
                        <table id="tbllistado" class="display compact nowrap">
                          <thead>
                            <th>Opciones</th>
                            <th>Nombre</th>
                            <th>Estado</th>
                          </thead>
                          <tbody>                            
                          </tbody>
                          <tfoot>
                            <th>Opciones</th>
                            <th>Nombre</th>
                            <th>Estado</th>
                          </tfoot>
                        </table>
                    </div>
                    
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
<?php
}
else
{
  require'noacceso.php'; 
}

require 'footer.php';
?>
<script type="text/javascript" src="scripts/grupo_de_gastos.js"></script>
<?php 
  }
  ob_end_flush();
?>