<?php
if (strlen(session_id())<1)
  session_start();
?>
<!DOCTYPE html> 
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Bmp Sistemas | Rilez Mexicana</title>
    <link rel = "icon" href = "../reportes/logo.jpg" 
        type = "image/x-icon"> 
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../public/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../public/css/font-awesome.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../public/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../public/css/_all-skins.min.css">
    <link rel="logo" href="../public/img/logo.jpg">
    <link rel="shortcut icon" href="../public/img/logo.jpg">

    <!-- datatables -->

    <link rel="stylesheet" type="text/css" href="../public/datatables/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../public/datatables/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../public/datatables/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../public/css/bootstrap-select.min.css">
    
    <link rel="stylesheet" type="text/css" href="../public/css/sweetalert2.min.css">
    <style>  </style>

  </head>
  <body class="hold-transition skin-blue-light sidebar-mini">
    <div class="wrapper">
    
      <header class="main-header">

        <!-- Logo index2.html esto va en la href-->
        <a href="#" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>Bmp</b>Sistemas</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>BMP Sistemas</b></span>
           
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button mensaje modulo fijo -->
         
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Navegación</span>
           <div id="modulo" class="pull-right"></div>
          </a>
  

          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <li class="user-footer">
                    
                    
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">

                    
             
                
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                     
                  
                  <img src="../files/usuarios/<?php echo $_SESSION['imagen']; ?>" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?php echo $_SESSION['nombre']; ?></span>

                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="../files/usuarios/<?php echo $_SESSION['imagen']; ?>" class="img-circle" alt="User Image">
                    <p>
                      Bmp Sistemas - Desarrollando Software
                      <small>www.bmpsistemas.com</small>
                    </p>

                  </li>
                  
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    
                    <div class="pull-right">
                      <a href="../ajax/usuarios.php?op=salir" class="btn btn-default btn-flat">Cerrar</a>
                    </div>
                  </li>
                </ul>
              </li>
              
            </ul>
          </div>

        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">       
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header"></li>
            

            <?php
            $capturista_id=$_SESSION['capturista_id'];
            if ($capturista_id>0)
            {
             
              require_once "../config/Conexion.php";
                             
              $sql="SELECT menu.id AS idmenus,menu.descripcion AS nombre FROM menu
              GROUP BY menu.descripcion ORDER BY menu.orden"; 
              //echo "$sql </br>";
              $resultado1= ejecutarConsulta($sql);      
              while ($fila1 = mysqli_fetch_assoc($resultado1))
              {
                 echo '<li class="treeview"> <a href="#">';
                $idmenus=$fila1['idmenus'];
                $menu=$fila1['nombre'];
                //echo "id menu ".$idmenus." menu ".$nombre."</br>";
                echo '<i class="fa fa-shopping-cart"></i><span>'.$menu.'</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">';
                $sql="SELECT modulo.descripcion,modulo.nombre FROM menu_modulos
                INNER JOIN menu ON menu.id = menu_modulos.menu
                INNER JOIN modulo ON modulo.id = menu_modulos.modulo  
                INNER JOIN accesos ON accesos.modulo_id = modulo.id
                WHERE accesos.usuario_id='$capturista_id' AND menu_modulos.menu='$idmenus'
                ORDER BY modulo.orden  ";
                //echo "$sql </br>";
                $resultado2= ejecutarConsulta($sql);      
                while ($fila2 = mysqli_fetch_assoc($resultado2))
                {
                  $modulo=$fila2['nombre'];
                  $descripcion=$fila2['descripcion'];
                  //echo "modulo ".$nombre." descripcion ".$descripcion."</br>";
                echo '<li><a href="'.$modulo.'"><i class="fa fa-circle-o"></i> '.$descripcion.'</a></li>';
                }
                echo '</ul>
                      </li>';
               }

                
                
              
            }
            ?>

            
            <li>
              <a href="#">
                <i class="fa fa-plus-square"></i> <span>Ayuda</span>
                <small class="label pull-right bg-red">PDF</small>
              </a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-info-circle"></i> <span>Acerca De...</span>
                <small class="label pull-right bg-yellow">Bmp</small>
              </a>
            </li>
                        
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
