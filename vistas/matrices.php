<?php
//activamos el almaceneamiento en el buffer
ob_start();
session_start();

if (!isset($_SESSION["nombre"]))
{
  header("location: login.html");
}
else
{
require 'header.php';
if (isset($_SESSION['almacen']) && $_SESSION['almacen']==1)
{
?>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">
                    <div id="divBuscar" class="row">  
                      <div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12">
                            <label>Descripcion:</label>
                            <input type="text" class="form-control" name="descripcion" id="descripcion" maxlength="100" placeholder="Descripcion de matriz existente" required>
                        </div>
                        <div class="form-group col-lg-1 col-md-2 col-sm-6 col-xs-6">
                            <label>Buscar:</label>
                            <button class="btn btn-primary" id="btnBuscar" onclick="buscarMatriz()"> <i class="fa fa-search-plus" aria-hidden="true"></i></button>
                        </div>

                          
                        <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <h1><button onclick="mostrarform(true)" class="btn btn-primary" id="btnbuscar" name="btnbuscar" >Agregar</button></h1>
                        </div>
                      </div>
                  </div>
                 
                
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoregistros">
                        <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">
                          <thead>
                            <th>Opciones</th>
                            <th>Codigo</th>
                            <th>Nombre</th>

                            <th>Estado</th>
                          </thead>
                          <tbody>                            
                          </tbody>
                          <tfoot>
                            <th>Opciones</th>
                            <th>Codigo</th>
                            <th>Nombre</th>
                            <th>Estado</th>
                          </tfoot>
                        </table>
                    </div>
                    <div class="panel-body" style="height: 400px;" id="formularioregistros">
                    >
                    <!-- <form name="formulario" id="formulario" method="POST"> -->

                      <div class="form-group col-lg-3 col-md-6 col-sm-12 col-xs-12">
                          <label>Articulo:</label>
                          <input type="text" class="form-control" name="nombre" id="nombre" maxlength="100" placeholder="Producto" required>
                      </div>
                      <div class="form-group col-lg-1 col-md-2 col-sm-6 col-xs-6">
                          <label>Buscar:</label>
                          <button class="btn btn-primary" id="btnBuscar" onclick="buscarArticulo(1)"> <i class="fa fa-search-plus" aria-hidden="true"></i></button>
                      </div>

                       <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                          <label>Selecione Producto  </label>
                          <select  id="idarticulos" name="idarticulos" class="form-control selectpicker" value="idmatrices" data-live-search="true" required  >
                          </select>
                      </div> 
                          
                      <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <button class="btn btn-primary" onclick="agregarMatriz()" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>

                        <button class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
                      </div>
                    <!-- </form> -->
                    </div>
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
<?php
}
else
{
  require'noacceso.php'; 
}

require 'footer.php';
?>
<script type="text/javascript" src="scripts/matrices.js"></script>
<?php 
  }
  ob_end_flush();
?>