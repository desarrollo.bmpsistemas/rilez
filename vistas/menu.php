<?php
//activamos el almaceneamiento en el buffer
ob_start();
session_start();

if (!isset($_SESSION["nombre"]))
{
  header("location: login.html");
}
else
{
require 'header.php';
if (isset($_SESSION['escritorio']) && $_SESSION['escritorio']==1)
{
  /*require_once "../modelos/Consultas.php";
  $consulta = new Consultas();
  $rsptac= $consulta->totalcomprahoy();
  $regc=$rsptac->fetch_object();
  $totalc=$regc->total_compra;

  $rsptav= $consulta->totalventahoy();
  $regv=$rsptav->fetch_object();
  $totalv=$regv->total_venta;

  //Datos para mostrar el grafico de barras de las compras
  $compras10 = $consulta->comprasultimos_10dias();
  $fechasc="";
  $totalesc="";
  while ($regfechac = $compras10->fetch_object())
  {
    $fechasc.='"'.$regfechac->fecha.'",';
    $totalesc.=$regfechac->total.',';
  }
  //quitar la ultima coma de las variables
  $fechasc=substr($fechasc,0,-1);
  $totalesc=substr($totalesc,0,-1);*/

  //Datos para mostrar el grafico de barras de las ventas por año
  /*$ventas12 = $consulta->ventasultimos_12meses();
  $fechasv="";
  $totalesv="";
  while ($regfechav = $ventas12->fetch_object())
  {
    $fechasv.='"'.$regfechav->fecha.'",';
    $totalesv.=$regfechav->total.',';
  }
  //quitar la ultima coma de las variables
  $fechasv=substr($fechasv,0,-1);
  $totalesv=substr($totalesv,0,-1);*/
  


?>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">
                          <h1 class="box-title">Menu Inicio </h1>
                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <!-- centro -->
                    
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
<?php
}
else
{
  require'noacceso.php'; 
}

require 'footer.php';
?>

<?php 
}
ob_end_flush();
?>