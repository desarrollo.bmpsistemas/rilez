<?php
//activamos el almaceneamiento en el buffer  
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) 
{
  header("location: login.html");
}
else
{
require 'header.php';
if (isset($_SESSION['capturista_id']) && $_SESSION['capturista_id']>0)
{
?>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">
                      <div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                          <label>Menus(*):</label>
                          <select onchange="listaModulos()" id="idmenus" name="idmenus" class="form-control selectpicker" data-live-search="true" required></select>
                      </div>
                      <div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                          <label>Modulos(*):</label>
                          <select id="idmodulos" name="idmodulos" class="form-control selectpicker" data-live-search="true" required></select>
                      </div>
                      
                      <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                          <label>Asignar(*):</label><br>
                          <button class="btn btn-success" id="btnagregar" onclick="agregarModulo()"><i class="fa fa-plus-circle"></i> Aceptar</button> 
                      </div>
                      <!--<div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                          <label>Opciones(*):</label><br>
                          <button class="btn btn-success" id="btnagregar" onclick="Catalogos()"><i class="fa fa-plus-circle"></i> Buscar</button> 
                      </div>-->
                      
                      <!--</form>-->
                    </div> <!-- /.box-header -->
                  </div>    
                </div><!-- /.box --> 
                   
                <!-- centro -->
                <div class="panel-body table-responsive" id="listadoregistros">
                    <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">
                      <thead>
                        <th>Opciones</th>
                        <th>Id Menu de Modulos</th>
                        <th>Descripcion</th>
                        <th>Modulo</th>
                        
                      </thead>
                      <tbody>                            
                      </tbody>
                      <tfoot>
                        <th>Opciones</th>
                        <th>Id Menu Modulos</th>
                        <th>Descripcion</th> 
                        <th>Modulo</th>
                        
                      </tfoot>
                    </table>
                </div>
            </div><!-- /.row -->
        </section><!-- /.content -->

      </div><!-- /.content-wrapper -->

      
      <!--<div class="modal fade" id="myModalCatalogos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
      <div class="modal-dialog" style="width: 90% !important;">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Abc de menus y modulos </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                  <tbody>
                    <div class="panel-body" style="height: 100px;" id="formulariorMunicipios">
                     
                        <a data-controls-modal="myModal" data-backdrop="static" data-keyboard="false" href="#">
                        <div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                          <label>Menus(*):</label>
                          <select onchange="tomaMenu()" id="idmenus2" name="idmenus2" class="form-control selectpicker" data-live-search="true" required></select>
                      </div>
                        
                        <div class="form-group col-lg-4 col-md-3 col-sm-3 col-xs-8">
                            <label>Menu:</label>
                            <input type="text"   class="form-control" name="nombreme" id="nombreme" maxlength="120" placeholder="Menu" required>
                        </div>
                        <div class="form-group col-lg-2 col-md-3 col-sm-3 col-xs-8">
                            <label>Orden:</label>
                            <input type="number"   class="form-control" name="orden" id="orden" maxlength="120" placeholder="Orden" required>
                        </div>
                        
                        <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-2">
                           <label>Guardar:</label>
                          <p><button  class="btn btn-primary" onclick="guardarMenu()" id="btnGuardarMe"><i class="fa fa-save"></i> Guardar </button></p>
                        </div>

                        <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                          <label>Modulos(*):</label>
                          <select id="idmodulos2" name="idmodulos2" class="form-control selectpicker" data-live-search="true" required></select>
                        </div>
                        <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-8">
                            <label>Descripcion:</label>
                            <input type="text"   class="form-control" name="descripcion" id="descripcion" maxlength="120" placeholder="Descripcion" required>
                        </div>
                        <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-8">
                            <label>Modulo:</label>
                            <input type="text"   class="form-control" name="modulo" id="modulo" maxlength="120" placeholder="Descripcion" required>
                        </div>
                        <div class="form-group col-lg-1 col-md-3 col-sm-3 col-xs-8">
                            <label>Orden:</label>
                            <input type="number"   class="form-control" name="ordenmo" id="ordenmo" maxlength="120" placeholder="Orden" required>
                        </div>
                        <div class="form-group col-lg-1 col-md-2 col-sm-2 col-xs-6">
                           <label>Agregar:</label>
                          <p><button  class="btn btn-primary" onclick="guardarModulo()" id="btnGuardarMo"><i class="fa fa-save"></i> Guardar </button></p>
                        </div>
                        <div class="form-group col-lg-1 col-md-2 col-sm-2 col-xs-6">
                           <label>Actualizar:</label>
                          <p><button  class="btn btn-primary" onclick="guardarModulo()" id="btnGuardarAct"><i class="fa fa-save"></i> Guardar </button></p>
                        </div>
                      
                      
                    </div>
                  </tbody>
                </div>  
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>        
          </div>
        </div>
      </div>-->


  <!--Fin-Contenido-->
<?php
}
else
{
  require'noacceso.php'; 
}
require 'footer.php';
?>

<script type="text/javascript" src="scripts/menu_modulos.js"></script>
<?php 
  }
  ob_end_flush();
?>