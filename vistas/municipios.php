<?php
//activamos el almaceneamiento en el buffer
ob_start();
session_start();

if (!isset($_SESSION["nombre"]))
{
  header("location: login.html");
}
else
{
require 'header.php';
if (isset($_SESSION['capturista_id']) && $_SESSION['capturista_id']>0)
{

?>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">
                          <div class="box-header with-border">
                      <div class="form-group col-lg-8 col-md-8 col-sm-8 col-xs-7">
                          <label>Municipios:</label>
                          <input type="text" class="form-control" name="nombreb" id="nombreb" maxlength="100" placeholder="Nombre" required>
                      </div>
                     
                      <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-2">
                         <h1><button onclick="buscar()" class="btn btn-primary" id="btnbuscar" name="btnbuscar" >Buscar</button></h1>
                      </div>
                      <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-3">
                        <h1> <button class="btn btn-success" id="btnagregar" onclick="mostrarform(true)"><i class="fa fa-plus-circle"></i> Agregar</button></h1>
                        <div class="box-tools pull-right">
                        </div>    
                      </div>
                    </div>

                    </div>
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoregistros">
                        <table id="tbllistado" class="display compact nowrap">
                          <thead>
                            <th>Opciones</th>
                            <th>Paises</th>
                            <th>Estados</th>
                            <th>Municipios</th>
                            <th>Estado</th>
                          </thead>
                          <tbody>                            
                          </tbody>
                          <tfoot>
                            <th>Opciones</th>
                            <th>Paises</th>
                            <th>Estados</th>
                            <th>Municipios</th>
                            <th>Estado</th>
                          </tfoot>
                        </table>
                    </div>
                    <div class="panel-body" id="formularioregistros">
                        <form name="formulario" id="formulario" method="POST">
                         
                          <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label>Paises(*): </label>
                            <select id="idpaises" name="idpaises" class="form-control selectpicker" value="idpaises" data-live-search="true" required onchange="listarestados()" >
                            </select>
                          </div>
                          
                          <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label>Estados(*):</label>
                            <select id="idestados" name="idestados" class="form-control selectpicker" data-live-search="true" required></select>
                          </div>
                         
                          <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label>Municipio(*):</label>
                            <input type="hidden" name="idmunicipios" id="idmunicipios">
                            <input type="text" class="form-control" name="nombre" id="nombre" maxlength="100" placeholder="Nombre" required>
                          </div>
                          

                          

                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i>Guardar</button>
                            <button class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
                          </div>

                        </form>
                      
                    </div>
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
<?php
}
else
{
  require'noacceso.php'; 
}
require 'footer.php';
?>

<script type="text/javascript" src="scripts/municipios.js"></script>
<?php 
  }
  ob_end_flush();
?>