<?php
//activamos el almaceneamiento en el buffer
ob_start();
session_start();

if (!isset($_SESSION["nombre"]))  
{
  header("location: login.html");
}
else
{
require 'header.php';
if (isset($_SESSION['capturista_id']) && $_SESSION['capturista_id']>0)
{
?>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">    
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">

                      <input type="hidden" name="idusuarios" id="idusuarios" value="<?php echo $_SESSION['capturista_id']; ?> ">
                      <input type="hidden" name="Sidalmacenes" id="Sidalmacenes" value="<?php echo $_SESSION['almacen_id']; ?> ">
                      <div style="display: none" class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label>Tipo:</label>
                            <select class="form-control select-picker" name="tipo" id="tipo"  required>
                              <option value="6">Ventas</option>
                            </select>
                      </div>

                      <div id="div" class="row">
                        <div class="form-group col-lg-2 col-md-5 col-sm-6 col-xs-12">
                            <label>Folio:</label>
                            <input onfocusin="ocultar()" type="number" class="form-control" name="Folio" id="Folio" maxlength="10" placeholder="Folio" required>
                        </div>
                        <div class="form-group col-lg-1 col-md-6 col-sm-6 col-xs-12">
                               <label>Buscar</label>
                              <p><button onclick="listarEntSal(1)" class="btn btn-primary" id="btnBuscart"><i class="fa fa-search"></i></button></p>
                        </div> 
                        <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label>Ó Busqueda por:</label>
                            <select onchange="mostrarDivPorPersona()" class="form-control select-picker" name="tipodebusqueda" id="tipodebusqueda"  required>
                              <option value="0">Selecione</option>
                              <option value="Persona">Por Persona</option>
                            </select>
                        </div>
                        <div id="divAlmacen" class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                              <label>Almacenes:</label>
                              <select  id="idalmaceness"  name="idalmaceness" class="form-control selectpicker"  data-live-search="true" required  >
                            </select>
                        </div>
                        <div id="divFechaInicial" class="form-group col-lg-2 col-md-3 col-sm-3 col-xs-12">
                            <label>Fecha Inicial:</label>
                            <input type="date" class="form-control" name="fecha_inicial" id="fecha_inicial" maxlength="10" placeholder="Fecha Inicial" required>
                        </div>
                        <div id="divFechaFinal" class="form-group col-lg-2 col-md-3 col-sm-3 col-xs-12">
                            <label>Fecha Final:</label>
                            <input type="hidden" name="datos" id="datos">
                            <input type="date" class="form-control" name="fecha_final" id="fecha_final" maxlength="10" placeholder="Fecha Final" required>
                        </div>
                        <div class="form-group col-lg-1 col-md-6 col-sm-6 col-xs-12">
                              <label>Buscar</label>
                              <p><button onclick="listarEntSal(4)" class="btn btn-primary" id="btnBuscart"><i class="fa fa-search"></i></button></p>
                        </div> 
                      </div>
                      
                      
                      
                    
                      <div id="divPersona" name="divPersona" class="row" style="display: none">

                        
                        <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label>Condicion:</label>
                            <select class="form-control select-picker" name="condicion" id="condicion"  required>
                              <option value="SinPagar">Sin Pagar</option>
                              <option value="Pagado">Pagado</option>
                            </select>
                        </div>
                          <div   class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <label>Persona:</label>
                            <input type="text"  class="form-control"  name="descripcion" id="descripcion" maxlength="100" placeholder="Nombre Libre" required>
                          </div>
                          <div  class="form-group col-lg-2 col-md-12 col-sm-12 col-xs-12">
                             <label>Buscar</label>
                            <p><button onclick="buscarPersonas()" class="btn btn-primary" id="btnBuscarp"><i class="fa fa-search"> Buscar</i></button></p>
                          </div>  
                          <div class="form-group col-lg-4 col-md-12 col-sm-12 col-xs-9">
                            <label>Selecione Persona: </label>
                            <select onchange="listarEntSal(2)"   id="idpersonas"  name="idpersonas" class="form-control selectpicker"  data-live-search="true" required  >
                            </select>
                          </div> 
                      </div>   

                      
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoregistros">
                        <table id="tbllistado"  class="table table-striped table-bordered table-condensed table-hover">
                          <thead>
                            
                            <th>Opc</th>
                            <th>Proveedor</th>
                            <th>Id</th>
                            <th>Fecha</th>
                            <th>Estatus</th>
                            <th>Total</th>
                            <th>Abonos</th>
                            <th>Saldo</th>
                            
                          </thead>
                          <tbody>                            
                          </tbody>
                          <tfoot>
                            <th>Opc</th>
                            <th>Proveedor</th>
                            <th>Id</th>
                            <th>Fecha</th>
                            <th>Estatus</th>
                            <th>Total</th>
                            <th>Abonos</th>
                            <th>Saldo</th>
                          </tfoot>
                        </table>
                    </div>
                    
                    <div id="div" class="row">
                        <div class="form-group col-lg-2 col-md-5 col-sm-6 col-xs-5">
                          <input type="hidden" name="idpagos" id="idpagos" value="0">
                            <label onclick="permisoDeFecha(1)">Fecha(?):</label>
                            <input type="date" class="form-control" name="fecha" id="fecha" maxlength="100" placeholder="Fecha"  required>
                        </div>
                        <div class="form-group col-lg-2 col-md-5 col-sm-6 col-xs-5">
                            <label>Folio:</label>
                            <input type="number" class="form-control" name="folio" id="folio" maxlength="20" placeholder="Folio" required>
                        </div>
                        <div class="form-group col-lg-1 col-md-5 col-sm-6 col-xs-5">
                            <label>Np:</label>
                            <input type="number" class="form-control" name="np" id="np" maxlength="20" placeholder="Np" value="0" required>
                        </div>
                        <div  class="form-group col-lg-1 col-md-12 col-sm-12 col-xs-12">
                             <label>Buscar</label>
                            <p><button onclick="listarEntSal(1)" class="btn btn-primary" id="btnBuscarf"><i class="fa fa-search"></i></button></p>
                        </div>  
                        <div class="form-group col-lg-1 col-md-5 col-sm-6 col-xs-5">
                            <label>Ref. Pago:</label>
                            <input type="text" class="form-control" name="referencia" id="referencia" value="PM" maxlength="20" placeholder="Ref de Pago" required>
                        </div>
                        <div class="form-group col-lg-2 col-md-12 col-sm-12 col-xs-9">
                            <label>Forma de Pago: </label>
                            <select   id="idformadepagos"  name="idformadepagos" class="form-control selectpicker"  data-live-search="true" required  >
                            </select>
                          </div> 
                        <div class="form-group col-lg-2 col-md-5 col-sm-6 col-xs-5">
                            <label>Importe:</label>
                            <input type="number" step="0.01" class="form-control" name="importe" id="importe" maxlength="20" placeholder="Pago" required>
                        </div>
                        <div  class="form-group col-lg-1 col-md-12 col-sm-12 col-xs-12">
                             <label>Grabar</label>
                            <p><button onclick="agregarPago(0)" class="btn btn-primary" id="btnGuardar0"><i class="fa fa-save"></i></button></p>
                        </div> 
                      </div>
                    

                    <div class="panel-body table-responsive" id="listadoregistros2">
                        <table id="tbllistado2"  class="table table-striped table-bordered table-condensed table-hover">
                          <thead>
                            
                            <th>Opc</th>
                            <th>Id</th>
                            <th>Np</th>
                            <th>Fecha</th>
                            <th>TipoPago</th>
                            <th>Importe</th>
                            <th>Ref.Pago</th>
                           
                            <th>Capturo</th>
                          </thead>
                          <tbody>                            
                          </tbody>
                          <tfoot>
                            <th>Opc</th>
                            <th>Id</th>
                            <th>Np</th>
                            <th>Fecha</th>
                            <th>TipoPago</th>
                            <th>Importe</th>
                            <th>Ref.Pago</th>
                           
                            <th>Capturo</th>
                          </tfoot>
                        </table>
                    </div>
                   
                    <div class="panel-body" style="height: 400px;" id="formularioregistros">
                        <form name="formulario" id="formulario" method="POST">
                           <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <input type="hidden" name="idarticulo" id="idarticulo">
                            <input type="hidden" name="idarticulos_almacenes" id="idarticulos_almacenes">
                            <label>Almacenes: </label>
                            <select onchange="leerIva_y_Moneda()" id="idalmacenes" name="idalmacenes" class="form-control selectpicker" value="idalmacenes" data-live-search="true" required >
                            </select>
                          </div>
                          
                          <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <label>Monedas: </label>
                            <select id="idmonedas" name="idmonedas" class="form-control selectpicker" value="idmonedas" data-live-search="true" required >
                            </select>
                          </div>
                           <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <label>Afectacion(*):</label>
                            <select class="form-control select-picker" name="afectacion" id="afectacion"  required>
                              <option value="A">Almacen Actual</option>
                              <option value="P">Pesos</option>
                              <option value="D">Dolares</option>
                              <option value="T">Todos</option>
                            </select>
                          </div>

                          <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <label>Ivas: </label>
                            <select id="idivas" name="idivas" class="form-control selectpicker" value="idivas" data-live-search="true" required >
                            </select>
                          </div>

                          <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label>Costo(*):</label>
                            <input type="number" step="0.01" class="form-control" name="costo" id="costo" maxlength="10" placeholder="Costo" required>
                          </div>
                          <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label>Por % Utilidad(*):</label>
                            <input type="number" step="0.01" class="form-control" name="utilidad" id="utilidad" maxlength="10" placeholder="% Utilidad" required>
                          </div>


                          <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label>Precio(*):</label>
                            <input type="number" step="0.01" class="form-control" name="precio" id="precio" maxlength="10" placeholder="Precio" required>
                          </div>

                          <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label>Stock(*):</label>
                            <input type="text" class="form-control" name="stock" id="stock" maxlength="10" placeholder="Stock" required>
                          </div>
                          <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label>Ubicacion(*):</label>
                            <input type="text" class="form-control" name="ubicacion" id="ubicacion" maxlength="10" placeholder="Ubicacion" required>
                          </div>

                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>

                            <button class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
                          </div>
                        </form>
                    </div>
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->
        
      
      <div class="modal fade" id="myModalAutorizarFecha" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
      <div class="modal-dialog" style="width: 50% !important;">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Autorizacion de Fecha </h4>
            </div>
            <div class="modal-body">
              
                <tbody>
                    <div class="panel-body" style="height: 80px;" id="formulariorAutorizacion">
                      
                        <a data-controls-modal="myModal" data-backdrop="static" data-keyboard="false" href="#">
                        <div class="form-group col-lg-4 col-md-12 col-sm-12 col-xs-12">
                            <label>Id usuario(*):</label><br>
                            <input type="number" step="0.01" class="form-control" name="usuarioF" id="usuarioF" maxlength="10" placeholder="Usuario" required>
                        </div>
                        <div class="form-group col-lg-4 col-md-12 col-sm-12 col-xs-12">
                            <label>Password(*):</label><br>
                            <input type="password"  class="form-control" name="claveF" id="claveF" maxlength="10" placeholder="Clave" required>
                        </div>
                        
                        <div class="form-group col-lg-4 col-md-12 col-sm-12 col-xs-12">
                          <label id="lblCantidad">Autorizar</label><br>
                          <button  class="btn btn-primary" onclick="autorizarFechaDePago()" id="btnBuscarAutorizar"><i class="fa fa-save"></i> Buscar </button>
                        </div>
                      
                  </div>
                </tbody>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>        
          </div>
      </div>
    </div>


    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
<?php
}
else
{
  require'noacceso.php'; 
}

require 'footer.php';
?>
<script type="text/javascript" src="scripts/pagos.js"></script>
<?php 
  }
  ob_end_flush();
?>