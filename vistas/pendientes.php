<?php
//activamos el almaceneamiento en el buffer 
ob_start();
session_start();

if (!isset($_SESSION["nombre"]))
{
  header("location: login.html");
}
else
{
require 'header.php';
if (isset($_SESSION['almacen']) && $_SESSION['almacen']==1)
{
?>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                  	<div class="box-header with-border">
                      
	                    <div id="divfechainicial" class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
	                        <label>Fecha Inicial:</label>
	                        <input type="date" onchange="mostrarPendientes()" class="form-control" name="fecha_inicial" id="fecha_inicial" maxlength="100" placeholder="Fecha Inicial" required>
	                    </div>
	                    <div id="divfechafinal" class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
	                        <label>Fecha Final:</label>
	                        <input type="date" onchange="mostrarPendientes()" class="form-control" name="fecha_final" id="fecha_final" maxlength="100" placeholder="Fecha Final" required>
	                    </div>
	                    <div  class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
	                        <label>Agregar:</label>
	                        <button class="btn btn-success" onclick="mostrarform(true)" type="button"><i class="fa fa-plus-circle"></i> Agregar</button>
	                    </div>
                	</div>
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoregistros">
                        <table id="tbllistado" class="display compact nowrap">
                          <thead>
                            <th>Opciones</th>
                            <th>Cliente</th>
                            <th>Fecha</th>
                            <th>Hora I</th>
                            <th>Hora F</th>
                            <th>Pendiente</th>
                            <th>Resultado</th>
                            <th>Estado</th>
                          </thead>
                          <tbody>                            
                          </tbody>
                          <tfoot>
                          	<th>Opciones</th>
                            <th>Cliente</th>
                            <th>Fecha</th>
                            <th>Hora I</th>
                            <th>Hora F</th>
                            <th>Pendiente</th>
                            <th>Resultado</th>
                            <th>Estado</th>
                          </tfoot>
                        </table>
                    </div>
                    <div class="panel-body" id="formularioregistros">
                        <form name="formulario" id="formulario" method="POST">                      
                          
                          <div class="form-group col-lg-5 col-md-4 col-sm-8 col-xs-8">
                           
                            <label>Cliente:</label>
                            <input type="text" class="form-control" name="nombreb" id="nombreb" maxlength="100" placeholder="Nombre" required>
                           
                          </div>
                     
                          <div class="form-group col-lg-2 col-md-2 col-sm-4 col-xs-4">
                             <button onclick="buscar()" class="btn btn-primary" id="btnbuscar" name="btnbuscar" ><h5>Buscar</h5></button>
                          </div>

                          <div class="form-group col-lg-5 col-md-6 col-sm-12 col-xs-12">
                          	<input type="hidden" name="idpendientes" id="idpendientes">
                            <label>Personas(*):</label>
                            <select   id="idpersonas" name="idpersonas" data-container="body" class="selectpicker form-control"  data-live-search="true" required></select>

                          </div>

                          <div class="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <label>De quien(*):</label>
                            <input type="text" class="form-control" name="origen" id="origen" maxlength="20" placeholder="De Quien" required>
                          </div>
                          <div class="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <label>Para(*):</label>
                            <select id="idusuarios" name="idusuarios" class="form-control selectpicker"  data-live-search="true" data-container="body" data-hide-disabled="true" required> </select>
                          </div>
                          <div class="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <label>Fecha(*):</label>
                            <input type="date" class="form-control" name="fecha" id="fecha" maxlength="20" placeholder="Fecha" required>
                          </div>

                          <div class="form-group col-lg-12 col-md-6 col-sm-12 col-xs-12">
                            <label>Asunto(*):</label>
                            <textarea class="form-control rounded-0" name="asunto" id="asunto" rows="3"></textarea>
                          </div>
                          <div class="form-group col-lg-12 col-md-6 col-sm-12 col-xs-12">
                            <label>Contestacion(*):</label>
                            <textarea class="form-control rounded-0" name="contestacion" id="contestacion" rows="3"></textarea>
                          </div>
                          
                          <div class="form-group col-lg-12 col-md-6 col-sm-12 col-xs-12">
                            <label>Importe(*):</label>
                            <input type="number" step="0.01" class="form-control" name="importe" id="importe" maxlength="10" placeholder="Importe" required>
                          </div>

                          <div class="form-group col-lg-8 col-md-6 col-sm-6 col-xs-12">
                            <label>Imagen:</label>
                            <input type="file" class="form-control" name="imagen" id="imagen">
                            <input type="hidden" name="imagenactual" id="imagenactual">
                            <img src="" width="150px" height="120px" id="imagenmuestra">
                          </div>
                          
                          <div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>
                            <button class="btn btn-danger" onclick="cancelarform(false)" type="button"><i class="fa fa-arrow-circle-left"></i> Regresar</button>
                          </div>
                        </form>
                    </div>
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
<?php
}
else
{
  require'noacceso.php'; 
}
require 'footer.php';
?>
<script type="text/javascript" src="scripts/pendientes.js"></script>
<?php 
  }
  ob_end_flush();
?>