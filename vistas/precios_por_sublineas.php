<?php
//activamos el almaceneamiento en el buffer
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) 
{
  header("location: login.html"); 
}
else
{
require 'header.php';
if (isset($_SESSION['almacen']) && $_SESSION['almacen']==1)
{
?>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">    
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">
                      <div id="articulos" class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <label>Selecione Almacen  </label>
                            <select  id="idalmacenes" name="idalmacenes" class="form-control selectpicker" data-live-search="true" required onchange="buscarcodigo()" ></select>
                      </div> 
                      <div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12">
                          <label>Lineas:</label>
                          <select id="idlineas" onchange="selectsublinea()" name="idlineas" class="form-control selectpicker"  data-live-search="true" required >
                          </select>
                      </div>
                      <div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12">
                          <label>SubLineas:</label>
                          <select id="idsublineas" onchange="selectarticulosporsublinea()" name="idsublineas" class="form-control selectpicker"  data-live-search="true" required >
                          </select>
                      </div>
                      <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label>Por % Utilidad(*):</label>
                            <input type="number" step="0.01" class="form-control" name="porcentaje_utilidad" id="porcentaje_utilidad" maxlength="10" placeholder="Factor Utilidad 1+%" required>
                      </div>
                      <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                         
                            <label>Precios Por Sublineas</label> 
                             <h1 class="box-title">
                             <button class="btn btn-success" id="btnagregar" onclick="actualizar_precios()" data-toggle="tooltip" title="Procesar Sublinea"><i class="fa fa-plus-circle"></i> Procesar</a></button> </br>   </h1>

                        <div class="box-tools pull-right">
                        </div>

                      </div>   
                    </div>
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoregistros">
                        <table id="tbllistado"  class="display compact nowrap">
                          <thead>
                            <th>Opciones</th>
                            <th>Articulo</th>
                            <th>Costo</th>
                            <th>Utilidad</th>
                            <th>Precio</th>
                            <th>Estado</th>
                          </thead>
                          <tbody>                            
                          </tbody>
                          <tfoot>
                            <th>Opciones</th>
                            <th>Articulo</th>
                            <th>Costo</th>
                            <th>Utilidad</th>
                            <th>Precio</th>
                            <th>Estado</th>
                          </tfoot>
                        </table>
                    </div>
                    <div class="panel-body" style="height: 400px;" id="formularioregistros">
                        
                    </div>
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
<?php
}
else
{
  require'noacceso.php'; 
}

require 'footer.php';
?>
<script type="text/javascript" src="scripts/precios_por_sublineas.js"></script>
<?php 
  }
  ob_end_flush();
?>