<?php
error_reporting(E_ALL);
ini_set('display_errors', '1'); 
ob_start();
session_start();

if (!isset($_SESSION["nombre"]))
{
  header("location: login.html");
}
else
{
require_once ('header.php');
if (isset($_SESSION['capturista_id']) && $_SESSION['capturista_id']>0)
{

?>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">
                      <div class="row">
                        <div   class="form-check form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                          <input type="hidden" name="Sidalmacenes" id="Sidalmacenes" value="<?php echo $_SESSION['almacen_id']; ?> ">
                          <label >Selecione Origen </label> <!--mostrarMovEntSal -->
                          <div class="input-group">
                              <div class="input-group-btn">
                                  <button class="btn btn-default" onclick="mostrarModalFechas()" title="Mostrar saldo de fondos por fecha" type="button"> <i class="fa fa-search" aria-hidden="true"></i></button>
                              </div>
                              <select onchange="mostrarFondos()"  id="idcat_de_origeness"  name="idcat_de_origeness" class="form-control selectpicker"  data-live-search="true" required  ></select>
                              <div class="input-group-btn">
                                  <button class="btn btn-default" onclick="mostrarFondos()" title="Mostrar saldo de origenes" type="button"> <i class="fa fa-search" aria-hidden="true"></i></button>
                              </div>
                            </div>
                        </div>
                        <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                          <label>Entradas:</label>
                          <input type="number" step="0.01" class="form-control" name="entradas" id="entradas" maxlength="10" placeholder="Importe" required>
                      </div>
                    <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                          <label>Saldo:</label>
                          <input type="number" step="0.01" class="form-control" name="saldo" id="saldo" maxlength="10" placeholder="Importe" required>
                      </div>

                        <div   class="form-check form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                         <label>Selecione Destino </label> <!--mostrarMovEntSal -->
                          
                          <div class="input-group">
                            <div class="input-group-btn">
                                  <button class="btn btn-default" onclick="mostrarModalFechas()" title="Mostrar saldo de fondos por fecha y destino" type="button"> <i class="fa fa-search" aria-hidden="true"></i></button>
                            </div>
                            <select   id="iddestinoss" onchange="selecionarCapturista()"  name="iddestinoss" class="form-control selectpicker"  data-live-search="true" required  ></select>
                            <div class="input-group-btn">
                              <button class="btn btn-default" onclick="mostrarform(true)" type="button"> <i class="fa fa-search-plus" title="Agregar retiros " aria-hidden="true"></i></button>
                            </div>
                          </div>
                        </div>
                        </div>
                        
                     
                    </div>
                    </div>
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoregistros">
                        <table id="tbllistado" class="display compact nowrap">
                          <thead>
                            <th>Opciones</th>
                            <th>Origen</th>
                            <th>Destino</th>
                            <th>Capturista</th>
                            <th>Fecha</th>
                            <th>Importe</th>
                            <th>Nota</th>
                            <th>Estado</th>
                          </thead>
                          <tbody>                            
                          </tbody>
                          
                        </table>
                    </div>
                    
                    <div class="panel-body" style="height: 400px;" id="formularioregistros">
                        <form name="formulario" id="formulario" method="POST">
                          
                          <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <input type="hidden" name="idcat_de_origenes" id="idcat_de_origenes" >
                            <input type="hidden" name="iddestinos" id="iddestinos" >
                            <input type="hidden" name="idusuarios" id="idusuarios" value="<?php echo $_SESSION['capturista_id']; ?> ">
                            <input type="hidden" name="capturista_id" id="capturista_id" value="<?php echo $_SESSION['capturista_id']; ?> ">
                            <label>id:</label>

                            <input type="text" class="form-control" readonly name="idfondos" id="idfondos" maxlength="10" placeholder="Id" required>
                          </div>

                          <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label>fecha:</label>
                            <input type="date" class="form-control" name="fecha" id="fecha" maxlength="10" placeholder="Fecha" required>
                          </div>
                          <div class="form-group col-lg-2 col-md-12 col-sm-12 col-xs-12">
                            <label>Importe</label>
                            <input type="number" step="0.01" class="form-control" name="importe" id="importe" maxlength="10" placeholder="Importe"  required>
                          </div>

                          

                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>nota:</label>
                            <input type="text" class="form-control" name="nota" id="nota" maxlength="50" placeholder="Nota"  required>
                          </div>
                          


                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>
                            <button class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
                          </div>
                        </form>
                    </div>



                    <div class="modal fade" id="myModalCajaChica" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Caja chica </h4>
                          </div>
                          <div class="modal-body">
                            
                              <tbody>
                                  <div class="panel-body" id="formularioCajaChica" >
                                    <div class="row">
                                        <div   class="form-check form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                          <label>Capturista:</label>
                                          <div class="input-group">
                                            <input type="text" class="form-control" name="busquedac" id="busquedac" maxlength="100" placeholder="Capturista" required>
                                            <div class="input-group-btn">
                                                <button class="btn btn-default" onclick="buscarCapturista()" title="Buscar Capturista" type="button"> <i class="fa fa-search" aria-hidden="true"></i></button>
                                            </div>
                                          </div>
                                        </div>

                                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                          
                                          <label>Selecione Capturista </label> 
                                          <select   id="idpersonas" onchange="tomarCapturista()" name="idpersonas" class="form-control selectpicker"  data-live-search="true" required  >
                                          </select>
                                        </div> 
                                      </div>
                                    </div>  
                              </tbody>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                          </div>        
                        </div>
                      </div>
                    </div>


                  




                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
    
    <div class="modal fade" id="myModalFechas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Consultas </h4>
          </div>
          <div class="modal-body">
            <tbody>
              <div class="panel-body" id="formularioCajaChica" >
                <div class="row">
                  <div   class="form-check form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <label>Fecha Inicial:</label>
                    <input type="date" class="form-control" name="fecha_inicial" id="fecha_inicial" maxlength="10"  required>
                  </div>
                  
                  <div   class="form-check form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <label>Fecha Final:</label>
                    <input type="date" class="form-control" name="fecha_final" id="fecha_final" maxlength="10" required>
                  </div>
                  <div   class="form-check form-group col-lg-1 col-md-1 col-sm-4 col-xs-4">  
                    <button class="btn btn-default" onclick="listar()" title="Mostrar Fondos" type="button"> <i class="fa fa-search" aria-hidden="true"></i></button>
                  </div>
                  <div   class="form-check form-group col-lg-1 col-md-1 col-sm-4 col-xs-4">  
                    <button class="btn btn-default" onclick="listarDestinos()" title="Mostrar  Destinos de caja " type="button"> <i class="fa fa-search" aria-hidden="true"></i></button>
                  </div>
                  <div   class="form-check form-group col-lg-1 col-md-1 col-sm-4 col-xs-4">  
                    <button class="btn btn-default" onclick="listarDestinosPorCapturista()" title="Mostrar  Destinos de caja por Usuario" type="button"> <i class="fa fa-search" aria-hidden="true"></i></button>
                  </div>
                </div> 
              </div> 
            </tbody>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>        
        </div>
      </div>
    </div>
  

    


    



  <!--Fin-Contenido-->
<?php
}
else
{
  require 'noacceso.php';
}
require 'footer.php';
?>
<script type="text/javascript" src="scripts/retiros.js"></script>
<?php 
}
ob_end_flush();
?>
