<?php
error_reporting(E_ALL);
ini_set('display_errors', '1'); 
ob_start();
session_start();

if (!isset($_SESSION["nombre"]))
{
  header("location: login.html");
}
else
{
require_once ('header.php');
if (isset($_SESSION['capturista_id']) && $_SESSION['capturista_id']>0)
{

?>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">
                    	<div class="row">
                        <div   class="form-check form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <input type="hidden" name="idusuarios" id="idusuarios" value="<?php echo $_SESSION['capturista_id']; ?> ">
                        <input type="hidden" name="Sidalmacenes" id="Sidalmacenes" value="<?php echo $_SESSION['almacen_id']; ?> ">
                        
                        <label>Busqueda de Persona </label> <!--mostrarMovEntSal -->
                          
                          <div class="input-group">
                            <div class="input-group-btn"> 
                              <button onclick="MostrarModalOpcionPersona()" class="btn btn-default" data-toggle="dropdown"  data-placement="left" title="Selecione tipo de busqueda"  type="radio"> <i class="fa fa-question-circle" aria-hidden="true"></i></button>
                            </div>
                           
                            <input type="text" class="form-control" placeholder="Search"  name="busquedap" id="busquedap" maxlength="100" placeholder="rfc-nombre-tel" required>
                            <div class="input-group-btn">
                              <button class="btn btn-default" onclick="buscarPersonas(1)" type="button"> <i class="fa fa-search" aria-hidden="true"></i></button>
                            </div>
                          </div>
                      	</div>

                        
                 
                        <div   class="form-check form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                         <label id="lblPersona" name="lblPersona">Selecione Persona: </label> <!--mostrarMovEntSal -->
                          
                          <div class="input-group">
                            <select onchange="listar()"  id="idpersonas"  name="idpersonas" class="form-control selectpicker"  data-live-search="true" required  ></select>
                            <div class="input-group-btn">
                              <button class="btn btn-default" onclick="mostrarform(true)" type="button"> <i class="fa fa-search" aria-hidden="true"></i></button>
                            </div>
                          </div>
                      	</div>
                       
                     
                    </div>
                    </div>
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoregistros">
                        <table id="tbllistado" class="display compact nowrap">
                          <thead>
                            <th>Opciones</th>
                            <th>Nombre</th>
                            <th>Fecha</th>
                            <th>Importe</th>
                            <th>Abonos</th>
                            <th>Nota</th>
                            <th>Estado</th>
                          </thead>
                          <tbody>                            
                          </tbody>
                          
                        </table>
                    </div>
                    <div class="panel-body table-responsive" id="listadoregistrosp">
                        <table id="tbllistadop" class="display compact nowrap">
                          <thead>
                            <th>Opciones</th>
                            <th>Folio</th>
                            <th>Fecha</th>
                            <th>Importe</th>
                            <th>Estado</th>
                          </thead>
                          <tbody>                            
                          </tbody>
                          
                        </table>
                    </div>
                    <div class="panel-body" style="height: 400px;" id="formularioregistros">
                        <form name="formulario" id="formulario" method="POST">
                          
                          <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                          	<input type="hidden" name="proveedor_id" id="proveedor_id" >
                            <label>id:</label>

                            <input type="text" class="form-control" readonly name="idsaldos" id="idsaldos" maxlength="10" placeholder="Id" required>
                          </div>

                          <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label>fecha:</label>
                            <input type="date" class="form-control" name="fecha" id="fecha" maxlength="10" placeholder="Fecha" required>
                          </div>
                          <div class="form-group col-lg-2 col-md-12 col-sm-12 col-xs-12">
                            <label>Importe</label>
                            <input type="number" step="0.01" class="form-control" name="importe" id="importe" maxlength="10" placeholder="Importe"  required>
                          </div>

                          

                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>nota:</label>
                            <input type="text" class="form-control" name="nota" id="nota" maxlength="50" placeholder="Nota"  required>
                          </div>
                          


                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>

                            <button class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
                          </div>
                        </form>
                    </div>
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
    

  

  <div class="modal fade" id="myModalBusquedaPersona" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
      <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Selecionar tipo de busqueda de persona </h4>
            </div>
            <div class="modal-body">
              
                <tbody>
                    <div class="panel-body" id="formularioPer" >
                      <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <input type="radio" onchange="checarRadioButton(0)" class="form-check-input" id="rbidpersona" name="rbpersonas" value="0" >
                          <label class="form-check-label" for="materialchecked">Buscar por Id</label>  
                      </div>  
                      <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <input type="radio" onchange="checarRadioButton(1)" class="form-check-input" id="rbrfcp"  name="rbpersonas" value="1" >
                          <label class="form-check-label" for="materialchecked">Buscar por Rfc</label>
                      </div>      
                      <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <input type="radio" onchange="checarRadioButton(2)" class="form-check-input" id="rbnombrep" name="rbpersonas"  value="2">
                          <label class="form-check-label" for="materialchecked">Buscar por Nombre</label>
                      </div>
                      <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">  
                          <input type="radio" onchange="checarRadioButton(3)" class="form-check-input" id="rbtelefonop" name="rbpersonas"  value="3" >
                          <label class="form-check-label" for="materialchecked">Buscar por Telefono</label> 
                      </div>
                  </div>
                </tbody>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>        
          </div>
      </div>
    </div>

  <!--Fin-Contenido-->
<?php
}
else
{
  require 'noacceso.php';
}
require 'footer.php';
?>
<script type="text/javascript" src="scripts/saldos_a_favor.js"></script>
<?php 
}
ob_end_flush();
?>