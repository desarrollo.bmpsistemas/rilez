<?php
//activamos el almaceneamiento en el buffer   
ob_start();
session_start(); 

if (!isset($_SESSION["nombre"])) 
{
  header("location: login.html");
}
else
{
require 'header.php';
if (isset($_SESSION['capturista_id']) && $_SESSION['capturista_id']>0)
{

?>
<!--Contenido--> 
      <!-- Content Wrapper. Contains page content -->
      <style>
        
     </style>

      
      <div class="content-wrapper">        
        <!-- Main content onfocusin="focusPrincipal()" -->
        <section class="content">
            <div class="row" style="height: 1000px;">
              
              <div class="col-md-12" id="divPrincipal" >
                  <div id="box" class="box">
                    <div id="header" class="box-header with-border">
                        
                      <input type="hidden" name="idusuarios" id="idusuarios" value="<?php echo $_SESSION['capturista_id']; ?> ">
                      
          
                      <div class="row">
                        <div class="form-group col-lg-2 col-md-4 col-sm-6 col-xs-12">
                            <label>Fecha:</label>
                            <input type="date" class="form-control" name="fecha" id="fecha" maxlength="10" placeholder="Fecha" required>
                        </div>
                         <div class="form-group col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <input type="hidden" name="Sidalmacenes" id="Sidalmacenes" value="<?php echo $_SESSION['almacen_id']; ?> ">
                            <input type="hidden" name="Srol" id="Srol" value="<?php echo $_SESSION['rol']; ?> ">
                            <input type="hidden" name="Sdescuento" id="Sdescuento" value="<?php echo $_SESSION['descuento']; ?> ">
                            <label>Selecione Almacen: </label> 
                            <select onchange="tomarIdDeAlmacen(1)"  id="idalmacenes" name="idalmacenes" class="form-control selectpicker"  data-live-search="true" required  >
                            </select>
                        </div> 
                        
                       
                         <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label >Iva</label>
                            <input type="checkbox" onchange="checarCheckBoxButtonIva(1)" name="chbIva" id="chbIva" value="0.16"  data-placement="left" title="Listar catalogo de iva " >
                            <label for="chbiva">Listar</label>
                            <div class="input-group">
                            <select  id="idivas" name="idivas" class="form-control selectpicker" value="idivas" data-live-search="true"></select>
                            <div class="input-group-btn">
                              <button class="btn btn-default" data-toggle="dropdown"  data-placement="left" title="Procesar iva " onclick="checarCheckBoxButtonIva(2)" type="button"> <i class="fa fa-search-plus" aria-hidden="true" ></i> </button>
                            </div>
                          </div>
                        </div>
                        

                        <div class="form-group col-lg-2 col-md-6 col-sm-12 col-xs-12">
                          <label>Monedas </label> <!--mostrarMovEntSal -->
                          <div class="input-group">
                            <select  id="idmonedas"  name="idmonedas" class="form-control selectpicker"  data-live-search="true" required  > </select>
                            <div class="input-group-btn">
                              <button class="btn btn-default" data-toggle="dropdown"  data-placement="left" title="Procesar moneda " onclick="procesarMoneda()" type="button"> <i class="fa fa-search-plus" aria-hidden="true" ></i> </button>
                            </div>
                          </div>
                        </div>
                        <!--<div class="form-group col-lg-1 col-md-6 col-sm-6 col-xs-12">
                            <label >Iva  </label>
                            <input type="text" class="form-control" name="iva" id="iva" maxlength="4" placeholder="Iva">
                        </div>--> 
                         
                      </div>
                          
                            <div id="cargando"></div>
                        
                      <div class="row">
                         <div   class="form-check form-group col-lg-3 col-md-6 col-sm-8 col-xs-12">
                         <label>Busqueda de Persona </label> <!--mostrarMovEntSal -->
                          
                          <div class="input-group">
                            <div class="input-group-btn"> 
                              <button onclick="MostrarModalOpcionPersona()" class="btn btn-default" data-toggle="dropdown"  data-placement="left" title="Selecione tipo de busqueda"  type="radio"> <i class="fa fa-question-circle" aria-hidden="true"></i></button>
                            </div>
                           
                            <input type="text" class="form-control" placeholder="Search"  name="busquedap" id="busquedap" maxlength="100" placeholder="rfc-nombre-tel" required>
                            <div class="input-group-btn">
                              <button class="btn btn-default" onclick="buscarPersonas(1)" type="button"> <i class="fa fa-search-plus" aria-hidden="true"></i></button>
                            </div>
                          </div>
                      </div>

                        
                        <!--<div class="btn-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                          <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" data-toggle="tooltip" data-placement="center" title="De click aqui para opciones ">Opciones <span class="caret"></span></button>
                          <ul class="dropdown-menu" role="menu" >
                            <li onclick="listar(0)"><a href="#">Buscar Personas</a></li>
                            <li onclick="mostrarform(true)"><a href="#">Agregar Personas</a></li>
                          </ul>
                        </div>--> 

                        <div class="form-group col-lg-4 col-md-12 col-sm-12 col-xs-12">
                            <label id="lblPersona" name="lblPersona">Selecione Persona: </label> <!--mostrarMovEntSal -->
                            <select onchange="datosDeLaPersona()"  id="idpersonas"  name="idpersonas" class="form-control selectpicker"  data-live-search="true" required  >
                            </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-12 col-sm-12 col-xs-12">
                          <label>Adeudos </label> <!--mostrarMovEntSal -->
                          <div class="input-group">
                            <select  id="adeudos"  name="adeudos" class="form-control selectpicker"  data-live-search="true" required  > </select>
                            <div class="input-group-btn">
                              <button class="btn btn-default" data-toggle="dropdown"  data-placement="left" title="Pida autorizacion para venta" onclick="pedirAutorizacion()" type="button"> <i class="fa fa-search-plus" aria-hidden="true" ></i> </button>
                            </div>
                          </div>
                        </div>

                      </div>

                      <div class="row">
                        <div class="form-group col-lg-5 col-md-4 col-sm-6 col-xs-12">
                            <label>Lineas:</label>
                            <select id="idlineas" onchange="selectsublinea()" name="idlineas" class="form-control selectpicker"  data-live-search="true" required >
                            </select>
                        </div>
                        <div class="form-group col-lg-5 col-md-4 col-sm-6 col-xs-12">
                            <label>SubLineas:</label>
                            <select id="idsublineas" onchange="selectarticulosporsublinea()" name="idsublineas" class="form-control selectpicker"  data-live-search="true" required >
                            </select>
                        </div>
                        <div  class="form-group col-lg-2 col-md-4 col-sm-12 col-xs-12">
                            <label>Exist.:</label>
                            <input type="number"  class="form-control" name="existencia" id="existencia" maxlength="10" placeholder="Existencia" required>
                        </div>
                      </div>


                      <!-- Default unchecked id="div" -->


                     

                      <div class="row">
                        <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                         <label>Producto</label> <!--mostrarMovEntSal -->
                          <div class="input-group">
                            <div class="input-group-btn"> 
                              <button onclick="MostrarModalOpcionProducto()" class="btn btn-default" data-toggle="dropdown"  data-placement="left" title="Selecione tipo de busqueda"  type="radio"> <i class="fa fa-question-circle" aria-hidden="true"></i></button>
                            </div>
                           
                            <input type="text" onfocusin="checarCheckBoxButtonArt(1)" class="form-control" placeholder="Search"  name="busquedaart" id="busquedaart" maxlength="100" placeholder="Buscar producto" required>
                            <div class="input-group-btn">
                              <button class="btn btn-default" onclick="buscarProductos()" type="button"> <i class="fa fa-search-plus" aria-hidden="true"></i></button>
                            </div>
                          </div> 
                        </div> 
                        


                        <div id="divArticulos" class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                          <input type="checkbox" onchange="checarCheckBoxButtonIva(3)" name="chbAnexo" id="chbAnexo"  data-placement="left" title="Agregar Concepto " >
                          <label for="chbAnexo">Articulos (Formato Libre?)</label>
                           <select id="idarticulos" onchange="buscarProducto(1)" name="idarticulos" class="form-control selectpicker" value="idarticulos" data-live-search="true" required > </select>
                        </div>

                        <div id="divConcepto">
                          <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <input type="checkbox" onchange="checarCheckBoxButtonIva(4)" name="chbAnexo" id="chbAnexo"  data-placement="left" title="Selecionar Concepto " >
                            <label>Descripcion libre</label>
                            <textarea class="form-control" rows="3" name="concepto" id="concepto"></textarea>
                          </div>
                        </div> 
                        
                        <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                              <label onclick="pedirAutorizacion(1)">Valor(?):</label>
                              <input type="number" step="0.01" class="form-control" name="valor" id="valor" maxlength="10" placeholder="Valor" required>
                        </div>
                         
                          
                        <div  class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label>Cantidad</label>
                            <input type="number" step="0.01" class="form-control" name="cantidad" id="cantidad" maxlength="10" placeholder="Cantidad" required>
                        </div>
                        
                        
                        <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                         <label>Descto. </label> <!--mostrarMovEntSal -->
                          <div class="input-group">
                           
                            <input type="number" step="0.01"   class="form-control"  name="descuento" id="descuento" maxlength="4" placeholder="Descto" required>
                            <div class="input-group-btn">
                              <button class="btn btn-default" onclick="agregarMovPedido(1)" type="button"> <i class="fa fa-save" aria-hidden="true"></i></button>
                            </div>
                            
                          </div>
                        </div> 
  
                      </div>  

                      
                    

                        <!-- /.box-header class="table table-striped table-bordered table-condensed table-hover" -->
                        <!-- centro -->
                        <div class="panel-body table-responsive" id="listadoregistros">
                            <table id="tbllistado" class="display compact nowrap">
                              <thead>
                                <th>Opciones</th>
                                <th>Codigo</th>
                                <th>Descripcion</th>
                                <th>Valor</th>
                                <th>Descto</th>
                                <th>Moneda</th>
                                <th>Iva</th>
                                <th>Cantidad</th>
                                <th>Monto</th>
                              </thead>
                              <tbody>                            
                              </tbody>
                              <tfoot>
                                <th>Opciones</th>
                                <th>Codigo</th>
                                <th>Descripcion</th>
                                <th>Valor</th>
                                <th>Descto</th>
                                <th>Moneda</th>
                                <th>Iva</th>
                                <th>Cantidad</th>
                                <th>Monto</th>
                              </tfoot>
                            </table>
                        </div>
                        <div  class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-12">
                              <label>Sub Total:</label>
                              <input type="number" step="0.01" class="form-control" name="sumaimporte" id="sumaimporte" maxlength="10" placeholder="SubTotal" required>
                          </div>
                          <div  class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-12">
                              <label>Suma Iva:</label>
                              <input type="number" step="0.01" class="form-control" name="sumaiva" id="sumaiva" maxlength="10" placeholder="Suma Iva" required>
                          </div>
                          <div  class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-12">
                              <label>Suma Total:</label>
                              <input type="number" step="0.01" class="form-control" name="sumatotal" id="sumatotal" maxlength="10" placeholder="Suma Total" required>
                          </div>
                          <div  class="form-group col-lg-2 col-md-4 col-sm-4 col-xs-12">
                              <label>Folio:</label>
                              <input type="number" class="form-control" name="folioES" id="folioES" maxlength="10" placeholder="Folio" required>
                          </div>
                          
                        </div>  


                      <button type="button" data-toggle="collapse" class="btn btn-primary btn-lg btn-block"  data-target="#clientes"> Agregar Clientes</button>
                      <div id="clientes" class="collapse">
                            <form name="formularioCte" id="formularioCte" method="POST">
                          
                          <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label>id:</label>

                            <input type="text" class="form-control" readonly name="idCte" id="idCte" maxlength="10" placeholder="Id" required>
                          </div>

                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Nombre:</label>
                            <input type="text" class="form-control" name="nombreCte" id="nombreCte" maxlength="50" placeholder="Nombre" required>
                          </div>
                          <div class="form-group col-lg-4 col-md-12 col-sm-12 col-xs-12">
                            <label>Nombre Comercial:</label>
                            <input type="text" class="form-control" name="nombrecomercialCte" id="nombrecomercialCte" maxlength="100" placeholder="Nombre Comercial"  required>
                          </div>

                          

                          <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>RFC:</label>
                            <input type="text" class="form-control" name="rfcCte" id="rfcCte" maxlength="15" placeholder="Rfc"  required>
                          </div>
                          
                          <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>Teléfono:</label>
                            <input type="text" class="form-control" name="telefonoCte" id="telefonoCte" maxlength="30" placeholder="Teléfono"  required>
                          </div>

                          <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>Correo:</label>
                            <input type="email" class="form-control" name="correoCte" id="correoCte" maxlength="50" placeholder="Correo"  required>
                          </div>

                           <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>Vendedores(*):</label>
                            <select id="idvendedoresCte" name="idvendedoresCte" class="form-control selectpicker" data-live-search="true" required></select>
                          </div>

                          <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label>Dias Credito:</label>
                            <input type="text" class="form-control" name="diascreditoCte" id="diascreditoCte" maxlength="50" placeholder="Dias de Credito">
                          </div>
                          <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label>Limite Credito:</label>
                            <input type="text" class="form-control" name="limitecreditoCte" id="limitecreditoCte" maxlength="50" placeholder="Limite de Credito"  required>
                          </div>
                          <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label>Descto Aut:</label>
                            <input type="number" step="0.01" onfocusout="checarDescuento()" class="form-control" step="0.00" name="descuentoCte" id="descuentoCte" maxlength="4" placeholder="Descuento Aut."  required>
                          </div>

                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>Referencia / Domicilio .:</label>
                            <textarea class="form-control rounded-0" id="referenciaCte" name="referenciaCte" rows="3"  required></textarea>
                          </div>
                          


                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <button class="btn btn-primary" onclick="guardaryeditarCte()" type="button" id="btnGuardarCte"><i class="fa fa-save"></i> Guardar</button>
                          </div>
                        </form>
                    </div>
                     

                    
                    <div class="panel-body" style="height: 1200px;" id="formularioregistros">
                         
                        <div class="form-group col-lg-5 col-md-4 col-sm-6 col-xs-12">
                            <input type="hidden" name="iddet_ent_sal" id="iddet_ent_sal" >
                            <label>Lineas:</label>
                            <select id="idlineas2" onchange="selectsublinea2()" name="idlineas2" class="form-control selectpicker"  data-live-search="true" required >
                            </select>
                        </div>
                        <div class="form-group col-lg-5 col-md-4 col-sm-6 col-xs-12">
                            <label>SubLineas:</label>
                            <select id="idsublineas2" onchange="selectarticulosporsublinea2()" name="idsublineas" class="form-control selectpicker"  data-live-search="true" required >
                            </select>
                        </div>
                        

                        <div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12">
                            <label> Articulos</label>
                            <select id="idarticulos2" onchange="buscarProducto(2)" name="idarticulos2" class="form-control selectpicker" value="idarticulos" data-live-search="true" required >
                            </select>
                        </div>
                        <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label onclick="autorizar(1)">Valor(?):</label>
                            <input type="number" step="0.01" class="form-control" name="valor2" id="valor2" maxlength="10" placeholder="Valor" required>
                        </div>
                        <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label >Iva  </label>
                            <select  id="idivas2" name="idivas2" class="form-control selectpicker" value="idivas2" data-live-search="true"  >
                            </select>
                        </div>  
                            
                        <div  class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label>Cantidad(*):</label>
                            <input type="number" step="0.01" class="form-control" name="cantidad2" id="cantidad2" maxlength="10" placeholder="Cantidad" required>
                        </div>


                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <button onclick="actualizarMovPedido()" class="btn btn-primary" id="btnGuardar2"><i class="fa fa-save"></i> Guardar</button>

                          <button class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
                        </div>
                      </form>
                    </div>
                    
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->


    


    <!--<div class="modal fade" id="myModalPedidos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
      <div class="modal-dialog" style="width: 80% !important;">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Entradas y Salidas de Movimientos </h4>
            </div>
            <div class="modal-body">
              
                <tbody>
                    <div class="panel-body" style="height: 80px;" id="formulariorPedidos">
                      
                        <a data-controls-modal="myModal" data-backdrop="static" data-keyboard="false" href="#">
                        
                        <div id="articulos" class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>Selecione Producto: </label>
                            <select onchange="buscarProducto(0)"  id="idarticulos" name="idarticulos" class="form-control selectpicker"  data-live-search="true" required  >
                            </select>
                        </div>  
                        <div id="costos" class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label>Valor(*):</label>
                            <input type="number" step="0.01" class="form-control" name="valor" id="valor" maxlength="10" placeholder="Valor" required>
                        </div>
                        <div id="articulos" class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label >Iva </label>
                            <select  id="idivas" name="idivas" class="form-control selectpicker" value="idivas" data-live-search="true" required onchange="buscarcodigo()" >
                            </select>
                        </div>  
                        <div id="cantidades" class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label id="lblCantidad">Existencia:</label>
                            <input type="number" step="0.01" class="form-control" name="existencia" id="existencia" maxlength="10" placeholder="Cantidad" required>
                        </div>
                        <div id="cantidades" class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label id="lblCantidad">Cantidad(*):</label>
                            <input type="number" step="0.01" class="form-control" name="cantidad" id="cantidad" maxlength="10" placeholder="Cantidad" required>
                        </div>

                        
                        <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                          <label id="lblCantidad">Grabar Producto</label>
                          <button  class="btn btn-primary" onclick="agregarMovPedido()" id="btnGuardarPedido"><i class="fa fa-save"></i> Guardar </button>
                        </div>
                      
                  </div>
                </tbody>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>        
          </div>
      </div>
    </div> -->

    

    <div class="modal fade" id="myModalEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
      <div class="modal-dialog" >
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Editar </h4>
            </div>
            <div class="modal-body">
              
                <tbody>
                    <div class="panel-body"  id="formulariorEditar">
                      
                        <a data-controls-modal="myModal" data-backdrop="static" data-keyboard="false" href="#">
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Cantidad:</label>
                            <input type="number" step="0.01" class="form-control" name="Cantidad" id="Cantidad" maxlength="10" placeholder="Cantidad" required >
                        </div>
                        
                        
                        <div class="form-group col-lg-4 col-md-12 col-sm-12 col-xs-9">
                          <label>Valor </label> <!--mostrarMovEntSal -->
                          <div class="input-group">
                            <input type="number"  class="form-control" name="Valor" id="Valor" maxlength="15" placeholder="Precio" required >
                            <div class="input-group-btn">
                              <button class="btn btn-default" data-toggle="dropdown"  data-placement="left" title="Pida autorizacion " onclick="pedirAutorizacion()" type="button"> <i class="fa fa-search-plus" aria-hidden="true" ></i> </button>
                            </div>
                          </div>
                        </div>

                      
                        
                        
                        
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <label id="lblCantidad">Actualizar</label>
                          <button  class="btn btn-primary" onclick="actualizarCantidad()" id="btnActCantidad"><i class="fa fa-save"></i> Actualizar </button>
                        </div>
                      
                  </div>
                </tbody>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>        
          </div>
      </div>
    </div>

    




    <div class="modal fade" id="myModalBusquedaPersona" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
      <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Selecionar tipo de busqueda de persona </h4>
            </div>
            <div class="modal-body">
              
                <tbody>
                    <div class="panel-body" id="formularioPer" >
                      <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <input type="radio" onchange="checarRadioButton(0)" class="form-check-input" id="rbidpersona" name="rbidpersona" value="0" >
                          <label class="form-check-label" for="materialchecked">Buscar por Id</label>  
                      </div>  
                      <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <input type="radio" onchange="checarRadioButton(1)" class="form-check-input" id="rbrfcp" name="rbrfcp" value="1" >
                          <label class="form-check-label" for="materialchecked">Buscar por Rfc</label>
                      </div>      
                      <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <input type="radio" onchange="checarRadioButton(2)" class="form-check-input" id="rbnombrep" name="rbnombrep" value="2">
                          <label class="form-check-label" for="materialchecked">Buscar por Nombre</label>
                      </div>
                      <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">  
                          <input type="radio" onchange="checarRadioButton(3)" class="form-check-input" id="rbtelefonop" name="rbtelefonop" value="3" >
                          <label class="form-check-label" for="materialchecked">Buscar por Telefono</label> 
                      </div>
                  </div>
                </tbody>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>        
          </div>
      </div>
    </div>


    <div class="modal fade" id="myModalBusquedaProducto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
      <div class="modal-dialog" >
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Selecionar tipo de busqueda  en productos</h4>
            </div>
            <div class="modal-body">
              
                <tbody>
                  <form id="myForm">
                  <div class="panel-body"  id="formulariorProd">
                      <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <input type="radio" onchange="checarRadioButtonArt(1)" class="form-check-input" id="rbidart" name="rbid" value="1" >
                      <label class="form-check-label" for="materialchecked">Buscar por Id</label>
                      </div>  
                      <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <input type="radio" onchange="checarRadioButtonArt(2)" class="form-check-input" id="rbnombreart" name="rbnombreart" value="2" >
                      <label class="form-check-label" for="materialchecked">Buscar por Nombre</label>
                      </div>
                      <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <input type="checkbox" onchange="checarRadioButtonArt(3)" class="form-check-input" id="chbArtAut" name="chbArtAut" value="3" >
                      <label class="form-check-label" for="materialchecked">Buscar Id Automatico</label>
                      </div>        
                      
                  </div>
                  </form>
                    
                </tbody>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>        
          </div>
      </div>
    </div>



    <div class="modal fade" id="myModalAnexos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
      <div class="modal-dialog" >
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Anexos </h4>
            </div>
            <div class="modal-body">
              
                <tbody>
                  <div class="panel-body"  id="formularior_Anexo">
                  <form name="formularioAnexo" id="formularioAnexo" method="POST">
                      <a data-controls-modal="myModal" data-backdrop="static" data-keyboard="false" href="#"></a>

                       <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label >Folio</label>
                            <div class="input-group">
                            <input type="number"  class="form-control" name="folio" id="folio" maxlength="100" placeholder="Folio" required>
                            <div class="input-group-btn">
                              <button class="btn btn-default" data-toggle="dropdown"  data-placement="left" title="Buscar Folio " id="btnBuscarAnexo" onclick="buscarAnexo()" type="button"> <i class="fa fa-search-plus" aria-hidden="true" ></i> </button>
                            </div>
                          </div>
                        </div>


                      <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>Cliente(*):</label>
                            <input type="text"  class="form-control" name="cliente" id="cliente" maxlength="100" placeholder="Cliente" required>
                      </div>
                      <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>Con Atencion(*):</label>
                            <input type="text"  class="form-control" name="conatencion" id="conatencion" maxlength="100" placeholder="Con Atencion" required>
                      </div>
                      <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>Asunto(*):</label>
                            <textarea class="form-control rounded-0" name="asunto" id="asunto" rows="3"></textarea>
                      </div>
                      <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Imagen:</label>
                            <input type="file" class="form-control" name="imagen" id="imagen">
                            <input type="hidden" name="imagenactual" id="imagenactual">
                            <img src="" width="150px" height="120px" id="imagenmuestra">
                      </div>
                      <!--onclick="guardaryeditarAnexo()"  onclick="enviarAnexo()"  -->
                      <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                          
                          <button type="submit" class="btn btn-primary"  id="btnGrabarAnexo"><i class="fa fa-save"></i> Grabar </button>
                        </div>
                  </form>  

                      
                  </div>
                </tbody>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>        
          </div>
      </div>
    </div>


  <div class="modal fade" id="myModalVerPedidos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
      <div class="modal-dialog" >
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Migrar Documentos</h4>
            </div>
            <div class="modal-body">
              
                <tbody>
                    <div class="panel-body"  id="formulariorVerPedidos">
                      
                        <a data-controls-modal="myModal" data-backdrop="static" data-keyboard="false" href="#">
                        
                        
                        <div  class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12">
                            <label>Fecha Inicial:</label>
                            <input  type="date" class="form-control" name="fecha_inicial" id="fecha_inicial" maxlength="10" placeholder="Fecha Inicial" required>
                        </div>
                        <div  class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12">
                            <label>Fecha Final:</label>
                            <input  type="date" class="form-control" name="fecha_final" id="fecha_final" maxlength="10" placeholder="Fecha Final" required>
                        </div>
                        <div class="form-group col-lg-2 col-md-3 col-sm-3 col-xs-3">
                            <label>Buscar:</label>
                            <button type="button" class="btn btn-brand btn-yahoo" id="btnBuscarDocto" onclick="buscarEntSal()"> <i class="fa fa-search-plus" aria-hidden="true"></i>Buscar</button>
                        </div>
                        <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label>Tipo De Precio:(?)</label>
                             <select class="form-control select-picker" name="tipodeprecio" id="tipodeprecio"  required>
                              <option value="actual">Actual</option>
                              <option value="historico">Historico</option>
                            </select>
                        </div>

                        <div id="articulos" class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label >Selecione Documento  </label>
                            <select  id="ident_sal" name="ident_sal" class="form-control selectpicker"  data-live-search="true" >
                            </select>
                        </div>  

                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <label>Migrar Documento  </label>
                          <button  class="btn btn-primary" onclick="tomarEntSal()" id="btnMigrarPedido"><i class="fa fa-save"></i> Aceptar </button>
                        </div>

                      
                  </div>
                </tbody>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>        
          </div>
      </div>
  </div> 

  <div class="modal fade" id="myModalDestino" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
      <div class="modal-dialog" >
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Destino del Traspaso</h4>
            </div>
            <div class="modal-body">
              
                <tbody>
                    <div class="panel-body"  id="formulariorVerPedidos">
                      
                        <a data-controls-modal="myModal" data-backdrop="static" data-keyboard="false" href="#">
                        <div class="row">
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>Selecione Destino: </label> 
                            <select onchange="tomarIdDeAlmacen(2)"  id="idalmacenesD" name="idalmacenesD" class="form-control selectpicker"  data-live-search="true" required  >
                            </select>
                        </div> 
                        
                         <div   class="form-check form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <label>Busqueda de receptor </label> <!--mostrarMovEntSal -->
                            <div class="input-group">
                              
                             
                              <input type="text" class="form-control"   name="receptor" id="receptor" value="PROVEEDORA DE MATERIAL DE COBRE" maxlength="100"  required>
                              <div class="input-group-btn">
                                <button class="btn btn-default" onclick="buscarPersonas(2)" type="button"> <i class="fa fa-search-plus" aria-hidden="true"></i></button>
                              </div>
                            </div>
                          </div>

                        

                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>Selecione Destino: </label> 
                            <select  id="idreceptores" name="idreceptores" class="form-control selectpicker"  data-live-search="true" required  >
                            </select>
                        </div> 
                        

                        <!--<div class="form-group col-lg-2 col-md-12 col-sm-12 col-xs-12">
                          <label>Aceptar  </label>
                          <button  class="btn btn-primary" onclick="destinoEntSal()" id="btnMigrarPedido"><i class="fa fa-save"></i> Aceptar </button>
                        </div>-->

                      
                  </div>
                </tbody>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>        
          </div>
      </div>
    </div>
  </div> 


  <div class="modal fade" id="myModalAutorizar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
      <div class="modal-dialog" >
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Autorizacion</h4>
            </div>
            <div class="modal-body">
              
                <tbody>
                    <div class="panel-body" style="height: 80px;" id="formulariorAutorizacion">
                      
                        <a data-controls-modal="myModal" data-backdrop="static" data-keyboard="false" href="#">
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Usuario(*):</label>
                            <input type="Password" class="form-control" name="login" id="login" maxlength="50" placeholder="Usuario" required >
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Password(*):</label>
                            <input type="password"  class="form-control" name="clave" id="clave" maxlength="15" placeholder="Clave" required >
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Autorizar Precio</label>
                            <input type="checkbox"  name="chbAutPrecio" id="chbAutPrecio" >
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Autorizar Venta</label>
                            <input type="checkbox"  name="chbAutVenta" id="chbAutVenta"  >  
                        </div>
                        
                        
                        
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <label id="lblCantidad">Autorizar</label>
                          <button  class="btn btn-primary" onclick="autorizar(1)" id="btnBuscarAutorizar"><i class="fa fa-save"></i> Buscar </button>
                        </div>
                      
                  </div>
                </tbody>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>        
          </div>
      </div>
    </div>

  <div class="modal fade" id="myModalTimbrar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
      <div class="modal-dialog" style="width: 80% !important;">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Timbrar</h4>
            </div>
            <div class="modal-body">
              
                <tbody>
                    <div class="panel-body" style="height: 150px;" id="formulariorVerPedidos">
                      
                        <a data-controls-modal="myModal" data-backdrop="static" data-keyboard="false" href="#">

                        <div class="form-group col-lg-1 col-md-12 col-sm-12 col-xs-12">
                            <label>Catalogos:</label>
                            <button class="btn btn-brand btn-yahoo" id="btnBuscarCatSal" onclick="cargarCatalogosSat()"> <i class="fa fa-search-plus" aria-hidden="true"></i>Cargar</button>
                        </div>
                       
                        <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label>Fecha Inicial:</label>
                            <input type="date"  class="form-control" name="fecha_inicialt" id="fecha_inicialt" maxlength="10" placeholder="Fecha inicial" required>
                        </div>
                        <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label>Fecha Final:</label>
                            <input type="date"  class="form-control" name="fecha_finalt" id="fecha_finalt" maxlength="10" placeholder="Fecha inicial" required>
                        </div>
                        <div class="form-group col-lg-1 col-md-6 col-sm-6 col-xs-12">
                          <label id="lblCantidad">Folios</label>
                          <button  class="btn btn-primary" onclick="buscarDocumentosParaTimbrar()" id="btnBuscart"><i class="fa fa-search-plus"></i> Buscar </button>
                        </div>
                        <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label>Documentos: </label> 
                            <select  id="iddocumentos" name="iddocumentos" class="form-control selectpicker"  data-live-search="true" required  >
                            </select>
                        </div> 
                         <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label>Folio(*):</label>
                            <input type="number"  class="form-control" name="foliot" id="foliot" maxlength="100" placeholder="Folio" required>
                        </div>

                        <div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <label>Forma de Pago: </label> 
                            <select  id="idsat_forma_de_pagos" name="idsat_forma_de_pagos" class="form-control selectpicker"  data-live-search="true" required  >
                            </select>
                        </div> 
                        <div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <label>Impuestos: </label> 
                            <select  id="idsat_impuestos" name="idsat_impuestos" class="form-control selectpicker"  data-live-search="true" required  >
                            </select>
                        </div> 
                        <div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <label>Metodo de Pago: </label> 
                            <select  id="idsat_metodo_de_pagos" name="idsat_metodo_de_pagos" class="form-control selectpicker"  data-live-search="true" required  >
                            </select>
                        </div> 
                        <div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <label>Moneda: </label> 
                            <select  id="idsat_monedas" name="idsat_monedas" class="form-control selectpicker"  data-live-search="true" required  >
                            </select>
                        </div> 
                        <div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <label>Comprobante: </label> 
                            <select  id="idsat_tipo_de_comprobantes" name="idsat_tipo_de_comprobantes" class="form-control selectpicker"  data-live-search="true" required  >
                            </select>
                        </div> 
                        <div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <label>Factor: </label> 
                            <select  id="idsat_tipo_factores" name="idsat_tipo_factores" class="form-control selectpicker"  data-live-search="true" required  >
                            </select>
                        </div> 
                        <div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <label>Tasa o Cuota: </label> 
                            <select  id="idsat_tasaocuotas" name="idsat_tasaocuotas" class="form-control selectpicker"  data-live-search="true" required  >
                            </select>
                        </div> 
                        <div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <label>Tipo de Relacion: </label> 
                            <select  id="idsat_tiporelaciones" name="idsat_tiporelaciones" class="form-control selectpicker"  data-live-search="true" required  >
                            </select>
                        </div> 
                        <div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <label>Uso de Cfdi: </label> 
                            <select  id="idsat_usocfdis" name="idsat_usocfdis" class="form-control selectpicker"  data-live-search="true" required  >
                            </select>
                        </div> 

                        <div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12">
                          <label>Aceptar  </label>
                          <button  class="btn btn-primary" onclick="timbrarSalida()" id="btnMigrarPedido"> Timbrar </button>
                        </div>

                      
                  </div>
                </tbody>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>        
          </div>
      </div>
  </div>
  

  <div class="modal fade" id="myModalTerminar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
      <div class="modal-dialog" style="width: 80% !important;">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Terminar Documento</h4>
            </div>
            <div class="modal-body">
              
                <tbody>
                    <div class="panel-body" style="height: 150px;" id="formulariorTerminar">
                      
                        <div class="row">
                        <a data-controls-modal="myModal" data-backdrop="static" data-keyboard="false" href="#" >
                        <div  class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>Referencia / Orden de Compra</label>
                            <input  type="text"  class="form-control" name="referencia" id="referencia" maxlength="20" value="S/R" placeholder="Referencia" required>
                        </div>
                        
                        <div  class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>Total Docto.</label>
                            <input type="number" step="0.01" class="form-control" value="0" name="totaldocumento" id="totaldocumento" maxlength="10" placeholder="Total" required>
                        </div>
                        <div  class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>Forma de pago</label>
                            <select  class="form-control select-picker" name="idformadepago" id="idformadepago"  required>
                              <option value="1">Contado</option>
                              <option value="2">Credito</option>
                            </select>
                        </div>
                        <div  class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>Vencimiento</label>
                            <input type="date"  class="form-control" name="vencimiento" id="vencimiento" maxlength="10" placeholder="Vencimiento" required>
                        </div>
                      </div> 
                      <div class="row"> 
                        <div  class="form-group col-lg-8 col-md-8 col-sm-12 col-xs-12">
                            <label>Observaciones(*)</label>
                            <input type="text"  class="form-control" value=" " name="observaciones" id="observaciones" maxlength="100" placeholder="Observaciones" required>
                        </div>
                      </div>  
                       
                     
                      <div class="row">  
                         <div  class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>Subtotal(*)</label>
                            <input type="number" step="0.01" class="form-control" name="subtotal" id="subtotal" maxlength="10" placeholder="Subtotal" required>
                        </div>
                        <div  class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>iva(*)</label>
                            <input type="number" step="0.01" class="form-control" name="iva" id="iva" maxlength="10" placeholder="Iva" required>
                        </div>
                        <div  class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>Total(*)</label>
                            <input type="number" step="0.01" class="form-control" name="total" id="total" maxlength="10" placeholder="Total" required>
                        </div>
                        <div class="form-group col-lg-3 col-md- col-sm-6 col-xs-12">
                          <button  class="btn btn-primary" onclick="terminarEntSal()" id="btnGuardarPedido"><i class="fa fa-save"></i> Guardar </button>
                        </div>
                      </div>

                      
                  </div>
                </tbody>
            </div>
            <div class="modal-footer">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Cerrar</button>
            </div>        
          </div>
      </div>
  </div>--> 






  <!--Fin-Contenido-->
<?php
}
else
{
  require'noacceso.php'; 
}

require 'footer.php';
?>
<script type="text/javascript" src="scripts/salidasporajustes.js"></script>
<?php 
  }
  ob_end_flush();
?>