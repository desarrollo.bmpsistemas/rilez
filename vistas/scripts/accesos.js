var tabla;
var codigo=0;
//Función que se ejecuta al inicio 
function init(){
	 $("#modulo").html("Abc de Accesos");
	mostrarform(false);
	listar();

	$.post("../ajax/modulos.php?op=select", function(r){
	            $("#idmodulos").html(r);
	            $('#idmodulos').selectpicker('refresh');

	});
	$.post("../ajax/almacenes.php?op=select", function(r){
	            $("#idalmacenesa").html(r);
	            $('#idalmacenesa').selectpicker('refresh');

	});

	$.post("../ajax/usuarios.php?op=select", function(r){
	            $("#idusuarios").html(r);
	            $('#idusuarios').selectpicker('refresh');
	            $("#idusuario").html(r);
	            $('#idusuario').selectpicker('refresh');

	});

	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	})
}

//Función limpiar
function limpiar()
{
	//$("#nombre").val("");
	//$("#idaccesos").val("");
	//$("#idmenus").val("");
	//$("#idusuarios").val("");
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
	}
}

function asignarAlmacenes()
{
	$("#myModalAlmacenes").modal("show");	
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}
function buscarcodigo()
{
	var codigo=$('select[id=idusuario]').val();
	$('#idusuarios').val(codigo);
	
	
	$.post("../ajax/almacenes.php?op=selectAlmacenesPorUsuario",{idusuarios:codigo}, function(data, status)
	{
		$("#idalmacenesq").html(data);
	    $('#idalmacenesq').selectpicker('refresh');

 	})
 	listar(codigo);
	
}

//Función Listar
function listar(codigo)
{
	tabla=$('#tbllistado').dataTable(
	{
		"scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false,

		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'

		            
		        ],
		"ajax":
				{
					url: '../ajax/accesos.php?op=listar',
					data:{idusuarios:codigo},
					type : "get",
					dataType : "json",						
					error: function(e){

						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar

function guardaryeditar(e)
{
	idaccesos=$("#idaccesos").val();
	idmodulos=$("#idmodulos").val();
	idusuarios=$("#idusuarios").val();
	
	$.post("../ajax/accesos.php?op=guardaryeditar",{idaccesos : idaccesos,idmodulos:idmodulos,idusuarios:idusuarios}, function(data, status)
	{
		alert(" "+data);		
 		//generarbarcode();

 	})
	
}
function agregarAlmacenAlUsuario()
{
	
	idalmacenesa=$("#idalmacenesa").val();
	idusuarios=$("#idusuarios").val();
	
	$.post("../ajax/almacenes.php?op=agregarAlmacenAlUsuario",{idalmacenes:idalmacenesa,idusuarios:idusuarios}, function(data, status)
	{
		//alert(" "+data);
		$.post("../ajax/almacenes.php?op=selectAlmacenesPorUsuario",{idusuarios:idusuarios}, function(data, status)
		{
			$("#idalmacenesq").html(data);
		    $('#idalmacenesq').selectpicker('refresh');

	 	});

 	})
	
}
function quitarAlmacenAlUsuario()
{
	
	idalmacenesq=$("#idalmacenesq").val();
	idusuarios=$("#idusuarios").val();
	
	$.post("../ajax/almacenes.php?op=quitarAlmacenAlUsuario",{idalmacenes:idalmacenesq,idusuarios:idusuarios}, function(data, status)
	{
		//alert(""+data);		
		
			$.post("../ajax/almacenes.php?op=selectAlmacenesPorUsuario",{idusuarios:idusuarios}, function(data, status)
			{
				$("#idalmacenesq").html(data);
			    $('#idalmacenesq').selectpicker('refresh');

		 	});

	});
	
}


function mostrar(idaccesos)
{
	$.post("../ajax/accesos.php?op=mostrar",{idaccesos : idaccesos}, function(data, status)
	{
		data = JSON.parse(data);		
		mostrarform(true);
		
		$("#idmodulos").val(data.idmodulos);
		$('#idmodulos').selectpicker('refresh');
		$("#modulo").val(data.modulo);

		
 		//generarbarcode();

 	})
}

//Función para desactivar registros
function desactivar(idaccesos)
{
	bootbox.confirm("¿Está Seguro de desactivar la accesos?", function(result){
		if(result)
        {
        	$.post("../ajax/accesos.php?op=desactivar", {idaccesos : idaccesos}, function(e){
        		//bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(idaccesos)
{
	bootbox.confirm("¿Está Seguro de activar la linea?", function(result){
		if(result)
        {
        	$.post("../ajax/accesos.php?op=activar", {idaccesos : idaccesos}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

function selectmodulos()
{
	idmenus=$('select[id=idmenus]').val();
	//Cargamos los items al select estados
	//alert("id menus "+idmenus);
	$.post("../ajax/modulos.php?op=selectpormenu", {idmenus : idmenus}, function(r){
		//alert("resultado "+r);
	            $("#idmodulos").html(r);
	            $('#idmodulos').selectpicker('refresh');

	});
}

init();