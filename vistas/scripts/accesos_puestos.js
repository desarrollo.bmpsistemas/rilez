var tabla;
var codigo=0;
//Función que se ejecuta al inicio
function init(){
	 $("#modulo").html("Abc de Acceso a Puestos");
	mostrarform(false);
	listar(codigo);

	$.post("../ajax/puestos.php?op=select", function(r){
	            $("#idpuesto").html(r);
	            $('#idpuesto').selectpicker('refresh');

	});
	$.post("../ajax/modulos.php?op=select", function(r){
	            $("#idmodulos").html(r);
	            $('#idmodulos').selectpicker('refresh');

	});

	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	})
}

//Función limpiar
function limpiar()
{
	//$("#nombre").val("");
	$("#idaccesos_puestos").val("");
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false); 
}
function buscarcodigo()
{
	codigo=$('select[id=idpuesto]').val();
	$('#idpuestos').val(codigo);
	
	listar(codigo);
	
	
}
//Función Listar
function listar(codigo)
{
	
	

	tabla=$('#tbllistado').dataTable(
	{
		"scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false,
        
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],
		"ajax":
				{
					url: '../ajax/accesos_puestos.php?op=listar',
					data:{idpuestos:codigo},
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar

function guardaryeditar(e)
{
	e.preventDefault(); //No se activará la acción predeterminada del evento
	$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]);
	var dataString = $('#formulario').serialize();
    //alert('Datos del formulario: '+dataString);

	$.ajax({
		url: "../ajax/accesos_puestos.php?op=guardaryeditar",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {                    
	          bootbox.alert(datos);	          
	          mostrarform(false);
	          tabla.ajax.reload();
	    }

	});
	limpiar();
}

function mostrar(idaccesos_puestos)
{
	$.post("../ajax/accesos_puestos.php?op=mostrar",{idaccesos_puestos : idaccesos_puestos}, function(data, status)
	{
		data = JSON.parse(data);		
		mostrarform(true);
		$("#idaccesos_puestos").val(data.idaccesos_puestos);
		$("#idpuestos").val(data.idpuestos);
		$('#idpuestos').selectpicker('refresh');
		$("#idmodulos").val(data.idmodulos);
		$('#idmodulos').selectpicker('refresh');


 	})
}

//Función para desactivar registros
function desactivar(idaccesos_puestos)
{
	bootbox.confirm("¿Está Seguro de desactivar  Accesos de Puestos", function(result){
		if(result)
        {
        	$.post("../ajax/accesos_puestos.php?op=desactivar", {idaccesos_puestos : idaccesos_puestos}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(idaccesos_puestos)
{
	bootbox.confirm("¿Está Seguro de activar la Accesos de Puestos?", function(result){
		if(result)
        {
        	$.post("../ajax/accesos_puestos.php?op=activar", {idaccesos_puestos : idaccesos_puestos}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}


init();