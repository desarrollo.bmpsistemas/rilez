var tabla;

//Función que se ejecuta al inicio 
function init(){
	 $("#modulo").html("Abc de Almacenes"); 
	mostrarform(false); 
	listar();

	$.post("../ajax/sucursales.php?op=selectsucursales", function(r){
	            $("#idsucursales").html(r);
	            $('#idsucursales').selectpicker('refresh');

	});
	$.post("../ajax/monedas.php?op=select", function(r){
	            $("#idmonedas").html(r);
	            $('#idmonedas').selectpicker('refresh');

	});
	$.post("../ajax/estatus.php?op=select", function(r){
	            $("#idestatus").html(r);
	            $('#idestatus').selectpicker('refresh');

	});

	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	})
	
	

}

//Función limpiar
function limpiar()
{
	$("#almacen").val("");
	$("#idalmacenes").val("");
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}

//Función Listar
function listar()
{
	tabla=$('#tbllistado').dataTable(
	{
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    "scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false,
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],
		"ajax":
				{
					url: '../ajax/almacenes.php?op=listar',
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar

function guardaryeditar(e)
{
	e.preventDefault(); //No se activará la acción predeterminada del evento
	$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]);

	$.ajax({
		url: "../ajax/almacenes.php?op=guardaryeditar",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {                    
	          bootbox.alert(datos);	          
	          mostrarform(false);
	          tabla.ajax.reload();
	    }

	});
	limpiar();
}

function mostrar(idalmacenes)
{
	$.post("../ajax/almacenes.php?op=mostrar",{idalmacenes : idalmacenes}, function(data, status)
	{
		data = JSON.parse(data);		
		mostrarform(true);

		$("#almacen").val(data.almacen);
		$("#idalmacenes").val(data.idalmacenes);
		$('#idalmacenes').selectpicker('refresh');
		$("#idsucursales").val(data.idsucursales);
		$('#idsucursales').selectpicker('refresh');
		$("#idmonedas").val(data.idmonedas);
		$('#idmonedas').selectpicker('refresh');
		$("#idestatus").val(data.idestatus);
		$('#idestatus').selectpicker('refresh');
		$("#tope").val(data.tope);
		
 	})
}
function buscarMoneda()
{
	//var texto = $("#idmonedas option:selected").text();
	//$("#idmonedas").keydown(function(event){
	//	$("idmonedas").text("El código de la tecla " + String.fromCharCode(event.which) + " es: " + event.which);
	//	$("#idmonedas").val("");
	//}); 
	alert("texto ");
}
//Función para desactivar registros
function desactivar(idalmacenes)
{
	bootbox.confirm("¿Está Seguro de desactivar  almacen ?", function(result){
		if(result)
        {
        	$.post("../ajax/almacenes.php?op=desactivar", {idalmacenes : idalmacenes}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(idalmacenes)
{
	bootbox.confirm("¿Está Seguro de activar almacen ?", function(result){
		if(result)
        {
        	$.post("../ajax/almacenes.php?op=activar", {idalmacenes : idalmacenes}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}


init();