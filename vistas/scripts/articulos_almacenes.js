var tabla;
var codigo=0;
var Opcion=0;
var referencia="";
//Función que se ejecuta al inicio
function init(){
	 $("#modulo").html("Abc de Articulos Por Almacen");
	mostrarform(false); 
	
	listar(Opcion,referencia);
	$("#codigob").focusin(function(e) {
        Opcion=1;   
    });
    $("#nombreb").focusin(function(e) {
        Opcion=2;   
    });
    

	/*$.post("../ajax/articulos.php?op=selectcatalogo", function(r){
	            $("#idarticulos").html(r);
	            $('#idarticulos').selectpicker('refresh');

	});*/

	$.post("../ajax/articulos_almacenes.php?op=select", function(r){
	            $("#idalmacenes").html(r);
	            $('#idalmacenes').selectpicker('refresh');

	});
	$.post("../ajax/articulos.php?op=selectivas", function(r){
	            $("#idivas").html(r);
	            $('#idivas').selectpicker('refresh');

	});

	$.post("../ajax/articulos.php?op=selectmonedas", function(r){
	            $("#idmonedas").html(r);
	            $('#idmonedas').selectpicker('refresh');

	});
    

	
    $("#utilidad").keyup(function(e) {
        calcularPrecio();
    });
	

	$("#formulario").on("submit",function(e)
	{
		idarticulos_almacenes=$("#idarticulos_almacenes").val();
		//alert("id articulos almacenes  "+idarticulos_almacenes);
		guardaryeditar(e);	
	})
}

//Función limpiar
function limpiar()
{
	//$("#nombre").val("");
	//$("#idalmacenes").val("");
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();

	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
		//$("#idarticulos").hide();
		$("#idarticulos_almacenes").val("");
		$("#articulos").css("display", "none");
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
		$("#articulos").css("display", "");
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}
function calcularPrecio()
{
	var precio=0;
	costo=$('#costo').val();
	utilidad=$('#utilidad').val();
	
	precio=Number((costo*utilidad));
	
	$('#precio').val(precio);
}

function buscarArticulo()
{

	if (Opcion==1)
	   referencia=$("#codigob").val();
    if (Opcion==2)
       referencia=$("#nombreb").val();
    //alert("opcion "+Opcion+ " ref "+referencia);
	$.post("../ajax/articulos.php?op=select",{Opcion: Opcion,referencia: referencia}, function(r){
		
	            $("#idarticulos").html(r);
	            $('#idarticulos').selectpicker('refresh');

	});

	
   	
}
//data:{Opcion: Opcion,referencia: referencia},
//Función Listar
function listar(codigo)
{
	//alert("lista funcion datatable "+codigo);

	tabla=$('#tbllistado').dataTable(
	{
		"scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false,

		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'

		            
		        ],
		"ajax":
				{
					url: '../ajax/articulos_almacenes.php?op=listar',
					data:{codigo: codigo},
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar

function guardaryeditar(e)
{
	e.preventDefault(); //No se activará la acción predeterminada del evento
	$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]);
	var codigo=$('select[id=idarticulos]').val();
	var dataString = $('#formulario').serialize();
   // alert('Datos del formulario: '+dataString);

	$('#idarticulo').val(codigo);
	//alert("id articulo  "+codigo);
	$.ajax({
		url: "../ajax/articulos_almacenes.php?op=guardaryeditar",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {                    
	          bootbox.alert(datos);	          
	          mostrarform(false);
	          tabla.ajax.reload();
	    }

	});
	limpiar();
}


function leerIva_y_Moneda()
{
	idalmacenes=$("#idalmacenes").val();
	$.post("../ajax/articulos_almacenes.php?op=leerIva_y_Moneda",{idalmacenes : idalmacenes}, function(data, status)
	{
		data = JSON.parse(data);
		//alert("datos"+data);
		$("#idmonedas").val(data.idmonedas);
		$('#idmonedas').selectpicker('refresh');
		$("#idivas").val(data.idivas);
		$('#idivas').selectpicker('refresh'); 
	})
}
function mostrar(idarticulos_almacenes)
{
	//alert("articulos_almacenes parametros "+idarticulos_almacenes);
	//idarticulo=$("#idarticulos").val();
	//$("#idarticulo").val(idarticulo);
	
	$.post("../ajax/articulos_almacenes.php?op=mostrar",{idarticulos_almacenes : idarticulos_almacenes}, function(data, status)
	{
		data = JSON.parse(data);		
		mostrarform(true);
		
		$("#idarticulos_almacenes").val(data.idarticulos_almacenes);
		$("#idalmacenes").val(data.idalmacenes);
		$('#idalmacenes').selectpicker('refresh');
		$("#idarticulo").val(data.idarticulos);
		$('#idarticulos').selectpicker('refresh');

		$("#idmonedas").val(data.idmonedas);
		$('#idmonedas').selectpicker('refresh');
		$("#stock").val(data.stock);
		$("#costo").val(data.costo);
		$("#precio").val(data.precio);
		$("#idivas").val(data.idivas);
		$('#idivas').selectpicker('refresh');
		$("#utilidad").val(data.porcentaje_utilidad);
		$("#ubicacion").val(data.ubicacion);
		$("#condicion").val(data.condicion);
		 
		

 	})
}

function agregar(idarticulos_almacenes)
{
	$.post("../ajax/articulos_almacenes.php?op=mostrar",{idarticulos_almacenes : idarticulos_almacenes}, function(data, status)
	{
		data = JSON.parse(data);		
		mostrarform(true);
		
		$("#idarticulos_almacenes").val("0");
		$("#idalmacenes").val(data.idalmacenes);
		$('#idalmacenes').selectpicker('refresh');
		$("#idarticulo").val(data.idarticulos);
		$('#idarticulos').selectpicker('refresh');
		$("#idmonedas").val(data.idmonedas);
		$('#idmonedas').selectpicker('refresh');
		$("#stock").val(data.stock);
		$("#costo").val(data.costo);
		$("#precio").val(data.precio);
		$("#idivas").val(data.idivas);
		$('#idivas').selectpicker('refresh');
		$("#utilidad").val(data.porcentaje_utilidad);
		$("#ubicacion").val(data.ubicacion);
		$("#condicion").val(data.condicion);
		 

 	})
	
}
//Función para desactivar registros
function desactivar(idarticulos_almacenes)
{
	bootbox.confirm("¿Está Seguro de desactivar la articulo en almacene?", function(result){
		if(result)
        {
        	$.post("../ajax/articulos_almacenes.php?op=desactivar", {idarticulos_almacenes : idarticulos_almacenes}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(idarticulos_almacenes)
{
	bootbox.confirm("¿Está Seguro de activar articulo en almacen ?", function(result){
		if(result)
        {
        	$.post("../ajax/articulos_almacenes.php?op=activar", {idarticulos_almacenes : idarticulos_almacenes}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}
function buscarcodigo()
{
	var codigo=$('select[id=idarticulos]').val();
	$('#idarticulo').val(codigo);
	listar(codigo);
	
	
}
init();