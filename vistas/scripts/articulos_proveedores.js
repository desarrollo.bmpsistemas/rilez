var tabla;
var codigo=1;
$('#idpersonas').val(codigo);
//Función que se ejecuta al inicio 	
function init(){
	 $("#modulo").html("Abc de Articulos Por Proveedor");
	mostrarform(false);
	
	
	//$("#edades").show();
	//alert("hola "+codigo); 
    listar(codigo);

	/*$.post("../ajax/articulos.php?op=selectcatalogo", function(r){
	            $("#idarticulos").html(r);
	            $('#idarticulos').selectpicker('refresh');

	});*/

	$.post("../ajax/lineas.php?op=select", function(r){
	            $("#idlineas").html(r);
	            $('#idlineas').selectpicker('refresh');

	});
	

	

	$("#formulario").on("submit",function(e)
	{
		//idarticulos_proveedores=$("#idarticulos_proveedores").val();
		//idarticulos=$("#idarticulos").val();

		//alert("id articulos proveedores  "+idarticulos_proveedores+" id articulos "+idarticulos);
		guardaryeditar(e);	
	})
}

//Función limpiar
function limpiar()
{
	$("#nombre").val("");
	$("#idalmacenes").val("");
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();

	if (flag)
	{
		$("#listadoregistros").hide(); 
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
		//$("#idarticulos").hide();
		//$("#idarticulos_proveedores").val("");
		$("#articulos").css("display", "none");
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
		$("#articulos").css("display", "");
	}
}
function selectsublinea()
{
	idlineas=$('select[id=idlineas]').val();
	//Cargamos los items al select estados
	//alert("id lineas "+idlineas);
	$.post("../ajax/sublineas.php?op=selectporlinea", {idlineas : idlineas}, function(r){
		//alert("resultado "+r);
	            $("#idsublineas").html(r);
	            $('#idsublineas').selectpicker('refresh');

	});
}
//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}
function selectarticulosporsublinea()
{
	idsublineas=$('select[id=idsublineas]').val();
	//Cargamos los items al select estados
	//alert("id lineas "+idlineas);
	$.post("../ajax/articulos.php?op=selectarticulosporsublinea", {idsublineas : idsublineas}, function(r){
		//alert("resultado "+r);
	    $("#idarticulos").html(r);
	    $('#idarticulos').selectpicker('refresh');

	});
}

//Función Listar
function listar(codigo)
{
	//alert("lista funcion datatable "+codigo);
	idpersonas=codigo;
	tabla=$('#tbllistado').dataTable(
	{
		"scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false,

		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf',
		            

		            
		        ],
		        
		        	
		        
		"ajax":
				{
					url: '../ajax/articulos_proveedores.php?op=listar',
					data:{idpersonas:idpersonas},
					type : "get",
					dataType : "json",						
					error: function(e){

						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar

function guardaryeditar(e)
{
	var idpersona=$('select[id=idpersona]').val();
	$('#idpersonas').val(idpersonas);
	var dataString = $('#formulario').serialize();
    //alert('Datos del formulario: '+dataString);

	e.preventDefault(); //No se activará la acción predeterminada del evento
	//$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]);

	$.ajax({
		url: "../ajax/articulos_proveedores.php?op=guardaryeditar",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {                    
	        //bootbox.alert(datos);	          
	        //mostrarform(false);
	        //tabla.ajax.reload();
	         var codigo=$('select[id=idpersona]').val();
			$('#idpersonas').val(codigo);
			//alert("id persoan "+codigo);
			listar(codigo);
			Swal.fire({
			  position: 'top-end',
			  type: 'success',
			  title: 'Sus Datos se han  guardado',
			  showConfirmButton: false,
			  timer: 1500
			})
	    }

	});
	limpiar();
}



function mostrar(idarticulos_proveedores)
{
	
	
	//alert("articulos_provedores parametros "+idarticulos_proveedores);
	

	$.post("../ajax/articulos_proveedores.php?op=mostrar",{idarticulos_proveedores : idarticulos_proveedores}, function(data, status)
	{
		data = JSON.parse(data);		
		mostrarform(true);
		
		$("#idarticulos_proveedores").val(data.idarticulos_proveedores);
		$("#idpersonas").val(data.idpersonas);
		$('#idpersonas').selectpicker('refresh');
		$("#idarticulos").val(data.idarticulos);
		$('#idarticulos').selectpicker('refresh');
		
		$("#costo").val(data.costo);
		$("#NoIdentificacion").val(data.NoIdentificacion);
		
 	})
}

//Función para desactivar registros
function desactivar(idarticulos_proveedores)
{
	bootbox.confirm("¿Está Seguro de desactivar la articulo por proveedor ?", function(result){
		if(result)
        {
        	$.post("../ajax/articulos_proveedores.php?op=desactivar", {idarticulos_proveedores : idarticulos_proveedores}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(idarticulos_proveedores)
{
	bootbox.confirm("¿Está Seguro de activar articulo por proveedor ?", function(result){
		if(result)
        {
        	$.post("../ajax/articulos_proveedores.php?op=activar", {idarticulos_proveedores : idarticulos_proveedores}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}
function buscar()
{
	nombre=$("#nombreb").val();
	Opcion=2;
	referencia=nombre
   	$.post("../ajax/personas.php?op=selectp", {Opcion : Opcion,referencia:referencia}, function(r){
	            $("#idpersonasb").html(r);
	            $('#idpersonasb').selectpicker('refresh');

	});
   	
}
function buscarcodigo()
{
	var codigo=$('select[id=idpersonasb]').val();
	$('#idpersonas').val(codigo);
	//alert("id persoan "+codigo);
	listar(codigo);
	
	
}
init();