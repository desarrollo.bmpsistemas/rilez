var tabla;
var nombre="";
//Función que se ejecuta al inicio
function init(){
	 $("#modulo").html("Abc de Calles");
	mostrarform(false);
	listar(nombre);

	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	})

}

//Función limpiar

function limpiar()
{
	$("#nombre").val("");
	$("#idcalles").val("");
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}
function buscar()
{
	//alert("buscando");
	nombre=$("#nombre").val();
   
   	listar(nombre);
}
//Función Listar
function listar(nombre)
{
	tabla=$('#tbllistado').dataTable(
	{
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla

	    "scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false,
        
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],
		"ajax":
				{
					url: '../ajax/calles.php?op=listar',
					data:{nombre:nombre},
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar
function altaDeColonias()
{
	//alert("buscando ");
	//window.open('../vistas/colonias.php','BmpSistemas', 'width=800, height=600');
	
}
function guardaryeditar(e)
{
	//alert("parametro "+e);
	//e.preventDefault(); //No se activará la acción predeterminada del evento
	//$("#btnGuardar").prop("disabled",true);
	//if (e==1)
	//	$("#idcalles").val(0);
	idcalles=$("#idcalles").val();
	nombre=$("#nombre").val();
	//alert("parametro "+e+" id col "+idcalles+" calle "+nombre);
	$.post("../ajax/calles.php?op=guardaryeditar",{Opcion:e,idcalles : idcalles,nombre:nombre}, function(data, status)
	{
		mensaje(data);
		listar(nombre);
	    //tabla.ajax.reload();
	    
 	});
 	$("#idcalles").val(0);

	
}

function mostrar(idcalles)
{
	$.post("../ajax/calles.php?op=mostrar",{idcalles : idcalles}, function(data, status)
	{
		data = JSON.parse(data);		
		//mostrarform(true);

		$("#nombre").val(data.nombre);
		$("#idcalles").val(data.idcalles);


 	})
}

//Función para desactivar registros
function desactivar(idcalles)
{
	bootbox.confirm("¿Está Seguro de desactivar la calle?", function(result){
		if(result)
        {
        	$.post("../ajax/calles.php?op=desactivar", {idcalles : idcalles}, function(e){
        		mensaje(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(idcalles)
{
	bootbox.confirm("¿Está Seguro de activar la calle?", function(result){
		if(result)
        {
        	$.post("../ajax/calles.php?op=activar", {idcalles : idcalles}, function(e){
        		mensaje(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

function mensaje(titulo)
{
	Swal.fire({
		  position: 'top-end',
		  type: 'success',
		  title: titulo,
		  showConfirmButton: false,
		  timer: 1500
		})
}
init();