var tabla;
var Opcion=0;
var referencia="0"; 
//Función que se ejecuta al inicio
function init(){
   $("#modulo").html("Abc de Clientes");
  //$('#idpersonas').prop('disabled', 'disabled');
  mostrarform(false);

  listar(Opcion);
  $( "#busqueda" ).keydown(function(e) {
      var code = (e.keyCode ? e.keyCode : e.which);
    //alert("valor"+code);
        if(code==13){

            listar();
        }
  });

  $.post("../ajax/vendedores.php?op=select", function(r){
              $("#idvendedores").html(r);
              $('#idvendedores').selectpicker('refresh');

  });



  $("#formulario").on("submit",function(e)
  {
    guardaryeditar(e);  
  })
}

//Función limpiar

function checarDescuento(e)
{
  descuento=Number($("#descuento").val());
  if (descuento>=1)
  {
     $("#descuento").val(0);
     Swal.fire({
        type: 'error',
        title: 'Descuento invalido debe ser menor que 1 '
        
      })

  }
}
function MostrarModalOpcionPersona()
{
  $("#myModalBusquedaPersona").modal("show"); 
}
function checarRadioButton(e)
{       
    ref=$("#busqueda").val();
    Opcion=e;
    $("#myModalBusquedaPersona .close").click();
}
function limpiar()
{
  $("#nombre").val("");
  $("#num_documento").val("");
  $("#direccion").val("");
  $("#telefono").val("");
  $("#email").val("");
  $("#idpersonas").val("0");
}

//Función mostrar formulario
function mostrarform(flag)
{
  limpiar();
  if (flag)
  {
    $("#listadoregistros").hide();
    $("#formularioregistros").show();
    $("#btnGuardar").prop("disabled",false);
    //$("#btnagregar").hide();
    $("#btnagregarMpo").show();
    $("#btnagregarCol").show();
    $("#btnagregarCalle").show();

  }
  else
  {
    $("#listadoregistros").show();
    $("#formularioregistros").hide();
    $("#btnagregar").show();
    //$("#btnagregarMpo").hide();
    //$("#btnagregarCol").hide();
    //$("#btnagregarCalle").hide();
  }
}

//Función cancelarform
function cancelarform()
{
  limpiar();
  mostrarform(false);
}

//Función Listar
function listar()
{

  //$("#btnagregarMpo").hide();
  //$("#btnagregarCol").hide();
  //$("#btnagregarCalle").hide();
  referencia=$("#busqueda").val();
  
  //alert(" Opcion "+Opcion+" referencia "+referencia);
  tabla=$('#tbllistado').dataTable(
  {
    "scrollY":        "200px",
    "scrollCollapse": true,
    "paging":         false,
    "aProcessing": true,//Activamos el procesamiento del datatables
      "aServerSide": true,//Paginación y filtrado realizados por el servidor
      dom: 'Bfrtip',//Definimos los elementos del control de tabla
      buttons: [              
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdf'
            ],

    "ajax":
        {
          url: '../ajax/clientes.php?op=listar',
          data:{Opcion:Opcion,referencia:referencia},
          type : "get",
          dataType : "json",            
          error: function(e){
            console.log(e.responseText);  
          }
        },
    "bDestroy": true,
    "iDisplayLength": 5,//Paginación
      "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
  }).DataTable();
}
//Función para guardar o editar
function agregarDatos(Opc)
{
  /*if (Opc==1)
    $("#myModalCalles").modal("show");
  if (Opc==2)
    $("#myModalColonias").modal("show");
  if (Opc==3)
    $("#myModalMunicipios").modal("show");*/

}
function guardaryeditar(e)
{
  e.preventDefault(); //No se activará la acción predeterminada del evento
  $("#btnGuardar").prop("disabled",true);
  var formData = new FormData($("#formulario")[0]);
  //referencia=$("#referencia").val();
  
  var dataString = $('#formulario').serialize();
  //alert('Datos del formulario: '+dataString);

  $.ajax({
    url: "../ajax/clientes.php?op=guardaryeditar",
      type: "POST",
      data: formData,
      contentType: false,
      processData: false,

      success: function(datos)
      {                    
            bootbox.alert(datos);           
            mostrarform(false);
            tabla.ajax.reload();
      }

  });
  limpiar();
}

function mostrar(idpersonas)
{
  $.post("../ajax/clientes.php?op=mostrar",{idpersonas : idpersonas}, function(data, status)
  {
    data = JSON.parse(data);    
    mostrarform(true);
    
    
    $("#nombre").val(data.descripcion);
    $("#nombrecomercial").val(data.nomComercial);
    $("#rfc").val(data.rfc);
    $("#idvendedores").val(data.vendedor_id);
    $("#idvendedores").selectpicker('refresh');

    //$('select[name=idvendedores').val(data.ven);
    //$('.selectpicker').selectpicker('refresh')

    $("#referencia").val(data.referencia);
    $("#correo").val(data.correo);
    $("#telefono").val(data.telefono);
    $("#diascredito").val(data.dias);
    $("#limitecredito").val(data.limite);
    $("#descuento").val(data.descuento);
    $("#idpersonas").val(data.id);
    //alert("vendedor id "+data.vendedor_id);

  })
}
function verHistorial()
{
  $.post("../ajax/clientes.php?op=verHistorial",{idpersonas : idpersonas}, function(data, status)
  {
    data = JSON.parse(data);    
    
    adeudo=number(data.total);
    alert("su adeudo es "+adeudo);
    

  })
}

//Función para desactivar registros
function desactivar(idpersonas)
{
  bootbox.confirm("¿Está Seguro de desactivar el estado cliente?", function(result){
    if(result)
        {
          $.post("../ajax/clientes.php?op=desactivar", {idpersonas : idpersonas}, function(e){
            //bootbox.alert(e);
              tabla.ajax.reload();
          }); 
        }
  })
}


//Función para activar registros
function activar(idpersonas)
{
  bootbox.confirm("¿Está Seguro de activar el estado del cliente?", function(result){
    if(result)
        {
          $.post("../ajax/clientes.php?op=activar", {idpersonas : idpersonas}, function(e){
           // bootbox.alert(e);
              tabla.ajax.reload();
          }); 
        }
  })
}

function selectpaises()
{
  
  $.post("../ajax/paises.php?op=select", function(r){
    
              $("#idpaisesm").html(r);
              $('#idpaisesm').selectpicker('refresh');

  });
}

function selectestados()
{
  idpaises=$('select[id=idpaisesm]').val();
  
  //Cargamos los items al select estados
  //alert("id paiese "+idpaises);
  $.post("../ajax/estados.php?op=selectestados", {idpaises : idpaises}, function(r){
    //alert("resultado "+r);
              $("#idestadosm").html(r);
              $('#idestadosm').selectpicker('refresh');

  });
}
/*function selectmunicipios()
{
 idestados=0;
  
  $.post("../ajax/municipios.php?op=selectMunicipios", {idestados : idestados}, function(r){
    //alert("resultado "+r);
              $("#idmunicipios").html(r);
              $('#idmunicipios').selectpicker('refresh');

  });
}*/

function buscarPersonas(e) 
{
  //alert("buscarndo personas "+Opcion);

 
  if (e==1)
  {
    //alert("buscando clientes");
     if ( $('input:radio[name=rbidpersona]:checked').val())
      Opcion=0;
    if ( $('input:radio[name=rbrfcp]:checked').val())
      Opcion=1;
    if ( $('input:radio[name=rbnombrep]:checked').val())
      Opcion=2;
    if ( $('input:radio[name=rbtelefonop]:checked').val())
      Opcion=3;
    ref=$("#busqueda").val();
  }
  
  
  
  
   
    if (ref.length>0)
    {
      $.post("../ajax/clientes.php?op=select",{Opcion:Opcion,referencia : ref}, function(r){
          //alert("datos "+r);  
                $("#idpersonas").html(r);
                $('#idpersonas').selectpicker('refresh');

      });
    }
    else
    {
      mensajeError("No ha selecionado que buscar ");
    }

  

  
  
  
}

function buscarColonia(nombre)
{
  nombre=$("#colonia").val();
  $.post("../ajax/colonias.php?op=select", {nombre : nombre}, function(r){
              $("#idcolonias").html(r);
              $('#idcolonias').selectpicker('refresh');

    });
 
}
function buscarCalle(nombre)
{
  nombre=$("#calle").val();
  $.post("../ajax/calles.php?op=select", {nombre : nombre}, function(r){
              $("#idcalles").html(r);
              $('#idcalles').selectpicker('refresh');

    });
 
}




function agregarMunicipio()
{

    
  nombre=$("#municipio").val();
  
  idestados=$('select[id=idestadosm]').val();
  
  $.post("../ajax/municipios.php?op=guardaryeditar",{idestados:idestados,nombre:nombre}, function(data, status)
  {
    
    $('#myModalMunicipios').modal({backdrop: 'static', keyboard: false});
    
    Swal.fire({
      position: 'top-end',
      type: 'success',
      title: 'Sus Datos se han  guardado',
      showConfirmButton: false,
      timer: 1500
    })
    $.post("../ajax/municipios.php?op=selectmunicipios", function(r){
              $("#idmunicipios").html(r);
              $('#idmunicipios').selectpicker('refresh');

    });

  })

}

function buscarColonia()
{

    
  nombre=$("#colonia").val();
  
  
  $.post("../ajax/colonias.php?op=select", {nombre : nombre}, function(r){
              $("#idcolonias").html(r);
              $('#idcolonias').selectpicker('refresh');

    });
 
}

function agregarColonia()
{
   
  nombre=$("#colonia").val();
 
  $.post("../ajax/colonias.php?op=guardaryeditar",{nombre:nombre}, function(data, status)
  {
    
    $('#myModalColonias').modal({backdrop: 'static', keyboard: false});
    
    Swal.fire({
      position: 'top-end',
      type: 'success',
      title: 'Sus Datos se han  guardado',
      showConfirmButton: false,
      timer: 1500
    })
    $.post("../ajax/colonias.php?op=select", {nombre : nombre}, function(r){
              $("#idcolonias").html(r);
              $('#idcolonias').selectpicker('refresh');

    });
    
  })

}
function buscarCalle()
{
   
  nombre=$("#calle").val();
  
  $.post("../ajax/calles.php?op=select", {nombre : nombre}, function(r){
              $("#idcalles").html(r);
              $('#idcalles').selectpicker('refresh');

    });
  /*Swal.fire({
      position: 'top-end',
      type: 'success',
      title: 'Sus Datos Fueron Procesados',
      showConfirmButton: false,
      timer: 1500
    })*/

}
function agregarCalle()
{

    
  nombre=$("#calle").val();
  
  
  
  $.post("../ajax/calles.php?op=guardaryeditar",{nombre:nombre}, function(data, status)
  {
    
    $('#myModalCalles').modal({backdrop: 'static', keyboard: false});
    
    Swal.fire({
      position: 'top-end',
      type: 'success',
      title: 'Sus Datos se han  guardado',
      showConfirmButton: false,
      timer: 1500
    })
    $.post("../ajax/calles.php?op=select",{nombre:nombre}, function(r){
              $("#idcalles").html(r);
              $('#idcalles').selectpicker('refresh');

    });
    
  })

}

/*e.preventDefault(); //No se activará la acción predeterminada del evento
  $("#btnGuardar").prop("disabled",true);
  var formData = new FormData($("#formulario")[0]);

  $.ajax({
    url: "../ajax/municipios.php?op=guardaryeditar",
      type: "POST",
      data: formData,
      contentType: false,
      processData: false,

      success: function(datos)
      {                    
            bootbox.alert(datos);           
            mostrarform(false);
            tabla.ajax.reload();
      }

  });
  limpiar();*/

init();