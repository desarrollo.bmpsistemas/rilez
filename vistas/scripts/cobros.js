var tabla;
var tabla2;
var codigo=0;
var Opcion=2;
var referencia="PUBLICO"; 
var estatus=false;
var fecha_inicial;
var fecha_final;
var TablaConDatos;
var datos;
var buscado='N';
//Función que se ejecuta al inicio 
function init(){
	$("#modulo").html("Abc de Cobros ");
	$("#divPersona").hide();
	$("#divFecha").hide();
	idusuarios=$('#idusuarios').val();	
	idalmacenes=$('#Sidalmacenes').val();
	document.getElementById('rbfolio').checked = true;
	mostrarform(false); 
	mostrarDivPorPersona();
	tomar_fecha();
	ponerReadOnly("busqueda");
	//fecha_inicial="2019-06-01";
	//fecha_final="2019-08-30";
	//listarEntSal(Opcion);
	//mostrarPagos(1);
	$('#fecha').attr('disabled', true);
	$('#np').attr('disabled', true);
	//listar2(Opcion,referencia,fecha_inicial,fecha_final);
	$("#codigob").focusin(function(e) {
        Opcion=1;   
    });
    $("#nombreb").focusin(function(e) {
        Opcion=2;   
    });
    
    
	$.post("../ajax/sat_forma_de_pagos.php?op=select", {idusuarios:idusuarios},function(r){
	            $("#idformasdepagos").html(r);
	            $('#idformasdepagos').selectpicker('refresh');

	});

	$.post("../ajax/almacenes.php?op=selectAlmacenesPorUsuario",{idusuarios:idusuarios}, function(r, status)
		{
			$("#idalmaceness").html(r);
            $('#idalmaceness').selectpicker('refresh');
     	});
	

	$.post("../ajax/articulos.php?op=selectivas", function(r){
	            $("#idivas").html(r);
	            $('#idivas').selectpicker('refresh');

	});

	$.post("../ajax/articulos.php?op=selectmonedas", function(r){
	            $("#idmonedas").html(r);
	            $('#idmonedas').selectpicker('refresh');

	});
    
	$.post("../ajax/articulos.php?op=select_tipos_de_pagos", function(r){
	            $("#idformadepagos").html(r);
	            $('#idformadepagos').selectpicker('refresh');

	});
	
    $("#utilidad").keyup(function(e) {
        calcularPrecio();
    });
	

	$("#formulario").on("submit",function(e)
	{
		idarticulos_almacenes=$("#idarticulos_almacenes").val();
		//alert("id articulos almacenes  "+idarticulos_almacenes);
		guardaryeditar(e);	
	})
	$( "#busqueda" ).keydown(function(e) {
  		var code = (e.keyCode ? e.keyCode : e.which);
		//alert("valor"+code);
        if(code==13){
        	$("#referencia").val('');
        	$("#importe").val('');
        	buscado='S';
            selecionarOpcion(1);
        }
	});
	$( "#referencia" ).keydown(function(e) {
  		var code = (e.keyCode ? e.keyCode : e.which);
		//alert("valor"+code);
        if(code==13){

            $("#importe").focus();
        }
	});
	$( "#importe" ).keydown(function(e) {
  		var code = (e.keyCode ? e.keyCode : e.which);
		//alert("valor"+code);
        if(code==13){

            agregarPago(0);
        }
	});
	
}

//Función limpiar

function inicializar(e)
{
	buscado='N';
}

function activarBusqueda()
{
	idalmacenes=$('select[id=idalmaceness]').val();
	if (idalmacenes>0)
	{
		quitarReadOnly("busqueda");

	}
}
function permisoDeFecha(e)
{
	$("#myModalAutorizarFecha").modal("show");
}
function modalBusqueda(e)
{
	$("#myModalBusqueda").modal("show");
}
function checarRadioButtonBusqueda(e)
{
		referencia=$("#busqueda").val();

        if(e == 1)
        {
            //document.getElementById('rbFolio').checked = false;
            Opcion=1;
            
        }
        if(e == 2)
        {
            //document.getElementById('rbnombre').checked = false;
            Opcion=2;
        }
        $("#myModalBusqueda .close").click();
        
    
}

function autorizarFechaDePago(e)
{
	usuario=$("#usuarioF").val();
	clave=$("#claveF").val();
	//alert("id user "+usuario+"clave "+clave);
	$.post("../ajax/cobros.php?op=activarOpciones",{idusuarios:usuario,clave:clave}, function(data, status)
		{
				
			//data = JSON.parse(data);	
			rol=data;
			//alert("datos "+data);
			
			if (rol!=7 )
			{
				Swal.fire({
				  position: 'top-end',
				  type: 'success',
				  title: 'Usuario sin rol de autorizacion ',
				  showConfirmButton: false,
				  timer: 1500
				});
				$('#fecha').attr('disabled', true);
				$("#np").val(0);
				$('#np').attr('disabled', true);
				
    			
			}	
			else
			{
				
    			//$('#fecha').attr('disabled', false);
    			Swal.fire({
				  position: 'top-end',
				  type: 'success',
				  title: 'Usuario autorizado ',
				  showConfirmButton: false,
				  timer: 1500
				});
				$('#fecha').attr('disabled', false);	
    			$('#np').attr('disabled', false);
			}
	
		})
}

function ocultar()
{
	//$("#divAlmacen").hide();
	//$("#divFechaInicial").hide();
	//$("#divFechaFinal").hide();
	$("#divPersona").hide();
	//$("#divFecha").hide();

}
function tomar_fecha()
{
	var fecha;
	fecha=formato_fecha();
	$('#fecha_inicial').val(fecha);
	$('#fecha_final').val(fecha);
	$('#fechainicial').val(fecha);
	$('#fechafinal').val(fecha);
	$('#fecha').val(fecha);
	
	//alert("feecha "+fecha);
}

function formato_fecha()
{
	var now = new Date();
	var day = ("0" + now.getDate()).slice(-2);
	var month = ("0" + (now.getMonth() + 1)).slice(-2);
	var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
    return today;
}
function limpiar()
{
	//$("#nombre").val("");
	//$("#idalmacenes").val("");
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();

	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
		//$("#idarticulos").hide();
		$("#idarticulos_almacenes").val("");
		$("#articulos").css("display", "none");
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
		$("#articulos").css("display", "");
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}
function calcularPrecio()
{
	var precio=0;
	costo=$('#costo').val();
	utilidad=$('#utilidad').val();
	
	precio=Number((costo*utilidad));
	
	$('#precio').val(precio);
}

function buscarPersonas() //buscarPersonas
{
	
	
	referencia=$('#busqueda').val();
	//alert("buscarndo personas Opcion "+Opcion+" Referencia "+referencia);

	$.post("../ajax/clientes.php?op=select",{Opcion:Opcion,referencia : referencia}, function(r){
			//alert("datos salidas "+r);	
            $("#idpersonas").html(r);
            $('#idpersonas').selectpicker('refresh');

	});
	
}

function selectFolio(ident_sal)
{

	$("#busqueda").val(ident_sal); 
	$("#referencia").focus(); 
	mostrarPagos(ident_sal);
   	
}
//data:{Opcion: Opcion,referencia: referencia},
//Función Listar
function selecionarOpcion(Op)
{
	buscado='S';
	if (Op==1)
	{
		
        
        
        
		if (document.getElementById('rbfolio').checked)
		{
			ref=$("#busqueda").val();
			Op=1;
			listarEntSal(Op,ref);
			$("#referencia").focus();
			//alert("entro a buscar ");
		}
		if (document.getElementById('rbnombre').checked)
		{
			buscarPersonas();
		}
	}
	if (Op==2)
	{
		saldo=0.1;
		
		idpersonas=$('select[id=idpersonas]').val();
		Op=2;
		listarEntSal(Op,idpersonas);
	}

}

function listarEntSal(Opc,Ref)
{
	{
		//alert("Opcion "+Opc +" ref "+Ref );

		tabla=$('#tbllistado').dataTable(
		{
			"scrollY":        "200px",
        	"scrollCollapse": true,
        	"paging":         false,

			"aProcessing": true,//Activamos el procesamiento del datatables
	    	"aServerSide": false,//Paginación y filtrado realizados por el servidor
	    	dom: 'Bfrtip',//Definimos los elementos del control de tabla
		    buttons: [		          
			            'copyHtml5',
			            'excelHtml5',
			            'csvHtml5',
			            'pdf'

			            
			        ],
			"ajax":
					{
						url: '../ajax/cobros.php?op=listar',
						data:{Opcion:Opc,referencia:Ref},
						type : "get",
						dataType : "json",						
						error: function(e){
							console.log(e.responseText);	
						}
						
					},
			"bDestroy": true,
			"iDisplayLength": 5,//Paginación
		    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
		}).DataTable();
		$("#referencia").focus();
		//buscado='S';
		
	}
}

function mostrarPagos(ident_sal)
{
	idpagos=0;
	
	//ident_sal=$('#folio').val();
	//alert("id ent sal "+ident_sal +" id pagos "+idpagos);

	tabla2=$('#tbllistado2').dataTable(
	{
		"scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false,

		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'

		            
		        ],
		"ajax":
				{
					url: '../ajax/cobros.php?op=mostrarPagos',
					data:{idpagos:idpagos,ident_sal:ident_sal},
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}

function guardaryeditar(e)
{
	e.preventDefault(); //No se activará la acción predeterminada del evento
	$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]);
	var codigo=$('select[id=idarticulos]').val();
	var dataString = $('#formulario').serialize();
   // alert('Datos del formulario: '+dataString);

	$('#idarticulo').val(codigo);
	//alert("id articulo  "+codigo);
	$.ajax({
		url: "../ajax/articulos_almacenes.php?op=guardaryeditar",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {                    
	          //bootbox.alert(datos);	          
	          mostrarform(false);
	          tabla.ajax.reload();
	    }

	});
	limpiar();
}



function mostrarDivPorPersona()
{
	$("#divPersona").hide();
	//$("#divFecha").hide();
	//$("#divAlmacen").hide();
	
	tipo=$('select[id=tipodebusqueda]').val();

	if (tipo=='Persona')
	{
	    $("#divPersona").show();
	    //$("#divFecha").hide();
	    //$("#divAlmacen").show();
		
	}
	if (tipo=='Fecha')
	{
	     $("#divPersona").hide();
	     $("#divFecha").show();
	     $("#divAlmacen").show();
		//$("#divFechaInicial").show();
		//$("#divFechaFinal").show();
	}
	  
	
}

function imprimirPago(ident_sal,idpagos)
{
	
	if (Number(idpagos)>0)
	window.open('../reportes/rep_pago.php?ident_sal='+ident_sal+'&idpagos='+idpagos,'BmpSistemas', 'width=800, height=600');
	else
	{
		Swal.fire({
		  type: 'error',
		  title: 'Documento  a imprimir invalido ...no se ha generado, o no se ha establecido  '
		  
		})
	}
}
function ponerReadOnly(id)
    {
        // Ponemos el atributo de solo lectura
        $("#"+id).attr("readonly","readonly");

        // Ponemos una clase para cambiar el color del texto y mostrar que
        // esta deshabilitado
        //$("#"+id).addClass("readOnly");
        //alert("solo lectura "+id);
    }
 
function quitarReadOnly(id)
    {
        // Eliminamos el atributo de solo lectura
        $("#"+id).removeAttr("readonly");
        // Eliminamos la clase que hace que cambie el color
        //$("#"+id).removeClass("readOnly");
        //alert("lectura y escritura "+id);
    }

function buscarVenta(e)
{
	ident_sal=$("#busqueda").val();
	if (buscado=='S' && ident_sal>0)
	{
	$.post("../ajax/cobros.php?op=buscarVenta",{ident_sal : ident_sal}, function(data, status)
	{
	
		if (data==0)
		{
			bootbox.alert("Folio no registrado");
			$("#busqueda").focus();
			ponerReadOnly("importe");
			return;
		}
		else
			quitarReadOnly("importe");
		mostrarPagos(ident_sal);
		$("#referencia").focus();
		//buscado='N';
	
	});
	}
	else
	$("#busqueda").focus();	
	
		
}
function leerIva_y_Moneda()
{
	idalmacenes=$("#idalmacenes").val();
	$.post("../ajax/articulos_almacenes.php?op=leerIva_y_Moneda",{idalmacenes : idalmacenes}, function(data, status)
	{
		data = JSON.parse(data);
		//alert("datos"+data);
		$("#idmonedas").val(data.idmonedas);
		$('#idmonedas').selectpicker('refresh');
		$("#idivas").val(data.idivas);
		$('#idivas').selectpicker('refresh'); 
	})
}
function mostrar(idarticulos_almacenes)
{
	
}

function agregarPago(e)
{
	
	idpagos=$("#idpagos").val();

	ident_sal=$("#busqueda").val();
	fecha=$("#fecha").val();
	importe=$("#importe").val();
	referencia=$("#referencia").val();
	idusuarios=$("#idusuarios").val();
	np=$("#np").val();
	idformadepagos=$('select[id=idformasdepagos]').val();
	//alert("idpagos"+idpagos+ " ident_sal "+ ident_sal+ " fecha "+fecha+" referencia"+referencia+" idformadepagos "+ idformadepagos +"importe" + importe+" idusuarios"+idusuarios);
	$.post("../ajax/cobros.php?op=agregarPago",{idpagos:idpagos,ident_sal:ident_sal,fecha:fecha,referencia:referencia,idformadepagos:idformadepagos,importe:importe,idusuarios:idusuarios,np:np}, function(e)
	{
		
		 //bootbox.alert(e);
	     //tabla2.ajax.reload();
	     mostrarPagos(ident_sal);
	     $("#busqueda").focus();
	     return;

 	})
}
//Función para desactivar registros
function desactivar(np)
{
	ident_sal=$("#busqueda").val();
	//bootbox.confirm("¿Está Seguro de desactivar pago ?"+np, function(result){
	//	if(result)
        //{
        	$.post("../ajax/cobros.php?op=desactivar", {np : np,idusuarios:idusuarios}, function(e){
        		//bootbox.alert(e);
	            //tabla.ajax.reload();
	            mostrarPagos(ident_sal);
        	});	
      //  }
	//})
}
//Función para activar registros
function activar(np)
{
	//bootbox.confirm("¿Está Seguro de activar pago ?", function(result){
	//	if(result)
        {
        	$.post("../ajax/cobros.php?op=activar", {np:np,idusuarios:idusuarios}, function(e){
        		//bootbox.alert(e);
	            mostrarPagos(ident_sal);
        	});	
        }
	//})
}
function eliminar(np)
{
	ident_sal=$("#busqueda").val();

	bootbox.confirm("¿Está Seguro de eliminar pago ?"+np, function(result){
		if(result)
        {
        	$.post("../ajax/cobros.php?op=eliminar", {np : np,idusuarios:idusuarios}, function(e){
        		//bootbox.alert(e);
	            //tabla.ajax.reload();
	            mostrarPagos(ident_sal);
        	});	
        }
	})
}


init();