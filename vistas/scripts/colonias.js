var tabla;
var nombre="";
//Función que se ejecuta al inicio
function init(){
	 $("#modulo").html("Abc de Colonias");
	//mostrarform(false);
	listar(nombre);

	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	})

}

//Función limpiar

function limpiar()
{
	
}

//Función mostrar formulario
function mostrarform(flag)
{
	
}

//Función cancelarform
function cancelarform()
{
	
}
function buscar()
{
	//alert("buscando");
	nombre=$("#nombre").val();
   
   	listar(nombre);
}
//Función Listar
function listar(nombre)
{
	tabla=$('#tbllistado').dataTable(
	{
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla

	    "scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false,
        
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],
		"ajax":
				{
					url: '../ajax/colonias.php?op=listar',
					data:{nombre:nombre},
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar
function altaDeColonias()
{
	//alert("buscando ");
	//window.open('../vistas/colonias.php','BmpSistemas', 'width=800, height=600');
	
}
function guardaryeditar(e)
{
	//alert("parametro "+e);
	//e.preventDefault(); //No se activará la acción predeterminada del evento
	//$("#btnGuardar").prop("disabled",true);
	//if (e==1)
	//	$("#idcolonias").val(0);
	idcolonias=$("#idcolonias").val();
	nombre=$("#nombre").val();
	//alert("parametro "+e+" id col "+idcolonias+" calle "+nombre);
	$.post("../ajax/colonias.php?op=guardaryeditar",{Opcion:e,idcolonias : idcolonias,nombre:nombre}, function(data, status)
	{
		mensaje(data);
		listar(nombre);
	    //tabla.ajax.reload();
	    
 	});
 	$("#idcolonias").val(0);

	
}

function mostrar(idcolonias)
{
	$.post("../ajax/colonias.php?op=mostrar",{idcolonias : idcolonias}, function(data, status)
	{
		data = JSON.parse(data);		
		//mostrarform(true);

		$("#nombre").val(data.nombre);
		$("#idcolonias").val(data.idcolonias);


 	})
}

//Función para desactivar registros
function desactivar(idcolonias)
{
	bootbox.confirm("¿Está Seguro de desactivar la calle?", function(result){
		if(result)
        {
        	$.post("../ajax/colonias.php?op=desactivar", {idcolonias : idcolonias}, function(e){
        		mensaje(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(idcolonias)
{
	bootbox.confirm("¿Está Seguro de activar la calle?", function(result){
		if(result)
        {
        	$.post("../ajax/colonias.php?op=activar", {idcolonias : idcolonias}, function(e){
        		mensaje(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

function mensaje(titulo)
{
	Swal.fire({
		  position: 'top-end',
		  type: 'success',
		  title: titulo,
		  showConfirmButton: false,
		  timer: 1500
		})
}
init();