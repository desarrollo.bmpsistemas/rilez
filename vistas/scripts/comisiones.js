var tabla;
var codigo=15;
//Función que se ejecuta al inicio
function init(){
	 $("#modulo").html("Abc de Comisiones");
	mostrarform(false);
	
	
	//$("#edades").show();
	//alert("hola "+codigo);
    listar();

	$.post("../ajax/vendedores.php?op=select", function(r){
	            $("#idvendedores").html(r);
	            $('#idvendedores').selectpicker('refresh');
	            

	});

	

	

	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	})
}

//Función limpiar

function llenarVendedores()
{
	mostrarform(false);	
	$.post("../ajax/vendedores.php?op=select", function(r){
	$("#idvendedoress").html(r);
	$('#idvendedoress').selectpicker('refresh');
	});

}
function agregarComision()
{
	//alert("enviando datos ");
	
	lim_inf=$("#lim_infs").val();
	lim_sup=$("#lim_sups").val();
	por_vendedor=$("#por_vendedors").val();
	por_gerente=$("#por_gerentes").val();
	idvendedores= $("#idvendedoress").val();
	
	//alert("lim inf "+lim_inf+"lim sup"+lim_sup+"por ven "+por_vendedor+"por ger "+por_gerente+"id ven "+idvendedores);
	$.post("../ajax/comisiones.php?op=guardaryeditar",{idvendedores:idvendedores,lim_inf:lim_inf,lim_sup:lim_sup,por_vendedor:por_vendedor,por_gerente:por_gerente}, function(data, status)
	{
		
		$('#myModal').modal({backdrop: 'static', keyboard: false});
		
		listar();
		Swal.fire({
		  position: 'top-end',
		  type: 'success',
		  title: 'Sus Datos se han  guardado',
		  showConfirmButton: false,
		  timer: 1500
		})

 	})

}




function limpiar()
{
	
	$("#idvendedores").val("");
	$("#idcomisiones").val("");
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();

	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
		$("#idarticulos").hide();
		
		$("#articulos").css("display", "none");
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
		$("#articulos").css("display", "");
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}

//Función Listar
function listar()
{
	//alert("lista funcion datatable "+codigo);
	tabla=$('#tbllistado').dataTable(
	{
		"scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false,

		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'

		            
		        ],
		"ajax":
				{
					url: '../ajax/comisiones.php?op=listar',
					data:{codigo: codigo},
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar

function guardaryeditar(e)
{
	e.preventDefault(); //No se activará la acción predeterminada del evento
	$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]);

	$.ajax({
		url: "../ajax/comisiones.php?op=guardaryeditar",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {                    
	          //bootbox.alert(datos);	          
	          //mostrarform(false);
	          //tabla.ajax.reload();
	          bootbox.alert(datos);	          
	          mostrarform(false);
	          listar();
	    }

	});
	limpiar();
}

function mostrar(idcomisiones)
{
	$.post("../ajax/comisiones.php?op=mostrar",{idcomisiones : idcomisiones}, function(data, status)
	{
		data = JSON.parse(data);		
		mostrarform(true);

		
		$("#idcomisiones").val(data.idcomisiones);
		$("#lim_inf").val(data.lim_inf);
		$("#lim_sup").val(data.lim_sup);
		$("#por_vendedor").val(data.por_vendedor);
		$("#por_gerente").val(data.por_gerente);
		$("#idvendedores").val(data.idvendedores);
 	})
}

//Función para desactivar registros
function desactivar(idcomisiones)
{
	bootbox.confirm("¿Está Seguro de desactivar la Comision?", function(result){
		if(result)
        {
        	$.post("../ajax/comisiones.php?op=desactivar", {idcomisiones : idcomisiones}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(idcomisiones)
{
	bootbox.confirm("¿Está Seguro de activar la comision?", function(result){
		if(result)
        {
        	$.post("../ajax/comisiones.php?op=activar", {idcomisiones : idcomisiones}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}
function buscarcodigo()
{
	var codigo=$('select[id=idarticulos]').val();
	//var codigo = $('#idarticulos').val();
	listar(codigo);
	//alert ("evento change "+codigo);

}




init();