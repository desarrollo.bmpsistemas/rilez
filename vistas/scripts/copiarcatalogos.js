var tabla;
var codigo=0;
var idalmacenes=1;
var idsublineas=1;
//Función que se ejecuta al inicio
function init(){
	 $("#modulo").html("Abc de Copias de Catalogos");
	mostrarform(false); 
	
	
	//$("#edades").show();
	//alert("hola "+codigo);
    listar(idalmacenes,idsublineas);

	/*$.post("../ajax/articulos.php?op=selectcatalogo", function(r){
	            $("#idarticulos").html(r);
	            $('#idarticulos').selectpicker('refresh');

	});*/

	/*$.post("../ajax/articulos_almacenes.php?op=select", function(r){
	            $("#idalmacenes").html(r);
	            $('#idalmacenes').selectpicker('refresh');

	});*/
	$.post("../ajax/lineas.php?op=select", function(r){
	            $("#idlineas").html(r);
	            $('#idlineas').selectpicker('refresh');
	            $("#idlineas2").html(r);
	            $('#idlineas2').selectpicker('refresh');

	});
	
	$.post("../ajax/almacenes.php?op=select", function(r){
		            $("#idalmacenes").html(r);
		            $('#idalmacenes').selectpicker('refresh');
		            $("#idalmacenes2").html(r);
		            $('#idalmacenes2').selectpicker('refresh');

	});


	
    $("#utilidad").keyup(function(e) {
        calcularPrecio();
    });
	

	$("#formulario").on("submit",function(e)
	{
		idarticulos_almacenes=$("#idarticulos_almacenes").val();
		//alert("id articulos almacenes  "+idarticulos_almacenes);
		guardaryeditar(e);	
	})
}

//Función limpiar
function limpiar()
{
	$("#nombre").val("");
	$("#idalmacenes").val("");
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();

	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
		$("#idarticulos").hide();
		$("#idarticulos_almacenes").val("");
		$("#articulos").css("display", "none");
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
		$("#articulos").css("display", "");
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}



//Función Listar
function listar(idalmacenes,idsublineas)
{
	//alert("id almacenes "+idalmacenes+" id sublineas "+idsublineas);
	tabla=$('#tbllistado').dataTable(
	{
		"scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false,

		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'

		            
		        ],
		"ajax":
				{
					url: '../ajax/copiarcatalogos.php?op=listar',
					data:{idalmacenes: idalmacenes,idsublineas:idsublineas},
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar


//Función para activar registros


function selectsublinea(e)
{
	if (e==1)
		idlineas=$('select[id=idlineas]').val();
	else
		idlineas=$('select[id=idlineas2]').val();	
	//Cargamos los items al select estados
	//alert("id lineas "+idlineas);
	$.post("../ajax/sublineas.php?op=selectporlinea", {idlineas : idlineas}, function(r){
		//alert("resultado "+r);
				if (e==1)
				{
	            	$("#idsublineas").html(r);
	            	$('#idsublineas').selectpicker('refresh');
				}
				else
				{
					$("#idsublineas2").html(r);
	            	$('#idsublineas2').selectpicker('refresh');	
				}
	});
}

function selectarticulosporsublinea(e)
{
	if (e==1)
	{
		idalmacenes=$('select[id=idalmacenes]').val();
		idsublineas=$('select[id=idsublineas]').val();
		listar(idalmacenes,idsublineas);
	}
	if (e==2)
	{
		idsublineas=$('select[id=idsublineas2]').val();
		idalmacenes=$('select[id=idalmacenes2]').val();
		listar(idalmacenes,idsublineas);
	}
	
}
function procesar()
{
	idalmacenesf=$('select[id=idalmacenes]').val();
	idlineasf=$('select[id=idlineas]').val();
	idsublineasf=$('select[id=idsublineas]').val();
	idalmacenesd=$('select[id=idalmacenes2]').val();
	idlineasd=$('select[id=idlineas2]').val();
	idsublineasd=$('select[id=idsublineas2]').val();
	operacion=$("#operacion").val();
	continua="S";
	if (operacion=='almacen' && (idalmacenesf==idalmacenesd))
	{
		alert("No se debe copiar un almacen dentro del mismo almacen");
		continua="N";
	}
	if (operacion=='linea' && (idalmacenesf==idalmacenesd) && continua=='S')
	{
		alert("No se debe copiar una linea dentro del mismo almacen");
		continua="N";
	}
	if (operacion=='linea' && (idlineasf!=idlineasd) && continua=='S')
	{
		alert("Para copiar una linea a otro almacen debe ser la misma linea entre ambos almacenes ");
		continua="N";
	}
	if (operacion=='sublinea' && (idlineasf!=idlineasd || idsublineasf!=idsublineasd ) && continua=='S' )
	{
		alert("Para copiar una sublinea a otro almacen debe la misma linea y sublinea entre ambos almacenes ");
		continua="N";
	}
	//alert("af "+idalmacenesf+" ilf "+idlineasf+" isf "+idsublineasf+" ad "+idalmacenesd+" ild "+idlineasd+" idsd "+idsublineasd+" operacion "+operacion);
	if (continua=='S')
	{
		$.post("../ajax/copiarcatalogos.php?op=procesar", 
			{idalmacenesf : idalmacenesf,idlineasf:idlineasf,idsublineasf : idsublineasf,
			 idalmacenesd : idalmacenesd,idlineasd:idlineasd,idsublineasd : idsublineasd,operacion:operacion}, function(r){
			listar(idalmacenesd,idsublineasd);

		});
	}
}

init();