var tabla;
var nombre="";
//Función que se ejecuta al inicio
function init(){
	 $("#modulo").html("Abc de Destinos");
	mostrarform(false);
	listar(nombre);

	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	})

}

//Función limpiar

function limpiar()
{
	$("#nombre").val("");
	$("#iddestinos").val("");
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}
function buscar()
{
	//alert("buscando");
	nombre=$("#nombre").val();
   
   	listar(nombre);
}
//Función Listar
function listar(nombre)
{
	tabla=$('#tbllistado').dataTable(
	{
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla

	    "scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false,
        
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],
		"ajax":
				{
					url: '../ajax/destinos.php?op=listar',
					data:{nombre:nombre},
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar
function altaDeColonias()
{
	//alert("buscando ");
	//window.open('../vistas/colonias.php','BmpSistemas', 'width=800, height=600');
	
}
function guardaryeditar(e)
{
	//alert("parametro "+e);
	//e.preventDefault(); //No se activará la acción predeterminada del evento
	//$("#btnGuardar").prop("disabled",true);
	//if (e==1)
	//	$("#iddestinos").val(0);
	iddestinos=$("#iddestinos").val();
	nombre=$("#nombre").val();
	//alert("parametro "+e+" id col "+iddestinos+" calle "+nombre);
	$.post("../ajax/destinos.php?op=guardaryeditar",{Opcion:e,iddestinos : iddestinos,nombre:nombre}, function(data, status)
	{
		mensaje(data);
		listar(nombre);
	    //tabla.ajax.reload();
	    
 	});
 	$("#iddestinos").val(0);

	
}

function mostrar(iddestinos)
{
	$.post("../ajax/destinos.php?op=mostrar",{iddestinos : iddestinos}, function(data, status)
	{
		data = JSON.parse(data);		
		//mostrarform(true);

		$("#nombre").val(data.nombre);
		$("#iddestinos").val(data.iddestinos);


 	})
}

//Función para desactivar registros
function desactivar(iddestinos)
{
	bootbox.confirm("¿Está Seguro de desactivar la calle?", function(result){
		if(result)
        {
        	$.post("../ajax/destinos.php?op=desactivar", {iddestinos : iddestinos}, function(e){
        		mensaje(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(iddestinos)
{
	bootbox.confirm("¿Está Seguro de activar la calle?", function(result){
		if(result)
        {
        	$.post("../ajax/destinos.php?op=activar", {iddestinos : iddestinos}, function(e){
        		mensaje(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

function mensaje(titulo)
{
	Swal.fire({
		  position: 'top-end',
		  type: 'success',
		  title: titulo,
		  showConfirmButton: false,
		  timer: 1500
		})
}
init();