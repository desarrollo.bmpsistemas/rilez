var tabla;
var codigo=1;
var idarticulos=0;
//$('#idmatrices').val(codigo);
//Función que se ejecuta al inicio 
function init(){
	 $("#modulo").html("Abc de Detalles de Matrices"); 
	mostrarform(false);
	
	
	//$("#edades").show();
	//alert("hola "+codigo); 
    listar(codigo);

	

	/*$.post("../ajax/matrices.php?op=select", function(r){
	            $("#idmatrices").html(r);
	            $('#idmatrices').selectpicker('refresh');

	});*/
	
	//$.post("../ajax/articulos.php?op=selectcatalogo", function(r){
	//            $("#idarticulos").html(r);
	//            $('#idarticulos').selectpicker('refresh');

	//});
	

	$("#formulario").on("submit",function(e)
	{
		//idarticulos_proveedores=$("#idarticulos_proveedores").val();
		//idarticulos=$("#idarticulos").val();

		//alert("id articulos proveedores  "+idarticulos_proveedores+" id articulos "+idarticulos);
		guardaryeditar(e);	
	})
}

//Función limpiar
function limpiar()
{
	$("#nombre").val("");
	$("#idalmacenes").val("");
}
function establecerFocus(e)
{
	Opcion=e;

}

function buscarArticulo(e)
{
	
	//alert("entro ");
	if (Opcion==1)
	{
		referencia=$("#codigo").val(); 
	}
	if (Opcion==2)
	{
		referencia=$("#nombre").val(); 
	}
	if (Opcion==3)
	{
		referencia=$("#codigo2").val();
		Opcion=1; 
	}
	if (Opcion==4)
	{
		referencia=$("#nombre2").val(); 
		Opcion=2;	
	}

	

    //alert("valores Opcion "+Opcion+" ref "+referencia);   
   	$.post("../ajax/articulos.php?op=select", {Opcion : Opcion,referencia:referencia}, function(r){
		//alert("resultado "+r);
		{
			if (e==1)
			{
		        $("#idmatrices").html(r);
		        $('#idmatrices').selectpicker('refresh');
	    	}
	    	else
	    	{
	    		$("#idarticulos").html(r);
		        $('#idarticulos').selectpicker('refresh');	
	    	}

	    }

	});
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();

	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
		$("#iddetalles_matrices").val(0);
		
		$("#divCaptura1").hide();
		//$("#idarticulos").hide();
		//$("#idarticulos_proveedores").val("");
		$("#articulos").css("display", "none");

		
		$("#idmatrices").attr('disabled', true);
		$("#idmatrices").css("display", 'none');
        $("#idmatrices").hide();
               

	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
		$("#articulos").css("display", "");

		$("#divCaptura1").show();
		$("#idmatrices").attr('disabled', false);
		$("#idmatrices").css("display", 'none');
        $("#idmatrices").hide();
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}

//Función Listar
function listarDetallesDeMatriz()
{
	var idmatrices=$('select[id=idmatrices]').val();
	listar(idmatrices);
}
function listar(codigo)
{
	//alert("lista funcion datatable "+codigo);
	
	tabla=$('#tbllistado').dataTable(
	{
		"scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false,

		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'

		            
		        ],
		"ajax":
				{
					url: '../ajax/detalles_matrices.php?op=listar',
					data:{idmatrices:codigo},
					type : "get",
					dataType : "json",						
					error: function(e){

						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar

function guardaryeditar(e)
{
	var idmatrices=$('select[id=idmatrices]').val();
	$('#idmatriz').val(idmatrices);
	var dataString = $('#formulario').serialize();
    //alert('Datos del formulario: '+dataString);

	e.preventDefault(); //No se activará la acción predeterminada del evento
	//$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]);

	$.ajax({
		url: "../ajax/detalles_matrices.php?op=guardaryeditar",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {                    
	    	  listar(idmatrices);
	    	  	Swal.fire({
				  position: 'top-end',
				  type: 'success',
				  title: 'Sus Datos se han  guardado',
				  showConfirmButton: false,
				  timer: 1500
				})
	          //bootbox.alert(datos);	          
	          //mostrarform(false);
	          tabla.ajax.reload();
	    }

	});
	limpiar();
}



function mostrar(iddetalles_matrices)
{
	
	
	//alert("articulos_provedores parametros "+idarticulos_proveedores);
	

	$.post("../ajax/detalles_matrices.php?op=mostrar",{iddetalles_matrices : iddetalles_matrices}, function(data, status)
	{
		data = JSON.parse(data);		
		mostrarform(true);
		
		//alert("cantidad "+data.cantidad)
		$("#costo").val(data.costo);
		$("#cantidad").val(data.cantidad);
		$("#codigo2").val(data.codigo);
		$("#nombre2").val(data.nombre);
		//alert("nombre que llego "+data.nombre)
		idarticulos=data.idarticulos;
		Opcion=3;
		$("#iddetalles_matrices").val(data.iddetalles_matrices);
		buscarArticulo(2);
		

		//$('#idarticulos').selectpicker('refresh');
		
		
		
 	})
}

//Función para desactivar registros
function desactivar(iddetalles_matrices)
{
	//bootbox.confirm("¿Está Seguro de desactivar la articulo por proveedor ?", function(result){
	//	if(result)
        {
        	$.post("../ajax/detalles_matrices.php?op=desactivar", {iddetalles_matrices : iddetalles_matrices}, function(e){
        		//bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	//})
}
function guardarDetalles(e)
{
	costo=$("#costo").val();
	cantidad=$("#cantidad").val();
	iddetalles_matrices=$("#iddetalles_matrices").val();
	idmatrices=$("#idmatrices").val();
	idarticulos=$('select[id=idarticulos]').val();
	//alert("iddetmat "+iddetalles_matrices+" cant "+cantidad+" costo "+costo+" id art "+idarticulos);
	
    {
    	$.post("../ajax/detalles_matrices.php?op=guardaryeditar", 
    		{iddetalles_matrices : iddetalles_matrices,idmatrices:idmatrices,idarticulos:idarticulos,cantidad:cantidad,costo:costo}, 
    		function(e){
    		bootbox.alert(e);
            tabla.ajax.reload();
    	});	
    }
	//})
}

//Función para activar registros
function activar(iddetalles_matrices)
{
	//bootbox.confirm("¿Está Seguro de desactivar la articulo por proveedor ?", function(result){
	//	if(result)
        {
        	$.post("../ajax/detalles_matrices.php?op=desactivar", {iddetalles_matrices : iddetalles_matrices}, function(e){
        		//bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	//})
}
function buscarcodigo()
{
	codigo=$('select[id=idmatrices]').val();
	$('#idmatriz').val(codigo);
	//alert("id persoan "+codigo);
	listar(codigo);
	
	
}
init();