var tabla;

//Función que se ejecuta al inicio
function init(){
$("#modulo").html("Abc de Detalles de Monedas");
var fecha="2019-03-25";
var hoy = new Date();
var dd = hoy.getDate();
var mm = hoy.getMonth()+1;
var yyyy = hoy.getFullYear();

var fecha;

fecha = yyyy+'-'+mm+'-'+dd;
//alert(" fecha "+fecha);
console.log(fecha);

	mostrarform(false);
	listar(fecha);

	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	})

	//Cargamos los items al select lineas
	$.post("../ajax/usuarios.php?op=select", function(r){
	            $("#idusuarios").html(r);
	            $('#idusuarios').selectpicker('refresh');

	});
	//$("#imagenmuestra").hide();
}

//Función limpiar
function limpiar()
{
	$("#valor").val("");
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}

//Función Listar
function listar(fecha)
{
	//alert("fecha recibida " +fecha);
	tabla=$('#tbllistado').dataTable(
	{
		"scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false,

		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'

		            
		        ],
		"ajax":
				{
					url: '../ajax/detalles_monedas.php?op=listar',
					data:{fecha: fecha},
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}


//Función para guardar o editar

function guardaryeditar(e)
{
	e.preventDefault(); //No se activará la acción predeterminada del evento
	$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]);

	$.ajax({
		url: "../ajax/detalles_monedas.php?op=guardaryeditar",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {                    
	          bootbox.alert(datos);	          
	          mostrarform(false);
	          tabla.ajax.reload();
	    }

	});
	limpiar();
}

function mostrar(iddetallesmonedas)
{
	$.post("../ajax/detalles_monedas.php?op=mostrar",{iddetallesmonedas : iddetallesmonedas}, function(data, status)
	{
		data = JSON.parse(data);		
		mostrarform(true);
		
		$("#idusuarios").val(data.idusuarios);
		$('#idusuarios').selectpicker('refresh');
		$("#fecha").val(data.fecha);
		$("#valor").val(data.valor);
 	})
}

function buscarporfecha()
{
	var fecha=$("#fecha").val();
	//var codigo = $('#idarticulos').val();
	//alert ("fecha funcio "+fecha);
	listar(fecha);
	//alert ("evento change "+codigo);

}
init();