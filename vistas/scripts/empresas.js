var tabla;

//Función que se ejecuta al inicio
function init(){
	 $("#modulo").html("Abc de Empresas");
	mostrarform(false);
	listar();

//Cargamos los items al select pises
	$.post("../ajax/municipios.php?op=selectmunicipios", function(r){
	            $("#idmunicipios").html(r);
	            $('#idmunicipios').selectpicker('refresh');

	});

	$.post("../ajax/colonias.php?op=select", function(r){
	            $("#idcolonias").html(r);
	            $('#idcolonias').selectpicker('refresh');

	});

	$.post("../ajax/calles.php?op=select", function(r){
	            $("#idcalles").html(r);
	            $('#idcalles').selectpicker('refresh');

	});


	$.post("../ajax/vendedores.php?op=select", function(r){
	            $("#idvendedores").html(r);
	            $('#idvendedores').selectpicker('refresh');

	});

	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	})
}

//Función limpiar
function limpiar()
{
	$("#nombre").val("");
	$("#num_documento").val("");
	$("#direccion").val("");
	$("#telefono").val("");
	$("#email").val("");
	$("#idpersonas").val("");
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}

//Función Listar
function listar()
{
	tabla=$('#tbllistado').dataTable(
	{
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],
		"ajax":
				{
					url: '../ajax/personas.php?op=listarc',
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar

function guardaryeditar(e)
{
	e.preventDefault(); //No se activará la acción predeterminada del evento
	$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]);

	$.ajax({
		url: "../ajax/personas.php?op=guardaryeditar",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {                    
	          bootbox.alert(datos);	          
	          mostrarform(false);
	          tabla.ajax.reload();
	    }

	});
	limpiar();
}

function mostrar(idpersonas)
{
	$.post("../ajax/personas.php?op=mostrar",{idpersonas : idpersonas}, function(data, status)
	{
		data = JSON.parse(data);		
		mostrarform(true);
		
		$("#nombre").val(data.nombre);
		$("#nombrecomercial").val(data.nombrecomercial);
		$("#rfc").val(data.rfc);
		$("#idmunicipios").val(data.idmunicipios);
		$("#idmunicipios").selectpicker('refresh');
		$("#idcolonias").val(data.idcolonias);
		$("#idcolonias").selectpicker('refresh');
		$("#idcalles").val(data.idcalles);
		$("#idcalles").selectpicker('refresh');
		$("#idvendedores").val(data.idvendedores);
		$("#idvendedores").selectpicker('refresh');
		$("#numero_interior").val(data.numero_interior);
		$("#numero_exterior").val(data.numero_exterior);
		$("#referencia").val(data.referencia);
		$("#codigo_postal").val(data.codigo_postal);
		$("#correo").val(data.correo);
		$("#telefono").val(data.telefono);
		$("#diascredito").val(data.diascredito);
		$("#limitecredito").val(data.limitecredito);
		$("#tipodepersona").val(data.tipodepersona);
		$("#idpersonas").val(data.idpersonas);

 	})
}

//Función para desactivar registros
function desactivar(idpersonas)
{
	bootbox.confirm("¿Está Seguro de desactivar el estado cliente?", function(result){
		if(result)
        {
        	$.post("../ajax/personas.php?op=desactivar", {idpersonas : idpersonas}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(idpersonas)
{
	bootbox.confirm("¿Está Seguro de activar el estado del cliente?", function(result){
		if(result)
        {
        	$.post("../ajax/personas.php?op=activar", {idpersonas : idpersonas}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}



init();