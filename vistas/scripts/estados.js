var tabla;

//Función que se ejecuta al inicio
function init(){
	 $("#modulo").html("Abc de Estados");
	mostrarform(false);
	listar();

	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	})

	//Cargamos los items al select lineas
	$.post("../ajax/municipios.php?op=selectpaises", function(r){
	            $("#idpaises").html(r);
	            $('#idpaises').selectpicker('refresh');

	});
	$.post("../ajax/paises.php?op=select", function(r){
	            $("#idpaises").html(r);
	            $('#idpaises').selectpicker('refresh');

	});
	//$("#imagenmuestra").hide();
}

//Función limpiar
function limpiar()
{
	
	$("#nombre").val("");
	$("#idestados").val("");
	
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}

//Función Listar
function listar()
{
	tabla=$('#tbllistado').dataTable(
	{
		"scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false,

		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    buttons: [
            {
                text: 'Rpt Por Nombre',
                action: function  ( e, dt, node, conf ) {
                    reportePorNombre();
                  
                }
            },
            {
                text: 'Rpt Por País',
                action: function  ( e, dt, node, conf ) {
                    reportePorCodigo();
                  
                }
            },
            
            
            
        ],
		"ajax":
				{
					url: '../ajax/estados.php?op=listarestados',
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar

function guardaryeditar(e)
{
	e.preventDefault(); //No se activará la acción predeterminada del evento
	$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]);

	$.ajax({
		url: "../ajax/estados.php?op=guardaryeditar",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {                    
	          bootbox.alert(datos);	          
	          mostrarform(false);
	          tabla.ajax.reload();
	    }

	});
	limpiar();
}

function mostrar(idestados)
{
	$.post("../ajax/estados.php?op=mostrar",{idestados : idestados}, function(data, status)
	{
		data = JSON.parse(data);		
		mostrarform(true);
		
		$("#idpaises").val(data.idpaises);
		$('#idpaises').selectpicker('refresh');
		
		$("#nombre").val(data.estado);
		$("#idestados").val(data.idestados);
 		//generarbarcode();

 	})
}

//Función para desactivar registros
function desactivar(idestados)
{
	bootbox.confirm("¿Está Seguro de desactivar el Estado?", function(result){
		if(result)
        {
        	$.post("../ajax/estados.php?op=desactivar", {idestados : idestados}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(idestados)
{
	bootbox.confirm("¿Está Seguro de activar el Estado?", function(result){
		if(result)
        {
        	$.post("../ajax/estados.php?op=activar", {idestados : idestados}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload(); 
        	});	
        }
	})
}

//función para generar el código de barras

init();