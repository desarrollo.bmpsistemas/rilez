var tabla;
var archivo="";
var contador=0;
//Función que se ejecuta al inicio 
function init(){
	mostrarform(false);
	listar(archivo);
	$("#modulo").html("Abc de Xml Compras");
	//alert("iniciando");
	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	})
	$.post("../ajax/almacenes.php?op=select", function(r){
	            $("#idalmacenes").html(r);
	            $('#idalmacenes').selectpicker('refresh');

	});
	
	//Cargamos los items al select lineas
	
	

	//$("#imagenmuestra").hide();
}

//Función limpiar
function limpiar()
{
	
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		//$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}

//Función Listar
function listar(archivo)
{
	tabla=$('#tbllistado').dataTable(
	{

		"scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false,

		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    buttons: [	

		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],
		"ajax":
				{
					url: '../ajax/filexml.php?op=listar&archivo='+archivo,
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar

function migrarArticulos(e)
{
	
    fic=$("#imagen").val();
    fic = fic.split('\\');
  	archivo=(fic[fic.length-1]);
    
  	//alert("archivo a migrar "+archivo);
    bootbox.confirm("¿Está Seguro de migrar los artículo del xml ?", function(result){
		if(result)
        {
        	$.post("../ajax/articulos.php?op=migrarArticulos", {nombre:archivo}, function(e){
        		bootbox.alert(e);
        		mostrarMensaje(e);
	            //tabla.ajax.reload();
        	});	
        }
	})
}
function migrarPrecios(e)
{
	
    fic=$("#imagen").val();
    fic = fic.split('\\');
  	archivo=(fic[fic.length-1]);
    afectacion=$("#afectacion").val();
    idalmacenes=$("#idalmacenes").val();
  	//alert("archivo a migrar "+archivo+" afectacion "+afectacion+"almacen id "+idalmacenes);
    bootbox.confirm("¿Está Seguro de migrar los Precios  ?", function(result){
		if(result)
        {
        	$.post("../ajax/articulos_almacenes.php?op=migrarPrecios", {archivo:archivo,afectacion:afectacion,idalmacenes:idalmacenes}, function(e){
        		mostrarMensaje(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}
function guardaryeditar(e)
{
	
    fic=$("#imagen").val();
    fic = fic.split('\\');
  	archivo=(fic[fic.length-1]);
  	//archivo="0123456789.xml";
	var dataString = $('#formulario').serialize();

    //alert('Datos serializados: '+dataString+" "+archivo);

    //alert("archivo "+archivo);
	e.preventDefault(); //No se activará la acción predeterminada del evento
		//$("#btnGuardar").prop("disabled",true);
		var formData = new FormData($("#formulario")[0]);
		//alert("enviando datos "+formData);
		$.ajax({
			url: "../ajax/filexml.php?op=guardaryeditar&archivo="+archivo,
		    type: "POST",
		    data: formData,
		    contentType: false,
		    processData: false,

		    success: function(datos)
		    {                    
		    	if (datos.length>5)
		          alert(" Mensaje: "+datos);	 
		          //mostrarMensaje("Datos Procesados");
		          listar(archivo);
		          //mostrarform(false);
		          //tabla.ajax.reload();
		    }

		});
}
function mostrarMensaje(mensaje)
{
	swal({   
		title: mensaje,   
		text: ":",   
		timer: 2500,   
		showConfirmButton: true 
	});
}
function mostrar(idarticulos)
{
	$.post("../ajax/articulos.php?op=mostrar",{idarticulos : idarticulos}, function(data, status)
	{
		data = JSON.parse(data);		

		mostrarform(true);


		$("#idsublineas").val(data.idsublineas);
		$('#idsublineas').selectpicker('refresh');
		$("#codigo").val(data.codigo);
		$("#nombre").val(data.nombre);
		$("#idsat_unidad_medida").val(data.sat_unidad_medida_id);
		$('#idsat_unidad_medida').selectpicker('refresh');

		$("#idsat_prod_serv").val(data.sat_prod_serv_id);
		$('#idsat_prod_serv').selectpicker('refresh');

		$("#producible").val(data.producible);
		$("#vendible").val(data.vendible);
		$("#cambio_precio").val(data.cambio_precio);
		$("#inventariable").val(data.inventariable);
		$("#peso").val(data.peso);
		$("#volumen").val(data.volumen);

		$("#imagenmuestra").show();
		$("#imagenmuestra").attr("src","../files/articulos/"+data.imagen);
		$("#imagenactual").val(data.imagen);
 		$("#idarticulos").val(idarticulos); //data.idarticulo
 		generarbarcode();

 	})
}

//Función para desactivar registros
function desactivar(idarticulos)
{
	bootbox.confirm("¿Está Seguro de desactivar el artículo?", function(result){
		if(result)
        {
        	$.post("../ajax/articulos.php?op=desactivar", {idarticulos : idarticulos}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(idarticulos)
{
	bootbox.confirm("¿Está Seguro de activar el Artículo?", function(result){
		if(result)
        {
        	$.post("../ajax/articulos.php?op=activar", {idarticulos : idarticulos}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload(); 
        	});	
        }
	})
}

function selectsublinea()
{
	idlineas=$('select[id=idlineas]').val();
	//Cargamos los items al select estados
	//alert("id lineas "+idlineas);
	$.post("../ajax/sublineas.php?op=selectporlinea", {idlineas : idlineas}, function(r){
		//alert("resultado "+r);
	            $("#idsublineas").html(r);
	            $('#idsublineas').selectpicker('refresh');

	});
}


function grabarArticulo()
{
		
		idsublineas=$("#idsublineas").val();
		codigo=$("#codigo").val();
		nombre=$("#nombre").val();
		idsat_unidad_medida=$("#idsat_unidad_medida").val();
		idsat_prod_serv=$("#idsat_prod_serv").val();
		producible=$("#producible").val();
		vendible=$("#vendible").val();
		cambio_precio=$("#cambio_precio").val();
		inventariable=$("#inventariable").val();
		peso=$("#peso").val();
		volumen=$("#volumen").val();
		imagen = $('#imagen').val();
		
        if (imagen.substring(3,11) == 'fakepath') {
            imagen = imagen.substring(12);
		}
		imagenactual=imagen;
		idarticulos=$("#idarticulos").val();
		//alert("hola este es el archivo "+imagen);
		/*e.preventDefault(); //No se activará la acción predeterminada del evento
		$("#btnGuardar").prop("disabled",true);
		var formData = new FormData($("#formulario")[0]);*/

		/*$.ajax({
			url: "../ajax/articulos.php?op=guardaryeditar",
		    type: "POST",
		    data: formData,
		    contentType: false,
		    processData: false,

		    success: function(datos)
		    {                    
		          bootbox.alert(datos);	          
		          //mostrarform(false);
		          //tabla.ajax.reload();
		    }

		});
		limpiar();*/
	
 		
 		$.post("../ajax/articulos.php?op=guardaryeditar",
 			{idsublineas:idsublineas,
	 			codigo:codigo,
	 			nombre:nombre,
	 			idsat_unidad_medida:idsat_unidad_medida,
	 			idsat_prod_serv:idsat_prod_serv,
	 			producible:producible,
	 			vendible:vendible,
	 			cambio_precio:cambio_precio,
	 			inventariable:inventariable,
	 			peso:peso,
	 			volumen:volumen,
	 			imagen:imagen,
	 			imagenactual:imagenactual,
	 			idarticulos:idarticulos
 			}, 
 			function(data, status)
			{
		
				//alert("resultado "+data);
				listar();
				Swal.fire({
				  position: 'top-end',
				  type: 'success',
				  title: 'Sus Datos se han  guardado',
				  showConfirmButton: false,
				  timer: 1500
			})

 	})
	

}


function selectarticulo()
{
	idarticulos=$('select[id=idarticulos]').val();
	mostrar(idarticulos)
}
function selectarticulosporsublinea()
{
	idsublineas=$('select[id=idsublineas]').val();
	//Cargamos los items al select estados
	//alert("id sublineas "+idsublineas);
	$.post("../ajax/articulos.php?op=selectarticulosporsublinea", {idsublineas : idsublineas}, function(r){
		//alert("resultado "+r);
	            $("#idarticulosc").html(r);
	            $('#idarticulosc').selectpicker('refresh');

	});
}


function selectunidad()
{
	idsat_unidad_medida=$('select[id=idsat_unidad_medida]').val();
	//Cargamos los items al select estados
	//alert("id sat unidad medida "+idsat_unidad_medida);
	/*$.post("../ajax/sat_unidad_medidas.php?op=selectporclave", {idsat_unidad_medida : idsat_unidad_medida}, function(r){
		//alert("resultado "+r);
	            $("#idsat_unidad_medida").html(r);
	            $('#idsat_unidad_medida').selectpicker('refresh');

	});*/
}

function selectprodserv()
{
	idsat_prod_serv=$('select[id=idsat_prod_serv]').val();
	//Cargamos los items al select estados
	//alert("id lineas "+idlineas);
	/*$.post("../ajax/sat_prod_servs.php?op=selectporclave", {idsat_prod_serv : idsat_prod_serv}, function(r){
		//alert("resultado "+r);
	            $("#idsat_prod_serv").html(r);
	            $('#idsat_prod_serv').selectpicker('refresh');

	});*/
}

function selectcatalogo()
{
	
	$.post("../ajax/articulos.php?op=selectcatalogo", function(r){
		
	            $("#idarticulos").html(r);
	            $('#idarticulos').selectpicker('refresh');

	});
}

//función para generar el código de barras
function generarbarcode()
{
	codigo=$("#codigo").val();
	JsBarcode("#barcode", codigo);
	$("#print").show();
}

//Función para imprimir el Código de barras
function imprimir()
{
	$("#print").printArea();
}

init();