var tabla;
var nombre="";
var referencia=""; 
var idalmacenes=1; 
var gasto="O";
var idusuarios=1;
var fecha;
var modulo="Abc de Gastos";
//Función que se ejecuta al inicio
function init(){
	 $("#modulo").html("Abc de gastos");

	mostrarform(false);
	$('#idgastos').val(0);
	//idalmacenes=$('#Sidalmacenes').val();
	idusuarios=$('#idusuarios').val();
	idalmacenes=$('#Sidalmacenes').val();
	
	$('input:checkbox[name=gastonormal]').attr('checked',true);	
	
	tomar_fecha();
	//$("#fecha").val(fecha);
	//listar(0,fecha,idalmacenes);
	
	$("#lblModulo").val(modulo); 

	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	})
	$("#btnEnviarXml").on("submit",function(e)
	{
		enviarXml(e);	
	})
	
	$.post("../ajax/almacenes.php?op=select", function(r){
	    $("#idalmacenes").html(r);
	    $('#idalmacenes').selectpicker('refresh');
	   
	});
	$.post("../ajax/cat_de_gastos.php?op=select", function(r){
		//alert("gastos "+r);
	    $("#idcat_de_gastos").html(r);
	    $('#idcat_de_gastos').selectpicker('refresh');
	   
	});
	/*$.post("../ajax/ivas.php?op=select", function(r){
	    $("#idivas").html(r);
	    $('#idivas').selectpicker('refresh');
	   
	});
	$.post("../ajax/monedas.php?op=select", function(r){
	    $("#idmonedas").html(r);
	    $('#idmonedas').selectpicker('refresh');
	   
	});*/
	$.post("../ajax/monedas.php?op=formasdepagos", function(r){
	    $("#idformasdepagos").html(r);
	    $('#idformasdepagos').selectpicker('refresh');
	   
	});
	//lecturaEscritura('fecha',0);

	//lecturaEscritura('idalmacenes',0);
	//lecturaEscritura('referencia',0);
	//lecturaEscritura('tipodegasto',0);
	//lecturaEscritura('archivoxml',0);
	//lecturaEscritura('idpersonas',0);
	lecturaEscritura('UUID',0);
	//lecturaEscritura('idcat_de_gastos',0);
	//lecturaEscritura('concepto',0);
	//lecturaEscritura('idtipodepagos',0);
	lecturaEscritura('SatClaveFormaDePago',0);
	lecturaEscritura('importe',0);
	lecturaEscritura('iva',0);
	//lecturaEscritura('ajuste',0);
	lecturaEscritura('subtotal',0);
	lecturaEscritura('SatNombreFormaDePago',0);
	lecturaEscritura('isrretenido',0);
	lecturaEscritura('ivaretenido',0);
	lecturaEscritura('total',0);
	lecturaEscritura('moneda',0);
	lecturaEscritura('tasaocuota',0);

}
function cambiarEstatusCampos()
{
	valor=$("#tipodegasto").val();
	//alert("valor "+valor);
	if (valor=='S')
		cambiarEstatus(0);
	else
		cambiarEstatus(1);
		
}
function cambiarEstatus(estatus)
{
	lecturaEscritura('btnGrabargasto2',0);

	//lecturaEscritura('idalmacenes',0);
	//lecturaEscritura('referencia',0);
	//lecturaEscritura('tipodegasto',0);
	//lecturaEscritura('archivoxml',estatus);
	//lecturaEscritura('idpersonas',0);
	lecturaEscritura('uuid',estatus);
	//lecturaEscritura('idcat_de_gastos',0);
	lecturaEscritura('concepto',estatus);
	lecturaEscritura('rfc',estatus);
	lecturaEscritura('SatClaveFormaDePago',estatus);
	lecturaEscritura('importe',estatus);
	lecturaEscritura('iva',estatus);
	lecturaEscritura('nombre',estatus);
	lecturaEscritura('subtotal',estatus);
	lecturaEscritura('SatNombreFormaDePago',estatus);
	lecturaEscritura('isrretenido',estatus);
	lecturaEscritura('ivaretenido',estatus);
	lecturaEscritura('total',estatus);
	lecturaEscritura('moneda',estatus);
	lecturaEscritura('tasaocuota',estatus);
	limpiar();
}
//Función limpiar
function fuera()
{
	//alert("sin resultados ");
	/*$("#idalmacenes").chosen({

        }).on('chosen:hiding_dropdown', function(){

           alert($('#idalmacenes .search-field input').val());

        });*/
}
function limpiar()
{
	$("#referencia").val("");
	//$("#archivoxml").val("SIN XML");
	$("#uuid").val("");
	$("#concepto").val("");
	$("#rfc").val("");
	$("#SatClaveFormaDePago").val("");
	$("#SatNombreFormaDePago").val("");
	$("#importe").val(0);
	$("#iva").val(0);
	$("#ajuste").val(0);
	$("#subtotal").val(0);
	$("#total").val(0);
	$("#ivaretenido").val(0);
	$("#isrretenido").val(0);
	$("#moneda").val("");
	$("#tasaocuota").val("");
	$("#nombre").val("");
}
function verificarAlmacen()
{
	
	
	//id=$('select[id=idalmacenes]').val();
	
	//alert("Almacen selecionado invalido "+id);
	
	
}

function tipoDeGastos()
{

	/*tipo=$('input:checkbox[name=gastonormal]:checked').val();
	//alert("impresion  "+impresion);
	if (tipo==1)
		$("#tipodegasto").val('O');
	else
		$("#tipodegasto").val('A');*/

	
	
}



function listarGastos()
{
	
	fecha=$("#fecha").val();
	idalmacenes=$('select[id=idalmacenes]').val();
	//alert("fecha "+fecha);
	
	listar(0,fecha,idalmacenes);
	
}

function tomar_fecha()
{
	//var fecha;
	fecha=formato_fecha();
	//$('#fecha_inicial').val(fecha);
	//$('#fecha_final').val(fecha);
	$('#fecha').val(fecha);
	//$('#fechainicial').val(fecha);
	//$('#fechafinal').val(fecha);
	//alert("feecha "+fecha);
}
function formato_fecha()
{
	var now = new Date();
	var day = ("0" + now.getDate()).slice(-2);
	var month = ("0" + (now.getMonth() + 1)).slice(-2);
	var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
    return today;
}

//Función mostrar formulario
function mostrarform(flag) 
{
	limpiar();
	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}
function buscar()
{
	nombre=$("#nombreb").val();
   
   	//listar(nombre);
}
//Función Listar
function listar(idgastos,fecha,idalmacenes)
{
	fecha=$("#fecha").val();
	fecha_inicial=$("#fecha").val();
	fecha_final=$("#fecha").val();
	idalmacenes=$('select[id=idalmacenes]').val();
	

	///alert("almacen "+idalmacenes+" fecha "+fecha);
		
	tabla=$('#tbllistado').dataTable(
	{
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    "scrollX": true,

	    "scrollY":        "300px",
        "scrollCollapse": true,
        "paging":         false,

	   buttons: [
            
            {
                text: 'Ver',
                action: function ( e, dt, node, conf ) {
                	listarGastos();
                    //console.log( 'Button 2 clicked on' );
                }
            },
            
            
            {
                text: 'Cancelar',
                action: function ( e, dt, node, conf ) {
                	cancelarCaptura();
                    //console.log( 'Button 2 clicked on' );
                }
            },
            
            
            {
                text: 'Reportes',
                action: function ( e, dt, node, conf ) {
                	 reporteES();
                    //console.log( 'Button 2 clicked on' );
                }
            },
            {
                text: 'Ayuda',
                action: function ( e, dt, node, conf ) {
                	chat();
                    console.log( 'Button 2 clicked on' );
                }
            },
        ],
		"ajax":
				{
					url: '../ajax/gastos.php?op=listar',
					data:{idgastos:idgastos,fecha:fecha,fecha_inicial:fecha_inicial,fecha_final:fecha_final,idalmacenes:idalmacenes},
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
	idgastos=$("#id").val();
	listarPagos(idgastos);

}
function listarPagos(idgastos)
{
	
	///alert("almacen "+idalmacenes+" fecha "+fecha);
		
	tabla=$('#tbllistadop').dataTable(
	{
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    "scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false,
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla

	   buttons: [
            
            
            
            {
                text: 'Reportes',
                action: function ( e, dt, node, conf ) {
                	 reporteES();
                    //console.log( 'Button 2 clicked on' );
                }
            },
            {
                text: 'Ayuda',
                action: function ( e, dt, node, conf ) {
                	chat();
                    console.log( 'Button 2 clicked on' );
                }
            },
        ],
		"ajax":
				{
					url: '../ajax/gastos.php?op=listarPagos',
					data:{idgastos:idgastos},
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();


}
//Función para guardar o editar

function guardaryeditar(e)
{
	idcat_de_gastos=Number($('select[id=idcat_de_gastos]').val());
	fecha=$("#fecha").val();
	//alert("Gasto no selecionado "+idcat_de_gastos);
	if (idcat_de_gastos<1 || idcat_de_gastos=='NaN' )
	{
		//alert("Gasto no selecionado");
		idcat_de_gastos=0;
	}

	if (e==1)
		$("#idgastos").val(0);
	if (idcat_de_gastos>0)
	{ 
		e.preventDefault(); //No se activará la acción predeterminada del evento
		//$("#btnGuardar").prop("disabled",true);
		var formData = new FormData($("#formulario")[0]);
		var dataString = $('#formulario').serialize();
	    //alert('Datos del formulario de anexo: '+dataString);

		$.ajax({
			url: "../ajax/gastos.php?op=guardaryeditar",
		    type: "POST",
		    data: formData,
		    contentType: false,
		    processData: false,

		    success: function(datos)
		    {    
		    	if (datos>0)
		    	{                
		         	bootbox.alert("Gasto registrado con id "+datos);	          
		         	tabla.ajax.reload();
		        	idgastos=$("#id").val(datos);
		        	listar(datos,fecha,idalmacenes);
					listarPagos(datos);
		     	}
		     	else
		     	bootbox.alert(datos);	          	
		         //mostrarform(false);
		        
		          
		    }

		});
	}
	else
	{
		mensaje("Catalogo del gastos no se ha selecionado ");
	}
	//limpiar();
}

function mostrar(idgastos)
{

	$.post("../ajax/gastos.php?op=mostrar",{idgastos : idgastos}, function(data, status)
	{
		//alert("Datos cargados ");
		data = JSON.parse(data);		
		//mostrarform(true);
		$("#id").val(data.gastos_id);
		$("#idgastos").val(data.gastos_id);
		$("#idcat_de_gastos").val(data.cat_de_gastos_id);
		$('#idcat_de_gastos').selectpicker('refresh');
		$("#idformasdepagos").val(data.forma_de_pago_id);
		$('#idformasdepagos').selectpicker('refresh');
		$("#idalmacenes").val(data.almacen_id);
		$('#idalmacenes').selectpicker('refresh');
		$("#fecha").val(data.fecha);
		$("#importe").val(data.importe);
		$("#referencia").val(data.referencia);
		$("#folio").val(data.folio);
		$("#importe").val(data.importe);
		$("#iva").val(data.iva);
		$("#ajuste").val(data.ajuste);
		$("#subtotal").val(data.subtotal);
		$("#isrretenido").val(data.isrretenido);
		$("#ivaretenido").val(data.ivaretenido);
		$("#total").val(data.total);
		$("#moneda").val(data.moneda);
		$("#valormoneda").val(data.valormoneda);
		$("#concepto").val(data.concepto);
		$("#folio").val(data.folio);
		$("#uuid").val(data.uuid);
		
		$("#tipodegasto").val(data.tipodegasto);
		$('#tipodegasto').selectpicker('refresh');

		//alert("uuid "+data.uuid);
		
		$("#SatNombreFormaDePago").val(data.sat_nombre_forma_de_pago);
		$("#SatClaveFormaDePago").val(data.sat_clave_forma_de_pago);
		
		$("#tasaocuota").val(data.tasaocuota);

		$.post("../ajax/proveedores.php?op=mostrar",{idpersonas : data.proveedor_id}, function(data, status)
		{
			//alert("cargandos datos del proveedor ");
			data = JSON.parse(data);		
			//mostrarform(true);
			$("#nombre").val(data.descripcion);
			$("#rfc").val(data.rfc);
			
		})
		idgastos=$("#id").val();
		listarPagos(idgastos);
		


 	})
}

//Función para desactivar registros
function desactivar(idgastos)
{
	
	bootbox.confirm("¿Está Seguro de eliminar  gasto?", function(result)
	{
	    if(result)
	    {
	    	$.post("../ajax/gastos.php?op=desactivar", {idgastos : idgastos,idusuarios:idusuarios}, function(e){
    		//bootbox.alert(e);
            tabla.ajax.reload();
	    	});
	    }
	    id=$("#id").val();
	    listar(id,fecha,idalmacenes);
		//listarPagos(idgastos);
	});
}
function eliminarPago(idpagos)
{
	bootbox.confirm("¿Está Seguro de eliminar  pago?", function(result)
	{
		if(result)
	    {
	    	$.post("../ajax/gastos.php?op=eliminarPago", {idpagos : idpagos,idusuarios:idusuarios}, function(e){
	    		//bootbox.alert(e);
	            tabla.ajax.reload();
	    	});	
	    }
	});
	id=$("#id").val()
	listar(id,fecha,idalmacenes);
	//listarPagos(idgastos);
	
}
function buscarXml()
{

	$.post("../ajax/gastos.php?op=cargarXml", {idgastos : idgastos}, function(e){
		//bootbox.alert(e);
        tabla.ajax.reload();
	});	
    
}
function subirXml()
{
	
	$("#myModalSubirXml").modal("show");	
} 
function subirImagen()
{
	
	$("#myModalSubirImagen").modal("show");	
} 
function enviarXml(e)
{
	//alert("enviado ");
	//e.preventDefault(); //No se activará la acción predeterminada del evento
	
	var dataString = $('#formularioXml').serialize();
    //alert('Datos del formulario: '+dataString);
	var formData = new FormData($("#formularioXml")[0]);

	$.ajax({
		url: "../ajax/gastos.php?op=enviarXml",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {                    
	    	
	    	if (datos.length>4)
	    	{
	    		titulo='Imagen guardada como '+datos;	
	    		
	    		$.post("../ajax/gastos.php?op=leer_xml", {archivo : datos}, function(e){
        		//if (e.length>15)
        			$("#archivoxml").val(datos)
	            	data = JSON.parse(e);		

					$("#uuid").val(data.UUID);
					$("#folio").val(data.Folio);
					$("#referencia").val(data.Folio);
					$("#fecha").val(data.Fecha);
					$("#nombre").val(data.Nombre);
					$("#rfc").val(data.Rfc);
					$("#concepto").val(data.Descripcion);
					$("#importe").val(data.SubTotal);
					$("#tasaocuota").val(data.TasaOCuota);
					$("#moneda").val(data.Moneda);
					$("#iva").val(Number(data.TotalImpuestosTrasladados));
					importe=$("#importe").val();
					iva=$("#iva").val();
					

					SubTotal=Number(importe)+Number(iva);
					SubTotal=Math.round(SubTotal * 100) / 100;
					//alert("importe"+importe+" iva "+iva+" subtotal "+SubTotal);
					$("#subtotal").val(SubTotal);
					$("#isrretenido").val(Number(data.TotalImpuestosRetenidos));
					$("#total").val(data.Total);
					Total=Number(data.Total);
					$("#SatClaveFormaDePago").val(data.SatClaveFormaDePago);
					$("#SatNombreFormaDePago").val(data.SatNombreFormaDePago);
					if (data.SatClaveFormaDePago=='01' && Number(Total)>2000 )
						$("#tipodegasto").val('N');
					else
						$("#tipodegasto").val('S');	
					$('#tipodegasto').selectpicker('refresh');
        		});	

	    	}
	    	else
	    	{
	    		titulo='Imagen invalida ';	
	    	}
	        Swal.fire({
				  position: 'top-end',
				  type: 'success',
				  title: titulo,
				  showConfirmButton: false,
				  timer: 1500
			})


			
	    }

	});
	//limpiar();
	//alert("telefono "+Telefono+" Nombre "+Nombre);
	//formularioregistros

}
function enviarImagen(e)
{
	
	
	var dataString = $('#formularioPdf').serialize();
    //alert('Datos del formulario: '+dataString);
	var formData = new FormData($("#formularioPdf")[0]);

	$.ajax({
		url: "../ajax/gastos.php?op=enviarImagen",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {                    
	    	
	    	if (datos.length>4)
	    	{
	    		titulo='Imagen guardada como '+datos;	
	    		//$("#imagenmuestra").attr("src","../files/pdfgastos/"+datos);
	    		//<embed src="nombre_archivo.pdf" type="application/pdf" width="800" height="600"></embed>		

	    	}
	    	else
	    	{
	    		titulo='Imagen invalida ';	
	    	}
	        Swal.fire({
				  position: 'top-end',
				  type: 'success',
				  title: titulo,
				  showConfirmButton: false,
				  timer: 1500
			})


			
	    }

	});
	
}
//Función para activar registros
function lecturaEscritura(campo,estatus) //lectura, escritura
{


	if (estatus==0)
	{
		//alert("solo lectura");
		$("#"+campo).attr("readonly",true);
		//$('input').attr('readonly', true); 
    	//$("#"+campo).addClass("readOnly");

	} 
	else
    {
    	//alert("escritura y lectura");
    	$('#'+campo).prop('readonly', false);
    	
    }


}
function activar(idgastos)
{
	bootbox.confirm("¿Está Seguro de activar la linea?", function(result){
		if(result)
        {
        	$.post("../ajax/gastos.php?op=activar", {idgastos : idgastos}, function(e){
        		//bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}


function imprimir(e)
{
	//window.open('../reportes/articulos.php?usuario=admin','GoogleWindow', 'width=800, height=600',datos);
	//window.open('../reportes/rep_entradas.php?ident_sal='+ident_sal,'BmpSistemas', 'width=800, height=600');
	ident_sal=e;
	if (Number(ident_sal)>0)
	window.open('../reportes/rep_gastos.php?folio_id='+ident_sal,'BmpSistemas', 'width=800, height=600');
	else
	{
		Swal.fire({
		  type: 'error',
		  title: 'Documento  a imprimir invalido ...no se ha generado, o no se ha establecido  '
		  
		})
	}
}
function verPdf(e)
{
	
	ident_sal=$("#idgastos").val();
	if (Number(ident_sal)>0)
	window.open('../reportes/ver_pdf_gastos.php?folio_id='+ident_sal,'BmpSistemas', 'width=800, height=600');
	else
	{
		Swal.fire({
		  type: 'error',
		  title: 'Documento invalido ...no se ha generado, o no se ha establecido  '
		  
		})
	}
}
function mensaje(titulo)
{
 	Swal.fire({
			  type: 'error',
			  title: titulo,
			  timer: 3500
			})
}
function mensajeError(titulo)
{
 	Swal.fire({
				  type: 'error',
				  title: titulo,
				  showConfirmButton: false,
				  timer: 2500
				})
} 
init();