var tabla;
var nombre="";
var referencia="";
var modulo="Abc Grupos de Gastos"; 
//Función que se ejecuta al inicio
function init(){
	 $("#modulo").html("Abc Grupo de Gastos");
	mostrarform(false);
		
	listar(nombre);
	$("#lblModulo").val(modulo); 

	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	})
}

//Función limpiar
function limpiar()
{
	$("#idgrupo_de_gastos").val("0");
	$("#nombre").val("");
	
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		//$("#listadoregistros").hide();
		//$("#formularioregistros").show();
		//$("#btnGuardar").prop("disabled",false);
		//$("#btnagregar").hide();
	}
	else
	{
		//$("#listadoregistros").show();
		//$("#formularioregistros").hide();
		//$("#btnagregar").show();
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}
function buscar()
{
	nombre=$("#nombre").val();
   
   	listar(nombre);
}
//Función Listar
function listar(nombre)
{

	tabla=$('#tbllistado').dataTable(
	{
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla

	    "scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false,

	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],
		"ajax":
				{
					url: '../ajax/grupo_de_gastos.php?op=listar',
					data:{nombre:nombre},
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
	$("#idgrupo_de_gastos").val(0);
}
//Función para guardar o editar

function guardaryeditar(e)
{
	e.preventDefault(); //No se activará la acción predeterminada del evento
    //$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]);

	$.ajax({
		url: "../ajax/grupo_de_gastos.php?op=guardaryeditar",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {                    
	          //bootbox.alert(datos);	          
	          mostrarform(false);
	          tabla.ajax.reload();
	    }

	});
	limpiar();
}
function guardar(e)
{
	nombre=$("#nombre").val();
	idgrupo_de_gastos=$("#idgrupo_de_gastos").val();
	if (e==1) // actualizar
	{
		//idcat_de_gastos=$("#idcat_de_gastos").val();
		if (idgrupo_de_gastos<=0)
		{
			alert("El id no esta establecido como para actualizar ");
			$("#nombre").focus();
		}
	}
	else
	if (e==0) 		
	{
		idgrupo_de_gastos=0;	
	}
	$.post("../ajax/grupo_de_gastos.php?op=guardaryeditar",{idgrupo_de_gastos : idgrupo_de_gastos,nombre:nombre}, function(data, status)
	{
		
		listar(nombre);

 	});
	
}
function mostrar(idgrupo_de_gastos)
{
	//alert("id "+idgrupo_de_gastos);
	$.post("../ajax/grupo_de_gastos.php?op=mostrar",{idgrupo_de_gastos : idgrupo_de_gastos}, function(data, status)
	{
		data = JSON.parse(data);		
		mostrarform(true);

		$("#nombre").val(data.nombre);
		$("#idgrupo_de_gastos").val(data.idgrupo_de_gastos);

 	})
}

//Función para desactivar registros
function desactivar(idgrupo_de_gastos)
{
	bootbox.confirm("¿Está Seguro de desactivar la calles?", function(result){
		if(result)
        {
        	$.post("../ajax/grupo_de_gastos.php?op=desactivar", {idgrupo_de_gastos : idgrupo_de_gastos}, function(e){
        		//bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(idgrupo_de_gastos)
{
	bootbox.confirm("¿Está Seguro de activar la linea?", function(result){
		if(result)
        {
        	$.post("../ajax/grupo_de_gastos.php?op=activar", {idgrupo_de_gastos : idgrupo_de_gastos}, function(e){
        		//bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}


init();