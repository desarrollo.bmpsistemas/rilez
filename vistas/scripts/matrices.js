var tabla;
var referencia='BASE';
//Función que se ejecuta al inicio 
function init(){
	 $("#modulo").html("Abc de Matrices");
	mostrarform(false);
	listar(referencia);

	/*.post("../ajax/articulos.php?op=selectcatalogo", function(r){
	            $("#idarticulos").html(r);
	            $('#idarticulos').selectpicker('refresh');
	            

	});*/

	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	})
}

//Función limpiar
function limpiar()
{
	//$("#nombre").val("");
	//$("#idmatrices").val("");
}
function agregarMatriz(e)
{
	idarticulos=$('select[id=idarticulos]').val();
	$.post("../ajax/matrices.php?op=guardaryeditar", {idarticulos : idarticulos}, function(e){
		bootbox.alert(e);
	    tabla.ajax.reload();
	});	
    
}

function buscarArticulo(e)
{
	
	
	//if (e==1)
	Opcion=2;
	referencia=$("#nombre").val();
    //alert("valores Opcion "+Opcion+" ref "+referencia);   
   	$.post("../ajax/articulos.php?op=select", {Opcion : Opcion,referencia:referencia}, function(r){
		//alert("resultado "+r);
		{
	        $("#idarticulos").html(r);
	        $('#idarticulos').selectpicker('refresh');
	    }

	});
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
		$("#divBuscar").hide();
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
		$("#divBuscar").show();
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}
function buscarMatriz()
{
	
	referencia=$("#descripcion").val();
	listar(referencia);
	//alert('referencia '+referencia);
}

//Función Listar
function listar(referencia)
{

	tabla=$('#tbllistado').dataTable(
	{
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],
		"ajax":
				{
					url: '../ajax/matrices.php?op=listar',
					data:{referencia:referencia},
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar

function guardaryeditar(e)
{
	e.preventDefault(); //No se activará la acción predeterminada del evento
	$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]);

	$.ajax({
		url: "../ajax/matrices.php?op=guardaryeditar",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {                    
	          bootbox.alert(datos);	          
	          mostrarform(false);
	          tabla.ajax.reload();
	    }

	});
	limpiar();
}

function mostrar(idmatrices)
{
	$.post("../ajax/matrices.php?op=mostrar",{idmatrices : idmatrices}, function(data, status)
	{
		data = JSON.parse(data);		
		mostrarform(true);
		$("#divBuscar").hide();
		$("#nombre").val(data.nombre);
		$("#idmatrices").val(data.idmatrices);

 	})
}

//Función para desactivar registros
function desactivar(idmatrices)
{
	bootbox.confirm("¿Está Seguro de desactivar la matriz?", function(result){
		if(result)
        {
        	$.post("../ajax/matrices.php?op=desactivar", {idmatrices : idmatrices}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(idmatrices)
{
	bootbox.confirm("¿Está Seguro de activar la matriz?", function(result){
		if(result)
        {
        	$.post("../ajax/matrices.php?op=activar", {idmatrices : idmatrices}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}


init();