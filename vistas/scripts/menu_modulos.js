var tabla;

//Función que se ejecuta al inicio   
function init(){
	 $("#modulo").html("Abc de Menus y Modulos");
	mostrarform(false);
	listar(0);

	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	})

	//Cargamos los items al select lineas
	$.post("../ajax/menus.php?op=select", function(r){
	            $("#idmenus").html(r);
	            $('#idmenus').selectpicker('refresh');
	             
	});

	$.post("../ajax/modulos.php?op=select", function(r){
	            $("#idmodulos").html(r);
	            $('#idmodulos').selectpicker('refresh');

	});
	
	//$("#imagenmuestra").hide();
}

//Función limpiar
function limpiar()
{
	
	$("#nombre").val("");
	//$("#idmenus").val("");
	$("#idmodulos").val("");
}
function Catalogos()
{
	$("#myModalCatalogos").modal("show");
	
}


//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		//$("#listadoregistros").hide();
		//$("#formularioregistros").show();
		//$("#btnGuardar").prop("disabled",false);
		//$("#btnagregar").hide();
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}

//Función Listar
function listar(e)
{
	//alert("id "+e);
	tabla=$('#tbllistado').dataTable(
	{
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],
		"ajax":
				{
					url: '../ajax/menu_modulos.php?op=listar',
					data:{idmenus:e},
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 1, "asc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar

function listaModulos()
{
	idmenus=$('select[id=idmenus]').val();	
	listar(idmenus);
}

function actualizarModulo()
{
	idmenus=$("#idmenus1").val();
	nombre=$("#nombre1").val();
	descripcion=$("#descripcion1").val();
	idmodulos=$("#idmodulos1").val();
	orden=$("#orden1").val();

	$.post("../ajax/menu_modulos.php?op=guardaryeditar", {idmenus:idmenus,idmodulos : idmodulos,descripcion:descripcion,nombre:nombre, orden:orden}, function(e){
		mensaje(e);
        tabla.ajax.reload();
	});	
       
	
}
function agregarModulo()
{
	    idmenus=$("#idmenus").val();
		idmodulos=$("#idmodulos").val();
		idmenumodulos=0;
		$.post("../ajax/menu_modulos.php?op=guardaryeditar", {idmenumodulos:idmenumodulos,idmenus:idmenus,idmodulos : idmodulos}, function(e){
		mensaje(e);
        tabla.ajax.reload();
	});	
       
	
}

function guardaryeditar(e) 
{
	idmenus=$("#idmenus2").val();
	nombre=$("#nombre2").val();
	descripcion=$("#descripcion2").val();
	idmodulos=$("#idmodulos2").val();
	orden=$("#orden2").val();
	$.post("../ajax/menu_modulos.php?op=guardaryeditar", {idmenus:idmenus,idmodulos : idmodulos,descripcion:descripcion,nombre:nombre, orden:orden}, function(e){
	mensaje(e);
    tabla.ajax.reload();

	});
	limpiar();
}

function mostrar(idmodulos)
{
	//limpiar();
	$.post("../ajax/menu_modulos.php?op=mostrar",{idmodulos : idmodulos}, function(data, status)
	{
		data = JSON.parse(data);		
		mostrarform(true);
		
		//$("#idmenus").val(data.idmenus);
		//$('#idmenus').selectpicker('refresh');
		//alert(" id menus "+data.idmenus);
		$("#orden1").val(data.orden);
		$("#orden2").val(data.orden);
		$("#nombre1").val(data.nombre);
		$("#nombre2").val(data.nombre);
		$("#descripcion1").val(data.descripcion);
		$("#descripcion2").val(data.descripcion);
		$("#idmodulos1").val(idmodulos);
		$('#idmodulos1').selectpicker('refresh');
		$("#idmenus2").val(data.idmenus);
		$('#idmenus2').selectpicker('refresh');

 	})
}

//Función para desactivar registros
function desactivar(idmenumodulos)
{
	bootbox.confirm("¿Está Seguro de quitar el modulo?", function(result){
		if(result)
        {
        	$.post("../ajax/menu_modulos.php?op=desactivar", {idmenumodulos : idmenumodulos}, function(e){
        		mensaje(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(idmenumodulos)
{
	bootbox.confirm("¿Está Seguro de quitar el modulo?", function(result){
		if(result)
        {
        	$.post("../ajax/menu_modulos.php?op=activar", {idmenumodulos : idmenumodulos}, function(e){
        		mensaje(e);
	            tabla.ajax.reload(); 
        	});	
        }
	})
}
function mensaje(titulo)
{
	Swal.fire({
		  position: 'top-end',
		  type: 'success',
		  title: titulo,
		  showConfirmButton: false,
		  timer: 1500
		})
}


//función para generar el código de barras

init();