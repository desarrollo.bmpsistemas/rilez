var tabla;
var idmenus=0;
//Función que se ejecuta al inicio
function init(){
	 $("#modulo").html("Abc de Menus");
	mostrarform(false);
	listar();

	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	})
}

//Función limpiar
function limpiar()
{
	//$("#nombre").val("");
	//$("#idmenus").val("");
	//$("#idmodulos").val("");
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		//$("#listadoregistros").hide();
		//$("#formularioregistros").show();
		//$("#btnGuardar").prop("disabled",false);
		//$("#btnagregar").hide();
	}
	else
	{
		//$("#listadoregistros").show();
		//$("#formularioregistros").hide();
		//$("#btnagregar").show();
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}

//Función Listar
function listar()
{
	tabla=$('#tbllistado').dataTable(
	{
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],
		"ajax":
				{
					url: '../ajax/menus.php?op=listar',
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar

function guardaryeditar(e)
{
	nombre=$("#nombre").val();
	orden=$("#orden").val();
	//alert("id menus "+idmenus);
	$.post("../ajax/menus.php?op=guardaryeditar",{idmenus : idmenus,nombre:nombre,orden:orden}, function(data, status)
	{
		bootbox.alert(data);
	    tabla.ajax.reload();	
	    idmenus=0;
 	})
}

function mostrar(e)
{
	idmenus=e;
	$.post("../ajax/menus.php?op=mostrar",{idmenus : idmenus}, function(data, status)
	{
		data = JSON.parse(data);		
		//mostrarform(true);

		$("#nombre").val(data.nombre);
		$("#orden").val(data.orden);
		idmenus=e;
		//alert("id menus "+idmenus);

 	})
}

//Función para desactivar registros
function desactivar(idmenus)
{
	bootbox.confirm("¿Está Seguro de desactivar la menus?", function(result){
		if(result)
        {
        	$.post("../ajax/menus.php?op=desactivar", {idmenus : idmenus}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(idmenus)
{
	bootbox.confirm("¿Está Seguro de activar el menu?", function(result){
		if(result)
        {
        	$.post("../ajax/menus.php?op=activar", {idmenus : idmenus}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}


init();