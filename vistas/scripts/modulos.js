var tabla;
var idmodulos=0;
//Función que se ejecuta al inicio
function init(){
	 $("#modulo").html("Abc de Modulos");
	mostrarform(false);
	listar();

	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	})
}

//Función limpiar
function limpiar()
{
	//$("#nombre").val("");
	//$("#idmodulos").val("");
	//$("#idmodulos").val("");
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		//$("#listadoregistros").hide();
		//$("#formularioregistros").show();
		//$("#btnGuardar").prop("disabled",false);
		//$("#btnagregar").hide();
	}
	else
	{
		//$("#listadoregistros").show();
		//$("#formularioregistros").hide();
		//$("#btnagregar").show();
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}

//Función Listar
function listar()
{
	tabla=$('#tbllistado').dataTable(
	{
		"scrollY":        "200px",
    	"scrollCollapse": true,
    	"paging":         false,
    	"aProcessing": true,//Activamos el procesamiento del datatables
      	"aServerSide": true,//Paginación y filtrado realizados por el servidor
      	dom: 'Bfrtip',//Definimos los elementos del control de tabla
      	buttons: [              
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdf'
            ],
		"ajax":
				{
					url: '../ajax/modulos.php?op=listar',
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar

function guardaryeditar(e)
{
	descripcion=$("#descripcion").val();
	nombre=$("#nombre").val();
	orden=$("#orden").val();
	
	$.post("../ajax/modulos.php?op=guardaryeditar",{idmodulos : idmodulos,descripcion:descripcion,nombre:nombre,orden:orden}, function(data, status)
	{
		//bootbox.alert(data);
	    tabla.ajax.reload();	
	    idmodulos=0;
 	})
}

function mostrar(e)
{
	idmodulos=e;
	$.post("../ajax/modulos.php?op=mostrar",{idmodulos : idmodulos}, function(data, status)
	{
		data = JSON.parse(data);		
		//mostrarform(true);
		$("#descripcion").val(data.descripcion);
		$("#nombre").val(data.nombre);
		$("#orden").val(data.orden);
		idmodulos=e;
		//alert("id menus "+idmodulos);

 	})
}

//Función para desactivar registros
function desactivar(idmodulos)
{
	bootbox.confirm("¿Está Seguro de desactivar la modulo?", function(result){
		if(result)
        {
        	$.post("../ajax/modulos.php?op=desactivar", {idmodulos : idmodulos}, function(e){
        		//bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(idmodulos)
{
	bootbox.confirm("¿Está Seguro de activar el modulo?", function(result){
		if(result)
        {
        	$.post("../ajax/modulos.php?op=activar", {idmodulos : idmodulos}, function(e){
        		//bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}


init();