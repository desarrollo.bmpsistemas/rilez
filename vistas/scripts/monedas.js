var tabla;
var nombre="";
var referencia="";
var modulo="Abc de Monedas";
//Función que se ejecuta al inicio
function init(){
	 $("#modulo").html("Abc de Monedas");
	mostrarform(false);
		
	listar();
	$("#lblModulo").val(modulo); 

	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	})
}

//Función limpiar
function limpiar()
{
	$("#nombre").val("");
	$("#idmonedas").val("");
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}
function buscar()
{
	nombre=$("#nombreb").val();
   
   	listar();
}
//Función Listar
function listar()
{
	nombre="";
	tabla=$('#tbllistado').dataTable(
	{
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla

	    "scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false,

	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],
		"ajax":
				{
					url: '../ajax/monedas.php?op=listar',
					data:{nombre:nombre},
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar

function guardaryeditar(e)
{
	e.preventDefault(); //No se activará la acción predeterminada del evento
	$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]);

	$.ajax({
		url: "../ajax/monedas.php?op=guardaryeditar",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {                    
	          bootbox.alert(datos);	          
	          mostrarform(false);
	          tabla.ajax.reload();
	    }

	});
	limpiar();
}

function mostrar(idmonedas)
{
	$.post("../ajax/monedas.php?op=mostrar",{idmonedas : idmonedas}, function(data, status)
	{
		data = JSON.parse(data);		
		mostrarform(true);

		$("#nombre").val(data.nombre);
		$("#idmonedas").val(data.idmonedas);
		$("#abreviatura").val(data.abreviatura);
		$("#valor").val(data.valor);
 	})
}

//Función para desactivar registros
function desactivar(idmonedas)
{
	bootbox.confirm("¿Está Seguro de desactivar la calles?", function(result){
		if(result)
        {
        	$.post("../ajax/monedas.php?op=desactivar", {idmonedas : idmonedas}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(idmonedas)
{
	bootbox.confirm("¿Está Seguro de activar la linea?", function(result){
		if(result)
        {
        	$.post("../ajax/monedas.php?op=activar", {idmonedas : idmonedas}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}


init();