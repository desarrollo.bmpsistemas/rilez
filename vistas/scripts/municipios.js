var tabla;
var nombre="";
//Función que se ejecuta al inicio 
function init(){
	 $("#modulo").html("Abc de Municipios");
	mostrarform(false);
	listar();
	

	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	})

	//Cargamos los items al select pises
	$.post("../ajax/paises.php?op=select", function(r){
	            $("#idpaises").html(r);
	            $('#idpaises').selectpicker('refresh');

	});

	//Cargamos los items al select estados
	$.post("../ajax/municipios.php?op=select", function(r){
	            $("#idmunicipios").html(r);
	            $('#idmunicipios').selectpicker('refresh');

	});
	$.post("../ajax/estados.php?op=select", function(r){
	            $("#idestados").html(r);
	            $('#idestados').selectpicker('refresh');

	});
	//$("#imagenmuestra").hide();
}


//Función limpiar
function limpiar()
{
	
	$("#nombre").val("");
	$("#idestados").val("");
	$("#idestados").val("");
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}
function buscar()
{
	nombre=$("#nombreb").val();
   
   	listar(nombre);
}
//Función Listar
function listar(nombre)
{
	tabla=$('#tbllistado').dataTable(
	{
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla

	    "scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false,

	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],
		"ajax":
				{
					url: '../ajax/municipios.php?op=listar',
					data:{nombre:nombre},
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar

function guardaryeditar(e)
{
	e.preventDefault(); //No se activará la acción predeterminada del evento
	$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]);

	$.ajax({
		url: "../ajax/municipios.php?op=guardaryeditar",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {                    
	          bootbox.alert(datos);	          
	          mostrarform(false);
	          tabla.ajax.reload();
	    }

	});
	limpiar();
}

function mostrar(idmunicipios)
{
	$.post("../ajax/municipios.php?op=mostrar",{idmunicipios : idmunicipios}, function(data, status)
	{
		data = JSON.parse(data);		
		mostrarform(true);
		$("#idpaises").val(data.idpaises);
		$('#idpaises').selectpicker('refresh');

		$("#idestados").val(data.idestados);
		$('#idestados').selectpicker('refresh');

		$("#idmunicipios").val(data.idmunicipios);
		$('#idmunicipios').selectpicker('refresh');
		
		$("#nombre").val(data.nombre);
		$("#idmunicipios").val(data.idmunicipios);
 		//generarbarcode();

 	})
}
function listarestados()
{
	idpaises=$('select[id=idpaises]').val();
	//Cargamos los items al select estados
	$.post("../ajax/estados.php?op=selectestados&idpaises="+idpaises, function(r){
	            $("#idestados").html(r);
	            $('#idestados').selectpicker('refresh');

	});


	

}

//Función para desactivar registros
function desactivar(idmunicipios)
{
	bootbox.confirm("¿Está Seguro de desactivar el Sublineas?", function(result){
		if(result)
        {
        	$.post("../ajax/municipios.php?op=desactivar", {idmunicipios : idmunicipios}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(idmunicipios)
{
	bootbox.confirm("¿Está Seguro de activar el Sublineas?", function(result){
		if(result)
        {
        	$.post("../ajax/municipios.php?op=activar", {idmunicipios : idmunicipios}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload(); 
        	});	
        }
	})
}

//función para generar el código de barras

init();