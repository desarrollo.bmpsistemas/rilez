var tabla;
var tabla2;
var codigo=0;
var Opcion=4;
var referencia=""; 
var estatus=false;
var fecha_inicial;
var fecha_final;
var TablaConDatos;
var datos;
//Función que se ejecuta al inicio 
function init(){
	$("#modulo").html("Abc de Pagos ");
	$("#divPersona").hide();
	$("#divFecha").hide();
	mostrarform(false); 
	mostrarDivPorPersona();
	tomar_fecha();
	//fecha_inicial="2019-06-01";
	//fecha_final="2019-08-30";
	listarEntSal(Opcion);
	mostrarPagos(1);
	$('#fecha').attr('disabled', true);
	$('#np').attr('disabled', true);
	//listar2(Opcion,referencia,fecha_inicial,fecha_final);
	$("#codigob").focusin(function(e) {
        Opcion=1;   
    });
    $("#nombreb").focusin(function(e) {
        Opcion=2;   
    });
    
    
	/*$.post("../ajax/articulos.php?op=selectcatalogo", function(r){
	            $("#idarticulos").html(r);
	            $('#idarticulos').selectpicker('refresh');

	});*/

	$.post("../ajax/almacenes.php?op=select", function(r){
		//alert("almacenes "+r);
	            $("#idalmaceness").html(r);
	            $('#idalmaceness').selectpicker('refresh');

	});
	$.post("../ajax/articulos.php?op=selectivas", function(r){
	            $("#idivas").html(r);
	            $('#idivas').selectpicker('refresh');

	});

	$.post("../ajax/articulos.php?op=selectmonedas", function(r){
	            $("#idmonedas").html(r);
	            $('#idmonedas').selectpicker('refresh');

	});
    
	$.post("../ajax/articulos.php?op=select_tipos_de_pagos", function(r){
	            $("#idformadepagos").html(r);
	            $('#idformadepagos').selectpicker('refresh');

	});
	
    $("#utilidad").keyup(function(e) {
        calcularPrecio();
    });
	

	$("#formulario").on("submit",function(e)
	{
		idarticulos_almacenes=$("#idarticulos_almacenes").val();
		//alert("id articulos almacenes  "+idarticulos_almacenes);
		guardaryeditar(e);	
	})
}

//Función limpiar


function permisoDeFecha(e)
{
	$("#myModalAutorizarFecha").modal("show");
}

function autorizarFechaDePago(e)
{
	usuario=$("#usuarioF").val();
	clave=$("#claveF").val();
	//alert("id user "+usuario+"clave "+clave);
	$.post("../ajax/pagos.php?op=activarOpciones",{idusuarios:usuario,clave:clave}, function(data, status)
		{
				
			//data = JSON.parse(data);	
			rol=data;
			//alert("datos "+data);
			
			if (rol!=7 )
			{
				Swal.fire({
				  position: 'top-end',
				  type: 'success',
				  title: 'Usuario sin rol de autorizacion ',
				  showConfirmButton: false,
				  timer: 1500
				});
				$('#fecha').attr('disabled', true);
				$("#np").val(0);
				$('#np').attr('disabled', true);
				
    			
			}	
			else
			{
				
    			//$('#fecha').attr('disabled', false);
    			Swal.fire({
				  position: 'top-end',
				  type: 'success',
				  title: 'Usuario autorizado ',
				  showConfirmButton: false,
				  timer: 1500
				});
				$('#fecha').attr('disabled', false);	
    			$('#np').attr('disabled', false);
			}
	
		})
}

function ocultar()
{
	//$("#divAlmacen").hide();
	//$("#divFechaInicial").hide();
	//$("#divFechaFinal").hide();
	$("#divPersona").hide();
	//$("#divFecha").hide();

}
function tomar_fecha()
{
	var fecha;
	fecha=formato_fecha();
	$('#fecha_inicial').val(fecha);
	$('#fecha_final').val(fecha);
	$('#fechainicial').val(fecha);
	$('#fechafinal').val(fecha);
	$('#fecha').val(fecha);
	
	//alert("feecha "+fecha);
}

function formato_fecha()
{
	var now = new Date();
	var day = ("0" + now.getDate()).slice(-2);
	var month = ("0" + (now.getMonth() + 1)).slice(-2);
	var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
    return today;
}
function limpiar()
{
	//$("#nombre").val("");
	//$("#idalmacenes").val("");
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();

	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
		//$("#idarticulos").hide();
		$("#idarticulos_almacenes").val("");
		$("#articulos").css("display", "none");
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
		$("#articulos").css("display", "");
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}
function calcularPrecio()
{
	var precio=0;
	costo=$('#costo').val();
	utilidad=$('#utilidad').val();
	
	precio=Number((costo*utilidad));
	
	$('#precio').val(precio);
}

function buscarPersonas() //buscarPersonas
{
	//alert("buscarndo personas "+Opcion);
	
	Opcion=2;
	referencia=$('#descripcion').val();
	tipoDeMovimiento="Salidas";
	//alert("buscarndo personas opcion "+Opcion+" referencia "+referencia);
	if (tipoDeMovimiento=="Entradas")
	{
		$.post("../ajax/personas.php?op=selectp",{Opcion:Opcion,referencia : referencia},  function(r){
			    //alert("datos entradas "+r);
	            $("#idpersonas").html(r);
	            $('#idpersonas').selectpicker('refresh');

		});
	}
	else
	{
		$.post("../ajax/personas.php?op=selectc",{Opcion:Opcion,referencia : referencia}, function(r){
				//alert("datos salidas "+r);	
	            $("#idpersonas").html(r);
	            $('#idpersonas').selectpicker('refresh');

		});
	}

}

function selectFolio(ident_sal)
{

	$("#folio").val(ident_sal); 
	$("#importe").focus(); 
	mostrarPagos(ident_sal);
   	
}
//data:{Opcion: Opcion,referencia: referencia},
//Función Listar
function listarEntSal(Opcion)
{
	//onchange="mostrarEntSal()"
	referencia=$('#referencia').val();
	fecha_inicial=$('#fecha_inicial').val();
	fecha_final=$('#fecha_final').val();
	ident_sal=$('#Folio').val();
	condicion=$('#condicion').val();
	if (condicion=="Pagado")
		saldo=0;
	else
		saldo=0.1;
	idpersonas=$('select[id=idpersonas]').val();
	if (Number(idpersonas)==0) idpersonas=0;

	//tipo=$('select[id=tipo]').val();
	
	//alert("Opcion "+Opcion +" fi "+fecha_inicial+ " ff "+fecha_final+" ident_sal "+ident_sal+" condicion "+condicion + " id per "+idpersonas+" saldo "+saldo);

	tabla=$('#tbllistado').dataTable(
	{
		"scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false,

		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'

		            
		        ],
		"ajax":
				{
					url: '../ajax/pagos.php?op=listar',
					data:{Opcion:Opcion,referencia:referencia,fecha_inicial:fecha_inicial,fecha_final:fecha_final,saldo:saldo,idpersonas:idpersonas,ident_sal:ident_sal},
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}

function mostrarPagos(ident_sal)
{
	idpagos=0;
	
	//ident_sal=$('#folio').val();
	//alert("id ent sal "+ident_sal +" id pagos "+idpagos);

	tabla2=$('#tbllistado2').dataTable(
	{
		"scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false,

		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'

		            
		        ],
		"ajax":
				{
					url: '../ajax/pagos.php?op=mostrarPagos',
					data:{idpagos:idpagos,ident_sal:ident_sal},
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}

function guardaryeditar(e)
{
	e.preventDefault(); //No se activará la acción predeterminada del evento
	$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]);
	var codigo=$('select[id=idarticulos]').val();
	var dataString = $('#formulario').serialize();
   // alert('Datos del formulario: '+dataString);

	$('#idarticulo').val(codigo);
	//alert("id articulo  "+codigo);
	$.ajax({
		url: "../ajax/articulos_almacenes.php?op=guardaryeditar",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {                    
	          bootbox.alert(datos);	          
	          mostrarform(false);
	          tabla.ajax.reload();
	    }

	});
	limpiar();
}



function mostrarDivPorPersona()
{
	$("#divPersona").hide();
	//$("#divFecha").hide();
	//$("#divAlmacen").hide();
	
	tipo=$('select[id=tipodebusqueda]').val();

	if (tipo=='Persona')
	{
	    $("#divPersona").show();
	    //$("#divFecha").hide();
	    //$("#divAlmacen").show();
		
	}
	if (tipo=='Fecha')
	{
	     $("#divPersona").hide();
	     $("#divFecha").show();
	     $("#divAlmacen").show();
		//$("#divFechaInicial").show();
		//$("#divFechaFinal").show();
	}
	  
	
}

function imprimirPago(ident_sal,idpagos)
{
	
	if (Number(idpagos)>0)
	window.open('../reportes/rep_pago.php?ident_sal='+ident_sal+'&idpagos='+idpagos,'BmpSistemas', 'width=800, height=600');
	else
	{
		Swal.fire({
		  type: 'error',
		  title: 'Documento  a imprimir invalido ...no se ha generado, o no se ha establecido  '
		  
		})
	}
}
function mostrarEntSal()
{

}
function leerIva_y_Moneda()
{
	idalmacenes=$("#idalmacenes").val();
	$.post("../ajax/articulos_almacenes.php?op=leerIva_y_Moneda",{idalmacenes : idalmacenes}, function(data, status)
	{
		data = JSON.parse(data);
		//alert("datos"+data);
		$("#idmonedas").val(data.idmonedas);
		$('#idmonedas').selectpicker('refresh');
		$("#idivas").val(data.idivas);
		$('#idivas').selectpicker('refresh'); 
	})
}
function mostrar(idarticulos_almacenes)
{
	
}

function agregarPago(e)
{
	
	idpagos=$("#idpagos").val();

	ident_sal=$("#folio").val();
	fecha=$("#fecha").val();
	importe=$("#importe").val();
	referencia=$("#referencia").val();
	idusuarios=$("#idusuarios").val();
	np=$("#np").val();
	idformadepagos=$('select[id=idformadepagos]').val();
	//alert("idpagos"+idpagos+ " ident_sal "+ ident_sal+ " fecha "+fecha+" referencia"+referencia+" idformadepagos "+ idformadepagos +"importe" + importe+" idusuarios"+idusuarios);
	$.post("../ajax/pagos.php?op=agregarPago",{idpagos:idpagos,ident_sal:ident_sal,fecha:fecha,referencia:referencia,idformadepagos:idformadepagos,importe:importe,idusuarios:idusuarios,np:np}, function(e)
	{
		
		 //bootbox.alert(e);
	     //tabla2.ajax.reload();
	     mostrarPagos(ident_sal)

 	})
}
//Función para desactivar registros
function desactivar(idarticulos_almacenes)
{
	bootbox.confirm("¿Está Seguro de desactivar la articulo en almacene?", function(result){
		if(result)
        {
        	$.post("../ajax/articulos_almacenes.php?op=desactivar", {idarticulos_almacenes : idarticulos_almacenes}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(idarticulos_almacenes)
{
	bootbox.confirm("¿Está Seguro de activar articulo en almacen ?", function(result){
		if(result)
        {
        	$.post("../ajax/articulos_almacenes.php?op=activar", {idarticulos_almacenes : idarticulos_almacenes}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

init();