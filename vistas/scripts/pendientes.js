var tabla;
var fecha_inicial;
var fecha_final;
var fecha;
var condicion=0;
var palabra="";
//Función que se ejecuta al inicio 
function init(){
	 $("#modulo").html("Abc de Pendientes");
	mostrarform(false);
	/*$("#nombreb").focusin(function(e) {
        Opcion=-1;   
        palabra="";
        $("#nombreb").val("");
    });*/
	/*$("#idpersonas").focusin(function(e) {
        Opcion=0;   
        palabra="";
        //alert("focus in ");
    });*/
	
	fecha=formato_fecha();
	$('#fecha_inicial').val(fecha);
	$('#fecha_final').val(fecha);
	fecha_inicial=fecha;
	fecha_final=fecha;

	listar(fecha_inicial,fecha_final);


	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	})

	

	//Cargamos los items al select sublineas
	$.post("../ajax/usuarios.php?op=select", function(r){
	            $("#idusuarios").html(r);
	            $('#idusuarios').selectpicker('refresh');
	           

	});

	/*$.post("../ajax/personas.php?op=selectc", function(r){
	            $("#idpersonas").html(r);
	            $('#idpersonas').selectpicker('refresh');
	           
	});*/

	 /*$(document).keydown(function(e) {
            
            num=e.which;
            caracter="";
            //alert(" caracter "+num);
            caracter=String.fromCharCode(num);
            
            if (num==16)
            	buscar();
            if (Opcion==0 || (num>=65 && num<=90))
            {

	            if (num==8 || num==37)
	            {
	            	
	            	if (palabra.length>0)
	            	{
	            	   palabra = palabra.substr(0, palabra.length-1);
	            	}
	            }
	            else
            	palabra=palabra+caracter;
                $("#origen").val(palabra);
            }

                               
      }); */  
	//$("#imagenmuestra").hide();
}

//Función limpiar
function limpiar()
{
	
	$("#imagenmuestra").attr("src","");
	$("#imagenactual").val("");
	$("#origen").val("");
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		//$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
		$("#divfechainicial").hide();
		$("#divfechafinal").hide();
		$("#divagregar").hide();
		$("#asunto").prop("readonly", false);
	    $("#contestacion").prop("readonly", true);
		$("#idpendientes").val(0);

	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
		$("#divfechainicial").show();
		$("#divfechafinal").show();
		$("#divagregar").show();
	}
	
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}
function formato_fecha()
{
	var now = new Date();
	var day = ("0" + now.getDate()).slice(-2);
	var month = ("0" + (now.getMonth() + 1)).slice(-2);
	var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
    return today;
}
function buscar()
{
	
	$("#origen").val("");
	//if (Opcion==-1)
	nombre=$("#nombreb").val();
	//else
	//nombre=palabra;

   	$.post("../ajax/personas.php?op=selectc", {nombre : nombre}, function(r){
	            $("#idpersonas").html(r);
	            $('#idpersonas').selectpicker('refresh');
	            Opcion=0;   
        		palabra="";

	});
   	
}

//Función Listar
function listar(fecha_inicial,fecha_final)
{
	tabla=$('#tbllistado').dataTable(
	{

		"scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false,

		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],
		"ajax":
				{
					url: '../ajax/pendientes.php?op=listar',
					data:{fecha_inicial: fecha_inicial,fecha_final: fecha_final},
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar

function guardaryeditar(e)
{
	//imagen=$("#imagen").val();
	//alert("Entro");
	if (origen.length>1 || origen!="")
	{
		e.preventDefault(); //No se activará la acción predeterminada del evento
		//$("#btnGuardar").prop("disabled",true);
		var dataString = $('#formulario').serialize();
	    //alert('Datos del formulario: '+dataString);
		var formData = new FormData($("#formulario")[0]);

		$.ajax({
			url: "../ajax/pendientes.php?op=guardaryeditar",
		    type: "POST",
		    data: formData,
		    contentType: false,
		    processData: false,

		    success: function(datos)
		    {                    
		    	  //$("#imagen").val(datos);
		    	  //alert("datos regresados "+datos);
		    	  listar(fecha_inicial,fecha_final);

		          //bootbox.alert(mensaje);	
		          //mostrarMensaje(mensaje);
		          //mostrarform(false);
		          //tabla.ajax.reload();
		        Swal.fire({
					  position: 'top-end',
					  type: 'success',
					  title: 'Sus Datos se han  guardado',
					  showConfirmButton: false,
					  timer: 1500
				})
		    }

		});
	}
	//limpiar();
	//formularioregistros
}
function mostrarMensaje(mensaje)
{
	swal({   
		title: mensaje,   
		text: "Intente nuevamente",   
		timer: 1500,   
		showConfirmButton: true 
	});
}
function mostrar(idpendientes)
{
	//alert("idpendientes "+idpendientes);
	$.post("../ajax/pendientes.php?op=mostrar",{idpendientes : idpendientes}, function(data, status)
	{
		
		data = JSON.parse(data);		
		
		mostrarform(true);
		
		$("#origen").val(data.origen);
		$("#importe").val(data.importe);
		$("#asunto").val(data.asunto);
		$("#contestacion").val(data.contestacion);
		
		$("#idpendientes").val(data.idpendientes);
		
	    if (data.condicion==0)
	    {
	    	$("#asunto").prop("readonly", true);
	    	$("#contestacion").prop("readonly", false);
		}
		else
		{
			$("#asunto").prop("readonly", false);
	    	$("#contestacion").prop("readonly", true);
		}
		$("#idusuarios").val(data.idusuarios);
	    $('#idusuarios').selectpicker('refresh');
	    $("#idpersonas").val(data.idpersonas);
	    $('#idpersonas').selectpicker('refresh');

		$("#imagenmuestra").show();
		$("#imagenmuestra").attr("src","../files/pendientes/"+data.imagen);
		$("#imagenactual").val(data.imagen);
 		
 		

 	})
}

//Función para desactivar registros
function desactivar(idpendientes)
{
	bootbox.confirm("¿Está Seguro de desactivar el pendiete?", function(result){
		if(result)
        {
        	$.post("../ajax/pendientes.php?op=desactivar", {idpendientes : idpendientes}, function(e){
        		//bootbox.alert(e);
	            tabla.ajax.reload();
	            //mostrarPendientes();
        	});	
        }
	})
}

//Función para activar registros
function activar(idpendientes)
{
	bootbox.confirm("¿Está Seguro de activar el Pendiente?", function(result){
		if(result)
        {
        	$.post("../ajax/pendientes.php?op=activar", {idpendientes : idpendientes}, function(e){
        		//bootbox.alert(e);
	            tabla.ajax.reload(); 
	            //mostrarPendientes();
        	});	
        }
	})
}


function mostrarPendientes()
{
	fecha_inicial=$("#fecha_inicial").val();
	fecha_final=$("#fecha_final").val();
	listar(fecha_inicial,fecha_final);
}

init();