var tabla;
var codigo=0;
var idalmacenes=1;
var idsublineas=1;
//Función que se ejecuta al inicio
function init(){
	 $("#modulo").html("Abc de Precios Por Sublineas");
	mostrarform(false); 
	
	
	//$("#edades").show();
	//alert("hola "+codigo);
    listar(idalmacenes,idsublineas);

	/*$.post("../ajax/articulos.php?op=selectcatalogo", function(r){
	            $("#idarticulos").html(r);
	            $('#idarticulos').selectpicker('refresh');

	});*/

	/*$.post("../ajax/articulos_almacenes.php?op=select", function(r){
	            $("#idalmacenes").html(r);
	            $('#idalmacenes').selectpicker('refresh');

	});*/
	$.post("../ajax/lineas.php?op=select", function(r){
	            $("#idlineas").html(r);
	            $('#idlineas').selectpicker('refresh');

	});
	
	$.post("../ajax/almacenes.php?op=select", function(r){
		            $("#idalmacenes").html(r);
		            $('#idalmacenes').selectpicker('refresh');
		            

	});


	
    $("#utilidad").keyup(function(e) {
        calcularPrecio();
    });
	

	$("#formulario").on("submit",function(e)
	{
		idarticulos_almacenes=$("#idarticulos_almacenes").val();
		//alert("id articulos almacenes  "+idarticulos_almacenes);
		guardaryeditar(e);	
	})
}

//Función limpiar
function limpiar()
{
	$("#nombre").val("");
	$("#idalmacenes").val("");
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();

	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
		$("#idarticulos").hide();
		$("#idarticulos_almacenes").val("");
		$("#articulos").css("display", "none");
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
		$("#articulos").css("display", "");
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}



//Función Listar
function listar(idalmacenes,idsublineas)
{
	//alert("id almacenes "+idalmacenes+" id sublineas "+idsublineas);
	tabla=$('#tbllistado').dataTable(
	{
		"scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false,

		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'

		            
		        ],
		"ajax":
				{
					url: '../ajax/precios_por_sublineas.php?op=listar',
					data:{idalmacenes: idalmacenes,idsublineas:idsublineas},
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar


//Función para activar registros


function selectsublinea()
{
	idlineas=$('select[id=idlineas]').val();
	//Cargamos los items al select estados
	//alert("id lineas "+idlineas);
	$.post("../ajax/sublineas.php?op=selectporlinea", {idlineas : idlineas}, function(r){
		//alert("resultado "+r);
	            $("#idsublineas").html(r);
	            $('#idsublineas').selectpicker('refresh');

	});
}

function selectarticulosporsublinea()
{
	idsublineas=$('select[id=idsublineas]').val();
	idalmacenes=$('select[id=idalmacenes]').val();
	listar(idalmacenes,idsublineas);
}
function actualizar_precios()
{
	idsublineas=$('select[id=idsublineas]').val();
	idalmacenes=$('select[id=idalmacenes]').val();
	porcentaje_utilidad=$("#porcentaje_utilidad").val();
	
	//Cargamos los items al select estados
	//alert("id lineas "+idlineas);
	$.post("../ajax/precios_por_sublineas.php?op=actualizar_precios", 
		{idalmacenes : idalmacenes,idsublineas : idsublineas,porcentaje_utilidad:porcentaje_utilidad}, function(r){
		listar(idalmacenes,idsublineas);

	});
}

init();