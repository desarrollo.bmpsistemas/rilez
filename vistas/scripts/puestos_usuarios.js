var tabla;
var codigo=0;
//Función que se ejecuta al inicio  
function init(){
	 $("#modulo").html("Abc de Puestos a Usuarios");
	mostrarform(false);
	listar(0);

	$.post("../ajax/puestos.php?op=select", function(r){
	            $("#idpuesto").html(r);
	            $('#idpuesto').selectpicker('refresh');

	});
	
	
	$.post("../ajax/usuarios.php?op=select", function(r){
	            $("#idusuarios").html(r);
	            $('#idusuarios').selectpicker('refresh');

	});
	

	$("#formulario").on("submit",function(e)
	{
		//alert("enviando formulario");
		agregar();	
	})
}

//Función limpiar
function limpiar()
{
	//$("#idusuarios").val("");
	//$("#idpuestos").val("");
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}
function buscarcodigo()
{
	codigo=$('select[id=idpuesto]').val();
	$('#idpuestos').val(codigo);
	cod=$('#idpuestos').val();
	//alert("idpuestos "+cod);
	listar(codigo);
	
	
}
//Función Listar
function listar(codigo)
{
	tabla=$('#tbllistado').dataTable(
	{
		"scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false,
        
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],
		"ajax":
				{
					url: '../ajax/puestos_usuarios.php?op=listar',
					data:{idpuestos:codigo},
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar



function mostrar(idusuarios)
{
	$.post("../ajax/puestos_usuarios.php?op=mostrar",{idpuestos_usuarios : idpuestos_usuarios}, function(data, status)
	{
		data = JSON.parse(data);		
		mostrarform(true);
		$("#idusuarios").val(data.idusuarios);
		$("#idpuestos").val(data.idpuestos);
		

 	})
}
function agregar()
{
	idpuestos=$("#idpuestos").val();
	idusuarios=$("#idusuarios").val();
	//alert("id puestos "+idpuestos+" idsuarios "+idusuarios);
	$.post("../ajax/puestos_usuarios.php?op=insertar",{idpuestos : idpuestos,idusuarios : idusuarios}, function(data, status)
	{
		//alert("datos devueltos  "+data);

		Swal.fire({
			  position: 'top-end',
			  type: 'success',
			  title: 'Sus Datos se han  guardado',
			  showConfirmButton: false,
			  timer: 1500
		})
		mostrarform(false);
		listar(idpuestos);
		tabla.ajax.reload();

 	})
}

//Función para desactivar registros
function desactivar(idusuarios)
{
	//idusuarios=$("#idusuarios").val();
	idpuestos=$("#idpuestos").val();
	//alert("id usuario "+idusuarios+" id puestos "+idpuestos);
	if (idusuarios>0)
	{
		bootbox.confirm("¿Está Seguro de desactivar  Accesos de Puestos", function(result){
			if(result)
	        {
	        	$.post("../ajax/puestos_usuarios.php?op=eliminar", {idusuarios : idusuarios}, function(e){
	        		bootbox.alert(e);
	        		listar(idpuestos);
		            tabla.ajax.reload();
	        	});	
	        }
		})
	}
}



init();