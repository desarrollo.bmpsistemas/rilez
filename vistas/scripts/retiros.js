var tabla;
var Opcion=0;
var referencia="0";  
var fecha;
var capturista_id=0;
//Función que se ejecuta al inicio
function init(){
   $("#modulo").html("Abc de Retiros");
  //$('#idpersonas').prop('disabled', 'disabled');
  mostrarform(false);
  tomar_fecha();
  idusuarios=$('#idusuarios').val();  
  idalmacenes=$('#Sidalmacenes').val();
  //document.getElementById('rbnombrep').checked = true; 
  checarRadioButton(2);
  listar();
  $( "#busqueda" ).keydown(function(e) {
      var code = (e.keyCode ? e.keyCode : e.which);
    //alert("valor"+code);
        if(code==13){

            listar();
        }
  });
  $( "#busquedap" ).keydown(function(e) {
      var code = (e.keyCode ? e.keyCode : e.which);
    //alert("valor"+code);
        if(code==13){
          
            buscarPersonas(1);
        }
  });

  $.post("../ajax/retiros.php?op=selectCat_de_Origenes", function(r){
              $("#idcat_de_origeness").html(r);
              $('#idcat_de_origeness').selectpicker('refresh');

  });
  $.post("../ajax/retiros.php?op=selectDestinos", function(r){
              $("#iddestinoss").html(r);
              $('#iddestinoss').selectpicker('refresh');

  });


  $("#formulario").on("submit",function(e)
  {
    guardaryeditar(e);  
  })
}

//Función limpiar
function tomar_fecha()
{
 
  fecha=formato_fecha();
 
  $('#fecha').val(fecha);
  $('#fecha_inicial').val(fecha);
  $('#fecha_final').val(fecha);
 
  //alert("feecha "+fecha);
}
function formato_fecha()
{
  var now = new Date();
  var day = ("0" + now.getDate()).slice(-2);
  var month = ("0" + (now.getMonth() + 1)).slice(-2);
  var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
    return today;
}


function MostrarModalOpcionPersona()
{
  $("#myModalBusquedaPersona").modal("show"); 
}
function checarRadioButton(e)
{       
    ref=$("#busqueda").val();
    Opcion=e;
    $("#myModalBusquedaPersona .close").click();
}
function limpiar()
{
  $("#nombre").val("");
  $("#num_documento").val("");
  $("#direccion").val("");
  $("#telefono").val("");
  $("#email").val("");
  $("#idpersonas").val("0");
}

//Función mostrar formulario
function mostrarform(flag)
{
  //limpiar();

  if (flag)
  {
    idcat_de_origenes=$('select[id=idcat_de_origeness]').val();
    iddestinos=$('select[id=iddestinoss]').val();
    
    $("#idcat_de_origenes").val(idcat_de_origenes);
    $("#iddestinos").val(iddestinos);

    $("#listadoregistros").hide();
    $("#formularioregistros").show();
    $("#idsaldos").val('');
  }
  else
  {
    $("#listadoregistros").show();
    $("#formularioregistros").hide();
    $("#btnagregar").show();
    //alert("ocultando formulario ");
  }
}

//Función cancelarform
function cancelarform()
{
  limpiar();
  mostrarform(false);
}
function selecionarCapturista()
{
    iddestinos=$('select[id=iddestinoss]').val();
    if (iddestinos==5)
    {
      $("#myModalCajaChica").modal("show"); 
    }
}


//Función Listar
function listar()
{
  fecha_inicial=$('#fecha_inicial').val();
  fecha_final=$('#fecha_final').val();
  
  //alert(" fi "+fecha_inicial+" ff "+fecha_final);
  tabla=$('#tbllistado').dataTable(
  {
    "scrollY":        "200px",
    "scrollCollapse": true,
    "paging":         false,
    "aProcessing": true,//Activamos el procesamiento del datatables
      "aServerSide": true,//Paginación y filtrado realizados por el servidor
      dom: 'Bfrtip',//Definimos los elementos del control de tabla
      buttons: [              
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdf'
            ],

    "ajax":
        {
          url: '../ajax/retiros.php?op=listar',
          data:{fecha_inicial:fecha_inicial,fecha_final:fecha_final},
          type : "get",
          dataType : "json",            
          error: function(e){
            console.log(e.responseText);  
          }
        },
    "bDestroy": true,
    "iDisplayLength": 5,//Paginación
      "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
  }).DataTable();
}

function listarDestinos()
{
  fecha_inicial=$('#fecha_inicial').val();
  fecha_final=$('#fecha_final').val();
  iddestinos=$('select[id=iddestinoss]').val();
  //alert(" fi "+fecha_inicial+" ff "+fecha_final);
  tabla=$('#tbllistado').dataTable(
  {
    "scrollY":        "200px",
    "scrollCollapse": true,
    "paging":         false,
    "aProcessing": true,//Activamos el procesamiento del datatables
      "aServerSide": true,//Paginación y filtrado realizados por el servidor
      dom: 'Bfrtip',//Definimos los elementos del control de tabla
      buttons: [              
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdf'
            ],

    "ajax":
        {
          url: '../ajax/retiros.php?op=listarDestinos',
          data:{fecha_inicial:fecha_inicial,fecha_final:fecha_final,iddestinos:iddestinos},
          type : "get",
          dataType : "json",            
          error: function(e){
            console.log(e.responseText);  
          }
        },
    "bDestroy": true,
    "iDisplayLength": 5,//Paginación
      "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
  }).DataTable();
}


function listarDestinosPorCapturista()
{
  fecha_inicial=$('#fecha_inicial').val();
  fecha_final=$('#fecha_final').val();
  iddestinos=$('select[id=iddestinoss]').val();
  capturista_id=$('select[id=idpersonas]').val();
  //alert(" fi "+fecha_inicial+" ff "+fecha_final);
  tabla=$('#tbllistado').dataTable(
  {
    "scrollY":        "200px",
    "scrollCollapse": true,
    "paging":         false,
    "aProcessing": true,//Activamos el procesamiento del datatables
      "aServerSide": true,//Paginación y filtrado realizados por el servidor
      dom: 'Bfrtip',//Definimos los elementos del control de tabla
      buttons: [              
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdf'
            ],

    "ajax":
        {
          url: '../ajax/retiros.php?op=listarDestinosPorCapturista',
          data:{fecha_inicial:fecha_inicial,fecha_final:fecha_final,iddestinos:iddestinos,capturista_id:capturista_id},
          type : "get",
          dataType : "json",            
          error: function(e){
            console.log(e.responseText);  
          }
        },
    "bDestroy": true,
    "iDisplayLength": 5,//Paginación
      "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
  }).DataTable();
}
function listarPagos(idsaldos)
{
  
  //alert(" Opcion "+Opcion+" referencia "+referencia);
  tabla=$('#tbllistadop').dataTable(
  {
    "scrollY":        "200px",
    "scrollCollapse": true,
    "paging":         false,
    "aProcessing": true,//Activamos el procesamiento del datatables
      "aServerSide": true,//Paginación y filtrado realizados por el servidor
      dom: 'Bfrtip',//Definimos los elementos del control de tabla
      buttons: [              
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdf'
            ],

    "ajax":
        {
          url: '../ajax/retiros.php?op=listarPagos',
          data:{idsaldos:idsaldos},
          type : "get",
          dataType : "json",            
          error: function(e){
            console.log(e.responseText);  
          }
        },
    "bDestroy": true,
    "iDisplayLength": 5,//Paginación
      "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
  }).DataTable();
}

//Función para guardar o editar
function mostrarFondos()
{
  idcat_de_origenes=$('select[id=idcat_de_origeness]').val();
  $.post("../ajax/retiros.php?op=mostrarFondos",{idcat_de_origenes : idcat_de_origenes}, function(data, status)
  {
    data = JSON.parse(data);    
    $("#entradas").val(data.entradas);
    $("#saldo").val(data.saldo);
    
  }) 
}
function mostrarModalFechas()
{

   $("#myModalFechas").modal("show"); 
}



function guardaryeditar(e)
{
  e.preventDefault(); //No se activará la acción predeterminada del evento
  //$("#btnGuardar").prop("disabled",true);
  var formData = new FormData($("#formulario")[0]);
  //referencia=$("#referencia").val();
  
  var dataString = $('#formulario').serialize();
  //alert('Datos del formulario: '+dataString);

  $.ajax({
    url: "../ajax/retiros.php?op=guardaryeditar",
      type: "POST",
      data: formData,
      contentType: false,
      processData: false,

      success: function(datos)
      {                    
            bootbox.alert(datos);           
            mostrarform(false);
            tabla.ajax.reload();
      }

  });
  limpiar();
}

function mostrar(idsaldos)
{
  $.post("../ajax/retiros.php?op=mostrar",{idsaldos : idsaldos}, function(data, status)
  {
    data = JSON.parse(data);    
    mostrarform(true);
    
    
    $("#fecha").val(data.fecha);
    $("#importe").val(data.importe);
    $("#nota").val(data.nota);
    $("#idpersonas").val(idpersonas);
    $("#idsaldos").val(idsaldos);
  })
}
function verHistorial()
{
  $.post("../ajax/retiros.php?op=verHistorial",{idpersonas : idpersonas}, function(data, status)
  {
    data = JSON.parse(data);    
    
    adeudo=number(data.total);
    //alert("su adeudo es "+adeudo);
    

  })
}

//Función para desactivar registros
function desactivar(iddet_destinos)
{
 //capturista_id=$('select[id=idpersonas]').val();
 //capturista=$('select[name="idpersonas"] option:selected').text();
 iddestinos=$('select[id=iddestinoss]').val();
 //alert("capturista destino caja chica es "+capturista_id);
 //ver si se trata de caja chica
 iddetdestinos=0;
 $.post("../ajax/retiros.php?op=verSiEsCajaChica",{iddet_destinos : iddet_destinos}, function(data, status)
  {
    data = JSON.parse(data);    
    iddestinos=data.destinos_id;
    capturista_id=data.capturista_id;
  })

 if (iddestinos==5)
 {
    bootbox.confirm("¿Está Seguro de eliminar destino de caja chica con id "+capturista_id, function(result){
    if(result)
          {
            $.post("../ajax/retiros.php?op=desactivar", {iddet_destinos : iddet_destinos,capturista_id:capturista_id}, function(e){
              //bootbox.alert(e);
                tabla.ajax.reload();
            }); 
          }
    })
 }
 else
 {
    capturista_id=0;
    bootbox.confirm("¿Está Seguro de eliminar destino ?", function(result){
    if(result)
          {
            $.post("../ajax/retiros.php?op=desactivar", {iddet_destinos : iddet_destinos,capturista_id:capturista_id}, function(e){
              //bootbox.alert(e);
                tabla.ajax.reload();
            }); 
          }
    })
  }
}


//Función para activar registros
function activar(idpersonas)
{
  bootbox.confirm("¿Está Seguro de activar el estado del cliente?", function(result){
    if(result)
        {
          $.post("../ajax/retiros.php?op=activar", {idpersonas : idpersonas}, function(e){
           // bootbox.alert(e);
              tabla.ajax.reload();
          }); 
        }
  })
}

function buscarCapturista()
{
  capturista=$("#busquedac").val();
  $.post("../ajax/retiros.php?op=selectCapturistas",{referencia:capturista}, function(r){
    
              $("#idpersonas").html(r);
              $('#idpersonas').selectpicker('refresh');

  });
}
function tomarCapturista()
{
  capturista_id=$('select[id=idpersonas]').val();
  $("#capturista_id").val(capturista_id);
}

function buscarPersonas(e) 
{
  //alert("buscarndo personas "+Opcion);

 
  if (e==1)
  {
    //alert("buscando clientes");
     if ( $('input:radio[name=rbidpersona]:checked').val())
      Opcion=0;
    if ( $('input:radio[name=rbrfcp]:checked').val())
      Opcion=1;
    if ( $('input:radio[name=rbnombrep]:checked').val())
      Opcion=2;
    if ( $('input:radio[name=rbtelefonop]:checked').val())
      Opcion=3;
    ref=$("#busquedap").val();
  }
  
  
  
  
   
    if (ref.length>0)
    {
      $.post("../ajax/proveedores.php?op=select",{Opcion:Opcion,referencia : ref}, function(r){
          //alert("datos "+r);  
                $("#idpersonas").html(r);
                $('#idpersonas').selectpicker('refresh');

      });
    }
    else
    {
      mensajeError("No ha selecionado que buscar ");
    }
  
}

init();