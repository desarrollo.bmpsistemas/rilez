var tabla;
var Opcion=0;
var referencia="0"; 
var fecha;
//Función que se ejecuta al inicio
function init(){
   $("#modulo").html("Abc de Saldos a Favor");
  //$('#idpersonas').prop('disabled', 'disabled');
  mostrarform(false);
  tomar_fecha();
  idusuarios=$('#idusuarios').val();  
  idalmacenes=$('#Sidalmacenes').val();
  document.getElementById('rbnombrep').checked = true; 
  checarRadioButton(2);
  listar(Opcion);
  $( "#busqueda" ).keydown(function(e) {
      var code = (e.keyCode ? e.keyCode : e.which);
    //alert("valor"+code);
        if(code==13){

            listar();
        }
  });
  $( "#busquedap" ).keydown(function(e) {
      var code = (e.keyCode ? e.keyCode : e.which);
    //alert("valor"+code);
        if(code==13){
          
            buscarPersonas(1);
        }
  });




  $("#formulario").on("submit",function(e)
  {
    guardaryeditar(e);  
  })
}

//Función limpiar
function tomar_fecha()
{
 
  fecha=formato_fecha();
 
  $('#fecha').val(fecha);
 
  //alert("feecha "+fecha);
}
function formato_fecha()
{
  var now = new Date();
  var day = ("0" + now.getDate()).slice(-2);
  var month = ("0" + (now.getMonth() + 1)).slice(-2);
  var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
    return today;
}


function MostrarModalOpcionPersona()
{
  $("#myModalBusquedaPersona").modal("show"); 
}
function checarRadioButton(e)
{       
    ref=$("#busqueda").val();
    Opcion=e;
    $("#myModalBusquedaPersona .close").click();
}
function limpiar()
{
  $("#nombre").val("");
  $("#num_documento").val("");
  $("#direccion").val("");
  $("#telefono").val("");
  $("#email").val("");
  $("#idpersonas").val("0");
}

//Función mostrar formulario
function mostrarform(flag)
{
  //limpiar();

  if (flag)
  {
    $("#listadoregistros").hide();
    $("#formularioregistros").show();
    $("#idsaldos").val('');
  }
  else
  {
    $("#listadoregistros").show();
    $("#formularioregistros").hide();
    $("#btnagregar").show();
    //alert("ocultando formulario ");
  }
}

//Función cancelarform
function cancelarform()
{
  limpiar();
  mostrarform(false);
}

//Función Listar
function listar()
{
  idpersonas=$('select[id=idpersonas]').val();
  $('#proveedor_id').val(idpersonas);
  
  //alert(" Opcion "+Opcion+" referencia "+referencia);
  tabla=$('#tbllistado').dataTable(
  {
    "scrollY":        "200px",
    "scrollCollapse": true,
    "paging":         false,
    "aProcessing": true,//Activamos el procesamiento del datatables
      "aServerSide": true,//Paginación y filtrado realizados por el servidor
      dom: 'Bfrtip',//Definimos los elementos del control de tabla
      buttons: [              
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdf'
            ],

    "ajax":
        {
          url: '../ajax/saldos_a_favor.php?op=listar',
          data:{idpersonas:idpersonas},
          type : "get",
          dataType : "json",            
          error: function(e){
            console.log(e.responseText);  
          }
        },
    "bDestroy": true,
    "iDisplayLength": 5,//Paginación
      "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
  }).DataTable();
}
function listarPagos(idsaldos)
{
  
  //alert(" Opcion "+Opcion+" referencia "+referencia);
  tabla=$('#tbllistadop').dataTable(
  {
    "scrollY":        "200px",
    "scrollCollapse": true,
    "paging":         false,
    "aProcessing": true,//Activamos el procesamiento del datatables
      "aServerSide": true,//Paginación y filtrado realizados por el servidor
      dom: 'Bfrtip',//Definimos los elementos del control de tabla
      buttons: [              
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdf'
            ],

    "ajax":
        {
          url: '../ajax/saldos_a_favor.php?op=listarPagos',
          data:{idsaldos:idsaldos},
          type : "get",
          dataType : "json",            
          error: function(e){
            console.log(e.responseText);  
          }
        },
    "bDestroy": true,
    "iDisplayLength": 5,//Paginación
      "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
  }).DataTable();
}

//Función para guardar o editar
function agregarDatos(Opc)
{
  /*if (Opc==1)
    $("#myModalCalles").modal("show");
  if (Opc==2)
    $("#myModalColonias").modal("show");
  if (Opc==3)
    $("#myModalMunicipios").modal("show");*/

}
function guardaryeditar(e)
{
  e.preventDefault(); //No se activará la acción predeterminada del evento
  //$("#btnGuardar").prop("disabled",true);
  var formData = new FormData($("#formulario")[0]);
  //referencia=$("#referencia").val();
  
  var dataString = $('#formulario').serialize();
  //alert('Datos del formulario: '+dataString);

  $.ajax({
    url: "../ajax/saldos_a_favor.php?op=guardaryeditar",
      type: "POST",
      data: formData,
      contentType: false,
      processData: false,

      success: function(datos)
      {                    
            bootbox.alert(datos);           
            mostrarform(false);
            tabla.ajax.reload();
      }

  });
  limpiar();
}

function mostrar(idsaldos)
{
  $.post("../ajax/saldos_a_favor.php?op=mostrar",{idsaldos : idsaldos}, function(data, status)
  {
    data = JSON.parse(data);    
    mostrarform(true);
    
    
    $("#fecha").val(data.fecha);
    $("#importe").val(data.importe);
    $("#nota").val(data.nota);
    $("#idpersonas").val(idpersonas);
    $("#idsaldos").val(idsaldos);
  })
}
function verHistorial()
{
  $.post("../ajax/saldos_a_favor.php?op=verHistorial",{idpersonas : idpersonas}, function(data, status)
  {
    data = JSON.parse(data);    
    
    adeudo=number(data.total);
    alert("su adeudo es "+adeudo);
    

  })
}

//Función para desactivar registros
function desactivar(idsaldos)
{
  bootbox.confirm("¿Está Seguro de eliminar saldo a favor?", function(result){
    if(result)
        {
          $.post("../ajax/saldos_a_favor.php?op=desactivar", {idsaldos : idsaldos}, function(e){
            //bootbox.alert(e);
              tabla.ajax.reload();
          }); 
        }
  })
}


//Función para activar registros
function activar(idpersonas)
{
  bootbox.confirm("¿Está Seguro de activar el estado del cliente?", function(result){
    if(result)
        {
          $.post("../ajax/saldos_a_favor.php?op=activar", {idpersonas : idpersonas}, function(e){
           // bootbox.alert(e);
              tabla.ajax.reload();
          }); 
        }
  })
}

function selectpaises()
{
  
  $.post("../ajax/paises.php?op=select", function(r){
    
              $("#idpaisesm").html(r);
              $('#idpaisesm').selectpicker('refresh');

  });
}

function selectestados()
{
  idpaises=$('select[id=idpaisesm]').val();
  
  //Cargamos los items al select estados
  //alert("id paiese "+idpaises);
  $.post("../ajax/estados.php?op=selectestados", {idpaises : idpaises}, function(r){
    //alert("resultado "+r);
              $("#idestadosm").html(r);
              $('#idestadosm').selectpicker('refresh');

  });
}
/*function selectmunicipios()
{
 idestados=0;
  
  $.post("../ajax/municipios.php?op=selectMunicipios", {idestados : idestados}, function(r){
    //alert("resultado "+r);
              $("#idmunicipios").html(r);
              $('#idmunicipios').selectpicker('refresh');

  });
}*/

function buscarPersonas(e) 
{
  //alert("buscarndo personas "+Opcion);

 
  if (e==1)
  {
    //alert("buscando clientes");
     if ( $('input:radio[name=rbidpersona]:checked').val())
      Opcion=0;
    if ( $('input:radio[name=rbrfcp]:checked').val())
      Opcion=1;
    if ( $('input:radio[name=rbnombrep]:checked').val())
      Opcion=2;
    if ( $('input:radio[name=rbtelefonop]:checked').val())
      Opcion=3;
    ref=$("#busquedap").val();
  }
  
  
  
  
   
    if (ref.length>0)
    {
      $.post("../ajax/proveedores.php?op=select",{Opcion:Opcion,referencia : ref}, function(r){
          //alert("datos "+r);  
                $("#idpersonas").html(r);
                $('#idpersonas').selectpicker('refresh');

      });
    }
    else
    {
      mensajeError("No ha selecionado que buscar ");
    }
  
}









init();