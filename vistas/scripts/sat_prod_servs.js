 var tabla;
var Opcion=1;
var referencia='01010101';
//Función que se ejecuta al inicio
function init(){
	 $("#modulo").html("Abc de Prod y Serv");
	mostrarform(false); 
	listar(Opcion,referencia);

	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	})
}

//Función limpiar
function limpiar()
{
	$("#idsat_prod_servs").val("");
	$("#clave").val("");
	$("#nombre").val("");

}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
	}
}
function selectBusqueda()
{
	listar(Opcion,referencia);
	
}
function selectFocus(e)
{
	if (e==1)
	{
		Opcion=e;
		referencia=$("#codigob").val();	
		//alert("valor 1 "+referencia.length);
		
	}
	if (e==2)
	{
		Opcion=e;
		referencia=$("#nombreb").val();	
		//alert("valor 2 "+referencia.length);
	}
	
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}

//Función Listar
function listar(Opcion,referencia)
{
	if (Opcion>0 && referencia.length>3)
	{
		tabla=$('#tbllistado').dataTable(
		{
			"aProcessing": true,//Activamos el procesamiento del datatables
		    "aServerSide": true,//Paginación y filtrado realizados por el servidor
		    dom: 'Bfrtip',//Definimos los elementos del control de tabla
		    buttons: [		          
			            'copyHtml5',
			            'excelHtml5',
			            'csvHtml5',
			            'pdf'
			        ],
			"ajax":
					{
						url: '../ajax/sat_prod_servs.php?op=listar',
						data:{Opcion:Opcion,referencia:referencia},
						type : "get",
						dataType : "json",						
						error: function(e){
							console.log(e.responseText);	
						}
					},
			"bDestroy": true,
			"iDisplayLength": 5,//Paginación
		    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
		}).DataTable();
	}
}
//Función para guardar o editar

function guardaryeditar(e)
{
	e.preventDefault(); //No se activará la acción predeterminada del evento
	$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]);

	$.ajax({
		url: "../ajax/sat_prod_servs.php?op=guardaryeditar",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {                    
	          bootbox.alert(datos);	          
	          mostrarform(false);
	          tabla.ajax.reload();
	    }

	});
	limpiar();
}

function mostrar(idsat_prod_servs)
{
	$.post("../ajax/sat_prod_servs.php?op=mostrar",{idsat_prod_servs : idsat_prod_servs}, function(data, status)
	{
		data = JSON.parse(data);		
		mostrarform(true);

		$("#idsat_prod_servs").val(data.idsat_prod_servs);
		$("#clave").val(data.clave);
		$("#nombre").val(data.nombre);

 	})
}

//Función para desactivar registros
function desactivar(idsat_prod_servs)
{
	bootbox.confirm("¿Está Seguro de desactivar el Sat productos servicios? ", function(result){
		if(result)
        {
        	$.post("../ajax/sat_prod_servs.php?op=desactivar", {idsat_prod_servs : idsat_prod_servs}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(idsat_prod_servs)
{
	bootbox.confirm("¿Está Seguro de activar Sat productos servicios ?", function(result){
		if(result)
        {
        	$.post("../ajax/sat_prod_servs.php?op=activar", {idsat_prod_servs : idsat_prod_servs}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}


init();