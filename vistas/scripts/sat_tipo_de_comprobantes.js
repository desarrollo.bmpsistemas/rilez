  var tabla;

//Función que se ejecuta al inicio
function init(){
	 $("#modulo").html("Abc de Tipos de Comprobantes");
	mostrarform(false);
	listar();

	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	})
}

//Función limpiar
function limpiar()
{
	$("#nombre").val("");
	$("#idsat_tipo_de_comprobantes").val("");
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}

//Función Listar
function listar()
{
	tabla=$('#tbllistado').dataTable(
	{
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],
		"ajax":
				{
					url: '../ajax/sat_tipo_de_comprobantes.php?op=listar',
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar

function guardaryeditar(e)
{
	e.preventDefault(); //No se activará la acción predeterminada del evento
	$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]);

	$.ajax({
		url: "../ajax/sat_tipo_de_comprobantes.php?op=guardaryeditar",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {                    
	          bootbox.alert(datos);	          
	          mostrarform(false);
	          tabla.ajax.reload();
	    }

	});
	limpiar();
}

function mostrar(idsat_tipo_de_comprobantes)
{
	$.post("../ajax/sat_tipo_de_comprobantes.php?op=mostrar",{idsat_tipo_de_comprobantes : idsat_tipo_de_comprobantes}, function(data, status)
	{
		data = JSON.parse(data);		
		mostrarform(true);

		$("#nombre").val(data.nombre);
		$("#idsat_tipo_de_comprobantes").val(data.idsat_tipo_de_comprobantes);

 	})
}

//Función para desactivar registros
function desactivar(idsat_tipo_de_comprobantes)
{
	bootbox.confirm("¿Está Seguro de desactivar el Sat Tipo de Comprobante?", function(result){
		if(result)
        {
        	$.post("../ajax/sat_tipo_de_comprobantes.php?op=desactivar", {idsat_tipo_de_comprobantes : idsat_tipo_de_comprobantes}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(idsat_tipo_de_comprobantes)
{
	bootbox.confirm("¿Está Seguro de activar Sat Tipo de Comprobante ?", function(result){
		if(result)
        {
        	$.post("../ajax/sat_tipo_de_comprobantes.php?op=activar", {idsat_tipo_de_comprobantes : idsat_tipo_de_comprobantes}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}


init();