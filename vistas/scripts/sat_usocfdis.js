var tabla;

//Función que se ejecuta al inicio 
function init(){
	 $("#modulo").html("Abc de Usos de Cfdi");
	mostrarform(false);
	listar();

	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	})
}

//Función limpiar
function limpiar()
{
	$("#idsat_usocfdis").val("");
	$("#clave").val("");
	$("#descripcion").val("");

}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}

//Función Listar
function listar()
{
	tabla=$('#tbllistado').dataTable(
	{
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],
		"ajax":
				{
					url: '../ajax/sat_usocfdis.php?op=listar',
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar

function guardaryeditar(e)
{
	e.preventDefault(); //No se activará la acción predeterminada del evento
	$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]);

	$.ajax({
		url: "../ajax/sat_usocfdis.php?op=guardaryeditar",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {                    
	          bootbox.alert(datos);	          
	          mostrarform(false);
	          tabla.ajax.reload();
	    }

	});
	limpiar();
}

function mostrar(idsat_usocfdis)
{
	$.post("../ajax/sat_usocfdis.php?op=mostrar",{idsat_usocfdis : idsat_usocfdis}, function(data, status)
	{
		data = JSON.parse(data);		
		mostrarform(true);

		$("#idsat_usocfdis").val(data.idsat_usocfdis);
		$("#clave").val(data.clave);
		$("#descripcion").val(data.descripcion);

 	})
}

//Función para desactivar registros
function desactivar(idsat_usocfdis)
{
	bootbox.confirm("¿Está Seguro de desactivar el Sat uso Cfdi ? ", function(result){
		if(result)
        {
        	$.post("../ajax/sat_usocfdis.php?op=desactivar", {idsat_usocfdis : idsat_usocfdis}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(idsat_usocfdis)
{
	bootbox.confirm("¿Está Seguro de activar Sat uso Cfdi ?", function(result){
		if(result)
        {
        	$.post("../ajax/sat_usocfdis.php?op=activar", {idsat_usocfdis : idsat_usocfdis}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}


init();