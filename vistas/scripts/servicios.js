var tabla;
var fecha_inicial;
var fecha_final;
var fecha;
var condicion=0;
var idusuarios;
var idalmacenes;
//Función que se ejecuta al inicio 
function init(){
	 $("#modulo").html("Abc de Servicios");
	mostrarform(false);
		
	fecha=formato_fecha();
	$('#fecha_inicial').val(fecha);
	$('#fecha_final').val(fecha);
	$('#fecha_entrada').val(fecha);
	$('#fecha_prox_serv').val(fecha);
	idusuarios=$('#idusuarios').val();	
	idalmacenes=$('#Sidalmacenes').val();
	$('#idservicios').val(0);
	fecha_inicial=fecha;
	fecha_final=fecha;

	listar(fecha_inicial,fecha_final);


	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	})


	

	//Cargamos los items al select sublineas
	$.post("../ajax/usuarios.php?op=select", function(r){
	            $("#idusuarios").html(r);
	            $('#idusuarios').selectpicker('refresh');
	            
	});
	$.post("../ajax/personas.php?op=selectc", function(r){
	            $("#idpersonas").html(r);
	            $('#idpersonas').selectpicker('refresh');
	           
	});

	

	//$("#imagenmuestra").hide();
}

//Función limpiar
function limpiar()
{
	
	$("#imagenmuestra").attr("src","");
	$("#imagenactual").val("");
	
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		//$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
		$("#divfechainicial").hide();
		$("#divfechafinal").hide();
		$("#divagregar").hide();
		$("#asunto").prop("readonly", false);
	    $("#contestacion").prop("readonly", true);
		$("#idservicios").val(0);

	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
		$("#divfechainicial").show();
		$("#divfechafinal").show();
		$("#divagregar").show();
	}
	
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}
function formato_fecha()
{
	var now = new Date();
	var day = ("0" + now.getDate()).slice(-2);
	var month = ("0" + (now.getMonth() + 1)).slice(-2);
	var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
    return today;
}
//Función Listar
function listar(idalmacenes,fecha_inicial,fecha_final)
{
	tabla=$('#tbllistado').dataTable(
	{

		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    "scrollX": true,

	    "scrollY":        "300px",
        "scrollCollapse": true,
        "paging":         false,
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],
		"ajax":
				{
					url: '../ajax/servicios.php?op=listar',
					data:{idalmacenes:idalmacenes,fecha_inicial: fecha_inicial,fecha_final: fecha_final},
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar

function guardaryeditar(e)
{

	//imagen=$("#imagen").val();
	//alert("imagen "+imagen );
	e.preventDefault(); //No se activará la acción predeterminada del evento
	//$("#btnGuardar").prop("disabled",true);
	var dataString = $('#formulario').serialize();
    //alert('Datos del formulario: '+dataString);
	var formData = new FormData($("#formulario")[0]);

	$.ajax({
		url: "../ajax/servicios.php?op=guardaryeditar",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {                    
	    	  //$("#imagen").val(datos);
	    	  //alert("datos regresados "+datos);
	    	  listar(fecha_inicial,fecha_final);

	          //bootbox.alert(datos);	
	          //mostrarMensaje(mensaje);
	          //mostrarform(false);
	          tabla.ajax.reload();
	        Swal.fire({
				  position: 'top-end',
				  type: 'success',
				  title: 'Sus Datos se han  guardado',
				  showConfirmButton: false,
				  timer: 1500
			})
	    }

	});
	//limpiar();
	//formularioregistros
}
function mostrarMensaje(mensaje)
{
	swal({   
		title: mensaje,   
		text: "Continuar!!!",   
		timer: 2500,   
		showConfirmButton: true 
	});
}
function mostrar(idservicios)
{
	//alert("idservicios "+idservicios);
	$('#idservicios').val(idservicios);
	id=$('#idservicios').val();
	//alert("id "+id);
	if (idservicios>0)
	{
		$.post("../ajax/servicios.php?op=mostrar",{idservicios : idservicios}, function(data, status)
		{
			
			data = JSON.parse(data);		
			
			//mostrarform(true);
			
			$("#cliente").val(data.cliente);
			$("#importe").val(data.importe);
			$("#fecha_de_entrada").val(data.fecha_de_entrada);
			$("#fecha_prox_serv").val(data.fecha_prox_serv);
			$("#servicio_a_realizar").val(data.servicio_a_realizar);
			$("#servicio_realizado").val(data.servicio_realizado);
			
			$("#imagenmuestra").show();
			$("#imagenmuestra").attr("src","../files/servicios/"+data.imagen);
			$("#imagenactual").val(data.imagen);
	 		mostrarMensaje("Ahora ya puede presionar el boton cerrar servicio");
	 		
	 		

	 	})
	}
	else
	mostrarMensaje("Debe dar click al servicio a terminar en donde dice terminar servicio");
}

function editar()
{
	//alert("idservicios "+idservicios);
	
	idservicios=$('#idservicios').val();
	
	servicio_realizado=$("#servicio_realizado").val();
	
	
	if (idservicios>0)
	{
		$.post("../ajax/servicios.php?op=terminarServicio",{idservicios : idservicios,servicio_realizado:servicio_realizado}, function(data, status)
		{
			mostrarMensaje(data);
		})
	}
	else
	mostrarMensaje("Debe dar click primero debe editar el servicio");
}

//Función para desactivar registros
function desactivar(idservicios)
{
	bootbox.confirm("¿Está Seguro de desactivar el pendiete?", function(result){
		if(result)
        {
        	$.post("../ajax/servicios.php?op=desactivar", {idservicios : idservicios}, function(e){
        		//bootbox.alert(e);
	            tabla.ajax.reload();
	            //mostrarservicios();
        	});	
        }
	})
}

//Función para activar registros
function activar(idservicios)
{
	bootbox.confirm("¿Está Seguro de activar el Pendiente?", function(result){
		if(result)
        {
        	$.post("../ajax/servicios.php?op=activar", {idservicios : idservicios}, function(e){
        		//bootbox.alert(e);
	            tabla.ajax.reload(); 
	            //mostrarservicios();
        	});	
        }
	})
}


function mostrarServicios()
{
	fecha_inicial=$("#fecha_inicial").val();
	fecha_final=$("#fecha_final").val();
	idalmacenes=$("#idalmacenes").val();
	listar(idalmacenes,fecha_inicial,fecha_final);
}

init();