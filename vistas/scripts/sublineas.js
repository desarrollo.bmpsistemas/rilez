var tabla;

//Función que se ejecuta al inicio 
function init(){
	 $("#modulo").html("Abc de Sublineas");
	mostrarform(false);
	listar();

	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	})

	//Cargamos los items al select lineas
	$.post("../ajax/lineas.php?op=select", function(r){
	            $("#idlineass").html(r);
	            $('#idlineass').selectpicker('refresh');

	});
	//$("#imagenmuestra").hide();
}

//Función limpiar
function limpiar()
{
	
	$("#nombre").val("");
	//$("#idlineas").val("");
	//$("#idlineas").val("");
}
function selectsublinea(e)
{
		
	idlineas=$('select[id=idlineas]').val();
				

	//Cargamos los items al select estados
	//alert("id lineas "+idlineas);
	$.post("../ajax/sublineas.php?op=selectporlinea", {idlineas : idlineas}, function(r)
	{
		//alert("resultado "+r);
		
	    $("#idsublineas").html(r);
	    $('#idsublineas').selectpicker('refresh');
	    

	});
}
function focusInLineas()
{
	idlineas=$('select[id=idlineass]').val();
	//alert("linea "+idlineas);
	$('#idlineas').val(idlineas);


	listar(idlineas);
	
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		$("#listadoregistros").hide();
		$("#idsublineas").val(0);
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();

	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}

//Función Listar

function listar(idlineas)
{
	tabla=$('#tbllistado').dataTable(
	{
		"scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false,

		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],
		"ajax":
				{
					url: '../ajax/sublineas.php?op=listar',
					data:{idlineas:idlineas},
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}

//Función para guardar o editar

function guardaryeditar(e)
{
	e.preventDefault(); //No se activará la acción predeterminada del evento
	$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]);
	idlineas=$("#idlineas").val();
	idsublineas=$("#idsublineas").val();
	alert("id linea "+idlineas+" sub "+idsublineas);
	$.ajax({
		url: "../ajax/sublineas.php?op=guardaryeditar",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {                    
	          //bootbox.alert(datos);	          
	          mostrarform(false);
	          tabla.ajax.reload();
	    }

	});
	limpiar();
}

function mostrar(idsublineas)
{
	$.post("../ajax/sublineas.php?op=mostrar",{idsublineas : idsublineas}, function(data, status)
	{
		data = JSON.parse(data);		
		mostrarform(true);
		
		$("#idlineas").val(data.idlineas);
		$('#idlineas').selectpicker('refresh');
		
		$("#nombre").val(data.sublinea);
		$("#idsublineas").val(data.idsublineas);
 		//generarbarcode();

 	})
}

//Función para desactivar registros
function desactivar(idsublineas)
{
	bootbox.confirm("¿Está Seguro de desactivar el Sublineas?", function(result){
		if(result)
        {
        	$.post("../ajax/sublineas.php?op=desactivar", {idsublineas : idsublineas}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(idsublineas)
{
	bootbox.confirm("¿Está Seguro de activar el Sublineas?", function(result){
		if(result)
        {
        	$.post("../ajax/sublineas.php?op=activar", {idsublineas : idsublineas}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload(); 
        	});	
        }
	})
}

//función para generar el código de barras

init();