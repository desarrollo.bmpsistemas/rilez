var tabla;
var Opcion=1;
var referencia=1;
//Función que se ejecuta al inicio 
function init(){
	 $("#modulo").html("Abc de Usuarios");
	mostrarform(false);
	listar();

	$("#formulario").on("submit",function(e) 
	{
		guardaryeditar(e);	 
	})

	
	$("#imagenmuestra").hide();
	
	$.post("../ajax/sucursales.php?op=select",{Opcion,referencia}, function(r){
	            $("#idsucursales").html(r);
	            $('#idsucursales').selectpicker('refresh');
	            $("#idsucursaless").html(r);
	            $('#idsucursaless').selectpicker('refresh');


	});

	$.post("../ajax/departamentos.php?op=select", function(r){
	            $("#iddepartamentos").html(r);
	            $('#iddepartamentos').selectpicker('refresh');

	});
	$.post("../ajax/puestos.php?op=select", function(r){
	            $("#idpuestos").html(r);
	            $('#idpuestos').selectpicker('refresh');
    });	            
	$.post("../ajax/estatus.php?op=select", function(r){
	            $("#idestatus").html(r);
	            $('#idestatus').selectpicker('refresh');
	});
	

}

//Función limpiar
function limpiar()
{
	
	$("#nombre").val("");
	$("#fechadeingreso").val("");
	$("#fechadereingreso").val("");
	$("#fechadebaja").val("");
	$("#telefono").val("");
	$("#correo").val("");
	$("#imss").val("");
	$("#curp").val("");
	$("#clave").val("");
	$("#imagenmuestra").attr("src","");
	$("#imagenmuestra").val("");
	$("#idusuarios").val("");
}

function listarEmpleados(e)
{
	if (e==1)
	{
		Opcion=1;
		referencia=$('select[id=idsucursaless]').val();
			
	}
	if (e==2)
	{
		Opcion=2;
		referencia=$('#nombreb').val();
			
	}
	listar();
	
}
//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}

//Función Listar
function listar() 
{
	//alert("hola listando ");
	tabla=$('#tbllistado').dataTable(
	{
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla
	    "scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false,
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],
		"ajax":
				{
					url: '../ajax/usuarios.php?op=listar',
					data:{Opcion:Opcion,referencia:referencia},
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar

function guardaryeditar(e)
{
	e.preventDefault(); //No se activará la acción predeterminada del evento
	$("#btnGuardar").prop("disabled",true);
	var formData = new FormData($("#formulario")[0]);

	$.ajax({
		url: "../ajax/usuarios.php?op=guardaryeditar",
	    type: "POST",
	    data: formData,
	    contentType: false,
	    processData: false,

	    success: function(datos)
	    {              

	        //alert("resultado : "+datos);
	        
	        posicion=datos.indexOf("&")
	        archivo=datos.substr(0,posicion);
	        id=datos.substr(posicion+1,datos.length);
	        //alert("cadena "+datos+" id "+id+" archivo "+archivo);
	        if (archivo.length>4)
	    	{
	    		$("#imagenmuestra").attr("src","../files/usuarios/"+archivo);

	    	}
	    	else
			{
		    	if (id>0)
		    	{
		    		mensaje("Usuario actualizado con id "+id+" Sin imagen ");	
		    	}
	    	}

	    	
	          //mostrarform(false);
	          tabla.ajax.reload();
	    }

	});
	limpiar();
}

function mostrar(idusuarios)
{
	$.post("../ajax/usuarios.php?op=mostrar",{idusuarios : idusuarios}, function(data, status)
	{
		data = JSON.parse(data);		
		mostrarform(true);

		$("#nombre").val(data.nombre);
		$('#curp').val(data.curp);
		$('#imss').val(data.imss);
		$("#fechadeingreso").val(data.fechadeingreso);
		$("#fechadereingreso").val(data.fechadereingreso);
		$("#fechadebaja").val(data.fechadebaja);
		$("#telefono").val(data.telefono);
		$("#correo").val(data.correo);
		$("#sueldo").val(data.sueldo);
		$("#sdi").val(data.sdi);
		$("#login").val(data.login);
		$("#clave").val(data.clave);
		$("#iddepartamentos").val(data.iddepartamentos);
		$("#iddepartamentos").selectpicker('refresh');
		$("#idsucursales").val(data.idsucursales);
		$("#idsucursales").selectpicker('refresh');

		$("#idpuestos").val(data.idpuestos);
		$("#idpuestos").selectpicker('refresh');
		$("#idestatus").val(data.idestatus);
		$("#idestatus").selectpicker('refresh');

		$("#imagenmuestra").show();
		$("#imagenmuestra").attr("src","../files/usuarios/"+data.imagen);
		$("#imagenactual").val(data.imagen);
		$("#idusuarios").val(data.idusuarios);
 		$("#descuento").val(data.descuento);

 	})
 	$.post("../ajax/usuarios.php?op=permisos&id="+idusuarios,function(r){
 		//alert("permisos "+r);
		$("#permisos").html(r);
	});
}

//Función para desactivar registros
function desactivar(idusuarios)
{
	bootbox.confirm("¿Está Seguro de desactivar el usuario?", function(result){
		if(result)
        {
        	$.post("../ajax/usuarios.php?op=desactivar", {idusuarios : idusuarios}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(idusuarios)
{
	bootbox.confirm("¿Está Seguro de activar el Usuario?", function(result){
		if(result)
        {
        	$.post("../ajax/usuarios.php?op=activar", {idusuarios : idusuarios}, function(e){
        		bootbox.alert(e);
	            tabla.ajax.reload(); 
        	});	
        }
	})
}
function mensaje(titulo)
{
	Swal.fire({
		  position: 'top-end',
		  type: 'success',
		  title: titulo,
		  showConfirmButton: false,
		  timer: 1500
		})
}


init();