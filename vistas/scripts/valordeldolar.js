var tabla;
var nombre="";
//Función que se ejecuta al inicio
function init(){
	 $("#modulo").html("Abc Valor del dolar");
	mostrarform(false);
	listar();

	$("#formulario").on("submit",function(e)
	{
		guardaryeditar(e);	
	})

}

//Función limpiar

function limpiar()
{
	$("#nombre").val("");
	$("#idvalordeldolar").val("");
}

//Función mostrar formulario
function mostrarform(flag)
{
	limpiar();
	if (flag)
	{
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
	}
	else
	{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
	}
}

//Función cancelarform
function cancelarform()
{
	limpiar();
	mostrarform(false);
}
function buscar()
{
	//alert("buscando");
	nombre=$("#nombre").val();
   
   	listar();
}
//Función Listar
function listar()
{
	tabla=$('#tbllistado').dataTable(
	{
		"aProcessing": true,//Activamos el procesamiento del datatables
	    "aServerSide": true,//Paginación y filtrado realizados por el servidor
	    dom: 'Bfrtip',//Definimos los elementos del control de tabla

	    "scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false,
        
	    buttons: [		          
		            'copyHtml5',
		            'excelHtml5',
		            'csvHtml5',
		            'pdf'
		        ],
		"ajax":
				{
					url: '../ajax/valordeldolar.php?op=listar',
					data:{nombre:nombre},
					type : "get",
					dataType : "json",						
					error: function(e){
						console.log(e.responseText);	
					}
				},
		"bDestroy": true,
		"iDisplayLength": 5,//Paginación
	    "order": [[ 0, "desc" ]]//Ordenar (columna,orden)
	}).DataTable();
}
//Función para guardar o editar
function altaDeColonias()
{
	//alert("buscando ");
	//window.open('../vistas/colonias.php','BmpSistemas', 'width=800, height=600');
	
}
function guardaryeditar(e)
{
	//alert("parametro "+e);
	//e.preventDefault(); //No se activará la acción predeterminada del evento
	//$("#btnGuardar").prop("disabled",true);
	//if (e==1)
	//	$("#idvalordeldolar").val(0);
	idusuarios=$("#idusuarios").val();
	valor=$("#valor").val();
	alert("idusuairos "+idusuarios+"valor "+valor);
	if (idusuarios>0 && valor>0)
	{
		$.post("../ajax/valordeldolar.php?op=guardaryeditar",{valor:valor,idusuarios:idusuarios}, function(data, status)
		{
			mensaje(data);
			listar();
		    //tabla.ajax.reload();
		    
	 	});
 	}
 	else
 	{
 		mensaje("Datos incompletos");
 	}	

	
}

function mostrar(idvalordeldolar)
{
	$.post("../ajax/valordeldolar.php?op=mostrar",{idvalordeldolar : idvalordeldolar}, function(data, status)
	{
		data = JSON.parse(data);		
		//mostrarform(true);

		$("#nombre").val(data.nombre);
		$("#idvalordeldolar").val(data.idvalordeldolar);


 	})
}

//Función para desactivar registros
function desactivar(idvalordeldolar)
{
	bootbox.confirm("¿Está Seguro de desactivar la valor del dolar?", function(result){
		if(result)
        {
        	$.post("../ajax/valordeldolar.php?op=desactivar", {idvalordeldolar : idvalordeldolar}, function(e){
        		mensaje(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

//Función para activar registros
function activar(idvalordeldolar)
{
	bootbox.confirm("¿Está Seguro de activar la valor del dolar?", function(result){
		if(result)
        {
        	$.post("../ajax/valordeldolar.php?op=activar", {idvalordeldolar : idvalordeldolar}, function(e){
        		mensaje(e);
	            tabla.ajax.reload();
        	});	
        }
	})
}

function mensaje(titulo)
{
	Swal.fire({
		  position: 'top-end',
		  type: 'success',
		  title: titulo,
		  showConfirmButton: false,
		  timer: 1500
		})
}
init();