<?php
//activamos el almaceneamiento en el buffer
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) 
{
  header("location: login.html");
}
else
{
require 'header.php';
if (isset($_SESSION['capturista_id']) && $_SESSION['capturista_id']>0)
{

?>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                  	<div class="box-header with-border">
                    <div class="row">
	                    <div  class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
	                        <label>Fecha Inicial:</label>
	                        <input type="date"  class="form-control" name="fecha_inicial" id="fecha_inicial" maxlength="100" placeholder="Fecha Inicial" required>
	                    </div>
	                    <div id="divfechafinal" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
	                        <label>Fecha Final:</label>
	                        <input type="date"  class="form-control" name="fecha_final" id="fecha_final" maxlength="100" placeholder="Fecha Final" required>
	                    </div>
                      <div  class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-6">
                          
                          <button class="btn btn-success" onclick="mostrarServicios()" type="button"><i class="fa fa-search"></i> Buscar</button>
                      </div>
	                    <div id="divagregar" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-6">
	                        <button class="btn btn-success" onclick="mostrarform(true)" type="button"><i class="fa fa-plus-circle"></i> Agregar</button>
	                    </div>
                    </div>  
                	</div>
                    <!-- /.box-header -->
                    <!-- centro -->
                    
                    <div class="panel-body table-responsive" id="listadoregistros">
                        <table id="tbllistado" class="display compact nowrap">
                          <thead>
                            <th>Opciones</th>
                            <th>Cliente</th>
                            <th>FechaE</th>
                            <th>FechaS</th>
                            <th>Fecha Prox</th>
                            <th>Entro Por</th>
                            <th>Salio Por</th>
                            <th>Estado</th>
                          </thead>
                          <tbody>                            
                          </tbody>
                          <tfoot>
                          	<th>Opciones</th>
                            <th>Cliente</th>
                            <th>FechaE</th>
                            <th>FechaS</th>
                            <th>Fecha Prox</th>
                            <th>Entro Por</th>
                            <th>Salio Por</th>
                            <th>Estado</th>
                          </tfoot>
                        </table>
                    </div>
                    


                    <button type="button" data-toggle="collapse" class="btn btn-primary btn-lg btn-block"  data-target="#iniciarServicio"> Abrir el servicio</button>
                    <div id="iniciarServicio" class="collapse">
                        <form name="formulario" id="formulario" method="POST">                      
                        <div class="row">  
                          <div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12">
                            <label>Fecha Entrada(*):</label>
                            <input type="date"  class="form-control" name="fecha_de_entrada" id="fecha_de_entrada" maxlength="10" placeholder="Fecha Entrada" required>
                          </div>

                          <div class="form-group col-lg-8 col-md-6 col-sm-12 col-xs-12">
                            <input type="hidden" name="idusuarios" id="idusuarios" value="<?php echo $_SESSION['capturista_id']; ?> ">
                            <input type="hidden" name="Sidalmacenes" id="Sidalmacenes" value="<?php echo $_SESSION['almacen_id']; ?> ">
                            <input type="hidden" name="idalmacenes" id="idalmacenes" value="<?php echo $_SESSION['almacen_id']; ?> ">
                            
                            
                            <label>Cliente</label>
                            <input type="text" class="form-control" name="cliente" id="cliente" maxlength="100" placeholder="Cliente" required>
                          </div>
                          
                         
                        </div>

                        <div class="row">  
                          
                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>Servicio a Realizar(*):</label>
                            <textarea  class="form-control rounded-0" name="servicio_a_realizar" id="servicio_a_realizar" rows="3"></textarea>
                          </div>
                         

                        </div>
                          
                        <div class="row">
                          
                          
                           <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <label>Fecha Prox S(*):</label>
                            <input type="date"  class="form-control" name="fecha_prox_serv" id="fecha_prox_serv" maxlength="10" placeholder="Prox Servicio" required>
                          </div>

                          <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <label>Importe(*):</label>
                            <input type="number" step="0.01" class="form-control" name="importe" id="importe" maxlength="10" placeholder="Importe" required>
                          </div>

                          <div class="form-group col-lg-6 col-md-3 col-sm-12 col-xs-12">
                            <label>Imagen:</label>
                            <input type="file" class="form-control" name="imagen" id="imagen">
                            <input type="hidden" name="imagenactual" id="imagenactual">
                            <img src="" width="150px" height="120px" id="imagenmuestra">
                          </div>
                          
                          <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>
                            
                          </div>
                        </div>
                        </form>
                      </div>


                      <button type="button" data-toggle="collapse" class="btn btn-primary btn-lg btn-block"  data-target="#terminarServicio"> Cerrar el servicio</button>
                      <div id="terminarServicio" class="collapse">
                          <div class="row">  
                            <div class="form-group col-lg-2 col-md-2 col-sm-12 col-xs-12">
                            <label>Id:</label>
                            <input type="number"  class="form-control" name="idservicios" id="idservicios" maxlength="10" placeholder="Id Servicio" required>
                          </div>
                            <div class="form-group col-lg-8 col-md-8 col-sm-12 col-xs-12">
                              <label>Servicio Realizado(*):</label>
                              <textarea class="form-control rounded-0" name="servicio_realizado" id="servicio_realizado" rows="3"></textarea>
                            </div>
                            <div class="form-group col-lg-2 col-md-2 col-sm-12 col-xs-12">
                               <button class="btn btn-primary" onclick="editar()" type="button" id="btnActualizar"><i class="fa fa-save"></i> Guardar</button>
                            </div>
                          </div>
                        
                      </div>



                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
<?php
}
else
{
  require'noacceso.php'; 
}
require 'footer.php';
?>

<script type="text/javascript" src="scripts/servicios.js"></script>
<?php 
  }
  ob_end_flush();
?>