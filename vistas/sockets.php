<?php
$Room="BmpSistemas";
$Nombre="SErgio";  

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Bmp Sistemas | rilez.org</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../public/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../public/css/font-awesome.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../public/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../public/css/_all-skins.min.css">
    <link rel="apple-touch-icon" href="../public/img/apple-touch-icon.png">
    <link rel="shortcut icon" href="../public/img/favicon.ico">
	
    <!-- datatables -->

    <link rel="stylesheet" type="text/css" href="../public/datatables/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../public/datatables/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../public/datatables/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../public/css/bootstrap-select.min.css">
    
    <link rel="stylesheet" type="text/css" href="../public/css/sweetalert2.min.css">
    <style>  </style>

  </head>

    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        
                        <?php echo "Room $Room"; ?>                         	
                        
                        
                        <div  class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            
                         
                        </div>
                        <div class="btn-group pull-right">
                            <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                
                            </button>
                            <ul class="dropdown-menu slidedown">
                                Sign Out
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div id="mensaje" name="mensaje"></div>
                    </div>
                    <div class="panel-footer">
                        <div class="input-group">
                            <input id="Mensaje" type="text" class="form-control input-sm" placeholder="Type your message here..." />
                            <span class="input-group-btn">
                                <button onclick="enviarMensaje()" class="btn btn-warning btn-sm" id="btnEnviar" >
                                    Send</button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <li style="display:none" id="plantilla" class="left clearfix">
        <span class="chat-img pull-left">
         <img src="http://placehold.it/50/55C1E7/fff&text=U"class="img-circle" />
         chat
        </span>
        <div class="chat-body clearfix">
                <div class="header">
                  <strong class="primary-font Nombre" >Jack Sparrow</strong> 
                    <small class="pull-right text-muted">
                    <span class="glyphicon glyphicon-asterisk Tiempo"
                    </span> 12/02/2015 </small>
                </div>
                <p class="Mensaje">
                   Mensaje
                </p>
        </div>
    </li>


    

<script type="text/javascript" src="scripts/sockets.js"></script>
<script src="../public/js/jquery-3.1.1.min.js"></script>
</html>