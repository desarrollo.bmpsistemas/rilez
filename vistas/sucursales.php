  <?php
//Activamos el almacenamiento en el buffer 
ob_start();
session_start();

if (!isset($_SESSION["nombre"]))
{
  header("location: login.html");
}
else
{
require 'header.php';
if (isset($_SESSION['capturista_id']) && $_SESSION['capturista_id']>0)
{

?>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">
                      <div   class="form-check form-group col-lg-4 col-md-8 col-sm-8 col-xs-8">
                            
                          <input type="radio" onchange="checarRadioButton(1)" class="form-check-input" id="rbrfc" name="rbrfc">
                          <label class="form-check-label" for="materialchecked">Por Rfc</label>
                          <input type="radio" onchange="checarRadioButton(2)" class="form-check-input" id="rbnombre" name="rbnombre">
                          <label class="form-check-label" for="materialchecked">Por Nombre</label>
                          <input type="radio" onchange="checarRadioButton(3)" class="form-check-input" id="rbtelefono" name="rbtelefono">
                          <label class="form-check-label" for="materialchecked">Por Telefono</label>
                          <input type="text"  class="form-control"  name="busqueda" id="busqueda" maxlength="100" placeholder="rfc-nombre-tel" required>
                      </div>

                      
                       <div class="btn-group">
                            <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" data-toggle="tooltip" data-placement="center" title="De click aqui para opciones ">Opciones <span class="caret"></span></button>
                            <ul class="dropdown-menu" role="menu" >
                             <li onclick="listar(0)"><a href="#">Buscar Empresa</a></li>
                             <li onclick="mostrarform(true)"><a href="#">Agregar Empresa</a></li>
                            
                            </ul>
                      </div>      
                     

                    </div>
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoregistros">
                        <table id="tbllistado" class="display compact nowrap">
                          <thead>
                            <th>Opciones</th>
                            <th>Sucursal</th>
                            <th>NombreComercial</th>
                            <th>Rfc</th>
                            <th>Teléfono</th>
                            <th>Correo</th>
                            <th>Estado</th>
                          </thead>
                          <tbody>                            
                          </tbody>
                          <tfoot>
                            <th>Opciones</th>
                            <th>Sucursal</th>
                            <th>NombreComercial</th>
                            <th>Rfc</th>
                            <th>Teléfono</th>
                            <th>Correo</th>
                            <th>Estado</th>
                          </tfoot>
                        </table>
                    </div>
                    <div class="panel-body" style="height: 400px;" id="formularioregistros">
                        <form name="formulario" id="formulario" method="POST">
                          
                          <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <label>id:</label>

                            <input type="text" class="form-control" readonly name="idpersonas" id="idpersonas" maxlength="10" placeholder="Id" required>
                          </div>

                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Nombre:</label>
                            <input type="text" class="form-control" name="nombre" id="nombre" maxlength="50" placeholder="Nombre" required>
                          </div>
                          <div class="form-group col-lg-4 col-md-12 col-sm-12 col-xs-12">
                            <label>Nombre Comercial:</label>
                            <input type="text" class="form-control" name="nombrecomercial" id="nombrecomercial" maxlength="100" placeholder="Nombre Comercial"  required>
                          </div>

                          

                          <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>RFC:</label>
                            <input type="text" class="form-control" name="rfc" id="rfc" maxlength="20" placeholder="Rfc"  required>
                          </div>
                          
                          <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>Teléfono:</label>
                            <input type="text" class="form-control" name="telefono" id="telefono" maxlength="20" placeholder="Teléfono"  required>
                          </div>

                          <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>Correo:</label>
                            <input type="email" class="form-control" name="correo" id="correo" maxlength="50" placeholder="Correo"  required>
                          </div>


                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>Referencia / Domicilio .:</label>
                            <textarea class="form-control rounded-0" id="referencia" name="referencia" rows="3"  required></textarea>
                          </div>
                          


                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>

                            <button class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
                          </div>
                        </form>
                    </div>
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
    <div class="modal fade" id="myModalMunicipios" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
      <div class="modal-dialog" style="width: 90% !important;">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Municipios</h4>
            </div>
            <div class="modal-body">
              
                <tbody>
                    <div class="panel-body" style="height: 100px;" id="formulariorMunicipios">
                      
                        <a data-controls-modal="myModal" data-backdrop="static" data-keyboard="false" href="#">
                        <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                          <label>Paises: </label>
                          <select onchange="selectestados()" id="idpaisesm" name="idpaisesm" class="form-control selectpicker" data-live-search="true" required></select>
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                          <label>Estados: </label>
                          <select id="idestadosm" name="idestadosm" class="form-control selectpicker" data-live-search="true" required></select>
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                          <label">Municipio(*):</label>
                          <input type="text" class="form-control" name="municipio" id="municipio" maxlength="50" placeholder="Municipio" required>
                        </div>

                        
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                          <button  class="btn btn-primary" onclick="agregarMunicipio()" id="btnGuardarMunicipio"><i class="fa fa-save"></i> Guardar Municipio </button>
                        </div>
                      
                  </div>
                </tbody>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>        
          </div>
      </div>
  </div> 

  <div class="modal fade" id="myModalColonias" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
      <div class="modal-dialog" style="width: 90% !important;">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Colonias</h4>
            </div>
            <div class="modal-body">
              
                <tbody>
                    <div class="panel-body" style="height: 100px;" id="formularioColonias">
                      
                        <a data-controls-modal="myModal" data-backdrop="static" data-keyboard="false" href="#">
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <label>Colonia(*):</label>
                          <input type="text" class="form-control" name="colonia" id="colonia" maxlength="50" placeholder="Colonia" required>
                        </div>
                        

                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <button  class="btn btn-success" onclick="buscarColonia()"></i> Buscar Colonia </button>
                          <button  class="btn btn-primary" onclick="agregarColonia()" id="btnGuardarColonia"><i class="fa fa-save"></i> Guardar Colonia </button>
                        </div>
                      
                  </div>
                </tbody>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>        
          </div>
      </div>
  </div> 

  <div class="modal fade" id="myModalCalles" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
      <div class="modal-dialog" style="width: 90% !important;">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Calles</h4>
            </div>
            <div class="modal-body">
              
                <tbody>
                    <div class="panel-body" style="height: 100px;" id="formularioCalles">
                      
                        <a data-controls-modal="myModal" data-backdrop="static" data-keyboard="false" href="#">
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <label>Calle(*):</label>
                          <input type="text" class="form-control" name="calle" id="calle" maxlength="50" placeholder="Colonia" required>
                        </div>
                        

                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <button  class="btn btn-success" onclick="buscarCalle()"></i> Buscar Calle </button>
                          <button  class="btn btn-primary" onclick="agregarCalle()" id="btnGuardarCalle"><i class="fa fa-save"></i> Guardar Calle </button>
                        </div>
                      
                  </div>
                </tbody>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>        
          </div>
      </div>
  </div> 

  <!--Fin-Contenido-->
<?php
}
else
{
  require 'noacceso.php';
}
require 'footer.php';
?>
<script type="text/javascript" src="scripts/sucursales.js"></script>
<?php 
}
ob_end_flush();
?>