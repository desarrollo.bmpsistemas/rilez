<?php
//activamos el almaceneamiento en el buffer
ob_start();
session_start();

if (!isset($_SESSION["nombre"]))
{
  header("location: login.html");
}
else
{
require 'header.php';
if (isset($_SESSION['capturista_id']) && $_SESSION['capturista_id']>0)
{
?>
<a href="https://www.positivessl.com/trusted-ssl-site-seal.php" style="font-family: arial; font-size: 10px; color: #212121; text-decoration: none;"><img src="https://www.positivessl.com/images-new/comodo_secure_seal_76x26_transp.png" alt="Trusted Site Seal" title="Trusted Site Seal for Transparent background" border="0" /></a><div style="font-family: arial;font-weight:bold;font-size:15px;color:#86BEE0;"><a href="https://www.positivessl.com" style="color:#86BEE0; text-decoration: none;">SSL Certificate</a></div>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content --> 
      <div class="content-wrapper">        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">
                      <div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <label>Sucursal(*): </label>
                            <select id="idsucursaless" name="idsucursaless" class="form-control selectpicker"  data-live-search="true" required onchange="listarEmpleados(1)" >
                            </select>
                      </div>
                      <div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <label>Nombre(*):</label>
                             <input type="text" class="form-control" onchange="listarEmpleados(2)" name="nombreb" id="nombreb" maxlength="50" placeholder="Nombre" required>
                      </div>
                      <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-2">

                         <h1><button onclick="buscar()" class="btn btn-primary" id="btnbuscar" name="btnbuscar" >Buscar</button></h1>
                      </div>
                      <div class="form-group col-lg-2 col-md-6 col-sm-6 col-xs-12">

                          
                          <h1><button class="btn btn-success" id="btnagregar" onclick="mostrarform(true)"><i class="fa fa-plus-circle"></i> Agregar</button></h1>
                      </div>    
                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoregistros">
                        <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">
                          <thead>
                            <th>Opciones</th>
                            <th>Sucursal</th>
                            <th>Nombre</th>
                            <th>Fecha Ingreso</th>
                            <th>Imss</th>
                            <th>Estatus</th>
                            <th>Login</th>
                            <th>Foto</th>
                            <th>Estado</th>
                          </thead>
                          <tbody>                            
                          </tbody>
                          <tfoot>
                            <th>Opciones</th>
                            <th>Sucursal</th>
                            <th>Nombre</th>
                            <th>Fecha Ingreso</th>
                            <th>Imss</th>
                            <th>Estatus</th>
                            <th>Login</th>
                            <th>Foto</th>
                            <th>Estado</th>
                          </tfoot>
                        </table>
                    </div>
                    <div class="panel-body" id="formularioregistros">
                        <form name="formulario" id="formulario" method="POST">
                          <div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <label>Sucursal(*): </label>
                            <select id="idsucursales" name="idsucursales" class="form-control selectpicker" data-live-search="true" required  > <!-- onchange="listarestados()-->
                            </select>
                          </div>
                          <div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <label>Nombre(*):</label>
                             <input type="hidden" name="idusuarios" id="idusuarios">
                            <input type="text" class="form-control" name="nombre" id="nombre" maxlength="100" placeholder="Nombre" required>
                          </div>
                          
                          <div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <label>Curp(*):</label>
                             <input type="text" class="form-control" name="curp" id="curp" maxlength="20" placeholder="Curp" required>
                          </div>
                          
                          <div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <label>Departamento(*): </label>
                            <select id="iddepartamentos" name="iddepartamentos" class="form-control selectpicker" value="iddepartamentos" data-live-search="true" required onchange="listarestados()" >
                            </select>
                          </div>                          
                          <div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <label>Puestos(*): </label>
                            <select id="idpuestos" name="idpuestos" class="form-control selectpicker" value="idpuestos" data-live-search="true" required onchange="listarestados()" >
                            </select>
                          </div>
                          <div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <label>Estatus(*): </label>
                            <select id="idestatus" name="idestatus" class="form-control selectpicker" value="idestatus" data-live-search="true" required onchange="listarestados()" >
                            </select>
                          </div>

                          <div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <label>Fecha de Ingreso(*):</label>
                            <input type="date" class="form-control" name="fechadeingreso" id="fechadeingreso" maxlength="20" placeholder="Fecha de ingreso" required>
                          </div>

                          <div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <label>Fecha de Reingreso(*):</label>
                            <input type="date" class="form-control" name="fechadereingreso" id="fechadereingreso" maxlength="10" placeholder="Fecha de reingreso" readonly>
                          </div>

                          <div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <label>Fecha de Baja:</label>
                            <input type="date" class="form-control" name="fechadebaja" id="fechadebaja" placeholder="fechadebaja" maxlength="10" readonly>
                          </div>

                          <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>Telefono:</label>
                            <input type="text" class="form-control" name="telefono" id="telefono" maxlength="30" placeholder="Telefono">
                          </div>
                          
                          <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>Imss:</label>
                            <input type="text" class="form-control" name="imss" id="imss" maxlength="15" placeholder="Num imss">
                          </div>
                          <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>Sueldo:</label>
                            <input type="text" class="form-control" name="sueldo" id="sueldo" maxlength="8" placeholder="Sueldo">
                          </div>

                          <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>Sdi:</label>
                            <input type="text" class="form-control" name="sdi" id="sdi" maxlength="8" placeholder="Salario Integado">
                          </div>
                          <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <label>Correo(*):</label>
                            <input type="email" class="form-control" name="correo" id="correo" maxlength="64" placeholder="Clave" required>
                          </div>

                          <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <label>Login(*):</label>
                            <input type="text" class="form-control" name="login" id="login" maxlength="20" placeholder="Login" required>
                          </div>
                          <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <label>Clave(*):</label>
                            <input type="password" class="form-control" name="clave" id="clave" maxlength="64" placeholder="Clave" required>
                          </div>
                          <div class="form-group col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <label>Descuento(*):</label>
                            <input type="number" step="0.01" class="form-control" name="correo" id="descuento" maxlength="5" placeholder="descuento" required>
                          </div>

                          <div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <label>Imagen:</label>
                            <input type="file" class="form-control" name="imagen" id="imagen">
                            <input type="hidden" name="imagenactual" id="imagenactual">
                            <img src="" width="150px" height="120px" id="imagenmuestra">
                          </div>
                          
                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>

                            <button class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
                          </div>
                        </form>
                    </div>
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
<?php
}
else
{
  require'noacceso.php'; 
}
require 'footer.php';
?>

<script type="text/javascript" src="scripts/usuarios.js"></script>
<?php 
  }
  ob_end_flush();
?>