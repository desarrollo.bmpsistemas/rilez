<?php
//activamos el almaceneamiento en el buffer
ob_start();
session_start();


if (!isset($_SESSION["nombre"]))
{
  header("location: login.html");
}
else
{
require 'header.php';
if (isset($_SESSION['capturista_id']) && $_SESSION['capturista_id']>0)
{

?>

<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">        
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">
                      
                      <div class="input-group form-group col-lg-6 col-md-8 col-sm-10 col-xs-12">
                        <input type="hidden" name="idusuarios" id="idusuarios" value="<?php echo $_SESSION['capturista_id']; ?> "> <!--data-toggle="tooltip" title="Buscar"  data-placement="right"-->
                        <input type="number" step="0.01" name="valor" id="valor"  maxlength="8" class="form-control"  placeholder="Valor">
                        <div onclick="buscar()" data-toggle="tooltip" title="Buscar" class="input-group-addon"><i class="fa fa-search"  ></i></div>
                        <div onclick="guardaryeditar('I')" data-toggle="tooltip" title="Nuevo"  id="btnagregar" class="input-group-addon"><i class="fa fa-save"  ></i></div>
                        
                      </div>


                      

                      
                    </div>
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive" id="listadoregistros">
                        <table id="tbllistado" class="display compact nowrap">
                          <thead>
                            <th>Opciones</th>
                            <th>Valor</th>
                            <th>Fecha</th>
                            <th>Usuario</th>
                            <th>Estado</th>
                          </thead>
                          <tbody>                            
                          </tbody>
                          <tfoot>
                            <th>Opciones</th>
                            <th>Valor</th>
                            <th>Fecha</th>
                            <th>Usuario</th>
                            <th>Estado</th>
                          </tfoot>
                        </table>
                    </div>
                    
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
<?php
}
else
{
  require'noacceso.php'; 
}

require 'footer.php';
?>
    

<script type="text/javascript" src="scripts/valordeldolar.js"></script>

 
  
  


<?php 
  }
  ob_end_flush();
?>