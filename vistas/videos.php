<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<head>
  <link href="https://vjs.zencdn.net/7.7.5/video-js.css" rel="stylesheet" />

  <!-- If you'd like to support IE8 (for Video.js versions prior to v7) -->
  <script src="https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
</head>
</head>
 
<body>
    <h1>Poner contenido encima de un vídeo</h1>
 
  
	<video
		id="my-video"
	    class="video-js"
	    controls
	    preload="auto"
	    width="640"
	    height="264"
	    poster="MY_VIDEO_POSTER.jpg"
	    data-setup="{}">

	  <source src="../files/ayudas/video.mp4" type='video/webm; codecs="vp8, vorbis"' />
	  <source src="../files/ayudas/video.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
	  <source src="../files/ayudas/video.mp4" type='video/ogg; codecs="theora, vorbis"' />
	  Video tag not supported. Download the video <a href="movie.webm">here</a>.
	<video>	

</body>
</html>